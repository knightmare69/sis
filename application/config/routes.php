<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller']  = "profile";
$route['404_override']        = 'profile';
$route['69']                  = 'error/error_69';

$route['facultyconfig']       = "sisconfig/facultyconfig";
$route['logout']              = "home/logout";

$route['generate_key/(:any)'] = "home/spec_op/1/$1";
$route['getmac/(:any)']       = "home/spec_op/2/$1";
$route['l0gout']              = "home/spec_op/3";

$route['applicant']           = "admission/applicant";
$route['application']         = "admission/applicant";

$route['epayment/(:any)']     = "epayment/index/$1";

/*added by: ARS Nov.15.2018 13:32H */
$route['faculty-evaluation']         = "faculty_evaluation/index/$1";
$route['faculty-evaluation/(:any)']         = "faculty_evaluation/index/$1";

$route['faculty-evaluation-summary']         = "faculty_evaluation_summary/index/$1";
$route['faculty-evaluation-summary/(:any)']  = "faculty_evaluation_summary/$1";




/* End of file routes.php */
/* Location: ./application/config/routes.php */
