
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Inbox extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_inbox','',TRUE);
   $this->load->model('user','',TRUE);
 }

 function index()
 {
   $xget = $this->input->get(NULL, TRUE);	 
   if($this->session->userdata('logged_in'))
   {
     $session_data     = $this->session->userdata('logged_in');
     $data['userinfo'] = $session_data;
     $data['title']    = "Inbox";
	 $data['uid']      = $session_data['id'];
	 $data['list']     = array();
	 $data['direct']   = 'from';
	 $data['page']     = is_key_exist($xget,'page','inbox'); 
	 
	 $mode             = (($data['page']!='inbox')?$data['page']:'mail');
	 $data['mode']     = $mode;
	 
	 if($xget && is_key_exist($xget,'id',''))
	 {
	  $userid = is_key_exist($session_data,'id','');
	  $msgid  = is_key_exist($xget,'id','');
	  if($userid!='' && $msgid!='')
	  {
	   $data['uid']     = $userid;
	   $data['content'] = $this->mod_inbox->load_content($userid,$msgid);	 
	   $data['display'] = $this->load->view('mail/content',$data,true); 
	  } 
	 }
     else
     {
	  $data['count']    = $this->mod_inbox->load_mail($session_data['id'],$mode,10,0,true);
	  $data['list']     = $this->mod_inbox->load_mail($session_data['id'],$mode,10,0);
	 }
	 
     $data['jslink']   = array('plugin/delete-table-row/delete-table-row.js',
                               'plugin/summernote/summernote.js',
                               'utilities/inbox.js');     
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_inbox', $data);
     $this->load->view('include/footer',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function txn($type="",$mode="",$args=array())
 {
   $session_data = $this->session->userdata('logged_in');
   $xpost        = $this->input->post(NULL,TRUE);
   $result       = array('success'=>false,'content'=>'','error'=>'Unknown Error');
   $json         = true; 
   $data         = array();
   switch($type)
   {
	 case 'get':
	  switch($mode)
	  {
		case 'compose':
		 $json = false;
		 $this->load->view('mail/compose');
        break;		
		case 'mail':
		case 'notify':
		case 'sent':
		case 'trash':
		 $json = false;
		 $page = ((!is_numeric($args))?0:$args);
		 if($this->session->userdata('logged_in')) 
         {
          $session_data     = $this->session->userdata('logged_in');
	      $data['userinfo'] = $session_data;
		  $data['uid']      = $session_data['id'];
		  $data['list']     = array();
		  $data['direct']   = 'from';
		  $data['mode']     = $mode;
		  $data['count']    = $this->mod_inbox->load_mail($session_data['id'],$mode,10,0,true);
		  $data['list']     = $this->mod_inbox->load_mail($session_data['id'],$mode,10,($page*10));
		  switch($mode)
		  {
           case 'sent':
            $data['direct'] = 'to';
		    break;
		   case 'trash':
            $data['direct'] = 'both';
		    break;
		  }
	     }
		 $this->load->view('mail/list_content',$data);
        break;		
	   case 'flags':
		 $json = false;
		 if($this->session->userdata('logged_in')) 
         {
          $session_data     = $this->session->userdata('logged_in');
	      $data['userinfo'] = $session_data;
	      $data['notes']    = $this->mod_inbox->load_flags($session_data['id']);
	     }
         $this->load->view('mail/flags',$data);
   		break;			
	   case 'badge':
		 if($this->session->userdata('logged_in')) 
         {
          $session_data = $this->session->userdata('logged_in');
	      $flags        = $this->mod_inbox->load_flags($session_data['id'],true);
		  if($flags)
		  {
			$result['success']=true;
            foreach($flags as $c)
            {
			 $result['count'] = is_key_exist($c,'TotalItem',0);	
			 $result['date']  = date('m/d/Y h:i A');
			}			
		  }	  
	     }
   		break;	
	   case 'content':
		 $json = false;
		 $userid = is_key_exist($session_data,'id','');
		 $msgid  = $args;
		 if($userid!='' && $msgid!='')
		 {
		  $data['uid']     = $userid;
		  $data['content'] = $this->mod_inbox->load_content($userid,$msgid);	 
		  $this->load->view('mail/content',$data); 
		 }	 
		 else
		  echo '<i class="fa fa-warning"></i> Invalid Parameters.';
		break;
       default:
		 $json = false;
        break;	   
	  }
	  
	  if($json){echo json_encode($result); }
	  break;
     case 'set':
	  switch($mode)
	  {
	   case 'send':
	     $typeid   = is_key_exist($xpost,'xtype',0); 
	     $xid      = is_key_exist($xpost,'uid',array()); 
	     $xto      = is_key_exist($xpost,'xto',''); 
	     $xsubj    = is_key_exist($xpost,'xsubj',''); 
		 $xcontent = is_key_exist($xpost,'xcontent',''); 
	     
		 if($typeid==2)
		 {
		  $config='';
	      $msg = $this->load->view('mail/mail_template',array('msg'=>$xcontent),true);
    
	      try
		  {	  
	       $xsubj = 'From '.$session_data['id'].'('.$session_data['email'].'):'.$xsubj;
		   $result['send'] = ($this->mod_main->xsendmail($config,$xto,$msg,$xsubj));
          }
		  catch(Exception $e)
		  {
	       $result['send'] = false;
		  }  
	     }
		 
		 foreach($xid as $u)
		 {
		  $issave = $this->mod_inbox->create_inbox($u,$typeid,$xsubj,$xcontent);
		 }
		 
	     $result['success']=true;
		 $result['content']='';
        break;
	   case 'isread':
	     $uid = is_key_exist($session_data,'id','');
		 $nid = is_key_exist($xpost,'nid');
		 if($nid){
		   foreach($nid as $n)
		   {
		     $issave = $this->mod_inbox->isread($uid,$n);
		   }
		 }
         $result['success']=true;
		 $result['content']='';
        break;	   
       case 'delete':
	     $uid = is_key_exist($session_data,'id','');
		 $mid = is_key_exist($xpost,'mid');
		 foreach($mid as $m)
		 {
		  $isdelete = $this->mod_inbox->delete_inbox($uid,$m);
		 }
		 
         $result['success']=true;
		 $result['content']='';
        break;	   
	  }
	  
	  if($json){echo json_encode($result); }
      break;	 
   }
 }
 
 function trial()
 {
  echo '<pre>';
  print_r($this->mod_inbox->load_flags('knightmare69',true));  
 }
 
}
?>

