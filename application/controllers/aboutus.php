<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class AboutUs extends CI_Controller {

 function __construct(){
   parent::__construct();
 }

 function index(){
  
   if($this->session->userdata('logged_in')) {
     $session_data = $this->session->userdata('logged_in');
     if ($session_data['idtype'] == 0){ redirect('activation', 'refresh'); }
     
     $data['userinfo'] = $session_data;
     $data['title'] = "About Us";
     $data['username'] = $session_data['username'];               
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_aboutus', $data);
     $footer['jslink'] = array();
     $this->load->view('include/footer',$footer);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 }