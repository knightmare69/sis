<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class accountabilities extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_accountabilities','',true);
   $this->load->model('mod_main','',TRUE);
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $this->mod_main->monitoring_action($session_data,'accountabilities');
	 
     $data['userinfo'] = $session_data;
     $data['title'] = "Accountabilities";
     $data['username'] = $session_data['username'];  
     $data['idtype'] = $session_data['idtype'];      
     $data['studentno'] = $session_data['idno'];
     $data['studentnolist'] = $this->mod_main->ListofParentChild($session_data['id']);
     
     if($data['idtype'] == 2)
      $data['studentno'] = $data['studentnolist'][0]->StudentNo;
	 else if($data['idtype'] == '-1')
	 {
	  $data['studentfilter']=1;	 
	  $data['studentno'] = 0;
	 }
     
	 if($data['studentno']==0)
	  $data['xtermid'] = '';
     else	 
      $data['xtermid'] = $this->mod_accountabilities->xtermID($data['studentno']);  
     
	 $data['ds'] = $this->mod_accountabilities->usp_getAccountabilities($data['studentno']);
     $data['jslink']=array('utilities/accountabilities.js');
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_accountabilities', $data);
     $this->load->view('include/footer',$data);
     $this->mod_main->Translog($session_data['id'],'View Accountabilities','-');
   }
   else
   {
    redirect('login', 'refresh');
   }
 }
 
 function data($opt='')
 {
  $output = array('success'=>false,'content'=>'','error'=>'Failed!');
  $p=$this->input->post();
  if($p==false){$p=array(); }
  switch($opt)
  {
   case 'info':
    $data['xdetail'] = $this->mod_main->call_xtraDetails($p['student']);
	$data['info_request']=true;
    if($p['student']!='')
	{	
	 $output['success'] =true;
	 $output['content'] = $this->load->view('include/studentinfo',$data,true);
	 $output['xtra']=$data['xdetail'];
	}	
   break;   
   case 'accounts':
   $data['studentno']=((array_key_exists('student',$p))?$p['student']:'');
   if($data['studentno']!='')
   {	   
    $data['ds'] = $this->mod_accountabilities->usp_getAccountabilities($data['studentno']);
    $output['success'] = true;
	$output['content'] = $this->load->view('vw_accountabilities_details',$data,true);
   }
   break;   
  }
  echo json_encode($output);  
 }

}
?>


