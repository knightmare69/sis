<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Actionrequired extends CI_Controller 
{
 
 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_crypto','',TRUE);
   $this->load->model('user','',TRUE);
   $this->load->helper(array("form"));
   $this->AdptPRISMS = $this->mod_main->getconfigvalue('AdaptSecurity');    
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $this->load->helper(array("form"));	
     $session_data = $this->session->userdata('logged_in');
     if ($session_data['lock'] == 1){redirect('lock', 'refresh');} 
	 if ($session_data['xaction'] == ''){redirect('profile', 'refresh');} 
	 
	 $xaction = $session_data['xaction'];
     $data['userinfo'] = $session_data;
     $data['userid'] = $session_data['id'];
     $data['title'] = "Action Required";
     $data['username'] = $session_data['username'];
	 $data['actionform'] = $this->user->action_to_complete($xaction,$session_data['idtype']);
     $mfooter['jslink'] = array('utilities/actionrequired.js');
     
     $this->load->view('include/header',$data);
     $this->load->view('vw_actionrequired', $data);
     $this->load->view('include/footer',$mfooter );
  }
  else
  {
   redirect('login', 'refresh');
  }
 }
 
 function process()
 {
  $prompt  = '';	 
  $profile = 0;
  if($this->input->post() and $this->session->userdata('logged_in'))
  {
   $success      = false;
   $session_data = $this->session->userdata('logged_in');
   $posts        = $this->input->post(NULL,true);
   
   if(array_key_exists('email',$posts) and trim($posts['email'])!='')
   {     
     $posts['email'] = ((strpos($posts['email'],'@')!==false)?substr($posts['email'],0,strpos($posts['email'],'@')):$posts['email']);
     $posts['email'] = ((defined('FACULTY_EMAIL') && FACULTY_EMAIL!='')?($posts['email'].FACULTY_EMAIL):$posts['email']);
	 $success = $this->user->isave_profileinfo($session_data['id'],$session_data['idno'] ,$session_data['idtype'], 'Email', $posts['email']);
     if(($session_data['idtype']=='3' || $session_data['idtype']=='-1') && defined('FACULTY_EVALID') && FACULTY_EVALID==1){
	   $exec = $this->user->recreate_vcode($session_data['id'],$posts['email'],true); 	
	   if(EMAIL_NOTIFY==1){
		   $email = trim($posts['email']);         
           $verify = explode('|', $exec);         
           $today  = new DateTime('NOW');
           date_add($today,date_interval_create_from_date_string("15 days"));
           $data['username'] = $session_data['id'];
           $data['email']    = $email;
           $data['title']    = "PRISMS Online:Action Required";
	       $data['verify']   = site_url('register/verify').'/' .$verify[0]; 
           $data['today']    = $verify[1] ; //$today->format( 'Y-m-d H:i:s' );
           $data['msg']      = "<h3>Hi, ".$data['username']."</h3>
				                <p class='lead'>We would like to thank you for registering your email account to PRISMS Portal.</p>
				                <p>You've entered ".mailto($email,$email)." as the contact email address for your PRISMS Portal. 
								   To complete the process, we just need to verify that this email address belongs to you.
				                   Simply click the link below and sign using your PRISMS Portal Username and password.
				                </p>
				                <!-- Callout Panel -->
				                <p class='callout'>
					            <a href='".$data['verify']."'>Verify Now! &raquo;</a> <i> This validation code is valid until ".$data['today']."</i>
				                </p><!-- /Callout Panel -->";
          $message           = $this->load->view('mail/mail_template', $data, TRUE);
          $issent            = $this->mod_main->xsendmail('',$email,$message); 
	      $prompt            = "<p>Please check your email for verification code.</p>";
		  $this->session->unset_userdata('logged_in');
	   }else{
	      $prompt            = "<p>Kindly communicate with your school IT administrator to validate your account.</p>";
	   }	   
	 }
   }
   
   if(array_key_exists('newpwd',$posts) && array_key_exists('conpwd',$posts) && trim($posts['newpwd'])==trim($posts['conpwd']))
   {
    $success = $this->user->ichangethypassword($session_data['id'],$posts['newpwd']);
    if($this->AdptPRISMS==1){
      $user       = $session_data['idno'];
      $conditions = array(1,3,5,7);
      $crypto     = $this->mod_crypto->BitEncrypt($posts['newpwd'],$conditions);
      $ESpass     = $this->user->ichangeESpassword($user,$crypto);
    } 
   }
   
   if(array_key_exists('bday',$posts))
   {
     $success = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'BirthDate', $posts['bday']);
     $profile++;
   }
   if(array_key_exists('bplace',$posts))
   {
     $success = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'BirthPlace', $posts['bplace']);
     $profile++;
   }
   if(array_key_exists('civil',$posts))
   {
     $success = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'CivilStatusID', $posts['civil']);
     $profile++;
   }
   if(array_key_exists('nation',$posts))
   {
     $success = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'NationalityID', $posts['nation']);
     $profile++;
   }
   if(array_key_exists('religion',$posts))
   {
     $success = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'ReligionID', $posts['religion']);
     $profile++;
   }
   if(array_key_exists('telno',$posts))
   {
     $success = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'TelNo', $posts['telno']);
     $profile++;
   }
   if(array_key_exists('mobile',$posts))
   {
     $success = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'MobileNo', $posts['mobile']);
     $profile++;
   }
   if(array_key_exists('street',$posts))
   {
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'res_street', $posts['street'] );
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'perm_street', $posts['street'] );
     $profile++;
   }
   if(array_key_exists('brgy',$posts))
   {
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'res_barangay', $posts['brgy'] );
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'perm_barangay', $posts['brgy'] );
     $profile++;
   }
   if(array_key_exists('city',$posts))
   {
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'res_towncity', $posts['city'] );
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'perm_towncity', $posts['city'] );
     $profile++;
   }
   if(array_key_exists('prov',$posts))
   {
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'res_province', $posts['prov'] );
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'perm_province', $posts['prov'] );
     $profile++;
   }
   if(array_key_exists('zip',$posts))
   {
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'res_zipcode', $posts['zip'] );
     $success  = $this->user->isave_profileinfo($session_data['id'], $session_data['idno'], $session_data['idtype'], 'perm_zipcode', $posts['zip'] );
     $profile++;
   }
   
   if($profile>0){
	   $req_profile = str_replace("index.php","assets/required/profile/",$_SERVER['SCRIPT_FILENAME']);
	   if($session_data['idtype']==3 || $session_data['idtype']==-1)
	    $req_profile .= 'employee/'.$session_data['idno'].'.txt';
	   else if($session_data['idtype']==1)
	    $req_profile .= 'student/'.$session_data['idno'].'.txt';
	   $handle  = @fopen($req_profile, 'w');
   }
   
   if($success){
	if($prompt!='')
	   die($prompt); 	   
    else{
      $session_data['xaction']='';
      $this->session->set_userdata('logged_in', $session_data);
      if($session_data['idtype']==0)
        redirect('activation', 'refresh');
	  else
	    redirect('profile', 'refresh');	
    }
   }
  }

  redirect('actionrequired', 'refresh');
 }
 
}
?>