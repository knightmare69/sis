
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Activation extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_activation','',TRUE);
   $this->load->helper(array("form"));
 }

 function index($target=''){
   $studentno=StripSlashes($this->input->post('studentno'));
   
   if($this->session->userdata('logged_in'))
   {
    $this->load->helper(array("form"));				
     $session_data = $this->session->userdata('logged_in');
	 if ($session_data['lock'] == 1){redirect('lock', 'refresh');} 
	 if ($session_data['appcount'] > 0){redirect('admission', 'refresh');} 
	 if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 $data['sidebar']       = false;
	 $data['xact_admission']= ACTIVE_ADMISSION;
     $data['xact_student']  = ACTIVE_STUDENT;
     $data['xact_parent']   = ACTIVE_PARENT;
     $data['xact_faculty']  = ACTIVE_FACULTY;
     $data['xact_admin']    = ACTIVE_ADMIN;   
	 
     $data['userinfo'] = $session_data;
     $data['userid']   = $session_data['id'];
     $data['username'] = $session_data['username'];
     if($target=='-1' || $target==''){
	 $data['target']   = $this->mod_activation->get_targettype($data['userid']);
     }else{
	 $data['target']   = $target;
	 }
	 
	 $data['ayterm']   = $this->mod_activation->get_ayterms();
     $data['program']  = $this->mod_main->get_courselist();
     $data['pprogram'] = $this->mod_main->get_courselist();
	 
     $data['title']    = "Activation";
	 $data['postdata'] = array();
	 
	 if($this->session->userdata('post_data'))
     {
	  $data['postdata'] = $this->session->userdata('post_data');
	  $this->session->unset_userdata('post_data');
	 }
	 
	 if(isset($data['postdata']['pageid'])==false)
	 {
	  if($data['xact_admin']==1)
        $data['postdata']['pageid']	=5;
		
	  if($data['xact_faculty']==1)
        $data['postdata']['pageid']	=4;
		
	  if($data['xact_parent']==1)
        $data['postdata']['pageid']	=3;
		
	  if($data['xact_student']==1)
        $data['postdata']['pageid']	=2;
		
	  if($data['xact_admission']==1)
        $data['postdata']['pageid']	=1;
	 }
     
      if ($studentno != '')
        {
          
            $data['student'] = $this->mod_activation->get_StudentInfo($studentno);
            
            if ( count($data['student']) == 0)
            {
              $data['studentno'] = $studentno;
              $data['name'] = '';
              $data['degree'] = '';
            }
            else
            {
              $data['studentno'] = $studentno;
              $data['name'] = $data['student'][0]->StudentName;
              $data['degree'] = $data['student'][0]->ProgName;
            }
        }
        else
        {
          $data['student'] = '';
          $data['studentno'] = '';
          $data['name'] = '';
          $data['degree'] = '';
        }
     
     
     $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                           'plugin/fuelux/wizard/wizard.js',
                           'plugin/jquery-form/jquery-form.min.js',
                           'jquery.cookies.js',
                           'utilities/activation.js'                           
                           );
     
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_activation', $data);
     $this->load->view('include/footer',$mfooter );
	 $this->mod_main->Translog($session_data['id'],'View Portal Activation','-');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
  function checkifscholar()
  {
   if($this->input->post())
   {
    $regid = $this->input->post('reg',true);
    $checker = $this->mod_activation->checkifscholar($regid);
    if($checker)
    {
    $checker = $checker[0];
    $ischolar = property_exists($checker,'isScholar')? $checker->isScholar : 0;
    echo $ischolar;
    }
    else
    {echo 0;}
    
	$this->mod_main->Translog($session_data['id'],'Portal Activation - Check if Scholar','RegID:'.$regid);
   }
   else
   {
   echo 0;
   }
   
  } 
 
 function verifystudent() {
  if($this->session->userdata('logged_in'))
   {
    $this->load->helper(array("form"));
    $this->load->model('mod_activation','',TRUE);
     $session_data = $this->session->userdata('logged_in');
     $data['userinfo'] = $session_data;
     $data['title'] = "Activation";
     $data['username'] = $session_data['username'];
     
     $idx = $session_data['id'];
     $lname = $this->input->post('lname',true);
     $fname = $this->input->post('fname', true);     
     $sex = $this->input->post('gender', true);
     $bday = $this->input->post('bday');
     $bday = date('Y-m-d',strtotime($bday));
     $studno = $this->input->post('studno', true);
     $progid = $this->input->post('program',true);     
   
     $termid = $this->input->post('ayterm', true);
     $regid = $this->input->post('regno', true);
     $orno = $this->input->post('orno', true);
     $ordt = $this->input->post('ordt', true);   
     
	 $isused = $this->mod_activation->InfoInUse($session_data['id'],$studno,1);
	 if($isused == 0)
	 {
      $rs = $this->mod_activation->verify_student($lname, $fname, $bday, $sex[0], $studno, $termid, $progid, $regid, $orno, $ordt );
     
      foreach($rs as $rs)
	  {
      $result = $rs->result;
      }
     }
	 else
	 {
	  $result = -1;
	 }
	 $this->mod_main->Translog($session_data['id'],'StudentActivation Processing',$studno.', '.$bday);
	 
     if ($result > 0){      
      $rs = $this->mod_activation->validate_member($idx, '1', $studno );
	  $this->mod_main->Translog($session_data['id'],'StudentActivation Success',$studno.', '.$bday);
     }
     else
	 {
	  $post_data = $this->input->post();
	  $post_data['pageid']='2';
	  $this->session->set_userdata('post_data', $post_data);
	  $this->mod_main->Translog($session_data['id'],'StudentActivation Failed',$studno.', '.$bday);
	 }
	 
     $data['result'] = $result;
	 $data['recommendation']=(($result==0)?'(Kindly approach the IT administrator or the registrar office to update your personal information)':'');
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_activation_success', $data);
     $this->load->view('include/footer');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
   
  
 }
 
 function searchstudentinfo()
 {
   $studentname = '';
   $degree = '';
   $studentno = StripSlashes($this->input->post('studno'));
   
   $data['student'] = $this->mod_activation->get_StudentInfo($studentno);
   
   //$this->MessageBox($data['student'][0]->StudentName . ' - ' . $data['student'][0]->ProgName);
   
   if (isset($data['student'][0]))
   {
    $studentname =$data['student'][0]->StudentName;
    $degree = $data['student'][0]->ProgName;
   }

   echo $studentname . ':' . $degree;
 }
 
 function verifyparents()
 {
    $hiddenstudentno=StripSlashes($this->input->post('hiddenstudentno'));
    $parentid=StripSlashes($this->input->post('userid'));
    $relationship=StripSlashes($this->input->post('relationship'));
 
    if ($relationship=='')
    {
     $relationship2 = 'Family';
    }
    else
    {
     $relationship2 = $relationship;
    }

    $confirm = $this->mod_activation->icreate($parentid, $hiddenstudentno, $relationship2);
    $this->mod_activation->iactivate($parentid, 2);
	$this->mod_main->Translog($parentid,'ParentActivation Success',$hiddenstudentno.','.$relationship2);
    redirect('home/logout', 'refresh');
 }
 
 function validatestudentinfo(){
  $session_data = $this->session->userdata('logged_in');
  $hiddenstudentno=str_replace("'","",StripSlashes($this->input->post('studno')));
  $birthdate=str_replace("'","",StripSlashes($this->input->post('birth')));
  $termid=str_replace("'","",StripSlashes($this->input->post('term')));
  $regid=str_replace("'","",StripSlashes($this->input->post('registration')));
  $receipt=str_replace("'","",StripSlashes($this->input->post('receiptno')));
  $date=str_replace("'","",StripSlashes($this->input->post('payment')));
  
   $data['ayterm'] = $this->mod_activation->get_ayterms();
         
   if ($termid=='')
   {
    $termid2 = $data['ayterm'][0]->TermID;
   }
   else
   {
    $termid2 = $termid;
   }
  
  $data['validate'] = $this->mod_activation->validate($hiddenstudentno, $birthdate, $termid2, $regid, $receipt, $date);
  $valid = $data['validate'][0]->Validate;
  $this->mod_main->Translog($session_data['id'],'Portal Activation - Validate Student','StudentNo:'.$hiddenstudentno);
  echo $valid;
 }

 function verifyfaculty(){
     $this->load->model('user','',TRUE);
     $this->load->model('mod_crypto','',TRUE);
     

     if($this->session->userdata('logged_in')) {  
        $session_data = $this->session->userdata('logged_in');
        $data['userinfo'] = $session_data;
        $data['title'] = "Activation";
        $data['username'] = $session_data['username'];
        $user = $this->input->post('prismsid',TRUE);
        $pwd = $this->input->post('prismspwd', TRUE);
        $conditions = array(1,3,5,7);
		$cryptopwd = $this->mod_crypto->BitEncrypt($pwd,$conditions);
        $valid = $this->user->isValidprismspwd($user, $cryptopwd);
        $idx = $session_data['id'];

       $isused = $this->mod_activation->InfoInUse($session_data['id'],$user,3);
	   if($isused == 0)
	   {
		if ($valid){      
            $rs = $this->mod_activation->validate_member($idx, '3', $user );
            $data['result'] = '1';
            $this->mod_main->Translog($session_data['id'],'FacultyActivation Success','-');            
        } 
		else
		{
            $data['result'] = '0';            
            $post_data = $this->input->post();
	        $post_data['pageid']='4';
	        $this->session->set_userdata('post_data', $post_data);
            $this->mod_main->Translog($session_data['id'],'FacultyActivation Failed','PRISMID:'.$user.', PWD:'.$pwd);  
        }
       }
	   else
	   {
        $data['result'] = '-1';            
        $post_data = $this->input->post();
	    $post_data['pageid']='4';
	    $this->session->set_userdata('post_data', $post_data);
        $this->mod_main->Translog($session_data['id'],'Faculty Activation is already in use','PRISMID:'.$user.', PWD:'.$pwd);  
       }
        $this->load->view('include/header',$data);
        $this->load->view('templates/mainmenu',$data);
        $this->load->view('vw_activation_success', $data);

     } else {redirect('login', 'refresh');}
 }
 
 function verifyadmin(){
     $this->load->model('user','',TRUE);
     $this->load->model('mod_crypto','',TRUE);
     

     if($this->session->userdata('logged_in')) {  
        $session_data = $this->session->userdata('logged_in');
        $data['userinfo'] = $session_data;
        $data['title'] = "Activation";
        $data['username'] = $session_data['username'];
        $user = $this->input->post('prismsid',TRUE);
        $pwd = $this->input->post('prismspwd', TRUE);
        $conditions = array(1,3,5,7);
		$cryptopwd = $this->mod_crypto->BitEncrypt($pwd,$conditions);
        $valid = $this->user->isValidprismsAdminpwd($user, $cryptopwd);
        $idx = $session_data['id'];

        
	   $isused = $this->mod_activation->InfoInUse($session_data['id'],$user,-1);
	   if($isused == 0)
	   {
		if ($valid)
		{      
            $rs = $this->mod_activation->validate_member($idx, '-1', $user );
            $data['result'] = '1';
            $this->mod_main->Translog($session_data['id'],'AdminActivation Success','-');            
        } 
		else
		{
            $data['result'] = '0';            
            $post_data = $this->input->post();
	        $post_data['pageid']='5';
	        $this->session->set_userdata('post_data', $post_data);
            $this->mod_main->Translog($session_data['id'],'AdminActivation Failed','PRISMID:'.$user.', PWD:'.$pwd);  
        }
       }
	   else
	   {
        $data['result'] = '-1';            
        $post_data = $this->input->post();
	    $post_data['pageid']='5';
	    $this->session->set_userdata('post_data', $post_data);
        $this->mod_main->Translog($session_data['id'],'AdminActivation is already in use','PRISMID:'.$user.', PWD:'.$pwd);  
       }
        $this->load->view('include/header',$data);
        $this->load->view('templates/mainmenu',$data);
        $this->load->view('vw_activation_success', $data);

     } else {redirect('login', 'refresh');}
 }
 
 function trial(){
	 $session_data = $this->session->userdata('logged_in');
	 $userid = $session_data['id'];
	 var_dump($this->mod_activation->get_targettype($userid));
 }
 
}
?>
