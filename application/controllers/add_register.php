<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class add_register extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
   $this->load->model('mod_main','',TRUE);
   $this->load->library('email');
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');   
   $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
   $this->form_validation->set_rules('passwordConfirm', 'Password Confirmation', 'trim|required|matches[password]');
 
   if($this->form_validation->run() == TRUE)
   {
        //Field validation failed.&nbsp; User redirected to login page
        //$this->load->view('register');
	    $data_username = $this->input->post('username');
        $data_email = $this->input->post('email');
        //$data_lastname = $this->input->post('lastname');
        //$data_firstname = $this->input->post('firstname');
        $this->mod_main->TransLog($data_username,'Register Form Validated','-');
		
		$result='|';
        $check = $this->checkusername($data_username);
		if($check=='true' || $check=='invalid')
		{
		 $this->mod_main->TransLog($data_username,'User ReEntered','-');
		 $result=$this->user->get_user($data_username);
		}
		else
		{
		 $result = $this->user->add_user();
		 $this->mod_main->TransLog($data_username,'User Added','-');
        }
        unset($_POST);		 
         
		if ($result <> '' ){
		 //$username = $data_firstname . ' ' . $data_lastname;
         $username = $data_username;
         $email = trim($data_email);         
         $verify = explode('|', $result);         
         $today = new DateTime('NOW');
         date_add($today,date_interval_create_from_date_string("15 days"));
         $data['username'] = $username;
         $data['email'] = $email;
         $data['title'] = "PRISMS Online:Registration";
		 
         $data['verify'] = site_url('register/verify').'/' .$verify[0]; 
         $data['today'] = $verify[1] ; //$today->format( 'Y-m-d H:i:s' );
         $data['msg'] = "<h3>Hi, ".$data['username']."</h3>
						 <p class='lead'>We would like to thank you for registering your email account to PRISMS Portal.</p>
						 <p>You've entered ".mailto($email,$email)." as the contact email address for your PRISMS Portal. To complete the process, we just need to verify that this email address belongs to you.
						 Simply click the link below and sign using your PRISMS Portal Username and password.
						 </p>
						 <!-- Callout Panel -->
						 <p class='callout'>
							<a href='".$data['verify']."'>Verify Now! &raquo;</a> <i> This validation code is valid until ".$data['today']."</i>
						 </p><!-- /Callout Panel -->";
         //$message = $this->load->view('templates/reg_mail', $data, TRUE);
         $msg = $this->load->view('mail/mail_template', $data, TRUE);
		 $this->session->set_userdata('registerinfo',array('username'=>$username,'email'=>$email));
		 if(EMAIL_NOTIFY==1){
		  if(!$this->mod_main->xsendmail('',$email,$msg,'Action Required: Please confirm your PRISMS Portal account',$data,'mail/mail_template'))
		  {
		   $this->mod_main->TransLog($data_username,'Email Failed','email:'.$email);
		   $this->session->set_userdata('error',array('email' => 'failed'));
		  }
		 }
		 //echo $this->email->print_debugger();
		 $this->mod_main->TransLog($data_username,'Registration Success','-');
         redirect('register/success', 'refresh'); 
        }
		$this->mod_main->TransLog($data_username,'Registration Failed','-');   
        redirect('register', 'refresh');
   }
   else
   {
     //Go to private area
	 $this->mod_main->TransLog('-','Form Validation Failed','-');   
     redirect('register', 'refresh');
   }

 }

 function check_database($password)
 {
   //Field validation succeeded.&nbsp; Validate against database
   $username = $this->input->post('username');
   
   //query the database
   $result = $this->user->login($username, $password);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->userid,
         'username' => $row->namealias
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 }
 
  function checkusername($username='')
  {
   if($username=='')
   {return 'invalid';} 
   
   $row = $this->user->getInfo($username);
   if($row || $row==1 || $row==true)
   {return 'true';} 
   else
   {return 'false';}
  }
  
}
?>
