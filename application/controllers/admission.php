<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Admission extends CI_Controller {

 function __construct(){
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_admission','',TRUE);
   $this->load->model('user','',TRUE);
 }

 function index(){
    if($this->session->userdata('logged_in')) 
	{
     $session_data = $this->session->userdata('logged_in');
     //$this->mod_main->monitoring_action($session_data,'admission');
	 
     $data['userinfo'] = $session_data;     
     $data['title'] = "Admission";
     $data['username'] = $session_data['username'];
     
     $data['app'] = $this->mod_admission->get_application($session_data['id']);
 
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_application', $data);     
     $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                                'plugin/fuelux/wizard/wizard.js',
                                'plugin/jquery-form/jquery-form.min.js',
                                'utilities/admission.js');
								
     $this->load->view('include/footer',$mfooter);
	 $this->mod_main->Translog($session_data['id'],'View Admission','-');
    } 
	else 
	{
     redirect('login', 'refresh');
    }
 }

 function applicant(){
     $session_data = $this->session->userdata('logged_in');
     $this->load->helper(array("form"));
     
	 $data['title']       = "Admission";
     $data['username']    = 'new_applicant';
     $data['rscampus']    = $this->mod_main->get_campuslist();
     $data['rschoice1']   = $this->mod_main->get_courselist(1,50,1000,0,0);
     $data['rschoice2']   = $this->mod_main->get_courselist(1,50,1000,0,0);
     $data['rschoice3']   = $this->mod_main->get_courselist(1,50,1000,0,0);
     $data['rsapptypes']  = $this->mod_main->get_apptypes(0);
     $data['rsayterm']    = $this->mod_main->get_ayterm();
     $data['rsreligion']  = $this->mod_main->get_religions();
     $data['rscs']        = $this->mod_main->get_civilstatus();
     $data['rscs1']       = $this->mod_main->get_civilstatus();
     $data['rsnat']       = $this->mod_main->get_nationality();
     $data['rsprov']     = $this->db->query("SELECT ProvinceID,ProvinceName FROM ES_Address_Province")->result();
     $data['rscit']     = $this->db->query("SELECT CityID,CityName,ProvinceID FROM ES_Address_Cities")->result();
     if($session_data){
	    $data['userinfo'] = $session_data;	 
		$data['username'] = $data['userinfo']['id'];	 
	 }
	 
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_admission', $data);

     $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                           'plugin/fuelux/wizard/wizard.js',
                           'plugin/jquery-form/jquery-form.min.js',
                           'plugin/x-editable/moment.min.js',
                           'plugin/x-editable/x-editable.min.js',
                           'plugin/masked-input/jquery.maskedinput.min.js',
                           'utilities/admission.js'
                           );
     $this->load->view('include/footer',$mfooter);
     $this->mod_main->Translog('New Applicant','Create New Application','-');
 }

 function welcome(){
        if(!$this->session->userdata('logged_in')){ redirect('login', 'refresh'); } 
        $session_data = $this->session->userdata('logged_in');
        $this->load->helper(array("form"));
        $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                              'plugin/fuelux/wizard/wizard.js',
                              'plugin/jquery-form/jquery-form.min.js',
                              'jquery.cookies.js',
                               'utilities/admission.js'
                              );

        $this->load->view('include/header');
        $this->load->view('templates/mainmenu');
        $this->load->view('vw_welcome');
        $this->load->view('include/footer',$mfooter );
   	    $this->mod_main->Translog('Applicant','Welcome PAGE','-');
 }
 
 function summary(){
   $this->load->helper(array("form"));

    $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                          'plugin/fuelux/wizard/wizard.js',
                          'plugin/jquery-form/jquery-form.min.js',
                          'jquery.cookies.js',
                          // 'utilities/admission.js'
                          );

    $this->load->view('include/header');
    $this->load->view('templates/mainmenu');
    $this->load->view('vw_summary');
    $this->load->view('include/footer',$mfooter );
    $this->mod_main->Translog('Applicant','Welcome PAGE','-');
 }

 function overview(){

   $data['summary'] = $this->input->post();
   $data['rscampus'] = $this->mod_main->get_campuslist();
   $data['rschoice1'] = $this->mod_main->get_courselist(1,50,1000,0,0);
   $data['rsapptypes'] = $this->mod_main->get_apptypes(0);
   $data['rsayterm'] = $this->mod_main->get_ayterm();
   $data['rsreligion'] = $this->mod_main->get_religions();
   $data['rscs'] = $this->mod_main->get_civilstatus();
   $data['rscs1'] = $this->mod_main->get_civilstatus();
   $data['rsnat'] = $this->mod_main->get_nationality();
   $content = $this->load->view('vw_summary', $data, true);

  echo json_encode($content);
 }

 function application(){
    if($this->session->userdata('logged_in')) {
     $this->load->helper(array("form"));				
     $session_data = $this->session->userdata('logged_in');     
     if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 
	 $data['userinfo'] = $session_data;
     
     $data['title'] = "Admission";
     $data['username'] = $session_data['username'];
     $data['rscampus'] = $this->mod_main->get_campuslist();
     $data['rschoice1'] = $this->mod_main->get_courselist();
     $data['rschoice2'] = $this->mod_main->get_courselist();
     $data['rschoice3'] = $this->mod_main->get_courselist();
     $data['rsapptypes'] = $this->mod_main->get_apptypes(0);
     $data['rsayterm'] = $this->mod_main->get_ayterm();
     $data['rsreligion'] = $this->mod_main->get_religions();
     $data['rscs'] = $this->mod_main->get_civilstatus();
     $data['rsnat'] = $this->mod_main->get_nationality();
     
     //$data['userinfo2'] = $this->mod_main->getUserInfo($session_data['id']);
     
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_admission', $data);
     
     $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                           'plugin/fuelux/wizard/wizard.js',
                           'plugin/jquery-form/jquery-form.min.js',
                           'utilities/admission.js'                           
                           );
     $this->load->view('include/footer',$mfooter);
     $this->mod_main->Translog($session_data['id'],'Create New Application','-');
   }
   else
   {
      //If no session, redirect to login page
      redirect('login', 'refresh');
   }
 }
 
 function txn($mode,$type,$args=false){
    $result = array('result'=>false,'content'=>'');
    $p      = (($this->input->post())?($this->input->post()):array());
	if($this->session->userdata('logged_in')){
		  $session_data = $this->session->userdata('logged_in');
		  switch($mode)
		  {
			case 'get':
			 switch($type)
			 {
			    case 'applist':
				    $opt     = $p['opt'];
					$termid  = $p['ayterm'];
					$campus  = $p['campus'];
					$apptype = $p['apptype'];
					$param   = $p['param'];
				    $exec    = $this->mod_admission->manage_applicants($opt,$termid,$campus,$apptype,$param);
					echo $this->load->view('admission/tdata',array('tdata'=>(($exec)?$exec:false)),true);
					die();
                break;
                case 'info':
				    $result['result']  = true; 
                break;				
                case 'upload':
				    $result['result']  = true; 
                break;				
                case 'save':
				    $result['result']  = true; 
                break;				
			 }
			break;
		  }
    }
    echo json_encode($result);	
 }
 
 
 function submitted(){
    $data['name']  = $this->input->post('Name');
    $data['appno'] = $this->input->post('AppNo');
	$data['appdt'] = $this->db->query("SELECT TOP 1 * FROM ES_Admission WHERE AppNo='".$data['appno']."'")->result();
	$data['sent']  = false;
	$data['code']  = str_replace('==','',base64_encode($data['appno']));
	$data['msg']   = "<h3>Hi, ".$data['name'].":</h3>
				      <p class='lead'>We would like to thank you for your application in SBU Admission. Your application number is <b>".$data['appno']."</b></p>
				      <p>You can print your Application Form using this <a href='".base_url('admission/printapp/'.$data['code'])."/1'>link</a>.</p>";
	$msg  = $this->load->view('mail/mail_template', $data, TRUE);
	$info = $this->db->query("SELECT TOP 1 AppNo,Email FROM ES_Admission WHERE AppNo='".$data['appno']."'")->result();
	if($info && count($info)>0){
		$email = (($info[0]->Email!='')?($info[0]->Email):'jhe69samson@princetech.com.ph');
		if(EMAIL_NOTIFY==1){
		 $data['sent'] = $this->mod_main->xsendmail('',$email,$msg,'Application Form',$data,'mail/mail_template');
		}
	}
	$content = $this->load->view('vw_successful', $data, true);
	echo json_encode($content);
 }

 
 function getapptype(){
  $isshs         =   $this->input->post('isshs');
  $campus     =   $this->input->post('campus');
  $progclass   =  (($isshs==1 && $this->input->post('progclass')==false)?'29':$this->input->post('progclass'));
   
  $apptype = $this->mod_main->get_customapptype($isshs,$campus,$progclass);
  $data = array(
      'error' => false,
      'data' => $apptype,
      'message' => 'Successfull!',
  );

  echo json_encode($data);
}

 function getcourse(){
  $isshs =   $this->input->post('isshs');
  $campus =   $this->input->post('campus');
  $choice1 = $this->mod_main->get_courselist($campus,50,1000,0,$isshs);
  $data = array(
      'error' => false,
      'data' => $choice1,
      'message' => 'Successfull!',
  );

  echo json_encode($data);
 }

 
 function emailvalidation(){

   $email = ($this->input->post("Email"));
   $termid = ($this->input->post("termid"));
   $result=  $this->db->query("SELECT * FROM ES_Admission WHERE Email = '$email' AND TermID = '$termid'");
   $num = $result->num_rows();
//   echo $num;
   if($num == 0){
       echo "true";
   } else {
       echo "false";
   }
  // echo "false";
    //print_r($this->input->post("termid"));
    //print_r($this->input->post());
 }
 function mobilevalidation(){

   $email = ($this->input->post("MobileNo"));
   $termid = ($this->input->post("termid"));
   $result=  $this->db->query("SELECT * FROM ES_Admission WHERE MobileNo = '$email' AND TermID = '$termid'");
   $num = $result->num_rows();
//   echo $num;
   if($num == 0){
       echo "true";
   } else {
       echo "false";
   }
  // echo "false";
    //print_r($this->input->post("termid"));
    //print_r($this->input->post());
 }

 function namevalidation(){
     //print_r($this->input->post());
   $firstname = ($this->input->post('FirstName'));
   $lastname = ($this->input->post('LastName'));
    $termid = ($this->input->post("termid"));
   $bday = ($this->input->post('DateOfBirth'));
   $result=  $this->db->query("SELECT * FROM ES_Admission WHERE FirstName = '$firstname'  AND LastName = '$lastname' AND DateOfBirth = '$bday' AND TermID = '$termid'");
   $num = $result->num_rows();
//   echo $num;
   if($num == 0){
       echo "true";
   } else {
       echo "false";
   }

 }

 function thankyou(){
    $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                          'plugin/fuelux/wizard/wizard.js',
                          'plugin/jquery-form/jquery-form.min.js',
                          'jquery.cookies.js',

                          );

    $this->load->view('include/header');
    $this->load->view('templates/mainmenu');
    $this->load->view('vw_successful');
    $this->load->view('include/footer',$mfooter );
    $this->mod_main->Translog('Applicant','View Portal Activation','-');
 }

 function admission_officer(){
    if($this->session->userdata('logged_in')) {
     $this->load->helper(array("form"));				
     $session_data = $this->session->userdata('logged_in');     
     if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 
	 $data['userinfo']   = $session_data;
     
     $data['title']      = "Admission Applicants";
     $data['username']   = $session_data['username'];
     
	 $data['rscampus']   = $this->mod_main->get_campuslist();
     $data['rsayterm']   = $this->mod_main->get_ayterm(6);
     $data['rsapptypes'] = $this->mod_main->get_apptypes(0);
	 $data['tdata']      = false;
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('admission/vw_profiler', $data);
     
     $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                           'plugin/fuelux/wizard/wizard.js',
                           'plugin/jquery-form/jquery-form.min.js',
                           'utilities/applicants.js'                           
                           );
     $this->load->view('include/footer',$mfooter);
     $this->mod_main->Translog($session_data['id'],'View Applicant List','-');
   }
   else
   {
      redirect('login', 'refresh');
   }
 }
 
 function admission_list(){
    if($this->session->userdata('logged_in')) {
     $this->load->helper(array("form"));				
     $session_data = $this->session->userdata('logged_in');     
     if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 
	 $data['userinfo']   = $session_data;
     
     $data['title']      = "Admission Applicants";
     $data['username']   = $session_data['username'];
     
	 $data['rscampus']   = $this->mod_main->get_campuslist();
     $data['rsayterm']   = $this->mod_main->get_ayterm(6);
     $data['rsapptypes'] = $this->mod_main->get_apptypes(0);
	 $data['tdata']      = false;
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('admission/vw_applicant', $data);
     
     $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                           'plugin/fuelux/wizard/wizard.js',
                           'plugin/jquery-form/jquery-form.min.js',
                           'utilities/applicants.js'                           
                           );
     $this->load->view('include/footer',$mfooter);
     $this->mod_main->Translog($session_data['id'],'View Applicant List','-');
   }
   else
   {
      redirect('login', 'refresh');
   }
 }
 
 function examschedule(){
	 echo "Banana oh nana!";
 }
 
 function medicalschedule(){
	 echo "Banana oh nana!";
 }
 
 function submit(){
   $app_no = $this->mod_admission->appno();
   $data = array();

   $data = $this->input->post();
   if($data['department']!='college' && $data['department']!='graduate'){
      $data['TermID'] = TERM_SHS;	   
      if(!array_key_exists('Choice1_Course',$data)){
         $data['Choice1_Course'] = 0;
      }	 
   }else{
	  $semestral = $this->db->query("SELECT TOP 1 Semestral FROM ES_Programs WHERE ProgID='".$data['Choice1_Course']."'")->row()->Semestral;
	  if($semestral>1){
         $data['TermID'] = TERM_GRD;	   
	  }else{
         $data['TermID'] = TERM_CAS;	   
	  }
   }
  
    
    $fname  = $data['FirstName'];
    $lname  = $data['LastName'];
    foreach($data as $k=>$v){
       if(!strtotime($v)){
         $data[$k] = $this->user->clean_input($v);
         if($k=='MiddleName' && strlen($v)>1){
           $data['MiddleInitial'] = substr($v,0,1);
         }
		 
       }
    }

   $data['AppNo']    = key($app_no[0]);
   $data['AppDate'] = date("Y-m-d H:i:s");
   
   $result = $this->mod_admission->create($data);
   if ($result){
	 if(is_object($result) && @property_exists($result,'AppNo')){
		$data['AppNo'] = $result->AppNo; 
	 }  
	 $this->mod_main->Translog($data['AppNo'],'Success Admission','Application Type:'.$this->input->post('ApplyTypeID', true));
     $results = array(
        'Name'  => $lname . ', ' . $fname,
        'AppNo' => $data['AppNo'],
     );
     echo json_encode($results);
   }else{
     $this->mod_main->Translog($session_data['id'],'Failed Admission','-');
     echo json_encode('Failed!');
   }
 }
 
 function submit_old(){  
   $this->load->model('mod_admission','',TRUE);
   $session_data = $this->session->userdata('logged_in');
   $idx = $session_data['id'];
   $lname = $this->mod_main->sanitizer($this->input->post('lname',true));
   $fname = $this->mod_main->sanitizer($this->input->post('fname', true));
   $mname = $this->mod_main->sanitizer($this->input->post('mname',true));
   $ext = $this->mod_main->sanitizer($this->input->post('ext', true));
   $sex = $this->mod_main->sanitizer($this->input->post('gender', true));
   $bday = $this->mod_main->sanitizer($this->input->post('birthdate'));
   $bloc = $this->mod_main->sanitizer($this->input->post('birthplace', true));
   $csid = $this->mod_main->sanitizer($this->input->post('civilstatus',true));
   $natid = $this->mod_main->sanitizer($this->input->post('nationality', true));
   $relid = $this->mod_main->sanitizer($this->input->post('religion',true));
   $telno = $this->mod_main->sanitizer($this->input->post('telephone', true));
   $mobile = $this->mod_main->sanitizer($this->input->post('mobile', true));
   
   $termid = $this->input->post('ayterm', true);
   $campusid = $this->input->post('campus', true);
   $apptype = $this->input->post('apptype', true);
   $ch1 = $this->input->post('choice1', true);
   $ch2 = $this->input->post('choice2', true);
   $ch3 = $this->input->post('choice3', true);
   if($ch3 == '')
   {$ch3 = 0;}
   $ch4 = 0;
   
   $chm1 =0;
   $chm2 =0;
   $chm3 =0;
   $chm4 =0;
   
   $result = $this->mod_admission->save_userprofile($idx,$lname, $fname, $mname, $ext,$sex,$bday,$bloc, $csid, $natid , $relid, $telno, $mobile );
   $result = $this->mod_admission->save_application($idx, $termid, $campusid, $apptype, $ch1, $chm1, $ch2, $chm2, $ch3, $chm3, $ch4, $chm4);
   $this->mod_main->Translog($session_data['id'],'Success Admission','Application Type:'.$apptype);
   if ($result) 
   {
    echo json_encode( $lname . ', ' . $fname  );
    //echo 'success'
   } 
   else 
   { 
   $this->mod_main->Translog($session_data['id'],'Failed Admission','-');
   echo json_encode('Failed!'); 
   }
 }
 
function printout($appno){
 //crystal report version
  $this->load->model('mod_crystalreports','',TRUE);
  if($this->session->userdata('logged_in')) {
	 $session_data = $this->session->userdata('logged_in');
	 list($file,$filename)= $this->mod_crystalreports->prepare_application($appno);
	 if(($file!='none' && $filename!='none') && ($file!='none1' && $filename!='none1'))
	 {
	   header('Content-type: application/pdf');
       header('Content-Disposition: inline; filename="' . $filename . '"');
       header('Content-Transfer-Encoding: binary');
       header('Accept-Ranges: bytes');
       @readfile($file);
	   unlink($file);
	   $this->mod_main->Translog($session_data['id'],'Print Application Form','-');
     }else{ 
	   redirect('admission', 'refresh'); 
	 }	
  }else{ 
     redirect('login', 'refresh'); 
  }
}

function printapp($appno){
 //crystal report version
  $this->load->model('mod_crystalreports','',TRUE);
  $appno = ((base64_encode(base64_decode($appno))==$appno)?base64_decode($appno):$appno);
  if($this->session->userdata('logged_in')) {
	 $session_data = $this->session->userdata('logged_in');
         $appno            = strip_tags($appno);
       //$appno            = $this->user->clean_input($appno);
       //var_dump($appno);
       //die();
	 list($file,$filename)= $this->mod_crystalreports->prepare_applicationb($appno);
	 if(($file!='none' && $filename!='none') && ($file!='none1' && $filename!='none1'))
	 {
	   header('Content-type: application/pdf');
       header('Content-Disposition: inline; filename="' . $filename . '"');
       header('Content-Transfer-Encoding: binary');
       header('Accept-Ranges: bytes');
       @readfile($file);
	   unlink($file);
	   $this->mod_main->Translog($session_data['id'],'Print Application Form','-');
     }else{ 
	   redirect('admission', 'refresh'); 
	 }	
  }else{ 
     redirect('login', 'refresh'); 
  }
}


function emailapp($appno){
 //crystal report version
  $this->load->model('mod_crystalreports','',TRUE);
  if($this->session->userdata('logged_in')) {
	 $session_data = $this->session->userdata('logged_in');
         $appno            = strip_tags($appno);
       //$appno            = $this->user->clean_input($appno);
       //var_dump($appno);
       //die();
	 list($file,$filename)= $this->mod_crystalreports->prepare_applicationb($appno);
	 if(($file!='none' && $filename!='none') && ($file!='none1' && $filename!='none1'))
	 {
	   header('Content-type: application/pdf');
       header('Content-Disposition: inline; filename="' . $filename . '"');
       header('Content-Transfer-Encoding: binary');
       header('Accept-Ranges: bytes');
       @readfile($file);
	   unlink($file);
	   $this->mod_main->Translog($session_data['id'],'Print Application Form','-');
     }else{ 
	   redirect('admission', 'refresh'); 
	 }	
  }else{ 
     redirect('login', 'refresh'); 
  }
}

function trial(){
	
}

 
} //end of class
?>

