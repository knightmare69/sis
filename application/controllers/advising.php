<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Advising extends CI_Controller{

 function __construct(){
   parent::__construct();
   
   date_default_timezone_set("Asia/Taipei");
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_advising','',TRUE);
   $this->load->model('mod_evaluation','',TRUE);
   $this->load->model('mod_advising_sched','',TRUE);
   $this->load->model('mod_crystalreports','',TRUE);
   $this->load->helper(array("form"));
   $this->AdvSchedCtrl    = 1;
   $this->AdvSchedStyle  = $this->mod_main->getconfigvalue('AdvisingSchedSelection');   
   $this->EnableAdv         = $this->mod_main->getconfigvalue('AdvisingModuleLock');    
   $this->RegOnly            = $this->mod_main->getconfigvalue('AdvRegularOnly'); 
   $this->jstimestamp   = '20171122';   
   $this->IsAdvance     = array('2014300456','2015300340','2014300288','2014300087','2015300414','2015300435','2015300084');
   $this->EnrollStart   = date('Y-m-d',strtotime('2017-11-20'));
   ini_set('memory_limit','128M');   
 }
 
 function index(){
   if($this->session->userdata('logged_in')) {
	 $session_data = $this->session->userdata('logged_in');
         $this->mod_main->monitoring_action($session_data);
	 
	 $data['title'] = "Advising";
	 $data['userinfo'] = $session_data;
	 $data['idtype'] = $session_data['idtype'];
	 $data['username'] = $session_data['username'];
	 
	 $data['megalock'] = ((isset($this->EnableAdv) && $data['idtype']!='-1')? $this->EnableAdv:'1');
	 
	 if($session_data['idtype']==1){
      $data['studentno'] = $session_data['idno'];
      $reeval = $this->mod_evaluation->reevaluate_student($data['studentno']);
     }elseif($session_data['idtype']=='-1'){
	  $this->EnableAdv=1;
	  $data['studentnolist']=true; 
	  $data['studentfilter']=true; 
	  $data['studentno'] = '';
	 }elseif($session_data['idtype']==2){
	  $username = $session_data['id'];
      $data['studentnolist'] = $this->mod_main->ListofParentChildwEval($username);
	  
	  if(count($data['studentnolist'])<=0)
		redirect('profile','refresh');
	  else{	  
	    $data['studentno'] = ((count($data['studentnolist'])>0)?($data['studentnolist'][0]->StudentNo):'');
	    $data['xdetail']   =  $this->mod_main->call_xtraDetails($data['studentno']);
	  } 
	 }
	 else
	  redirect('profile','refresh');
	 
	 $row = $this->mod_advising->call_SPconfig($data['studentno']);
	 $xactiveinfo = $this->mod_advising->generate_controller($row);
	 $xadvschedctrl = $this->mod_advising->checkadvschedctrl($xactiveinfo['activetermid'],$xactiveinfo['progid'],$xactiveinfo['majorid'],$xactiveinfo['yrlvlid']);
	 $xactiveinfo['regonly'] = $this->RegOnly;
	 
	 if($this->EnableAdv==0 && $data['idtype']!='-1')
	 {
	  $xactiveinfo['btnstatus'] ='';
      $xactiveinfo['xadv'] = 0;
      $xactiveinfo['xenr'] = 0;
	 }	 
	 
	 if($this->RegOnly==1 && $xactiveinfo['isreg']==0)
	 {
	  $xactiveinfo['btnstatus'] ='';
      $xactiveinfo['xadv'] = 0;
      $xactiveinfo['xenr'] = 0;
	 }
	 
	 if($this->AdvSchedCtrl==1 && $session_data['idtype']==1 && $xadvschedctrl['inperiod']==0 && ($xactiveinfo['regid']=="" || $xactiveinfo['xprt']==0 && $xactiveinfo['xcor']==0)){
      $xactiveinfo['xadv']      = 0;
      $xactiveinfo['xenr']      = (($xactiveinfo['regid']=="")?0:$xactiveinfo['xenr']);
	  $xactiveinfo['btnstatus'] = (($xactiveinfo['regid']=="")?0:$xactiveinfo['btnstatus']);
	  if($xadvschedctrl['dis_datestart']!='' && $xadvschedctrl['dis_dateend']!=''){
		$advperiod = 'From <strong>'.$xadvschedctrl['dis_datestart'].'</strong> To <strong>'.$xadvschedctrl['dis_dateend'].'</strong>';
		$enrperiod = (($xactiveinfo['onenperiod']==1)? preg_replace("/\From .*? To/","From ".$xadvschedctrl['dis_datestart']." To",$xactiveinfo['enperiod']) : $xactiveinfo['enperiod']);
        $enrperiod = str_replace('(Closed)','',$enrperiod);
		$xactiveinfo['advperiod'] = (($xadvschedctrl['inperiod']==0)?'<b class="txt-color-red">'.$advperiod.' (Closed)</b>':$advperiod);	
		$xactiveinfo['enperiod']  = (($xadvschedctrl['inperiod']==0 && $xactiveinfo['onenperiod']==0)?'<b class="txt-color-red">'.$enrperiod.' 11:59 PM (Closed)</b>':$enrperiod.' 11:59 PM');	
	  }
	 }else if($xadvschedctrl['inperiod']=='-1'){
		$xactiveinfo['advperiod']   = '<b class="txt-color-red">'.$xactiveinfo['advperiod'].' (Closed For Online)</b>';
		$xactiveinfo['enperiod']    = '<b class="txt-color-red">'.$xactiveinfo['enperiod'].' (Closed For Online)</b>';
		$xactiveinfo['onadvperiod'] = 0;
	    $xactiveinfo['onenperiod']  = 0;
		$xactiveinfo['xadv']        = 0;
	    $xactiveinfo['xenr']        = 0;
		$xactiveinfo['xprt']        = 0;
	    $xactiveinfo['xcor']        = 0;
	    $xactiveinfo['btnstatus']   = '';
	 }

	 $data['advising_data'] = $xactiveinfo;
	 $data['advschedctrl']  = $xadvschedctrl;
	 $data['xalert']= $this->mod_advising->generate_alert($xactiveinfo['onadvperiod']
	                                                     ,$xactiveinfo['onenperiod']
														 ,$xactiveinfo['balance']
														 ,$xactiveinfo['withbalance']
														 ,$xactiveinfo['accounts']
														 ,$xactiveinfo['withaccount']
														 ,$xactiveinfo['regonly']
														 ,$xactiveinfo['inactive']);
	 
	 unset($xactiveinfo['advperiod'],$xactiveinfo['enrperiod'],$xactiveinfo['btnstatus'],$xactiveinfo['status'],$xactiveinfo['pstatus']);
	 if($xactiveinfo['activetermid']!=0 && $xactiveinfo['studentno']!=''){$this->session->set_userdata('enrollinfo',$xactiveinfo); }
	 $footer['jslink'] = array('utilities/alertmsg.js?'.$this->jstimestamp,'utilities/advising.js?'.$this->jstimestamp);
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_advisingcontrol', $data);
     $this->load->view('include/footer',$footer);
	 $this->mod_main->Translog($session_data['id'],'View Advising Controller',$session_data['username']);
	 if($xactiveinfo['studentno']!=''){
       $this->mod_main->Translog($session_data['id'],'Initial Advising Data','STDNO:'.$xactiveinfo['studentno'].',PROGID:'.$xactiveinfo['progid'].',CURR:'.$xactiveinfo['curriculumid'].',YRLVL:'.$xactiveinfo['yrlvlid']); 
     }
   }
   else
   {
     redirect('login', 'refresh');
   }
 }
 
 function txn($mode='',$type='',$args='')
 {
  $result = array('result'=>false,'content'=>'');
  $p = (($this->input->post())?($this->input->post()):array());
  if($this->session->userdata('logged_in')){
   $session_data = $this->session->userdata('logged_in');
   $enroll_data = (($this->session->userdata('enrollinfo'))?($this->session->userdata('enrollinfo')):false);
   
   switch($mode)
   {
	case 'get':
     switch($type)
     {
	  case 'studentinfo':
	   $data['megalock']  = ((isset($this->EnableAdv) && $session_data['idtype']!='-1')? $this->EnableAdv:'1');
	   $data['studentno'] = $args;  
	   $reeval = $this->mod_evaluation->reevaluate_student($data['studentno']);
	   $row = $this->mod_advising->call_SPconfig($data['studentno']);
	   if($row && $args!='')
	   {
		$xactiveinfo = $this->mod_advising->generate_controller($row);
		$xactiveinfo['regonly'] = $this->RegOnly;
	    
        if($this->RegOnly==1 && $xactiveinfo['isreg']==0)
	    {
		 $xactiveinfo['btnstatus'] ='';
		 $xactiveinfo['xadv'] = 0;
		 $xactiveinfo['xenr'] = 0;
	    }
	    
	    $data['advising_data'] = $xactiveinfo;
	    $data['xalert']= $this->mod_advising->generate_alert($xactiveinfo['onadvperiod'],$xactiveinfo['onenperiod'],$xactiveinfo['balance'],$xactiveinfo['withbalance'],$xactiveinfo['accounts'],$xactiveinfo['withaccount'],$xactiveinfo['regonly'],$xactiveinfo['inactive']);
	    unset($xactiveinfo['advperiod']
		     ,$xactiveinfo['enrperiod']
			 ,$xactiveinfo['btnstatus']
			 ,$xactiveinfo['status']
			 ,$xactiveinfo['pstatus']);
		
	    if($xactiveinfo['activetermid']!=0){$this->session->set_userdata('enrollinfo',$xactiveinfo); }
		$result['comment']=($this->session->userdata('enrollinfo')['studentno']);
	    $result['result']=true;
	    $result['content']=$data;
		$this->mod_main->Translog($session_data['id'],'Get Advising Data','StudentNo:'.$data['studentno']);
		if($xactiveinfo['studentno']!=''){$this->mod_main->Translog($session_data['id'],'Initial Advising Data','STDNO:'.$xactiveinfo['studentno'].',PROGID:'.$xactiveinfo['progid'].',CURR:'.$xactiveinfo['curriculumid'].',YRLVL:'.$xactiveinfo['yrlvlid']); }
	   }
	  break;
	  case 're-evaluation':
	  case 'evaluation':
	   $data['studentno'] = (($session_data['idtype']==1)?($session_data['idno']):($enroll_data['studentno']));
	   if($data['studentno']!='')
	   {
		$row = false;
		$reeval = $this->mod_evaluation->studAcadEvaluation($data['studentno']);
		$this->db->reconnect();
		$row = $this->mod_evaluation->fetch_row($data['studentno']);
		if($row)
		{
		 list($data['evaluation'], $data['summary']) = $this->mod_evaluation->generate_table($row);
		 $this->mod_evaluation->save_EvaltoText($data['studentno'],json_encode(array('content'=>$data['evaluation'],'summary'=>$data['summary'])));  
	     $result['result']=true;
	     $result['content']=$data;
		}
	   }
      break;
      case 'advisedsubj':
       $data['studentno'] = (($session_data['idtype']==1)?($session_data['idno']):($enroll_data['studentno']));
	   if($data['studentno']!='')
	   {
	    $row = $this->mod_advising->call_SPadv($data['studentno']);
        if($row){
		 list($data['xdetail'],$data['xtable']) = $this->mod_advising->generate_advising($row);
	     if($enroll_data['onenperiod']==0)
         {
		  $enroll_data['minunit'] = $data['xdetail']['min'];
	      $enroll_data['maxunit'] = $data['xdetail']['max'];	 
		  $this->session->set_userdata('enrollinfo',$enroll_data);
		 }
		 else
		 {
		  $data['xdetail']['min'] = $enroll_data['minunit'];
	      $data['xdetail']['max'] = $enroll_data['maxunit'];	 
		 }	 
		 
		 if($enroll_data['studentno']!=''){$this->mod_main->Translog($session_data['id'],'Advising Data','STDNO:'.$enroll_data['studentno'].',Min:'.$enroll_data['minunit'].',Max:'.$enroll_data['maxunit']); }
		 $result['result']=true;
	     $result['content']=$data;	
		}		
	   }	
      break;
      case 're-process':
	   log_message('error','wala pa');
	  break;
	  case 'forschedsubj':
	   if($args!=''){
	    $data['advisedid'] = $args;
	    $row = $this->mod_advising_sched->getadvisedsubj($data['advisedid']);
	    if($row){
	     $data['row'] = $this->mod_advising_sched->generate_table($row,$enroll_data['onenperiod'],0);
         $result['result']=true;
	     $result['content']=$data;
         if($enroll_data['studentno']!=''){$this->mod_main->Translog($session_data['id'],'Get Advised Subjects','STDNO:'.$enroll_data['studentno'].',ADVID:'.$data['advisedid']); }		 
	    }
	   }	
      break; 	  
	 }	 
	break;
	case 'set':   
	 switch($type)
	 {
	  case 'advise':
	   $advisedid=0; $regid=0; $studno=0; 
       $advsubj=0;$advunit=0;
       $adviserid='student';
       $advisedate=date('Y-m-d H:i:s');
  
       if((isset($this->EnableAdv) and $this->EnableAdv==0) && $session_data['idtype']!='-1'){$result['content']='advising'; $result['error']='Advising is locked!'; break; } 
	   if(!$this->session->userdata('enrollinfo')){$result['content']='advising'; $result['error']='Data unable!'; break; }
       if(!$this->input->post()){$result['content']='advising'; $result['error']='No Post Data!'; break; }
	   
	   $studno     = (($session_data['idtype']==1)?$session_data['idno']:$enroll_data['studentno']);
       $termid     = (array_key_exists('aytermid',$p)? str_replace("'","",$p['aytermid']):0);
       $collegeid  = (array_key_exists('collegeid',$p)? str_replace("'","",$p['collegeid']):0);
       $campusid   = (array_key_exists('campusid',$p)? str_replace("'","",$p['campusid']) :0);
       $progid     = $enroll_data['progid'];  //(array_key_exists('progid',$p)? str_replace("'","",$p['progid']):0);
       $feeid      = $enroll_data['feesid'];  //(array_key_exists('feeid',$p)? str_replace("'","",$p['feeid']):0);
       $yearlvl    = $enroll_data['yrlvlid']; //(array_key_exists('yrlvl',$p)? str_replace("'","",$p['yrlvl']) : 0);
       $min        = $enroll_data['minunit']; //(array_key_exists('min',$p)? str_replace("'","",$p['min']) : 0);
       $max        = $enroll_data['maxunit']; //(array_key_exists('max',$p)? str_replace("'","",$p['max']) : 0);
       $advsubj    = (array_key_exists('totalsubj',$p)? str_replace("'","",$p['totalsubj']) : 0);
       $advunit    = (array_key_exists('totalcunit',$p)? str_replace("'","",$p['totalcunit']) : 0);
       
	   if($min==''){$min  = 0;}
       if($max==''){$max  = 0;}
        
       $query="EXEC sis_SaveAdvising @TermID=".$termid.",
									 @CollegeID=".$collegeid.",
									 @CampusID=".$campusid.",
									 @ProgID=".$progid.",
									 @StudentNo='".$studno."',
									 @YearLevel=".$yearlvl.",
									 @MinLoad=".$min.",
									 @MaxLoad=".$max.",
									 @AdvisedSubject=".$advsubj.",
									 @AdvisedUnits=".$advunit.",
									 @FeesID=".$feeid.",
									 @AdviserID='".$adviserid."',
									 @DateAdvised='".$advisedate."',
									 @ModifiedBy='',
									 @DateModified=Null,
									 @AccessCode='',
									 @Accountabilities='',
									 @OutBalance=0";
    
       $advisedid = $this->mod_advising->saveadvising($termid,$studno,$query);
       if($advisedid =='failed'){$result['content']='advising'; $result['error']='Advising failed!'; break; }
       if($enroll_data['studentno']!=''){$this->mod_main->Translog($session_data['id'],'Save Advising Details','STDNO:'.$enroll_data['studentno'].',TERM:'.$termid.',PROGID:'.$progid.',YRLVL:'.$yearlvl.',FEEID:'.$feeid); }		 
	    
       for($i=1;$i<=$advsubj;$i++)
	   {
	    $postid = 'sbjid'.$i;
	    $subjid = (array_key_exists($postid,$p)? str_replace("'","",$p[$postid]) : '');
	    if($subjid != ''){$this->mod_advising->saveadvisingdetails($advisedid,$i,$subjid,0); }
	   }
	
	   $this->mod_main->Translog($session_data['id'],'Save Advised Data','-');
	   $this->mod_advising->savereg($termid,$studno);
	   $this->mod_main->Translog($session_data['id'],'Save Registrations.', 'TermID:'.$termid.', STDNO:'.$studno);
	   $enroll_data['advisedid']=$advisedid;
	   log_message('error','advperiod:'.$enroll_data['onadvperiod'].'|enrperiod:'.$enroll_data['onenperiod'].'|withbal:'.$enroll_data['withbalance'].'|withaccnt'.$enroll_data['withaccount']);
	   if($enroll_data['onadvperiod']==1 && $enroll_data['onenperiod']==0)
	   {
	    $enroll_data['xadv']=0;
	    $enroll_data['xenr']=1;
	    $this->session->set_userdata('enrollinfo',$enroll_data);
		$result['result']  = true;
		$result['content'] = 'advising/schedules';
	   }
	   elseif(($enroll_data['onadvperiod']==1 || $enroll_data['onenperiod']==1)) //&& $enroll_data['withbalance']==1 && $enroll_data['withaccount']==1)
	   {
	    $enroll_data['xadv']=0;
	    $enroll_data['xenr']=1;
	    $this->session->set_userdata('enrollinfo',$enroll_data);
		$result['result']  = true;
		$result['content'] = 'advising/schedules';
	   }
	   else
	   {
		$result['result']  = false;
		$result['content'] = 'advising';
       }
      break;
      case 'registersubj':
	   $temp_reg = $this->mod_advising->get_RegID($enroll_data['studentno'],$enroll_data['activetermid']);
	   $adviseid = $enroll_data['advisedid'];
	   $regid    = (($enroll_data['regid']=='')?(($temp_reg && $temp_reg!='')?$temp_reg:''):$enroll_data['regid']);
	   if($regid=='' || $adviseid==''){$result['content'] = 'Failed to Save Subjects Schedule'; break;}
	   $rs = $this->mod_advising_sched->registersubjects($adviseid, $regid);
	   if($rs->rows <> 0)
	   {
		$result['result']  = true;
		$result['content'] = 'Successfully Save Subjects Schedule';
	   }
       else
	   {
		$result['content'] = 'Failed to Save Subjects Schedule';
	   }		   
	  break;
	  case 'register':
       $regid = 0;
       $this->EnableAdv=(($session_data['idtype']=='-1')?1:$this->EnableAdv);
	   if (isset($this->EnableAdv) and $this->EnableAdv==0){ $result['content']='advising'; $result['error']='Advising Is Disabled At This Moment!'; break; } 
	   $studno   = (($session_data['idtype']==1)?($session_data['idno']):($enroll_data['studentno']));
	   $adviseid = $p['adviseid'];
       $regid    = (($enroll_data['regid']!='')?$enroll_data['regid']:'');
	   $tid      = (($p['termid']!='')?($p['termid']):($enroll_data['activetermid']));
       $cid      = (($p['campusid']!='')?($p['campusid']):($enroll_data['campusid']));
	   $delete   = $p['todelete'];
       
	   if(trim($delete)!='')
	   {
	    $rs = explode(',',$delete);
	    foreach($rs as $k => $v)
	    {
	     if($adviseid!=''){$this->mod_advising->deleteadvsubject($adviseid,$v); }
	     $this->mod_main->Translog($session_data['id'],'Remove Subjects','AdvisedID:'.$adviseid.' SubjectID:'.$v);
	    }
	   }	
	
	   if($tid!='' && $cid!='')
	   {
        $rs = $this->mod_advising_sched->register($studno, $tid, $cid); 	
	    $regid = $rs->regid; 
	   }
	
       if($regid <> 0 && $adviseid <> '')
	   {
	    $rs = $this->mod_advising_sched->registersubjects($adviseid, $regid);
		
		$enroll_data['regid']=$regid;
        $this->session->set_userdata('enrollinfo',$enroll_data);	 
	    if($enroll_data['studentno']!=''){$this->mod_main->Translog($session_data['id'],'Save Registration','STDNO:'.$enroll_data['studentno'].',ADVID:'.$enroll_data['advisedid'].',REGID:'.$enroll_data['regid']); }		 
	    if($rs->rows <> 0)
	    {
	     $this->mod_main->Translog($session_data['id'],'Registration Details','-');
         if($enroll_data['onenperiod']==1)
         {
	      $fees = $this->mod_advising_sched->processfees($regid);
	      if($fees->num_rows() <> 0)
	      {
		   $enroll_data['xadv']=0;
		   $enroll_data['xenr']=0;
		   $enroll_data['xprt']=1;
		   $this->session->set_userdata('enrollinfo',$enroll_data);
	       $this->mod_main->Translog($session_data['id'],'Assessment Processed','-');
		   $result['result']  = true;
		   $result['content'] = 'advising/assessment/1';
          }
	      else
	      {
	       $this->mod_main->Translog($session_data['id'],'Assessment Not Generated','-');
	       $result['result']  = true;
		   $result['content'] = 'advising/assessment/2';
		   $result['error'] = 'Failed to Generate Assessment! Try registering again!';
	      }
	     }
         else
         {
	       $result['result']  = true;
		   $result['content'] = 'advising';
		   $result['error'] = 'Enrollment Is Closed at the Moment! Return When Enrollment Period is on to print your assessment!';
	     }		  
        }
	    else
	    {
		 $result['content'] = 'advising/schedules';
		 $result['error']   = 'Failed To Register! Try Again!';
		}
       }
	   else
	   {
	    $result['content'] = 'advising';
		$result['error']   = 'Data Error! Please Try Again!';
	   }
      break;
      case 'deleteall':
       $query = '';
	  break;	  
	  case 'subjsched':
	   echo 'wala pa po';
	  break;  
	 }
	break;   
   }
  }
  echo json_encode($result);	 
 }
 //
 
 function adviser(){
   if($this->session->userdata('logged_in') && $this->session->userdata('enrollinfo'))
   {
     $session_data = $this->session->userdata('logged_in');
     $enroll_data = $this->session->userdata('enrollinfo');
     $this->mod_main->monitoring_action($session_data,'advising');
	 if (isset($this->EnableAdv) && $this->EnableAdv==0 && $session_data['idtype']!='-1'){ redirect('advising', 'refresh'); } 
	 if (isset($enroll_data['xadv']) && $enroll_data['xadv']!=1){redirect('advising', 'refresh'); } 
     
	 $data['userinfo'] = $session_data;
     $data['title'] = "Advising";
     $data['username'] = $session_data['username']; 
	 $data['studentno'] = (($session_data['idtype']==1)?($session_data['idno']):($enroll_data['studentno']));
     	 
	 $data['xdetail'] =  $this->mod_advising->call_xtraDetails($data['studentno']);
	 $data['xdetail']->TermID = $enroll_data['activetermid'];
	 $data['xdetail']->AyTerm = $enroll_data['activeterm'];
	 $data['xdetail']->CollegeID = $enroll_data['collegeid'];
	 $data['xdetail']->CampusID = $enroll_data['campusid'];
	 $data['xdetail']->ProgID = $enroll_data['progid'];
	 $data['xdetail']->YrlvlID = $enroll_data['yrlvlid'];
	 $data['xdetail']->FeesID = $enroll_data['feesid'];
	 $data['onperiod'] = 0;
	 $data['onenperiod'] =$enroll_data['onenperiod'];
	 
	 //$data['onperiod'] = $this->mod_advising->checkadvisingperiod($data['studentno']);
	 if($enroll_data['onadvperiod']==1 || $enroll_data['onenperiod']==1){$data['onperiod'] = 1;}
	 if(($enroll_data['onadvperiod']==1 || $enroll_data['onenperiod']==1) && $enroll_data['withbalance']==1 && $enroll_data['withaccount']==1){$data['allowenroll']=1;}
	 //generate empty data.
	 list($data['rs'],$data['summary']) =  $this->mod_evaluation->generate_table('');
	 list($xinfo,$rs2)= $this->mod_advising->generate_advising('');
	 $data['rs2'] = $rs2;
	 //generate legend.
	 $data['lgnd'] =  $this->mod_evaluation->generate_legend();
	 $data['xalert']=$this->mod_advising->generate_alert($enroll_data['onadvperiod'],$enroll_data['onenperiod'],$enroll_data['balance'],$enroll_data['withbalance'],$enroll_data['accounts'],$enroll_data['withaccount'],$enroll_data['regonly'],$enroll_data['inactive']);
	 
	 $data['isreg'] = $enroll_data['isreg'];
	 $data['jslink'] = array('utilities/alertmsg.js?'.$this->jstimestamp
	                        ,'utilities/advising.js?'.$this->jstimestamp);
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_advising', $data);
     $this->load->view('include/footer',$data);
   }
   else
   {
    redirect('advising', 'refresh');
   }
 }
 
 function schedules($opt=0){
   if($this->session->userdata('logged_in') && $this->session->userdata('enrollinfo')) 
   {
     $session_data = $this->session->userdata('logged_in');
     $enroll_data = $this->session->userdata('enrollinfo');
	 $this->mod_main->monitoring_action($session_data,'advising');
	 if (isset($this->EnableAdv) && $this->EnableAdv==0  && $session_data['idtype']!='-1'){die('disabled');}// redirect('advising', 'refresh'); }
	 if (isset($enroll_data['xenr']) && $enroll_data['xenr']!=1){redirect('advising', 'refresh'); } 
	 
	 $data['title'] = "Advising";
         $data['username'] = $session_data['username'];
	 $data['userinfo'] = $session_data;
	 $data['studentno'] =(($session_data['idtype']==1)?$session_data['idno']:$enroll_data['studentno']);
	 $data['studentinfo'] = $enroll_data;
	 $data['advisedid'] = ((array_key_exists('advisedid',$enroll_data))?$enroll_data['advisedid']:'');
	 //$data['studentinfo'] = $this->mod_advising_sched->getlatestreginfo($data['studentno']);
	 //$data['advisedid'] = (is_object($data['studentinfo']) && property_exists($data['studentinfo'],'AdvisedID'))? $data['studentinfo']->AdvisedID : '';
     
	 $data['progclass'] = $enroll_data['progclass'];
 	 $data['snic'] = $enroll_data['snic'];     
     
	 $data['optctrl'] = ((isset($this->AdvSchedStyle))?$this->AdvSchedStyle:0);
	 if($data['optctrl']==1 || $data['optctrl']==2){$data['optsection'] = $this->mod_advising_sched->optClassSection($data['studentno']);}
     
	 $row = $this->mod_advising_sched->getadvisedsubj($data['advisedid']);
	 $data['row'] = $this->mod_advising_sched->generate_table($row,$enroll_data['onenperiod'],0);
      	 
	 $data['opt'] = $opt;
         $data['xprt'] =$enroll_data['xprt'];
	 
	 $data['xalert']=$this->mod_advising->generate_alert($enroll_data['onadvperiod'],$enroll_data['onenperiod'],$enroll_data['balance'],$enroll_data['withbalance'],$enroll_data['accounts'],$enroll_data['withaccount'],$enroll_data['regonly'],$enroll_data['inactive']);
	 
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_advising_schedule', $data);
     $ft['jslink']= array('utilities/alertmsg.js?'.$this->jstimestamp
	                     ,'utilities/advising.js?'.$this->jstimestamp);
     $this->load->view('include/footer',$ft);
	 $this->mod_main->Translog($session_data['id'],'View Schedule Selection','-');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

 function SIC_SNIC()
 {
  if($this->session->userdata('logged_in')) 
  {
   if($this->input->post()) 
   {
    $session_data= $this->session->userdata('logged_in');
    $enroll_data= $this->session->userdata('logged_in');
    
	if($this->AdvSchedStyle!=0 and $enroll_data['snic']!=1){ echo 'nodata-[o.o]-nodata'; die();}
    $stdno = $session_data['idno'];
   
    $termid = $this->input->post('termid',true);
	$row = $this->mod_advising->getofferedsubj($termid,$stdno);
    list($sic,$snic)=$this->mod_advising->listofferedsubj($row);
    echo $sic.'-[o.o]-'.$snic;
   }
   else
   {echo 'nodata-[o.o]-nodata';}
  }
  else
  {
   if($this->input->post()) 
   {echo 'nodata-[o.o]-nodata';}
   else
   {echo '';}
  }
 }
 
 function removesubject()
 {
  if($this->session->userdata('logged_in')) 
  {
   $session_data= $this->session->userdata('logged_in');
   $enroll_data= $this->session->userdata('logged_in');
    
   if($this->AdvSchedStyle!=0 or $enroll_data['snic']!=1){ echo 'nodata-[o.o]-nodata'; die();}
   $username = $session_data['id'];
   
   if($this->input->post())
   {
    $advid = $this->input->post('adviseid',true);
    $subjid = $this->input->post('subjectid',true);
    $result = $this->mod_advising->deleteadvsubject($advid,$subjid);
	$this->mod_main->Translog($username,'Remove Subject','AdvisedID:'.$advid.',SubjID:'.$subjid);
	echo $result;
   }
   else
   {echo 0;}   
  }
  else
  {echo 'nodata';}
 }
 
 function addsubject()
 {
  if($this->session->userdata('logged_in')) 
  {
   $session_data= $this->session->userdata('logged_in');
   $enroll_data= $this->session->userdata('logged_in');
    
   if($this->AdvSchedStyle!=0 or $enroll_data['snic']!=1){ echo 'nodata-[o.o]-nodata'; die();}
   
   $username = $session_data['id'];
   if($this->input->post())
   {
    $advid = $this->input->post('adviseid',true);
    $subjid = $this->input->post('subjectid',true);
    $result = $this->mod_advising->addsubject($advid,$subjid);
	$this->mod_main->Translog($username,'Append Subject','AdvisedID:'.$advid.',SubjID:'.$subjid);
	echo $result;
   }
   else
   {echo 0;}   
  }
  else
  {echo 'nodata';}
 }
 
 function offeredschedule(){
  if($this->session->userdata('logged_in')) {
    $session_data = $this->session->userdata('logged_in');
    $enroll_data  = $this->session->userdata('enrollinfo');
    $studno       = (($session_data['idtype']==1)?$session_data['idno']:$enroll_data['studentno']);
    $progid       = $this->input->post('progid',true);
    $termid       = $this->input->post('termid',true);
    $campusid     = $this->input->post('campusid', true);
    $currid       = $this->input->post('curriculum', true);
    $subjid       = $this->input->post('subject', true);
	$osched       = $this->mod_advising_sched->initofferedschedules($studno, $campusid,$termid, $currid, $subjid);
    $tmp          = "";
    $allow        = "0";
	$preferred    = "0";
	
   foreach($osched as $r){    
    $preferred = ($r->ProgID == $progid ? "1": "0");
	$allow     = intval($r->Status);
	$tmp       = $tmp . $r->ScheduleID . "|" .
						$r->Program . "|" .
						$r->SectionName . "|" .
						$r->Course . "|" .
						$r->Registered . "/" . $r->Limit . "|" .
						$r->Sched_1 . " " . $r->Room1 . 
								trim_slashes(($r->Sched_2 <> ""? "<br>" . $r->Sched_2 . " " . $r->Room1 : "")) .
								trim_slashes(($r->Sched_3 <> ""? "<br>" . $r->Sched_3 . " " . $r->Room1: "")) .
								trim_slashes(($r->Sched_4 <> ""? "<br>" . $r->Sched_4 . " " . $r->Room1: "")) .
								trim_slashes(($r->Sched_5 <> ""? "<br>" . $r->Sched_5 ." ". $r->Room1: "")) . "|" .
						$r->Acadunits . "|" .
						$r->Labunits . "|" .
						$allow . "|" . $preferred . "|" . $r->SectionID .
						";";
   }
   If (trim($tmp) <> "") { $tmp = substr($tmp, 0, -1); }
   
   //echo json_encode(  trim_slashes($tmp) );
   echo  trim_slashes($tmp) ;
  } else { redirect('login', 'refresh'); }
 }
 
 function checkconflict(){
  $sched1 = $this->input->post('selcsid',true);
  $sched2 = $this->input->post('myscids',true);
  
  $tags = explode('|' , $sched2);
  $tmp = 0 ;
  foreach($tags as $i =>$key) {
  //$i >0;
      //echo $i.' '.$key .'<br>';
      //echo $sched1 . '-'. $key . ';' ;
       if ($sched1 <> $key){
        $conflict = $this->mod_advising_sched->compareschedule(trim($sched1), trim( $key));
         foreach($conflict as $r){
          if ($r->Conflict <> 0){
           //$tmp = $tmp . $r->Conflict . ',';
           $tmp=$tmp+1;
          }
         } 
      }
  }
  echo json_encode($tmp);
 }
 
 function preferredsection(){
    $session_data = $this->session->userdata('logged_in');
	$advid = $this->input->post('advid',true);
    $sectionid = $this->input->post('idx',true);
	$rs = $this->mod_advising->ipreferredsection($advid, $sectionid);
	$tmp="";
    foreach($rs as $r){
	  $tmp = $tmp . $r->ScheduleID . "|" . $r->SectionName . "|" .
				$r->Sched_1 . " " . $r->Room1 . 
                        trim_slashes(($r->Sched_2 <> ""? "<br>" . $r->Sched_2 . " " . $r->Room1 : "")) .
                        trim_slashes(($r->Sched_3 <> ""? "<br>" . $r->Sched_3 . " " . $r->Room1: "")) .
                        trim_slashes(($r->Sched_4 <> ""? "<br>" . $r->Sched_4 . " " . $r->Room1: "")) .
                        trim_slashes(($r->Sched_5 <> ""? "<br>" . $r->Sched_5 ." ". $r->Room1: "")) .
								"|" . $r->SubjectID . ";" ; 
								
	}
	
	If (trim($tmp) <> "") {  $tmp = substr($tmp, 0, -1); $this->mod_main->Translog($session_data['id'],'Preferred Block Section',$sectionid);}
   echo  trim_slashes($tmp) ;
 }
 
 function enrollment(){
   $session_data = $this->session->userdata('logged_in');
   $enroll_data = $this->session->userdata('enrollinfo');
   $username = $session_data['id'];
   $adviseid = $this->input->post('adviseid',true);
   $subjectid = $this->input->post('subjectid',true);
   $scheduleid = $this->input->post('scheduleid',true);
   
   $result = $this->mod_advising_sched->save_schedule($adviseid,str_replace("s","", $subjectid) , $scheduleid);
   //$result = $this->mod_advising_sched->registersubjects($adviseid, $enroll_data['regid']);
   //$result = $adviseid . '|' . str_replace("s","", $subjectid) . '|' . '|' . $scheduleid;
   $this->mod_main->Translog($username,'Save Schedule','AdvID:'.$adviseid.', SubjID'.$subjectid.', SchedID:'.$scheduleid);
   echo $result;
 }
 
 function removeschedule(){
   $session_data = $this->session->userdata('logged_in');
   $username = $session_data['id'];
   $adviseid = $this->input->post('adviseid',true);
   $subjectid = $this->input->post('subjectid',true);  
   $result = $this->mod_advising_sched->rem_schedule($adviseid,str_replace("s","", $subjectid));
   $this->mod_main->Translog($username,'Remove Schedule' ,'AdvID:'.$adviseid.', SubjID:'.$subjectid);
   echo $result;
 }
 
 function cancelrequest()
 {
  if($this->session->userdata('logged_in')) {
    $session_data = $this->session->userdata('logged_in');
    $username = $session_data['id'];
    $studno = $session_data['idno'];
    $schedid = $this->input->post('idx',true);
    $msg = $this->input->post('message',true);
    $result = $this->mod_advising->cancelrequest($studno, $schedid);
    $this->mod_main->Translog($username,'Cancel Schedule Request','StdNo:'.$studno.', SchedID:'.$schedid);
    echo $result;
  }else { 
    redirect('login', 'refresh'); 
  }
 }
 
 function register(){
  if($this->session->userdata('logged_in')){
    $session_data = $this->session->userdata('logged_in');
	$enroll_data = $this->session->userdata('enrollinfo');
	$regid = 0;
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
    if (isset($this->EnableAdv) && $this->EnableAdv==0  && $session_data['idtype']!='-1'){ redirect('advising', 'refresh'); } 
	$studno = (($session_data['idtype']==1)?($session_data['idno']):($enroll_data['studentno']));
	$adviseid = $this->input->post('adviseid',true);
    $tid = (($this->input->post('termid',true)!='')?($this->input->post('termid',true)):($enroll_data['activetermid']));
    $cid = (($this->input->post('campusid',true)!='')?($this->input->post('campusid',true)):($enroll_data['campusid']));
	
    $delete = $this->input->post('todelete',true);
    if(trim($delete)!='')
	{
	 $rs = explode(',',$delete);
	 foreach($rs as $k => $v)
	 {
	  $this->mod_advising->deleteadvsubject($adviseid,$v);
	  $this->mod_main->Translog($session_data['id'],'Remove Subjects','AdvisedID:'.$adviseid.' SubjectID:'.$v);
	 }
	}	
	
	if($tid!='' && $cid!=''){
     $result = $this->mod_advising_sched->register($studno, $tid, $cid); 	
	 $regid = $result->regid; 
	}
	
    if($regid <> 0 && $adviseid <> '')
	{
	 log_message('error','AdviseID:'.$adviseid.' RegID:'.$regid);
     $result = $this->mod_advising_sched->registersubjects($adviseid, $regid);
	 $enroll_data['regid']=$regid;
     $this->session->set_userdata('enrollinfo',$enroll_data);	 
	 if($result->rows <> 0)
	 {
	  $this->mod_main->Translog($session_data['id'],'Registration Details','-');
      if($enroll_data['onenperiod']==1)
      {
	   $fees = $this->mod_advising_sched->processfees($regid);
	   if ($fees->num_rows() <> 0)
	   {
		$enroll_data['xprt']==1;
		$this->session->set_userdata('enrollinfo',$enroll_data);
	    $this->mod_main->Translog($session_data['id'],'Assessment Processed','-');
		redirect('advising/assessment/1', 'refresh');
       }
	   else
	   {
	    $this->mod_main->Translog($session_data['id'],'Assessment Not Generated','-');
	    redirect('advising/assessment/2', 'refresh');
	   }
	  }
      else
      {
	   redirect('advising', 'refresh');	  
	  }		  
     }
	 else
	 {redirect('advising/schedules/1', 'refresh');}
    }
	else
	{
	 echo 'Blank Inputs';	
	}
  } 
  else { redirect('advising', 'refresh'); }
 }
 
 function assessment($opt=0){
  if($this->session->userdata('logged_in')) {
     $session_data = $this->session->userdata('logged_in');
	 $enroll_data = $this->session->userdata('enrollinfo');
	 $this->mod_main->monitoring_action($session_data,'advising');
	 if (isset($this->EnableAdv) && $this->EnableAdv==0  && $session_data['idtype']!='-1'){ redirect('advising', 'refresh'); } 
	 if ($enroll_data['onenperiod']==0){redirect('advising', 'refresh');}
	 
     $data['userinfo'] = $session_data;     
     $data['title'] = "Enrollment";
     $data['username'] = $session_data['username'];
     $data['studentno'] = (($session_data['idtype']==1)?($session_data['idno']):($enroll_data['studentno']));
	 $data['studentinfo'] = $this->mod_advising_sched->getlatestreginfo($data['studentno']);
	 
	 $regid = (is_object($data['studentinfo']) && property_exists($data['studentinfo'],'RegID'))?$data['studentinfo']->RegID:'';
     
	 $row = $this->mod_advising_sched->getenrolledsubj($regid);
	 
	 $data['row'] = $this->mod_advising_sched->generate_table($row,0,1);
     $fees = $this->mod_advising_sched->getassessedfees($regid);
     $data['fees'] = $this->mod_advising_sched->generate_feetablev2($fees);
     $data['opt']=0;
	 
	 if($opt!=0)
      $data['opt']=$opt;
	 else
     {
	  if(strpos($data['fees'],'No Assessed Fees')!== false)
	  {$data['opt'] = 2.1;}
	  if(strpos($data['row'],'No Subject is Advised')!== false)
	  {$data['opt'] = 2.2;}
	 }	 
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_advised_assessment', $data);     
     $ft['jslink']= array('utilities/alertmsg.js?'.$this->jstimestamp
	                     ,'utilities/advising.js?'.$this->jstimestamp);
     $this->load->view('include/footer',$ft);
     $this->mod_main->Translog($session_data['id'],'View Assessment','-');
   }
   else { redirect('login', 'refresh'); }
 }
 
 function request(){
  if($this->session->userdata('logged_in')) {
    $session_data = $this->session->userdata('logged_in');
    $studno = $session_data['idno'];
    $schedid = $this->input->post('schedid',true);
    $msg = $this->input->post('message',true);
    $result = $this->mod_advising->sendrequest($studno, $schedid, $msg);
    echo $result->result;
    $this->mod_main->Translog($session_data['id'],'Send Schedule Request','StdNo:'.$studno.',SchedID:'.$schedid);
  } else { redirect('login', 'refresh'); }
 }

 function printout(){
 //crystal report version
  if($this->session->userdata('logged_in')) {
	$date_assess = $this->mod_main->getconfigvalue('DateEnableAssessment');   
	if(strtotime($date_assess)){
	 $current = date('m/d/Y');
	 $period  = date('m/d/Y',strtotime($date_assess));
	 if($current < $period)
	 {
	  echo 'Printing of Pre-Reg/Assessment is not allowed at this moment.';
	  die();
	 }
	}else{
	 if($date_assess!='Yes' && $date_assess!='1' && $date_assess!=1)
	 {
	  echo 'Printing of Pre-Reg/Assessment is disabled at this moment.';
	  die();
	 }	
	}
	
    if(!extension_loaded('com_dotnet'))
    {
     echo 'Error Loading Extension. Unable to Print.';
	 die();
    }
    else
    {
    $session_data = $this->session->userdata('logged_in');
	$enroll_data = $this->session->userdata('enrollinfo');
	
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	if (isset($this->EnableAdv) && $this->EnableAdv==0 && $session_data['idtype']!='-1'){ redirect('advising', 'refresh'); } 
	
	$studno = (($session_data['idtype']==1)?$session_data['idno']:$enroll_data['studentno']);    
	$studentinfo = $this->mod_advising_sched->getlatestreginfo($studno);
       /*
	$exec = $this->db->query("SELECT (CASE WHEN dbo.fn_sisForwardedBalance(1,StudentNo,r.TermID) > TotalAssessment AND TotalAssessment>0 THEN 1 ELSE 0 END) as 
                                    WithError 
                                FROM ES_Registrations as r
                               WHERE TermID=dbo.fn_sisConfigNetworkTermID() AND StudentNo='".$studno."'")->result();
	if($exec[0]->WithError==1){
	  echo 'Please ask for the accounting for your Pre-Registration/Assessment.';
	  die();	
	}
	*/
	if(is_object($studentinfo))
	{
	 $file='none';
	 $filename='none';
	 $regid = property_exists($studentinfo,'RegID')? $studentinfo->RegID : '';
	
	 if(trim($regid)!='')
	 {
	  log_message('error','Printing - '.$regid);
	  list($file,$filename)= $this->mod_crystalreports->prepare_prereg($regid);
	 }
    	
	 if(($file!='none' && $filename!='none') && ($file!='none1' && $filename!='none1'))
	 {
	  //echo $file;
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print Pre-Reg','-');
     /*
	 */	  
	 }
	 elseif($file=='none' && $filename=='none')
	 {echo 'Error Loading the report. Please Report to the administrators.';}
	 elseif($file=='none1' && $filename=='none1')
	 {echo 'Error Loading the component. Please Report to the administrators.';}
    }
	else { redirect('advising', 'refresh'); }
	}
  }else { redirect('login', 'refresh'); }

 }
 
  function printcor(){
 //crystal report version
  if($this->session->userdata('logged_in')) {
	$date_cor = $this->mod_main->getconfigvalue('DateEnableCOR');   
        if(strtotime($date_cor)){
	 $current = date('m/d/Y');
	 $period  = date('m/d/Y',strtotime($date_cor));
	 if($current < $period)
	 {
	  echo 'Printing of "Certificate of Registration" is not allowed at this moment.';
	  die();
	 }
	}else{
	  if($date_cor!='Yes' && $date_cor!='1' && $date_cor!=1){
	   echo 'Printing of "Certificate of Registration" is disabled at this moment.';
	   die();
	  }	
	}
	
	if(!extension_loaded('com_dotnet'))
	{
	 echo 'Error Loading Extension. Unable to Print.';
	 die();
	}
	
	$session_data = $this->session->userdata('logged_in');
	$enroll_data = $this->session->userdata('enrollinfo');
	
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	if (isset($this->EnableAdv) && $this->EnableAdv==0 && $session_data['idtype']!='-1'){ redirect('advising', 'refresh'); } 
	
    $studno = (($session_data['idtype']==1)?$session_data['idno']:$enroll_data['studentno']);    
    
	$file='none';
	$filename='none';
	$regid = array_key_exists('regid',$enroll_data)? $enroll_data['regid'] : '';
	$ayterm = array_key_exists('activeterm',$enroll_data)? $enroll_data['activeterm'] : '';
	
   if($regid!='' && $regid!=' ')
   {
	list($file,$filename)= $this->mod_crystalreports->prepare_cor($regid,$ayterm);
		
	if($file!='none' && $filename!='none')
	{
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print C.O.R.','-');
	}
	else
	{echo 'Error Loading the report. Please Report to the administrators.';}
   }
   else { redirect('advising', 'refresh'); }
   
  }else { redirect('login', 'refresh'); }
}
 
 
 function payment(){
  if($this->session->userdata('logged_in')) {
    if (isset($this->EnableAdv) and $this->EnableAdv==0){ redirect('advising', 'refresh'); } 
	
    $session_data = $this->session->userdata('logged_in');
    $data['userinfo'] = $session_data;     
     $data['title'] = "Enrollment";
     $data['username'] = $session_data['username'];
     $data['studentno'] = $session_data['idno'];
     $data['studentinfo'] = $this->mod_advising_sched->getlatestreginfo($data['studentno']);
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $data['studentinfo'] = $this->mod_advising_sched->getlatestreginfo($session_data['idno']);
     $this->load->view('vw_payment',$data);
     $ft['jslink']= array('utilities/advising.js?'.$this->jstimestamp);
     $this->load->view('include/footer',$ft);
  }else{ 
     redirect('login', 'refresh'); 
  }
 }
 
 function trial(){
  die();  
 }

} // end of class
?> 

