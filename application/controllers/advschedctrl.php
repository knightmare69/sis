<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Advschedctrl extends CI_Controller 
{
 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_main','',true);
   $this->load->model('mod_advschedctrl','',true);
 }
 
 function index()
 {
   $session_data = $this->session->userdata('logged_in');
   if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
   if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 
   $data['userinfo'] = $session_data;
   $data['title'] = "Advising Schedule Control";
   $data['username'] = $session_data['username'];
   $data['ayterm'] = $this->mod_main->get_ayterm(10000);
   $data['program'] = $this->mod_advschedctrl->get_schedule();
   
   $footer['jslink'] = array('plugin/bootstrap-timepicker/bootstrap-timepicker.min.js','utilities/advschedctrl.js?20160419');
   
   $this->load->view('include/header',$data);
   $this->load->view('templates/mainmenu',$data);
   $this->load->view('vw_advschedctrl', $data);
   $this->load->view('include/footer',$footer);
   $this->mod_main->Translog($session_data['id'],'View Advising Schedule Ctrl','-'); 
 }
 
 function txn($mode='',$type='',$args='')
 {
  $result = array('success'=>false,'content'=>'','error'=>'No Data Available');
  $p = (($this->input->post())?($this->input->post()):array());
  
  if($this->session->userdata('logged_in'))
  {
   $session_data = $this->session->userdata('logged_in');
   switch($mode)
   {
    case 'get':
	 switch($type)
	 {
	   case 'tblist':
	    $termid = (($args!='')? $args : '-1');
		$tblist = $this->mod_advschedctrl->get_schedule($termid);
		if($tblist)
		{
		  $result['success'] = true;
		  $result['content'] = $this->load->view('vw_advschedctrl_list',array('list'=>$tblist),true);
		  $result['error']   = '';
		}	
	   break;
	 }
	break;
	case 'set':
     switch($type)
	 {
	   case 'schedule':
	    $indx     = ((@array_key_exists('indx',$p))? $p['indx'] : 'new');
	    $termid   = ((@array_key_exists('termid',$p))? $p['termid'] : -1);
	    $progid   = ((@array_key_exists('progid',$p))? $p['progid'] : -1);
	    $majorid  = ((@array_key_exists('majorid',$p))? $p['majorid'] : 0);
	    $yrlvl    = ((@array_key_exists('yrlvl',$p))? $p['yrlvl'] : 0);
	    $dstart   = ((@array_key_exists('dstart',$p))? $p['dstart'] : '');
	    $dend     = ((@array_key_exists('dend',$p))? $p['dend'] : '');
	    $inactive = ((@array_key_exists('inactive',$p))? $p['inactive'] : 0);
		
		$process = $this->mod_advschedctrl->manage_schedule(1,$indx,$termid,$progid,$majorid,$yrlvl,$dstart,$dend,$inactive);
		if($process)
		{
		 $result['success'] = true;	
	     $this->mod_main->Translog($session_data['id'],'Manage Advising Schedule','TermID:'.$termid.' PROGID:'.$progid.' MAJOR:'.$majorid); 
		}	
	   break;
	 }  
	break;
	case 'delete':
     switch($type)
	 {
	   case 'schedule':
	    $indx     = ((@array_key_exists('indx',$p))? $p['indx'] : 'new');
	    $process = $this->mod_advschedctrl->manage_schedule(3,$indx);
		$result['success'] = $process;	
	    $this->mod_main->Translog($session_data['id'],'Delete Advising Schedule','SchedID:'.$indx); 
			
	   break;
	 }  
	break;
   }
  }
  else
  {
    $result['error']=false;
	$result['error']='No Session Found';  
  }	  
	 
  echo json_encode($result);
 }

}
?>