
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Calendar extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['userinfo'] = $session_data;
     $data['title'] = "Calendar";
     $data['username'] = $session_data['username'];        
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_calendar', $data);
     
     $mfooter['jslink'] = array('plugin/fullcalendar/jquery.fullcalendar.min.js',
                                'utilities/calendar.js'
                                );
     
     $this->load->view('include/footer',$mfooter);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
}

?>

