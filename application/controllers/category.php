
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Category extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('mod_main','',TRUE);
    $this->load->helper('url');
  }
 
 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     
     $this->load->helper(array("form"));		
    $session_data = $this->session->userdata('logged_in');
    $data['userinfo'] = $session_data;
    $data['username'] = $session_data['username'];
    $data['title'] = "Religion";
    $data['javascript'] = array('utilities/religion.js');
    $data['religion'] = $this->mod_religion->get_all();
    $this->load->view('include/header',$data);
    $this->load->view('templates/mainmenu',$data);
    $this->load->view('vreligion', $data);
    $this->load->view('include/footer',$data);
     
     
     
     
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

  function delete()
  {
    $indexid = StripSlashes($this->input->post('tindex'));
    $confirm = $this->mod_religion->del_record($indexid);
    
    if ($confirm>=1){
     $this->MessageBox('Record deleted!');
     redirect('utilities/religion', 'refresh');
    }
    
  }
 
 function keep(){
  $this->messagebox(site_url("news/local/123"));
  //redirect('utilities/religion', 'refresh');
  
 }
 
}

?>

