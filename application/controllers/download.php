
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Download extends CI_Controller {

 function __construct()
 {
   parent::__construct();
  $this->load->model("mod_downloads",'',true);
  $this->load->model('mod_main','',true);
 }

 function index(){
   if($this->session->userdata('logged_in')){
     $session_data = $this->session->userdata('logged_in');
	 if (!@array_key_exists('id',$session_data)){ redirect('logout', 'refresh'); } 
	 if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 
     $data['userinfo'] = $session_data;
     $data['title'] = "Downloads";
     $data['username'] = $session_data['username'];
       
   //$data['ds1'] = $this->mod_downloads->usp_getDetails($descName);
     $data['rs'] = $this->mod_downloads->DDList();
    
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_downloads', $data);
     $this->load->view('include/footer');
	 $this->mod_main->Translog($session_data['id'],'View Downloads','-');
   }
   else
   {
     //If no session, redirect to login page
     redirect('logout', 'refresh');
   }
 }
 

 function Dtls($descName){
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
	 if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 
     $data['userinfo'] = $session_data;
     $data['title'] = "Downloads";
     $data['username'] = $session_data['username'];
     
     $data['rs'] = $this->mod_downloads->DDList();
     $data['ds1'] = $this->mod_downloads->usp_getDetails($descName);
     $data['xdescName'] = $descName;
     
     
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_downloads', $data);
     $this->load->view('include/footer');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 public function ibed11() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED1/1pgsapplicants.pdf");
		$name = '1pgsapplicants.pdf';
		force_download($name, $data);
                }
                public function ibed12() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED1/2admissionprocedure.pdf");
		$name = '2admissionprocedure.pdf';
		force_download($name, $data);
                }
         	public function ibed13() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED1/3scheduleofadmissiontest.pdf");
		$name = '3scheduleofadmissiontest.pdf';
		force_download($name, $data);
                }
                
         	public function ibed14() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED1/4list-of-qualified-examinees.pdf");
		$name = '4list-of-qualified-examinees.pdf';
		force_download($name, $data);
                }
                
        	public function ibed15() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED1/5schedule-of-enrollment.pdf");
		$name = '5schedule-of-enrollment.pdf';
		force_download($name, $data);
                }
                
                public function ibed16() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED1/6enrollment-procedure.pdf");
		$name = '6enrollment-procedure.pdf';
		force_download($name, $data);
                }
                
                public function ibed17() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED1/7application-a-recommendation-forms.pdf");
		$name = '7application-a-recommendation-forms.pdf';
		force_download($name, $data);
                }
        
                //
        
                public function ibed21() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED2/1pgsapplicants.pdf");
		$name = '1pgsapplicants.pdf';
		force_download($name, $data);
                }
                public function ibed22() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED2/2admissionprocedure.pdf");
		$name = '2admissionprocedure.pdf';
		force_download($name, $data);
               }
         	public function ibed23() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED2/3scheduleofadmissiontest.pdf");
		$name = '3scheduleofadmissiontest.pdf';
		force_download($name, $data);
               }
         	public function ibed24() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED2/4list-of-qualified-examinees.pdf");
		$name = '4list-of-qualified-examinees.pdf';
		force_download($name, $data);
               }
         	public function ibed25() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED2/5schedule-of-enrollment.pdf");
		$name = '5schedule-of-enrollment.pdf';
		force_download($name, $data);
                }
                public function ibed26() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED2/6enrollment-procedure.pdf");
		$name = '6enrollment-procedure.pdf';
		force_download($name, $data);
                }
                public function ibed27() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/IBED2/7application-a-recommendation-forms.pdf");
		$name = '7application-a-recommendation-forms.pdf';
		force_download($name, $data);
                }

                
                public function ibed31() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/ibed3/1pgsapplicants.pdf");
		$name = '1pgsapplicants.pdf';
		force_download($name, $data);
                }
                public function ibed32() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/ibed3/2admissionprocedure.pdf");
		$name = '2admissionprocedure.pdf';
		force_download($name, $data);
               }
         	public function ibed33() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/ibed3/3scheduleofadmissiontest.pdf");
		$name = '3scheduleofadmissiontest.pdf';
		force_download($name, $data);
               }
         	public function ibed34() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/ibed3/4list-of-qualified-examinees.pdf");
		$name = '4list-of-qualified-examinees.pdf';
		force_download($name, $data);
               }
         	public function ibed35() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/ibed3/5schedule-of-enrollment.pdf");
		$name = '5schedule-of-enrollment.pdf';
		force_download($name, $data);
                }
                public function ibed36() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/ibed3/6enrollment-procedure.pdf");
		$name = '6enrollment-procedure.pdf';
		force_download($name, $data);
                }
                public function ibed37() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/ibed3/7add-a-drop-schedule-a-procedure.pdf");
		$name = '7add-a-drop-schedule-a-procedure.pdf';
		force_download($name, $data);
                }
                
                public function CASA1() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/CASA/1pgsapplicants.pdf");
		$name = 'pgsapplicants.pdf';
		force_download($name, $data);
                }
                public function CASA2() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/CASA/2admissionprocedure.pdf");
		$name = '2admissionprocedure.pdf';
		force_download($name, $data);
               }
         	public function CASA3() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/CASA/3scheduleofadmissiontest.pdf");
		$name = '3scheduleofadmissiontest.pdf';
		force_download($name, $data);
               }
         	public function CASA4() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/CASA/4list-of-qualified-examinees.pdf");
		$name = '4list-of-qualified-examinees.pdf';
		force_download($name, $data);
               }
         	public function CASA5() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/CASA/5schedule-of-enrollment.pdf");
		$name = '5schedule-of-enrollment.pdf';
		force_download($name, $data);
                }
                public function CASA6() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/CASA/6enrollment-procedure.pdf");
		$name = '6enrollment-procedure.pdf';
		force_download($name, $data);
                }
                public function CASA7() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/CASA/7add-a-drop-schedule-a-procedure.pdf");
		$name = '7application-a-recommendation-forms.pdf';
		force_download($name, $data);
                }
                
                public function AARF1() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/AARF/admission_application_form.pdf");
		$name = 'admission_application_form.pdf';
		force_download($name, $data);
                }
                
		public function AARF2() {

		$this->load->helper('download');
		$data = file_get_contents("./Downloads/AARF/class_adviser_or_guidance_counselors_recommedation_form.pdf");
		$name = 'class_adviser_or_guidance_counselors_recommedation_form.pdf';
		force_download($name, $data);
                }
                
      public function AARF3() {
		
		$this->load->helper('download');
		$data = file_get_contents("./Downloads/AARF/principals_recommendation_form.pdf");
		$name = 'principals_recommendation_form.pdf';
		force_download($name, $data);
                }
}

?>

