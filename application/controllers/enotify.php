<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Enotify extends CI_Controller 
{
 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_enotify','',TRUE);
   $this->load->library('email');
 }

 function index()
 {
  $session_data = $this->session->userdata('logged_in');
  $this->mod_main->monitoring_action($session_data,'enotify');
	 
  $data['userinfo'] = $session_data;
  $data['title'] = "Email Notification";
  $data['username'] = $session_data['username'];  
  $data['xtable'] = $this->mod_enotify->Gen_MemberList(50,0);
  
  $footer['jslink'] = array('plugin/summernote/summernote.js','utilities/enotify.js?201606081');
  
  $this->load->view('include/header',$data);
  $this->load->view('templates/mainmenu',$data);
  $this->load->view('vw_enotify',$data);
  $this->load->view('include/footer',$footer);
 }
 
 function txn($type='',$mode='',$args=array())
 {
  $session_data = $this->session->userdata('logged_in');
  $result = array('success'=>false,'content'=>'','error'=>'');
  $xpost  = $this->input->post(NULL,TRUE);
  switch($type)
  {
	case 'get':
	  switch($mode)
	  {
		case 'list':
		  $uid   = is_key_exist($xpost,'uid',0);
          $result['success']=true;	   			  
          $result['content']=$this->mod_enotify->Gen_xMemberList(250,$uid);	   		
          $this->mod_main->Translog($session_data['id'],'Generate List','Initial UID:'.$uid);	  
        break;		
	  } 
	  echo json_encode($result,JSON_PARTIAL_OUTPUT_ON_ERROR);
    break;
    case 'set':
	  switch($mode)
	  {
	   case 'send':
	    $email = is_key_exist($xpost,'email','');
	    $subj  = is_key_exist($xpost,'subj','');
	    $msg   = is_key_exist($xpost,'msg','');
		
		if($email!='' && $subj!='' && $msg!='')
        {  
	     $config='';
	     $data['msg'] =$msg;
         $msg = $this->load->view('mail/mail_template',$data,true);
    
	     //try
	     //{
          $result['success'] = $this->mod_main->xsendmail($config,$email,$msg,$subj);
	      $result['error']   = (($result['success'])?'':'Failed to Send Mail.');
          $this->mod_main->Translog($session_data['id'],'Send Mail','Subj:'.$subj);
	     //}
	     //catch(Exception $e)
	     //{
	     // $result['error'] = 'Failed to Send Mail.';
         // $this->mod_main->Translog($session_data['id'],'Failed Mail','Subj:'.$subj);
	     //}
        }
        else
        {
		 $result['error'] = 'Invalid Data.';	
         $this->mod_main->Translog($session_data['id'],'Send Mail','Subj:'.$subj);
	    }
       break;	   
	  }
	  echo json_encode($result);
    break;	
  }  
 }
 
 function emailall()
 {
  if($this->input->post())
  {
   $data = $this->input->post(NULL,TRUE);
   $email= $data['email'];
   $subj = $data['subj'];
   $msg  = $data['msg'];
   
   if($email!='' && $subj!='' && $msg!='')
   {
	$config='';
	$data['msg'] =$msg;
    $msg = $this->load->view('mail/mail_template',$data,true);
    
	try
	{
     if($this->mod_main->xsendmail($config,$email,$msg,$subj))
      echo 'success';
	 else
      echo 'failed';
	}
	catch(Exception $e)
	{
	 echo 'failed';
	}
   }
   else
   {
    echo 'nodata';
   }
  }
  else
  {
   echo 'nodata';
  }
 }
 
} 
?>