<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Enrollment extends CI_Controller{

 function __construct(){
   parent::__construct();
   
   date_default_timezone_set("Asia/Taipei");
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_advising','',TRUE);
   $this->load->model('mod_advising_sched','',TRUE);
   $this->load->model('mod_evaluation','',TRUE);
   $this->load->model('mod_crystalreports','',TRUE);
   $this->load->helper(array("form"));
   $this->AdvSchedCtrl    = 1;
   $this->AdvSchedStyle  = $this->mod_main->getconfigvalue('AdvisingSchedSelection');   
   $this->EnableAdv         = $this->mod_main->getconfigvalue('AdvisingModuleLock');    
   $this->RegOnly            = $this->mod_main->getconfigvalue('AdvRegularOnly'); 
   $this->jstimestamp   = '20171122';   
   $this->IsAdvance     = array('');
   $this->EnrollStart   = date('Y-m-d',strtotime('2017-11-20'));
   ini_set('memory_limit','128M');   
 }
 
 function index(){
   if($this->session->userdata('logged_in')) {
	 $session_data = $this->session->userdata('logged_in');
     $this->mod_main->monitoring_action($session_data);
	 $data['title']    = "Enrollment";
	 $data['userinfo'] = $session_data;
	 $data['idtype']   = $session_data['idtype'];
	 $data['username'] = $session_data['username'];
	 
	 //$session_data['idno'] = 'MD-1800129';
	 $termid               = 27;
	 $campus               = 1;
	 $islist               = $this->mod_main->isForEnrollment($session_data['idno']);
	 $regid                = 0;
	 $data['xdetail']      =  $this->mod_main->call_xtraDetails($session_data['idno']);
     $data['progid']       = (($session_data['idno']!=0)?($data['xdetail']->ProgID):0);
     $data['progclass']    = (($session_data['idno']!=0)?($data['xdetail']->ProgClass):30);
	 if($islist && ($islist->RegID=='' || $islist->RegID==0)){
	     $reginfo  = $this->mod_advising_sched->register($islist->StudentNo, $islist->TermID, 1);
		 $regid    = $reginfo->regid;
         if($regid!='' && $regid!=0){
          $regdtls  = $this->mod_main->saveFromSection($regid,$islist->SectionID);		
		  $feeinfo  = $this->mod_advising_sched->processfees($regid);
		  $schedule = $this->mod_main->getScheduleBySection($islist->SectionID);
		  $data['schedule'] = $this->mod_advising_sched->generate_table($schedule,1,1);
		  $data['validation'] = $islist->ValidationDate;
		  $data['buttons']  = true;
         }else{
          $data['schedule'] = "<div class='alert alert-danger no-margin fade in'><i class='fa-fw fa fa-warning'></i> Failed To Execute Enrollment.</div>";
		  $data['buttons']  = false;
         }
	 }elseif($islist && $islist->RegID!=''){
		$regid    = $islist->RegID;
		$schedule = $this->mod_main->getScheduleBySection($islist->SectionID);
		$data['schedule'] = $this->mod_advising_sched->generate_table($schedule,1,1);
		$data['validation'] = $islist->ValidationDate;
		$data['buttons']  = true;
	 }else{
		$data['schedule'] = "<div class='alert alert-warning no-margin fade in'><i class='fa-fw fa fa-warning'></i> Enrollment Is Closed For Your Program And YearLevel.</div>";
		$data['buttons']  = false;
	 }
	 
	 $footer['jslink'] = array('utilities/enrollment.js?'.$this->jstimestamp);
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('enrollment/vw_enrollment', $data);
     $this->load->view('include/footer',$footer);
	 $this->mod_main->Translog($session_data['id'],'View Advising Controller',$session_data['username']);
   }else
	 redirect('profile','refresh');
 }

 function printout(){
 //crystal report version
  if($this->session->userdata('logged_in')){
    if(!extension_loaded('com_dotnet')){
     echo 'Error Loading Extension. Unable to Print.';
	 die();
    }else{
    $session_data = $this->session->userdata('logged_in');
	
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	
	 //$session_data['idno'] = 'MD-1800129';
	$studno = $session_data['idno'];    
	$studentinfo = $this->mod_main->isForEnrollment($session_data['idno']);
	
	if(is_object($studentinfo)){
	 $file='none';
	 $filename='none';
	 $regid = property_exists($studentinfo,'RegID')? $studentinfo->RegID : '';
	
	 if(trim($regid)!=''){
	  log_message('error','Printing - '.$regid);
	  list($file,$filename)= $this->mod_crystalreports->prepare_prereg($regid);
	 }
    	
	 if(($file!='none' && $filename!='none') && ($file!='none1' && $filename!='none1')){
	  //echo $file;
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print Pre-Reg','-');
	 }
	 elseif($file=='none' && $filename=='none')
	 {echo 'Error Loading the report. Please Report to the administrators.';}
	 elseif($file=='none1' && $filename=='none1')
	 {echo 'Error Loading the component. Please Report to the administrators.';}
    }
	else { redirect('enrollment', 'refresh'); }
	}
  }else { redirect('login', 'refresh'); }

 }
 
 function printcor(){
 //crystal report version
  if($this->session->userdata('logged_in')) {
	if(!extension_loaded('com_dotnet'))
	{
	 echo 'Error Loading Extension. Unable to Print.';
	 die();
	}
	
	$session_data = $this->session->userdata('logged_in');
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	
	//$session_data['idno'] = 'MD-1800129';
	$studno = $session_data['idno'];    
	$studentinfo = $this->mod_main->isForEnrollment($session_data['idno']);
    
	$file='none';
	$filename='none';
	$regid = property_exists($studentinfo,'RegID')? $studentinfo->RegID : '';
	$ayterm = property_exists($studentinfo,'TermID')? $studentinfo->TermID : '';
	
   if($regid!='' && $regid!=' '){
	list($file,$filename)= $this->mod_crystalreports->prepare_cor($regid,$ayterm);
		
	if($file!='none' && $filename!='none'){
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print C.O.R.','-');
	}else{
	  echo 'Error Loading the report. Please Report to the administrators.';
	}
   }
   else { redirect('enrollment', 'refresh'); }
   
  }else { redirect('login', 'refresh'); }
}
 
}
?> 