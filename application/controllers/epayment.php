<?php if (!defined('BASEPATH')) die();
session_start();
Class Epayment extends CI_Controller
{
  function __construct(){
	parent::__construct();
	$this->load->model('mod_main','',TRUE);
	$this->load->model('mod_advising_sched','',TRUE);
	$this->load->model('mod_epayment','',TRUE);
  }
  
  function index($studno='')
  {
	$session_data = $this->session->userdata('logged_in');
    $this->mod_main->monitoring_action($session_data);
    if($this->session->userdata('logged_in'))		
	{
	 $data['title']       = "EPayment";
	 $data['userinfo']    = $session_data;
	 
	 if(is_key_exist($session_data,'idtype')==1)
	  $data['idno'] = $session_data['idno'];
	 else if(is_key_exist($session_data,'idtype')==-1)
	  $data['idno'] = $studno;
	 
	 $data['idtype']      = $session_data['idtype'];
	 $data['username']    = $session_data['username'];
	 $studentinfo         = $this->mod_main->getStudentRegInfo($data['idno']);
	 $fees                = $this->mod_advising_sched->getassessedfees(is_key_exist($studentinfo,'RegID'));
     $feedata             = $this->mod_epayment->generate_feetable($fees);
	 
	 $data['studentinfo'] = $studentinfo;
	 $data['fees']        = $feedata['content'];
	 $data['total']       = $feedata['total'];
	 $data['payment']     = $feedata['payment'];
	 $data['balance']     = $feedata['balance'];
	 $data['ondue']       = $feedata['duebalance'];
	 $data['schedule']    = $feedata['schedule'];
	 $data['pending']     = '';
	 
	 $footer['jslink'] = array();
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_epayment', $data);
     $this->load->view('include/footer',$footer);
	}
    else
     redirect('profile','refresh');		
  }
  
  function txn($type='',$mode='',$args='')
  {
	$xpost        = $this->input->post(NULL,TRUE);
	$xget         = $this->input->get(NULL,TRUE);
    $result       = array('success'=>false,'content'=>'','error'=>'');	
	$session_data = $this->session->userdata('logged_in');
	switch($type)
	{
	 case 'get':
	  switch($mode)
	  {
	    case 'paymentinfo':
		  $regid  = is_key_exist($xpost,'regid');
		  $sched  = is_key_exist($xpost,'sched');
		  $assess = $this->mod_epayment->get_billref($regid,$sched);
		  switch($args)
		  {
		   case '7Connect':
		   case 'Connect7':
		    $referenceURL   = 'http://testpay.7-eleven.com.ph/v1/reference';
			$transactionKey = '628e936f45884030ac1f34bcde9c28efa6ae9c839623b45b8942bd4490e1f05d';
			$merchantID     = ((defined('Connect7ID'))?Connect7ID:'testmerchant');
			$merchantRef    = is_key_exist($assess,'BillRefNo','1234567890');
			$expDate        = is_key_exist($assess,'ExpDate',date('Y').'0101235959'); 
			$amount         = is_key_exist($assess,'Amount','100.00');
			$successURL     = 'ienroll.sanbeda-alabang.edu.ph';
			$failURL        = 'ienroll.sanbeda-alabang.edu.ph';
			 //set variables
			$fields = array(
			  'merchantID'  => $merchantID,
			  'merchantRef' => $merchantRef,
			  'amount'      => $amount,
			  'expDate'     => $expDate,
			  'successURL'  => $successURL,
			  'failURL'     => $failURL,
			  'token'       => sha1($merchantID.$merchantRef.'{'.$transactionKey.'}'),
			  'email'       => is_key_exist($session_data,'email','jhe69samson@yahoo.com'),
			);
			 
			$params = http_build_query($fields);
			$options = array(
				'http' => array(
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'method'  => 'POST',
					'content' => $params,
				),
				'ssl'=>array(
					'verify_peer'      => false,
					'verify_peer_name' => false,
				),
			);
			 
			$context = stream_context_create($options);
			$data    = file_get_contents("$referenceURL?$params",false,$context);
			$cdata   = json_decode($data,true);
			if(is_key_exist($cdata,'payID')!=false)
			{	
		     $result['success'] = true;
			 $result['payID']   = $cdata['payID'];	
			 $result['payIDx']  = str_replace('-','',$cdata['payID']);	
			}
			else
             $result['error'] = 'Failed to get Data';				
		 
           break;		   
		  }
        break;
        case 'status':
		  $regid  = is_key_exist($xpost,'regid');
		  $sched  = is_key_exist($xpost,'sched');
		  $assess = $this->mod_epayment->get_billref($regid,$sched);
		  switch($args)
		  {
		   case '7Connect':	  
		   case 'Connect7':	  
		    $referenceURL   = 'http://testpay.7-eleven.com.ph/inquire';
			$transactionKey = '628e936f45884030ac1f34bcde9c28efa6ae9c839623b45b8942bd4490e1f05d';
			$merchantID     = ((defined('Connect7ID'))?Connect7ID:'testmerchant');
			$merchantRef    = is_key_exist($assess,'BillRefNo','1234567890');
			$expDate        = is_key_exist($assess,'ExpDate',date('Y').'0101235959'); 
			$amount         = is_key_exist($assess,'Amount','100.00');
			
			$fields = array(
			  'merchantID'  => $merchantID,
			  'merchantRef' => $merchantRef,
			  'token'       => sha1($merchantRef.'{'.$transactionKey.'}'),
			);
			 
			$params = http_build_query($fields);
			$options = array(
				'http' => array(
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'method'  => 'POST',
					'content' => $params,
				),
				'ssl'=>array(
					'verify_peer'      => false,
					'verify_peer_name' => false,
				),
			);
			 
			$context        = stream_context_create($options);
			$data           = file_get_contents("$referenceURL?$params",false,$context);
			$cdata          = json_decode($data,true);
			$result['Data'] = $cdata;
		   break;
		  }
        break;		
	  }
	  echo json_encode($result);
     break;	
	 case 'set':
	  switch($mode)
	  {
	    case 'process':
		 switch($args)
		 {
		   case '7Connect':
           case 'Connect7':
		     $transtype   = 'UNDEFINED';
		     $transKey    = 'UNDEFINED';
		     $merchantID  = ((defined('Connect7ID'))?Connect7ID:'testmerchant');
		     $fields = array(
				        'merchantID'   => $merchantID,
				        'merchantRef'  => "",
				        'amount'       => 0,
				        'authCode'     => '',
				        'responseCode' => 'UNDEFINED',
				        'responseDesc' => '',
				        'token'        => 'UNDEFINED');
			 if($xget)
			 {	 
		      $transtype             = is_key_exist($xget,'type',false);
			  $fields['merchantRef'] = is_key_exist($xget,'merchantRef','');
			  $fields['amount']      = is_key_exist($xget,'amount',0);
			  $token                 = is_key_exist($xget,'token',''); 
			  $transKey              = '628e936f45884030ac1f34bcde9c28efa6ae9c839623b45b8942bd4490e1f05d';
			  $validtoken            = sha1($transtype.$fields['merchantID'].$fields['merchantRef'].'{'.$transKey.'}');
			  if($token!=$validtoken)
			  {
			   $fields['authCode']     = "";
			   $fields['responseCode'] = "DECLINED";
			   $fields['responseDesc'] = "Invalid token";
			  }
              else
              {
			   $fields['authCode']     = "1111";
			   $fields['responseCode'] = "SUCCESS";
			   $fields['responseDesc'] = "";
			   switch($transtype) 
			   {
				case "VALIDATE":
				// Check if merchantRef is still valid
				break;
				case "CONFIRM":
				// Update the paid status of the table
				break;
				case "VOID":
				// Update the paid status of the table
				break;
				default:
			     $fields['authCode']     = "";
			     $fields['responseCode'] = "DECLINED";
			     $fields['responseDesc'] = "Unknown transaction type";
			   }
			  }		
             }else{
			   $fields['authCode']     = "";
			   $fields['responseCode'] = "DECLINED";
			   $fields['responseDesc'] = "Invalid params";
			 }
             $fields['token'] = sha1($transtype.$fields['merchantID'].$fields['merchantRef'].$fields['authCode'].$fields['responseCode'].'{'.$transKey.'}');		 
			 $params = http_build_query($fields);
			 
			 //write logfile
			 $root_path = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']);
			 $myFile = $root_path."tmp/7-CONNECT.log";
			 //die($myFile);
			 $fh = fopen($myFile, 'w+') or exit();
			 fwrite($fh, date('Y-m-d H:i ') . $params . "\n");
			 fclose($fh);	 
			 
			 //return result
			 die("?".$params);
		   break;		   
		 }
        break;		
	  }
	  echo json_encode($result);
     break;	 
	}
  }  
}
?>

