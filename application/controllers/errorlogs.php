<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Errorlogs extends CI_Controller 
{

 function __construct()
 {
   parent::__construct();
   date_default_timezone_set("Asia/Taipei");
   $this->load->model('mod_main','',TRUE);	
   $this->db->db_debug=false;
 }
 
 function index($date='')
 {
  $session_data = $this->session->userdata('logged_in');
  $this->mod_main->monitoring_action($session_data,'$error_log');
	 
  $data['userinfo'] = $session_data;
  $data['title'] = "Admin Controls";
  $data['title1'] = "Error Log";
  $data['idno'] = $session_data['idno'];
  $data['username'] = $session_data['username'];
  
  $data['note']=1;
  if($date=='')
  {$date = date('Y-m-d');}
  else
  {
  $data['note']=0;
  $date = date('Y-m-d',strtotime($date));
  }
  //$date = '2014-09-10';
  $data['errors'] = $this->geterror($date);
  
  $this->load->view('include/header',$data);
  $this->load->view('templates/mainmenu',$data);
  $this->load->view('vw_errorlog',$data);
  $this->load->view('include/footer');
  
 }
 
 function geterror($date='')
 {
  $xdir =str_replace('index.php','',$_SERVER['SCRIPT_FILENAME']); 
  $data['errors']='<tr><td colspan="3">Nothing to Display</td></tr>';
  if(isset($date) && $date!='')
  {
   if(file_exists($xdir.APPPATH.'logs/log-'.$date.'.php'))
   {
   $data['errors'] = $this->load->file($xdir.APPPATH.'/logs/log-'.$date.'.php',true);
   $errors = explode('ERROR <-> ',$data['errors']);
   
   foreach($errors as $key => $err_str)
   {
    if(trim($err_str)=='')
     unset($errors[$key]);
	else
    {
     $err_str = str_replace(' <-> ','</td><td>',$err_str);
     $errors[$key] = '<tr><td>'.$err_str.'</td></tr>';
    }
   }
   
   krsort($errors);
   $data['errors'] = implode($errors);
   }
  }
  
  return $data['errors'];
 }

}
?>