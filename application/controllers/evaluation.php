
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Evaluation extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   date_default_timezone_set("Asia/Taipei");
   $this->load->model('mod_evaluation','',true);
   $this->load->model('mod_main','',true);
   $this->load->model('mod_crystalreports','',true);
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $this->mod_main->monitoring_action($session_data,'evaluation');
	 
     $data['userinfo'] = $session_data;
     $data['title'] = "Student Evaluation";
	 $data['username'] = $session_data['username'];
	 
	 $idtype = $session_data['idtype'];
	 if($idtype==1)
	 {
	  $data['studentno'] = $session_data['idno'];
      $username = $session_data['id'];
     }
	 elseif($idtype==2)
	 {
	  $studentno = StripSlashes($this->input->get('studentno'));
      $username = $session_data['id'];
      $data['studentnolist'] = $this->mod_main->ListofParentChildwEval($username);
	  
	  if(count($data['studentnolist'])<=0)
		redirect('profile','refresh');
	  else
	    $data['studentno'] = (($studentno == '' && count($data['studentnolist'])>=1)?($data['studentnolist'][0]->StudentNo):$studentno);
	
	 }
	 elseif($idtype=='-1')
	 {
	  $data['studentfilter'] = true;	 
	  $data['studentno'] = 0;
	 }
	 
	 $data['xdetail']   =  $this->mod_main->call_xtraDetails($data['studentno']);
     $data['progid']    = (($data['studentno']!=0)?($data['xdetail']->ProgID):0);
     $data['progclass'] = (($data['studentno']!=0)?($data['xdetail']->ProgClass):30);
	 $arr_data          = $this->mod_evaluation->read_TexttoEval($data['studentno']);
	 
	 if($data['progclass']>=30 && ($data['progid']>0 && $data['progid']!=29))
	 {	 
	  if($arr_data && is_object($arr_data) && @property_exists($arr_data,'content') && @property_exists($arr_data,'summary'))
	  {
	   $rs = $arr_data->content;
	   $summary = $arr_data->summary;
	   $data['status'] = 'success';
	  }
      else 
      {		 
	   $this->db->reconnect();
	   $eval = $this->mod_evaluation->studAcadEvaluation($data['studentno']);
       $this->db->reconnect();
	   $row = $this->mod_evaluation->fetch_row($data['studentno']);
	   $data['status'] = (($row && $row!='' && $row!=false)? 'success' : 'failed');
	  
	   list($rs,$summary) =  $this->mod_evaluation->generate_table($row);
	   $this->mod_evaluation->save_EvaltoText($data['studentno'],json_encode(array('content'=>$rs,'summary'=>$summary)));  
	  }
	 }
	 else
	 {
	  $rs = '<div class="alert alert-danger"><i class="fa fa-warning"></i> Evaluation is not yet available for your program.</div>';
      $summary = '';	  
	 }	 
	 
	 $data['rs']=$rs;
	 $data['summary']=$summary;
	 $data['lgnd'] =  $this->mod_evaluation->generate_legend();
	 $data['jslink'] = array('utilities/evaluator.js');
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_evaluation', $data);
     $this->load->view('include/footer', $data);
	 
	 $this->mod_main->Translog($session_data['id'],'View Evaluation','-');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function reevaluate($idno='',$reevaluate=0)
 {
  $output = array('result'=>false,'content'=>'','summary'=>'','xtra'=>''); 
  $row = false;
  if($this->session->userdata('logged_in'))
  {
   $session_data = $this->session->userdata('logged_in');
   $data['username'] = $session_data['username'];
   $idtype = $session_data['idtype'];
   
   if($idtype==1)
	$data['studentno'] = $session_data['idno'];
   elseif($idtype==2 || $idtype=='-1')
   {
	$data['studentno'] = StripSlashes($idno);
	$data['info_request']=true;
   }  
   
   if($reevaluate==1)
   {
	$this->db->reconnect();
	$result           = $this->mod_evaluation->studAcadEvaluation($data['studentno']);
	$output['prereq'] = '';
    if($result)
    {
	 $this->db->close();
	 $this->db->reconnect();	
	 $data['xdetail'] = $this->mod_main->call_xtraDetails($data['studentno']);
	 $data['progclass'] = ((@property_exists($data['xdetail'],'ProgClass'))? ($data['xdetail']->ProgClass) : 50);
	 $output['xtra'] = $this->load->view('include/studentinfo',$data,true);
	 $this->db->close();	
	 $this->db->reconnect();	
	 $row = $this->mod_evaluation->fetch_row($data['studentno']);
	}
   }
   else
   {
	$this->db->reconnect();	
	$data['xdetail'] = $this->mod_main->call_xtraDetails($data['studentno']);
	$data['progclass'] = ((@property_exists($data['xdetail'],'ProgClass'))? ($data['xdetail']->ProgClass) : 50);
	$output['xtra'] = $this->load->view('include/studentinfo',$data,true);
	$arr_data = $this->mod_evaluation->read_TexttoEval($data['studentno']);
	 
	if($arr_data && is_object($arr_data) && @property_exists($arr_data,'content') && @property_exists($arr_data,'summary'))
	{
	  $output['content'] = $arr_data->content;
	  $output['summary'] = $arr_data->summary;
	  $row ='filedata';
	}
	else
	{	
	  $this->db->reconnect();
	  $eval = $this->mod_evaluation->studAcadEvaluation($data['studentno']);
	  $this->db->close();	
	  $this->db->reconnect();	
	  $row = $this->mod_evaluation->fetch_row($data['studentno']);   	 
      $output['fromfile'] = false; 
	}
   }	
   
   if($row && $row!='' && $row!=false)
   {
	 if($row=='filedata')
	  $output['fromfile']=true;	
	 else
	 {
	  list($output['content'],$output['summary']) =  $this->mod_evaluation->generate_table($row);
	  $this->mod_evaluation->save_EvaltoText($idno,json_encode($output));  
	 }	 
	 
	 $output['result']=true;
   }
  }
  
  echo json_encode($output);
}

function printdc($idno=''){
 //crystal report version
  if($this->session->userdata('logged_in')) {
    $session_data = $this->session->userdata('logged_in');
    $this->mod_main->monitoring_action($session_data,'evaluation');
	$idtype = $session_data['idtype'];
	
    if($idtype==1)
	  $studno = $session_data['idno'];
    elseif($idtype==2 || $idtype='-1')
	  $studno = StripSlashes($idno);
    
	$file='none';
	$filename='none';
	
   if($studno!='' && $studno!=' ')
   {
	list($file,$filename)= $this->mod_crystalreports->prepare_deficient($studno);
		
	if($file!='none' && $filename!='none')
	{
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print Evaluation Result','-');
	}
	else
	{echo 'Error Loading the report. Please Report to the administrators.';}
   }
   else { redirect('evaluation', 'refresh'); }
   
  }else { redirect('login', 'refresh'); }
 
 }
 function trial()
 {
  echo '<pre>';
  print_r($this->mod_evaluation->exec_Evaluation('2011300665',true)); 
 }
 

}
?>

