<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Faculty extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_faculty','',TRUE);
   $this->load->model('mod_crystalreports','',TRUE);
   $this->load->model('mod_vbscripts','',TRUE);
   $this->load->library('zip');
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
	 $this->mod_main->monitoring_action($session_data,'faculty');
	 if ($session_data['idtype']!= 3 && $session_data['idtype']!='-1'){redirect('login', 'refresh');}
	 
     $data['userinfo'] = $session_data;     
     $data['title'] = "Faculty";
     $data['username'] = $session_data['username'];
     $idtype = $session_data['idtype'];
	 
	 $data['btnPrint']=0;
	 $data['btnSubPrint']=0;
	 $data['xViewOther'] = 0;
	 $data['xPostSubmission'] = 0;
	 $data['xPostOther'] = 0;
	 $data['xEncodeOther'] = 0;
	 $data['xUnpost'] = 0;
	 $data['xSinglePost'] = 0;
	 $data['xMultiTab'] = 0;
	 
	 $data['employeeid'] = $session_data['idno'];
	 $priviledge = $this->mod_faculty->get_priviledges($data['employeeid']);
	 //print_r($priviledge);
	 //die();
	 foreach($priviledge as $rs)
	 {
	  $data['xViewOther'] = property_exists($rs,'AllowtoViewOther')? $rs->AllowtoViewOther : 0;
	  $data['xPostSubmission'] = property_exists($rs,'AllowPostSubmission')? $rs->AllowPostSubmission : 0;
	  $data['xPostOther'] = property_exists($rs,'AllowPostOther')? $rs->AllowPostOther : 0; 
	  $data['xEncodeOther'] = property_exists($rs,'AllowEncodeOther')? $rs->AllowEncodeOther : 0;
	  $data['xUnpost'] = property_exists($rs,'AllowUnpost')? $rs->AllowUnpost : 0;
	  $data['xSinglePost'] = property_exists($rs,'SinglePosting')? $rs->SinglePosting : 0;
	  $data['btnPrint'] = property_exists($rs,'SinglePrinting')? $rs->SinglePrinting : 0;
	  $data['btnSubPrint'] = property_exists($rs,'SubmitPrint')? $rs->SubmitPrint : 0;
	  $data['xMultiTab'] = property_exists($rs,'MultiTab')? $rs->MultiTab : 0;
	  
	  $data['xUplBefPost'] = property_exists($rs,'UplBeforePost')? $rs->UplBeforePost : 0;
	  $data['xOverwrite'] = property_exists($rs,'OverwriteUpl')? $rs->OverwriteUpl : 0;
	  
	  $data['xPreDesc'] = property_exists($rs,'PreDesc')? $rs->PreDesc : '';
	  $data['xPGComp'] = property_exists($rs,'PGComputation')? $rs->PGComputation : '';
	  $data['xPTGComp'] = property_exists($rs,'PTGComputation')? $rs->PTGComputation : '';
	  $data['xPrelimComp'] = property_exists($rs,'PrelimComputation')? $rs->PrelimComputation : '';
	  
	  $data['xMidDesc'] = property_exists($rs,'MidDesc')? $rs->MidDesc : '';
	  $data['xMGComp'] = property_exists($rs,'MGComputation')? $rs->MGComputation : '';
	  $data['xMTGComp'] = property_exists($rs,'MTGComputation')? $rs->MTGComputation : '';
	  $data['xMidtermComp'] = property_exists($rs,'MidtermComputation')? $rs->MidtermComputation : '';
	  
	  $data['xPreFinDesc'] = property_exists($rs,'PreFinDesc')? $rs->PreFinDesc : '';
	  $data['xPFGComp'] = property_exists($rs,'PFGComputation')? $rs->PFGComputation : '';
	  $data['xPFTGComp'] = property_exists($rs,'PFTGComputation')? $rs->PFTGComputation : '';
	  $data['xPreFinComp'] = property_exists($rs,'PreFinComputation')? $rs->PreFinComputation : '';
	  
	  $data['xFinDesc'] = property_exists($rs,'FinDesc')? $rs->FinDesc : '';
	  $data['xFGComp'] = property_exists($rs,'FGComputation')? $rs->FGComputation : '';
	  $data['xFTGComp'] = property_exists($rs,'FTGComputation')? $rs->FTGComputation : '';
	  $data['xFinComp'] = property_exists($rs,'FinalComputation')? $rs->FinalComputation : '';
	 }
	 
	 $data['ayterm']= $this->mod_faculty->get_listAYTERM($data['employeeid']);
	 $data['gradesys']= $this->mod_faculty->generate_gradingsystem(50);
	 $data['gradetrans']= $this->mod_faculty->generate_transmutation();
	 
	 $campus = $this->mod_main->get_campuslist(1);
	 $data['campus']=$this->mod_main->generate_campus($campus);
	 /*
	 echo '<pre>';
	 print_r($campus);
	 die();
	 */
	 
	 $data['jslink'] = array('utilities/faculty.js');
	 
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_faculty', $data);
     $this->load->view('include/footer',$data);
	 $this->mod_main->Translog($session_data['id'],'View Faculty Modules','-');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function classsched()
 {
   if(!$this->session->userdata('logged_in')){redirect('login', 'refresh');}
     
   $session_data = $this->session->userdata('logged_in');
   if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
   if($session_data['idtype']!= 3 && $session_data['idtype']!='-1'){redirect('login', 'refresh');}
   
   $data['userinfo'] = $session_data;
   $idtype = $session_data['idtype'];
   
   $xterm='';
   $xcamp=1;
   $xfac='';
   $xmine=1;
   
   if($this->input->post('termid'))
   {$xterm=str_replace("'","",$this->input->post('termid'));}
   if($this->input->post('campusid'))
   {$xcamp=str_replace("'","",$this->input->post('campusid'));}
   if($this->input->post('facultyid'))
   {$xfac=str_replace("'","",$this->input->post('facultyid'));}
   if($this->input->post('mineonly'))
   {$xmine=str_replace("'","",$this->input->post('mineonly'));}
   
   if($xterm!='' && $xfac!='')
   {
    list($row,$stat) = $this->mod_faculty->generate_classSched($session_data['idno'],$xcamp,$xterm,$xmine);
    echo $row.'[=,=]'.$stat;
   }
   else
   {
     $xrow = '<div class="alert alert-block alert-danger">
	            <a class="close" data-dismiss="alert" href="#">'.utf8_encode('×').'</a>
				<h4 class="alert-heading">Alert!</h4>
				<p>
				There is an error in accessing the data.
				</p>
			 </div>';
     $xstat = '<table class="table table-striped">
					  <tr><td><small>Total Subject(s):</small></td><td>0</td></tr>
					  <tr><td><small>Total Lecture Unit(s):</small></td><td>0</td></tr>
					  <tr><td><small>Total Lab Unit(s):</small></td><td>0</td></tr>
					  <tr><td><small>Total Lecture Hour(s):</small></td><td>0</td></tr>
					  <tr><td><small>Total Lab Hour(s):</small></td><td>0</td></tr>
			 </table>';
	echo $xrow.'[=,=]'.$xstat;
   }
 }
 
 function classgrade()
 {
   if(!$this->session->userdata('logged_in'))
   {redirect('login', 'refresh');}
     
   $session_data = $this->session->userdata('logged_in');
   if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
   if($session_data['idtype']!= 3 && $session_data['idtype']!='-1'){redirect('login', 'refresh');}
   
   $data['userinfo'] = $session_data;
   $idtype = $session_data['idtype'];
   $xschd='';
   $xid=0;
   
   if($this->input->post('schedid'))
   {$xschd=str_replace("'","",$this->input->post('schedid'));}
   
   if($this->input->post('xid'))
   {$xid=str_replace("'","",$this->input->post('xid'));}
   
   if($xschd!='')
   {
    $imgpath = 'assets/img/id pics/';
    //$export = $this->mod_vbscripts->ExportImageBySched($imgpath,$xschd);
    //echo $export;
    //die();	
    $row = $this->mod_faculty->generate_classGrade($xid,$xschd);
	$gsheet = $this->mod_faculty->sp_facultyportal('0.6',0,0,$xschd);
    
	$filename = $gsheet[0]->FileName;
    $row = str_replace('data-hardfile=""','data-hardfile="'.$filename.'"',$row);
	 
    echo $row;
   }
   else
   {
   echo '<div class="alert alert-block alert-danger">
	        <a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading">Alert!</h4>
			<p>
			There is an error in accessing the data.
			</p>
	    </div>';
   }
 }
 
 function manage($opt=0)
 {
  $output = 'nodata';
  
  if(!$this->session->userdata('logged_in'))
  {echo $output;}
  else
  {  
   $session_data = $this->session->userdata('logged_in');
  
   if($opt==1)
   {
    if($this->input->post())
    {
     $xpost   = $this->input->post();
     $xopt    = array_key_exists('opt',$xpost)? str_replace("'","",$xpost['opt']) : '';
     $xsched  = array_key_exists('sched',$xpost)? str_replace("'","",$xpost['sched']) : '';
     $xstudno = array_key_exists('stdno',$xpost)? str_replace("'","",$xpost['stdno']) : '';
     $xemp    = array_key_exists('idno',$session_data)? $session_data['idno'] : '';
	 $xcs     = array_key_exists('cs',$xpost)? str_replace("'","",$xpost['cs']) : '0';
     $xexam   = array_key_exists('exam',$xpost)? str_replace("'","",$xpost['exam']) : '0';
     $xgrade  = array_key_exists('grade',$xpost)? str_replace("'","",$xpost['grade']) : '0';
     $xtgrade = array_key_exists('tgrade',$xpost)? str_replace("'","",$xpost['tgrade']) : '0';
     $xfinal  = array_key_exists('final',$xpost)? str_replace("'","",$xpost['final']) : '';
     $xrex    = array_key_exists('rex',$xpost)? str_replace("'","",$xpost['rex']) : '';
     $xremark = array_key_exists('remark',$xpost)? str_replace("'","",$xpost['remark']) : '';
     $xnoabs  = array_key_exists('noabs',$xpost)? str_replace("'","",$xpost['noabs']) : '';	
	
	 if($xsched!='' and $xstudno!='')
	 {
	  $result = $this->mod_faculty->sp_gradeencoding($xsched,$xopt,$xemp,$xstudno,$xcs,$xexam,$xgrade,$xtgrade,$xfinal,$xrex,$xremark,$xnoabs);
	  $output='success';
	 }
    $this->mod_main->Translog($session_data['id'],'Save Grades','SCHED:'.$xsched.', STDNO:'.$xstudno.', CS:'.$xcs.', EXAM:'.$xexam.', GRADE:'.$xgrade.'FINAL:'.$xfinal);
    }
   }elseif($opt==2 || $opt==3){
    if($this->input->post())
    {
     $xpost = $this->input->post();
     $xopt = array_key_exists('opt',$xpost)? str_replace("'","",$xpost['opt']) : '';
     $xsched = array_key_exists('sched',$xpost)? str_replace("'","",$xpost['sched']) : '';
     $xemp = $session_data['idno'];
	 
	 if($xsched!='' and $xopt!='')
	 {
	  $result = $this->mod_faculty->sp_gradeencoding($xsched,$xopt,$xemp);
	  $output='success';
	 }
	if($opt==2) 
     $this->mod_main->Translog($session_data['id'],'Post Grades sheet','SCHED:'.$xsched);
    else if($opt==3)
     $this->mod_main->Translog($session_data['id'],'Unpost Grade sheet','SCHED:'.$xsched);	
	}
   }elseif($opt==4){
    if($this->input->post())
    {
     $xpost = $this->input->post();
     $xidno = array_key_exists('idno',$xpost)? str_replace("'","",$xpost['idno']) : '';
	 
	 if($xidno!='' and $xidno!=' ')
	 {
	  $imgpath = 'assets/img/id pics/'.$xidno;
	  $img = $this->mod_main->CheckImage($imgpath);
	  $output = $this->mod_main->SetProfile($img);
	 }
    }
   }  
 
   echo $output;
  }
 
 }
 
  function printgradesheets($xschedid='',$xfacultyid='',$opt=0){
 //crystal report version
  if($this->session->userdata('logged_in')) {
    if(!extension_loaded('com_dotnet'))
    {
     echo 'error loading extension';
    }
    else
    {
    $session_data = $this->session->userdata('logged_in');
	
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	if (isset($this->AdvLock) and $this->AdvLock==0){ redirect('advising', 'refresh'); } 
	
    $empid = $session_data['idno'];    
	
	if($xschedid!='' && $xfacultyid!='' && $empid!='')
	{
	 $file='none';
	 $filename='none';
	
	 if($xschedid!='' && $xfacultyid!='' && $empid!='' && $opt>=0)
	 {
	  list($file,$filename)= $this->mod_crystalreports->prepare_gradesheet($xschedid,$xfacultyid,$empid,$opt);
	 }
	 else
	 {
	  $opt = ($opt * -1)-1;
	  list($file,$filename)= $this->mod_crystalreports->prepare_gradesheet_bulk($xschedid,$xfacultyid,$empid,$opt);
	 }
    	
	 if(($file!='none' && $filename!='none') && ($file!='none1' && $filename!='none1'))
	 {
	  //echo $file;
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print GradeSheet','-');
     /*
	 */	  
	 }
	 elseif($file=='none' && $filename=='none')
	 {echo 'Error Loading the report. Please Report to the administrators.';}
	 elseif($file=='none1' && $filename=='none1')
	 {echo 'Error Loading the component. Please Report to the administrators.';}
    }
	else { redirect('faculty', 'refresh'); }
	}
  }else { redirect('login', 'refresh'); }

 }
 
 function printsubmitmonitor($xtermid='',$xcampid='')
 {
 //crystal report version
  if($this->session->userdata('logged_in')) {
    if(!extension_loaded('com_dotnet'))
    {
     echo 'error loading extension';
    }
    else
    {
    $session_data = $this->session->userdata('logged_in');
	
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	if (isset($this->AdvLock) and $this->AdvLock==0){ redirect('advising', 'refresh'); } 
	
    $empid = $session_data['idno'];    
	
	if($xtermid!='' && $xcampid!='' && $empid!='')
	{
	 $file='none';
	 $filename='none';
	
	 if($xtermid!='' && $xcampid!='' && $empid!='')
	 {
	  list($file,$filename)= $this->mod_crystalreports->prepare_submitmonitor($xtermid,$xcampid,'',$empid);
	 }
	 	
	 if(($file!='none' && $filename!='none') && ($file!='none1' && $filename!='none1'))
	 {
	  //echo $file;
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print Monitoring of Grades Submission','-');
     /*
	 */	  
	 }
	 elseif($file=='none' && $filename=='none')
	 {echo 'Error Loading the report. Please Report to the administrators.';}
	 elseif($file=='none1' && $filename=='none1')
	 {echo 'Error Loading the component. Please Report to the administrators.';}
    }
	else { redirect('faculty', 'refresh'); }
	}
  }else { redirect('login', 'refresh'); }

 }
 
 function uploadgradesheet()
 {
  if(($this->session->userdata('logged_in')) && ($this->input->post()))
  {
   $xpost = $this->input->post();
   $session_data = $this->session->userdata('logged_in');
   $uname = $session_data['id'];
   $idno = $session_data['idno'];
   
   $uschedid = array_key_exists('uschedid',$xpost)? str_replace("'","",$xpost['uschedid']):'';
   $ufacid   = array_key_exists('ufacid',$xpost)? str_replace("'","",$xpost['ufacid']):'';
   $usecid   = array_key_exists('usecid',$xpost)? str_replace("'","",$xpost['usecid']):'';
   
   $uterm   = array_key_exists('usheetterm',$xpost)? str_replace("'","",$xpost['usheetterm']):'';
   
   $xcontent = file_get_contents($_FILES['ugradesheet']['tmp_name']);
   
   $config['upload_path'] = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'uploads/gradesheets/';
   $config['allowed_types'] = 'xls|xlsx|doc|docx|pdf';
   //$config['max_size']   = '100';
   //$config['max_width']  = '500';
   //$config['max_height'] = '500';

   $this->load->library('upload', $config);
   $field_name = "ugradesheet";

   if (!$this->upload->do_upload($field_name))
   {
    $error = $this->upload->display_errors();
    $this->session->set_userdata('error_data', $error);
    $this->mod_main->Translog($uname,'Upload GradeSheet Failed','-');
	redirect('faculty/promptpage/2','refresh');
   }
   else
   {
    $data = $this->upload->data();
	
	$tbl = 'sis_GradeSheetFiles';
	$fullpath = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']); //assets/img/profile/'.md5($uname.$stdno);
    $filepath = 'uploads/gradesheets/'.$uschedid.'-'.date("YmdHis").$data['file_ext'];
    $filename = $uschedid.'-'.date("YmdHis").$data['file_ext'];
	
	$compresspath='uploads/tmp_zip/'.$uschedid.'-'.date("YmdHis").'.zip';
   
    $file = $this->mod_main->CheckFile($filepath);
    if($file !='nofile' && $uschedid!="" && $ufacid!="" && $usecid!="")
    {unlink($file);}
   
    rename($data['full_path'],$fullpath.$filepath);
	
	$result  = $this->mod_faculty->uploadfile($ufacid,$usecid,$uschedid,$filename,'',$uterm,$idno);
	
	if($result)
	{
	 $this->zip->read_file(($fullpath.$filepath)); 
	 $this->zip->archive($fullpath.$compresspath);
	}
	
	if(file_exists($fullpath.$compresspath))
	{
	 $success = $this->mod_vbscripts->ImportFile($compresspath,$filename,$idno,$tbl);
	 //$success = $this->mod_vbscripts->ImportFile($filepath,$filename,$idno,$tbl);
	 //unlink($fullpath.$compresspath);
	}
	
	if($success)
	{
     $this->mod_main->Translog($uname,'Upload GradeSheet Success','-');
	 redirect('faculty/promptpage/1','refresh');
    }
	else
	{
	 $this->mod_main->Translog($uname,'Upload GradeSheet Failed','-');
	 redirect('faculty/promptpage/2','refresh');	
	}
   }	
  }
  redirect('faculty', 'refresh');
 }
  
 function exportgradesheet($xid=0,$ext='')
 {
  //print_r($this->input->post());
  //print_r($_FILES['ugradesheet']);
  //die();
  $exportpath = 'downloads/exports/'.date("YmdHis").'.'.$ext;
  
  $tbl = "sis_GradeSheetFiles";
  $col = "Data";
  $con = "EntryID";
  
  if(($this->session->userdata('logged_in')) && ($xid!=0))
  {
   $success = $this->mod_vbscripts->ExportFile($exportpath,$xid,$tbl,$col,$con);
   // $success = $this->mod_vbscripts->ImportFile($filepath,$filename,$idno,$tbl);
   // unlink($fullpath.$compresspath);
  }
  else
  {
  echo 'Error';
  die();
  }
  redirect('faculty', 'refresh');
 } 
 
 function promptpage($x=0)
 {
   if($x<=0)
   {redirect('faculty','refresh');}
   
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
	 if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 
     $data['userinfo'] = $session_data;     
     $data['title'] = "Faculty";
     $data['username'] = $session_data['username'];
     $idtype = $session_data['idtype'];
	 if($idtype!= 3)
	 {
	  redirect('login', 'refresh');
	 }
     
	 $data['ribbonpage'] ='Faculty Portal';
	 $data['promptmsg'] ='<div class="well" style="alignment-adjust: central;">
                           <p><a href="'.site_url('faculty').'" class="btn btn-info btn-sm" role="button">Back to Faculty Portal</a></p>
                          </div>';
	 if($x==1)
	 {
	 $data['promptmsg'] ='<div class="well" style="alignment-adjust: central;">
                           <h1>Success</h1>
                           <p>Your grade sheet was successfully uploaded.</p>
                           <p><a href="'.site_url('faculty').'" class="btn btn-info btn-sm" role="button">Back to Faculty Portal</a></p>
                          </div>';
	 }
	 elseif($x==2)
	 {
	 $data['promptmsg'] ='<div class="well" style="alignment-adjust: central;">
                           <h1>Failed</h1>
                           <p>Uploading your grade sheets failed.</p>
                           <p><a href="'.site_url('faculty').'" class="btn btn-danger btn-sm" role="button">Back to Faculty Portal</a></p>
                          </div>';
	 }
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('templates/promptpage',$data);
     $this->load->view('include/footer',$data);
   }
   else
   {
    redirect('faculty', 'refresh');
   }  
 
 }
 
 function trial($x)
 {
   $session_data = $this->session->userdata('logged_in');
   $data['userinfo'] = $session_data;     
   $idno = $session_data['idno'];
 
   $gsheet = $this->mod_faculty->sp_facultyportal('0.6',0,0,$x);
   if($gsheet)
    echo $gsheet[0]->FileName;
 }
}
?>