<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class faculty_evaluation extends CI_Controller {

 function __construct(){
   parent::__construct();
   
   date_default_timezone_set("Asia/Manila");
   $this->load->model('mod_grades','',true);
   $this->load->model('mod_main','',true);
   $this->load->model('mod_crystalreports','',true);
   $this->EnableGrades = $this->mod_main->getconfigvalue('GradesModuleLock');
   $this->WithChinese = ((defined('WITH_CHINESE') && WITH_CHINESE==1)?true:false);
   if(strtotime($this->EnableGrades))
    $this->EnableGrades = (($this->EnableGrades<=date('m/d/Y')) ? 1 : 0);   
   else
    $this->EnableGrades = (($this->EnableGrades=="1" || strtolower($this->EnableGrades)=="yes" || strtolower($this->EnableGrades)=="true") ? 1 : 0);   
 }

 function index(){
	if(!$this->session->userdata('logged_in')){
      //If no session, redirect to login page
      redirect('login', 'refresh');
	}
   
    $studentno = StripSlashes($this->input->get('studentno'));
    
	$session_data = $this->session->userdata('logged_in');
	$this->mod_main->monitoring_action($session_data,'grades');
	$data['disabled'] = ($this->EnableGrades==1)?0:1;
	$data['title'] = "Faculty Evaluation";
	$data['userinfo'] = $session_data;

	$username = $session_data['id'];
	$data['username'] = $session_data['username'];
	$data['idtype'] = $session_data['idtype'];
	$data['studentno'] = $session_data['idno'];
	$data['studentnolist'] = $this->mod_main->ListofParentChild($username);

	if ($data['idtype'] == 2)
	$data['studentno'] = $data['studentnolist'][0]->StudentNo;
	else if ($data['idtype'] == '-1')
	{
	$data['studentfilter']=1;	 
	$data['studentno'] = 0;
	}

	$regid             = $this->db->query("SELECT TOP 1 r.RegID FROM ES_Registrations as r INNER JOIN ES_Grades as g ON r.StudentNo=g.StudentNo AND r.TermID=g.TermID WHERE r.StudentNo='".$data['studentno']."' AND r.TermID=4 AND g.Final<>'DRP' ORDER BY GradeIDX DESC")->result();
     $data['regid']     = (($regid && count($regid)>0)?($regid[0]->RegID):0);
	$data['faculty']   = $this->db->query("SELECT rd.RegID, cs.ScheduleID,cs.SubjectID,s.SubjectCode,s.SubjectTitle,e.EmployeeID,e.LastName,e.FirstName,eval.EntryID,eval.TemplateID,eval.FinalRating,eval.EvaluationDate,eval.DateSubmitted,eval.Comment
											 FROM ES_RegistrationDetails as rd 
									INNER JOIN ES_ClassSchedules as cs  ON rd.ScheduleID=cs.ScheduleID 
									INNER JOIN ES_Subjects as s ON cs.SubjectID=s.SubjectID
									INNER JOIN HR_Employees as e ON cs.FacultyID=e.EmployeeID
									LEFT JOIN ES_FacultyEvaluation as eval ON rd.ScheduleID=eval.SchedID AND cs.FacultyID=eval.FacultyID AND eval.StudentNo='".$data['studentno']."'
											WHERE rd.RegID='".$data['regid']."' AND rd.RegTagID<>3 AND cs.FacultyID<>''")->result();
	$data['eval']      = $this->db->query("SELECT * FROM ES_FacultyEvaluation_Templatedetails WHERE TemplateID=1")->result();										
	
 	//$this->load->view('include/header',$data);
	//$this->load->view('templates/mainmenu',$data);
	$this->load->view('faculty-evaluation/index',['data' => $data ] );
	$mfooter['jslink'] = array('utilities/facultyevaluate.js');     
	$this->load->view('include/footer',$mfooter);
	$this->mod_main->Translog($session_data['id'],'View Grades','-');
   
 }
 
 function mgmt($type='get',$param=false){
	 $session_data = $this->session->userdata('logged_in');
	 $output  = array('success'=>false,'message'=>'','error'=>'Invalid Parameters');
     $data    = array();
	 $xpost   = $this->input->post();
	 switch($type){
		case 'get':
		   $data['studentno'] = $session_data['idno'];
		   switch($param){
			 case 'list':
			    $regid             = $this->db->query("SELECT TOP 1 r.RegID FROM ES_Registrations as r INNER JOIN ES_Grades as g ON r.StudentNo=g.StudentNo AND r.TermID=g.TermID WHERE r.StudentNo='".$data['studentno']."' AND r.TermID=4 AND g.Final<>'DRP' ORDER BY GradeIDX DESC")->result();
                $data['regid']     = (($regid && count($regid)>0)?($regid[0]->RegID):0);
	            $data['faculty']   = $this->db->query("SELECT rd.RegID, cs.ScheduleID
				                                             ,cs.SubjectID,s.SubjectCode
															 ,s.SubjectTitle,e.EmployeeID
															 ,e.LastName,e.FirstName
															 ,eval.EntryID,eval.TemplateID
															 ,eval.FinalRating,eval.EvaluationDate,eval.DateSubmitted,eval.Comment
											 FROM ES_RegistrationDetails as rd 
									INNER JOIN ES_ClassSchedules as cs  ON rd.ScheduleID=cs.ScheduleID 
									INNER JOIN ES_Subjects as s ON cs.SubjectID=s.SubjectID
									INNER JOIN HR_Employees as e ON cs.FacultyID=e.EmployeeID
									LEFT JOIN ES_FacultyEvaluation as eval ON rd.ScheduleID=eval.SchedID 
									                                      AND cs.FacultyID=eval.FacultyID AND eval.StudentNo='".$data['studentno']."'
											WHERE rd.RegID='".$data['regid']."'  AND rd.RegTagID<>3 AND cs.FacultyID<>''")->result();
											
				
				$output['success'] = true;
                $output['content'] = $this->load->view('faculty-evaluation/list',$data,true);				
             break;			
             case 'eval':
			    $xid          = array_key_exists('xid',$xpost)? str_replace("'","",$xpost['xid']) : '';
			    if($xid==false || $xid=='new'){
			    $data['eval'] = $this->db->query("SELECT * FROM ES_FacultyEvaluation_Templatedetails WHERE TemplateID=1 ORDER BY CategoryID,IndexID")->result();
                                $output['comment'] = '';
				}else{
				$data['eval'] = $this->db->query("SELECT  td.*,fe.Comment,sc.Score FROM ES_FacultyEvaluation_Templatedetails as td
                                                      LEFT JOIN ES_FacultyEvaluation_Scores as sc ON td.IndexID=sc.QuestionID
                                                      LEFT JOIN ES_FacultyEvaluation as fe ON sc.RefID=fe.EntryID
                                                          WHERE td.TemplateID=1 AND sc.RefID='".$xid."' ORDER BY td.CategoryID,td.IndexID")->result();	
                                $output['comment'] = (($data['eval'])?($data['eval'][0]->Comment):'');
				}
				$output['success']= true;
                $output['content'] = $this->load->view('faculty-evaluation/eval',$data,true);
             break;			 
		   }
		   echo json_encode($output);
        break;
		case 'submit':
		   if($param==false){
		    $studno            = $session_data['idno'];
		    $regid             = $this->db->query("SELECT TOP 1 r.RegID FROM ES_Registrations as r INNER JOIN ES_Grades as g ON r.StudentNo=g.StudentNo AND r.TermID=g.TermID WHERE r.StudentNo='".$studno."' ORDER BY GradeIDX DESC")->result();
            $regid             = (($regid && count($regid)>0)?($regid[0]->RegID):0);
	        $iseval            = $this->db->query("UPDATE rd SET rd.IsEvaluated=1 FROM ES_FacultyEvaluation as fe INNER JOIN ES_RegistrationDetails AS rd ON fe.SchedID=rd.ScheduleID AND rd.RegID='".$regid."' WHERE rd.RegID='".$regid."' AND fe.StudentNo='".$studno."'
		                                           UPDATE fe SET fe.DateSubmitted=GETDATE() FROM ES_FacultyEvaluation as fe INNER JOIN ES_RegistrationDetails AS rd ON fe.SchedID=rd.ScheduleID AND rd.RegID='".$regid."' WHERE rd.RegID='".$regid."' AND fe.StudentNo='".$studno."'");     
		   }else{
			$studno            = $session_data['idno'];
		    $regid             = $this->db->query("SELECT TOP 1 r.RegID FROM ES_Registrations as r INNER JOIN ES_Grades as g ON r.StudentNo=g.StudentNo AND r.TermID=g.TermID WHERE r.StudentNo='".$studno."' ORDER BY GradeIDX DESC")->result();
            $regid             = (($regid && count($regid)>0)?($regid[0]->RegID):0);
	        $iseval            = $this->db->query("UPDATE rd SET rd.IsEvaluated=1 FROM ES_FacultyEvaluation as fe INNER JOIN ES_RegistrationDetails AS rd ON fe.SchedID=rd.ScheduleID AND rd.RegID='".$regid."' WHERE rd.RegID='".$regid."' AND fe.StudentNo='".$studno."' AND fe.EntryID='".$param."'
		                                           UPDATE fe SET fe.DateSubmitted=GETDATE() FROM ES_FacultyEvaluation as fe INNER JOIN ES_RegistrationDetails AS rd ON fe.SchedID=rd.ScheduleID AND rd.RegID='".$regid."' WHERE rd.RegID='".$regid."' AND fe.StudentNo='".$studno."' AND fe.EntryID='".$param."'");     
		      
		   }
		   $output['success'] = true;
		   echo json_encode($output);
		break;
        case 'set':
	       $studno  = $session_data['idno'];
		   $xid     = array_key_exists('xid',$xpost)? str_replace("'","",$xpost['xid']) : '';
		   $xfacul  = array_key_exists('faculty',$xpost)? str_replace("'","",$xpost['faculty']) : '';
		   $xsched  = array_key_exists('sched',$xpost)? str_replace("'","",$xpost['sched']) : '';
		   $answer  = array_key_exists('ans',$xpost)? str_replace("'","",$xpost['ans']) : '';
		   $comment = array_key_exists('comment',$xpost)? str_replace("'","",$xpost['comment']) : '';
		   
		   if($xid=='new'){
			   $insert = $this->db->query("INSERT INTO ES_FacultyEvaluation(TemplateID,FacultyID,SchedID,StudentNo,FinalRating,EvaluationDate,Comment) SELECT 1 as TempID,'".$xfacul."' as FacultyID,'".$xsched."' as Sched,'".$studno."' as StudNo, 0 as Rating, GETDATE() as EvalDate,'".$comment."' as Comment  WHERE '".$xsched."' NOT IN (SELECT SchedID FROM ES_FacultyEvaluation WHERE StudentNo='".$studno."')");
			   if($insert){
			    $xid    = $this->db->query("SELECT TOP 1 EntryID FROM ES_FacultyEvaluation WHERE StudentNo='".$studno."' AND SchedID='".$xsched."'")->result();
			    $xid    = (($xid && count($xid)>0)?($xid[0]->EntryID):false);
			   }
		   }else{
			   $update = $this->db->query("UPDATE ES_FacultyEvaluation SET Comment='".$comment."' WHERE SchedID='".$xsched."' AND StudentNo='".$studno."'");
		   }
		   
		   if($answer!='' && $xid!='' || $xid!='new'){
			  $tmparr = explode('|',$answer);
              if(count($tmparr)>0){
				  foreach($tmparr as $k=>$v){
					 list($ques,$rate)=explode(':',$v); 
                                         $rate   = (($rate=='null' || $rate=='')?0:$rate);
					 $score = $this->db->query("INSERT INTO ES_FacultyEvaluation_Scores(RefID,QuestionID,Score) SELECT '".$xid."' as RefID,'".$ques."' as QID,'".$rate."' as Score WHERE '".$ques."' NOT IN (SELECT QuestionID FROM ES_FacultyEvaluation_Scores WHERE RefID='".$xid."')
					                            UPDATE ES_FacultyEvaluation_Scores SET Score='".$rate."' WHERE RefID='".$xid."' AND QuestionID='".$ques."'");
			    
				  }
			  }			  
			  $final  = $this->db->query("UPDATE e SET e.FinalRating=a.Mean FROM (SELECT (SUM(Score)/COUNT(*)) as Mean,RefID FROM ES_FacultyEvaluation_Scores GROUP BY RefID) as a INNER JOIN ES_FacultyEvaluation as e ON e.EntryID=a.RefID WHERE e.EntryID='".$xid."'");	  
			 
			  $output['success'] = true;
			  $output['EntryID'] = $xid;
			  $output['error']   = '';
		   }
		   echo json_encode($output);
        break;		
	 }
 }
}
?>

