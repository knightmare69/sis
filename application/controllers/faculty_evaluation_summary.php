<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class faculty_evaluation_summary extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   
   date_default_timezone_set("Asia/Manila");
   $this->load->model('mod_faculty_evaluation_summary','model',true);
   $this->load->model('mod_main','',true);
 }

 function index()
 {
   
   if(!$this->session->userdata('logged_in'))
   {
    //If no session, redirect to login page
    redirect('login', 'refresh');
   }
     $studentno = StripSlashes($this->input->get('studentno'));
    
     $session_data = $this->session->userdata('logged_in');

	 $data['title'] = "Faculty Evaluation";
     $data['userinfo'] = $session_data;
    
 	 $username = $session_data['id'];
     $data['username'] = $session_data['username'];

    
	 $data['terms'] = $this->model->get_terms();
     $data['teachers'] =$this->model->get_employeelist(105);
     
     $this->load->view('faculty-evaluation-summary/index',['data' => $data, 'jslink' => array('utilities/faculty-evaluation-summary.js')  ] );
     $this->load->view('include/footer',$data);
               
	 $this->mod_main->Translog($session_data['id'],'View Grades','-');
   
 }
 
 function txn($type="",$mode="",$args="")
 {
	$p = $this->input->post();
	$result = array('success'=>false, 'content'=>'');
	
	switch($type)
    {
	  case 'get':
           switch($mode)
    	   {		
            case 'info':
                $rs = $this->model->get_employeelist($p['term']);
                
                
                $vw = $this->load->view('faculty-evaluation-summary/teacher',['data'=>$rs],true);
                
                //$vw = '1';
                if($vw != ''){
                    $result['success'] = true;            
          	        $result['content'] = $vw;
                }else{
                    $result['success'] = true;            
          	        $result['content'] = "No record found!";
                }
    
            break;		
            
            case 'eval':
            
                $rs = $this->model->get_evaluationSummary($p['term'], $p['id'] );                                
                $vw = $this->load->view('faculty-evaluation-summary/evaluation',['data'=>$rs],true);

                $comment = $this->model->get_comments($p['term'], $p['id'] );
                
                $vw_comment = $this->load->view('faculty-evaluation-summary/comment',['data'=>$comment],true);

                                
                if($vw != ''){
                    $result['success'] = true;            
          	        $result['content'] = $vw;
                    $result['comment'] = $vw_comment;
                }else{
                    $result['success'] = true;            
          	        $result['content'] = "No record found!";
                    $result['comment'] = "No record found!";
                }
    
            break;		
            
    	   }
           ob_clean();           
    	   echo json_encode($result);
	  break;
	  case 'print':
	  
	  break;
	  case 'trial':
	   $data['ds2'] = $this->mod_grades->return_TermID($args);
	   $data['xtermid'] = $data['ds2'][0]->TermID;
	   $data['info_request']=true;
	   $data['xdetail'] = $this->mod_main->call_xtraDetails($args,$data['xtermid']);
	   $data['progclass'] = $data['xdetail']->ProgClass;
	   $data['ds'] = $this->mod_grades->return_dataset($args,$data['xtermid'],$data['progclass']);
	   $result['content'] = utf8_encode($this->load->view('grades/vw_grades_details',$data,true));
	   $result['term'] = $this->load->view('grades/vw_grades_terms',$data,true);
	   $result['xtra'] = $this->load->view('include/studentinfo',$data,true);
	   $result['success'] = true;
	   echo '<pre>';
	   print_r($result);
	  break;		
      default:
       echo 'trial';
	  break;
	}	
 }
 
 function printout($opt='summary',$term=0,$fid=0){
   $fid = ((base64_encode(base64_decode($fid))==$fid)?base64_decode($fid):$fid);
   $this->load->model('mod_crystalreports','',true);
   switch($opt){
     case 'faculty':
           list($file,$filename)= $this->mod_crystalreports->prepare_facultyeval(trim($fid),trim($term));
	   if(($file!='none' && $filename!='none') && ($file!='none1' && $filename!='none1')){
		   header('Content-type: application/pdf');
		   header('Content-Disposition: inline; filename="' . $filename . '"');
		   header('Content-Transfer-Encoding: binary');
		   header('Accept-Ranges: bytes');
                   echo $filename;
		   @readfile($file);
		   unlink($file);
		   $this->mod_main->Translog($session_data['id'],'Print Faculty Evaluation','EmpID:'.$fid);
		 }else{ 
		   //redirect('faculty_evaluation_summary', 'refresh'); 
                   echo 'empty';
		 }
	 break;
	 default:
	    $rs = $this->model->get_employeelist($term);
            $vw = $this->load->view('faculty-evaluation-summary/teacher',['data'=>$rs],true);
            echo 'uy';
	 break;
   
   }
 } 

}
?>

