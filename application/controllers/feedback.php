<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Feedback extends CI_Controller {

 function __construct()
 {
   parent::__construct();
  $this->load->model("mod_downloads",'',true);
 }

 function index($module='',$process=''){
   if($this->session->userdata('logged_in')){
     $session_data = $this->session->userdata('logged_in');
     $data['title'] = "Feedbacks";
     $data['username'] = $session_data['username'];
     
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     //$this->load->view('vwfeedback', $data);
     $this->load->view('include/footer');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

 
}