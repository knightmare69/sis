<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fileeditor extends CI_Controller{
 function __construct(){
   parent::__construct();
   date_default_timezone_set("Asia/Taipei");
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_fileeditor','',TRUE);
   $this->db->db_debug=false;
 }
 
 function index()
 {
  if(!$this->session->userdata('logged_in')){redirect('login','refresh');}
  $session_data = $this->session->userdata('logged_in');
  $session_data['minify'] = "minified";
  $this->mod_main->monitoring_action($session_data,'fileeditor');
  $data['userinfo'] = $session_data;
  $data['title']='FileEditor'; 
  $data['directory'] = $this->mod_fileeditor->generate_list();
  $data['jslink'] = array('utilities/fileeditor.js');
  $this->load->view('include/header',$data);
  $this->load->view('templates/mainmenu',$data);
  $this->load->view('vw_fileeditor',$data);
  $this->load->view('include/footer',$data);	 	 
 }

 function manage($type='',$mode='',$args='')
 {
  $output=array('result'=>false,'content'=>'','error'=>'');
  $session_data = $this->session->userdata('logged_in');
  $p=$this->input->post();
  $data=array();
  if(!is_array($p)){$p=array();}
  switch($type)
  {
	case 'get':
     switch($mode)
	 {
	  case 'open':
       $path = ((array_key_exists('path',$p))?$p['path']:'');
	   if($path!='' && file_exists($path))
	   {
		$output['result']=true;
        $output['content']=file_get_contents($path);		 
	   }	
	  break;	  
	  case 'directory_list':
	   if($args=='custom')
	   {	   
	    $path = ((array_key_exists('path',$p))?$p['path']:'');
	    $data['directory'] = $this->mod_fileeditor->generate_list($path,true);
	    $view ='utilities/list/list_files';
	   }
	   else
	   {
		$path = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']); 
        log_message('error','listing:'.$path);		
	    $data['directory'] = $this->mod_fileeditor->generate_list($path);
	    $view ='vw_directory';
	   }
       if($path!='' && file_exists($path))
       {		   
	    $output['result']=true;
        $output['content']=$this->load->view($view,$data,true);		 	
	   }
	  break;
	 }
	break;
    case 'set':
     switch($mode)
	 {
	  case 'save':
       $path    = ((array_key_exists('path',$p))?$p['path']:'');
	   $content = ((array_key_exists('data',$p))?$p['data']:'');
	   if($path!='' && $content!='')
	   {	   
	    $result  = @file_put_contents($path,$content);
	    if($result)
		{	
		 $output['result']=true;
         $output['content']='Successfully Save Data!';
	    }
	   }
	  break;
	  case 'create':
	   $path    = ((array_key_exists('path',$p))?$p['path']:'');
	   if($path!='')
	   {	   
	    $result  = mkdir($path);
	    if($result)
		{	
		 $output['result']=true;
         $output['content']='Successfully Create Folder!';
	    }
	   }
	  break;
	  case 'newsave':
       $path    = ((array_key_exists('path',$p))?$p['path']:'');
	   $content = ((array_key_exists('data',$p))?$p['data']:'');
	   if($path!='' && $content!='')
	   {	   
	    if(file_exists($path)){unlink($path); }
	    if(!$fp = @fopen($path, FOPEN_WRITE_CREATE)){return FALSE; }
        fwrite($fp, $content.PHP_EOL);
        fclose($fp);
		$result  = (file_exists($path));
	    if($result)
		{	
		 $output['result']=true;
         $output['content']='Successfully Save Data!';
	    }
	   }
	  break;
	  case 'delete':
       $path    = ((array_key_exists('path',$p))?$p['path']:'');
	   if($path!='')
	   {	   
	    $success = true;
		if(is_dir($path))
		 exec('rmdir /S /Q "'.$path.'"');
	    else
		 $success = @unlink($path);
		
		if(!$success){exec('del /S /Q "'.$path.'"'); }
	    $result =((!file_exists($path))?true:false); 
		if($result)
		{	
		 $output['result']=true;
         $output['content']='Successfully Delete File!';
	    }
	   }
	  break;	
	  case 'backup':
       ini_set("memory_limit","1024M");	 
	   $path       = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']);
	   $backuppath = $path.'backup/'; 
       $backup     = $backuppath.date('Ymd').'.zip'; 
       $this->load->library('zip');
       $this->zip->read_dir($path,false); 
       if(!is_dir($backuppath)){mkdir($backuppath); }
       if($this->zip->archive($backup))
	   {
		$output['result']=true;
        $output['content']='Successfully Backup!';   
	   }
       else
	   {
		$output['result']=false;
        $output['content']='Failed to Generate Backup!';   
	   }
	  break;
	  case 'uploadfile':
	     if(array_key_exists('xpath',$p) && array_key_exists('ufile',$_FILES)){
		   $xpath    = $p['xpath'];
		   $filename = $_FILES["ufile"]["name"];
           if ($_FILES["ufile"]["error"] > 0) {
			 $output['error']  = "Return Code: " . $_FILES["ufile"]["error"] . "<br>";
		   }else{
             move_uploaded_file($_FILES["ufile"]["tmp_name"],$xpath.$filename);   
		     $output['result'] = true; 
		   }
		 }
	  break;
      case 'paste':
	   $path    = ((array_key_exists('path',$p))?$p['path']:'');
	   $xpath   = ((array_key_exists('dpath',$p))?$p['dpath']:'');
	   if($path!='' && $xpath!='')
	   {
		if(is_dir($path))
		{	 
		 if(!file_exists($xpath)){mkdir($xpath); }
		 exec('ROBOCOPY "'.$path.'" "'.$xpath.'" /E',$yaks);
	    }
		else	 
		 exec('COPY "'.$path.'" "'.$xpath.'"',$yaks);
	     
		if(file_exists($xpath))
		{	
		 $output['result']=true;
         $output['content']='Successfully Executed!';
	    }
	   }
      break;	  
	 }
	break;	
  }
  echo json_encode($output);
 }
  
 function trial()
 {
  $path = 'C:/xampp/htdocs/sis/uploads/sql server guide.txt';
  $xpath = 'C:/';	 
  exec('MOVE /Y "'.$path.'" "'.$xpath.'"',$yaks);
  print_r($yaks);
  die();
  if(count($yaks)>0) 
   echo 'success';
  else
   echo 'failed';
  
  //echo filesize('C:/Documents and Settings/');
  /*	 
  $this->load->library('zip');
  $path = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']);	  
  $this->zip->read_dir($path);
  $this->zip->archive($path.'sis_20151116.zip');
  */
 } 
 
 function backup()
 {
  ini_set("memory_limit","1024M");	 
  $path       = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']);
  $backuppath = $path.'backup/'; 
  $backup     = $backuppath.date('Ymd').'.zip'; 
  $this->load->library('zip');
  $this->zip->read_dir($path,false); 
  if(!is_dir($backuppath)){mkdir($backuppath); }
  if($this->zip->archive($backup))
   echo 'success';
  else
   echo 'failed';	  
 }
}
?>