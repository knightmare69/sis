<?php if (!defined('BASEPATH')) die();
session_start(); //we need to call PHP's session object to access it through CI
class forgot extends CI_Controller {
   function __construct(){
		parent::__construct();
        $this->load->helper(array("form"));
        $this->load->model('mod_forgot','',TRUE); 
        $this->load->model('mod_main','',TRUE); 
        $this->load->model('user','',TRUE);   		
   }
	
   public function index()
   {
	 if($this->session->userdata('logged_in'))		
	 {
	  $session_data = $this->session->userdata('logged_in');
	  if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	  if ($session_data['idtype'] == 0){ redirect('activation', 'refresh'); } else { redirect('home', 'refresh'); }
	 } 
	 else 
	 {       
	  if($this->session->userdata('registerinfo'))
	    $data['reseted'] = $this->mod_main->filter_reset(xidentifyuser());
	  else
	  {
	   $registration_data = $this->session->userdata('registerinfo'); 
	   $data['reseted']   = is_key_exist($registration_data,'reseted',0);
      }	
	  $data['title'] = "Forgot";
	  $this->load->view('include/header2', $data);		
	  $this->load->view('forgot/vw_forgot', $data);		
	 }		
   }
   
   function txn($type='',$mode='',$args='')
   {
	 $result = array('success'=>false,'content'=>'','error'=>'Undefined Error'); 
	 $xpost  = $this->input->post(NULL,TRUE);
	 $session_data = $this->session->userdata('logged_in');
	 switch($type)
	 {
	   case 'get':
	    switch($mode)
		{
	      case 'question':
		   $idno  = is_key_exist($xpost,'idno','');
		   $query = $this->mod_forgot->get_question($idno);
		   if($query)
		   {
			foreach($query as $rs)
            {
			 $result['uid']   = is_key_exist($rs,'UserID','');
             $result['type']  = is_key_exist($rs,'IDType','');
             $result['param'] = is_key_exist($rs,'Question','');
             $result['pid']   = is_key_exist($rs,'QuestionID','');
             $result['email'] = is_key_exist($rs,'Email','');
			}
			
            $result['success'] = ((is_key_exist($result,'uid')!=false)? true : false);
            $result['error']   = (($result['success']==false)?'No Account Is Found With That Credential':'');			
		   }
           else
            $result['error'] = 'No Account Is Found With That Credential';  
		   
		   $result['content'] = $idno;			   
          break;		  
		}
        echo json_encode($result);
       break;
       case 'set':
	    switch($mode)
		{
		  case 'reset':
		    $uname = is_key_exist($xpost,'uname','');
		    $email = is_key_exist($xpost,'email','');
            $npwd  = $this->mod_main->GenerateCode();
			$xinfo = $this->mod_forgot->get_userinfo($uname,$email);
			if($xinfo)
		    {
			 $uid     = is_key_exist($xinfo,'UserID');
			 $usern   = is_key_exist($xinfo,'UserName');
			 $email   = is_key_exist($xinfo,'Email');
			 $idno    = is_key_exist($xinfo,'IDNo');
			 $type    = is_key_exist($xinfo,'IDType');
			 $success = $this->mod_forgot->set_password($uid,$idno,$type,$npwd);	
			 $result  = $this->process_reset($success,$usern,$email,$npwd);
			}		
		    else
			{
			 $this->mod_main->TransLog($email,'Forgot Password Failed','-');	
			 $result['error']   = 'No Account is found with this credentials';
			 $result['success'] = false;	
			} 
          break;		  
		  case 'xreset':
		    $uid    = is_key_exist($xpost,'uid','');
			$usern  = is_key_exist($xpost,'uname','');
		    $idno   = is_key_exist($xpost,'idno','');
		    $type   = is_key_exist($xpost,'idtype',0);
		    $qid    = is_key_exist($xpost,'qid',0);
		    $ans    = is_key_exist($xpost,'ans','');
		    $amail  = is_key_exist($xpost,'amail','');
		    $notify = is_key_exist($xpost,'notify');
            $npwd   = $this->mod_main->GenerateCode();
			$xinfo  = $this->mod_forgot->validate_acct($uid,$usern,$qid,$ans);
			if($xinfo)
		    {
			 $usern = is_key_exist($xinfo,'UserName','');
			 $idno  = is_key_exist($xinfo,'IDNo','');
			 $success = $this->mod_forgot->set_password($uid,$idno,$type,$npwd,$amail);	
			 $result  = $this->process_reset($success,$usern,$amail,$npwd);
			}		
		    else
			{
			 $this->mod_main->TransLog($amail,'Forgot Password Failed','-');	
			 $result['error']   = 'Invalid Answer!';
			 $result['success'] = false;	
			} 
          break;		  
		}
	    echo json_encode($result);
       break;	   
	 }
   }
   
   function process_reset($success=false,$usern='',$email='',$npwd='')
   {
	 $result = array('success'=>false,'content'=>'','error'=>'Error While Resetting Password');
	 if($success)
	 {
	  $result['success']= true;
	  $result['issent'] = false;
	  $result['uname']  = $usern;    
	  $result['email']  = $email;
	  $result['npwd']   = $npwd;
	  $this->mod_main->TransLog($email,'Forgot Password','USER:'.$usern.',PWD:'.$npwd);	
	  if(EMAIL_NOTIFY==1)
	  {
	   $data['default'] = false;
	   $data['title']   = "PRISMS Online:Reset Password";
	   $data['msg']     = "<h3>Hi, ".$result['uname']."</h3>
						   <p class='lead'>We have successfully reset your password</p>
						   <p>You may now sign in to the PRISMS Portal using the information below : </p>			
						   <p>Username: ".$result['uname']."</p>								
						   <p>Password: ".$result['npwd']."</p>
						   <p><i>*Note:Please check your spelling and case.</i></p>";
					 
	    try {
		   error_reporting(0);	
	       $result['issent'] = $this->mod_main->xsendmail('',$email,'',$data['title'],$data,'mail/mail_template');
	    } catch(Exception $e){
		   $result['issent'] = false;
	    }	
	  }
	  $result['content'] = $this->load->view('forgot/vw_forgot_result',$result,true);
	  $registration_data['reseted']=date('Ymd');
	  $this->session->set_userdata('registerinfo',$registration_data);
	 }	
	 else
	 {
	  $this->mod_main->TransLog($email,'Forgot Password Failed','-');
	  $result['error']   = "Failed to reset your password"; 
	  $result['success'] = false;
	 }		
	 return $result;
   }

}

/* End of file forgot.php */
/* Location: ./application/controllers/forgot.php */
?>