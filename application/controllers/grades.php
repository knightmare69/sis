<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Grades extends CI_Controller {

 function __construct(){
   parent::__construct();
   
   date_default_timezone_set("Asia/Taipei");
   $this->load->model('mod_grades','',true);
   $this->load->model('mod_main','',true);
   $this->load->model('mod_crystalreports','',true);
   $this->EnableGrades = $this->mod_main->getconfigvalue('GradesModuleLock');
   $this->WithChinese = ((defined('WITH_CHINESE') && WITH_CHINESE==1)?true:false);
   if(strtotime($this->EnableGrades))
    $this->EnableGrades = (($this->EnableGrades<=date('m/d/Y')) ? 1 : 0);   
   else
    $this->EnableGrades = (($this->EnableGrades=="1" || strtolower($this->EnableGrades)=="yes" || strtolower($this->EnableGrades)=="true") ? 1 : 0);   
 }

 function index(){
   $studentno = StripSlashes($this->input->get('studentno'));
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
	 $this->mod_main->monitoring_action($session_data,'grades');
	 $data['disabled'] = ($this->EnableGrades==1)?0:1;
	 $data['title'] = "Report of Grades";
     $data['userinfo'] = $session_data;
    
 	 $username = $session_data['id'];
     $data['username'] = $session_data['username'];
     $data['idtype'] = $session_data['idtype'];
     $data['studentno'] = $session_data['idno'];
     $data['studentnolist'] = $this->mod_main->ListofParentChild($username);
     
     if ($data['idtype'] == 2)
      $data['studentno'] = $data['studentnolist'][0]->StudentNo;
	 else if ($data['idtype'] == '-1')
	 {
	  $data['studentfilter']=1;	 
	  $data['studentno'] = 0;
	 }
	 
	 $data['withchinese'] = $this->WithChinese;
     $data['ds2'] = $this->mod_grades->return_TermID($data['studentno']);
	 $lastid = (count($data['ds2'])-1);
	 $data['xtermid'] = (($data['studentno']!=0)?($data['ds2'][$lastid]->TermID):-1);
	 $data['xdetail'] =  $this->mod_main->call_xtraDetails($data['studentno'],$data['xtermid']);
     if($data['xtermid']==0)
	 {	 
	  $data['progclass'] = 50;
	  $data['progname']='';
     }
	 else
	 {	 
      $data['progclass'] = (($data['studentno']!=0)?($data['xdetail']->ProgClass):30);
      $data['progname'] = (($data['studentno']!=0 && property_exists($data['xdetail'],'ProgName'))?($data['xdetail']->ProgName):'');
     }
	 
	 $data['ds'] = $this->mod_grades->return_dataset($data['studentno'],$data['xtermid'],$data['progclass']);
	 if($data['ds2']!='' && $data['ds2']!=false && $data['progclass'] > 20 && $data['xtermid']>0 && $data['progname']!='Senior High School')
	 {
      $rs = $this->mod_grades->get_GradeSummary($data['studentno'],$data['xtermid']);
      foreach($rs as $xs )
	  {
       $data['gwa'] = (($xs->gwa>0)?$xs->gwa:'0.00');
       $data['enrolled'] = (($xs->enrolled>0)?$xs->enrolled:'0.00');
       $data['earned'] = (($xs->earned>0)?$xs->earned:'0.00');
       $data['cqpa1'] = (($xs->cqpa1>0)?$xs->cqpa1:'0.00');
       $data['cqpa2'] = (($xs->cqpa2>0)?$xs->cqpa2:'0.00');
      }
	 }
	 else
	 {
       $data['gwa'] = '';
       $data['enrolled'] = '';
       $data['earned'] = '';
       $data['cqpa1'] = '';
       $data['cqpa2'] = '';
	 }
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('grades/vw_grades', $data);
     $mfooter['jslink'] = array('utilities/grades.js');     
     $this->load->view('include/footer',$mfooter);
	 $this->mod_main->Translog($session_data['id'],'View Grades','-');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function txn($type="",$mode="",$args="")
 {
	$p = $this->input->post();
	$result = array('success'=>false, 'content'=>'');
	
	$data['withchinese'] = $this->WithChinese;
	switch($type)
    {
	  case 'get':
       switch($mode)
	   {
		case 'grades':
		 $data['info_request']=true;
		 $data['xdetail'] = $this->mod_main->call_xtraDetails($p['student'],$p['term']);
		 if(!array_key_exists('term',$p) || $p['term']==0)
		 {	 
	      $data['progclass'] = 50;
          $data['progname'] = '';
         }
		 else
		 {
		  $data['progclass'] = $data['xdetail']->ProgClass;
          $data['progname'] = ((property_exists($data['xdetail'],'ProgName'))?($data['xdetail']->ProgName):'');
		 }
		 
		 if($data['progclass']<=0){$data['progclass']=50; }
		 
		 $data['ds'] = $this->mod_grades->return_dataset($p['student'],$p['term'],$data['progclass']);
		 $check      = $this->db->query("SELECT COUNT(*) as UnEvaluated,dbo.fn_sisConfigNetworkTermID() as DefaultTerm FROM ES_Registrations as r INNER JOIN ES_RegistrationDetails as rd ON r.RegID=rd.RegID INNER JOIN ES_Grades as g ON r.TermID=g.TermID AND rd.ScheduleID=g.ScheduleID AND r.StudentNo=g.StudentNo AND g.FinalRemarks<>''  WHERE r.CollegeID=1 AND r.StudentNo='".$p['student']."' AND r.TermID='".$p['term']."' AND rd.RegTagID<>3 AND (IsEvaluated=0 OR IsEvaluated IS NULL)")->result();
		 $sched      = $this->db->query("SELECT TOP 1 MidtermEndGradeEncoding as StartDate, EndOfAY as EndDate, (CASE WHEN CONVERT(DATE,MidtermEndGradeEncoding)<=GETDATE() AND EndOfAY>=GETDATE() THEN 1 ELSE 0 END) as OnPeriod,AcademicYear,SchoolTerm FROM ES_AYTERMConfig as ac INNER JOIN ES_AYTERM as a ON a.TermID=ac.TermID WHERE a.TermID='".$p['term']."'")->result();
		 $checkterm  = (($check && count($check)>0)?($check[0]->DefaultTerm):0);
		 $check      = (($check && count($check)>0)?($check[0]->UnEvaluated):0);
		 $onperiod   = (($sched && count($sched)>0)?($sched[0]->OnPeriod):0);
		 if($p['term']!=4){
                   $check = 0;
                 }
		 if($check>0 && $onperiod==1){
		    $result['content'] = "<div class='alert alert-warning'><h3><i class='fa-fw fa fa-warning'></i> You need to evaluate your professor(s)/instructor(s) before you can view your grades. Click <a href='".base_url('faculty-evaluation')."'>here</a> to proceed.</h3></div>";	 
		 }elseif($check>0 && $onperiod==0){
		    $result['content'] = "<div class='alert alert-warning'><h3><i class='fa-fw fa fa-warning'></i> Faculty Evaluation Is Closed At The Moment.</h3></div>";	 
		 }else{
	           $result['content'] = $this->load->view('grades/vw_grades_details',$data,true);
                 }

                 $result['xtra'] = $this->load->view('include/studentinfo',$data,true);
		 $result['success'] = true;
		 $result['summary'] = false;
		 
		 if($data['progclass'] > 20 && $p['term']>0 && $data['progname']!='Senior High School')
	     {
		  $result['summary'] = true;
		  $rs = $this->mod_grades->get_GradeSummary($p['student'],$p['term']);
		  foreach($rs as $xs )
		  {
		   $result['gwa']      = (($xs->gwa>0)?$xs->gwa:'0.00');
		   $result['enrolled'] = (($xs->enrolled>0)?$xs->enrolled:'0.00');
		   $result['earned']   = (($xs->earned>0)?$xs->earned:'0.00');
		   $result['cqpa1']    = (($xs->cqpa1>0)?$xs->cqpa1:'0.00');
		   $result['cqpa2']    = (($xs->cqpa2>0)?$xs->cqpa2:'0.00');
          }
	     }
        break;
        case 'info':
		 $data['ds2'] = $this->mod_grades->return_TermID($p['student']);
	     $lastid = (count($data['ds2'])-1);
	     $data['xtermid'] = (($lastid>=0)? ($data['ds2'][$lastid]->TermID) : 0);
	     $data['info_request']=true;
		 $data['xdetail'] = $this->mod_main->call_xtraDetails($p['student'],$data['xtermid']);
		 if($data['xtermid']==0)
	     {	 
	      $data['progclass'] = 50;
          $data['progname'] = '';
         }
		 else
		 {
		  $data['progclass'] = $data['xdetail']->ProgClass;
          $data['progname'] = ((property_exists($data['xdetail'],'ProgName'))?($data['xdetail']->ProgName):'');
		 }
		 //$data['ds'] = $this->mod_grades->return_dataset($p['student'],$data['xtermid'],$data['progclass']);
		 $result['content'] = '';//utf8_encode($this->load->view('grades/vw_grades_details',$data,true));
		 $result['term'] = $this->load->view('grades/vw_grades_terms',$data,true);
		 $result['xtra'] = $this->load->view('include/studentinfo',$data,true);
		 $result['success'] = true;
		 $result['summary'] = false;
		 if($data['progclass'] > 20 && $data['xtermid']>0 && $data['progname']!='Senior High School')
	     {
		  $result['summary'] = true;
          $rs = $this->mod_grades->get_GradeSummary($p['student'],$data['xtermid']);
          foreach($rs as $rs )
	      {
		   $result['gwa'] = (($rs->gwa>0)?$rs->gwa:'0.00');
		   $result['enrolled'] = (($rs->enrolled>0)?$rs->enrolled:'0.00');
		   $result['earned'] = (($rs->earned>0)?$rs->earned:'0.00');
		   $result['cqpa1'] = (($rs->cqpa1>0)?$rs->cqpa1:'0.00');
		   $result['cqpa2'] = (($rs->cqpa2>0)?$rs->cqpa2:'0.00');
          }
	     }
        break;		
	   }
	   echo json_encode($result);
	  break;
	  case 'print':
	  
	  break;
	  case 'trial':
	   $data['ds2'] = $this->mod_grades->return_TermID($args);
	   $data['xtermid'] = $data['ds2'][0]->TermID;
	   $data['info_request']=true;
	   $data['xdetail'] = $this->mod_main->call_xtraDetails($args,$data['xtermid']);
	   $data['progclass'] = $data['xdetail']->ProgClass;
	   $data['ds'] = $this->mod_grades->return_dataset($args,$data['xtermid'],$data['progclass']);
	   $result['content'] = utf8_encode($this->load->view('grades/vw_grades_details',$data,true));
	   $result['term'] = $this->load->view('grades/vw_grades_terms',$data,true);
	   $result['xtra'] = $this->load->view('include/studentinfo',$data,true);
	   $result['success'] = true;
	   echo '<pre>';
	   print_r($result);
	  break;		
      default:
       echo 'trial';
	  break;
	}	
 }
 
 function printg($idno='',$termid='')
 {
  if($this->session->userdata('logged_in')) {
    $session_data = $this->session->userdata('logged_in');
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	$idtype = $session_data['idtype'];
	$studno = $session_data['idno'];
    
	if($idtype==2 || $idtype=='-1')
	 $studno = StripSlashes($idno);
	
    $file='none';
    $filename='none';
	
    if($studno!='' && $studno!=' ')
    {
	 list($file,$filename)= $this->mod_crystalreports->prepare_grades($studno,$termid);
		
	 if($file!='none' && $filename!='none')
	 {
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print Grades','-');
	 }
	 else
	 {echo 'Error Loading the report. Please Report to the administrators.';}
    }
    else { redirect('grades', 'refresh'); }
  }else { redirect('login', 'refresh'); }

 }
 
 function trial()
 {
   echo '<pre>';
   print_r($this->mod_grades->exec_Grade('2013300509','97',50));   
 }
 
}
?>

