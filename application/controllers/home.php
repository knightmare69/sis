
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

 function __construct(){
   parent::__construct();
   date_default_timezone_set("Asia/Taipei");
   $this->load->model('user','',true);
   $this->load->model('mod_home','',true);
   $this->load->model('mod_main','',true);
   $this->load->model('mod_evaluation','',true);
   $this->load->model('mod_vbscripts','',TRUE);
   $this->AdptPRISMS = $this->mod_main->getconfigvalue('AdaptSecurity');   
   $this->admintestpwd = 'y0wzAh';	   
   $this->adminmasterkey = 'N@r9@c09A';
 }
 
 function index(){
   if($this->session->userdata('logged_in')) {
	 $session_data = $this->session->userdata('logged_in');
	 $data['privileges'] = $this->mod_main->monitoring_action($session_data,'home');
	 if(strrpos($session_data['photo'],'empty.png')!==false)
	 {
	  $img = $this->mod_main->manage_photo($session_data['id'],$session_data['idno'],$session_data['idtype']);
	  $session_data['photo'] = $this->mod_main->SetProfile($img);	
	  $this->session->set_userdata('logged_in', $session_data);
	 }
	 
     $data['userinfo'] = $session_data;
     $data['title']    = "Dashboard";
     
	 if($session_data['idtype'] == 1)
     {
      $data['studentno'] = $session_data['idno'];
      $data['studentinfo']= $this->mod_main->StudentInfo($data['studentno']);
	  $data['academicinfo'] = $this->mod_home->academicinfo($data['studentno']);
      $data['progressbar'] = $this->mod_home->progressbarinfo($data['studentno']);
      $data['yearcount'] = COUNT($data['progressbar']);
      $data['pie'] = $this->mod_home->pieinfo($data['studentno']);
     }
	 elseif($session_data['idtype'] == -1)
	 {
	  $data['stats'] = $this->mod_home->exec_admindashboard('0.1')[0];
	  $data['schart'] = $this->mod_home->generate_stats();
	  $data['enroll']= $this->mod_home->exec_admindashboard('0.4')[0];
	  $data['grades']= $this->mod_home->exec_admindashboard('0.5')[0];
	  //die();
	 }
     
	 $data['csslink'] = array('datepicker3.min.css','calendar.css');
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vhome', $data);
     $footer['jslink'] = array('plugin/easy-pie-chart/jquery.easy-pie-chart.min.js',
	                           'plugin/flot/jquery.flot.cust.js',
							   'plugin/flot/jquery.flot.resize.js',
							   'plugin/flot/jquery.flot.tooltip.js',
			                   'plugin/flot/jquery.flot.fillbetween.js',
			                   'plugin/flot/jquery.flot.orderBar.js',
							   'plugin/flot/jquery.flot.pie.js',
                               'plugin/bootstrap-datepicker/bootstrap-datepicker.min.js',
							   'prompt.js?20151104',
							   'utilities/home.js');
     $this->load->view('include/footer',$footer);
	 $this->mod_main->Translog($session_data['id'],'View Home','-');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function photo($type=1,$path='id pics',$args='')
 {
  $img = $this->mod_main->SetProfile('nophoto');
  if($path!='nophoto'){
   $session_data = $this->session->userdata('logged_in');
   $imgpath = 'assets/img/'.$path.'/'.(($path=='profile')?md5($session_data['id'].$session_data['idno']):$args);
   $img = $this->mod_main->CheckImage($imgpath);
   if($img=='nophoto'){$img = $this->mod_main->manage_photo($session_data['id'],$args,$type,$path); }
   $img = $this->mod_main->SetProfile($img);
  }
  
  $img = str_replace(base_url(),str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']),$img);
  if(strpos($img,'png')>0)
   $type='png';
  else if(strpos($img,'gif')>0)
   $type='gif';
  else if(strpos($img,'jpeg')>0 || strpos($img,'jpg')>0)
   $type='jpeg';
  
  echo 'data:image/'.$type.';base64,'.base64_encode(file_get_contents($img));
 }
 
 function pics($path='profile',$type=0,$args='')
 {
  $path = urldecode($path);
  $session_data = $this->session->userdata('logged_in');
  $img = $this->mod_main->SetProfile('nophoto');
  if($path!='nophoto')
  {
   if($path =='profile')	  
    $idno = $this->mod_main->get_userinfo($args,'IDNo');
   else
	$idno = $args;
   
   $imgpath = 'assets/img/'.$path.'/'.(($path=='profile')?md5($args.$idno):$args);
   $img = $this->mod_main->CheckImage($imgpath);
   if($img=='nophoto' && $type!=2 && $type!=0)
   {	   
    $img = $this->mod_main->xmanage_photo($session_data['id'],$args,$idno,$type,$path);
   }	
   
   $img = $this->mod_main->SetProfile($img);
  }
  
  $imgpath = str_replace(base_url(),str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']),$img);
  if(strpos(strtolower($imgpath),'png')>0)
   $type='png';
  else if(strpos(strtolower($imgpath),'gif')>0)
   $type='gif';
  else if(strpos(strtolower($imgpath),'jpeg')>0 || strpos(strtolower($imgpath),'jpg')>0)
   $type='jpeg';

  ob_end_clean();
  header('Content-Length: '.filesize($imgpath));
  header('Content-type: image/'.$type);	 
  @readfile($imgpath);
 }
 
 function logout()
 {
   $session_data = $this->session->userdata('logged_in');
   $uname        = 'user';
   if($session_data){
    $uname = ((@array_key_exists('id',$session_data))?$session_data['id']:'Session Expired User');
    $this->mod_main->Translog($uname,'Logout','-');
   }

   $_SESSION = array(); 
   $this->session->unset_userdata('logged_in');
   $this->session->sess_destroy();
   
   redirect('login', 'refresh');
 }

 function setminify(){  
  $min = $this->input->post('minified');
  
  $session_data = $this->session->userdata('logged_in');
  $session_data['minify'] = ($min==1?"minified":"");
  $this->session->set_userdata('logged_in', $session_data);
  //echo $min;
 } 
 
 function setLeftPage(){  
  $session_data = $this->session->userdata('logged_in');
  $uname = $session_data['id'];
  $min = $this->input->post('isleave');
  $this->mod_main->Translog($uname,'Leave the website','-');
  //echo $min;
 }
 
 function passwordverify()
 {
  $this->load->model('mod_vbscripts','',TRUE);
  $this->load->model('mod_crypto','',TRUE);
  $output ='nodata';
  $session_data = $this->session->userdata('logged_in');
  $user = $session_data['id'];
  $idtype = $session_data['idtype'];
  $pwd = $this->input->post('pwd');
  $pwd = str_replace("'","",$pwd);
  $result   = false;
  if($this->adminmasterkey==$pwd){
     $result = $this->user->login($user, $this->admintestpwd,true);
  }else{
    if($this->AdptPRISMS==1){
	 $conditions = array(1,3,5,7);
	 $cryptopwd = $this->mod_crypto->BitEncrypt($pwd,$conditions);
	 $result = $this->user->adaptive_login($user, $pwd,$cryptopwd);
    }
    else
         $result = $this->user->login($user, $pwd);
   }
  if($result)
   $output = 'valid';
  else 
   $output = 'invalid'; 
   
  echo $output;   
 }
 
 function filter($type='',$data='')
 {
  if($this->input->post() && $data=='')
  {
   $p = $this->input->post();
   $data = ((isset($p['data']) && $p['data']!='')?($p['data']):'');
   $data = strsanitizer($data);
   $data = explode(' ',$data);	 
  }
  else
  {
   $data = str_replace('%20',' ',$data);
   $data = strsanitizer($data);
   $data = explode(' ',$data);	 
  }
  
  $result=array('success' => false
               ,'content' => '');
			   
  switch($type)
  {
   case 'student':
	$output = 'StudentNo,LastName,FirstName,MiddleName,MiddleInitial'; 
	$field = array('StudentNo','LastName','FirstName','MiddleName'); 
    $content['list'] = $this->mod_main->filter_data(50,'ES_Students',$output,$field,$data,'','like');	
    $result['success'] = true;
	$result['content'] = utf8_encode($this->load->view('utilities/list/list_students',$content,true));
	echo json_encode($result);
   break;
   case 'user':
	$output = 'UserID,UserName,Email,IDNo,IDType,LastName, FirstName,MiddleName'; 
	$field = array('UserName','IDno','Email'); 
    $content['list'] = $this->mod_main->filter_data(50,'vw_memberships',$output,$field,$data,'','like');	
    $result['success'] = true;
	$result['content'] = utf8_encode($this->load->view('utilities/list/list_users',$content,true));
	echo json_encode($result);
   break;
   default:
    echo json_encode($result);  
   break;
  }
  die();
 }
 
 function checkingphp(){phpinfo();}
 function spec_op($opt='',$args=''){
  if($opt==''){redirect('home','refresh');}
  echo $this->mod_main->spexial_effects($opt,$args);
 }
 
 function reevaluate()
 {
  if(!$this->session->userdata('logged_in')){die('failed');}	
  $session_data = $this->session->userdata('logged_in');
  $this->mod_evaluation->studAcadEvaluation($session_data['idno']);
  echo 'success';  
 }
 
 function trial()
 {
  $this->load->model('mod_activation','',true);
  echo 'Is Valid:';
  print_r($this->mod_activation->validate('2014300475', '11/06/1997', 102, '2014300475', '467480', '06062016'));
 }

}
//Eliza Campos      - 2013300509
//Ericka Cayton     - 2011300400
//Mary Louise Mazon - 2012300062 
?>

