
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Institution extends CI_Controller {

 

 function __construct()
 {
   parent::__construct();
   
   $this->load->model('mod_institution','',TRUE);
   
   if($this->session->userdata('logged_in'))
   { 
     $this->institution(); 
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function index(){
  
 }

 function institution()
 {
     $sel = $this->uri->segment(2);
     
     $ntitle = $sel;
     
     switch($sel)
     {
      case "schoolinfo": $ntitle='School Information';       
       break;
      case "campus":     $ntitle='Campus Information';        
       break;
      case "colleges":     $ntitle='College Information';        
       break;
      case "programs":     $ntitle='Academic Programs';        
       break;
      case "majorgroups":     $ntitle='Major Groups';        
       break;
      case "majorstudy":     $ntitle='Major Study';        
       break;
      default: $ntitle='School Information';       
       break;
     }
     
     $session_data = $this->session->userdata('logged_in');
     $data['userinfo'] = $session_data;
     $data['username'] = $session_data['username'];
     $data['title'] = "Institution :: " . $ntitle;
     $data['nheader'] = $ntitle;
     $data['menus'] = $this->mod_institution->menus();
     $data['nactive'] = ($sel ==''? 'schoolinfo': $sel) ;
     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vinstitution', $data);
     $this->load->view('include/footer',$data);
 }
 function schoolinfo(){
  
 }
 function campus(){
  
 }
 function colleges(){
  
 }
 function programs(){}
 function majorgroups(){}
 function majorstudy(){}
}

?>

