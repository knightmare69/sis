
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Ledger extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_ledger','',TRUE);
   $this->load->model('mod_crystalreports','',TRUE);
   $this->latestonly = true;
 }

 function index()
 {
   if($this->session->userdata('logged_in')){
     $session_data = $this->session->userdata('logged_in');
	 if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 
     $data['userinfo'] = $session_data;
     $data['title'] = "Student Ledger";
     $data['username'] = $session_data['username']; 
     $data['idtype']   = $session_data['idtype'];	 
     $data['studentno'] = $session_data['idno'];
     
	 if($session_data['idtype'] == 2)
     {
	  $data['studentnolist'] = $this->mod_main->ListofParentChild($session_data['id']);
      $data['studentno'] = $data['studentnolist'][0]->StudentNo;
	 } 
	 else if($session_data['idtype'] == '-1')
	 {
	  $data['studentfilter']=1;	 
	  $data['studentno'] = 0;
	 }
	 
	 $xrow = $this->mod_ledger->fetch_row($data['studentno']);
	 $data['row'] = $this->mod_ledger->generate_tablev1($xrow); //sp_studentledger
	 //$data['row'] = $this->mod_ledger->generate_tablev2($xrow); //sp_subsidiaryledger
     $data['jslink']=array('utilities/ledger.js');
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_ledger', $data);
     $this->load->view('include/footer');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function data($opt='')
 {
  $output = array('success'=>false,'content'=>'','error'=>'Failed!');
  $p=$this->input->post();
  if($p==false){$p=array(); }
  switch($opt)
  {
   case 'info':
    $data['xdetail'] = $this->mod_main->call_xtraDetails($p['student']);
	$data['info_request']=true;
    if($p['student']!='')
	{	
	 $output['success'] = true;
	 $output['content'] = $this->load->view('include/studentinfo',$data,true);
	 $output['xtra']    = $data['xdetail'];
	}	
   break;   
   case 'data':
    if($p['student']!='')
	{
     $xrow = $this->mod_ledger->fetch_row($p['student']);
	 $output['success'] =true;
	 $output['content'] = $this->mod_ledger->generate_tablev1($xrow);		
	}	
   break;
  }
  echo json_encode($output);  
 }
}

?>

