
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Lock extends CI_Controller {

 function __construct(){
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->load->model('user','',TRUE);
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $this->load->helper(array("form"));
     $data['userinfo'] = $session_data;
     $data['title'] = "Lock";
	 $id = $session_data['id'];
     $data['username'] = $session_data['username'];
     $data['photo'] = $session_data['photo'];

     $session_data['lock'] = 1;
     $this->session->set_userdata('logged_in', $session_data);
     $this->mod_main->Translog($id,'Lock Site','-');     
     $this->load->view('vw_lock', $data);
     //$this->load->view('include/footer');
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 function access(){
  $session_data = $this->session->userdata('logged_in');
  $user = $session_data['id'];
  $idtype = $session_data['idtype'];
  $pwd = $this->input->post('pwd');
  $result = $this->user->login($user, $pwd);
  
   if($result)
   {
    foreach($result as $r)
	{
     $session_data['lock'] = 0;
     $this->session->set_userdata('logged_in', $session_data);
     $this->mod_main->Translog($user,'Unlock Site','-');
     if($idtype>0 || $idtype== -1)
     {redirect('profile', 'refresh');}
     else{redirect('activation', 'refresh');}

    }
   }
   else { 
          $this->mod_main->Translog($user,'Unlock Failed(Password Error)','-');     
	      redirect('lock', 'refresh'); 
		}		   
    
 }
 
}

?>

