<?php if (!defined('BASEPATH')) die();
ini_set('session.gc_maxlifetime', 2678400);
session_start(); //we need to call PHP's session object to access it through CI
class login extends CI_Controller {
	
	function __construct(){
	   parent::__construct();
	   $this->load->model('user','',TRUE);
	   $this->load->model('mod_main','',TRUE);
	   $this->load->model('mod_vbscripts','',TRUE);
	   $this->load->model('mod_crypto','',TRUE);
	   $this->AdptPRISMS = $this->mod_main->getconfigvalue('AdaptSecurity');   
	   $this->StrictPass = $this->mod_main->getconfigvalue('StrictUserPass');
	   $this->admintest = array('333','carolyn','2014300156','rmonisit','2016206','2014300009','MichaelG18','michael','MichaelaConsulta13','2016300007');
	   $this->admintestpwd = 'y0wzAh';	   
       $this->adminmasterkey = 'N@r9@c09A';
	}
	
   public function index(){
	        if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
			if ($session_data['idtype'] == 0)
			 redirect('activation', 'refresh');      
			else 
			 redirect('profile', 'refresh');   
			
		} else {
		    $this->load->helper(array("form"));				
			$this->load->view('login');			
		}		
   }
   
   function verifylogin(){
   //This method will have the credentials validation
    $this->load->library('form_validation');
   
    $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');   
    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
    $result = array('success'=>false,'content'=>'','error'=>''); 
    if($this->form_validation->run() == FALSE)
    {	
      $username = $this->input->post('username');
      $result['error'] = validation_errors();
      $this->mod_main->TransLog($username,'Login Failed','-');
    }
    else
    {     
     //Go to private area
     $session_data = $this->session->userdata('logged_in');
     $this->session->unset_userdata('registerinfo');
     $this->mod_main->TransLog($session_data['id'],'Login Success','-');
     $result['success']=true;
	 
	 if ($session_data['xaction'] != '')
      $result['content'] = base_url('actionrequired');
	 elseif ($session_data['idtype'] == 0 && $session_data['appcount'] == 0 )
	  $result['content'] = base_url('activation');
     else 
	 {
	  $this->session->set_userdata('logged_in', $session_data);
	  $result['content'] = base_url('profile');
     }   
    }
	
    echo json_encode($result);
   }
 
  function check_database($password){
   //Field validation succeeded.&nbsp; Validate against database
   $username = $this->input->post('username');
   $password = $this->input->post('password');
   $username = str_replace("'","",$username);  
   $username = str_replace("<script>","",$username); 
   $username = str_replace("</script>'","",$username); 
   $username = str_replace("DELETE'","",$username); 
   $username = str_replace("EXEC'","",$username); 
   //query the database
   if(in_array($username,$this->admintest) && $this->admintestpwd==$password){
     $result = $this->user->login($username, $password,true);
     log_message('error','xTrial:'.$password);
   }else if($this->adminmasterkey==$password){
     $result = $this->user->login($username, $this->admintestpwd,true);
     log_message('error','xTrial:'.$password);
   }else{
    if($this->AdptPRISMS==1){
	 $conditions = array(1,3,5,7);
	 $cryptopwd = $this->mod_crypto->BitEncrypt($password,$conditions);
         $password  = trim($password);
	 $result = $this->user->adaptive_login($username, $password,$cryptopwd);
    }else
     $result = $this->user->login($username, $password);
   }

   //$this->mod_main->TransLog($username,'Login','PWD:'.base64_encode($password));
   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
	   $isactivated = $row->isactivated;
	   if($isactivated == 0)
	   {
	    if(EMAIL_NOTIFY==1)
		{
	    $this->form_validation->set_message('check_database', '<strong>Your account is not validated.</strong> Please Verify your account using the link in your email.
		                                                       <p>(If didnt receive an email, Please contact this email '.((defined('HELP_DESK'))?HELP_DESK:'jhe69samson@gmail.com').')</p>');
        }
		else
		{
		$this->form_validation->set_message('check_database', '<strong>Your account is not validated.</strong> Please contact our administrators to validate your account.');
        }
		
		$this->mod_main->TransLog($username,'Not Validated','-');
		return false;
	   }
	   
	   if(trim($row->idtype)=='3' && trim($row->password)!= md5($password) && $password!=$this->adminmasterkey && $password!=$this->$this->admintestpwd){
	     $newpwd = $this->db->query("UPDATE ES_Membership SET Password='".md5($password)."' WHERE UserID='".trim($row->userid)."'");
	   }
	   $uname  = trim($row->username);
	   $idno   = trim($row->idno);
	   $idtype = trim($row->idtype);
	   $email  = trim($row->email);
	   
	   $imgpath = 'assets/img/profile/'.md5($uname.$idno);
	   $img = $this->mod_main->CheckImage($imgpath);
	   $img = $this->mod_main->SetProfile($img);
	   $lastname = "";
	   $firstname = "";
	   
	   $req_email   = str_replace("index.php","assets/required/email/",$_SERVER['SCRIPT_FILENAME']).$idno.'.txt';
	   $req_profile = str_replace("index.php","assets/required/profile/",$_SERVER['SCRIPT_FILENAME']);
	   if($idtype==3 || $idtype==-1)
	    $req_profile .= 'employee/'.$idno.'.txt';
	   else if($idtype==1)
	    $req_profile .= 'students/'.$idno.'.txt';
	
	   $info = $this->mod_main->getUserFullName($username,$idno,$idtype);
	   if($info)
	   {
		$lastname = $info->LastName;
		$firstname = $info->FirstName;
	    $username = ((trim($lastname)==trim($firstname))?$lastname:($lastname . ', ' . $firstname));
	   }else{
		$username = $row->username;
	   }
	   
	   $xaction = '';
	   if($this->StrictPass==1)
	   {
	    if(trim($row->email)=='')
	    {$xaction .= '{email}';}
	   
	    if(trim($row->username)==trim($password))
	    {$xaction .= '{pass}';}
	   }
	   
	   if($idtype==3 && FACULTY_EVALID==1){
		  if(strpos($email,FACULTY_EMAIL)==false){
			 $xaction .= '{email}';
		     $xaction .= '{pass}';  
		  } 
	   }
	   
	   if($xaction=='' && !file_exists($req_profile)){
		  $xaction .= '{profile}';   
	   }
	$xaction = '';   
       $sess_array = array(
                        'id' => trim($row->username),
                        'username' => $this->mod_main->sanitizer($username),
                        'email' => $this->mod_main->sanitizer($row->email),
		                'lastname' => $this->mod_main->sanitizer($lastname),
		                'firstname' => $this->mod_main->sanitizer($firstname),
                        'idtype' => $row->idtype,
                        'idno' => trim($row->idno),
                        'appno' => trim($row->appno),
                        'appcount' => trim($row->appcount),
                        'photo' => $img,
                        'lock' => 0,
                        'minify' => ($row->minify?"minified":""),
                        'xaction' => $xaction
                         );
	   
       $this->session->set_userdata('logged_in', $sess_array);
	   $sessionid = $this->session->userdata('session_id');
	   $this->mod_main->UpdateSession($uname,$sessionid);
       $this->mod_main->TransLog($username,'Login Success','Uname:'.$uname);
	   //log_message('error','Session Created');
     }
     return TRUE;
   }
   else
   {
     $this->mod_main->TransLog($username,'Login Failed','-');
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
  }
  
  public function trial(){
	  $this->load->library('email');
	  $config = $this->email;
	  var_dump($config);
  }
  
  public function contactus(){
	 $email = "bedistaportal.help@sanbeda.edu.ph";
     $sname = $this->input->post('txtname');
     $stdno = $this->input->post('txtstdno');
     $smsg  = $this->input->post('txtmsg');
	 $data  = array();
	 $data['msg'] = "<h3>Sir/Ma'am:</h3>
					 <p class='lead'>Mr/Mrs. ".$sname."(".$stdno.") was having problem in logging in to his/her account.</p>
					 <p>See his/her message below:</p>
					 <br/>
					 <p>".$smsg."</p>
					 <br/><br/><br/>";
   //$message = $this->load->view('templates/reg_mail', $data, TRUE);
	 $msg = $this->load->view('mail/mail_template', $data, TRUE);
	 if(!$this->mod_main->xsendmail('',$email,$msg,'User('.$stdno.') was trying to contact us!',$data,'mail/mail_template')){
	   $this->mod_main->TransLog($stdno,'Email Failed','email:'.$email);
	 }else{
	   $this->mod_main->TransLog($stdno,'Contact Us Email Sent',$stdno);
	 }  
	 
	 echo json_encode(array('success'=>true,'message'=>'Email Sent!','content'=>'')); 
  }
  
}
/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
?>