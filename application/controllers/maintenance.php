<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Maintenance extends CI_Controller 
{
 function __construct()
 {
  parent::__construct();
  date_default_timezone_set("Asia/Taipei");
  $this->load->model('mod_main','',true);
 }
 
 function index()
 {
  if(!$this->session->userdata('logged_in')){redirect('login','refresh');}
  $session_data = $this->session->userdata('logged_in');
  $this->mod_main->monitoring_action($session_data,'maintenance');
  $data['userinfo'] = $session_data;
  $data['title']='Maintenance';
  $data['maintenance'] = MAINTENANCE_MODE; 
  $this->load->view('include/header',$data);
  $this->load->view('templates/maintenance',$data);
  $this->load->view('include/footer',$data);	 
 }
}
?>