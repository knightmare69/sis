<?php
class Notify extends CI_Controller
{

   public function index(){
      $this->load->view('notify/mail');  
   }

   public function mail(){
    $this->load->view('notify/mail');
   }
   
   public function notification(){
      if($this->session->userdata('logged_in')) {
         $session_data = $this->session->userdata('logged_in');
         $data['userinfo'] = $session_data;
         $this->load->model('user','',TRUE);
         $data['notes'] =  $this->user->check_notification($session_data['idno']);
         $this->load->view('notify/notifications',$data);
      } 
   }
   
   public function tasks(){
    $this->load->view('notify/tasks');
   }
}
