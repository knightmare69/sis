<?php
class NuSoapClient extends CI_Controller {
	
	private $wsdl,$client;
	
	function __construct() {
		parent::__construct();
		global $wsdl, $client;
		$this->load->library("nuSoap_lib");
		$proxyhost = isset($_POST['proxyhost'])? $_POST['proxyhost'] : '';
		$proxyport = isset($_POST['proxyport'])? $_POST['proxyport'] : '';
		$proxyusername = isset($_POST['proxyusername'])? $_POST['proxyusername'] : '';
		$proxypassword = isset($_POST['proxypassword'])? $_POST['proxypassword'] : '';
		
		$wsdl = base_url().'nuSoapServer?wsdl';
		//$wsdl = 'localhost:69/cts/index.php/webservice/nuSoapServer/index?wsdl';
		$client = new nusoap_client($wsdl,'wsdl',$proxyhost,$proxyport,$proxyusername,$proxypassword);
		
		$err = $client->getError();
		
		if($err)
		{
		  echo '<h2>Constructor Error</h2><pre>'.$err.'</pre>';	
		}
	}
	
	public function index()
	{ 
	  global $wsdl, $client;
	  try
	  {
		$param  = array('tmp' => 'ABCD');
        $result = $client->call('getMember',array('tmp'=>'ABCD'),'','',false,true);
		echo $wsdl;
		echo '<h2>Result</h2>'; 		
        echo '<pre>';
        print_r($result); 		
        echo '</pre>'; 		
	  
	    echo '<h2>Request</h2><pre>'.htmlspecialchars($client->request,ENT_QUOTES).'</pre>';
	    echo '<h2>Reponse</h2><pre>'.htmlspecialchars($client->response,ENT_QUOTES).'</pre>';
	  }
	  catch(SoapFault $exception)
	  {
		echo $exception;  
	  }
	}
}
?>	