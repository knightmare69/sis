<?php
class NuSoapServer extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library("nuSoap_lib");
		$this->nusoap_server = new soap_server();
		$this->nusoap_server->configureWSDL("Member", "urn:Member");
		$this->nusoap_server->register(
			"getMember",
			array("tmp" => "xsd:string",),
		    array("return"=>"xsd:string"),
			"urn:Member",
			"urn:Member#getMember",
			"rpc",
			"encoded",
			"Returns the information of a certain member"
		);
		
		
		function getMember($tmp) 
		{
		  if(!$tmp)
			return new soap_fault('-1','Server','Parameters missing for getMember','Please refer documentation');
		  else
			return "from Member() : $tmp";
		}
	}
	
	function index() 
	{
	  $this->nusoap_server->service(file_get_contents("php://input"));
	}
}
?>