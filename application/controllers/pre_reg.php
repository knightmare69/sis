<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class pre_reg extends CI_Controller {
  function __construct()
  {
    parent::__construct();    
	date_default_timezone_set('asia/taipei');
  }
  
  function index(){
      /* define('FPDF_FONTPATH',APPPATH .'plugins/font/');
      require(APPPATH .'plugins/fpdf.php');
      require(APPPATH .'plugins/fpdi/fpdi.php');
    
      $pdf = new FPDF('p','mm','A4');
      $pdf -> AddPage();
    
      $pdf -> setDisplayMode ('fullpage');
    
      $pdf -> setFont ('times','B',20);
      
      //$pdf -> cell(200,30,"San Beda College Alabang",0,1);
      $pdf->Cell(200,10,'San Beda College Alabang',0,1,'C');  
      $pdf->Ln(20);
    
      $pdf -> setFont ('times','B','20');
      $pdf -> write (10,"Description");
    
      //$pdf -> output ('your_file_pdf.pdf','D');
      $pdf -> output ();    */
      
      if($this->session->userdata('logged_in')) {        
        $this->load->view('reports/pre_registrations');
      } else { redirect('login', 'refresh'); }
    
  }

  
}
?>