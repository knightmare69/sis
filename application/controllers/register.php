
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Register extends CI_Controller {

 function __construct() {
   parent::__construct();   
   $this->load->helper(array("form","captcha"));
   $this->load->model('mod_main','',TRUE);   
 }

 function index(){
  if($this->session->userdata('logged_in')){
   $session_data = $this->session->userdata('logged_in');
   if($session_data['idtype'] == 0)
	redirect('activation', 'refresh');      
   else
	redirect('home', 'refresh');      
  }else{       
    if(!$this->session->userdata('registerinfo'))
	 $data['registered'] = $this->mod_main->filter_ip(xidentifyuser());
	else{
	 $registration_data = $this->session->userdata('registerinfo'); 
	 $data['registered'] = ((array_key_exists('registered',$registration_data))?$registration_data['registered']:0);
    }	
	$data['title'] = "Register";
	if(defined('ENABLE_CAPTCHA') && ENABLE_CAPTCHA==1){
	 $xcode = $this->mod_main->getRandomString(5) ;
	 $data['captcha'] = $this->mod_main->create_captcha($xcode);
	}
	   
	$this->load->view('register\vw_register', $data);		
  }		
 }
 
 function txn($type,$mode='',$args='')
 {
   $xpost  = $this->input->post('NULL',TRUE);
   $result = array('success'=>false,'content'=>'','error'=>'Invalid Parameters');
   switch($type){
	 case 'get':
	  $result['success']=false;
     break;
     case 'set':
	  switch($mode){
		case 'add_register':
		   //This method will have the credentials validation
		   $this->load->library('form_validation');   
		   $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('passwordConfirm', 'Password Confirmation', 'trim|required|matches[password]');
		 
		   if($this->form_validation->run() == TRUE)
		   {
			$data_username = is_key_exist($xpost,'username');
			$data_email    = is_key_exist($xpost,'email');
			$this->mod_main->TransLog($data_username,'Register Form Validated','-');
			
			$check = $this->checkusername($data_username);
			if($check=='true' || $check=='invalid'){
			 $this->mod_main->TransLog($data_username,'User ReEntered','-');
			 $exec = $this->user->get_user($data_username);
			}else{
			 $exec = $this->user->add_user();
			 $this->mod_main->TransLog($data_username,'User Added','-');
			}
				 
			if($exec){
			 $username = $data_username;
			 $email = trim($data_email);         
			 $verify = explode('|', $result);         
			 $today = new DateTime('NOW');
			 date_add($today,date_interval_create_from_date_string("15 days"));
			 $data['username'] = $username;
			 $data['email'] = $email;
			 $data['title'] = "BEDISTA Online:Registration";
				 
			 $data['verify'] = site_url('register/verify').'/' .$verify[0]; 
		     $data['today']  = $verify[1] ; //$today->format( 'Y-m-d H:i:s' );
			 $data['msg']    = "<h3>Hi, ".$data['username']."</h3>
								<p class='lead'>We would like to thank you for registering your e-mail account to BEDISTA Portal.</p>
								<p>You've entered ".mailto($email,$email)." as the contact e-mail address for your BEDISTA Portal. To complete the process, we just need to verify that this e-mail address belongs to you.
								 Simply click the link below and sign in using your BEDISTA Portal username and password.
								</p>
								<!-- Callout Panel -->
								<p class='callout'>
								 <a href='".$data['verify']."'>Verify Now! &raquo;</a> <i> This validation code is valid until ".$data['today'].". Your account will be automatically deleted if the you didn't verify within the validity period of verification.</i>
								</p><!-- /Callout Panel -->";
			 //$message = $this->load->view('templates/reg_mail', $data, TRUE);
			 /*
			 $data['msg']    = "<h3>Peace,</h3>
						 <p class='lead'>We would like to thank you for registering to BEDISTA Portal.</p>
						 <p>You've entered ".mailto($email,$email)." as the contact e-mail address. To complete the process, we just need to verify that this e-mail address belongs to you.Simply click the link below and log-in using your BEDISTA Portal username and password.
						 </p>
						 <!-- Callout Panel -->
						 <p class='callout'>
						  <a href='".$data['verify']."'>Verify Now! &raquo;</a> <i> This validation code is valid until ".date(strtotime($data['today']),"M-d-Y h:i A").". Your account will be automatically deleted if you didn't verify within the validity period of verification.</i>
								</p><!-- /Callout Panel -->";
			 //$message = $this->load->view('templates/reg_mail', $data, TRUE);
			 */
			 $msg = $this->load->view('mail/mail_template', $data, TRUE);
			 $this->session->set_userdata('registerinfo',array('username'=>$username,'email'=>$email));
			 if(EMAIL_NOTIFY==1){
			  if(!$this->mod_main->xsendmail('',$email,$msg,'Action Required: Please confirm your BEDISTA Portal account',$data,'mail/mail_template')){
			   $this->mod_main->TransLog($data_username,'Email Failed','email:'.$email);
			   $this->session->set_userdata('error',array('email' => 'failed'));
			  }
			 }
			 
			 //echo $this->email->print_debugger();
			 $this->mod_main->TransLog($data_username,'Registration Success','-');
			 redirect('register/success', 'refresh'); 
			}
			$this->mod_main->TransLog($data_username,'Registration Failed','-');   
			redirect('register', 'refresh');
		   }else{
			 //Go to private area
			 $this->mod_main->TransLog('-','Form Validation Failed','-');   
			 redirect('register', 'refresh');
		   }
        break;		
	  }
	  $result['success']=true;
     break;	 
   }
   echo json_encode($result);   
 }
 
 function success(){
  $registration_data = array('username'=>'','email'=>'');
  if($this->session->userdata('registerinfo')){$registration_data = $this->session->userdata('registerinfo'); }
  if($this->session->userdata('error')){
   $data['error'] = $this->session->userdata('error');
   $this->session->unset_userdata('error');
  }else{
    if(defined('EMALPERPC') && EMALPERPC==1){
	  $registration_data['registered'] = date('Ymd');
      $this->session->set_userdata('registerinfo',$registration_data);   
    } 
  }
  $data['reginfo'] = $registration_data;
  $data['title']   = "Register";
  $this->load->view('register\vw_register_success', $data);
  $this->mod_main->TransLog('-','Registration Success','-');     
 }
 
 function trial(){
  $registration_data = array();
  if($this->session->userdata('registerinfo')){$registration_data = $this->session->userdata('registerinfo'); }
  if($this->session->userdata('error')){
   $data['error'] = $this->session->userdata('error');
   $this->session->unset_userdata('error');
  }else{
    if(defined('EMALPERPC') && EMALPERPC==1){
	  $registration_data['registered'] = date('Ymd');
      $this->session->set_userdata('registerinfo',$registration_data);   
    } 
  }
  
  $data['title'] = "Register";
  $this->load->view('register\vw_register_success', $data);
  $this->mod_main->TransLog('-','Registration Success','-');    
 }
 
 function verify($verification){
   $this->load->model('user','',TRUE);
   $data['title'] = "Verify";
   
   $row = $this->user->verifyacct($verification);
   
   foreach($row as $rs){
	$data['verify']    = $rs->verify;
    $data['code']      = $rs->validcode;
    $data['activated'] = $rs->activated;
   }
   
   if($data['verify']==0 && $data['activated']==0)
    $this->mod_main->TransLog('-','Verification Failed',$verification);     
   else{
    $this->mod_main->TransLog('-','Verification',$verification);     
	$result = $this->db->query("SELECT TOP 1 * FROM vw_Memberships WHERE userid=(SELECT TOP 1 UserID FROM ES_Membership WHERE ValidationCode='".$verification."')");
    if($result){
     $sess_array = array();
     foreach($result->result() as $row){
	   
	   $uname  = trim($row->username);
	   $idno   = trim($row->idno);
	   $idtype = trim($row->idtype);
	   $email  = trim($row->email);
	   
	   $imgpath = 'assets/img/profile/'.md5($uname.$idno);
	   $img = $this->mod_main->CheckImage($imgpath);
	   $img = $this->mod_main->SetProfile($img);	 
       $sess_array = array(
                        'id' => trim($row->username),
                        'username' => $this->mod_main->sanitizer($row->username),
                        'email' => $this->mod_main->sanitizer($row->email),
		                'lastname' => $this->mod_main->sanitizer($row->lastname),
		                'firstname' => $this->mod_main->sanitizer($row->firstname),
                        'idtype' => $row->idtype,
                        'idno' => trim($row->idno),
                        'appno' => trim($row->appno),
                        'appcount' => trim($row->appcount),
                        'photo' => $img,
                        'lock' => 0,
                        'minify' => ($row->minify?"minified":""),
                        'xaction' => ''
                         );
	   
       $this->session->set_userdata('logged_in', $sess_array);
	 }	    
    }
   }
	
   $this->load->view('register\vw_register_verify', $data);	
 }
 
 
 function checkusername(){
  $this->load->model('user','',TRUE);
  $username  = $_REQUEST['username'];
  $row = $this->user->getInfo($username);
  //print_r($row);
  
  if($row){
   echo 'false';   
  } else { echo 'true'; }
 }
 
 function checkemail(){
  $this->load->model('user','',TRUE);
  $username ='';
  $email  = $_REQUEST['email'];
  if(array_key_exists('username',$_REQUEST))
  {
  $username  = $_REQUEST['username'];
  }
  
  $row = $this->user->getInfobyemail($email,$username);
  
  if($row)
   echo 'false';   
  else
   echo 'true';
 }
 
 function regenerate_captcha()
 {
  $xcode = $this->mod_main->getRandomString(5) ;
  echo json_encode($this->mod_main->create_captcha($xcode));
 }
 
}

?>

