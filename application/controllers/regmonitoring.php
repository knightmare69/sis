<?php
class Regmonitoring extends CI_Controller{

function __construct()
{
 parent::__construct();
 $this->load->model('mod_main','',true);
 $this->load->model('mod_regmonitoring','',true);
}

function index()
{
 if($this->session->userdata('logged_in'))
 {
  $session_data = $this->session->userdata('logged_in');
  if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
  if ($session_data['idtype'] == 0){ redirect('activation', 'refresh'); } 
  if ($session_data['idtype'] != -1){ redirect('profile', 'refresh'); } 
  if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
 
  $data['title'] = "Registration Monitoring";
  $data['userinfo'] = $session_data;
  $data['username'] = $session_data['username'];
  $data['idtype'] = $session_data['idtype'];
  $data['idno'] = $session_data['idno'];
	
  $data['tbdata'] = $this->mod_regmonitoring->GenerateTable($termid=93,$progid=0,$regid=0);
  $data['tbcount'] = $this->mod_regmonitoring->GetTotalCount($termid=93,$progid=0,$regid=0);
 
  $this->load->view('include/header',$data);
  $this->load->view('templates/mainmenu',$data);
  $this->load->view('vw_regmonitoring', $data);
  $this->load->view('include/footer','');
  //$this->mod_main->Translog($session_data['id'],'View Users Monitoring','-');
 }
 else
 {
  redirect('login', 'refresh');
 }
}

}
?>