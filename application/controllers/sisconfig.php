<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Sisconfig extends CI_Controller 
{
 
  function __construct()
  {
   parent::__construct();
   $this->load->helper('form');
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_sisconfig','xconfig',TRUE);
  }
  
  function index()
  {
   if($this->session->userdata('logged_in'))
   {
   $session_data = $this->session->userdata('logged_in');
   if($session_data['idtype']!=-1){redirect('login','refresh');}
   if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
   if ($session_data['lock'] == 1){redirect('lock', 'refresh');} 
   
   $data['userinfo'] = $session_data;
   $data['title'] = "Admin Controls";
   $data['title1'] = "Configuration";
   $data['idno'] = $session_data['idno'];
   $data['username'] = $session_data['username']; 
   $data['xlink']='sisconfig/saveconfig/';
   $data['configlist'] = $this->xconfig->loadlist(1);
   $data['jslink'] = array('utilities/sisconfig.js');
   $this->load->view('include/header',$data);
   $this->load->view('templates/mainmenu',$data);
   $this->load->view('vw_sisconfig','');
   $this->load->view('include/footer',$data);
   $this->mod_main->Translog($session_data['id'],'View System Config','-');
   }
   else
   {
    redirect('login','refresh');
   }
  }
  
  function facultyconfig()
  {
   if($this->session->userdata('logged_in'))
   {
   $session_data = $this->session->userdata('logged_in');
   if($session_data['idtype']!=-1){redirect('login','refresh');}
   if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
   if ($session_data['lock'] == 1){redirect('lock', 'refresh');} 
   
   $data['userinfo'] = $session_data;
   $data['title'] = "Admin Controls";
   $data['title1'] = "Faculty Portal Configuration";
   $data['idno'] = $session_data['idno'];
   $data['username'] = $session_data['username']; 
   $data['xlink']='sisconfig/saveconfig/2.2';
   $data['configlist'] = $this->xconfig->loadlist(3);
   $data['jslink'] = array('utilities/sisconfig.js');
   $this->load->view('include/header',$data);
   $this->load->view('templates/mainmenu',$data);
   $this->load->view('vw_sisconfig','');
   $this->load->view('include/footer',$data);
   $this->mod_main->Translog($session_data['id'],'View Faculty Config','-');
   }
   else
   {
    redirect('login','refresh');
   }
  }
  
  function saveconfig($x='2.1')
  {
   $session_data = $this->session->userdata('logged_in');
   $input=$this->input->post();
   //$this->mod_main->arr_detail($x);
   if(count($input)>0 and is_array($input))
   {
    foreach($input as $key => $value)
    {
	 if($x=='2.1')
 	   $this->xconfig->sp_xconfigMgmt($key,$value);
     else 
       $this->xconfig->sp_configMgmt($x,$key,$value);
     
	 $this->mod_main->Translog($session_data['id'],'Alter Configuration','Key:'.$key.' Val:'.$value);
	}
   }
   
   if($x=='2.1')
    redirect('sisconfig', 'refresh');
   else
    redirect('facultyconfig', 'refresh');
  }

}
?>