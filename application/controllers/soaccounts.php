<?php 
Class SOAccounts extends CI_Controller
{
 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_main','',true);
   $this->load->model('mod_soa','',true);
   $this->load->model('mod_crystalreports','',true);
 }

 function index()
 {
     $session_data = $this->session->userdata('logged_in');
	 if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 
	 $data['userinfo'] = $session_data;
	 $data['title'] = "SOA";
     $data['studentno'] = $session_data['idno'];
     $data['username'] = $session_data['username'];     
	 
	 $AYTerms = $this->mod_soa->sp_AYTerms($data['studentno']); 
	 $data['reginfo'] = $this->mod_soa->sp_GetRegInfo($data['studentno'],$AYTerms[0]->RegID);
	 $campus=$data['reginfo']->CampusID;
	 $college=$data['reginfo']->CollegeID;
	 $termid=$data['reginfo']->TermID;
	 $progid=$data['reginfo']->ProgID;
	 $yrlvlid=$data['reginfo']->YearLevelID;
	 $data['studentinfo'] = $this->mod_soa->sp_SOA($data['studentno'],$campus,$termid,$college,$progid,$yrlvlid)->row();
	 $data['assessment']=$this->mod_soa->tblAssessment($data['studentno'],$campus,$termid,$college,$progid,$yrlvlid);
	 $data['payment']=$this->mod_soa->tblPayment($data['studentno'],$campus,$termid,$college,$progid,$yrlvlid);
	 $data['cboAYTerm']=$this->mod_soa->cboRegAYTerms($data['studentno'],$AYTerms[0]->RegID); 
	 
	 $footer['jslink'] = array('utilities/soaccounts.js');
	 
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_soa', $data);
     $this->load->view('include/footer',$footer);
	 //$this->mod_main->Translog($session_data['id'],'View Advising Controller','-');
 }

 function refreshdata($opt=0,$regid=0,$studno='')
 {
  $campus=0;
  $college=0;
  $termid=0;
  $progid=0;
  $yrlvlid=0;
  
  if($opt!=0 and $studno!='')
  {
  
  $AYTerms=$this->mod_soa->sp_AYTerms($studno);
  if($regid==0 and $studno!='')
   $regid = $AYTerms[0]->RegID;
  
  
  $xreginfo=$this->mod_soa->sp_GetRegInfo($studno,$regid);
  if($xreginfo)
  {
   $campus=property_exists($xreginfo,'CampusID')?$xreginfo->CampusID:0;
   $college=property_exists($xreginfo,'CollegeID')?$xreginfo->CollegeID:0;
   $termid=property_exists($xreginfo,'TermID')?$xreginfo->TermID:0;
   $progid=property_exists($xreginfo,'ProgID')?$xreginfo->ProgID:0;
   $yrlvlid=property_exists($xreginfo,'YearLevelID')?$xreginfo->YearLevelID:0;
  }
  
   if($opt==1 and $regid!=0) //information
   {
    $studentinfo = $this->mod_soa->sp_SOA($studno,$campus,$termid,$college,$progid,$yrlvlid)->row();
	$cboAYTerm=$this->mod_soa->cboRegAYTerms($studno,$regid); 
	if($studentinfo)
	{
    echo '<table class="table table-bordered">
	       <tbody>
			 <tr><td width="35%">Academic Year & Term:</td><td>'.$cboAYTerm.'</td></tr>
			 <tr><td width="35%">Name:</td><td>'.$studentinfo->Student.'</td></tr>
			 <tr><td width="35%">StudentNo:</td><td>'.$studentinfo->StudentNo.'</td></tr>
			 <tr><td width="35%">Campus:</td><td>'.$studentinfo->CampusName.'</td></tr>
			 <tr><td width="35%">College:</td><td>'.$studentinfo->CollegeName.'</td></tr>
			 <tr><td width="35%">Program:</td><td>'.$studentinfo->ProgName.'</td></tr>
			 <tr><td width="35%">Year Level:</td><td>'.$studentinfo->YearLevel.'</td></tr>
			</tbody>
		 </table>';
    }
	else
	{
	 echo'<table class="table table-bordered">
	       <tbody>
			 <tr><td colspan="2"><center>No Record Found!</center></td></tr>
			</tbody>
		  </table>';
	}
   }
   elseif($opt==2 and $regid!=0) //assessment
   {
    echo $this->mod_soa->tblAssessment($studno,$campus,$termid,$college,$progid,$yrlvlid); 
   }
   elseif($opt==3 and $regid!=0) //payment
   {
	echo $this->mod_soa->tblPayment($studno,$campus,$termid,$college,$progid,$yrlvlid);
   }
   
  }
  else
  {
   echo 'nodata';
  }
 }
 
 function printSOA($xregid=0)
 {
  $session_data = $this->session->userdata('logged_in');
  if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 
  $data['userinfo'] = $session_data;
  $data['username'] = $session_data['username'];     
  
  $studno  = $session_data['idno'];
  $regid   = $xregid;
  $xreginfo = $this->mod_soa->sp_GetRegInfo($studno,$regid);
  $campus  = $xreginfo->CampusID;
  $college = $xreginfo->CollegeID;
  $termid  = $xreginfo->TermID;
  $progid  = $xreginfo->ProgID;
  $yrlvlid = $xreginfo->YearLevelID;
 
  $file='none';
  $filename='none';
	
  if($studno!='' && $studno!=' ')
  {
    list($file,$filename)= $this->mod_crystalreports->prepare_SOA($studno,$campus,$termid,$college,$progid,$yrlvlid);
		
	if($file!='none' && $filename!='none')
	{
	  header('Content-type: application/pdf');
      header('Content-Disposition: inline; filename="' . $filename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Accept-Ranges: bytes');
      @readfile($file);
	  unlink($file);
	  $this->mod_main->Translog($session_data['id'],'Print SOA','-');
	}
	else
	{echo 'Error Loading the report. Please Report to the administrators.';}
  }
  else 
  { redirect('soaccounts', 'refresh'); }
 }
}
?>