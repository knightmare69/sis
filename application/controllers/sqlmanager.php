<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Sqlmanager extends CI_Controller {

 function __construct()
 { 
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->db->db_debug = FALSE;
 }
 
 function index()
 {
   if($this->session->userdata('logged_in')) {
     $session_data = $this->session->userdata('logged_in');
     if ($session_data['idtype'] == 0){ redirect('activation', 'refresh'); }
     if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 
	 $data['userinfo'] = $session_data;
     $data['title'] = "SQL Manager";
     $data['username'] = $session_data['username'];
     $data['idtype'] = $session_data['idtype'];
     $data['photo'] = $session_data['photo'];
	 $footer['jslink'] = array('utilities/sqlquery.js',);
	 
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
	 $this->load->view('vw_sqlmanager',$data);
	 $this->load->view('include/footer',$footer);
	 $this->mod_main->Translog($session_data['id'],'View SQL Manager','-');
	 
   }
 }
 
 function txn()
 {
  $output = array('result'=>false,'content'=>'','error'=>'Nothing to Execute');
  $session_data = $this->session->userdata('logged_in');
  $p = $this->input->post();
  if($p!='')
  {
	    if(array_key_exists('query',$p) &&  $p['query']!='')
		{
		 try
		 {
		  $execute = $this->db->query($p['query']);
		  //log_message('error',$this->userid.' Execute query:'.$p['query']);
		  if(is_bool($execute) && $execute==true)
		  {
		   $output['result'] = true;   
		   $output['content'] = 'Command Successfully Executed';	
		   if($this->db->affected_rows()>0)
		   {
			$row = $this->db->affected_rows();
			$output['content'] = $row.' Record(s) Was Affected';   
		   }	   
		  }		  
		  else if(is_bool($execute) && $execute==false)
		   $output['error']=$this->db->_error_message();	 
		  else
		  {
		   $thead='';
		   $tbody='';
		   
		   $result = $execute->result();
		   foreach($result as $rs)
		   {
			$thead='';
			$trow='';
			foreach($rs as $key=>$val)
			{
			 if($key!='Password' && $key!='StudentPicture')
			 {
			  $thead.='<th>'.utf8_encode($key).'</th>';
			  $trow.='<td>'.utf8_encode($val).'</td>';		
			 }
			 else		
			 {	
			  $thead.='<th>'.utf8_encode($key).'</th>';
			  $trow.='<td>BINARY VALUE</td>';		
			 }
			}
			
			if($thead!=''){$thead='<tr>'.$thead.'</tr>'; }
			if($trow!=''){$tbody.='<tr>'.$trow.'</tr>'; }
		   }
		   
		   if($thead!=''||$tbody!=''){
			$output['result'] = true;   
			$output['content'] = '<table class="table table-bordered" style="white-space:nowrap;">
								   <thead>'.$thead.'</thead>
								   <tbody>'.$tbody.'</tbody>
								  </table>';
		   echo json_encode($output);
		   die();
		   }	  
		  }
		 }
		 catch(Exception $e)
		 {
		  $output['error'] = $this->db->_error_message();
		 }	 
		}
	    echo json_encode($output,JSON_PARTIAL_OUTPUT_ON_ERROR);
  }
  else
  {
   echo json_encode($output);	  
  }	  
  
 }
 
 
}
//DV-1690 Brothel That’s Free If You Can Ejaculate 3 Times in Tsukasa Aoi
?>