<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Subscribe extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('mod_main','',TRUE);
    $this->load->model('mod_subscribe','',TRUE);
    $this->load->helper('url');
  }
 
 function index()
 {
    $studentno=StripSlashes($this->input->post('studentno'));
     
   if($this->session->userdata('logged_in'))
   {
        $this->load->helper(array("form"));		
        $session_data = $this->session->userdata('logged_in');
	    if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	    if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
		
        $data['username'] = $session_data['username'];
        $data['userid'] = $session_data['id'];
        $data['userinfo'] = $session_data;
        $data['title'] = "Subscription";
        $data['ayterm'] = $this->mod_subscribe->get_ayterms();
        
        if ($studentno != '')
        {
          
            $data['student'] = $this->mod_subscribe->get_StudentInfo($studentno);
            
            if ( count($data['student']) == 0)
            {
              $data['studentno'] = $studentno;
              $data['name'] = '';
              $data['degree'] = '';
            }
            else
            {
              $data['studentno'] = $studentno;
              $data['name'] = $data['student'][0]->StudentName;
              $data['degree'] = $data['student'][0]->ProgName;
            }
        }
        else
        {
          $data['student'] = '';
          $data['studentno'] = '';
          $data['name'] = '';
          $data['degree'] = '';
        }

        //$data['validate'] = $this->mod_subscribe->validate($studentno, $birthdate, $termid, $regid);

        $data['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                           'plugin/fuelux/wizard/wizard.js',
                           'plugin/jquery-form/jquery-form.min.js',
                           'utilities/activation.js',
                           'utilities/subscribe.js'
                           );
        
        $this->load->view('include/header',$data);
        $this->load->view('templates/mainmenu',$data);
        $this->load->view('vsubscribe', $data);
        $this->load->view('include/footer',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
 
 
  function create()
  {
    $hiddenstudentno=StripSlashes($this->input->post('hiddenstudentno'));
    $parentid=StripSlashes($this->input->post('userid'));
    $relationship=StripSlashes($this->input->post('relationship'));
 
    if ($relationship=='')
    {
     $relationship2 = 'Family';
    }
    else
    {
     $relationship2 = $relationship;
    }

    $confirm = $this->mod_subscribe->icreate($parentid, $hiddenstudentno, $relationship2);
    redirect('subscribe', 'refresh');
  }
  
  function validatestudentinfo(){
  
  $hiddenstudentno=StripSlashes($this->input->post('studno'));
  $birthdate=StripSlashes($this->input->post('birth'));
  $termid=StripSlashes($this->input->post('term'));
  $regid=StripSlashes($this->input->post('registration'));
  $receipt=StripSlashes($this->input->post('receiptno'));
  $date=StripSlashes($this->input->post('payment'));
  
   $data['ayterm'] = $this->mod_subscribe->get_ayterms();
         
   if ($termid=='')
   {
    $termid2 = $data['ayterm'][0]->TermID;
   }
   else
   {
    $termid2 = $termid;
   }
   //$this->MessageBox($hiddenstudentno . ' ' . $birthdate . ' ' . $termid2 . ' ' . $regid . ' ' . $receipt . ' ' . $date);
  
  $data['validate'] = $this->mod_subscribe->validate($hiddenstudentno, $birthdate, $termid2, $regid, $receipt, $date);
  $valid = $data['validate'][0]->Validate;

  echo $valid;
 }

}
?>