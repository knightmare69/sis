<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class SubscribeStudentList extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('mod_subscribestudentlist','',TRUE);
    $this->load->model('mod_main','',TRUE);
    $this->load->helper('url');
  }
 
 function index()
 {
   if($this->session->userdata('logged_in'))
   {
        $this->load->helper(array("form"));		
        $session_data = $this->session->userdata('logged_in');
		if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	    if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
		
        $data['username'] = $session_data['username'];
        $data['userinfo'] = $session_data;
        $username = $session_data['id'];
        
        $data['title'] = "Subcription";
        $data['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                           'plugin/fuelux/wizard/wizard.js',
                           'plugin/jquery-form/jquery-form.min.js',
                           'utilities/activation.js',
                           'utilities/subscribestudentlist.js'
                           );
        $data['pass'] = $this->mod_main->getUserInfo($username);
        $data['password'] = $data['pass'][0]->password;
        $data['student'] = $this->mod_subscribestudentlist->get_StudentInfo($username);
  
        
        $this->load->view('include/header',$data);
        $this->load->view('templates/mainmenu',$data);
        $this->load->view('vsubscribestudentlist', $data);
        $this->load->view('include/footer',$data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

  function unsubscribe()
  {
    $indexid = StripSlashes($this->input->post('indexid'));
    $confirm = $this->mod_subscribestudentlist->unsubscribe($indexid);
    
    if ($confirm>=1){
     //$this->MessageBox('Record deleted!');
     redirect('subscribestudentlist', 'refresh');
    }
    
  }
 
 function keep(){
  $this->messagebox(site_url("news/local/123"));
  //redirect('utilities/religion', 'refresh');
  
 }
 
 function countstudent(){
  $session_data = $this->session->userdata('logged_in');
  $username = $session_data['id'];
  $data['countstudent'] = COUNT($this->mod_subscribestudentlist->get_StudentInfo($username));
  
  if ($data['countstudent'] <= 1){
    echo 0;
  }else{
    echo 1;
  }
  
 }
 
 function checkpwd(){
  $session_data = $this->session->userdata('logged_in');
  $username = $session_data['id'];
  $data['countstudent'] = COUNT($this->mod_subscribestudentlist->get_StudentInfo($username));
  $data['pass'] = $this->mod_main->getUserInfo($username);
  $password = $data['pass'][0]->password;
        
  $userpassword=$this->input->post('pwd');
  
  if ($password == MD5($userpassword))
  {echo 1;}
  else
  {echo 0;} 
 }
 
}

?>