
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Sysusers extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->helper(array("form"));		
   $this->load->model('mod_sysusers','',TRUE);              
   
   
   if($this->session->userdata('logged_in'))
   {
     $this->sys_users();
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
   
 }

 function index()
 {
   
 }
 function sys_users(){
 	
	$sel = $this->uri->segment(2);
	$columns = '';
	$ntitle='Employees';
	$table = 'employees';		
	
	switch($sel){
		case "employees": 
			$ntitle='Employees';	
			$table = 'employees';		
			break;
		case "useraccount": $ntitle='User Accounts';
			$table = 'useraccount';		
			break;		
		case "usergroups": 
			$ntitle='User Groups';
			$table = 'usergroups';
			break;												
		default:
			break;
	}

    $session_data = $this->session->userdata('logged_in');
    $data['username'] = $session_data['username'];
	$data['title'] = "System Users :: " . $ntitle;
	$data['module'] = $ntitle;
	$data['activemodule'] = $ntitle;
//	$data['javascript'] = array('utilities/utilities.js');
	$data['mods'] = $this->mod_sysusers->menus();
	//$data['fields'] = $this->mod_utilities->cs_getfields($table);
//	$data['recordset'] = $this->mod_utilities->cs_getdata($table);	
	$this->load->view('include/header', $data);
	$this->load->view('templates/mainmenu', $data);
	$this->load->view('vsysusers', $data);     
//	$this->load->view('utilities/cs_modal', $data);     
	$this->load->view('include/footer', $data);  
 }

function employees(){
	
}

function useraccount(){
	
}
function usergroups(){
	
}

}

?>

