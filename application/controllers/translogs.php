<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Translogs extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   date_default_timezone_set("Asia/Taipei");
   $this->load->model('mod_main','',TRUE);	
   $this->load->model('mod_translogs','',TRUE);	
 }
 
 function index()
 {
  $session_data = $this->session->userdata('logged_in');
  if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
  if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 
  $data['userinfo'] = $session_data;
  $data['title'] = "Admin Controls";
  $data['title1'] = "Transactions Log";
  $data['idno'] = $session_data['idno'];
  $data['username'] = $session_data['username'];
  $row= $this->mod_translogs->xgetTranslogs('',20,0);
  list($data['logs'],$data['lastrow'],$data['latestrow'],$data['listid'])= $this->mod_translogs->processtranslog($row);
  $data['rcount']=$this->mod_translogs->getcount();
  $data['lastrow']= $this->mod_main->base64encode($data['lastrow']);
  $data['latestrow']= $this->mod_main->base64encode($data['latestrow']);
  $footer['jslink']= array('utilities/translog.js');
  
  $this->load->view('include/header',$data);
  $this->load->view('templates/mainmenu',$data);
  $this->load->view('vw_translog',$data);
  $this->load->view('include/footer',$footer);
 }
 
 function getdata($lastrow='')
 {
  $output = array('result'=>false,'content'=>'','error'=>'');
  $p      = $this->input->post(NULL,TRUE);
  $last   = 'nodata';
  $where  = '';
  
  if($this->session->userdata('logged_in'))
  {
   $limit =$p['limit'];
   $offset=$p['page'];
   $where =$this->filterdata($p);   
   $output['comment']=$where;
   
   $row= $this->mod_translogs->xgetTranslogs($where,$limit,$offset);
   list($output['content'],$last)=$this->mod_translogs->producetranslog($row);
   $output['last']=$this->mod_main->base64encode($last);
   $output['result']=true;
  }
  
  echo json_encode($output,JSON_PARTIAL_OUTPUT_ON_ERROR);
 }
 
 function filterdata($data)
 {
  $result='';
  if($data!='' && is_array($data))
  {
   if(array_key_exists('estart',$data) && $data['estart']!='' && array_key_exists('eend',$data) && $data['eend']!=''){$result = (($result=='')?$result:($result.' AND '))."(EventDate BETWEEN '".$data['estart']."' AND '".$data['eend']."')"; }	   
   if(array_key_exists('uname',$data) && $data['uname']!=''){$result = (($result=='')?$result:($result.' AND '))."(UserID LIKE '%".$data['uname']."%')"; }	   
   if(array_key_exists('ipadd',$data) && $data['ipadd']!=''){$result = (($result=='')?$result:($result.' AND '))."(ComputerName LIKE '%".$data['ipadd']."%')"; }	   
   if(array_key_exists('module',$data) && $data['module']!=''){$result = (($result=='')?$result:($result.' AND '))."(Module LIKE '%".$data['module']."%')"; }	   
   if(array_key_exists('action',$data) && $data['action']!=''){$result = (($result=='')?$result:($result.' AND '))."(Action LIKE '%".$data['action']."%')"; }	   
   if(array_key_exists('param',$data) && $data['param']!=''){$result = (($result=='')?$result:($result.' AND '))."(Parameters LIKE '%".$data['param']."%')"; }	   
  }
  
  return $result;  
 }
 
}
?>