<?php
class usermonitoring extends CI_Controller{

function __construct()
{
 parent::__construct();
 $this->load->model('user','',TRUE);
 $this->load->model('mod_main','',true);
 $this->load->model('mod_activation','',true);
 $this->load->model('mod_usermonitoring','',true);
 $this->load->model('mod_crypto','',TRUE);
 $this->load->library('email');	
 $this->AdptPRISMS = $this->mod_main->getconfigvalue('AdaptSecurity');  
}

function index()
{
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	 if ($session_data['idtype'] == 0){ redirect('activation', 'refresh'); } 
	 if ($session_data['idtype'] != -1){ redirect('profile', 'refresh'); } 
	 if ($session_data['xaction'] != ''){redirect('actionrequired', 'refresh');} 
	 
     $data['title'] = "User Management and Monitoring";
     $data['userinfo'] = $session_data;
     $data['username'] = $session_data['username'];
     $data['idtype'] = $session_data['idtype'];
     $data['idno'] = $session_data['idno'];
	 $data['enable_remad']=0;
	 $data['tbdata'] = $this->mod_usermonitoring->tbl_UserList(0,20,1);
	 $mfooter['jslink'] = array('utilities/usermonitoring.js?20151109');     
	 $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_usermonitoring', $data);
     $this->load->view('include/footer',$mfooter);
	 $this->mod_main->Translog($session_data['id'],'View Users Monitoring','-');
   }
   else
   {
     redirect('login', 'refresh');
   }
}

function management()
{
 $session_data = $this->session->userdata('logged_in');	
 $xpost= $this->input->post();
 
 $opt = ($xpost && array_key_exists('opt',$xpost))?$xpost['opt']:'';
 $opt = str_replace("'",'',$opt);
 $prevuname = ($xpost && array_key_exists('prevuname',$xpost))?$xpost['prevuname']:'';
 $prevuname = str_replace("'",'',$prevuname);
 $username = ($xpost && array_key_exists('username',$xpost))?$xpost['username']:'';
 $username = str_replace("'",'',$username);
 $pwd = ($xpost && array_key_exists('upass',$xpost))?$xpost['upass']:'';
 $pwd = str_replace("'",'',$pwd);
 $lname = ($xpost && array_key_exists('lname',$xpost))?$xpost['lname']:'';
 $lname = str_replace("'",'',$lname);
 $fname = ($xpost && array_key_exists('lname',$xpost))?$xpost['fname']:'';
 $fname = str_replace("'",'',$fname);
 $mname = ($xpost && array_key_exists('mname',$xpost))?$xpost['mname']:'';
 $mname = str_replace("'",'',$mname);
 $miname = ($xpost && array_key_exists('miname',$xpost))?$xpost['miname']:'';
 $miname = str_replace("'",'',$miname);
 $email = ($xpost && array_key_exists('email',$xpost))?$xpost['email']:'';
 $email = str_replace("'",'',$email);
 $telno = ($xpost && array_key_exists('mobile',$xpost))?$xpost['mobile']:'';
 $telno = str_replace("'",'',$telno);
 $idno = ($xpost && array_key_exists('uidno',$xpost))?$xpost['uidno']:'';
 $idno = str_replace("'",'',$idno);
 $idtype = ($xpost && array_key_exists('uidtype',$xpost))?$xpost['uidtype']:0;
 $idtype = str_replace("'",'',$idtype);
 if($idtype== NULL || $idtype== 'null')
 {$idtype=0;}
 
 $active = ($xpost && array_key_exists('active',$xpost))?$xpost['active']:0;
 $active = str_replace("'",'',$active);
 
 $subscript = ($xpost && array_key_exists('subscript',$xpost))?$xpost['subscript']:'';
 
 if($opt=='' && $username=='' && $email=='')
 {
  echo 'nodata';
  die();
 }
 
 $result = $this->mod_usermonitoring->manage_user($opt,$prevuname,$username,$pwd,$lname,$fname,$mname,$miname,$email,$telno,$idno,$idtype,$active);
 if($result && $result!=='')
 {
  if($opt==0){
	 $this->mod_main->Translog($session_data['id'],'Create User','UID:'.$username);
  }else{
	 $this->mod_main->Translog($session_data['id'],'Update User Info','UID'.$username);
  }	 

  if($idtype==2 && $subscript!='')
  {
	$run = $this->mod_usermonitoring->manage_user_subscript(2,$prevuname,$username);	
	$list = explode('@',$subscript);
    foreach($list as $k => $v)
    {
	 $xval = explode('|',$v);
	 $run = $this->mod_usermonitoring->manage_user_subscript(1,$prevuname,$username,$xval[0],$xval[1]);	
	}	
  }
  
  if($opt==0)
  {
   $username = $username;
   $email = trim($email);         
   $verify = explode('|', $result);         
   $today = new DateTime('NOW');
   date_add($today,date_interval_create_from_date_string("15 days"));
   $data['username'] = $username;
   $data['email'] = $email;
   $data['title'] = "PRISMS Online:Registration";
		 
   $data['verify'] = site_url('register/verify').'/' .$verify[0]; 
   $data['today'] = $verify[1] ; //$today->format( 'Y-m-d H:i:s' );
   $data['msg'] = "<h3>Hi, ".$data['username']."</h3>
				 <p class='lead'>We would like to thank you for registering your email account to PRISMS Portal.</p>
				 <p>You've entered ".mailto($email,$email)." as the contact email address for your PRISMS Portal. To complete the process, we just need to verify that this email address belongs to you.
				 Simply click the link below and sign using your PRISMS Portal Username and password.
				 </p>
				 <!-- Callout Panel -->
				 <p class='callout'>
					<a href='".$data['verify']."'>Verify Now! &raquo;</a> <i> This validation code is valid until ".$data['today']."</i>
				 </p><!-- /Callout Panel -->";
 
   $message = $this->load->view('mail/mail_template', $data, TRUE);
 
   if(EMAIL_NOTIFY==1)
   {
    try
    {
     if(!$this->xsendmail('',$email,$message))
     {
      $config['protocol']='smtp';
      $config['smtp_host']='ssl://smtp.googlemail.com';
      $config['smtp_port']='465';
      $config['smtp_timeout']='30';
      $config['smtp_user']='jhe69samson@gmail.com';
      $config['smtp_pass']='knightmare69';
      $config['charset']='utf-8';
      $config['newline']="\r\n";
      $config['wordwrap'] = TRUE;
    
	  if(!$this->xsendmail($config,$email,$message))
      {
       $this->mod_main->TransLog($data_username,'Email Failed to Send','email:'.$email);
       $this->session->set_userdata('error',array('email' => 'failed'));
      }
     }
    }
    catch(Exception $e) 
    {
     $this->mod_main->TransLog($data_username,'Email Failed','email:'.$email);
     $this->session->set_userdata('error',array('email' => 'failed'));
    }
   }
  }
  if ($prevuname!='' and $prevuname != $username)
  {
   $newimgpath = 'assets/img/profile/'.md5($prevuname.$idno);
   $imgpath = 'assets/img/profile/'.md5($prevuname.$idno);
   $img = $this->mod_main->CheckImage($imgpath);
   if($img !='nophoto')
   {
     $oldimg = $_SERVER['DOCUMENT_ROOT'] .'/sis/'. $img;
     $newimg = str_replace($imgpath,$newimgpath,$oldimg);
	 
	 rename($oldimg,$newimg);
   }
  }
  
  echo 'success';
 }
 else
 {
  echo 'nodata';
 } 
}

function getsubscriptionlist()
{
 $xpost = $this->input->post();
 $list = $this->mod_usermonitoring->manage_user_subscript(0,$xpost['username'],$xpost['username']);
 foreach($list as $rs)
 {
  echo '<tr class="">
          <td><a class="btn btn-danger btn-xs btn-delete_sub"><i class="fa fa-trash-o"></i> Delete</a></td>
		  <td><input name="studno" type=text class="ptc_tbltextbox" style="box-shadow:none;" value="'.$rs->StudentNo.'"/></td>
		  <td>
			<select name="relation" type=text class="ptc_tbltextbox" style="box-shadow:none;">
			 <option value="Family" '.(($rs->Relationship=='Family')?'selected':'').'>Family</option>
			 <option value="Parent" '.(($rs->Relationship=='Parent')?'selected':'').'>Parent</option>
			 <option value="Guardian" '.(($rs->Relationship=='Guardian')?'selected':'').'>Guardian</option>
			</select>
		  </td>
	    </tr>';
 }
}

function refresh()
{
 echo  $this->mod_usermonitoring->tbl_UserList(0,50,1);
}

function xlist($lastid=0)
{
 echo json_encode($this->mod_usermonitoring->Arr_UserList($lastid,100,1));
}

function slist()
{
 $params='';
 $p=$this->input->post();
 if($p['search']!=''){$params=$p['search']; }
 echo json_encode($this->mod_usermonitoring->Arr_UserList(0,9999,1,$params));
}

function trash($userid=0)
{
 $this->mod_usermonitoring->sp_UserMgmtMonitoring('2',$userid,0);
 echo 'success';
}

function activate($userid=0)
{
 $this->mod_usermonitoring->sp_UserMgmtMonitoring('1',$userid,0);
 echo 'success';
}

function regenerate()
{
 $xpost= $this->input->post();
 
 $username = ($xpost && array_key_exists('username',$xpost))?$xpost['username']:'';
 $username = str_replace("'",'',$username);
 $email = ($xpost && array_key_exists('email',$xpost))?$xpost['email']:'';
 $email = str_replace("'",'',$email);
 
 if($username=='' && $email=='')
 {
  echo 'nodata';
  die();
 }
 
 $result=$this->mod_usermonitoring->recreate_vcode($username,$email);
	
 if ($result <> '')
 {
	$verify = explode('|', $result);         
	$today = new DateTime('NOW');
	date_add($today,date_interval_create_from_date_string("15 days"));
	$data['username'] = $username;
	$data['email'] = $email;
	$data['title'] = "PRISMS Online:Registration";
		 
	$data['verify'] = "<a href='".site_url('register/verify').'/' .$verify[0]."'>Verify Now! &raquo;</a>"; 
	$data['today'] = $verify[1] ; //$today->format( 'Y-m-d H:i:s' );
	$data['msg'] = "<h3>Hi, ".$data['username']."</h3>
					 <p class='lead'>We would like to thank you for registering your email account to PRISMS Portal.</p>
					 <p>You've entered ".mailto($email,$email)." as the contact email address for your PRISMS Portal. To complete the process, we just need to verify that this email address belongs to you.
					 Simply click the link below and sign using your PRISMS Portal Username and password.
					 </p>
					 <!-- Callout Panel -->
					 <p class='callout'>
						".$data['verify']." <i> This validation code is valid until ".$data['today']."</i>
					 </p>
					 <!-- /Callout Panel -->";
	 $msg = $this->load->view('mail/mail_template', $data, TRUE);
	 try
	 {
	  if(EMAIL_NOTIFY==1 && !$this->mod_main->xsendmail('',$email,$msg,'Action Required: Please confirm your PRISMS Portal account',$data,'mail/mail_template')){echo 'Failed to send email : '.$data['verify']; die();}
	  echo $data['verify'];
	 }
	 catch(Exception $e)
	 {
	  echo $data['verify'];
	 }
	 die();
 }
 else
 {
  echo 'nodata';
  die();
 }
}

function reset()
{
	$this->load->model('user','',TRUE);
	$xpost = $this->input->post();
	
	$user = ($xpost && array_key_exists('username',$xpost))?$xpost['username']:'';
	$user = str_replace("'",'',$user);
	
	$result = FALSE;
	$npwd = $this->mod_main->GenerateCode();
	if ($user == '') 
	{
	 echo 'nodata';
	 die();	
	}	
	
	$rs = $this->user->getInfo($user);
	if ($rs){
		if(EMAIL_NOTIFY!=1){$npwd = $rs[0]->username;}
		$result = $this->user->iresetuserpwd($rs[0]->username, $npwd);            
		$email =  '"'.$rs[0]->email.'"';
		
	    if($this->AdptPRISMS==1){
	     $conditions = array(1,3,5,7);
	     $crypto     = $this->mod_crypto->BitEncrypt($npwd,$conditions);
	     $ESpass     = $this->user->ichangeESpassword($rs[0]->username,$crypto);
	    }
	}                       
	
	if($result){
		if(EMAIL_NOTIFY==1)
		{
		 $data['userid']   	= $rs[0]->username ; 
		 $data['username'] 	= $rs[0]->lastname . ', ' . $rs[0]->firstname;        
		 $data['email']    	= $rs[0]->email;
		 $data['pwd']      	= $npwd;
		 $data['title'] 	= "PRISMS Online:Reset Password";
		 $data['msg'] 		= "<h3>Hi, ".$data['userid']."</h3>
							   <p class='lead'>We have successfully reset your password</p>
							   <p>You may now sign in to the PRISMS Portal using the information below : </p>			
                               <p>User ID : ".$data['userid']."</p>								
							   <p>Password : ".$data['pwd']."</p>";
         try
         {		
		  if(!$this->mod_main->xsendmail('',$email,'','PRISMS Online:Reset Password',$data,'mail/mail_template'))
		  {
		   $npwd = $rs[0]->username;
		   $result = $this->user->iresetuserpwd($rs[0]->username,$npwd);
		  }
         }
       	 catch(Exception $e)
         {
		  $npwd = $rs[0]->username;
		  $result = $this->user->iresetuserpwd($rs[0]->username,$npwd);	
		 }		
		}
		$this->mod_main->TransLog($email,'Reset Password',$npwd);
		echo $npwd;
	} 
	else 
	{
		$this->mod_main->TransLog($email,'Reset Password Failed','-');
		echo 'nodata';
	} 
}

function xoveride($opt='',$username='',$args='')
{
 $success = FALSE;
 $error='Failed to execute overide!';
 $username = str_replace('%20',' ',$username);
 $args     = str_replace('%20',' ',$args);
 switch($opt)
 {
  case 'validate':
    $username = utf8_decode($username);
    
	if($username!=''){
    $data = array('IsActivated' => 1);
    if($username=='all')
	 $this->db->where("Username='".$username."' OR Email='".$username."'");    
    else	
	 $this->db->where("IsActivated=0");    
    
	$success = $this->db->update('ES_Membership', $data);
    }
	else
	{
	 $error='Invalid Input';	
	}	
  break;
  case 'reset':
   $username = utf8_decode($username);
   $newpwd = utf8_decode($args);
	
   if($newpwd!='' && $username!=''){
   $data = array('password' => md5($newpwd));
   $this->db->where("Username='".$username."' OR Email='".$username."'");    
   $success = $this->db->update('ES_Membership', $data);
   }
   else
   {
	$error='Invalid Input';	
   }	
  break;
  case 'select':
   $username = utf8_decode($username);
   if($username!=''){
    $this->db->from('ES_Membership');	   
    $this->db->where("Username='".$username."' OR Email='".$username."'");    
    $success = $this->db->get()->result(); 
    echo '<pre>';
	print_r($success);
	die();
   }
   else
   {
	$error='Invalid Input';	
   }	
  break;
  default:
   redirect('usermonitoring','refresh');
  break;  
 }
 if($success)
  echo 'Successfully Execute Task';
 else
  echo $error;	 
}

function trial()
{
 echo'<pre>';
 print_r($this->mod_usermonitoring->Arr_UserList(0,50,1));
}

}
// end of class
?>