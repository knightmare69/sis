<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
Class userprivileges extends CI_Controller
{
 function __construct()
 {
  parent:: __construct();
  $this->load->model('mod_main','',true);
  $this->load->model('mod_userprivileges','',true);
 }

 function index()
 {
  $session_data= $this->session->userdata('logged_in');
  $data['userinfo']    = $session_data;
  $data['title']       = 'User Privileges';
  $data['pages']       = $this->mod_userprivileges->gen_display();
  $data['custom_user'] = $this->mod_userprivileges->get_custom_user();
  $data['jslink']   = array('utilities/userprivileges.js');
  $this->load->view('include/header',$data);
  $this->load->view('templates/mainmenu',$data);
  $this->load->view('privileges/vw_userprivileges',$data);
  $this->load->view('include/footer',$data);
 }
 
 function manage($type='',$mode='',$args='')
 {
  $output=array('result'=>false,'content'=>'','error'=>'Server Side Error');
  $session_data= $this->session->userdata('logged_in');
  $p = $this->input->post(); 
  if(!is_array($p)){$p=array();}
  switch($type)
  {
   case 'get':
    switch($mode)
	{
	 case 'user_privilege':
      $userid=((array_key_exists('userid',$p))?$p['userid']:'');
      $idno  =((array_key_exists('idno',$p))?$p['idno']:'');
	  $idtype=((array_key_exists('idtype',$p))?$p['idtype']:'');
	  $privileges = $this->mod_userprivileges->get_privileges_list($userid,$idno,$idtype);
	  if($privileges!='' && $privileges!=false)
	  {
	   $output['result']=true;
   	   $output['content']=$privileges;	
	  }	  
	 break;	 
	}
   break;   
   case 'set':
    switch($mode)
	{
     case 'save_custom_user':
      $userid=((array_key_exists('userid',$p))?$p['userid']:'');
      $idno  =((array_key_exists('idno',$p))?$p['idno']:'');
	  $idtype=((array_key_exists('idtype',$p))?$p['idtype']:'');
	  if($userid!='' && $idno!='' && $idtype!='')
	  {	  
	   if($this->mod_userprivileges->save_custom_user('knightmare69','admin','-1'))
	   {
	    $output['result']=true;
        $output['content']='Successfully Save Add User!';		
	   }
      }
      else
      {
	   $output['error']='Insufficient Details!';  
	  }		  
	 break;	
	 case 'save_default':
	 case 'save_custom':
	  $userid=((array_key_exists('userid',$p))?$p['userid']:'');
      $idno  =((array_key_exists('idno',$p))?$p['idno']:'');
	  $idtype=((array_key_exists('idtype',$p))?$p['idtype']:'');
	  $data=((array_key_exists('data',$p))?$p['data']:'');
	  $result = $this->mod_userprivileges->save_privileges($userid,$idno,$idtype,$data);
	  if($result)
	   $output=array('result'=>true,'content'=>'Successfully Save Data!','error'=>'');	  
	  else
	   $output['error']='Failed To Save Data';	  
	    
	 break;
	}
   break;   
  }
  echo json_encode($output);
 }
 
 function trial()
 {
  echo '<pre>';
  print_r($this->mod_userprivileges->get_custom_user());
  //echo $this->mod_userprivileges->save_custom_user('knightmare69','admin','-1');
  /*	 
  $filepath=str_replace("index.php","xtrial.php",$_SERVER['SCRIPT_FILENAME']);
  echo date('Y-m-d h:i A',filemtime($filepath));
  include($filepath);
  echo '<pre>';
  print_r($privileges);
  $data='p_id:000000,access:desc&Access Page#value&1,print:1|p_id:000001,access:1,print:0';
	  
  $userid='knightmare69';
  $idno='admin';
  $data='p_id:000000,access:desc&Access Page#value&1,print:1|p_id:000001,access:1,print:0';
  $result = $this->mod_userprivileges->save_privileges($userid,$idno,$data);
  echo $result;  
  */
 }

}
?>