
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Utilities extends CI_Controller {
  
 function __construct()
 {
   parent::__construct();
   $this->load->helper(array("form"));
	$this->load->model('mod_main','',TRUE);              
   $this->load->model('mod_utilities','',TRUE);              
   
   if($this->session->userdata('logged_in'))
   {
     $this->utils();
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
   
   
 }
 
 function utils(){
 	$sel = $this->uri->segment(2);
	$columns = '';
	$ntitle='Department';
	$table = 'department';		
	$modal = 'cs_modal';
	$data['javascript'] = array('utilities/utilities.js');
	
	switch($sel){
		case "department": 
			$ntitle='Department';	
			$table = 'department';
			$modal = 'dept_modal';
			$data['rscampus'] = $this->mod_main->get_campuslist();
			$data['javascript'] = array('utilities/department.js');
			break;
		case "position": $ntitle='Position Title';
			$table = 'position';		
			break;		
		case "civilstatus": 
			$ntitle='Civil Status';
			$table = 'civilstatus';
			break;					
		case "nationality": 
			$ntitle='Nationality';
			$table = 'nationality';
			break;
		case 'otherschool':
		 	$ntitle='Other School';
			$table = 'otherschool';
			break;
		default:
			break;
	}

    $session_data = $this->session->userdata('logged_in');
    $data['username'] = $session_data['username'];
	$data['title'] = "Utilities :: " . $ntitle;
	$data['module'] = $ntitle;
	$data['activemodule'] = $ntitle;
	
	$data['mods'] = $this->mod_utilities->listofmods();
	$data['fields'] = $this->mod_utilities->cs_getfields($table);
	$data['recordset'] = $this->mod_utilities->cs_getdata($table);
	
	$this->load->view('include/header', $data);
	$this->load->view('templates/mainmenu', $data);
	$this->load->view('vutilities', $data);     
	$this->load->view('utilities/' . $modal, $data);     
	$this->load->view('include/footer', $data);        

 }
 
function index(){}

function department($crud = ''){
 
  switch($crud){
	case 'save':
	  $this->MessageBox($crud);
	 break;
	case 'delete':
	 $tid = StripSlashes($this->input->post('tindex'));
	 $this->MessageBox($tid);
	 break;
	default:
	 break;
  }
 }

function position(){ }

function civilstatus(){
     
   $name = StripSlashes($this->input->post('name'));
  
   if(isset($name)){
     if($name <> ''){     
      $short = StripSlashes($this->input->post('short'));
      $active = StripSlashes($this->input->post('inactive'));
      $tid = StripSlashes($this->input->post('tid'));
      
      $this->MessageBox($name);
      /*
      if($this->mod_religion->sav_record($tid, $name, $short, $active)){
       //redirect('utilities/religion', 'refresh');
      }else{
       $this->MessageBox('Record not saved!');
      }
     */
     }
  }
  
   
}

function nationality(){
  
}

function otherschool(){
   
}

function building(){
  
   
}

function rooms(){
    
 
}


function religion(){
  
   if($this->session->userdata('logged_in'))
  {
   $this->load->model('mod_religion','',TRUE);       
    $session_data = $this->session->userdata('logged_in');
    $data['username'] = $session_data['username'];
    $data['title'] = "Religion";
    $data['javascript'] = array('utilities/religion.js');
    $data['religion'] = $this->mod_religion->get_all();
    $this->load->view('include/header',$data);
    $this->load->view('templates/mainmenu',$data);
    $this->load->view('vreligion', $data);
    $this->load->view('include/footer',$data);
  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
  
  $name = StripSlashes($this->input->post('name'));
  
  if(isset($name)){
     if($name <> ''){     
      $short = StripSlashes($this->input->post('short'));
      $active = StripSlashes($this->input->post('inactive'));
      $tid = StripSlashes($this->input->post('tid'));
      
      if($this->mod_religion->sav_record($tid, $name, $short, $active)){
       redirect('utilities/religion', 'refresh');
      }else{
       $this->MessageBox('Record not saved!');
      }
     
     }
  }
  
}

}

?>

