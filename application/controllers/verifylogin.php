<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_vbscripts','',TRUE);
   $this->load->model('mod_crypto','',TRUE);
   $this->AdptPRISMS = $this->mod_main->getconfigvalue('AdaptSecurity');   
   $this->StrictPass = $this->mod_main->getconfigvalue('StrictUserPass');
 }

 function index(){
   //This method will have the credentials validation
   $this->load->library('form_validation');
  
   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');   
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
   
   if($this->form_validation->run() == FALSE){
     //Field validation failed.&nbsp; User redirected to login page
	 $this->mod_main->TransLog('-','Login Failed','-');
     $this->load->view('login');
   }else{     
     //Go to private area
     $session_data = $this->session->userdata('logged_in');
	 $this->session->unset_userdata('registerinfo');
     $this->mod_main->TransLog($session_data['id'],'Login Success','-');
     if ($session_data['xaction'] != ''){ redirect('actionrequired', 'refresh'); }
     elseif ($session_data['idtype'] == 0 && $session_data['appcount'] == 0 ){ redirect('activation', 'refresh'); }
     else{
	  $this->session->set_userdata('logged_in', $session_data);
	  redirect('profile', 'refresh');
	 }   
   }
 }

 function check_database($password){
   //Field validation succeeded.&nbsp; Validate against database
   $username = $this->input->post('username');
   $username = str_replace("'","",$username);
   $username = str_replace("<script>","",$username);  
   $username = str_replace("</script>","",$username);
   $username = str_replace("DELETE","",$username);   
   
   //query the database
   if($this->AdptPRISMS==1){
	$conditions = array(1,3,5,7);
	$cryptopwd = $this->mod_crypto->BitEncrypt($password,$conditions);
    $result = $this->user->adaptive_login($username, $password,$cryptopwd);
   }
   else
    $result = $this->user->login($username, $password);
   
   if($result){
     $sess_array = array();
     foreach($result as $row){
	   $isactivated = $row->isactivated;
	   if($isactivated == 0){
	    if(EMAIL_NOTIFY==1){
	     $this->form_validation->set_message('check_database', '<strong>Your account is not validated.</strong> Please Verify your account using the link in your email.
		                                                       <p>(If didnt receive an email, Please contact this email '.((defined('HELP_DESK'))?HELP_DESK:'jhe69samson@gmail.com').')</p>');
        }else{
		 $this->form_validation->set_message('check_database', '<strong>Your account is not validated.</strong> Please contact our administrators to validate your account.');
        }
		
		$this->mod_main->TransLog($username,'Not Validated','-');
		return false;
	   }
	   
	   $uname  = trim($row->username);
	   $idno   = trim($row->idno);
	   $idtype = trim($row->idtype);
	   
	   $imgpath = 'assets/img/profile/'.md5($uname.$idno);
	   $img = $this->mod_main->CheckImage($imgpath);
	   $img = $this->mod_main->SetProfile($img);
	   $lastname = "";
	   $firstname = "";
	   
	   $info = $this->mod_main->getUserFullName($username,$idno,$idtype);
	   if($info)
	   {
		$lastname = $info->LastName;
		$firstname = $info->FirstName;
	    $username = ((trim($lastname)==trim($firstname))?$lastname:($lastname . ', ' . $firstname));
	   }
	   else
	   {$username = $row->username;}
	   
	   //log_message('error','Set Profile');
	   $xaction = '';
	   //$xaction = '{pass}';
	   if($this->StrictPass==1)
	   {
	    if(trim($row->email)=='')
	    {$xaction .= '{email}';}
	   
	    if(trim($row->username)==trim($password))
	    {$xaction .= '{pass}';}
	   }
	   
	   if(($idtype==1 and ENABLE_STUDENT!=1) or ($idtype==2 and ENABLE_PARENT!=1) or ($idtype==3 and ENABLE_FACULTY!=1) or ($idtype==-1 and ENABLE_ADMIN!=1)) 
	   {
	    $message = array('heading' => '<h2>PORTAL IS NOT INCLUDED</h2>','message' => '<p>Portal is not available for your package</p>'); 	   
		show_error($message);
	   }
	   
       $sess_array = array(
                        'id' => trim($row->username),
                        'username' => $this->mod_main->sanitizer($username),
                        'email' => $this->mod_main->sanitizer($row->email),
		                'lastname' => $this->mod_main->sanitizer($lastname),
		                'firstname' => $this->mod_main->sanitizer($firstname),
                        'idtype' => $row->idtype,
                        'idno' => trim($row->idno),
                        'appno' => trim($row->appno),
                        'appcount' => trim($row->appcount),
                        'photo' => $img,
                        'lock' => 0,
                        'minify' => ($row->minify?"minified":""),
                        'xaction' => $xaction
                         );
	   
       $this->session->set_userdata('logged_in', $sess_array);
	   $sessionid = $this->session->userdata('session_id');
	   $this->mod_main->UpdateSession($uname,$sessionid);
	   //log_message('error','Session Created');
     }
     return TRUE;
   }
   else
   {
     $this->mod_main->TransLog($username,'Login Failed','-');
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 }
}
?>
