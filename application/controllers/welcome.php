
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Welcome extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_activation','',TRUE);
   $this->load->helper(array("form"));
 }

 function index()
 {


    $this->load->helper(array("form"));

     $mfooter['jslink'] = array('plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                           'plugin/fuelux/wizard/wizard.js',
                           'plugin/jquery-form/jquery-form.min.js',
                           'jquery.cookies.js',
                           'utilities/activation.js'
                           );

     $this->load->view('include/header');
     $this->load->view('templates/mainmenu');
     $this->load->view('vw_welcome');
     $this->load->view('include/footer',$mfooter );
	 $this->mod_main->Translog('Applicant','Welcome PAGE','-');

 }

}

?>
