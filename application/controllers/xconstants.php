<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Xconstants extends CI_Controller {
 function __construct(){
   parent::__construct();
   date_default_timezone_set("Asia/Taipei");
   $this->load->helper('form');
   $this->load->model('mod_main','',true);
   $this->load->model('mod_constants','',true);
 }
 
 function index($opt='normal')
 {
  if(!$this->session->userdata('logged_in')){redirect('login','refresh');}
  $session_data = $this->session->userdata('logged_in');
  $this->mod_main->monitoring_action($session_data,'constant');
  $data['userinfo'] = $session_data;
  $data['title']='Constant_Config';	
  $data['username'] = $session_data['username'];
  $data['idtype'] = $session_data['idtype'];
  $data['photo'] = $session_data['photo'];	 
  $data['configlist']=$this->mod_constants->gen_list();
  $this->load->view('include/header',$data);
  $this->load->view('templates/mainmenu',$data);
  $this->load->view('vw_constconfig',$data);
  $this->load->view('include/footer',$data);
  $this->mod_main->Translog($session_data['id'],'View Constant(s)','-');
 }
 
 function save()
 {
  if(!$this->session->userdata('logged_in')){redirect('login','refresh');}
  $session_data = $this->session->userdata('logged_in');
  $p= $this->input->post();
  if(is_array($p)){
   $this->mod_constants->save_list($p); 
   $this->mod_main->Translog($session_data['id'],'Save Constant Value(s)','-');
  }
  redirect('home','refresh');
 }
}
?> 