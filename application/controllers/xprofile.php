
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Xprofile extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
   $this->load->model('mod_main','',TRUE);
   $this->load->model('mod_evaluation','',TRUE);
   $this->load->model('mod_vbscripts','',TRUE);
   $this->load->model('mod_crypto','',TRUE);
   $this->load->helper(array("form"));		
   $this->AdptPRISMS = $this->mod_main->getconfigvalue('AdaptSecurity');         
 }

 function index()
 {
   log_message('error','trial check lang');
   if($this->session->userdata('logged_in'))
   {
	 $session_data = $this->session->userdata('logged_in');
     /*$this->mod_main->monitoring_action($session_data,'profile');
	 if(strrpos($session_data['photo'],'empty.png')!==false){
	  $img = $this->mod_main->manage_photo($session_data['id'],$session_data['idno'],$session_data['idtype']);
	  $session_data['photo'] = $this->mod_main->SetProfile($img);
	  $this->session->set_userdata('logged_in', $session_data);
	 }*/
	 
	 $idtype = $session_data['idtype'];
	 $appcount = $session_data['appcount'];
	 $img = $session_data['photo'];
     
	 $data['allow']=0;
	 
	 $data['csoption']  = '';
	 $data['natoption'] = '';
	 $data['reloption'] = '';
	 
	 $userinfo=$this->mod_main->getUserFullProfile($session_data['id'],$session_data['idno'],$session_data['idtype']); 
	 if($idtype!=0 || $appcount!=0){
	  if ($idtype==0 && $appcount!=0){
	   $data['allow']=$this->mod_main->getconfigvalue('AllowApplicantToMod');;
	  }elseif ($idtype==1){
	   if($session_data['idno']!=''){
		   $this->mod_evaluation->reevaluate_student($session_data['idno']); 
	   }
	   $data['allow']=$this->mod_main->getconfigvalue('AllowStudToMod');   
	   $data['history']=$this->mod_main->EnrolmentHistory($session_data['idno']);
	   $data['accounts']=$this->mod_main->get_accountabilities($session_data['idno']);
	   $data['balance']=$this->mod_main->get_currentbalance($session_data['idno']);
	   $data['studentinfo']=$this->mod_main->StudentInfo($session_data['idno']);
	   
	   $data['Father_Name']=(property_exists($userinfo,'Father_Name'))?$userinfo->Father_Name:'';
	   $data['Father_Occupation']=(property_exists($userinfo,'Father_Occupation'))?$userinfo->Father_Occupation:'';
	   $data['Father_Company']=(property_exists($userinfo,'Father_Company'))?$userinfo->Father_Company:'';
	   $data['Father_CompanyAdd']=(property_exists($userinfo,'Father_CompanyAdd'))?$userinfo->Father_CompanyAdd:'';
	   $data['Father_Email']=(property_exists($userinfo,'Father_Email'))?$userinfo->Father_Email:'';
	   $data['Father_TelNo']=(property_exists($userinfo,'Father_TelNo'))?$userinfo->Father_TelNo:'';
	   
	   $data['Mother_Name']=(property_exists($userinfo,'Mother_Name'))?$userinfo->Mother_Name:'';
	   $data['Mother_Occupation']=(property_exists($userinfo,'Mother_Occupation'))?$userinfo->Mother_Occupation:'';
	   $data['Mother_Company']=(property_exists($userinfo,'Mother_Company'))?$userinfo->Mother_Company:'';
	   $data['Mother_CompanyAdd']=(property_exists($userinfo,'Mother_CompanyAdd'))?$userinfo->Mother_CompanyAdd:'';
	   $data['Mother_Email']=(property_exists($userinfo,'Mother_Email'))?$userinfo->Mother_Email:'';
	   $data['Mother_TelNo']=(property_exists($userinfo,'Mother_TelNo'))?$userinfo->Mother_TelNo:'';
	   
	   $data['Guardian_Name']=(property_exists($userinfo,'Guardian_Name'))?$userinfo->Guardian_Name:'';
	   $data['Guardian_Relation']=(property_exists($userinfo,'Guardian_Relationship'))?$userinfo->Guardian_Relationship:'';
	   $data['Guardian_Occupation']=(property_exists($userinfo,'Guardian_Occupation'))?$userinfo->Guardian_Occupation:'';
	   $data['Guardian_Company']=(property_exists($userinfo,'Guardian_Company'))?$userinfo->Guardian_Company:'';
	   $data['Guardian_Email']=(property_exists($userinfo,'Guardian_Email'))?$userinfo->Guardian_Email:'';
	   $data['Guardian_TelNo']=(property_exists($userinfo,'Guardian_TelNo'))?$userinfo->Guardian_TelNo:'';
	   
	   $data['Emergency_Contact']=(property_exists($userinfo,'Emergency_Contact'))?$userinfo->Emergency_Contact:'';
	   $data['Emergency_Address']=(property_exists($userinfo,'Emergency_Address'))?$userinfo->Emergency_Address:'';
	   $data['Emergency_Mobile']=(property_exists($userinfo,'Emergency_Mobile'))?$userinfo->Emergency_Mobile:'';
	   $data['Emergency_TelNo']=(property_exists($userinfo,'Emergency_TelNo'))?$userinfo->Emergency_TelNo:'';
	   
	   $data['Elem_School']=(property_exists($userinfo,'Elem_School'))?$userinfo->Elem_School:'';
	   $data['Elem_Addr']=(property_exists($userinfo,'Elem_Addr'))?$userinfo->Elem_Addr:'';
	   $data['Elem_InclDates']=(property_exists($userinfo,'Elem_InclDates'))?$userinfo->Elem_InclDates:'';
	   $data['Elem_AwardHonor']=(property_exists($userinfo,'Elem_AwardHonor'))?$userinfo->Elem_AwardHonor:'';
	   
	   $data['HS_School']=(property_exists($userinfo,'HS_School'))?$userinfo->HS_School:'';
	   $data['HS_Addr']=(property_exists($userinfo,'HS_Addr'))?$userinfo->HS_Addr:'';
	   $data['HS_InclDates']=(property_exists($userinfo,'HS_InclDates'))?$userinfo->HS_InclDates:'';
	   $data['HS_AwardHonor']=(property_exists($userinfo,'HS_AwardHonor'))?$userinfo->HS_AwardHonor:'';
	   
	   $data['College_School']=(property_exists($userinfo,'HS_School'))?$userinfo->College_School:'';
	   $data['College_Addr']=(property_exists($userinfo,'HS_Addr'))?$userinfo->College_Addr:'';
	   $data['College_Degree']=(property_exists($userinfo,'College_Degree'))?$userinfo->College_Degree:'';
	   $data['College_InclDates']=(property_exists($userinfo,'HS_InclDates'))?$userinfo->College_InclDates:'';
	   $data['College_AwardHonor']=(property_exists($userinfo,'HS_AwardHonor'))?$userinfo->College_AwardHonor:'';
	   
	   $data['Vocational_School']=(property_exists($userinfo,'Vocational_School'))?$userinfo->Vocational_School:'';
	   $data['Vocational_Addr']=(property_exists($userinfo,'Vocational_Addr'))?$userinfo->Vocational_Addr:'';
	   $data['Vocational_InclDates']=(property_exists($userinfo,'Vocational_InclDates'))?$userinfo->Vocational_InclDates:'';
	   $data['Vocational_Degree']=(property_exists($userinfo,'Vocational_Degree'))?$userinfo->Vocational_Degree:'';
	  }
	  elseif ($idtype==2)
	  {
	   $data['allow']=$this->mod_main->getconfigvalue('AllowParentToMod');   
	  }
	  elseif($idtype==3 || $idtype=='-1')
	  {
	   $data['allow']=$this->mod_main->getconfigvalue('AllowFacToMod');   
	   $data['Father_Surname']=((property_exists($userinfo,'Father_Surname'))?($userinfo->Father_Surname):'');
	   $data['Father_FirstName']=((property_exists($userinfo,'Father_FirstName'))?($userinfo->Father_FirstName):'');
	   $data['Father_MiddleName']=((property_exists($userinfo,'Father_MiddleName'))?($userinfo->Father_MiddleName):'');
	  
	   $data['Mother_Surname']=((property_exists($userinfo,'Mother_Surname'))?($userinfo->Mother_Surname):'');
	   $data['Mother_FirstName']=((property_exists($userinfo,'Mother_FirstName'))?($userinfo->Mother_FirstName):'');
	   $data['Mother_MiddleName']=((property_exists($userinfo,'Mother_MiddleName'))?($userinfo->Mother_MiddleName):'');
	   $data['Mother_Maiden']=((property_exists($userinfo,'Mother_Maiden'))?($userinfo->Mother_Maiden):'');
	   
	   $data['Spouse_Surname']=(property_exists($userinfo,'Spouse_Surname'))?$userinfo->Spouse_Surname:'';
	   $data['Spouse_FirstName']=(property_exists($userinfo,'Spouse_FirstName'))?$userinfo->Spouse_FirstName:'';
	   $data['Spouse_MiddleName']=(property_exists($userinfo,'Spouse_MiddleName'))?$userinfo->Spouse_MiddleName:'';
	   $data['Spouse_Occupation']=(property_exists($userinfo,'Spouse_Occupation'))?$userinfo->Spouse_Occupation:'';
	   $data['Spouse_Agency']=(property_exists($userinfo,'Spouse_Agency'))?$userinfo->Spouse_Agency:'';
	   $data['Spouse_AgencyAddress']=(property_exists($userinfo,'Spouse_AgencyAddress'))?$userinfo->Spouse_AgencyAddress:'';
	   $data['Spouse_AgencyTelNo']=(property_exists($userinfo,'Spouse_AgencyTelNo'))?$userinfo->Spouse_AgencyTelNo:'';
	  }
	 
	  $session_data['lastname']   = is_key_exist($userinfo,'LastName','Trial');
	  $session_data['firstname']  = $userinfo->FirstName;
	  $session_data['middlename'] = $userinfo->MiddleName;
	  $session_data['gender']     = $userinfo->Gender;
	  $session_data['birthdate'] = datewformat($userinfo->DateOfBirth,'m/d/Y');
	  $session_data['birthplace'] = $userinfo->PlaceOfBirth;
	  $session_data['telno']  = $userinfo->TelNo;
	  $session_data['mobile'] = $userinfo->MobileNo;
	  $session_data['natid']  = $userinfo->NationalityID;
	  $session_data['nation']  = $userinfo->Nationality;
	  $session_data['relid']  = $userinfo->ReligionID;
	  $session_data['religion']  = $userinfo->Religion;
	  $session_data['cstatusid']= $userinfo->CivilStatusID;
	  $session_data['cstatus']= $userinfo->CivilStatus;
   
	  $session_data['res_addr']= $userinfo->Res_Address;
	  $session_data['res_strt']= $userinfo->Res_Street;
	  $session_data['res_brgy']= $userinfo->Res_Barangay;
	  $session_data['res_city']= $userinfo->Res_TownCity;
	  $session_data['res_prov']= $userinfo->Res_Province;
	  $session_data['res_zip']= $userinfo->Res_ZipCode;
   
	  $session_data['perm_addr']= $userinfo->Perm_Address;
	  $session_data['perm_strt']= $userinfo->Perm_Street;
	  $session_data['perm_brgy']= $userinfo->Perm_Barangay;
	  $session_data['perm_city']= $userinfo->Perm_TownCity;
	  $session_data['perm_prov']= $userinfo->Perm_Province;
	  $session_data['perm_zip']= $userinfo->Perm_ZipCode;
	  
	 }
	 else
	 {redirect('admission', 'refresh');}
	 
	 $cs=$this->mod_main->get_civilstatus();
	 $nat=$this->mod_main->get_nationality();
	 $rel=$this->mod_main->get_religions();
	 
	 foreach($cs as $n)
	 {$data['csoption'] .= $n->StatusID.'|'.$n->CivilDesc.'@';}
	 foreach($nat as $n)
	 {$data['natoption'] .= $n->NationalityID.'|'.$n->Nationality.'@';}
	 foreach($rel as $n)
	 {$data['reloption'] .= $n->ReligionID.'|'.$n->Religion.'@';}
	 
	 $data['userinfo'] = $session_data;
	 $data['title'] = "Profile";
     $data['username'] = $session_data['username'];        
	 $data['csslink']   = array('cropper/cropper.min.css');
     $mfooter['jslink'] = array('plugin/maxlength/bootstrap-maxlength.min.js',
                                'plugin/bootstrap-timepicker/bootstrap-timepicker.min.js',
                                'plugin/bootstrap-tags/bootstrap-tagsinput.min.js',
                                'plugin/noUiSlider/jquery.nouislider.min.js',
                                'plugin/ion-slider/ion.rangeSlider.min.js',
                                'plugin/colorpicker/bootstrap-colorpicker.min.js',
                                'plugin/knob/jquery.knob.min.js',
                                'plugin/x-editable/moment.min.js',
                                'plugin/x-editable/jquery.mockjax.min.js',
                                'plugin/x-editable/x-editable.min.js',
                                'plugin/typeahead/typeahead.min.js',
                                'plugin/typeahead/typeaheadjs.min.js',
                                'plugin/cropper/cropper.min.js',
                                'utilities/profile.js',
                                'prompt.js?20160529');

     $this->load->view('include/header',$data);
     $this->load->view('templates/mainmenu',$data);
     $this->load->view('vw_profile', $data);
	 $this->load->view('include/footer',$mfooter);
	 $this->mod_main->Translog($session_data['id'],'View Profile','-');
   }
   else
   {
     //If no session, redirect to login page
	 log_message('error','No Session Found');
     redirect('login', 'refresh');
   }
 }


function ChangePassword(){
 $session_data = $this->session->userdata('logged_in');
 if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 

 $user   = $session_data['id'];
 $pwd    = $this->input->post('npwd');
 $crypto = $this->input->post('npwd');
 if($pwd!=''){
  $result = $this->user->ichangethypassword($user,$pwd);
  if($result){$this->mod_main->Translog($user,'ChangePassword',$pwd); }  
  
  if($this->AdptPRISMS==1){
   $user       = $session_data['idno'];
   $conditions = array(1,3,5,7);
   $crypto     = $this->mod_crypto->BitEncrypt($pwd,$conditions);
   $ESpass     = $this->user->ichangeESpassword($user,$crypto);
  }
 }
 redirect('profile', 'refresh'); 
}

function changeemail()
{
    $session_data = $this->session->userdata('logged_in');
    if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	
    $user = $session_data['id'];
    $email = $this->input->post('nemail');
   if($email!=''){
    $result = $this->user->ichangethyemail($user, $email);
    if($result)
    {
	 $session_data['email']=$this->mod_main->sanitizer($email);
	 if($session_data['email']!='' && $session_data['email']!='')
	 {
	  $session_data['email'] = $session_data['email'];
	 }
	 $this->session->set_userdata('logged_in', $session_data);
	 $this->mod_main->Translog($user,'ChangeEmail',$email);
    }
   }
   redirect('profile', 'refresh');
}

function record(){
    $session_data = $this->session->userdata('logged_in');
	if ($session_data['lock'] == 1){ redirect('lock', 'refresh'); } 
	
    $user = $session_data['id'];
    $idno = $session_data['idno'];
	$idtype = $session_data['idtype'];
    $name = $this->input->post('name');
    $name2 = "";
    $value = $this->input->post('value');
    
	switch($name)
	{
        case 'sex': $name = 'gender';
        break;
        case 'dob': $name = 'birthdate';
        break;
        case 'csgroup': $name = 'CivilStatusID';
        break;
        case 'natgroup': $name = 'NationalityID';
        break;
        case 'relgroup': $name = 'ReligionID';
        break;
        case 'resaddress': 
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'res_address', $value['address'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'res_street', $value['street'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'res_barangay', $value['brgy'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'res_towncity', $value['city'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'res_province', $value['prov'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'res_zipcode', $value['zip'] );
            exit;
        break;
        case 'permaddress': 
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'perm_address', $value['address'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'perm_street', $value['street'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'perm_barangay', $value['brgy'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'perm_towncity', $value['city'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'perm_province', $value['prov'] );
            $result = $this->user->isave_profileinfo( $user, $idno, $idtype, 'perm_zipcode', $value['zip'] );
            exit;
        break;
        default:         
        break;
    }
	
    //$value = utf8_decode($value);
    $xvalue = $this->mod_main->sanitizer_insert($value);
    $result = $this->user->isave_profileinfo( $user, $idno, $idtype, $name, $xvalue);
	
	if($name=="lastname"||$name=="firstname")
	{
	 $session_data[$name]=$this->mod_main->sanitizer($value);
	 if($session_data['lastname']!='' && $session_data['firstname']!='' && $session_data['lastname']!='Administrator')
	 {$session_data['username'] = $session_data['lastname'].', '. $session_data['firstname'];}
     else if($session_data['lastname']=='Administrator')
	 {$session_data['username'] = $session_data['lastname'];}
 
	 $this->session->set_userdata('logged_in', $session_data);
	}
	$value=$this->mod_main->sanitizer_insert($value);
	$this->mod_main->Translog($user,'UpdateRecord',$name.':'.$value);
}

function username()
{
 if($this->session->userdata('logged_in'))
 {
  $sess_data=$this->session->userdata('logged_in');
  $username=$sess_data['username'];
  echo $username;
 }
 else
 {echo 'nochange';}
}

function pic_upload()
{
 if($this->session->userdata('logged_in'))
 {
  $session_data = $this->session->userdata('logged_in');
  $uname = $session_data['id'];
  $idno = $session_data['idno'];
  
  $config['upload_path'] = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']) .'assets/img/profile/';
  $config['allowed_types'] = 'gif|jpg|png';
  //$config['max_size']	  = '100';
  //$config['max_width']  = '500';
  //$config['max_height'] = '500';

  $this->load->library('upload', $config);
  $field_name = "pic_file";

  if (!$this->upload->do_upload($field_name))
  {
   $error = $this->upload->display_errors();
   $this->session->set_userdata('error_data', $error);
   $this->mod_main->Translog($uname,'Update Profile Failed','-');
  }
  else
  {
   $data = $this->upload->data();
   $fullpath = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']); //assets/img/profile/'.md5($uname.$stdno);
   $imgpath = 'assets/img/profile/'.md5($uname.$idno);
   
   $img = $this->mod_main->CheckImage($imgpath);
   if($img !='nophoto' && $idno!="")
   {unlink($fullpath.$img);}
   
   rename($data['full_path'],$fullpath.$imgpath.$data['file_ext']);
   $newimg = base_url($imgpath.$data['file_ext']);
   $session_data['photo'] = $newimg;
   
   
   $this->session->set_userdata('logged_in', $session_data);
   $this->mod_main->Translog($uname,'Update Profile Success','-');
  }	
 }
 redirect('profile', 'refresh'); 
}

function getinfo()
{
$session_data=$this->session->userdata('logged_in');
print_r($session_data);
}


function trial()
{
 $tdata = array('entrya'=>'trial lang','entryb'=>'trial lang uli');
 //setcookie('trial_cookie','trial lang po',0);
 //setcookie('cts_cookie',false,1);
 echo '<pre>';
 //print_r($_COOKIE);	
 //print_r($_SESSION);	
 print_r(json_decode(json_encode($tdata),true));
}


}
?>

