<?php
$directory = realpath(dirname(__FILE__));
$document_root = realpath($_SERVER['DOCUMENT_ROOT']);
$base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .$_SERVER['HTTP_HOST'];
if(strpos($directory, $document_root)===0) 
{
  $base_url .= str_replace(DIRECTORY_SEPARATOR, '/', substr($directory, strlen($document_root)));
}
$base_url = str_replace('application/errors','assets/',$base_url);
defined("APP_URL") ? null : define("APP_URL", str_replace("/assets", "", $base_url));
defined("ASSETS_URL") ? null : define("ASSETS_URL", $base_url);
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="enrollment system, school information system, online enrollment, student portal">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">	

<title>Database Error - PRISMS Portal</title>

<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>css/smartadmin-production.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>css/smartadmin-skins.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>css/prince.css">

</head>
<body>
<div id="container">
<div class="row">

 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
 <div class="text-center error-box">
	<h1 class="prince_error_h1"><i class="fa fa-times-circle text-danger error-icon-shadow"></i> <?php echo $heading;?></h1> 
	<h6>You have experienced a problem with database connection.</h6> 
	<p>We are working on it. Please bear with us. If error is persistent, please report it to us.</p>
	<hr>
	<?php echo '<p data-message="'.$message.'"></p>'; ?>
	<div>
	<button type="button" class="btn btn-primary" onclick="location.reload();"><i class="fa fa-refresh"></i> Try Refresh Page</button>
	<button type="button" class="btn btn-success" onclick="window.location = '<?php echo APP_URL;?>';"><i class="fa fa-refresh"></i> Return to Main Page</button>
	</div>
 </div>
 </div>
 
 </div>
</div> <!-- End of content -->

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script>
	if (!window.jQuery) {
		document.write('<script src="<?php echo ASSETS_URL; ?>js/libs/jquery-2.0.2.min.js"><\/script>');
	}
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
	if (!window.jQuery.ui) {
		document.write('<script src="<?php echo ASSETS_URL; ?>js/libs/jquery-ui-1.10.3.min.js"><\/script>');
	}
</script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="<?php echo ASSETS_URL; ?>/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->
<script src="<?php echo ASSETS_URL; ?>js/bootstrap/bootstrap.min.js"></script>

</body>
</html>