<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   	
	function is_key_exist($arr=array(),$key='',$default=false)
	{
	 $type=((@is_object($arr))? 'object':((@is_array($arr))?'array':'undefined'));
	 switch($type)	
	 {
	  case 'array':
	   return ((@array_key_exists($key,$arr))?$arr[$key]:$default);
      break;	  
	  case 'object':
	   return ((@property_exists($arr,$key))?($arr->$key):$default);
      break;	  
	  default:
	   return $default;
	  break;
	 }
	}
	
	function pwd_encrypt($str)
	{
	 return md5($str);	
	}
	
	function datewformat($vdate=false,$xformat='m/d/Y')
	{
	 if(strtotime($vdate))
	 {
	   $tmpdate = New DateTime($vdate);
       return $tmpdate->format($xformat);	   
	 }
     else	 
	  return '';	
	}
	
	function takestrpart($str,$len=30,$filler='')
	{
	  return ((strlen($str)>$len)?(substr($str,0,($len-3)).$filler):$str);	
	}
	
    function putDecimal($value) {
     if((float) $value === floor($value))
	 {
	  $xval = floor($value).'.00';
	  return number_format($xval, 2, '.', ',');
	 }
	 else
	 {return number_format($value, 2, '.', ',');}
    }
	
	function strsanitizer($text='')
	{
	  if(is_array($text))
	  {  
	   foreach($text as $k=>$v)
       {
		$text[$k]=strsanitizer($v);   
	   }	
	  }
	  else
	  {  
	   $text = str_replace("&", "&#38;", $text);
	   $text = str_replace("<", "&lt;", $text); 
	   $text = str_replace(">", "&gt;", $text); 
	   $text = str_replace("\"", "&quot;", $text); 
	   $text = str_replace("'", "&#039;", $text);
	   $text = str_replace("=", "&#61;", $text);
	   $text = str_replace("|", "&#124;", $text); 
	  }
      return $text; 	  
	}
	
	function strunsanitizer($text='')
	{
	  if(is_array($text))
	  {  
	   foreach($text as $k=>$v)
       {
		$text[$k]=strunsanitizer($v);   
	   }	
	  }
	  else
	  {  
	   $text = str_replace("&#38;", "&",  $text);
	   $text = str_replace("&lt;", "<", $text); 
	   $text = str_replace("&gt;", ">", $text); 
	   $text = str_replace("&quot;", "\"", $text); 
	   $text = str_replace("&#039;", "'", $text);
	   $text = str_replace("&#61;", "=", $text);
	   $text = str_replace("&#124;", "|", $text); 
	  }
      return $text; 	  
	}
?>