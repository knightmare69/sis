<?php
class mod_accountabilities extends CI_MODEL
{

 function xtermID($studentno)
 {
  $this -> db -> select('termID');
  $this -> db -> from('es_students');
  $this -> db -> where('studentno', $studentno);
  $this -> db -> limit(1);
  $query = $this -> db -> get();
  if($query -> num_rows() == 1)
  {
   $output = $query->result();
   $x = $output[0]->termID;
   return $x;
  }
  else
  {
   return false;
  }
 }
	
 function usp_getAccountabilities($studentno=null)
 {
  $query = "SELECT  IndexID ,
                dbo.fn_AcademicYearTerm(TermID) AYTerm ,
                StudentNo ,
                Reason ,
				Cleared,
                CASE Cleared
                  WHEN 0 THEN ''
                  WHEN 1 THEN 'Cleared'
                END AS [Status] ,
                DateEntered ,
                EnteredBy ,
                DateUpdate ,
                UpdateBy
        FROM    ES_StudentAccountabilities
        WHERE   StudentNo = '".$studentno."'
		ORDER BY dbo.fn_AcademicYearTerm(TermID)  DESC;";
  $result = $this->db->query($query);
  return $result->result();	
 } 

}
?>