
<?php
Class mod_activation extends CI_Model {
    
	function InfoInUse($username,$idno,$idtype){
	  $this->db->select('Count(*) as result');
	  $this->db->from('ES_Membership');
	  $where = "(username!='".$username."' and idno='".$idno."' and idtype=".$idtype.")";
      $this->db->where($where);
	  $result = $this->db->get()->row();
	  return $result->result;
	}
    
  	function verify_student($lname, $fname, $bday, $sex, $studno, $termid, $progid, $regid, $orno, $ordt ){
	  $qry = "EXEC sp_VerifyStudentInfo '".$lname."', '".$fname."','".$bday."', '".$sex."','".$studno."' ,'".$termid."','".$progid."','".$regid."','".$orno."','".$ordt."';" ;
	  $result = $this->db->query($qry);
	  return $result->result();	
	}
    
	function get_targettype($userid){
	  $qry    = "SELECT ISNULL(TargetType,-1) as TargetType FROM ES_Membership WHERE UserName='".$userid."'";	
	  $result = $this->db->query($qry);
	  if($result && count($result->result())>0){
		$target = $result->result()[0]->TargetType;
		return $target; 
	  }else{
		return -1;  
	  }
	}
	
	function checkifscholar($regid){
	  $query = $this->db->query('SELECT dbo.fn_sisCheckIfScholar('.$regid.') as isScholar');						
	  return $query->result();
	}
	
	function validate_member($idx, $idtype, $idno){
    	$data = array(
               'idtype' => $idtype,
               'idno' => $idno
		);
		
		$this->db->where('username', $idx);
		$success = $this->db->update('ES_Membership', $data);
	}
	
	function get_StudentInfo($studentno)
        {
            $this->db->from('vw_Students');
	    $this->db->where('StudentNo', $studentno);
	    return $this->db->get()->result();
        }
	
	function get_ayterms()
	{
		$query = $this->db->query('SELECT TOP 25 * FROM ES_AYTerm ORDER BY  AcademicYear DESC, SchoolTerm DESC');						
		return $query->result();
	}
        
        function validate($studentno='', $birthdate='', $termid=0, $regid=0, $receiptno='', $date='')
        {
		if(is_numeric($regid)==false)
		{$regid=0;}
		
		if($termid=='')
		{$termid=0;}
		
		if($receiptno=='')
		{$receiptno=0;}
		
		if($date!='' && strtotime($date))
		{
		 $tmpdate = new DateTime($date);
         $date    = $tmpdate->format('m-d-Y');		 
		}	
		else
		 $date    = '';	
		
		$query = $this->db->query("SELECT [dbo].[fn_ParentStudentValidation]('".$studentno."','".$birthdate."',".$termid.",".$regid.",'".$receiptno."','".$date."') AS 'Validate'");						
		return $query->result();
        }
        
        function icreate($parentid, $studentno, $relationship)
        {
		$success = false;
            
		$data = array(
			'ParentID' => $parentid,
			'StudentNo' => $studentno,
			'Relationship' => $relationship,
			);
			
		$success = $this->db->insert('ES_ParentChildren', $data);
                        
		return $success;
        }
	
	    function iactivate($username, $typeid)
        {
		$success = false;
            
		$data = array(
			'IDType' => $typeid,
			);
			
			$this->db->where('UserName', $username);
			$success = $this->db->update('ES_Membership', $data);
                        
		return $success;
        }
} //end of class
?>