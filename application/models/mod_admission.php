<?php
Class mod_admission extends CI_Model
{
    
	function call_sp($param=array('Opt'=>'0.1'))
	{
	 $qry = "EXEC sis_Admission";	
	 return $qry;
	}
	
	function save_userprofile($idx,$lname='', $fname='', $mname='', $ext='', $sex='',
                             $bday, $bloc='', $csid=0, $natid = 0, $relid=0, $telno='', $mobile='' ){
		
		$success=false;
		
		if($idx <> ''){
			$data = array(
               'lastname' => $lname,
               'firstname' => $fname,
               'middlename' => $mname,
               'extname' => $ext,
               'gender' => $sex,
               'birthdate' => $bday,
               'birthplace' => $bloc,
               'civilstatusid' => $csid,
               'nationalityid' => $natid,
               'religionid' => $relid,
               'telno' => $telno,
               'mobileno' => $mobile
            );

			$this->db->where('username', $idx);
			$success = $this->db->update('ES_Membership', $data);

		}
		else
		{
			//$data = array('Religion' => $name ,'ShortName' => $short);
			//$success = $this->db->insert('es_religion', $data);
         
		}
		return $success;
	}
	
	function save_application($idx, $termid, $campusid, $apptype, $ch1, $chm1, $ch2, $chm2, $ch3, $chm3, $ch4, $chm4 ){
		$success=false;
		
		$qry = "EXEC sp_Application '".$idx."','". $termid ."','".$campusid."','". $apptype."','". $ch1."','". $chm1."','".$ch2."','". $chm2."','". $ch3."','". $chm3."','". $ch4."','". $chm4 ."';" ;
		//echo $qry;
		$success = $this->db->query($qry);
		return $success;
	}
	
	function get_application($username)
	{
				
	   $this -> db -> from('vw_admission');
	   $this -> db -> where('username', $username);   		
	   $query = $this -> db -> get();
		
		return $query->result();			
	}
	
	function manage_applicants($opt=0,$termid=1,$campus=1,$apptype=1,$param=false){
		switch($opt){
		  case 0:
		    $qry  = "SELECT CONCAT(LastName,',',FirstName,' ',MiddleInitial) as Fullname
							      ,atype.ApplicationType
								  ,astats.AdmStatus
							      ,p.ProgID
							      ,p.ProgCode
							      ,p.ProgName
							      ,m.MajorDiscDesc
							      ,a.AppNo 
								  ,a.AppDate
							      ,a.LastName 
							      ,a.FirstName 
							      ,a.MiddleName 
								  ,a.Gender
						      FROM ES_Admission as a
					    INNER JOIN ES_ApplicationTypes as atype ON a.ApplyTypeID=atype.TypeID
						INNER JOIN ES_AdmissionStatus as astats ON a.AdmStatusID=astats.StatusID
					    INNER JOIN ES_Programs as p ON a.Choice1_Course=p.ProgID
					     LEFT JOIN ES_DisciplineMajors as m ON a.Choice1_CourseMajor=m.IndexID
						     WHERE a.TermID           = '".$termid."'
							   AND a.Choice1_CampusID = '".$campus."'
							   AND a.ApplyTypeID      = '".$apptype."' ".(($param==false || $param=='')?"":(" AND (a.AppNo='".$param."' OR a.LastName LIKE '%".$param."%' OR a.FirstName LIKE '%".$param."%')"))."  
					      ORDER BY AppDate DESC";
		    $exec = $this->db->query($qry);
		    return (($exec) ? $exec->result() : array());
		  break;
		  case 1:
		    $appno = $param['appno'];
		    if($appno=='new'){
				return true;
			}else{
				return true;
			}
		    return true;
		  break;
		  case 2:
		    $appno = $param['appno'];
			if($appno=='' || $appno==false){return false;}
		    $qry   = "UPDATE ES_Admission SET AdmStatusID=2 WHERE TermID='".$termid."' AND AppNo='".$appno."'";
			$ins   = "INSERT  INTO ES_Students
								( StudentNo,AppNo,CampusID,TermID,ProgID,YearLevelID,MajorDiscID ,CurriculumID ,
								  LastName,FirstName,Middlename,MiddleInitial,ExtName,Fullname,DateOfBirth,PlaceOfBirth,Gender,CivilStatusID,ReligionID,NationalityID,TelNo,MobileNo,Email,
								  Res_Address ,Res_Street ,Res_Barangay ,Res_TownCity ,Res_Province ,Res_ZipCode ,
								  Perm_Address ,Perm_Street ,Perm_Barangay ,Perm_TownCity ,Perm_Province ,Perm_ZipCode ,
								  Father ,Father_Occupation ,Father_Company ,Father_CompanyAddress ,Father_TelNo ,Father_Email ,
								  Mother ,
								  Mother_Occupation ,
								  Mother_Company ,
								  Mother_CompanyAddress ,
								  Mother_TelNo ,
								  Mother_Email ,
								  Guardian ,
								  Guardian_Relationship ,
								  Guardian_Address ,
								  Guardian_Street ,
								  Guardian_Barangay ,
								  Guardian_TownCity ,
								  Guardian_Province ,
								  Guardian_ZipCode ,
								  Guardian_Occupation ,
								  Guardian_Company ,
								  Guardian_TelNo ,
								  Guardian_Email ,
								  Elem_School ,
								  Elem_Address ,
								  Elem_InclDates ,
								  HS_School ,
								  HS_Address ,
								  HS_InclDates ,
								  StudentPicture ,
								  FamilyID
								)
								SELECT TOP 1
										dbo.fn_K12_GenerateSN(CampusID, TermID, GradeLevelID) as StudentNo,
										AppNo ,
										CampusID ,
										TermID ,
										ProgramID ,
										GradeLevelID,
										MajorID ,
										0 AS CurriculumID ,
										LastName ,
										FirstName ,
										MiddleName ,
										MiddleInitial ,
										ExtName ,
										CONCAT(UPPER(LastName), ', ', FirstName, ' ',MiddleInitial) AS Fullname ,
										DateOfBirth ,
										PlaceOfBirth ,
										Gender ,
										CivilStatusID ,
										ReligionID ,
										NationalityID ,
										ISNULL(fb.Guardian_TelNo,'') as TelNo ,
										ISNULL(fb.Guardian_Mobile,'') as Mobile ,
										ISNULL(fb.Guardian_Email,'') as Email ,
										ISNULL(fb.Guardian_Address,'') as Res_Address,
										ISNULL(fb.Guardian_Street,'') as Res_Street,
										ISNULL(fb.Guardian_Barangay,'') as Res_Brgy,
										ISNULL(fb.Guardian_TownCity,'') as Res_City,
										ISNULL(fb.Guardian_Province,'') as Res_Province,
										ISNULL(fb.Guardian_ZipCode,'') as Res_ZipCode,
										ISNULL(fb.Guardian_Address,'') as Perm_Address,
										ISNULL(fb.Guardian_Street,'') as Perm_Street,
										ISNULL(fb.Guardian_Barangay,'') as Perm_Brgy,
										ISNULL(fb.Guardian_TownCity,'') as Perm_City,
										ISNULL(fb.Guardian_Province,'') as Perm_Province,
										ISNULL(fb.Guardian_ZipCode,'') as Perm_ZipCode,
										ISNULL(fb.Father_Name,'') as Father,
										ISNULL(fb.Father_Occupation,'') as Father_Occupation,
										ISNULL(fb.Father_Company,'') as Father_Company,
										ISNULL(fb.Father_CompanyAddress,'') as Father_CompanyAddress,
										ISNULL(fb.Father_TelNo,'') as Father_TelNo,
										ISNULL(fb.Father_Email,'') as Father_Email,
										ISNULL(fb.Mother_Name,'') as Mother ,
										ISNULL(fb.Mother_Occupation,'') as Mother_Occupation ,
										ISNULL(fb.Mother_Company,'') as Mother_Company ,
										ISNULL(fb.Mother_CompanyAddress,'') as Mother_CompanyAddress ,
										ISNULL(fb.Mother_TelNo,'') as Mother_TelNo ,
										ISNULL(fb.Mother_Email,'') as Mother_Email,
										ISNULL(fb.Guardian_Name,'') as Guardian,
										ISNULL(fb.Guardian_RelationshipOthers,'') as Guardian_Relation,
										ISNULL(fb.Guardian_Address,'') as Guardian_Address ,
										ISNULL(fb.Guardian_Street,'') as Guardian_Street ,
										ISNULL(fb.Guardian_Barangay,'') as Guardian_Brgy ,
										ISNULL(fb.Guardian_TownCity,'') as Guardian_City ,
										ISNULL(fb.Guardian_Province,'') as Guardian_Province ,
										ISNULL(fb.Guardian_ZipCode,'') as Guardian_ZipCode ,
										'' AS Guardian_Occupation ,
										'' AS Guardian_Company ,
										ISNULL(fb.Guardian_TelNo,'') as Guardian_TelNo ,
										ISNULL(fb.Guardian_Email,'') as Guardian_Email ,
										Elem_School ,
										Elem_Address ,
										Elem_InclDates ,
										HS_School ,
										HS_Address ,
										HS_InclDates ,
										Photo ,
										a.FamilyID
								FROM    ES_Admission AS a
								WHERE   AppNo = '".$appno."' AND StatusID=2
								AND     a.AppNo NOT IN ( SELECT AppNo FROM ES_Students )";
		    return true;
		  break;
		}		 
	}
  
  function create($data){
    $check = $this->db->query("SELECT AppNo FROM ES_Admission WHERE TermID='".$data['TermID']."' AND LastName='".$data['LastName']."' AND FirstName='".$data['FirstName']."' ORDER BY AppDate DESC")->result();
	if(count($check)>0){
	    $query         = $check[0];	
        $data['AppNo'] = $check[0]->AppNo;
		$this->db->where('AppNo', $check[0]->AppNo);
		$this->db->update('ES_Admission', $data);
		if($data['department']=='gs' || $data['department']=='jhs' || $data['department']=='shs'){
			$exec = $this->create_k12($data['AppNo']);
		}
	}else{
		$query =  $this->db->insert('ES_Admission',$data);
			      $this->db->insert('ES_AdmissionInterview',['AppNo' => $data['AppNo']]);
			      $this->db->insert('ES_CET_Results',['AppNo' => $data['AppNo']]);
		if($data['department']=='gs' || $data['department']=='jhs' || $data['department']=='shs'){
			$exec = $this->create_k12($data['AppNo']);
		}
	}
    return $query;
  }
  
  function create_k12($appno){
    $qry = "INSERT INTO ESv2_Admission(AppNo,AppDate
						                 ,TermID,StatusID,AppTypeID
						                 ,CampusID,ProgramID,MajorID
                                                                 ,GradeLevelID
						                 ,LRN
						                 ,LastName,FirstName,MiddleName,MiddleInitial,ExtName
						                 ,Gender,PlaceOfBirth,DateOfBirth
                                         ,CivilStatusID,NationalityID,ReligionID,FamilyID)
                                        SELECT TOP 1 AppNo,AppDate
		                                                 ,TermID,AdmStatusID,ApplyTypeID
			                                         ,Choice1_CampusID,Choice1_Course,ISNULL(Choice1_CourseMajor,0) as Choice1_CourseMajor
			                                         ,(CASE WHEN ApplyTypeID=20 THEN 5 ELSE 6 END) as GradeLevelID
			                                         ,LRN
			                                         ,LastName,FirstName,MiddleName,MiddleInitial,ExtName
			                                         ,Gender,PlaceOfBirth,DateOfBirth
                                                     ,CivilStatusID,NationalityID,ReligionID,AppNo as FamilyID
	                                               FROM ES_Admission 
  	                                             WHERE AppNo='".$appno."' AND AppNo NOT IN (SELECT AppNo FROM ESv2_Admission)

                 INSERT INTO ESv2_Admission_FamilyBackground(FamilyID,Guardian_TelNo,Guardian_Mobile,Guardian_Email
                                                            ,Guardian_Address,Guardian_Street,Guardian_Barangay,Guardian_TownCity,Guardian_Province,Guardian_ZipCode
                                                            ,Father_Name,Father_Occupation,Father_Company,Father_Email,Father_TelNo
                                                            ,Mother_Name,Mother_Occupation,Mother_Company,Mother_Email,Mother_TelNo)
                                                      SELECT AppNo,TelNo,MobileNo,Email
                                                            ,Perm_Address,Perm_Street,Perm_Barangay,Perm_TownCity,Perm_Province,Perm_ZipCode
                                                            ,Father,Father_Occupation,Father_Company,Father_Email,Father_TelNo
                                                            ,Mother,Mother_Occupation,Mother_Company,Mother_Email,Mother_TelNo 
                                                        FROM ES_Admission
                                                       WHERE AppNo='".$appno."'";
    $exec = $this->db->query($qry);
    return true;
  }

  function alterappno($data,$appno){
    $query =  $this->db->insert('ES_Admission',$data);
		   $this->db->insert('ES_AdmissionInterview',['AppNo' => $data['AppNo']]);
		   $this->db->insert('ES_CET_Results',['AppNo' => $data['AppNo']]);
    return $query;
  }
  
  function appno(){
    $this->db->select("dbo.fn_GenerateAPPNO('".date('Y')."0%')");
    return $this->db->get()->result_array();
  }
  
}
?>