<?php
Class mod_advising extends CI_Model
{
 private $termid                = 0;
 private $campusid         = 0;
 private $ayterm               = '';
 private $schoolterm       = '';
 private $curriculumid     = 0;
 private $regid                  = 0;
 private $yrlvlid                 = 0;
 private $minload            = 0;
 private $maxload           = 0;
 private $countenr               = 0;
 private $isadvised              = 0;
 private $subj_elecoffered = array();
 private $subj_autoadv       = array();
 private $advise_style         = 1;
 private $regularsdate        = '2017-11-24 00:00';
 private $irregsdate             = '2017-11-25 00:00';
 
 function call_SPadv($param=null) 
 {
   $result = false;
   if($param!=''){
    //$query = "EXEC sis_AutoAdvise @StudentNo ='".$param."';";
    //$result = $this->db->query($query);
    //return $result->result();	
      return $this->exec_AutoAdvise($param);
   }else  
      return false;
 }
 
 function call_SPconfig($param=null)
 {
   $result = false;
   if($param!='')
    return $this->exec_InitialConfiguration($param);
   else
    return $result;
 }
  
 function changetoreg($termid,$studno){
	 $exec = $this->db->query("UPDATE stats SET stats.IsRegular=(CASE WHEN (SELECT COUNT(TermID) from dbo.fn_getRetentionPreviousSem(".$termid.",".$studno."))=0 THEN 1 ELSE 0 END)
								 FROM ES_Registrations as r
						   INNER JOIN sis_StudentCurrentStatus as stats ON r.StudentNo=stats.StudentNo
								WHERE r.TermID='".$termid."' AND r.StudentNo='".$studno."' AND r.IsOnlineEnrolment=1");
	 return true;							
 }
 
 function studstats($studno){
   $result = array('advstyle'=>1 ,'startdate'=>$this->regularsdate);
   $exec   = $this->db->query("SELECT TOP 1 IsRegular, (CASE WHEN IsRegular=1 THEN '".$this->regularsdate."' ELSE '".$this->irregsdate."' END) as StartDate FROM sis_StudentCurrentStatus WHERE StudentNo='".$studno."'");	
   if($exec && count($exec->result())>0){
	  $rs = $exec->result()[0];
      return array('advstyle'=>$rs->IsRegular,'startdate'=>$rs->StartDate);	  
   }else{
	  return array('advstyle'=>0,'startdate'=>'');	   
   }
   return $result;  
 }
  
 
 function isallowtobeahead($termid,$studno){
	 if($termid && $studno){
		 $qry  = "SELECT TOP 1 DateStart FROM sis_AllowToEnrollAhead WHERE TermID='".$termid."' AND StudentNo='".$studno."'";
		 $exec = $this->db->query($qry);
		 if($exec && count($exec->result())>0){
			 $rs = $exec->result()[0];
			 return $rs->DateStart;
		 }
	 }
	 return false;
 }
 
 function generate_controller($param=null){
  date_default_timezone_set("Asia/Taipei");
  $data = array('activetermid' => 0
               ,'activeterm'   => ''
               ,'collegeid'    => 0
               ,'college'      => ''
               ,'campusid'     => 0
	           ,'campus'       => ''
	           ,'advperiod'    => ''
               ,'enperiod'     => ''
               ,'onadvperiod'  => 0
               ,'onenperiod'   => 0
	           ,'studentno'    => ''
	           ,'studentname'  => ''
               ,'advisedid'    => ''
               ,'regid'        => ''
               ,'regdate'      => ''
               ,'progid'       => 0
               ,'majorid'      => 0
               ,'program'      => ''
               ,'progclass'    => ''
               ,'curriculumid' => 0
               ,'curriculum'   => ''
               ,'yrlvlid'      => 0
               ,'yrlvl'        => ''
               ,'feesid'       => ''
               ,'tempcode'     => ''
               ,'isassess'     => 0
               ,'minunit'      => 0
               ,'maxunit'      => 0
               ,'btnstatus'    => ''
               ,'status'       => ''
               ,'pstatus'      => ''
               ,'inactive'     => ''
               ,'accounts'     => ''
               ,'withaccount'  => 0
               ,'balance'      => ''
               ,'withbalance'  => 0
               ,'retentionid'  => 1
               ,'retention'    => ''
               ,'regwoass'     => 0
	           ,'snic'         => 0
	           ,'isreg'        => 0
	           ,'xreadv'       => 0
	           ,'xadv'         => 0
	           ,'xenr'         => 0
	           ,'xprt'         => 0
	           ,'xcor'         => 0);
  
  if($param!="" && ($param instanceof Object || is_array($param)))
  {
    if(sizeof($param)>0)
	{
	 $rs = $param[0];
	 $activetermid    = ((property_exists($rs,'Config_TermID'))? ($rs->Config_TermID):0);
     $activeterm      = ((property_exists($rs,'Config_Ayterm'))? ($rs->Config_Ayterm):'');
     $studentno       = ((property_exists($rs,'StudentNo'))? ($rs->StudentNo):'');
	 $studentname     = ((property_exists($rs,'Fullname'))? utf8_encode($rs->Fullname):'');
	 $advtermid       = ((property_exists($rs,'Advise_TermID'))? ($rs->Advise_TermID):'');
     $advterm         = ((property_exists($rs,'Advise_Ayterm'))? ($rs->Advise_Ayterm):'');
	
	 $collegeid       = ((property_exists($rs,'CollegeID'))?($rs->CollegeID):'');
	 $campusid        = ((property_exists($rs,'CampusID'))?($rs->CampusID):'');
	 $xcampusname     = ((property_exists($rs,'Campus'))?($rs->Campus):'');
	
     $advisingstartug = ((property_exists($rs,'AdvisingPeriodStart_Undergrad'))?($rs->AdvisingPeriodStart_Undergrad):'');
     $advisingendug   = ((property_exists($rs,'AdvisingPeriodEnd_Undergrad'))?($rs->AdvisingPeriodEnd_Undergrad):'');
     $advisingstartg  = ((property_exists($rs,'AdvisingPeriodStart_Grad'))?($rs->AdvisingPeriodStart_Grad):'');
     $advisingendg    = ((property_exists($rs,'AdvisingPeriodEnd_Grad'))?($rs->AdvisingPeriodEnd_Grad):'');
	
     $enrollstartug = ((property_exists($rs,'StartEnrollment_Undergrad'))?($rs->StartEnrollment_Undergrad):'');
     $enrollendug   = ((property_exists($rs,'EndEnrollment_Undergrad'))?($rs->EndEnrollment_Undergrad):'');
     $enrollstartg  = ((property_exists($rs,'StartEnrollment_Grad'))?($rs->StartEnrollment_Grad):'');
     $enrollendg    = ((property_exists($rs,'EndEnrollment_Grad'))?($rs->EndEnrollment_Grad):'');
	 
     $onadviseperiod = ((property_exists($rs,'WithinAdvisingPeriod'))?($rs->WithinAdvisingPeriod):0);
     $onenrollperiod = ((property_exists($rs,'WithinEnrollmentPeriod'))?($rs->WithinEnrollmentPeriod):0);
	
	 $xadvisedid    = ((property_exists($rs,'AdvisedID'))?($rs->AdvisedID):'');
	 $xregid        = ((property_exists($rs,'RegistrationID'))?($rs->RegistrationID):'');
	 $xregdate      = ((property_exists($rs,'RegistrationDate'))?($rs->RegistrationDate):'');
	 $xcollege      = ((property_exists($rs,'CollegeName'))?($rs->CollegeName):'');
	 $progid        = ((property_exists($rs,'ProgID'))?($rs->ProgID):0);
	 $majorid       = ((property_exists($rs,'MajorID'))?($rs->MajorID):0);
	 $xprog         = ((property_exists($rs,'AcademicProgram'))?($rs->AcademicProgram):'');
	 $progclass     = ((property_exists($rs,'ProgClass'))?($rs->ProgClass):'');
	 $xcurriculumid = ((property_exists($rs,'CurriculumID'))?($rs->CurriculumID):0);
	 $xcurriculum   = ((property_exists($rs,'CurriculumCode'))?($rs->CurriculumCode):'');
	 $yearlvlid     = ((property_exists($rs,'YearLevelID'))?($rs->YearLevelID):'');
	 $xyrlvl        = ((property_exists($rs,'YearLevel'))?($rs->YearLevel):'');
	 $xminunits     = ((property_exists($rs,'MinUnits'))?($rs->MinUnits):0);
	 $xmaxunits     = ((property_exists($rs,'MaxUnits'))?($rs->MaxUnits):0);
	 $accounts      = ((property_exists($rs,'Accountabilities'))?($rs->Accountabilities):'');
	 $xretentionid  = ((property_exists($rs,'RetentionID') && $rs->RetentionID!='')?($rs->RetentionID):'');
	 $xretention    = ((property_exists($rs,'Retention'))?($rs->Retention):'');
	 $feesid        = ((property_exists($rs,'FeeID'))?($rs->FeeID):'');
	 $xtempcode     = ((property_exists($rs,'TemplateCode'))?($rs->TemplateCode):'');
	 $xbalance      = ((property_exists($rs,'OutstandingBalance'))?($rs->OutstandingBalance):0.00);
	 $xstatus       = ((property_exists($rs,'Status'))?($rs->Status):'');
	 $xprogclass    = ((property_exists($rs,'ProgClass'))?($rs->ProgClass):0);
	 $xinactive     = ((property_exists($rs,'Inactive'))?($rs->Inactive):0);
	 $xwithaccount  = ((property_exists($rs,'AllowWithAcct'))?($rs->AllowWithAcct):0);
	 $xwithbalance  = ((property_exists($rs,'AllowWithBal'))?($rs->AllowWithBal):0);
	 $xmode         = ((property_exists($rs,'Mode'))?($rs->Mode):'');
     $xregwoassess  = ((property_exists($rs,'RegWoutAssess'))?($rs->RegWoutAssess):0);
     $xsnic         = ((property_exists($rs,'snic'))?($rs->snic):0);
     $xisreg        = ((property_exists($rs,'IsRegular'))?($rs->IsRegular):0);
     $xisassess     = ((property_exists($rs,'IsAssessed') && $rs->IsAssessed >0)? 1:0);
     $registerd     = ((property_exists($rs,'Registered') && $rs->Registered >0)? 1:0);
	 
     $xbalance      = (($xbalance==500 && $studentno=='2016300054')?0:$xbalance);
	 $isahead       = $this->isallowtobeahead($activetermid,$studentno); 
	 if(strtotime($isahead) && strtotime($isahead)>strtotime('1990-01-01')){
        $advisingstartug = $isahead;
        $advisingstartg  = $isahead;
        $enrollstartug   = $isahead;
        $enrollstartg    = $isahead;
	 }else{
		$xstats          = $this->studstats($studentno);
        $isreg           = $xstats['advstyle'];		
		$advisingstartug = $xstats['startdate'];
        $advisingstartg  = $xstats['startdate'];
        $enrollstartug   = $xstats['startdate'];
        $enrollstartg    = $xstats['startdate'];
	 }
	 
	 if(defined('ENABLE_RETENTION') && ENABLE_RETENTION==0){
	   $xretentionid = 1;
	 }
	 //$xdata         = $this->checkadvschedctrl($activetermid,$progid); 
	 //$data['datestart'] = $xdata['datestart'];
	 //$data['dateend']   = $xdata['dateend'];
	 //$data['inperiod']  = $xdata['inperiod'];
	 $xbalance     = (($xbalance>0)?$xbalance:'0.00');
	 $xinbetween   = 0;
	 $xcurrentDate = date('M-d-Y');
     if($xprogclass> 20 && $xprogclass<=50)
	 {
	  $advisingstart = date('M-d-Y', strtotime($advisingstartug));
	  $advisingend = date('M-d-Y', strtotime($advisingendug));
	  $enrollstart = date('M-d-Y', strtotime($enrollstartug));
	  $enrollend = date('M-d-Y', strtotime($enrollendug));
	  $advising = "From ".date('F d, Y', strtotime($advisingstartug))." To ".date('F d, Y', strtotime($advisingendug));
      $enrollment = "From ".date('F d, Y', strtotime($enrollstartug))." To ".date('F d, Y', strtotime($enrollendug));
	 }
	 else if($xprogclass>50)
	 {
	  $advisingstart = date('M-d-Y', strtotime($advisingstartg));
	  $advisingend = date('M-d-Y', strtotime($advisingendg));
	  $enrollstart = date('M-d-Y', strtotime($enrollstartg));
	  $enrollend = date('M-d-Y', strtotime($enrollendg));
      $advising = "From ".date('F d, Y', strtotime($advisingstart))." To ".date('F d, Y', strtotime($advisingend));
      $enrollment = "From ".date('F d, Y', strtotime($enrollstartg))." To ".date('F d, Y', strtotime($enrollend));
	 }
	 else
	 {
	  $advisingstart = date('M-d-Y', strtotime($advisingstartug));
	  $advisingend = date('M-d-Y', strtotime($advisingendug));
	  $enrollstart = date('M-d-Y', strtotime($enrollstartug));
	  $enrollend = date('M-d-Y', strtotime($enrollendug));
	  $advising = "From ".date('F d, Y', strtotime($advisingstart))." To ".date('F d, Y', strtotime($advisingend));
      $enrollment = "From ".date('F d, Y', strtotime($enrollstart))." To ".date('F d, Y', strtotime($enrollend));
	 }	 
	 
	 $onadviseperiod  = ((strtotime($xcurrentDate)>= strtotime($advisingstart) && strtotime($xcurrentDate)<=strtotime($advisingend))?1:0);
     $onenrollperiod  = ((strtotime($xcurrentDate)>= strtotime($enrollstart) && strtotime($xcurrentDate)<=strtotime($enrollend))?1:0);

	 if($xcurrentDate>= $enrollstart && $xcurrentDate<= $advisingend){$xinbetween = 1;}
	 if($onadviseperiod==0) $advising = '<b class="txt-color-red">'.$advising.' (Closed)</b>';
	 if($onenrollperiod==0) $enrollment = '<b class="txt-color-red">'.$enrollment.' (Closed)</b>';
	 
	 if($xminunits > $xmaxunits){$xminunits=6; }
	 
	 $regdate = date('M-d-Y', strtotime($xregdate));
	 if($xregid == "" || $xregid == 0)
	 {
	  $xregid  = "";
	  $xregdate = "";
     }
	 
	 if($xbalance==0){$xbalance='0.00';}
	 $btnstatus = '';
	 $xreadv = 0;
	 $xadv   = 0;
	 $xenr   = 0;
	 $xprt   = 0;
	 $xcor   = 0;
	 
	 if($progclass>20)
	 {
	  if($xinactive==1)
	   $xstatus="<b class='txt-color-red'>Unable to process your enrollment because you are inactive. Please visit the registrar's office.</b>";
	  elseif($onadviseperiod==1 || $onenrollperiod==1){
	   $xadv = (($onadviseperiod==1 || $onenrollperiod==1)?1:0);
	   $xenr = (($onadviseperiod==1 || $onenrollperiod==1)?1:0);
	   $btnstatus = '<a href="#" class="btn btn-primary pull-right" onclick="$('."'#xadvise'".').click();"><span class="fa fa-arrow-circle-right"></span> Proceed to Advising</a>';
	  
	   if(strtolower($xstatus)=='not yet registered'){
	    $xstatus="<b class='txt-color-red'>".$xstatus."</b>";
	    $xenr = 0;
	    if($onenrollperiod==1 && (($xbalance>0 && $xwithbalance==0) || (trim($accounts)!='' && $xwithaccount==0)))
	    {
		 $xadv = 0;
		 $btnstatus = '';
	    }
       }elseif(strtolower($xstatus)=='registered' || strtolower($xstatus)=='payment in-process' || strtolower($xstatus)=='in-process' ){
	    $xreadv    = (($xadv==0)?1:0);
	    $xenr      = (($xenr==1 && $xisassess==0)?1:0);
	    $xprt      = (($onenrollperiod==1 && $xisassess==1)?1:0);
	    $xstatus   = (($xisassess<=0)?'Advised':$xstatus); 
	    $btnstatus = (($xisassess==0)?'<a href="javascript:void(0);" class="btn btn-success pull-right" onclick="$('."'#xreg'".').click();"><span class="fa fa-arrow-circle-right"></span> Proceed to Enrollment</a>':'');	  
	    $xstatus="<b class='txt-color-yellow'>".$xstatus."</b>";
	    //$xisreg=1;
	   
	    if($onenrollperiod==1 && (($xbalance>0 && $xwithbalance==0) || (trim($accounts)!='' && $xwithaccount==0)))
	    {
		 $xenr = 0;
		 $xprt = 1;
		 $btnstatus = '';
                 $xprt = $xisassess;
	    }
	    $xadv   =  0;
	   }elseif(strtolower($xstatus)=='validated' || strtolower($xstatus)=='enrolled'){
	    $xcor = (($xenr==1)?1:0);
	    $xenr=0;
	    $xadv=0;
	    $xstatus="<b class='txt-color-green'>".$xstatus."</b>";
	    $btnstatus = '';
	    $xisreg=1;
	   }
	   elseif(($xwithaccount==0 || $xwithbalance==0) && $onadviseperiod==0)
	   {
	    $xadv=0;
	    $xenr=0;
	   }
	  }
	  
	  if($onadviseperiod==1 && $onenrollperiod==0 && ($xretentionid=='' || $xretentionid==0 || $xretentionid>3))
	  {
	   $xretentionid=1;
       $xretention='';		
	  }
	  //$onenrollperiod=1;
	  //$xretentionid=0;
	  //$xmaxunits=0;
	  if($onenrollperiod==1 && ($xretentionid=='' || $xretentionid==0 || ($xretentionid>1 && $xmaxunits<=0)))
	  {
	   $xretention=(($xretentionid=='')?'Undefined':$xretention);
	   $btnstatus='';
	   $xadv=0;
	   $xenr=0;  
	  }	  
	 }	 
	 
	 if($feesid=='' || $feesid==0){
		$xtempcode ='<b class="txt-color-red">NO TEMPLATE OF FEES AVAILABLE!</b>'; 
		$btnstatus ='';
        $xadv      =0;
        $xenr      =0;
        $xprt      =0;		
        $xcor      =0;		
	 }
	 
	//$xcor = $registerd;
	 
	 $data['activetermid'] = $activetermid;
     $data['activeterm']   = $activeterm;
     $data['collegeid']    = $collegeid;
     $data['college']      = $xcollege;
     $data['campusid']     = $campusid;
	 $data['campus']       = $xcampusname;
	 $data['advperiod']    = $advising;
     $data['enperiod']     = $enrollment;
     $data['onadvperiod']  = $onadviseperiod;
     $data['onenperiod']   = $onenrollperiod;
	 $data['studentno']    = $studentno;
	 $data['studentname']  = str_replace("'","`",$studentname);
     $data['advisedid']    = $xadvisedid;
     $data['regid']        = $xregid;
     $data['regdate']      = $xregdate;
     $data['progid']       = $progid;
     $data['majorid']      = $majorid;
     $data['program']      = $xprog;
     $data['progclass']    = $xprogclass;
	 $data['curriculumid'] = $xcurriculumid;
     $data['curriculum']   = $xcurriculum;
     $data['yrlvlid']      = $yearlvlid;
     $data['yrlvl']        = $xyrlvl;
     $data['feesid']       = $feesid;
     $data['tempcode']     = $xtempcode;
     $data['minunit']      = $xminunits;
     $data['maxunit']      = $xmaxunits;
	 $data['inactive']     = $xinactive;
	 $data['accounts']     = str_replace("'","`",$accounts);
     $data['withaccount']  = $xwithaccount;
	 $data['balance']      = $xbalance;
     $data['withbalance']  = $xwithbalance;
	 $data['retentionid']  = $xretentionid;
	 $data['retention']    = $xretention;
     $data['regwoass']     = $xregwoassess;
	 $data['snic']         = $xsnic;
	 $data['isreg']        = $xisreg;
	 $data['btnstatus']    = $btnstatus;
	 $data['status']       = $xstatus;
	 $data['xreadv']       = $xreadv;
	 $data['xadv']         = $xadv;
	 $data['xenr']         = $xenr;
	 $data['xprt']         = $xprt;
	 $data['xcor']         = $xcor;
	}
  }
  return $data;
 }
 
 function checkadvschedctrl($termid=-1,$progid=0,$majorid=0,$yrlvl=0)
 {
   $result = array('dis_datestart'=>'','datestart'=>'','dis_dateend'=>'','dateend'=>'','inperiod'=>-1);
   $query = "SELECT TOP 1 DateStart,DateEnd,(CASE WHEN DateStart IS NOT NULL AND DateEnd IS NOT NULL AND (GETDATE() BETWEEN DateStart AND DateEnd) THEN 1 ELSE 0 END) AS InPeriod 
               FROM sis_AdvisingSchedule 
			  WHERE TermID='".$termid."' 
			    AND ProgID = '".$progid."' 
			    AND MajorID = '".$majorid."'
			    AND ".(($yrlvl>0)?"(ISNULL(YearLvlID,0)=".$yrlvl." OR ISNULL(YearLvlID,0)=0)":"ISNULL(YearLvlID,0)=0")."				
			    AND Inactive=0 ".(($yrlvl>0)?"ORDER BY YearLvlID DESC":"");	 
   $exec  = $this->db->query($query);
   if($exec)
   {
	 $rdata = $exec->result();
     foreach($rdata as $rs)
     {
	  $result['datestart'] = ((@property_exists($rs,'DateStart'))?($rs->DateStart):'');
	  $result['dateend']   = ((@property_exists($rs,'DateEnd'))?($rs->DateEnd):'');	 
	  $result['inperiod']  = ((@property_exists($rs,'InPeriod'))?($rs->InPeriod):0);
	 
	  if($result['datestart']!='')
	  {
	   $tmpdate = new DateTime($result['datestart']);
       $result['dis_datestart'] = $tmpdate->format('F d, Y h:i A');
	  }
	  if($result['dateend']!='')
	  {
	   $tmpdate = new DateTime($result['dateend']);
       $result['dis_dateend'] = $tmpdate->format('F d, Y h:i A');
	  }	  
	 
	 }	 
   }	
   return $result;   
 }
 
 function generate_alert($advperiod=0,$enperiod=0,$balance=0,$withbalance=0,$accounts='',$withaccount=0,$regonly=0,$inactive=0)
 {
  $result=array('shedadv'  => 0
			   ,'shedenr'  => 0
			   ,'balance'  => 0
			   ,'accounts' => 0
			   ,'regonly'  => 0
			   ,'inactive' => 0);

  $result['shedadv']  = (($advperiod==0 && $enperiod==0)?1:0);
  $result['shedenr']  = (($advperiod==0 && $enperiod==0)?1:0);
  $result['balance']  = ((($advperiod==0 || $enperiod==1) && ($balance>0 && $withbalance==0))?1:0);
  $result['account']  = ((($advperiod==0 || $enperiod==1) && ($accounts!='' && $withaccount==0))?1:0);
  $result['regonly']  = (($regonly==1)?1:0);
  $result['inactive'] = (($inactive==1)?1:0);
	                        
  return $result;	 
 }
 
 function call_xtraDetails($param=null) {
  $result = false;
  if($param!='')
   {
    $query = "SELECT 
	                dbo.fn_ProgramCollegeName(s.Progid) AS CollegeName, 
	                dbo.fn_GetProgramNameWithMajor(ProgID,MajorDiscID) AS Program, 
					dbo.fn_CurriculumCode(CurriculumID) AS Curriculum
	           FROM dbo.ES_Students s
	         WHERE s.StudentNo = '".$param."'";
    $result = $this->db->query($query);
    return $result->row();	
   }  
  return $result;
 } 
 
 
 function saveadvising($termid=0,$studentno=0,$query='') {
  if($query!='')
  {
   $this->db->query($query); 
   $this->db->flush_cache();
   $result = $this->db->query("SELECT TOP 1 AdvisedID FROM ES_Advising WHERE TermID=".$termid." AND StudentNo='".$studentno."'");
   if($result->num_rows()>0)
   {
    $row = $result->row(); 
    return $row->AdvisedID;
   }
   else
	return 'failed';
  }
  else
   return 'failed';
 }
 
 function get_RegID($studentno='',$termid='')
 {
  $output=false;
  if($studentno!='' && $termid!='')
  {  
   $query = "SELECT TOP 1 RegID from ES_Registrations WHERE StudentNo='".$studentno."' and TermID='".$termid."'";  
   $result = $this->db->query($query);  
   $output = (($result)?($result->result()[0]->RegID) : '');
  }
  return $output;  
 }
 
 function savereg($termid=0,$studentno=0) 
 {
  if($termid!=0 && $studentno!=0)
  {
   $this->db->query("EXEC sis_SaveRegistration @TermID=".$termid.",@StudentNo='".$studentno."'"); 
   $this->db->flush_cache();
  }
 }
 
 function saveadvisingdetails($adviseid=0,$seqno=0,$subjid=0,$electiveid=0) 
 {
  $this->db->query("EXEC sis_SaveAdvisingDetails @AdvisedID=".$adviseid.",@SubjectID=".$subjid.",@ElectiveSubjectID=".$electiveid.",@SeqNo=".$seqno.",@ScheduleID=0;"); 
  $this->db->flush_cache();  
 }
 
 function generate_legend(){
  $display='';
  $keys = array('Blank','Failed','Passed','Incomplete','Dropped','Unofficially Dropped','Conditional Failure','Withdrawal','Unauthorized Withdrawal','No Grade','Leave of Absences','No Credit','Audit');
  $display='';
  foreach($keys as $i)
  {
   $xcss = $this->colorcoding($i,1);
   if($i=='Blank')
   {$display.='<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><span class="fa fa-square-o"></span> <small><b>'.$i.'</b></small></div>';}
   else
   {$display.='<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><span '.$xcss.' class="fa fa-square"></span> <small><b>'.$i.'</b></small></div>';}
  }
  return $display;
 } 
 
 
 function putDecimal($value){
     if(is_numeric($value))
	 {$xval = number_format($value,2);
	  return $xval;}
	 else
	 {
	  if($value=='' || $value==' ')
	  {return 0.00;}
	  else
	  {return $value;}
	 }
 }
 
 function colorcoding($value='',$lgnd=0){
  if($value == '' || $value == ' ' || $value=='Blank')
  {
   if($lgnd==0)
   {return "class='bg-color-white'";}
   elseif($lgnd==1)
   {return "style='background-color:black;color:white;'";}
  }
  elseif($value == 'Footer' || $value == 'footer')
  {
   return "class='bg-color-blueLight' style='font-size:8pt;'";
  }
  elseif($value == 'Failed' || $value == 'failed'|| $value== 'FAIL' || $value >= 5)
  {
   if($lgnd==0)
   {return "class='bg-color-red' style='color:white;'";}
   elseif($lgnd==1)
   {return "style='color: #a90329;'";}
  }
  elseif($value == 'Passed' || $value == 'passed'|| $value== 'PASS'|| $value== 'Credited'|| $value== 'credited' || ($value <= 3 && $value > 0 ))
  {
   if($lgnd==0)
   {return "style='background-color: #75FF75;'";}
   elseif($lgnd==1)
   {return "style='color: #75FF75;'";}
  }
  elseif($value == 'Incomplete' || $value == 'incomplete'|| $value== 'INC' || ($value <= 4 && $value > 3))
  {
   if($lgnd==0)
   {return "style='background-color:#CCCCCC;'";}
   elseif($lgnd==1)
   {return "style='color: #CCCCCC;'";}
  }
  elseif($value == 'Dropped' || $value == 'dropped'|| $value== 'DRP')
  {
   if($lgnd==0)
   {return "class='bg-color-orange'";}
   elseif($lgnd==1)
   {return "style='color: orange;'";}
  }
  elseif($value == 'Unofficially Dropped' || $value == 'unofficially dropped'|| $value== 'UD')
  {
   if($lgnd==0)
   {return "class='bg-color-orangeDark'";}
   elseif($lgnd==1)
   {return "style='color: #a57225;'";}
  }
  elseif($value == 'Conditional Failure' || $value == 'conditional failure'|| $value== 'CF')
  {
   if($lgnd==0)
   {return "style='background-color:#FF00FF;'";}
   elseif($lgnd==1)
   {return "style='color:#FF00FF;'";}
  }
  elseif($value == 'Withdrawal' || $value == 'withdrawal'|| $value== 'W')
  {
   if($lgnd==0)
   {return "style='background-color:#CC66FF;'";}
   elseif($lgnd==1)
   {return "style='color:#CC66FF;'";}
  }
  elseif($value == 'Unauthorized Withdrawal' || $value == 'unauthorized withdrawal'|| $value== 'UW')
  {
   if($lgnd==0)
   {return "style='background-color:#FF9147;'";}
   elseif($lgnd==1)
   {return "style='color:#FF9147;'";}
  }
  elseif($value == 'No Grade' || $value == 'no grade'|| $value== 'NoGRDE')
  {
   if($lgnd==0)
   {return "style='background-color:#FFFF66;'";}
   elseif($lgnd==1)
   {return "style='color:#FFFF66;'";}
  }
  elseif($value == 'Leave of Absences' || $value == 'leave of absences'|| $value== 'LOA')
  {
   if($lgnd==0)
   {return "style='background-color:black;color:white;'";}
   elseif($lgnd==1)
   {return "style='color:black;'";}
  }
  elseif($value == 'No Credit' || $value == 'No Credit'|| $value== 'NC' || $value== 'Not Credited')
  {
   if($lgnd==0)
   {return "class='bg-color-pink'";}
   elseif($lgnd==1)
   {return "style='color:pink'";}
  }
  elseif($value == 'Audit' || $value == 'audit'|| $value== 'Aud')
  {
   if($lgnd==0)
   {return "style='background-color:#9900FF;'";}
   elseif($lgnd==1)
   {return "style='color:#9900FF;'";}
  }
  elseif($value == '* UNPOSTED' || $value == '* Unposted'|| $value== '* unposted'|| $value== 'unposted')
  {
   if($lgnd==0)
   {return "style='background-color:#F0F0F0;'";}
   elseif($lgnd==1)
   {return "style='color:#F0F0F0;'";}
  }
 }
 
 function gen_icons($final,$remarks,$offered){
  if(($final=='')&&($remarks=='') &&($offered==''||$offered=='0'||$offered==0))
  {return "";}
  
  if(($final!=''||$final=='')&&($remarks==''||($remarks!='' && $remarks!='Passed' && $remarks!='PASSED' && $remarks!='Credited' && $remarks!='* UNPOSTED')) &&($offered!='' && $offered!='0' && $offered!=0))
  {return "<span class='fa fa-check-circle' style='Color:Green'></span>";}
  
  
  if(($final!='')&&($remarks!='') &&($offered!='' || $offered!='0' || $offered!=0))
  {return "<span class='fa fa-lock' style='Color:#CC9900'></span>";}
 }
 
 function generate_advising($param=null){
  $xdetails = array();
  $thead    = "<thead>
                <th>#</th>
				<th>COURSE CODE</th>
				<th>TITLE</th>
				<th><center>LEC</center></th>
				<th><center>LAB</center></th>
				<th><center>CREDIT</center></th>
			   </thead>";
  $tbody = "<tbody><tr><td colspan='6'><center id='tblloader1'>No Data is Available</center></td></tr></tbody>";
  if($param!='' && $param!=Null)
  {
   $count = 0;
   $totallec = 0;
   $totallab = 0;
   $totalcredit = 0;
   $minload  = 0;
   $maxload  = 0;
   $current  = 0;
   $prev_subj = '';
   
   $tbody = "<tbody>";
   foreach($param as $rs)
   {
    $pushdsply = 0;
    $termid  = property_exists($rs,'TermID')? $rs->TermID : 0;
    $ayterm  = property_exists($rs,'Ayterm')? $rs->Ayterm : 0;
    $campusid  = property_exists($rs,'CampusID')? $rs->CampusID : 1;
    $collegeid  = property_exists($rs,'CollegeID')? $rs->CollegeID : 0;
    $progid  = property_exists($rs,'ProgID')? $rs->ProgID : 0;
    $studentno  = property_exists($rs,'StudentNo')? $rs->StudentNo : '';
    $yrlvlid  = property_exists($rs,'YearLevelID')? $rs->YearLevelID : '';
    $minload  = property_exists($rs,'MinLoad')? $rs->MinLoad : 0;
    $maxload  = property_exists($rs,'MaxLoad')? $rs->MaxLoad : 0;
    $advisedsubj  = property_exists($rs,'AdvisedSubject')? $rs->AdvisedSubject : 0;
    $advisedunit  = property_exists($rs,'AdvisedUnits')? $rs->AdvisedUnits : 0;
    $adviseddate  = property_exists($rs,'DateAdvised')? $rs->DateAdvised : '';
    $adviserid  = property_exists($rs,'AdviserID')? $rs->AdviserID : '';
    $modifiedby  = property_exists($rs,'ModifiedBy')? $rs->ModifiedBy : '';
    $modifieddate  = property_exists($rs,'DateModified')? $rs->DateModified : '';
    $accesscode  = property_exists($rs,'AccessCode')? $rs->AccessCode : '';
    $feeid  = property_exists($rs,'FeesID')? $rs->FeesID : '';
    $feetemp = property_exists($rs,'FeesTemplate')? $rs->FeesTemplate : '';
    $account  = property_exists($rs,'Accountabilities')? $rs->Accountabilities : '';
    $outbalance  = property_exists($rs,'OutBalance')? $rs->OutBalance : 0;
    $regid  = property_exists($rs,'RegID')? $rs->RegID : '';
    $subjid  = property_exists($rs,'SubjectID')? $rs->SubjectID : '';
    $subjcode  = property_exists($rs,'SubjectCode')? $rs->SubjectCode : '';
    $subjtitle  = property_exists($rs,'SubjectTitle')? $rs->SubjectTitle : '';
    $acadunits  = property_exists($rs,'AcadUnits')? $rs->AcadUnits : 0;
    $labunits  = property_exists($rs,'LabUnits')? $rs->LabUnits : 0;
    $noadvised  = property_exists($rs,'NumberAdvised')? $rs->NumberAdvised : 0;
    $nozerobase  = property_exists($rs,'NonZeroBase')? $rs->NonZeroBase : 0;
    $isnonacad  = property_exists($rs,'IsNonAcademic')? $rs->IsNonAcademic : 0;
	
	$xgroup  = property_exists($rs,'OneSubjKey')? $rs->OneSubjKey : 0;
	//$xgroup='0';
	//$xgroup='1';


	if($prev_subj == $subjcode)
	 continue;
	else
	 $prev_subj = $subjcode;	

	if($minload=='')
	{$minload=0;}
	if($maxload=='')
	{$maxload=0;}
	
 	$xcredit = $acadunits + $labunits;
	$current += $xcredit;
	$xcredit = $this->putDecimal($xcredit);
	
	if($isnonacad==1)
	{
	 $xdsplylec = $acadunits > 0 ? "(".$acadunits.")" : $acadunits;
	 $xdsplylab = $labunits > 0 ? "(".$labunits.")" : $labunits;
	 $xdsplycredit = $xcredit > 0 ? "(".$xcredit.")" : $xcredit;
	}
	else
	{
	 $xdsplylec = $acadunits;
	 $xdsplylab = $labunits;
	 $xdsplycredit = $xcredit;
	}
	
	$pushdsply = 1;
	if($noadvised>0)
	{
     $totallec += $acadunits;
     $totallab += $labunits;
	 $totalcredit = $advisedunit;
	 
	 $pushdsply = 1;
	}
	else
	{
	 if($current<=$maxload && $maxload>0)
	 {
     $totallec += $acadunits;
     $totallab += $labunits;
	 $totalcredit += ($acadunits+$labunits);
	 $pushdsply = 1;
     }
	 elseif($maxload==0)
	 {
	  $tbody = "<tr><td colspan='6'><center id='tblloader1'><b><i class='fa fa-warning'></i> No Max Unit Load. Please go to register to fix.</b></center></td></tr>";
	  $xdetails = $minload.'[- -]'
		         .$maxload.'[- -]'
		         .'0[- -]'
		         .$totallec.'[- -]'
		         .$totallab.'[- -]'
		         .$totalcredit.'[- -]'
		         .'-1';	
	 }
    }
	
	if($pushdsply == 1)
	{
     $count += 1;
	 $tbody .= '<tr>
                 <td>'.$count.'<input type="hidden" id="sbjid'.$count.'" name="sbjid'.$count.'" data-group="'.$xgroup.'" data-code="'.$subjcode.'" data-title="'.$subjtitle.'" value="'.$subjid.'"/></td>
                 <td>'.$subjcode.'</td>
                 <td>'.$subjtitle.'</td>
                 <td><center>'.$xdsplylec.'</center></td>
                 <td><center>'.$xdsplylab.'</center></td>
			     <td><center>'.$xdsplycredit.'</center></td>
                </tr>';
			   
     $xdetails['min'] = $minload;
	 $xdetails['max'] = $maxload;
	 $xdetails['count'] = $count;
	 $xdetails['totallec'] = $totallec;
	 $xdetails['totallab'] = $totallab;
	 $xdetails['totalcredit'] = $totalcredit;
	 $xdetails['noadvised'] = $noadvised;	
	}			
   }
   $tbody .= "</tbody>";

   if($count<=1 && $subjid=='' && $subjtitle=='')
   {
    $tbody = "<tbody><tr><td colspan='6'><center id='tblloader1'>No Data is Available</center></td></tr></tbody>";
   }
   elseif($count<=1 && $subjid=='' && $subjtitle=='NO SUBJECTS OFFERED YET')
   {
     $tbody = "<tbody><tr><td colspan='6'><center id='tblloader1'><b><i class='fa fa-warning'></i> NO SUBJECT IS OFFERED YET.</b></center></td></tr></tbody>";
	 $xdetails['min'] = $minload;
	 $xdetails['max'] = $maxload;
	 $xdetails['count'] = 0;
	 $xdetails['totallec'] = $totallec;
	 $xdetails['totallab'] = $totallab;
	 $xdetails['totalcredit'] = $totalcredit;
	 $xdetails['noadvised'] = $noadvised;	
   }
  }
  
  $xtable="<table class='table table-bordered'>".$thead.$tbody."</table>";
  return array($xdetails,$xtable);
 }
 
 
 function checkadvisingperiod($studno=0){
  $result = $this->db->query("SELECT dbo.fn_sisIsAdvisingOpen('".$studno."') as OnPeriod");
  if($result->num_rows()>0)
  {
    $row = $result->row(); 
    return $row->OnPeriod;
  }
  else
  {return '0';}
 }
 
 function checkenrollmentperiod($termid){
  $result = $this->db->query("SELECT ISNULL(1,0) AS OnPeriod  FROM ES_AYTermConfig WHERE TermID =".$termid."  AND GETDATE() BETWEEN AdvisingPeriodStart AND AdvisingPeriodEnd");
  if($result->num_rows()>0)
  {
    $row = $result->row(); 
    return $row->OnPeriod;
  }
  else
  {return '0';}
 }
 
 function getofferedsubj($termid=0,$studno=0)
 {
  $query ="EXEC sis_OfferedSubject @TermID=".$termid.", @StudentNo='".$studno."';"; //@CurriculumID=".$curriculum.";";
  $result = $this->db->query($query); 
  $this->db->flush_cache();
  return $result->result();
 }
 
 function listofferedsubj($row='')
 {
  if($row!='')
  {
   $SIC='';
   $SNIC='';
   $SICcount=1;
   $SNICcount=1;
   if($row!='')
   {
    foreach($row as $rs)
    {
     $subjid = property_exists($rs,'SubjectID')? $rs->SubjectID : 0;
	 $subjcode = property_exists($rs,'SubjectCode')? $rs->SubjectCode : '';
	 $subjtitle = property_exists($rs,'SubjectTitle')? $rs->SubjectTitle : '';
     $lec = property_exists($rs,'LectUnits')? $rs->LectUnits : 0.0;
     $lab = property_exists($rs,'LabUnits')? $rs->LabUnits : 0.0;
     $credit = property_exists($rs,'CreditUnits')? $rs->CreditUnits : 0.0;
     $stat = property_exists($rs,'Stat')? $rs->Stat : -1;
     $prereq = property_exists($rs,'PREREQUISITEPASSED')? $rs->PREREQUISITEPASSED : 0;
     $remark = property_exists($rs,'Remarks')? $rs->Remarks : '';
	 $allow = '1';
	 
	 if($stat==1)
	 {
	 
	 if($prereq == 0)
	 {$allow = 'p';}
	 else
	 {
	  if($remark=='Passed')
	  {$allow = 'pass';}
	  if($remark=='Credited')
	  {$allow = 'cre';}
	  if($remark=='Unposted')
	  {$allow = 'unp';}
	 }
	 
	 
	 $SIC .= "<tr id='s".$subjid."' onclick='subjICselected(this);'>
	          <td>".$SICcount."</td>
	          <td>".$subjcode."</td>
	          <td>".$subjtitle."</td>
	          <td>".$lec."</td>
	          <td>".$lab."</td>
	          <td style='display:none;'>".$credit."</td>
	          <td style='display:none;'>".$allow."</td>
	         </tr>";
			 
     $SICcount++;
	 }
	 elseif($stat==0)
	 {
	 $SNIC .= "<tr id='s".$subjid."' onclick='subjNICselected(this);'>
	          <td>".$SNICcount."</td>
	          <td>".$subjcode."</td>
	          <td>".$subjtitle."</td>
	          <td>".$lec."</td>
	          <td>".$lab."</td>
	          <td style='display:none;'>".$credit."</td>
	          <td style='display:none;'>".$allow."</td>
	         </tr>";
     
     $SNICcount++;			 
	 }
    }
	/*
	if($SIC!='')
	{$SIC='<table><tbody>'.$SIC.'</tbody></table>';}
	if($SNIC!='')
	{$SNIC='<table><tbody>'.$SNIC.'</tbody></table>';}
    return array($SIC,$SNIC);
	*/
   }	
   return array($SIC,$SNIC);
  }
 }

 function sendrequest($studno=null, $schedid, $msg){
    $result = false;
	 if($studno!=''){
		$query = "exec sp_studentrequest '".$studno."', '".$schedid."', '".$msg."'";
		$result = $this->db->query($query);
		return $result->row();	
	 }  
	return $result;
 }
 
 function enrollmentpolicy($stats="",$inactive=0,$onadvperiod=0,$onenperiod=0,$hasbalance=0,$hasaccounts=0,$allowbalance=0,$allowaccount=0,$regwoassess=0)
 {
  $advising=0;
  $enrollment=0;
  $printprereg=0;
  $printcor=0;
  $alertid='';
  $alertnote='';
  $statstyle='';
  
  if($inactive==1)
  {
    $alertid = 0;
  }
  else
  {
    if(($onadvperiod==1 && $onenperiod==1)||($onadvperiod==0 && $onenperiod==1))
    {
	 $advising=1;
	 $enrollment=1;
	}
    elseif($onadvperiod==1 && $onenperiod==0)
    {
	 $advising=1;
	}	
	
	if($stats=='Not Yet Registered' || $stats=='')
	 {
	  if(($onadvperiod==1||$onenperiod==1) && $allowaccount==1 && $withbalance==1)
	  {
	   $advising=1;
	   $stats = $stats.'  <a href="#" class="btn btn-primary pull-right" onclick="$('."'#xadvise'".').click();"><span class="fa fa-arrow-circle-right"></span> Proceed to Advising</a>';
	  }
	  else
	  {
	   if($onadvperiod==0)
	   {$alertid = 1;}
	  }
	 }
	 elseif($stats=='Registered')
	 {
	   if($onenperiod==1)
	   {
	   $enrollment=1;
       $printprereg=1;
	   }
	   else
	   {
	   $alertid=2;
	   }
	 }
	 elseif($stats=='Validated' || $stats=='Enrolled')
	 {
	  if($onadvperiod==1||$onenperiod==1)
	  {$printcor=1;}
	 } 
	
  }
	 
	 
  return array($advising,$enrollment,$printprereg,$printcor,$alertid,$alertnote,$statstyle);	 
 }

 function statscolor($stats='',$inactive=0)
 {
  if(strtolower($stats)=='not yet registered' || $stats=='' || $inactive==1)
   return "style='color:red;'";
  elseif($stats=='Registered' || $stats=='Enrolled')
   return "style='color:blue;'";
  else
   return "";
 }
 
  function cancelrequest($studno=null, $schedid)
  {
	$result = false;
	 if($studno!=''){
		$this->db->where('StudentNo', $studno);
		$this->db->where('ScheduleID', $schedid);
		$result = $this->db->delete('ES_ClassSchedules_AllowedStudents');		
		return $result;
	 }  
	return $result;
  }

  function ipreferredsection($advisedid, $sectionid)
  {
    $query = "EXEC sp_preparedThisSection '".$advisedid."','".$sectionid."'";  
    $result = $this->db->query($query);  
    return $result->result();		
  }
  
  function deleteadvsubject($advid=0,$subject=0)
  {
    $result = $this->db->delete('ES_Advising_Details', array('AdvisedID' => $advid,'SubjectID' => $subject));
    if($result)	
	{return 1;}
	else
	{return 0;}
  }
  
  function deleteregsubject($regid=0,$schedule=0)
  {
    $result = $this->db->delete('ES_RegistrationDetails', array('RegID' => $regid,'ScheduleID' => $schedule));
    if($result)	
	{return 1;}
	else
	{return 0;}
  }
  
  function addsubject($advid=0,$subject=0)
  { 
    $query = "INSERT INTO ES_Advising_Details(AdvisedID,SubjectID,ElectiveSubjectID,SeqNo,ScheduleID) VALUES(".$advid.",".$subject.",0,dbo.sis_GetSeqNo_AdvDetails(".$advid."),0);";
    $result = $this->db->query($query);
	if($result)
	{return 1;}
	else
	{return 0;}
  }
  
  function get_ConfigTermDetail($stdno='')
  {
   $this->db->reconnect();
   $query = "SELECT TOP 1 t.TermID AS TermID
			,s.CampusID AS CampusID
			,CONCAT(t.AcademicYear,' ',t.SchoolTerm) AS AYTerm
			,t.SchoolTerm AS SchoolTerm
			,dbo.fn_sisWithinAdvisingPeriod(t.TermID,s.StudentNo) AS WithinAdvisingPeriod
			,dbo.fn_sisWithinEnrollmentPeriod(t.TermID,s.StudentNo) AS WithinEnrollmentPeriod
			,CASE WHEN dbo.fn_sisConfigAllowToEnrollWithAccountabilities() = 1 THEN 1 ELSE (CASE WHEN ISNULL((SELECT ac.Cleared FROM ES_StudentAccountabilities AS ac WHERE ac.StudentNo = s.StudentNo AND ac.TermID = t.TermID),1) = 1 THEN 1 ELSE 0 END) END AS AllowWithAccount
			,CASE WHEN dbo.fn_sisConfigAllowToEnrollWithBalance() = 1 THEN 1 ELSE (CASE WHEN ISNULL((SELECT COUNT(rb.IndexID) FROM ES_RegisterWithBalance AS rb WHERE rb.StudentNo = s.StudentNo AND rb.TermID = t.TermID),0) > 0 THEN 1 ELSE (CASE WHEN dbo.fn_StudentForwardedBalance(1, s.StudentNo, t.TermID) <= dbo.fn_sisConfigAllowOutstandingBalance() THEN 1 ELSE 0 END ) END ) END AS AllowWithBalance
			,(SELECT COUNT(AdvisedID) FROM ES_Advising WHERE StudentNo = s.StudentNo AND TermID = t.TermID) AS IsAlreadyAdvise
			,(SELECT COUNT(r.RegID) FROM ES_Registrations AS r WHERE r.StudentNo = s.StudentNo AND r.TermID = t.TermID AND r.CampusID=s.CampusID) AS CountEnrollment
			,r.RegID
			,ISNULL(adj.Min_NewLoad,dbo.fn_sisConfigDefaultMinLoadUnits(p.ProgClass,t.TermID)) as MinUnits
			,CASE WHEN adj.NewLoad IS NULL THEN
			  CASE WHEN t.SchoolTerm='Summer' THEN
			   dbo.fn_sisConfigDefaultMaxLoadUnits(p.ProgClass,t.TermID) 
			  ELSE
			   CASE WHEN r.MaxUnitsAllowed IS NULL THEN dbo.fn_GetMaxLoadUnits_r2(s.StudentNo,t.TermID, dbo.sis_CurricularLevel(s.StudentNo)) ELSE r.MaxUnitsAllowed END
			  END
             ELSE
			  adj.NewLoad
			 END as MaxUnits
			,s.CurriculumID as CurriculumID 
            ,dbo.fn_getAutoYearLevel(s.StudentNo) AS YearLevelID
		FROM ES_AYTerm t 
  INNER JOIN ES_Students s ON s.StudentNo='".$stdno."'
  LEFT OUTER JOIN ES_Programs as p ON s.ProgID = p.ProgID
  LEFT OUTER JOIN ES_AdjustMaxLoadsUnit AS adj ON s.StudentNo=adj.StudentNo AND t.TermID=adj.TermID AND s.CampusID=adj.CampusID
  LEFT OUTER JOIN ES_Registrations AS r ON r.StudentNo=s.StudentNo AND r.TermID=t.TermID AND r.CampusID=s.CampusID
	   WHERE t.TermID = dbo.fn_sisConfigNetworkTermID()
	ORDER BY r.RegID DESC";
    $result = $this->db->query($query);
	if($result)
	{
	  $data = array();
      foreach($result->result() as $rs)
	  {
	   $data['TermID']           = ((@property_exists($rs,'TermID'))? $rs->TermID : 0); 
	   $data['CampusID']         = ((@property_exists($rs,'CampusID'))? $rs->CampusID : 1); 
	   $data['AYTerm']           = ((@property_exists($rs,'AYTerm'))? $rs->AYTerm : ''); 
	   $data['SchoolTerm']       = ((@property_exists($rs,'SchoolTerm'))? $rs->SchoolTerm : ''); 
	   $data['WithinAdv']        = ((@property_exists($rs,'WithinAdvisingPeriod'))? $rs->WithinAdvisingPeriod : 0); 
	   $data['WithinEnr']        = ((@property_exists($rs,'WithinEnrollmentPeriod'))? $rs->WithinEnrollmentPeriod : 0); 
	   $data['AllowWithAccount'] = ((@property_exists($rs,'AllowWithAccount'))? $rs->AllowWithAccount : 0); 
	   $data['AllowWithBalance'] = ((@property_exists($rs,'AllowWithBalance'))? $rs->AllowWithBalance : 0); 
	   $data['IsAlreadyAdvise']  = ((@property_exists($rs,'IsAlreadyAdvise'))? $rs->IsAlreadyAdvise : 0); 
	   $data['CountEnrollment']  = ((@property_exists($rs,'CountEnrollment'))? $rs->CountEnrollment : 0); 
	   $data['RegID']            = ((@property_exists($rs,'RegID'))? $rs->RegID : 0); 
	   $data['CurriculumID']     = ((@property_exists($rs,'CurriculumID'))? $rs->CurriculumID : 0); 
	   $data['YearLevelID']      = ((@property_exists($rs,'YearLevelID'))? $rs->YearLevelID : 0); 
	   $data['MinUnits']         = ((@property_exists($rs,'MinUnits'))? $rs->MinUnits : 0); 
	   $data['MaxUnits']         = ((@property_exists($rs,'MaxUnits'))? $rs->MaxUnits : 0); 
	   
	   if(@array_key_exists('WithinAdv',$data) && @array_key_exists('WithinEnr',$data))
	   {
		 $data['AllowWithAccount'] = (($data['WithinAdv']==1 && $data['WithinEnr']==0)? 1 : $data['AllowWithAccount']);
		 $data['AllowWithBalance'] = (($data['WithinAdv']==1 && $data['WithinEnr']==0)? 1 : $data['AllowWithBalance']);  
	   }	   
	   break;
	  }
	  return $data;
	}	
    else
     return false;		
  }
  
  function exec_InitialConfiguration($stdno='')
  {
	$configdata = $this->get_ConfigTermDetail($stdno);
    if($configdata){
	 $this->db->reconnect();	
	 $termid    = ((array_key_exists('TermID',$configdata))?$configdata['TermID']:0);	
	 $campus    = ((array_key_exists('CampusID',$configdata))?$configdata['CampusID']:1);
	 $ayterm    = ((array_key_exists('AYTerm',$configdata))?$configdata['AYTerm']:'');
	 $sterm     = ((array_key_exists('SchoolTerm',$configdata))?$configdata['SchoolTerm']:'');
	 $withinadv = ((array_key_exists('WithinAdv',$configdata))?$configdata['WithinAdv']:0);
	 $withinenr = ((array_key_exists('WithinEnr',$configdata))?$configdata['WithinEnr']:0);
	 $allowacct = ((array_key_exists('AllowWithAccount',$configdata))?$configdata['AllowWithAccount']:0);
	 $allowbal  = ((array_key_exists('AllowWithBalance',$configdata))?$configdata['AllowWithBalance']:0);
	 $countenr  = ((array_key_exists('CountEnrollment',$configdata))?$configdata['CountEnrollment']:0);
	 $regid     = ((array_key_exists('RegID',$configdata))?$configdata['RegID']:'');
	 $yrlvlid   = ((array_key_exists('YearLevelID',$configdata))?$configdata['YearLevelID']:0);
	 $min       = ((array_key_exists('MinUnits',$configdata))?$configdata['MinUnits']:0);
	 $max       = ((array_key_exists('MaxUnits',$configdata))?$configdata['MaxUnits']:0);
	 $query     = ''; 
	 
	 if($countenr<=0)
	 {
	  $query="SELECT TOP 1 ".$termid." AS Config_TermID ,
                     '".$ayterm."' AS Config_Ayterm ,
                     ".$campus." AS CampusID ,
                     dbo.fn_CampusName(".$campus.") AS Campus ,
				     p.CollegeID ,
				     p.ProgID ,
					 s.MajorDiscID as MajorID,
                     s.StudentNo ,
                     CONCAT(s.LastName,', ',s.FirstName,' ',s.MiddleInitial) AS Fullname ,
                     ".$termid." AS Advise_TermID ,
                     '".$ayterm."' AS Advise_Ayterm ,
                     config.AdvisingPeriodStart AS AdvisingPeriodStart_Undergrad ,
                     config.AdvisingPeriodStart_Graduate AS AdvisingPeriodStart_Grad ,
                     config.AdvisingPeriodEnd AS AdvisingPeriodEnd_Undergrad ,
                     config.AdvisingPeriodEnd_Graduate AS AdvisingPeriodEnd_Grad ,
                     config.StartEnrollment AS StartEnrollment_Undergrad ,
                     config.StartEnrollment_Graduate AS StartEnrollment_Grad ,
                     config.EndEnrollment AS EndEnrollment_Undergrad ,
                     config.EndEnrollment_Graduate AS EndEnrollment_Grad ,
                     ISNULL((SELECT TOP 1 AdvisedID FROM ES_Advising WHERE TermID=".$termid." and StudentNo='".$stdno."'),'') as AdvisedID,
				     '' AS RegistrationID ,
                     NULL AS RegistrationDate ,
                     p.ProgName AS AcademicProgram ,
				     col.CollegeName AS CollegeName ,
				     cur.IndexID as CurriculumID,
                     cur.CurriculumCode ,
				     ".$yrlvlid." AS YearLevelID ,
				     ISNULL(dbo.fn_YearLevel(".$yrlvlid."),0) AS YearLevel,
					 ".(($min==0)? "dbo.fn_sisConfigDefaultMinLoadUnits(p.ProgClass,".$termid.")" : $min)." as MinUnits,
					 ".(($sterm=='Summer')? "dbo.fn_sisConfigDefaultMaxLoadUnits(p.ProgClass,".$termid.")" : "dbo.fn_GetMaxLoadUnits_r2(s.StudentNo,".$termid.", ".$yrlvlid.")")." as MaxUnits,
                     CASE WHEN (SELECT COUNT(TermID) from dbo.fn_getRetentionPreviousSem(".$termid.",s.StudentNo))=0 THEN 1 ELSE (SELECT RetentionID from dbo.fn_getRetentionPreviousSem(".$termid.",s.StudentNo)) END AS RetentionID ,
                     CASE WHEN (SELECT COUNT(TermID) from dbo.fn_getRetentionPreviousSem(".$termid.",s.StudentNo))=0 THEN 'Regular Load' ELSE (SELECT [Status] from dbo.fn_getRetentionPreviousSem(".$termid.",s.StudentNo)) END AS Retention ,
                     dbo.fn_sisAutoTableofFeesID(".$termid.", '".$stdno."', dbo.fn_getAutoYearLevel(s.StudentNo)) AS FeeID,
				     dbo.fn_TemplateCode(dbo.fn_sisAutoTableofFeesID(".$termid.", '".$stdno."', dbo.fn_getAutoYearLevel(s.StudentNo))) AS TemplateCode ,
				     CASE WHEN s.Inactive = 1 THEN 'Unable to process your enrollment because you are inactive. Please visit the registrar''s office.' ELSE 'Not Yet Registered' END AS Status ,
                     p.ProgClass,
					 s.Inactive ,
				     CASE WHEN ab.Cleared = 0 THEN 'PLEASE CHECK YOU ACCOUNTABILITIES' ELSE '' END AS Accountabilities,
				     ".$allowacct." AS AllowWithAcct,
				     dbo.fn_sisForwardedBalance(1,s.StudentNo,dbo.fn_sisConfigNetworkTermID()) AS OutstandingBalance,
                     ".$allowbal." AS AllowWithBal,
				     CASE WHEN s.NonZeroBase = 0 THEN 'Zero Based' ELSE 'Non-Zero Based' END AS Mode,
				     CASE WHEN p.ProgClass = 50 THEN config.RegisterWithoutAssessment ELSE RegisterWithoutAssessment_Graduate END AS RegWoutAssess
				    ,".$withinadv." AS WithinAdvisingPeriod
				    ,".$withinenr." AS WithinEnrollmentPeriod
				    ,dbo.fn_sisConfigAllowToEnrollSubjectsNotInCurriculum(p.ProgClass,".$termid.") AS snic
				    ,case when s.Inactive=1 or p.ProgClass <> 50 then 0 else sstat.IsRegular end AS IsRegular
				    ,ISNULL((SELECT COUNT(*) FROM ES_Journals WHERE IDNo=s.StudentNo AND ReferenceNo=(SELECT TOP 1 CONVERT(VARCHAR(50),RegID) FROM ES_Registrations where StudentNo=s.StudentNo and TermID=".$termid.")),0) as IsAssessed
					,0 as Registered
		         FROM ES_Students AS s		
           LEFT OUTER JOIN ES_AYTermConfig AS config ON config.TermID=".$termid." AND config.CampusID=".$campus."
           LEFT OUTER JOIN ES_Programs AS p ON s.ProgID = p.ProgID
           LEFT OUTER JOIN ES_Curriculums AS cur ON s.CurriculumID = cur.IndexID
	       LEFT OUTER JOIN ES_StudentAccountabilities AS ab ON s.StudentNo = ab.StudentNo AND ab.TermID = ".$termid."
	       LEFT OUTER JOIN ES_RegisterWithBalance AS rb ON s.StudentNo = rb.StudentNo AND rb.TermID = ".$termid."
	       LEFT OUTER JOIN ES_Colleges AS col ON p.CollegeID = col.CollegeID
	       LEFT OUTER JOIN dbo.sis_StudentCurrentStatus AS sstat ON s.StudentNo = sstat.StudentNo
                WHERE s.StudentNo = '".$stdno."'";	 
	 }else{
	  $query="SELECT TOP 1 ".$termid." AS Config_TermID ,
                     '".$ayterm."' AS Config_Ayterm ,
                     ".$campus." AS CampusID ,
                     dbo.fn_CampusName(r.CampusID) AS Campus ,
					 r.CollegeID ,
					 r.ProgID ,
                     s.StudentNo ,
                     s.Fullname ,
                     r.TermID AS Advise_TermID ,
                     dbo.fn_AcademicYearTerm(r.TermID) AS Advise_Ayterm ,
                     config.AdvisingPeriodStart AS AdvisingPeriodStart_Undergrad ,
                     config.AdvisingPeriodStart_Graduate AS AdvisingPeriodStart_Grad ,
                     config.AdvisingPeriodEnd AS AdvisingPeriodEnd_Undergrad ,
                     config.AdvisingPeriodEnd_Graduate AS AdvisingPeriodEnd_Grad ,
                     config.StartEnrollment AS StartEnrollment_Undergrad ,
                     config.StartEnrollment_Graduate AS StartEnrollment_Grad ,
                     config.EndEnrollment AS EndEnrollment_Undergrad ,
                     config.EndEnrollment_Graduate AS EndEnrollment_Grad ,
                     ISNULL((SELECT TOP 1 AdvisedID FROM ES_Advising WHERE TermID=r.TermID and StudentNo=s.StudentNo),'') as AdvisedID,
					 r.RegID AS RegistrationID ,
                     r.RegDate AS RegistrationDate ,
                     p.ProgName AS AcademicProgram ,
					 col.CollegeName AS CollegeName ,
				     cur.IndexID as CurriculumID,
                     cur.CurriculumCode ,
					 r.YearLevelID ,
                     y.YearLevel ,
					 ".(($min==0)? "dbo.fn_sisConfigDefaultMinLoadUnits(p.ProgClass,".$termid.")" : $min)." as MinUnits,
					 ".(($max==0)? (($sterm=='Summer')? "dbo.fn_sisConfigDefaultMaxLoadUnits(p.ProgClass,".$termid.")" : "r.MaxUnitsAllowed") : $max)." as MaxUnits,
                     CASE WHEN (SELECT COUNT(TermID) from dbo.fn_getRetentionPreviousSem(".$termid.",s.StudentNo))=0 THEN 1 ELSE (SELECT RetentionID from dbo.fn_getRetentionPreviousSem(".$termid.",s.StudentNo)) END AS RetentionID ,
                     CASE WHEN (SELECT COUNT(TermID) from dbo.fn_getRetentionPreviousSem(".$termid.",s.StudentNo))=0 THEN 'Regular Load' ELSE (SELECT [Status] from dbo.fn_getRetentionPreviousSem(".$termid.",s.StudentNo)) END AS Retention ,
					 CASE WHEN r.TableofFeeID=0 OR r.TableofFeeID IS NULL THEN dbo.fn_sisAutoTableofFeesID(".$termid.", '".$stdno."', r.YearLevelID) ELSE r.TableofFeeID END AS FeeID,
				      CASE WHEN r.TableofFeeID=0 OR r.TableofFeeID IS NULL THEN dbo.fn_TemplateCode(dbo.fn_sisAutoTableofFeesID(".$termid.", '".$stdno."', r.YearLevelID)) ELSE dbo.fn_TemplateCode(r.TableofFeeID) END AS TemplateCode ,
                     CASE WHEN r.ValidationDate IS NOT NULL THEN 'Validated' ELSE CASE WHEN dbo.fn_Registration_IsInProcess(1,r.RegID,1,r.StudentNo,r.TermID)=1 THEN 'PAYMENT IN-PROCESS' ELSE 'Registered' END END AS [Status] ,
                     p.ProgClass,
					 s.Inactive,
					 CASE WHEN ab.Cleared = 0 THEN 'PLEASE CHECK YOUR ACCOUNTABILITIES.' ELSE '' END AS Accountabilities,
					 ".$allowacct." AS AllowWithAcct,
				     dbo.fn_sisForwardedBalance(1,s.StudentNo,dbo.fn_sisConfigNetworkTermID()) AS OutstandingBalance,
					 ".$allowbal." AS AllowWithBal,
					 CASE WHEN s.NonZeroBase = 0 THEN 'Zero Based' ELSE 'Non-Zero Based' END AS Mode,
					 CASE WHEN p.ProgClass = 50 THEN config.RegisterWithoutAssessment ELSE RegisterWithoutAssessment_Graduate END AS RegWoutAssess
					,".$withinadv." AS WithinAdvisingPeriod
				    ,".$withinenr." AS WithinEnrollmentPeriod
				    ,dbo.fn_sisConfigAllowToEnrollSubjectsNotInCurriculum(p.ProgClass,".$termid.") AS snic
					,case when s.Inactive=1 or p.ProgClass <> 50 then 0 else sstat.IsRegular end AS IsRegular
					,ISNULL((SELECT COUNT(*) FROM ES_Journals WHERE IDNo=r.StudentNo AND ReferenceNo=CONVERT(VARCHAR(50),(SELECT TOP 1 RegID FROM ES_Registrations where StudentNo=r.StudentNo and TermID=".$termid."))),0) as IsAssessed
					,r.Registered
			   FROM ES_Registrations AS r
         LEFT OUTER JOIN ES_Students AS s ON r.StudentNo = s.StudentNo
	     LEFT OUTER JOIN ES_Programs AS p ON r.ProgID = p.ProgID
	     LEFT OUTER JOIN ES_TableofFees AS t ON r.TableofFeeID = t.TemplateID
	     LEFT OUTER JOIN ES_AYTermConfig AS config ON r.TermID = config.TermID AND r.CampusID = config.CampusID
	     LEFT OUTER JOIN ES_Curriculums AS cur ON s.CurriculumID = cur.IndexID
         LEFT OUTER JOIN ES_YearLevel AS y ON r.YearLevelID = y.YearLevelID
	     LEFT OUTER JOIN ES_StudentAccountabilities AS ab ON r.StudentNo = ab.StudentNo AND ab.TermID = r.TermID
	     LEFT OUTER JOIN ES_RegisterWithBalance AS rb ON s.StudentNo = rb.StudentNo AND rb.TermID = ".$termid."
	     LEFT OUTER JOIN ES_Colleges AS col ON r.CollegeID = col.CollegeID
	     LEFT OUTER JOIN dbo.sis_StudentCurrentStatus AS sstat ON r.StudentNo = sstat.StudentNo
              WHERE r.StudentNo = '".$stdno."' AND r.TermID = ".$termid;	 
	 }	 
	 
	 $result = (($query)? $this->db->query($query) : false);
	 return (($result)? $result->result() : false);
	}
    else
     return false;		
  }
  
  function get_ElectiveOfferedInBlock($term,$campus,$curriculum,$yrlvl)
  {
	$this->db->reconnect();
	$query = "SELECT cs.SubjectID
	                ,su.SubjectCode AS SubjectCode 
					,su.SubjectTitle AS SubjectTitle 
					,su.AcadUnits AS AcadUnits
					,su.LabUnits AS LabUnits 
					,su.CreditUnits AS CreditUnits
					,su.IsNonAcademic AS IsNonAcademic
                FROM ES_ClassSchedules AS cs
     LEFT OUTER JOIN ES_Subjects AS su ON cs.SubjectID = su.SubjectID
               WHERE cs.SectionID IN (SELECT ISNULL((SELECT TOP 1 SectionID FROM ES_ClassSections AS c WHERE c.TermID = '".$term."' AND c.CampusID = '".$campus."' AND c.CurriculumID = '".$curriculum."' AND c.YearLevelID = y.YearLevelID AND c.IsDissolved = 0),0) as SectionID FROM ES_YearLevel as y WHERE YearLevelID<=".$yrlvl.")                 
                 AND cs.SubjectID IN (SELECT cd.SubjectID FROM ES_CurriculumDetails AS cd WHERE cd.CurriculumID = '".$curriculum."' AND cd.YearTermID = 22)
				 AND cs.IsDissolved = 0               
            ORDER BY cs.Cntr";
	$exec = (($query)? $this->db->query($query) : false);
    if($exec)
    {
	 $count=0;
     foreach($exec->result() as $rs)
     {
	   $rs_data = array();
       $rs_data['SubjectID']     = ((@property_exists($rs,'SubjectID'))?($rs->SubjectID):0);
       $rs_data['SubjectCode']   = ((@property_exists($rs,'SubjectCode'))?($rs->SubjectCode):'');
       $rs_data['SubjectTitle']  = ((@property_exists($rs,'SubjectTitle'))?($rs->SubjectTitle):'');
       $rs_data['AcadUnits']     = ((@property_exists($rs,'AcadUnits'))?($rs->AcadUnits):0);
       $rs_data['LabUnits']      = ((@property_exists($rs,'LabUnits'))?($rs->LabUnits):0);
       $rs_data['CreditUnits']   = ((@property_exists($rs,'CreditUnits'))?($rs->CreditUnits):0);
       $rs_data['IsNonAcademic'] = ((@property_exists($rs,'IsNonAcademic'))?($rs->IsNonAcademic):0);
	   if($rs_data['SubjectID']>0)
	   {	   
	    $this->subj_elecoffered[$count] = $rs_data;
	    $count++;
	   }	
	 }
	 
	 return true;	 
	}
    else
     return false;		
  }
  
  function get_AdvisableSubjects($stdno='')
  {
	$this->db->reconnect();
	$query = "SELECT * FROM (
                       SELECT  DISTINCT '".$this->ayterm."' AS Ayterm ,
                      '".$this->termid."' AS TermID ,
                      '".$this->campusid."' AS CampusID ,
                      p.CollegeID ,
                      s.ProgID ,
                      s.StudentNo ,
                      '".$this->yrlvlid."' AS YearLevelID ,
                      ".(($this->minload==0)?"dbo.fn_sisConfigDefaultMinLoadUnits(p.ProgClass, ".$this->termid.")" : $this->minload)." AS MinLoad,
                      ".(($this->maxload==0)?(($this->schoolterm=='Summer')?"dbo.fn_sisConfigDefaultMaxLoadUnits(p.ProgClass, ".$this->termid.")":"dbo.fn_GetMaxLoadUnits_r2(s.StudentNo, ".$this->termid.", ".$this->yrlvlid.")") : $this->maxload)." AS MaxLoad,
                      0 AS AdvisedSubjects ,
                      0 AS AdvisedUnits ,
                      s.StudentNo AS AdviserID ,
                      GETDATE() AS DateAdvised ,
                      '' AS ModifiedBy ,
                      NULL AS DateModified ,
                      '' AS AccessCode ,
                      dbo.fn_sisAutoTableofFeesID(".$this->termid.", s.StudentNo,".$this->yrlvlid.") AS FeesID ,
                      '' AS Accountabilities ,
                      dbo.fn_StudentForwardedBalance(1, s.StudentNo, ".$this->termid.") AS OutBalance ,
                      0 AS RegID ,
                      e.CourseID AS SubjectID ,
                      e.CourseCode AS SubjectCode ,
                      e.CourseTitle AS SubjectTitle ,
                      e.LectUnits AS AcadUnits ,
                      e.LabUnits AS LabUnits ,
                      e.CreditUnits AS CreditUnits ,
                      0 AS NumberAdvised ,
                      dbo.fn_TemplateCode(dbo.fn_sisAutoTableofFeesID(".$this->termid.", s.StudentNo, ".$this->yrlvlid.")) AS FeesTemplate ,
                      s.NonZeroBase ,
                      e.IsNonAcademic AS IsNonAcademic ,
                      CASE WHEN e.ElectiveSubjects = 1 THEN e.CourseID ELSE 0 END AS ElectiveSubjectID ,
                      e.ElectiveSubjects,
					  ISNULL(Onesubject.FKey,0) AS OneSubjKey,
				      e.YearStanding AS SubjectYearStanding,
                      e.SortOrder 
                 FROM ES_Students AS s
      LEFT OUTER JOIN ES_Programs AS p ON p.ProgID = s.ProgID
      LEFT OUTER JOIN sis_AcademicEvaluation AS e ON e.StudentNo = s.StudentNo
	  LEFT OUTER JOIN ( SELECT one.TermID ,
							   one.CampusID ,
							   d.FKey,
							   d.SubjectID
						  FROM ES_OneSubjecttoEnroll AS one
			   LEFT OUTER JOIN ES_OneSubjecttoEnroll_Details AS d ON one.IndexID = d.FKey) AS Onesubject ON Onesubject.SubjectID = e.CourseID AND Onesubject.TermID = '".$this->termid."' AND Onesubject.CampusID = '".$this->campusid."'
	       INNER JOIN ES_ClassSchedules as cs ON e.CourseID = cs.SubjectID and cs.TermID=".$this->termid."		
           INNER JOIN ES_ClassSections as sec ON cs.SectionID = sec.SectionID and s.CurriculumID=sec.CurriculumID		   
	            WHERE s.StudentNo = '".$stdno."'
                  AND e.OfferedSubject > 0
                  AND e.Remarks NOT IN ('Passed', 'Credited', 'Not Credited', '* UNPOSTED',  '* Enrolled')
                  AND e.PreRequisitePassed = 1
				  AND e.Semester <> 'Elective Courses'
				  AND e.YearStanding<=".$this->yrlvlid."
	 UNION ALL
	          SELECT  DISTINCT '".$this->ayterm."' AS Ayterm ,
                      '".$this->termid."' AS TermID ,
                      '".$this->campusid."' AS CampusID ,
                      p.CollegeID ,
                      s.ProgID ,
                      s.StudentNo ,
                      '".$this->yrlvlid."' AS YearLevelID ,
                      ".(($this->minload==0)?"dbo.fn_sisConfigDefaultMinLoadUnits(p.ProgClass, ".$this->termid.")" : $this->minload)." AS MinLoad,
                      ".(($this->maxload==0)?(($this->schoolterm=='Summer')?"dbo.fn_sisConfigDefaultMaxLoadUnits(p.ProgClass, ".$this->termid.")":"dbo.fn_GetMaxLoadUnits_r2(s.StudentNo, ".$this->termid.", ".$this->yrlvlid.")") : $this->maxload)." AS MaxLoad,
                      0 AS AdvisedSubjects ,
                      0 AS AdvisedUnits ,
                      s.StudentNo AS AdviserID ,
                      GETDATE() AS DateAdvised ,
                      '' AS ModifiedBy ,
                      NULL AS DateModified ,
                      '' AS AccessCode ,
                      dbo.fn_sisAutoTableofFeesID(".$this->termid.", s.StudentNo,".$this->yrlvlid.") AS FeesID ,
                      '' AS Accountabilities ,
                      dbo.fn_StudentForwardedBalance(1, s.StudentNo, ".$this->termid.") AS OutBalance ,
                      0 AS RegID ,
                      e.CourseID AS SubjectID ,
                      e.CourseCode AS SubjectCode ,
                      e.CourseTitle AS SubjectTitle ,
                      e.LectUnits AS AcadUnits ,
                      e.LabUnits AS LabUnits ,
                      e.CreditUnits AS CreditUnits ,
                      0 AS NumberAdvised ,
                      dbo.fn_TemplateCode(dbo.fn_sisAutoTableofFeesID(".$this->termid.", s.StudentNo, ".$this->yrlvlid.")) AS FeesTemplate ,
                      s.NonZeroBase ,
                      e.IsNonAcademic AS IsNonAcademic ,
                      CASE WHEN e.ElectiveSubjects = 1 THEN e.CourseID ELSE 0 END AS ElectiveSubjectID ,
                      1 as ElectiveSubjects,
					  ISNULL(Onesubject.FKey,0) AS OneSubjKey,
				      e.YearStanding AS SubjectYearStanding,
                      e.SortOrder 
                 FROM ES_Students AS s
      INNER JOIN ES_Programs AS p ON p.ProgID = s.ProgID
      INNER JOIN sis_AcademicEvaluation AS e ON e.StudentNo = s.StudentNo
	  INNER JOIN sis_AcademicEvaluation as elec ON elec.StudentNo=s.StudentNo AND elec.ElectiveSubjects=1 AND elec.Semester <> 'Elective Courses' AND elec.YearStanding=".$this->yrlvlid."
	  LEFT OUTER JOIN ( SELECT one.TermID ,
							   one.CampusID ,
							   d.FKey,
							   d.SubjectID
						  FROM ES_OneSubjecttoEnroll AS one
			   LEFT OUTER JOIN ES_OneSubjecttoEnroll_Details AS d ON one.IndexID = d.FKey) AS Onesubject ON Onesubject.SubjectID = e.CourseID AND Onesubject.TermID = '".$this->termid."' AND Onesubject.CampusID = '".$this->campusid."'
	       INNER JOIN ES_ClassSchedules as cs ON e.CourseID = cs.SubjectID and cs.TermID=".$this->termid."		
           INNER JOIN ES_ClassSections as sec ON cs.SectionID = sec.SectionID and s.CurriculumID=sec.CurriculumID		   
	            WHERE s.StudentNo = '".$stdno."'
                  AND e.Semester = 'Elective Courses'
				  AND e.OfferedSubject > 0
                  AND e.Remarks NOT IN ('Passed', 'Credited', 'Not Credited', '* UNPOSTED',  '* Enrolled')
                  AND e.PreRequisitePassed = 1) as subj Order By SortOrder";  
   //die($query);
   $result = (($query)? $this->db->query($query) : false);	
   return $result;   
  }
  
  function get_AdvisedSubjects($stdno='',$opt=0)
  {
	$query = "SELECT  '".$this->ayterm."' AS Ayterm ,
                      ad.TermID ,
                      ad.CampusID ,
                      ad.CollegeID ,
                      ad.ProgID ,
                      ad.StudentNo ,
                      ad.YearLevel AS YearLevelID ,
                      ad.MinLoad ,
                      ad.MaxLoad ,
                      ad.AdvisedSubject ,
                      ad.AdvisedUnits ,
                      ad.AdviserID ,
                      ad.DateAdvised ,
                      ad.ModifiedBy ,
                      ad.DateModified ,
                      ad.AccessCode ,
                      ad.FeesID ,
                      ad.Accountabilities ,
                      ad.OutBalance ,
                      ad.RegID ,
                      s.SubjectID ,
                      s.SubjectCode ,
                      s.SubjectTitle ,
                      s.AcadUnits ,
                      s.LabUnits ,
                      ".$this->isadvised." AS NumberAdvised ,
                      dbo.fn_TemplateCode(ad.FeesID) AS FeesTemplate ,
                      st.NonZeroBase ,
                      s.IsNonAcademic ,
                      d.ElectiveSubjectID ,
                      CASE WHEN d.ElectiveSubjectID > 0 THEN 1 ELSE 0 END AS ElectiveSubjects
                 FROM ES_Advising AS ad
      LEFT OUTER JOIN ES_Advising_Details AS d ON d.AdvisedID = ad.AdvisedID
      LEFT OUTER JOIN ES_Subjects AS s ON s.SubjectID = d.SubjectID
      LEFT OUTER JOIN ES_Students AS st ON st.StudentNo = ad.StudentNo
                WHERE ad.StudentNo = '".$stdno."' 
				  AND ad.TermID = '".$this->termid."'
                  ".(($opt==1)?"AND ad.RegID IS NOT NULL AND ad.RegID = (SELECT r.RegID FROM ES_Registrations AS r WHERE r.RegID = ad.RegID)":"")."
				ORDER BY d.EntryID";  
   $result = (($query)? $this->db->query($query) : false);	
   return (($result)?$result->result():false);  
  }
  
  function exec_AutoAdvise($stdno='')
  {
	$configdata = $this->get_ConfigTermDetail($stdno);
    if($configdata)
    {
	 $this->db->reconnect();	
	 $this->termid       = ((array_key_exists('TermID',$configdata))?$configdata['TermID']:0);	
	 $this->campusid     = ((array_key_exists('CampusID',$configdata))?$configdata['CampusID']:1);
	 $this->ayterm       = ((array_key_exists('AYTerm',$configdata))?$configdata['AYTerm']:'');
	 $this->schoolterm   = ((array_key_exists('SchoolTerm',$configdata))?$configdata['SchoolTerm']:'');
	 $withinadv          = ((array_key_exists('WithinAdv',$configdata))?$configdata['WithinAdv']:0);
	 $withinenr          = ((array_key_exists('WithinEnr',$configdata))?$configdata['WithinEnr']:0);
	 $allowacct          = ((array_key_exists('AllowWithAccount',$configdata))?$configdata['AllowWithAccount']:0);
	 $allowbal           = ((array_key_exists('AllowWithBalance',$configdata))?$configdata['AllowWithBalance']:0);
	 $this->isadvised    = ((array_key_exists('IsAlreadyAdvise',$configdata))?$configdata['IsAlreadyAdvise']:0);
	 $this->countenr     = ((array_key_exists('CountEnrollment',$configdata))?$configdata['CountEnrollment']:0);
	 $this->regid        = ((array_key_exists('RegID',$configdata))?$configdata['RegID']:'');
	 $this->curriculumid = ((array_key_exists('CurriculumID',$configdata))?$configdata['CurriculumID']:0);
	 $this->yrlvlid      = ((array_key_exists('YearLevelID',$configdata))?$configdata['YearLevelID']:0);
	 $this->minload      = ((array_key_exists('MinUnits',$configdata))?$configdata['MinUnits']:0);
	 $this->maxload      = ((array_key_exists('MaxUnits',$configdata))?$configdata['MaxUnits']:0);
	 $query              = ''; 
	 
	 if($this->isadvised==0)
	 {	 
	  $elecoffered = $this->get_ElectiveOfferedInBlock($this->termid,$this->campusid,$this->curriculumid,$this->yrlvlid);
	  $execdata    = $this->get_AdvisableSubjects($stdno);
	  if($execdata)
	  {
	   $indx      = 0;
	   $aunit     = 0;
	   $prev_subj = '';
	   $result = $execdata->result();
	   $this->minload = ((@array_key_exists(0,$result) && @property_exists($result[0],'MinLoad'))?($result[0]->MinLoad) : $this->minload);
	   $this->maxload = ((@array_key_exists(0,$result) && @property_exists($result[0],'MaxLoad'))?($result[0]->MaxLoad) : $this->maxload);
	 
	   foreach($result as $key => $rs)
       {
		 $isElective = ((@property_exists($rs,'ElectiveSubjects'))?($rs->ElectiveSubjects):0);
		 
		 if($isElective==1)
	     {
		  $elec_subj  = (array_key_exists($indx,$this->subj_elecoffered)?($this->subj_elecoffered[$indx]):false);
		  if($elec_subj==false){
			unset($result[$key]);
			continue;
		  }else{	  
		    $subjectid  = ((@property_exists($rs,'SubjectID'))?($rs->SubjectID):0);
		    $result[$key]->SubjectID         = $elec_subj['SubjectID'];
		    $result[$key]->SubjectCode       = $elec_subj['SubjectCode'];
		    $result[$key]->SubjectTitle      = $elec_subj['SubjectTitle'];
		    $result[$key]->AcadUnits         = $elec_subj['AcadUnits'];
		    $result[$key]->LabUnits          = $elec_subj['LabUnits'];
		    $result[$key]->CreditUnits       = $elec_subj['CreditUnits'];
		    $result[$key]->IsNonAcademic     = $elec_subj['IsNonAcademic'];
		    $result[$key]->ElectiveSubjectID = $subjectid;
		    $indx++;
		  }
		 }
	   }
	   
	   
	   foreach($result as $key=>$rs)
	   {
	     if($prev_subj==$rs->SubjectCode)
		  continue;
         else
          $prev_subj=$rs->SubjectCode;			 
	  
		 $aunit = $aunit + ($rs->AcadUnits+$rs->LabUnits);   
	     //if($this->maxload < $aunit){unset($result[$key]);}
	   }
	   
	   return $result;
	  }
      return false;	  
     }
	 else if($this->isadvised==1)
	  return $this->get_AdvisedSubjects($stdno,0);
     else
	  return $this->get_AdvisedSubjects($stdno,1);	 
	}
    else
     return false;		
  }
}
?>