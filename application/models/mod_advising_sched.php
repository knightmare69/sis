<?php
Class mod_advising_sched extends CI_Model {

function intClassSection($studno=''){
  //$query = "EXEC sp_sisGetSections @StudentNo='".$studno."';";  
  //$result = $this->db->query($query);  
  $result = $this->exec_sp_sisGetSections($studno);
  if($result)
   return $result->result();		
  else
   return false;
}

function optClassSection($studno='')
{
 $output='';
 $result = $this->intClassSection($studno);
 if($result)
 {
  $prev_sec = '';
  foreach($result as $rs)
  {
   if($prev_sec == $rs->SectionName)
	continue;
   else
	 $prev_sec = $rs->SectionName;
 
   $output.='<option data-value="'.$rs->SectionName.'" value="'.$rs->SectionID.'">'.$rs->SectionName.'</option>';
  }
 }
 return $output;
}
 
function exec_sp_sisGetSections($studno='')
{
 $this->db->close();	
 $this->db->reconnect();
 $query="SELECT SectionID ,
                SectionName
		  FROM (SELECT c.SectionID AS SectionID,
					   c.SectionName AS SectionName,
					   (CASE WHEN (SELECT COUNT(ScheduleID) FROM ES_RegistrationDetails 
									WHERE ScheduleID=cs.ScheduleID AND ISNULL(RegTagID,0) < 3) >= cs.Limit THEN 1 ELSE 0 END) AS IsFull
				  FROM ES_Students as s
				  INNER JOIN ES_Advising AS a ON s.StudentNo = a.StudentNo AND a.CampusID = a.CampusID
				  INNER JOIN ES_Advising_Details AS ad ON ad.AdvisedID=a.AdvisedID 
				  INNER JOIN ES_ClassSchedules AS cs ON cs.SubjectID = ad.SubjectID AND cs.TermID = dbo.fn_sisConfigNetworkTermID()
				  INNER JOIN ES_ClassSections AS c ON cs.SectionID=c.SectionID AND c.CurriculumID=s.CurriculumID AND c.ProgramID=s.ProgID
				 WHERE a.TermID=dbo.fn_sisConfigNetworkTermID() 
					   AND a.StudentNo = '".$studno."'
					   AND c.IsDissolved = 0
					   AND cs.IsDissolved = 0
			GROUP BY c.SectionID , cs.ScheduleID, cs.Limit , c.SectionName) as t
			GROUP BY SectionID, SectionName
			HAVING  CASE WHEN SUM(IsFull) > 0 THEN 1 ELSE 0 END = 0";	
$result = $this->db->query($query);
return $result;	   
}
 
function initofferedschedules($studno, $campus, $term, $currid, $subjectid){
  $query = "EXEC sp_OfferedClassSchedules '".$studno."','".$campus."','".$term."','".$currid."','".$subjectid."'";  
  $result = $this->db->query($query);  
  return $result->result();		
}

function save_schedule($advisedid , $subjectid, $scheduleid){
  
  $success=false;
  $scheduleid=trim($scheduleid);
  
  $data = array('ScheduleID' => $scheduleid );
  $this->db->where('AdvisedID', $advisedid);
  $this->db->where('SubjectID', $subjectid);
  $success = $this->db->update('ES_Advising_Details', $data);
	  
  return $success;	  
}

function rem_schedule($advisedid , $subjectid){
  $success=false;
  $data = array('ScheduleID' => 0 );

  $this->db->where('AdvisedID', $advisedid);
  $this->db->where('SubjectID', $subjectid);
  $success = $this->db->update('ES_Advising_Details', $data);
	  
  return $success;	  
}

function compareschedule($csid1, $csid2){
   $query = "EXEC sp_CheckSchedConflicts '".$csid1."', '".$csid2."'";
   $result = $this->db->query($query);
	return $result->result();
}

function register($studno='', $tid=0, $cid=0){
  if(trim($tid)==''){$tid=0;}
  if(trim($cid)==''){$cid=0;}
  
  $query = "EXEC sp_SaveEnrollment '".$studno."',".$tid.",".$cid.",0,'',0,0";
  $result = $this->db->query($query);
  $check  = $this->checkfeeid($tid,$studno);
  log_message('error',$query);
  return $result->row();	
}

function checkfeeid($termid,$studno){
	$yrlvl = $this->studno_yearlevel($termid,$studno);
	$query = "UPDATE ES_Advising SET FeesID=dbo.fn_sisAutoTableofFeesID(TermID,StudentNo,".(($yrlvl)?"'".$yrlvl."'":"1").")".(($yrlvl)?",YearLevelID='".$yrlvl."'":"")." WHERE TermID='".$termid."' AND StudentNo='".$studno."' AND FeesID=0
              UPDATE ES_Registrations SET TableofFeeID=dbo.fn_sisAutoTableofFeesID(TermID,StudentNo,".(($yrlvl)?"'".$yrlvl."'":"1").") WHERE TermID='".$termid."' AND StudentNo='".$studno."' AND TableofFeeID=0"; 
	$result = $this->db->query($query);
    return true;	
}

function studno_yearlevel($termid,$studno){
	$query = "SELECT a.AcademicYear
				    ,a.SchoolTerm
				    ,r.YearLevelID
				    ,ISNULL((SELECT TOP 1 YearLevelID FROM ES_Registrations WHERE TermID<>r.TermID AND StudentNo=r.StudentNo ORDER BY RegID DESC),1) as PrevLevelID 
			    FROM ES_Registrations as r 
		  INNER JOIN ES_AYTerm as a ON r.TermID=a.TermID 
			   WHERE r.TermID='".$termid."' AND r.StudentNo='".$studno."'";
	$exec  = $this->db->query($query);
    if($exec && $exec->result()){
		$rs     = $exec->result()[0];
	    $yrlvl  = ((strtolower($rs->SchoolTerm)=='2nd Semester' || strtolower($rs->SchoolTerm)=='Summer')?PrevLevelID:YearLevel);
        return $rs->{$yrlvl};		
	}else
	  return false;		
}

function registersubjects($adviseid='', $regid=0)
{	
 $qry = "EXEC sp_registersubject '".$adviseid."','". $regid ."';" ; 
 $result = $this->db->query($qry);
 return $result->row();	  
}

function processfees($regid=0){
 log_message('error','processing:'.$regid);
 $qry = "EXEC sp_comRegAssessment '". $regid ."';" ; 
 $this->db->close();
 $this->db->reconnect();
 $result = $this->db->query($qry);
 return $result ;
}

function getlatestreginfo($param=Null) {
 $result = false;
 if($param && $param!=''){ 
	 $query = "EXEC sp_AdvisedInfo '".$param."'"; 		 
	 $result = $this->db->query($query);
	 return $result->row();	
 }
 return $result;
}

function getadvisedsubj($param=Null){
 $result = false;
 if($param && $param!=''){
   $query = "EXEC [sp_AdvisedCourses] '".$param."'"; 
   $result = $this->db->query($query);
   return $result->result();
 }
 return $result;
}

function getenrolledsubj($param=Null){
 $result = false;
 if($param && $param!='')
 {
   $query = "EXEC sp_RegistrationDetails '".$param."'"; 
   $result = $this->db->query($query);
   return $result->result();
 }
 return $result;
}

function getassessedfees($param=NULL){
 $result = false;
 if($param && $param!='')
 {
   $query = "EXEC dbo.ES_rptRegAssessment_Accounts @RegID = '".$param."', @BackAccount=0"; 
   $result = $this->db->query($query);
   return $result->result();
 }
 return $result;
}

function generate_table($row='',$check=0,$page=0) {
  $tableid='';
  $totalsubj=0;
  $totallec=0;
  $totallab=0;
  
  if($page==0){$tableid='advisedsubj';}
  
  $xdisplay="<table id='".$tableid."' class='table table-bordered' Style='Cursor:Pointer;' oncontextmenu='return false'>
            <thead>
				<tr class='success'><th>#</th>
			   <th>CODE</th>
				<th>DESCRIPTION</th>
				<th>LEC</th>
				<th>LAB</th>
				<th>SECTION</th>				
				<th>SCHEDULE</th>				
				</tr>
			</thead>
			</table>";
  
		$tblcontent = "<tbody><tr><td>x</td><td colspan=5><center><strong>No Subject is Advised.</strong></center></td></tr></tbody>";
 if($row && $row!='')
 {
  $tblcontent  = "<tbody>";
  $tblfoot ='';
  $xcount = 1;
  
  foreach($row as $rs)
  {
   $xcsid = property_exists($rs,'ScheduleID')? $rs->ScheduleID : '';
   $xsubjid = property_exists($rs,'SubjectID')? $rs->SubjectID : '';
   $xseq = property_exists($rs,'SeqNo')? $rs->SeqNo : $xcount;
   $xcode = property_exists($rs,'SubjectCode')? $rs->SubjectCode : '';
   $xtitle = property_exists($rs,'SubjectTitle')? $rs->SubjectTitle : '';
   $xsectionid = property_exists($rs,'SectionID')? $rs->SectionID : 0;
   $xsection = property_exists($rs,'SectionName')? $rs->SectionName : '';
   $xlec = property_exists($rs,'AcadUnits')? $rs->AcadUnits : 0;
   $xlab = property_exists($rs,'LabUnits')? $rs->LabUnits : 0;
   $xsched = property_exists($rs,'Sched_1')? $rs->Sched_1 : '';
   $room = property_exists($rs,'Room1')? $rs->Room1 : '';
   $xsched2 = property_exists($rs,'Sched_2')? $rs->Sched_2 : '';
   $room2 = property_exists($rs,'Room2')? $rs->Room2 : '';
   $xsched3 = property_exists($rs,'Sched_3')? $rs->Sched_3 : '';
   $room3 = property_exists($rs,'Room3')? $rs->Room3 : '';
   $xsched4 = property_exists($rs,'Sched_4')? $rs->Sched_4 : '';
   $room4 = property_exists($rs,'Room4')? $rs->Room4 : '';
   $xsched5 = property_exists($rs,'Sched_5')? $rs->Sched_5 : '';
   $room5 = property_exists($rs,'Room5')? $rs->Room5 : '';
   $xprerequisite = property_exists($rs,'PreRequisitePassed')? $rs->PreRequisitePassed : 1;
   	
   $xsched = ($xsched <>""? $xsched . " / " .  $room : "") . ($xsched2 <> ""? "<br>" . $xsched2 . " / " . $room2 :"" );
   //$faculty = property_exists($rs,'FacultyName')? $rs->FacultyName : '';
   
   $tblcontent  .= "<tr id='s".$xsubjid."' class='".(($xprerequisite==0 && $check==1 && $page==0)?'danger':'')."' data-units='".($xlec+$xlab)."' data-prerequisite='".$xprerequisite."' onclick='trselected(this)' ondblclick='popschedule()' style='font-size: 10px;'>
                     <td>".$xcount.".</td>
			         <td id='". $xcsid ."'><i class='fa fa-2x fa-times txt-color-red pull-left btnsubjdelete hidden'></i>".$xcode."</td>
				     <td>".$xtitle."</td>
				     <td style='font-weight: bold;'>".$xlec."</td>
				     <td style='font-weight: bold;'>".$xlab."</td>
				     <td id='". $xsectionid ."'>".$xsection."</td>
				     <td>".$xsched."</td>				       
					</tr>";
	
	$totalsubj=$totalsubj+1;;
	$totallec=$totallec+$xlec;
    $totallab=$totallab+$xlab;
	
	$xcount++;
  }
  if($xcount>0)
  {
   $tblfoot = "<tfoot><tr style='background-color:#d6d6d6; font-size: x-small; font-weight: bold;'>
                       <td colspan=2>Total</td>
				       <td id='totalsbj'>".$totalsubj."</td>
				       <td id='totallec'>".$totallec."</td>
				       <td id='totallab'>".$totallab."</td>
				       <td></td>
				       <td></td>				       
					</tr></tfoot>";
  }
  $tblcontent .= "</tbody>".$tblfoot;
 }
 $xdisplay = str_replace("</table>",$tblcontent."</table>",$xdisplay);
 return $xdisplay;
}

function generate_feetable($row){
 $xdisplay="<table class='table table-bordered' style='white-space:nowrap;'>
            <thead>
				<tr><th>ACCOUNTS</th>
			   <th></th>
				<th>FULL PAYMENT</th>
				<th>1st Payment</th>
				<th>2nd Payment</th>
				<th>3rd Payment</th></tr>
			</thead>
			</table>";
 
 $tblcontent = "<tbody><tr><td colspan=6><center><strong>No Assessed Fees.</strong></center></td></tr></tbody>";
 if($row && $row!='')
 {
  $tblcontent  = "<tbody>";
  $classheader=array();
  $classcontent=array();
  $xtuition ='';
  $xtuitiontotal =0;
  $xlabfee = '';
  $xlabfeetotal = 0;
  $xmiscfee = '';
  $xmiscfeetotal = 0;
  $xotherfee = '';
  $xotherfeetotal = 0;
  $xfee = '';
  $xfeetotal = 0;
  $xcount = 1;
  $xfirsttotal =0;
  $xsecondtotal=0;
  $xthirdtotal=0;
  
  $xtemptotal=0;
  $xtempfirst=0;
  $xtempsecond=0;
  $xtempthird=0;
  
  foreach($row as $rs)
  {
   $xcount = $xcount+1; 
   $xclassname = property_exists($rs,'ClassName')? $rs->ClassName : '';
   $xclasssort = property_exists($rs,'ClassSort')? $rs->ClassSort : 0;
   $xacct = property_exists($rs,'AcctName')? $rs->AcctName : '';
   $xdebit = property_exists($rs,'Debit')? $rs->Debit : 0;
   $xactual = property_exists($rs,'ActualPayment')? $rs->ActualPayment : 0;
   $xuno = property_exists($rs,'1st Payment')? $rs->{'1st Payment'} : 0;
   $xdos = property_exists($rs,'2nd Payment')? $rs->{'2nd Payment'} : 0;
   $xtres = property_exists($rs,'3rd Payment')? $rs->{'3rd Payment'} : 0;
   $xfirsttotal = property_exists($rs,'FirstPayment')? $rs->FirstPayment : 0;
   //$xtotal = property_exists($rs,'TotalPayment')? $rs->TotalPayment : 0;
   $xtotal = property_exists($rs,'AssessedFees')? $rs->AssessedFees : 0;
   
   $xclassname = strtolower($xclassname); 
   if($xclassname=='tuition' || $xclassname=='tuition fee' || $xclassname=='tuition fees')
   { 
     $xtuitiontotal = $xtuitiontotal+ $xdebit;
     $xtuition .= "<tr><td>".$xacct."</td>
			          <td></td>
				       <td align='right'>".$this->putDecimal($xdebit)."</td>
				       <td align='right'>".$this->putDecimal($xuno)."</td>
				       <td align='right'>".$this->putDecimal($xdos)."</td>
				       <td align='right'>".$this->putDecimal($xtres)."</td></tr>";
   }
   elseif($xclassname=='laboratory' || $xclassname=='laboratory fee' || $xclassname=='laboratory fees')
   {
     $xlabfeetotal = $xlabfeetotal+ $xdebit;
     $xlabfee .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$xacct."</td>
			           <td></td>
				       <td align='right'>".$this->putDecimal($xdebit)."</td>
				       <td align='right'>".$this->putDecimal($xuno)."</td>
				       <td align='right'>".$this->putDecimal($xdos)."</td>
				       <td align='right'>".$this->putDecimal($xtres)."</td></tr>";
   }
   elseif($xclassname=='miscellaneous' || $xclassname=='miscellaneous fee'  || $xclassname=='miscellaneous income' || $xclassname=='miscellaneous fees')
   {
     $xmiscfeetotal = $xmiscfeetotal+ $xdebit;
     $xmiscfee .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$xacct."</td>
			           <td></td>
				       <td align='right'>".$this->putDecimal($xdebit)."</td>
				       <td align='right'>".$this->putDecimal($xuno)."</td>
				       <td align='right'>".$this->putDecimal($xdos)."</td>
				       <td align='right'>".$this->putDecimal($xtres)."</td></tr>";
   }
   elseif($xclassname=='other' || $xclassname=='other fee' || $xclassname=='other fees')
   {
     $xotherfeetotal = $xotherfeetotal+ $xdebit;
     $xotherfee .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>".$xacct."</small></td>
			           <td></td>
				       <td align='right'>".$this->putDecimal($xdebit)."</td>
				       <td align='right'>".$this->putDecimal($xuno)."</td>
				       <td align='right'>".$this->putDecimal($xdos)."</td>
				       <td align='right'>".$this->putDecimal($xtres)."</td></tr>";
   }
   else
   {
     $xfeetotal = $xfeetotal+ $xdebit;
     $xfee .= "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$xacct."</td>
			           <td></td>
				       <td align='right'>".$this->putDecimal($xdebit)."</td>
				       <td align='right'>".$this->putDecimal($xuno)."</td>
				       <td align='right'>".$this->putDecimal($xdos)."</td>
				       <td align='right'>".$this->putDecimal($xtres)."</td></tr>";
   }
   
   
   $xtemptotal+=$xdebit;
   $xtempfirst+=$xuno;
   $xtempsecond+=$xdos;
   $xtempthird+=$xtres;
   
  }
  
  if($xtuition!='')
  {
   $tblcontent .= "<tr><td><strong>Tuition Fee</strong></td>
			           <td colspan=2><b>".$this->putDecimal($xtuitiontotal)."</b></td>
				       <td></td>
				       <td></td>
				       <td></td></tr>".$xtuition;  
  }
  if($xlabfee!='')
  {
   $tblcontent .= "<tr><td><strong>Laboratory Fee</strong></td>
			           <td colspan=2><b>".$this->putDecimal($xlabfeetotal)."</b></td>
				       <td></td>
				       <td></td>
				       <td></td></tr>".$xlabfee;
  }
  if($xmiscfee!='')
  {
   $tblcontent .= "<tr><td><strong>Miscellaneous Fee</strong></td>
			           <td colspan=2><b>".$this->putDecimal($xmiscfeetotal)."</b></td>
				       <td></td>
				       <td></td>
				       <td></td></tr>".$xmiscfee;
  
  }
  if($xotherfee!='')
  {
   $tblcontent .= "<tr><td><strong>Other Fee</strong></td>
			           <td colspan=2><b>".$this->putDecimal($xotherfeetotal)."</b></td>
				       <td></td>
				       <td></td>
				       <td></td></tr>".$xotherfee;
  
  }
  if($xfee!='')
  {$tblcontent .= $xfee;}
  
  if($xtotal==0 ||$xtotal=='')
  {$xtotal = $xtemptotal;}
  
  if($xfirsttotal==0 || $xfirsttotal=='')
  {$xfirsttotal = $xtempfirst;}
  
  if($xsecondtotal==0 || $xsecondtotal==0)
  {$xsecondtotal = $xtempsecond;}
  
  if($xthirdtotal==0 || $xthirdtotal==0)
  {$xthirdtotal = $xtempthird;}
  
  $footer =  "<tr><td><strong>Total Assessment : </strong></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>
				  <tr><td></td>
				  <td colspan=2><strong>".$this->putDecimal($xtotal)."</strong></td>
			     <td><strong>".$this->putDecimal($xfirsttotal)."</strong></td>
				  <td><strong>".$this->putDecimal(0)."</strong></td>
				  <td><strong>".$this->putDecimal(0)."</strong></td></tr>";
  
  $tblcontent .= $footer."</tbody>";
 }
 $xdisplay = str_replace("</table>",$tblcontent."</table>",$xdisplay);
 return $xdisplay;
}


function generate_feetablev2($row){
 $xdisplay="<table class='table table-bordered'>
            <thead>
				<tr><th>ACCOUNTS</th>
			   <th></th>
				<th>FULL PAYMENT</th>
				<th>1st Payment</th>
				<th>2nd Payment</th>
				<th>3rd Payment</th></tr>
			</thead>
			</table>";
 
 $tblcontent = "<tbody><tr><td colspan=6><center><strong>No Assessed Fees.</strong></center></td></tr></tbody>";
 if($row && $row!='')
 {
  $tblcontent  = "<tbody>";
  $classheader=array();
  $classcontent=array();
  $classtotal=array();
  $xtuition ='';
  $xtuitiontotal =0;
  $xlabfee = '';
  $xlabfeetotal = 0;
  $xmiscfee = '';
  $xmiscfeetotal = 0;
  $xotherfee = '';
  $xotherfeetotal = 0;
  $xfee = '';
  $xfeetotal = 0;
  $xcount = 1;
  $xfirsttotal =0;
  $xsecondtotal=0;
  $xthirdtotal=0;
  
  $xtemptotal=0;
  $xtempfirst=0;
  $xtempsecond=0;
  $xtempthird=0;
  
  foreach($row as $rs)
  {
   $xcount = $xcount+1; 
   $xclassname = property_exists($rs,'ClassName')? $rs->ClassName : '';
   $xclasssort = property_exists($rs,'ClassSort')? $rs->ClassSort : 0;
   $xacct = property_exists($rs,'AcctName')? $rs->AcctName : '';
   $xdebit = property_exists($rs,'Debit')? $rs->Debit : 0;
   $xactual = property_exists($rs,'ActualPayment')? $rs->ActualPayment : 0;
   $xuno = property_exists($rs,'1st Payment')? $rs->{'1st Payment'} : 0;
   $xdos = property_exists($rs,'2nd Payment')? $rs->{'2nd Payment'} : 0;
   $xtres = property_exists($rs,'3rd Payment')? $rs->{'3rd Payment'} : 0;
   $xfirsttotal = property_exists($rs,'FirstPayment')? $rs->FirstPayment : 0;
   $xtotal = property_exists($rs,'AssessedFees')? $rs->TotalPayment : 0;
   
   if(array_key_exists($xclasssort,  $classheader))
   {
   $classtotal[$xclasssort] = $classtotal[$xclasssort] + $xdebit;
   $classcontent[$xclasssort] = $classcontent[$xclasssort]."<tr><td style='text-indent:15px;'><small>".$xacct."</small></td>
			                       <td></td>
				                   <td align='right'>".$this->putDecimal($xdebit)."</td>
				                   <td align='right'>".$this->putDecimal($xuno)."</td>
				                   <td align='right'>".$this->putDecimal($xdos)."</td>
				                   <td align='right'>".$this->putDecimal($xtres)."</td></tr>";
   
   
   }
   else
   {
   $classheader[$xclasssort] = $xclassname;
   $classtotal[$xclasssort] = $xdebit;
   $classcontent[$xclasssort] = "<tr><td style='text-indent:15px;'><small>".$xacct."</small></td>
			                       <td></td>
				                   <td align='right'>".$this->putDecimal($xdebit)."</td>
				                   <td align='right'>".$this->putDecimal($xuno)."</td>
				                   <td align='right'>".$this->putDecimal($xdos)."</td>
				                   <td align='right'>".$this->putDecimal($xtres)."</td></tr>";
   }
   
   $xtemptotal+=$xdebit;
   $xtempfirst+=$xuno;
   $xtempsecond+=$xdos;
   $xtempthird+=$xtres;
   
  }
  
  ksort($classheader);
  foreach($classheader as $key => $val)
  {
   $tblcontent .= "<tr><td><strong>".$val."</strong></td>
			           <td colspan=2><b>".$this->putDecimal($classtotal[$key])."</b></td>
				       <td></td>
				       <td></td>
				       <td></td></tr>".$classcontent[$key];
  
  }
  
  $xtotal       = $xtemptotal;
  $xfirsttotal  = $xtempfirst;
  $xsecondtotal = $xtempsecond;
  $xthirdtotal  = $xtempthird;
  
  $footer =  "<tr><td><strong>Total Assessment : </strong></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>
				  <tr><td></td>
				  <td colspan=2><strong>".$this->putDecimal($xtotal)."</strong></td>
			     <td><strong>".$this->putDecimal($xfirsttotal)."</strong></td>
				  <td><strong>".$this->putDecimal($xsecondtotal)."</strong></td>
				  <td><strong>".$this->putDecimal($xthirdtotal)."</strong></td></tr>";
  
  $tblcontent .= $footer."</tbody>";
 }
 $xdisplay = str_replace("</table>",$tblcontent."</table>",$xdisplay);
 return $xdisplay;
}


function putDecimal($value) {
     if((float) $value === floor($value))
	 {
	  $xval = floor($value).'.00';
	  return number_format($xval, 2, '.', ',');
	 }
	 else
	 {return number_format($value, 2, '.', ',');}
 }

}
?>