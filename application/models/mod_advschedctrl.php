<?php
Class mod_advschedctrl extends CI_Model
{
  function get_schedule($termid=0)
  {
	 $query = "SELECT Sched.IndexID,P.ProgID,P.ProgCode,P.ProgName,0 as MajorID,'' As MajorName, YearLvlID as YearLevelID, dbo.fn_YearLevel(YearLvlID) as YearLevel, Sched.TermID, Sched.DateStart, Sched.DateEnd, Sched.Inactive  
				 FROM ES_Programs as P 
			LEFT JOIN sis_AdvisingSchedule as Sched ON Sched.ProgID=p.ProgID AND ISNULL(Sched.MajorID,0)=0 AND Sched.TermID='".$termid."'
				WHERE P.ProgClass>21
	           UNION ALL
	           SELECT Sched.IndexID,P.ProgID,P.ProgCode,P.ProgName,ISNULL(M.IndexID,0) as MajorID,M.MajorDiscDesc As MajorName,YearLvlID as YearLevelID, dbo.fn_YearLevel(YearLvlID) as YearLevel, Sched.TermID, Sched.DateStart, Sched.DateEnd, Sched.Inactive 
			     FROM ES_Programs as P
		   INNER JOIN ES_ProgramMajors AS PM ON P.ProgID=PM.ProgID AND PM.Inactive=0
		   INNER JOIN ES_DisciplineMajors as M ON PM.MajorDiscID=M.IndexID
		    LEFT JOIN sis_AdvisingSchedule as Sched ON Sched.ProgID=p.ProgID AND ISNULL(Sched.MajorID,0)=ISNULL(M.IndexID,0) AND Sched.TermID='".$termid."'
			    WHERE P.ProgClass>21
			 ORDER BY ProgID,MajorID";
	 
	 $result = $this->db->query($query);
     if($result)
	  return $result->result();	
     else
	  return false;
  }
  
  function manage_schedule($opt=1,$id='new',$term=-1,$prog=0,$major=0,$yrlvl=0,$dstart='',$dend='',$inactive=0)
  {
	 $session_data = $this->session->userdata('logged_in');
	 $query = '';
	 if($opt==1)
     {
	   if($id=='new' || $id=='')
	   {	   
        $query = "INSERT INTO sis_AdvisingSchedule(TermID,ProgID,MajorID,YearLvlID,DateStart,DateEnd,Inactive,DateCreated,CreatedBy) 
		          SELECT ".$term.",ProgID,".$major.",".$yrlvl.",'".$dstart."','".$dend."',".$inactive.",GETDATE(),'".$session_data['id']."' FROM ES_Programs 
				   WHERE ProgID='".$prog."' AND (SELECT COUNT(*) FROM sis_AdvisingSchedule WHERE ProgID=".$prog." AND MajorID=".$major." AND YearLvlID='".$yrlvl."' AND TermID='".$term."')=0";
       }
	   else
	    $query = "UPDATE sis_AdvisingSchedule SET YearLvlID='".$yrlvl."',DateStart='".$dstart."', DateEnd='".$dend."',Inactive=".$inactive.",DateModified=GETDATE(),ModifiedBy='".$session_data['id']."' WHERE IndexID='".$id."'";
       
       if($dstart!='' && $dend!='')
       {
		$exec  = $this->db->query($query);
        return $exec;	  
	   }	
       else
        return false;		   
	 }	
     else if($opt==3)
     {
       $query = "DELETE FROM sis_AdvisingSchedule WHERE IndexID='".$id."'";
	   $exec  = $this->db->query($query);
       return $exec;	   
	 }		 
  }
  
  
}
?>