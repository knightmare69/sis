<?php
Class mod_constants extends CI_Model
{
 var $xlist=array('HEADER_TITLE'
                 ,'MAINTENANCE_MODE'
                 ,'MAINTENANCE_START'
                 ,'MAINTENANCE_MSG'
                 ,'PROMPT_WARN'
				 ,'HELP_DESK'
				 ,'<SEPARATOR>'
				 ,'INSTITUTION'
				 ,'INST_CODE'
				 ,'INST_EMAIL'
				 ,'INST_SITE'
				 ,'INST_PHONE'
				 ,'<SEPARATOR>'
				 ,'xDBDRIVER'
				 ,'xHOST'
				 ,'xDBNAME'
				 ,'xUSERNAME'
				 ,'xPASSWORD'
				 ,'<SEPARATOR>'
				 ,'EMAIL_LIMIT'
				 ,'EMAIL_TAG'
				 ,'EMAIL_NOTIFY'
				 ,'<SEPARATOR>'
				 ,'AUTO_VALID'
				 ,'ENABLE_CAPTCHA'
				 ,'<SEPARATOR>'
				 ,'ACTIVE_ADMISSION'
				 ,'ACTIVE_STUDENT'
				 ,'ENABLE_STUDENT'
				 ,'INST_STUD'
				 ,'ACTIVE_PARENT'
				 ,'ENABLE_PARENT'
				 ,'INST_PAR'
				 ,'ACTIVE_FACULTY'
				 ,'ENABLE_FACULTY'
				 ,'INST_FAC'
				 ,'ACTIVE_ADMIN'
				 ,'ENABLE_ADMIN'
				 ,'INST_ADM'
				 ,'<SEPARATOR>'
				 ,'PARENT_EVALUATION'
				 ,'PARENT_LEDGER'
				 ,'PARENT_ACCOUNTABILITY'
				 ,'<SEPARATOR>'
				 ,'WITH_CHINESE'
				 ,'GRADES_GSHS_PERIOD_AVE'
				 ,'GRADES_GSHS_GRADE1'
				 ,'GRADES_GSHS_LGRADE1'
				 ,'GRADES_GSHS_GRADE2'
				 ,'GRADES_GSHS_LGRADE2'
				 ,'GRADES_GSHS_GRADE3'
				 ,'GRADES_GSHS_LGRADE3'
				 ,'GRADES_GSHS_GRADE4'
				 ,'GRADES_GSHS_LGRADE4'
				 ,'GRADES_GSHS_GRADEF'
				 ,'GRADES_GSHS_LGRADEF'
				 ,'GRADES_GSHS_CONDUCT1'
				 ,'GRADES_GSHS_LCONDUCT1'
				 ,'GRADES_GSHS_CONDUCT2'
				 ,'GRADES_GSHS_LCONDUCT2'
				 ,'GRADES_GSHS_CONDUCT3'
				 ,'GRADES_GSHS_LCONDUCT3'
				 ,'GRADES_GSHS_CONDUCT4'
				 ,'GRADES_GSHS_LCONDUCT4'
				 ,'GRADES_GSHS_CONDUCTF'
				 ,'GRADES_GSHS_LCONDUCTF');
 var $xhidden = array('SKELETON_KEY');
 
 function gen_list()
 {
  $output='<table class="table table-bordered"><tbody><xdata></tbody></table>';
  $list='';
  $xconstant = get_defined_constants(true);	 
  $user_defined = ((isset($xconstant['user']))?($xconstant['user']):(array()));	 
  foreach($this->xlist as $i => $k)
  {
   if($k=='<SEPARATOR>')
    $list = $list.'<tr><td colspan="2"></td></tr>';
   else
    $list = $list.'<tr><td width="40px">'.$k.'</td><td><input class="form-control" id="'.$k.'" name="'.$k.'" type="text" value='.'"'.((array_key_exists($k,$user_defined))?htmlentities($user_defined[$k]):'').'"'.'/></td></tr>';	  
  }
  $list = $list.'<tr><td colspan="2"></td></tr>';
  foreach($this->xhidden as $i => $k)
  {
   $list = $list.'<input class="form-control" id="'.$k.'" name="'.$k.'" type="hidden" value="'.((array_key_exists($k,$user_defined))?$user_defined[$k]:'').'">';	  
  }
  $output= str_replace('<xdata>',$list,$output);
  return $output;
 }
 
 function save_list($value=array())
 {
  $xconstant = get_defined_constants(true);	 
  $user_defined = ((isset($xconstant['user']))?($xconstant['user']):(array()));	 
  $filepath=str_replace("index.php","config.php",$_SERVER['SCRIPT_FILENAME']);
  if(file_exists($filepath)){unlink($filepath); }
  if(!$fp = @fopen($filepath, FOPEN_WRITE_CREATE)){return FALSE; }
  fwrite($fp, '<?php'.PHP_EOL);
  fwrite($fp, '//DO NOT ALTER ANY VALUE IN THIS FILE. IF VALUE IS ALTER AND AFFECTS THE SYSTEM PROCCESS, WE WILL NOT FIX IT.'.PHP_EOL);
  foreach($this->xlist as $i => $k)
  {
   if($k=='<SEPARATOR>')
    $message ="//===========================================================================";
   else
	$message ="define('".$k."','".((array_key_exists($k,$value))?$value[$k]:((array_key_exists($k,$user_defined))?$user_defined[$k]:''))."'); ";
   
   fwrite($fp, $message.PHP_EOL);
  }
  
  foreach($this->xhidden as $i => $k)
  {
   if($k=='<SEPARATOR>')
    $message =" ";
   else
	$message ="define('".$k."','".((array_key_exists($k,$value))?$value[$k]:((array_key_exists($k,$user_defined))?$user_defined[$k]:''))."'); ";
   
   fwrite($fp, $message.PHP_EOL);
  }
  fwrite($fp, '?>');
  fclose($fp);
  return TRUE;		
 }
}
?>