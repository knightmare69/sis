<?php
Class mod_crystalreports extends CI_Model
{
 //$this->config->load('database');
 
 //This is compatible with Crystal Report 9
 
 //var $FormatType = 14; var $FormatExt = '.doc';  //Word for Windows
 //var $FormatType = 27; var $FormatExt = '.xls';  //Excel 7.0
 private $comname     = "CrystalRuntime.Application.9";
 private $FormatType  = 31; 
 private $FormatExt   = '.pdf';  //Portable Document format (PDF)
 //var $FormatType = 35; var $FormatExt = '.rtf';  //Exact Rich Text
 
 
 //var $FormatType = "crEFTHTML40"; var $FormatExt = '.html'; //HTML40
 
 function prepare_deficient_primo($stdno='')
 {
  $server = xHOST;
  $user   = xUSERNAME;
  $pass   = xPASSWORD;
  $dbname = xDBNAME;
  
  //echo $server.$dbname.$user.$pass;
  //die();
  ini_set('max_execution_time', 900); //300 seconds = 5 minutes
  if($stdno!=null && $stdno!='' && $stdno!=' ')
  {
  
  $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/deficiencycourses.rpt';
  $file = str_replace('.rpt',$stdno.'.pdf',$rptfile);
  $filename = 'deficiencycourses'.$stdno.'.pdf';
  
  //try
  //{
  $crapp = new COM ($this->comname) or die ("Error on load");
  $creport = $crapp->OpenReport($rptfile, 1);
  $csubrpt1 = $creport->OpenSubReport("Company_Logo");
  $csubrpt2 = $creport->OpenSubReport("evalresult");

  $creport->Database->Tables(1)->SetLogOnInfo($server, $dbname, $user, $pass);
  $csubrpt1->Database->Tables(1)->SetLogOnInfo($server, $dbname, $user, $pass);
  $csubrpt2->Database->Tables(1)->SetLogOnInfo($server, $dbname, $user, $pass);
  
  $creport->EnableParameterPrompting = False;
  $creport->DiscardSavedData;
  $creport->RecordSelectionFormula="{ES_Registrations.StudentNo}='".$stdno."'";
  $creport->ReadRecords();	

  $creport->ExportOptions->DiskFileName=$file;
  $creport->ExportOptions->DestinationType=1; // Export to File
  $creport->ExportOptions->FormatType=31; // Type: RTF
  $creport->DiscardSavedData();
  $creport->Export(false);
  $csubrpt2 = null;
  $csubrpt1 = null;
  $creport = null;
  $crapp = null;
  //}
  //catch(Exception $e) 
  //{
  // return array('none','none');
  //}
  return array($file,$filename);
  }
  else
  {
  return array('none','none');
  }  
 }
 
 function prepare_application($appno=null)
 {
  $file = 'none';
  $filename = 'none';
  
  if($appno!=null && $appno!='' && $appno!=' ')
  {
  //$rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/admission_form.rpt';
   $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/admission_exampermit.rpt';
   $file = str_replace('.rpt',$appno.'.pdf',$rptfile);
   $filename = 'admission_form'.$appno.'.pdf';
   $mainquery = "EXEC sis_ApplicationForm '".$appno."';";
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,false,false,false,false,false);
   //list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true,$subrpt1,$subquery1,$subrpt2,$subquery2);
  }
  return array($file,$filename);
 }
 
 function prepare_applicationb($appno=null)
 {
  $file = 'none';
  $filename = 'none';
  
  if($appno!=null && $appno!='' && $appno!=' '){
   $checkqry = $this->db->query("SELECT COUNT(*) as Items FROM ESv2_Admission WHERE AppNo='".$appno."'")->result();
   if($checkqry && ($checkqry[0]->Items)>0){
	$rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/admission_form - K12.rpt';
   }else{
	$rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/admission_form - SBU.rpt';
   }   
   $file = str_replace('.rpt',$appno.'.pdf',$rptfile);
   $filename = 'admission_form'.$appno.'.pdf';
   $mainquery = "EXEC sis_ApplicationForm '".$appno."';";
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,false,false,false,false,false);
   //list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true,$subrpt1,$subquery1,$subrpt2,$subquery2);
  }
  return array($file,$filename);
 }
 
 function prepare_prereg($regid=null)
 {
  $file = 'none';
  $filename = 'none';
  
  if($regid!=null && $regid!='' && $regid!=' ')
  {
   $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/preregistration.rpt';
   $file = str_replace('.rpt',$regid.'.pdf',$rptfile);
   $filename = 'preregistration'.$regid.'.pdf';
   $mainquery = "EXEC ES_rptRegAssessment '".$regid."', 'San Beda College Alabang', 'Republic of the Philippines', 'Nagtahan, Sampaloc, Manila', 'Main Campus', 'allen',  'STUDENT/REGISTRAR';";
   $subrpt1 = "dsr_subjects";
   $subquery1 = "EXEC ES_rptRegAssessment_EnrolledSubjects '".$regid."';";
   $subrpt2 = "AssessedFees";
   $subquery2 = "EXEC ES_rptRegAssessment_Accounts '".$regid."','0';";
   
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true,$subrpt1,$subquery1,$subrpt2,$subquery2);
   //list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true,$subrpt1,$subquery1,$subrpt2,$subquery2);
  }
  return array($file,$filename);
 }
 
 
 function prepare_cor($regid=null,$yearterm=null)
 {
  $file = 'none';
  $filename = 'none';
  
  if($regid!=null && $regid!='' && $regid!=' ')
  {
   $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/COR.rpt';
   $file = str_replace('.rpt',$regid.'.pdf',$rptfile);
   $filename = 'COR'.$regid.'.pdf';
   $mainquery = "EXEC ES_rptRegAssessment '".$regid."', 'San Beda College Alabang', '', 'Benavidez St., Sta. Cruz, Manila', 'Alabang Campus', 'admin',  'Student'";
   $subrpt1 = "dsr_subjects";
   $subquery1 = "EXEC ES_GetStudentRegistration_Subjects '".$regid."'";
   $subrpt2 = "AssessedFees";
   $subquery2 = "EXEC ES_rptRegAssessment_Accounts '".$regid."','0'";
   $subrpt3 = "dsr_subjects2";
   $subquery3 = "EXEC ES_GetStudentRegistration_Subjects '".$regid."'";
   
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true,$subrpt1,$subquery1,$subrpt2,$subquery2);//,$subrpt3,$subquery3);
  }
  return array($file,$filename);
 }
 
 function prepare_deficient($stdno='')
 {
  $file = 'none';
  $filename = 'none';
  
  if($stdno!=null && $stdno!='' && $stdno!=' ')
  {
   $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/sis_evaluation_deficiencycourses.rpt';
   $file = str_replace('.rpt',$stdno.'.pdf',$rptfile);
   $filename = 'sis_evaluation_deficiencycourses'.$stdno.'.pdf';
   $mainquery = "EXEC [sis_rptEvaluation_DeficientCourses] '".$stdno."';";
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true);
  }
  return array($file,$filename);
 }
 
 function prepare_grades($stdno='',$termid=0)
 {
  $file = 'none';
  $filename = 'none';
  
  if($stdno!=null && $stdno!='' && $stdno!=' ' && $termid!=null && $termid!='' && $termid!=' ')
  {
   $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/report of grades.rpt';
   $file = str_replace('.rpt',$stdno.'.pdf',$rptfile);
   $filename = 'report of grades'.$stdno.'.pdf';
   $mainquery = "EXEC po_reportofgrades '".$stdno."','".$termid."';";
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true);
  }
  return array($file,$filename);
 }
 
  
 function prepare_SOA($stdno='',$campusid=0,$termid=0,$collegeid=0,$programid=0,$yrlvlid=0)
 {
  $file = 'none';
  $filename = 'none';
  
  if($stdno!=null && $stdno!='' && $stdno!=' ')
  {
   $dateasof = date('Y/m/d H:i:s A');
   $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/SOA.rpt';
   $file = str_replace('.rpt',$stdno.'.pdf',$rptfile);
   $filename = 'SOA'.$stdno.'.pdf';
   $mainquery = "EXEC dbo.ES_rptStatementofAccount_College @CampusID='".$campusid."', @TermID=".$termid.", @FilterBy = 0, @Bulk = 0, @ByLevel = 0, @CollegeID=".$collegeid.", @ProgramID =".$programid.", @YearLevelID=".$yrlvlid.", @StudentNo = '".$stdno."', @DateAsOF = '".$dateasof."', @IsMidtermFinal = 2, @PrintSOALetter = 0, @PrintedBy = 'allen';";
   $subquery1="EXEC dbo.ES_rptGetStudentRegistration_Subjects @CampusID=".$campusid.", @TermID = ".$termid.", @FilterBy = 0, @Bulk = 0, @ByLevel = 0, @CollegeID=".$collegeid.", @ProgramID =".$programid.", @YearLevelID=".$yrlvlid.", @StudentNo='".$stdno."';";
   $subrpt1 = "subject enrolled";
   $subquery2="EXEC dbo.ES_rptStatementofAccount_OfficialReceipts_College @CampusID=".$campusid.", @TermID = ".$termid.", @FilterBy = 0, @Bulk = 0, @ByLevel = 0, @CollegeID=".$collegeid.", @ProgramID =".$programid.", @YearLevelID=".$yrlvlid.", @StudentNo='".$stdno."', @DateAsOF = '".$dateasof."'";
   $subrpt2 = "payment";
   $subquery3="EXEC dbo.ES_rptStatementofAccount_Assessment_College @CampusID=".$campusid.", @TermID = ".$termid.", @FilterBy = 0, @Bulk = 0, @ByLevel = 0, @CollegeID=".$collegeid.", @ProgramID =".$programid.", @YearLevelID=".$yrlvlid.", @StudentNo='".$stdno."', @DateAsOF = '".$dateasof."'";
   $subrpt3 = "assessment";

   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true,$subrpt1,$subquery1,$subrpt2,$subquery2,$subrpt3,$subquery3);
  }
  return array($file,$filename);
 }
 
 function prepare_gradesheet($schedid=0,$facultyid='',$empid='',$opt=0)
 {
  $file = 'none';
  $filename = 'none';
 
  if($schedid!=null && $facultyid!='' && $empid!=' ')
  {
   if($opt==1)
   {$rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/gradesheets/grading_sheet_midterm.rpt';}
   else
   {$rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/gradesheets/grading_sheet_final.rpt';}
   
   $file = str_replace('.rpt',$empid.'.pdf',$rptfile);
   $filename = 'grading_sheet'.$empid.'.pdf';
   $mainquery = "EXEC ES_rptGradeSheet '".$schedid."', '".$facultyid."', '".$empid."';";
   
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true);
  }
  return array($file,$filename);
 } 
 
 function prepare_gradesheet_bulk($termid=0,$campusid='',$empid='',$opt=0)
 {
  $file = 'none';
  $filename = 'none';
 
  if($termid!=null && $campusid!='' && $empid!=' ')
  {
   if($opt==1)
   {$rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/gradesheets/grading_sheet_midterm_bulk.rpt';}
   else
   {$rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/gradesheets/grading_sheet_final_bulk.rpt';}
   
   $file = str_replace('.rpt',$empid.'.pdf',$rptfile);
   $filename = 'grading_sheet_bulk'.$empid.'.pdf';
   $mainquery = "EXEC ES_rptGradeSheet_Bulk '".$termid."', '".$campusid."', '', '".$empid."';";
   
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true);
  }
  return array($file,$filename);
 }
 
 function prepare_submitmonitor($termid=0,$campusid='',$facid='',$empid='')
 {
  $file = 'none';
  $filename = 'none';
 
  if($termid!=null && $campusid!='' && $empid!=' ')
  {
   $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/gradesheets/monitoring_submission_of_grades.rpt';
   
   $file = str_replace('.rpt',$empid.'.pdf',$rptfile);
   $filename = 'monitoring_submission_of_grades'.$empid.'.pdf';
   $mainquery = "EXEC ES_rptMonitoringSubmittedGradingSheet '".$termid."', '".$campusid."', '".$facid."', '".$empid."';";
   
   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,true);
  }
  return array($file,$filename);
 }
 
 function getBetween($content,$start,$end)
 {
    $r = explode($start, $content);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return $content;
 }
 
 function prepare_facultyeval($idno='',$term=0)
 {
  $file = 'none';
  $filename = 'none';
  if($idno!=null && $idno!='' && $idno!=' ')
  {
   $dateasof = date('Y/m/d H:i:s A');
   $rptfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/rpt/faculty_evaluation/faculty_evaluation.rpt';
   $file = str_replace('.rpt',$idno.'.pdf',$rptfile);
   $filename = 'faculty_evaluation'.$idno.'.pdf';
   $mainquery = "EXEC rpt_FacultyEvaluation @Opt=1,@ID='".$idno."',@TermID='".$term."'";
			   
   $subquery1="EXEC rpt_FacultyEvaluation @Opt=2,@ID='".$idno."',@TermID='".$term."'";
   $subrpt1 = "details";
			   
   $subquery2="EXEC rpt_FacultyEvaluation @Opt=3,@ID='".$idno."',@TermID='".$term."'";
   $subrpt2 = "comments";

   list($file,$filename)=$this->SetBIReports($rptfile,$file,$filename,$mainquery,false,$subrpt1,$subquery1,$subrpt2,$subquery2);
  }
  return array($file,$filename);
 }
 
 function SetBIReports($filename=null,$output=null, $outputname=null,$mainquery=null,$sublogo=false,$subreport1=null,$subquery1=null,$subreport2=null,$subquery2=null,$subreport3=null,$subquery3=null,$subreport4=null,$subquery4=null,$subreport5=null,$subquery5=null)
 {
  $server = xHOST;
  $user   = xUSERNAME;
  $pass   = xPASSWORD;
  $dbname = xDBNAME;
  
  ini_set('max_execution_time', 5000); //300 seconds = 5 minutes
  
  $crapp  = new COM ($this->comname) or die ("Error on load Crystal Report");
  $dbconn = new COM ("ADODB.Connection") or die ("Error on load ADODB Connection");
  $dbconn->ConnectionTimeout=120;
  $dbconn->CommandTimeout=600;
  //$dbconn->Open("Provider=SQLOLEDB; Data Source=".$server.";Initial Catalog=".$dbname."; User ID=".$user."; Password=".$pass.";Connect Timeout=5000");
  $dbconn->Open('Provider=SQLOLEDB.1;Password='.$pass.';Persist Security Info=True;User ID='.$user.';Initial Catalog='.$dbname.';Data Source='.$server.';');
  log_message('error','Printing Components Loaded');
  
  $rsMain = new COM ("ADODB.Recordset")  or die ("Error on load ADODB Recordset");
  $creport = $crapp->OpenReport($filename, 1);
  $rsMain->Open($mainquery,$dbconn,3,1,1);
  $creport->Database->SetDataSource($rsMain->DataSource);
  log_message('error','Printing DB Connection Set');
  
  if($sublogo==true)
  {
   $rsLogo = new COM ("ADODB.Recordset")  or die ("Error on load ADODB Recordset");
   $sqlLogo = "EXEC ES_rptReportLogo  'San Beda College Alabang','Republic of the Philippines','Alabang Hills Village, Muntinlupa City', '-1'";
   $rsLogo->Open($sqlLogo,$dbconn,3,1,1);
   $csublogo = $creport->OpenSubReport("Company_Logo");
   $csublogo->Database->SetDataSource($rsLogo->DataSource);
  }
  
  if($subreport1!=null and $subquery1!=null)
  {
  $rsSub1 = new COM ("ADODB.Recordset")  or die ("Error on load ADODB Recordset");
  $csubrpt1 = $creport->OpenSubReport($subreport1);
  $rsSub1->Open($subquery1,$dbconn,3,1,1);
  $csubrpt1->Database->SetDataSource($rsSub1->DataSource);
  }
  
  if($subreport2!=null and $subquery2!=null)
  {
  $rsSub2 = new COM ("ADODB.Recordset")  or die ("Error on load ADODB Recordset");
  $csubrpt2 = $creport->OpenSubReport($subreport2);
  $rsSub2->Open($subquery2,$dbconn,3,1,1);
  $csubrpt2->Database->SetDataSource($rsSub2->DataSource);
  }
  
  if($subreport3!=null and $subquery3!=null)
  {
  $rsSub3 = new COM ("ADODB.Recordset")  or die ("Error on load ADODB Recordset");
  $csubrpt3 = $creport->OpenSubReport($subreport3);
  $rsSub3->Open($subquery3,$dbconn,3,1,1);
  $csubrpt3->Database->SetDataSource($rsSub3->DataSource);
  }
  
  if($subreport4!=null and $subquery4!=null)
  {
  $rsSub4 = new COM ("ADODB.Recordset")  or die ("Error on load ADODB Recordset");
  $csubrpt4 = $creport->OpenSubReport($subreport4);
  $rsSub4->Open($subquery1,$dbconn,3,1,1);
  $csubrpt4->Database->SetDataSource($rsSub4->DataSource);
  }
  
  if($subreport5!=null and $subquery5!=null)
  {
  $rsSub5 = new COM ("ADODB.Recordset")  or die ("Error on load ADODB Recordset");
  $csubrpt5 = $creport->OpenSubReport($subreport5);
  $rsSub5->Open($subquery1,$dbconn,3,1,1);
  $csubrpt5->Database->SetDataSource($rsSub5->DataSource);
  }
  
  $creport->EnableParameterPrompting = False;
  $creport->DiscardSavedData;
  
  $creport->ExportOptions->DestinationType=1; // Export to File
  $creport->ExportOptions->FormatType=$this->FormatType; 
  $creport->ExportOptions->DiskFileName=$output;
  log_message('error','All Setting');
  try
  {
   $creport->Export(false);
   log_message('error','Export');
  }
  catch(Exception $e)
  {
   die("Unable To Print!! Please Try Again Later!!");	  
  }	
  
  if($subreport5!=null and $subquery5!=null)
  {
   $rsSub5->Close();
   $csubrpt5=null;
  }
  if($subreport4!=null and $subquery4!=null)
  {
   $rsSub4->Close();
   $csubrpt4=null;
  }
  
  if($subreport3!=null and $subquery3!=null)
  {
   $rsSub3->Close();
   $csubrpt3=null;
  }
  
  if($subreport2!=null and $subquery2!=null)
  {
   $rsSub2->Close();
   $csubrpt2=null;
  }
  
  if($subreport1!=null and $subquery1!=null)
  {
   $rsSub1->Close();
   $csubrpt1=null;
  }
  
  if($sublogo!=null and $sublogo!=null)
  {
   $rsLogo->Close();
   $csublogo=null;
  }
  
  $rsMain->Close();
  $dbconn->Close();
  $rsMain  = null;
  $dbconn  = null;
  $creport = null;
  $crapp   = null;
  
  return array($output,$outputname);
 }
 
}
?>