<?php
Class mod_enotify extends CI_Model
{
 
 function GetUserList($limit='500',$uid=0,$find='')
 {
  $this->db->select('TOP '.$limit.' *');
  $this->db->from('ES_Membership');
  $this->db->where('IsActivated',1);
  $this->db->where('IDType<>0');
  if($uid!=0)
  {
    $this->db->where('UserID>'.$uid); 
  }	 
  return $this->db->get()->result();
 }
 
 function identify_type($idtype=0)
 {
  switch($idtype)
  {
	 case -1:
		return 'Administrator';	 
	  break;
	 case 1:
		return 'Student';	 
	  break;
	 case 2:
		return 'Parent';	 
	  break;
	 case 3:
		return 'Faculty';	 
	  break;
  }
 }
 
 function identify_idtype($typen='')
 {
  switch(strtolower($typen))
  {
	 case 'administrator':
		return -1;	 
	  break;
	 case 'student':
		return 1;	 
	  break;
	 case 'parent':
		return 2;	 
	  break;
	 case 'faculty':
		return 3;	 
	  break;
  } 
 }
 
 function generate_trow($data=false)
 {
  if($data)
  {
   $tmpdata='';
   $count=1;
   foreach($data as $rs)
   {
	$uid    = is_key_exist($rs,'UserID',0);
    $uname  = is_key_exist($rs,'UserName','');
    $email  = is_key_exist($rs,'Email','');
    $idno   = is_key_exist($rs,'IDNo','');
    $lname  = is_key_exist($rs,'LastName','');
    $fname  = is_key_exist($rs,'FirstName','');
    $mname  = is_key_exist($rs,'MiddleInitial','');
    $idtype = is_key_exist($rs,'IDType',0);
	$typen  = $this->identify_type($idtype);
	
	$fulln = (($lname==$fname || $lname=='' || $fname=='')?$lname:$lname.', '.$fname.' '.$mname);
    $chkbx = '<input type="checkbox" id="chk'.$count.'" class="chkdata" data-pointer="'.utf8_encode($email).'"/>';
	$tmpdata.='<tr data-uid="'.$uid.'">
	            <td>'.$chkbx.'</td>
				<td>'.utf8_encode($uname).'</td>
				<td>'.utf8_encode($email).'</td>
				<td>'.$idno.'</td>
				<td>'.utf8_encode($fulln).'</td>
				<td>'.$typen.'</td>
			   </tr>';
	$count++;
   }
   return $tmpdata;
  }	
  else
   return '';	  
 }
 
 function Gen_MemberList($limit='500',$uid=0)
 {
  $result = $this->GetUserList($limit,$uid);
  $table='<table id="tblmember" class="table table-bordered" style="white-space:nowrap;">
          <thead>
		   <tr>
		     <th></th>
			 <th>Username</th>
			 <th>Email</th>
			 <th>IDNo</th>
			 <th>Name</th>
			 <th>Type</th>
		   </tr>
		  </thead>
		  <tbody>
		  <xdata>
		  </tbody>
		  </table>';
  $tdata = (($result)? $this->generate_trow($result) : '<tr><td colspan="6">No data available</td></tr>');
  $table = str_replace('<xdata>',$tdata,$table);
  return $table;
 }
 
 function Gen_xMemberList($limit=500,$uid=0,$find='')
 {
  $result = $this->GetUserList($limit,$uid);
  return $this->generate_trow($result);
 }
 
}
?> 