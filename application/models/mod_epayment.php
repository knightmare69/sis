<?php
Class mod_epayment extends CI_Model
{
  function get_subject($regid='')
  {
	return false;  
  }
  
  function get_assessment($regid='')
  {
	return false;  
  }
  
  function get_billref($regid='',$paysched=0)
  {
	return false;  
  }
  
  function generate_feetable($row){
   $classheader    = array();
   $classcontent   = array();
   $classtotal     = array();
   $xtuition       = '';
   $xtuitiontotal  = 0;
   $xlabfee        = '';
   $xlabfeetotal   = 0;
   $xmiscfee       = '';
   $xmiscfeetotal  = 0;
   $xotherfee      = '';
   $xotherfeetotal = 0;
   $xfee           = '';
   $xfeetotal      = 0;
   $xcount         = 1;
   $xtotal         = 0;
   $xfirsttotal    = 0;
   $xsecondtotal   = 0;
   $xthirdtotal    = 0;
   $xfourtotal     = 0;
   $xfivetotal     = 0;
  
   $xtemptotal  = 0;
   $xtempfirst  = 0;
   $xtempsecond = 0;
   $xtempthird  = 0;
   $xtempfour   = 0;
   $xtempfive   = 0;
  
   $totalpay   = 0;
   $duebalance = 0;
   $schedule   = 0;
   $xdisplay   = "<table class='table table-bordered'>
                   <thead>
					<tr><th>ACCOUNTS</th>
						<th></th>
						<th>FULL PAYMENT</th>
						<th>1st Payment</th>
						<th>2nd Payment</th>
						<th>3rd Payment</th></tr>
				   </thead>
				  </table>";
 
   $tblcontent = "<tbody><tr><td colspan=6><center><strong>No Assessed Fees.</strong></center></td></tr></tbody>";
   if($row && $row!='')
   {
    $tblcontent     = "<tbody>";
    foreach($row as $rs)
    {
     $xcount     = $xcount+1; 
     $xclassname = is_key_exist($rs,'ClassName','');
     $xclasssort = is_key_exist($rs,'ClassSort',0);
     $xacct      = is_key_exist($rs,'AcctName','');
     $xdebit     = is_key_exist($rs,'Debit',0);
     $xtotal     = is_key_exist($rs,'AssessedFees',0);
     $xuno       = is_key_exist($rs,'1st Payment',0);
     $xdos       = is_key_exist($rs,'2nd Payment',0);
     $xtres      = is_key_exist($rs,'3rd Payment',0);
     $xfour      = is_key_exist($rs,'4th Payment',0);
     $xfive      = is_key_exist($rs,'5th Payment',0);
	 
     $xactual    = is_key_exist($rs,'ActualPayment',0);
     
     if(array_key_exists($xclasssort,  $classheader))
     {
      $classtotal[$xclasssort] = $classtotal[$xclasssort] + $xdebit;
      $classcontent[$xclasssort] = $classcontent[$xclasssort]."<tr><td style='text-indent:15px;'><small>".$xacct."</small></td>
			                                                       <td></td>
				                                                   <td align='right'>".putDecimal($xdebit)."</td>
				                                                   <td align='right'>".putDecimal($xuno)."</td>
				                                                   <td align='right'>".putDecimal($xdos)."</td>
				                                                   <td align='right'>".putDecimal($xtres)."</td></tr>";
     }
     else
     {
      $classheader[$xclasssort] = $xclassname;
      $classtotal[$xclasssort] = $xdebit;
      $classcontent[$xclasssort] = "<tr><td style='text-indent:15px;'><small>".$xacct."</small></td>
			                            <td></td>
				                        <td align='right'>".putDecimal($xdebit)."</td>
				                        <td align='right'>".putDecimal($xuno)."</td>
				                        <td align='right'>".putDecimal($xdos)."</td>
				                        <td align='right'>".putDecimal($xtres)."</td></tr>";
     }
   
     $xtemptotal +=$xdebit;
     $xtempfirst +=$xuno;
     $xtempsecond+=$xdos;
     $xtempthird +=$xtres;
     $xtempfour  +=$xfour;
     $xtempfive  +=$xfive;
	 $totalpay   +=$xactual;
    }
    
    ksort($classheader);
    foreach($classheader as $key => $val)
    {
     $tblcontent .= "<tr><td><strong>".$val."</strong></td>
			             <td colspan=2><b>".putDecimal($classtotal[$key])."</b></td>
				         <td></td>
				         <td></td>
				         <td></td></tr>".$classcontent[$key];
    }
  
    $xtotal       = $xtemptotal;
    $xfirsttotal  = $xtempfirst;
    $xsecondtotal = $xtempsecond;
    $xthirdtotal  = $xtempthird;
    $xfourtotal = $xtempfour;
    $xfivetotal  = $xtempfive;
  
    $footer =  "<tr><td><strong>Total Assessment : </strong></td> <td></td> <td></td> <td></td> <td></td> <td></td></tr>
    		    <tr><td></td>
				    <td colspan=2><strong>".putDecimal($xtotal)."</strong></td>
			        <td><strong>".putDecimal($xfirsttotal)."</strong></td>
				    <td><strong>".putDecimal($xsecondtotal)."</strong></td>
				    <td><strong>".putDecimal($xthirdtotal)."</strong></td></tr>";
  
    $tblcontent .= $footer."</tbody>";
   }
   $xdisplay = str_replace("</table>",$tblcontent."</table>",$xdisplay);
   
   if($xtotal!=0)
   {
	if($totalpay>=$xtotal)
	{	
	 $duebalance = 0;
	 $schedule   = 0;
	} 
    else if($totalpay<$xfirsttotal)
	{	
     $duebalance = $xfirsttotal;
	 $schedule   = 1;
    }
	else if($totalpay<($xfirsttotal+$xsecondtotal))
	{	
     $duebalance = $xsecondtotal;
	 $schedule   = 2;
    }
	else if($totalpay<($xfirsttotal+$xsecondtotal+$xthirdtotal))
	{	
     $duebalance = ($xfirsttotal+$xsecondtotal+$xthirdtotal)-$totalpay;
	 $schedule   = 3;
    }
	else if($totalpay<($xfirsttotal+$xsecondtotal+$xthirdtotal+$xfourtotal))
	{	
     $duebalance = ($xfirsttotal+$xsecondtotal+$xthirdtotal+$xfourtotal)-$totalpay;
	 $schedule   = 4;
    }
	else if($totalpay<($xfirsttotal+$xsecondtotal+$xthirdtotal+$xfourtotal+$xfivetotal))
	{	
     $duebalance = ($xfirsttotal+$xsecondtotal+$xthirdtotal+$xfourtotal+$xfivetotal)-$totalpay; 
	 $schedule   = 5;
	} 
   }	   
   $balance = ($xtotal-$totalpay);
   return array('content'    => $xdisplay
               ,'total'      => $xtotal
			   ,'payment'    => $totalpay
			   ,'balance'    => $balance
			   ,'duebalance' => $duebalance
			   ,'schedule'   => $schedule);
  }
  
  function gen_payoption()
  {
	$output  = '';
	if(defined('Connect7') && Connect7==TRUE)
	{
	 $output = '<label class="radio">
				<input type="radio" name="service" value="Connect7"/>
				<i/>
				7-Connect - Pay at any 711 Store
				</label>';	
	}	
  }
}
?>