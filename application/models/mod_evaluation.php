<?php
Class mod_evaluation extends CI_Model
{
 private $isregistered  = 0;
 private $curriculumid = 0;
 private $progid       = 0;
 private $majorid      = 0;
 private $termid       = 0;
 private $campusid     = 0;
 private $yrlvlid      = 0;
 private $assume       = 0;
 private $withinadv    = 0;
 private $withinenr    = 0;
 private $curriculum   = array();
 private $subjects     = array();
 private $prerequisite = array();
 private $equivalent   = array();
 private $iselective   = array();
 private $elective     = array();
 private $felective    = array();
 private $grades       = array();
 private $finalremark  = array();
 private $evaluation   = array();
 private $offeredsubj  = array();
 
 private $insection    = array();
 private $advisable    = array();
 
 private $totalcourse     = 0;
 private $totallecunit    = 0;
 private $totallabunit    = 0;
 private $totalcredit     = 0;
 private $totalsubjearned = 0;
 private $totallecearned  = 0;
 private $totallabearned  = 0;
 private $totalcreditearn = 0;
 private $totalsubjto     = 0;
 private $totallecto      = 0;
 private $totallabto      = 0;
 private $totalcreditto   = 0;
 
 function call_xtraDetails($param=null)
 {
  $result = false;
  if($param!='')
   {
    $query = "SELECT 
	             dbo.fn_StudentName(s.StudentNo) AS StudentName,
		         dbo.fn_ProgramCollegeName(s.Progid) AS CollegeName,
		         dbo.fn_GetProgramNameWithMajor(ProgID,MajorDiscID) AS Program ,
		         dbo.fn_CurriculumCode(CurriculumID) AS Curriculum
	            FROM dbo.ES_Students s
	           WHERE s.StudentNo = '".$param."';";
	      
    $result = $this->db->query($query);
    return $result->row();	
   }  
  return $result;
 } 
 
 function call_SP($param=null){
  $result = false;
  if($param!='')
  {
   $this->db->reconnect();
   $query = "EXEC sis_evaluation @StudentNo ='".$param."';";
   $result = $this->db->query($query);
   return (($result)? $result->result() : false);		
  }
  
  return $result;
 } 
 
 function studAcadEvaluation($param=null)
 {
  $result = false;
  if($param!='')
  {
    //$query = "EXEC sis_StudentAcademicEvaluation @StudentNo ='".$param."';";
    //return $this->db->query($query)->result();		
	return  $this->exec_Evaluation($param);
  }  
  return $result;
 } 
 
 function delAcadEvaluation($studno='')
 {
  $result=false;
  if($studno!='')
  {
   $this->db->reconnect();
   return $this->db->delete('sis_AcademicEvaluation',array('StudentNo'=>$studno)); 
  }
  
  return $result;  
 }
 
 function GenExpandedRegStud($param=null)
 {
  $result = false;
  if($param!='')
   {
    $query = "EXEC sis_GenerateExpandedRegularStudents @StudentNo ='".$param."';";
    $result = $this->db->query($query);
	return $result;		
   }  
  return $result;
 }
 
 function fetch_row($param='')
 {
  $this->db->reconnect();
  $row = $this->call_SP($param);
  return $row;
 }
 
 function generate_table($row){
	function sort_object($a,$b)
	{
     if($a->SortOrder == $b->SortOrder){return 0 ;}
     return ($a->SortOrder < $b->SortOrder) ? -1 : 1;
    }
	 
	$xdisplay ="<table class='table table-bordered'>
                <thead><tr><th>YEAR/TERM</th>
                           <th>#</th>
                           <th>COURSE CODE</th>
                           <th>COURSE TITLE</th>
                           <th>UNIT</th>
                           <th>FINAL</th>
                           <th>REEXAM</th>
                           <th>CREDIT</th>
                           <th>REMARKS</th>
                           <th>PRE-REQUISITES</th>
                           <th>EQUIVALENT</th>
                           <th>YR STANDING</th>
                           <th>ACADEMIC YEAR TERM TAKEN</th>
                           <th>YEAR LEVEL TAKEN</th>
						   <th>TAKEN FROM OTHER SCHOOL</th>
                           <th>DATE ENTERED</th>
                           <th>DATE POSTED</th>
                        </tr></thead><tbody><tr><td colspan='17'><center id='tblloader2'><span class='fa fa-refresh fa-spin'></span>  Loading Please Wait...</center></td></tr></tbody></table>";
    
	$xsummary="<table class='table table-striped' style='font-size:small;'>
	           <tbody>
	           <tr><td><b>Total Course in Curriculum:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lecture Units:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Laboratory Units:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Credit Units:</b></td><td>0</td></tr>
			   
	           <tr><td><b>Total Credited Course:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lecture Units Earned:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lab. Units Earned:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Credit Units Earned:</b></td><td>0</td></tr>
			   
	           <tr><td><b>Total Credit Course to Earn:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lecture Units to Earn:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lab. Units to Earn:</b></td><td>0</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Credit Units  to Earn:</b></td><td>0</td></tr>
	           </tbody>
			   </table>";
			   
  if($row && $row!='')
  {
   $xrow_span = 1;
   $xcount = 0.0;
   $xfoot_totalunit = 0.0;
   $xfoot_totalcunit = 0.0;
   $initial = '';
   $xgroupdetail = '';
   $xdisplay = "<table class='table table-bordered' style='white-space: nowrap;'>";
   $xdisplay .="<thead><tr>
                           <th>YEAR/TERM</th>
                           <th>#</th>
                           <th colspan='2'>COURSE CODE</th>
                           <th>TITLE</th>
                           <th>UNIT</th>
                           <th>FINAL</th>
                           <th>REEXAM</th>
                           <th>CREDIT</th>
                           <th>REMARKS</th>
                           <th>PRE-REQ</th>
                           <th>EQUIVALENT</th>
                           <th>YR STANDING</th>
                           <th>AY TERM TAKEN</th>
                           <th>YR LVL TAKEN</th>
                           <th>TAKEN FR OTHER</th>
                           <th>DT ENTERED</th>
                           <th>DT POSTED</th>
                        </tr></thead><tbody>";
   
   $xfcss = $this->colorcoding('Footer');						
   
   usort($row,'sort_object');
		 
   foreach($row as $rs){
    $xindexid  = property_exists($rs,'IndexID')? $rs->IndexID : 0;
    $xsortorder  = property_exists($rs,'SortOrder')? $rs->SortOrder : 0;
    $xstudentno  = property_exists($rs,'Studentno')? $rs->Studentno : 0;
    $xremarks  = property_exists($rs,'Remarks')? $rs->Remarks : '';
	$xsemester = property_exists($rs,'Semester')? $rs->Semester : '';
	$xid     = property_exists($rs,'CourseID')? $rs->CourseID : '';
	$xcode = property_exists($rs,'CourseCode')? $rs->CourseCode : '';
	$xtitle = property_exists($rs,'CourseTitle')? $rs->CourseTitle : '';
        $xtitle = '';//((strlen($xtitle)>=20)?(substr($xtitle,0,20).' ...'):$xtitle);
	$xcunits = property_exists($rs,'CreditUnits')? $rs->CreditUnits : 0;
	$xfinal = property_exists($rs,'Final')? $rs->Final : '';
	$xreexam = property_exists($rs,'ReExam')? $rs->ReExam : '';
	$xctunits = property_exists($rs,'CreditTheUnit')? $rs->CreditTheUnit : 0;
	
	$prerequisitep  = property_exists($rs,'PREREQUISITEPASSED')? $rs->PREREQUISITEPASSED : 0;
	$prerequisite  = property_exists($rs,'PreRequisites')? $rs->PreRequisites : '';
	$equivalent  = property_exists($rs,'Equivalent')? $rs->Equivalent : '';
	$yearstanding  = property_exists($rs,'Equivalent')? $rs->YearStanding : 0;
	$aytermtaken  = property_exists($rs,'AcademicYearTermTaken')? $rs->AcademicYearTermTaken : '';
	$yrlvltaken  = property_exists($rs,'YearLevelTaken')? $rs->YearLevelTaken : 0;
	$takenfromother  = property_exists($rs,'TakenFromOtherSchool')? $rs->TakenFromOtherSchool : '';
	$dateentered  = property_exists($rs,'DateEntered')? $rs->DateEntered : '';
	$dateposted  = property_exists($rs,'DatePosted')? $rs->DatePosted : '';
	
	if($dateentered!='' && $dateentered!=0)
	{$dateentered  = date('Y/m/d H:i:s A',strtotime($dateentered));}
	
	if($dateposted!='' && $dateposted!=0){
	 $dateposted  = date('Y/m/d H:i:s A',strtotime($dateposted));
	}else{
	 $xfinal='';
	 $xreexam='';
	}
	
	$gradeidx  = property_exists($rs,'GradeIDX')? $rs->GradeIDX : '';
	$termid  = property_exists($rs,'TermID')? $rs->TermID : 0;
	$xstatus  = property_exists($rs,'Status')? $rs->Status : '';
	$yrtermid  = property_exists($rs,'YearTermID')? $rs->YearTermID : 0;
	$regtagid  = property_exists($rs,'RegTagID')? $rs->RegTagID : 0;
	$equivalentsubj  = property_exists($rs,'EquivalentSubject')? $rs->EquivalentSubject : 0;
	$yrtermstats  = property_exists($rs,'YearTermStatus')? $rs->YearTermStatus : 0;
	$curriculumpk  = property_exists($rs,'CurriculumPK')? $rs->CurriculumPK : 0;
	$equivalentmode  = property_exists($rs,'EquivalentMode')? $rs->EquivalentMode : 0;
	$electivesubject  = property_exists($rs,'ElectiveSubjects')? $rs->ElectiveSubjects : 0;
	$currentenrolled  = property_exists($rs,'CurrentlyEnrolled')? $rs->CurrentlyEnrolled : 0;
	$queryforequiv  = property_exists($rs,'QueryForEquivalent')? $rs->QueryForEquivalent : '';
	$combinedsubj  = property_exists($rs,'CombinedSubjects')? $rs->CombinedSubjects : 0;
	$historygrade  = property_exists($rs,'HistoryGrade')? $rs->HistoryGrade : 0;
	$offeredsubj  = property_exists($rs,'OfferedSubject')? $rs->OfferedSubject : 0;
	$grdhistory1  = property_exists($rs,'GradeHistory1')? $rs->GradeHistory1 : '';
	$grdhistory2  = property_exists($rs,'GradeHistory2')? $rs->GradeHistory2 : '';
	$equivvalues  = property_exists($rs,'EquivalentValues')? $rs->EquivalentValues : '';
	
	$totalcc  = property_exists($rs,'totalCC')? $rs->totalCC : 0;
	$totallecc  = property_exists($rs,'totalLectC')? $rs->totalLectC : 0;
	$totallabc  = property_exists($rs,'totalLabC')? $rs->totalLabC : 0;
	$totalcreditu  = property_exists($rs,'totalCreditU')? $rs->totalCreditU : 0;
	$totalsube  = property_exists($rs,'totalSubE')? $rs->totalSubE : 0;
	$totallece  = property_exists($rs,'totalLecE')? $rs->totalLecE : 0;
	$totallabe  = property_exists($rs,'totalLabE')? $rs->totalLabE : 0;
	$totalunite  = property_exists($rs,'totalUnitsE')? $rs->totalUnitsE : 0;
	$subjtoearn  = property_exists($rs,'SubjToEarn')? $rs->SubjToEarn : 0;
	$lectoearn  = property_exists($rs,'LecToEarn')? $rs->LecToEarn : 0;
	$labtoearn  = property_exists($rs,'LabToEarn')? $rs->LabToEarn : 0;
	$cretoearn  = property_exists($rs,'CreToEarn')? $rs->CreToEarn : 0;
	
	
	if($xctunits == 1)
	{$xctunits = $xcunits;}
	
	$xcss = $xcss = $this->colorcoding('');
	if($xremarks !='' && $xremarks !=' ')
	{$xcss = $this->colorcoding($xremarks);}
	elseif($xfinal !='')
	{$xcss = $this->colorcoding($xfinal);}
    
	
	$xicon =$this->gen_icons($xfinal,$xremarks,$offeredsubj);
	
	
    if($initial=='' || ($initial != '' && $initial != $xsemester))
	{
	 if($initial != '' && $initial != $xsemester)
	 {
      $xrow_span = $xrow_span + $xcount;
	  $xgroupdetail.= "<tr ".$xfcss.">
	                     <td></td>
                         <td>".$xcount." Subject(s)</td>
                         <td></td>
                         <td></td>
                         <td>".$this->putDecimal($xfoot_totalunit)."</td>
                         <td></td>
                         <td></td>
                         <td>".$this->putDecimal($xfoot_totalcunit)."</td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
						 <td></td>
						 <td></td>
						 <td></td>
                   </tr>";
	  $xdisplay .= str_replace("(xmarker)","rowspan='".$xrow_span."'",$xgroupdetail);
	  //initialize again
	  $xgroupdetail='';
	  $xrow_span = 1;
	  $initial = $xsemester;
	  $xfoot_totalunit = $xcunits;
      $xfoot_totalcunit = $xctunits;
	  $xcount = 1;	 
	 }
	
	$initial = $xsemester;
	$xfoot_totalunit = $xcunits;
    $xfoot_totalcunit = $xctunits;
	$xcount = 1;
    
	$xorder  = property_exists($rs,'sortOrder')? $rs->sortOrder : $xcount;
	$xorder  = $xorder!=0? $xorder : $xcount;
	$xtrim_semester = str_replace('-','<br><br>',$xsemester);
	
	$xgroupdetail = "<tr ".$xcss.">
                         <td (xmarker) class='bg-color-white' align='center'><b class='txt-color-black'>".$xtrim_semester."</b></td>
                         <td><small>".$xorder."</small></td>
						 <td><small>".$xcode."</small></td>
						 <td> ".$xicon."</td>
                         <td><small>".$xtitle."</small></td>
                         <td><small>".$xcunits."</small></td>
                         <td><small><b>".$xfinal."</b></small></td>
                         <td><small>".$xreexam."</small></td>
                         <td><small>".$xctunits."</small></td>
                         <td><small><b>".$xremarks."</b></small></td>
                         <td><small>".$prerequisite."</small></td>
						 <td><small>".$equivalent."</small></td>
						 <td><small>".$yearstanding."</small></td>
						 <td><small>".$aytermtaken."</small></td>
						 <td><small>".$yrlvltaken."</small></td>
						 <td><small>".$takenfromother."</small></td>
						 <td><small>".$dateentered."</small></td>
						 <td><small>".$dateposted."</small></td>
                   </tr>";
    }
	elseif($initial != '' && $initial == $xsemester)
    {
	$xfoot_totalunit = $xfoot_totalunit + $xcunits;
    $xfoot_totalcunit = $xfoot_totalcunit + $xctunits;
	$xcount = $xcount + 1;
    
	$xorder  = property_exists($rs,'sortOrder')? $rs->sortOrder : $xcount;
	$xorder  = $xorder!=0? $xorder : $xcount;
	
	$xgroupdetail.= "<tr ".$xcss.">
	                     <td><small>".$xorder."</small></td> 
						 <td><small>".$xcode."</small></td>
						 <td> ".$xicon."</td>
                         <td><small>".$xtitle."</small></td>
                         <td><small>".$xcunits."</small></td>
                         <td><small><b>".$xfinal."</b></small></td>
                         <td><small>".$xreexam."</small></td>
                         <td><small>".$xctunits."</small></td>
                         <td><small><b>".$xremarks."</b></small></td>
                         <td><small>".$prerequisite."</small></td>
						 <td><small>".$equivalent."</small></td>
						 <td><small>".$yearstanding."</small></td>
						 <td><small>".$aytermtaken."</small></td>
						 <td><small>".$yrlvltaken."</small></td>
						 <td><small>".$takenfromother."</small></td>
						 <td><small>".$dateentered."</small></td>
						 <td><small>".$dateposted."</small></td>
                   </tr>";
    }
   }
   if($xgroupdetail!='')
   {
	$xrow_span = $xrow_span + $xcount;
	$xgroupdetail.= "<tr ".$xfcss.">
                         <td></td>
						 <td>".$xcount." Subject(s)</td>
                         <td></td>
						 <td></td>
                         <td>".$this->putDecimal($xfoot_totalunit)."</td>
                         <td></td>
                         <td></td>
                         <td>".$this->putDecimal($xfoot_totalcunit)."</td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
                         <td></td>
						 <td></td>
						 <td></td>
						 <td></td>
                   </tr>";
	$xdisplay .= str_replace("(xmarker)","rowspan='".$xrow_span."'",$xgroupdetail);
	
	$xgroupdetail='';
	$xrow_span = 1;
   }
   $xdisplay .="<tbody></table>";
   
   $xsummary="<table class='table table-striped' style='font-size:small;'>
	           <tbody>
	           <tr><td><b>Total Course in Curriculum:</b></td><td>".$totalcc."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lecture Units:</b></td><td>".$totallecc."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Laboratory Units:</b></td><td>".$totallabc."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Credit Units:</b></td><td>".$totalcreditu."</td></tr>
			   
	           <tr><td><b>Total Credited Course:</b></td><td>".$totalsube."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lecture Units Earned:</b></td><td>".$totallece."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lab. Units Earned:</b></td><td>".$totallabe."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Credit Units Earned:</b></td><td>".$totalunite."</td></tr>
			   
	           <tr><td><b>Total Credit Course to Earn:</b></td><td>".$subjtoearn."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lecture Units to Earn:</b></td><td>".$lectoearn."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Lab. Units to Earn:</b></td><td>".$labtoearn."</td></tr>
	           <tr><td style='text-indent:20px;'><b>Total Credit Units  to Earn:</b></td><td>".$cretoearn."</td></tr>
	           </tbody>
			   </table>";
   
   
  }
  return array($xdisplay,$xsummary);
 }
 
 function generate_legend(){
  $display='';
  $keys = array('Blank','Failed','Passed','Incomplete','Dropped','Unofficially Dropped','Conditional Failure','Withdrawal','Unauthorized Withdrawal','No Grade','Leave of Absences','No Credit','Audit');
  $display='';
  foreach($keys as $i)
  {
   $xcss = $this->colorcoding($i,1);
   if($i=='Blank')
   {$display.='<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><span class="fa fa-square-o"></span> <small><b class="txt-color-black">'.$i.'</b></small></div>';}
   elseif($i=='Unofficially Dropped' || $i=='Unauthorized Withdrawal')
   {$display.='<div class="col-xs-12 col-sm-12"><span '.$xcss.' class="fa fa-square"></span> <small><b class="txt-color-black">'.$i.'</b></small></div>';}
   else
   {$display.='<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><span '.$xcss.' class="fa fa-square"></span> <small><b class="txt-color-black">'.$i.'</b></small></div>';}
  }
  $display.='<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><span class="fa fa-lock" style="Color:#CC9900"></span><small><b class="txt-color-black"> Grade Posted</b></small></div>';
  $display.='<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><span class="fa fa-check-circle" style="Color:Green"></span></span><small><b class="txt-color-black"> Subject Offered</b></small></div>';
  return $display;
 } 
 
 
 function putDecimal($value){
     if(is_numeric($value))
	 {$xval = number_format($value,2);
	  return $xval;}
	 else
	 {
	  if($value=='' || $value==' ')
	  {return 0.00;}
	  else
	  {return $value;}
	 }
 }
 
 function colorcoding($value='',$lgnd=0){
  if($value == '' || $value == ' ' || $value=='Blank')
  {
   if($lgnd==0)
   {return "class='bg-color-white'";}
   elseif($lgnd==1)
   {return "style='background-color:black;color:white;'";}
  }
  elseif($value == 'Footer' || $value == 'footer')
  {
   return "class='bg-color-blueLight' style='font-size:8pt;'";
  }
  elseif($value == 'Failed' || $value == 'failed'|| $value== 'FAIL' || $value >= 5)
  {
   if($lgnd==0)
   {return "class='bg-color-red' style='color:white;'";}
   elseif($lgnd==1)
   {return "style='color: #a90329;'";}
  }
  elseif($value == 'Passed' || $value == 'passed'|| $value== 'PASS' || $value== 'CRD' || $value== 'Credited' || ($value <= 3 && $value > 0 ))
  {
   if($lgnd==0)
   {return "style='background-color: #75FF75;'";}
   elseif($lgnd==1)
   {return "style='color: #75FF75;'";}
  }
  elseif($value == 'Incomplete' || $value == 'incomplete'|| $value== 'INC' || ($value <= 4 && $value > 3))
  {
   if($lgnd==0)
   {return "style='background-color:#CCCCCC;'";}
   elseif($lgnd==1)
   {return "style='color: #CCCCCC;'";}
  }
  elseif($value == 'Dropped' || $value == 'dropped'|| $value== 'DRP')
  {
   if($lgnd==0)
   {return "class='bg-color-orange'";}
   elseif($lgnd==1)
   {return "style='color: orange;'";}
  }
  elseif($value == 'Unofficially Dropped' || $value == 'unofficially dropped'|| $value== 'UD')
  {
   if($lgnd==0)
   {return "class='bg-color-orangeDark'";}
   elseif($lgnd==1)
   {return "style='color: #a57225;'";}
  }
  elseif($value == 'Conditional Failure' || $value == 'conditional failure'|| $value== 'CF')
  {
   if($lgnd==0)
   {return "style='background-color:#FF00FF;'";}
   elseif($lgnd==1)
   {return "style='color:#FF00FF;'";}
  }
  elseif($value == 'Withdrawal' || $value == 'withdrawal'|| $value== 'W')
  {
   if($lgnd==0)
   {return "style='background-color:#CC66FF;'";}
   elseif($lgnd==1)
   {return "style='color:#CC66FF;'";}
  }
  elseif($value == 'Unauthorized Withdrawal' || $value == 'unauthorized withdrawal'|| $value== 'UW')
  {
   if($lgnd==0)
   {return "style='background-color:#FF9147;'";}
   elseif($lgnd==1)
   {return "style='color:#FF9147;'";}
  }
  elseif($value == 'No Grade' || $value == 'no grade'|| $value== 'NoGRDE')
  {
   if($lgnd==0)
   {return "style='background-color:#FFFF66;'";}
   elseif($lgnd==1)
   {return "style='color:#FFFF66;'";}
  }
  elseif($value == 'Leave of Absences' || $value == 'leave of absences'|| $value== 'LOA')
  {
   if($lgnd==0)
   {return "style='background-color:black;color:white;'";}
   elseif($lgnd==1)
   {return "style='color:black;'";}
  }
  elseif($value == 'No Credit' || $value == 'No Credit'|| $value== 'NC' || $value== 'Not Credited')
  {
   if($lgnd==0)
   {return "class='bg-color-pink'";}
   elseif($lgnd==1)
   {return "style='color:pink'";}
  }
  elseif($value == 'Audit' || $value == 'audit'|| $value== 'Aud')
  {
   if($lgnd==0)
   {return "style='background-color:#9900FF;'";}
   elseif($lgnd==1)
   {return "style='color:#9900FF;'";}
  }
  elseif($value == '* UNPOSTED' || $value == '* Unposted'|| $value== '* unposted'|| $value== 'unposted')
  {
   if($lgnd==0)
   {return "style='background-color:#F0F0F0;'";}
   elseif($lgnd==1)
   {return "style='color:#F0F0F0;'";}
  }
 }
 
 function gen_icons($final,$remarks,$offered){
  if(($final=='')&&($remarks=='') &&($offered==''||$offered=='0'||$offered==0))
  {return "";}
  
  if(($final!=''||$final=='')&&($remarks==''||($remarks!='' && $remarks!='Passed' && $remarks!='PASSED' && $remarks!='Credited' && $remarks!='* UNPOSTED')) &&($offered!='' && $offered!='0' && $offered!=0))
  {return "<span class='fa fa-check-circle' style='Color:Green'></span>";}
  
  
  if(($final!='')&&($remarks!='')&&($remarks!='*UNPOSTED')&&($remarks!='*unposted') &&($offered!='' || $offered!='0' || $offered!=0))
  {return "<span class='fa fa-lock' style='Color:#CC9900'></span>";}
 }
 
 function get_StudCurriculum($stdno='')
 {
  $this->db->reconnect();
  $str_query = "SELECT c.IndexID          as CurriculumID,
					   c.CurriculumCode   as CurriculumCode,
					   c.CurriculumDesc   as CurriculumDesc,
					   c.ProgramID        as ProgID,
					   c.MajorID          as MajorID,
					   cd.IndexID         as CurriculumIndexID,
					   cd.YearTermID      as YearTermID,
					   cd.YearStandingID  as YearStandingID,
					   yt.YearTermDesc    as YearTerm,
					   cd.SortOrder       as SortOrder,
					   cd.SubjectID       as SubjectID,
					   cd.SubjectElective as SubjectElective,
					   cd.SubjectMajor    as SubjectMajor,
					   s.SubjectCode      as SubjectCode,
					   s.SubjectTitle     as SubjectTitle,
					   s.AcadUnits        as AcadUnits,
					   s.LabUnits         as LabUnits,
					   s.CreditUnits      as CreditUnits,
					   pre.SubjectID      as PreRequisiteID,
					   equi.SubjectID     as EquivalentID, 
					   s.IsNonAcademic    as IsNonAcademic,
					   dbo.fn_sisConfigNetworkTermID() as CurrentTermID,
					   std.CampusID       as CurrentCampusID,
					   ISNULL(dbo.fn_getAutoYearLevel(std.StudentNo), 0) as AdvisedYearLevel,
					   (SELECT ISNULL(Value,0) FROM ES_Configuration WHERE [Key] = 'AssumeUnpostedGradesPassed' AND Section = 'Registration') AS AssumeUnpostedPassed,
					   dbo.fn_sisWithinAdvisingPeriod(dbo.fn_sisConfigNetworkTermID(),std.StudentNo) as WithinAdvisingPeriod,
					   dbo.fn_sisWithinEnrollmentPeriod(dbo.fn_sisConfigNetworkTermID(),std.StudentNo) as WithinEnrollmentPeriod
              FROM ES_Students std 
        INNER JOIN ES_Curriculums c ON std.CurriculumID=c.IndexID
        INNER JOIN ES_CurriculumDetails cd  ON c.IndexID=cd.CurriculumID 
        INNER JOIN ES_Subjects s ON cd.SubjectID=s.SubjectID
		LEFT  JOIN ES_PreRequisites pre ON cd.IndexID=pre.CurriculumIndexID AND pre.Options='Pre-Requisite'
        LEFT  JOIN ES_PreRequisites equi ON cd.IndexID=equi.CurriculumIndexID AND equi.Options='Equivalent'
		LEFT  JOIN ES_YearTerms yt ON cd.YearTermID = yt.IndexID
        WHERE std.StudentNo='".$stdno."'
		ORDER BY YearTermID, SortOrder";
  $result = $this->db->query($str_query);
  if($result)
	 return $result->result();
  else
	 return false;
 }
 
 function get_Equivalents($stdno='')
 {
  $this->db->reconnect();
  $str_query = "SELECT s.SubjectID, 
                       s.SubjectCode, 
					   s.SubjectTitle 
		          FROM ES_Students std 
            INNER JOIN ES_Curriculums c ON std.CurriculumID=c.IndexID
            INNER JOIN ES_CurriculumDetails cd  ON c.IndexID=cd.CurriculumID 
		    INNER JOIN ES_PreRequisites equi ON cd.IndexID=equi.CurriculumIndexID AND equi.Options='Equivalent'
            INNER JOIN ES_Subjects s ON equi.SubjectID=s.SubjectID
				 WHERE std.StudentNo='".$stdno."'";
  $result = $this->db->query($str_query);
  if($result)
	 return $result->result();
  else
	 return false; 
 }
 
 function get_SubjectsInBlockSection($termid=0,$campus=1,$curriculum=0,$yrlvl=0)
 {
  $this->db->reconnect();
  $str_query = "SELECT cs.SubjectID,
                       cs.SectionID,
					   cs.ScheduleID
                  FROM ES_ClassSchedules AS cs
                 WHERE cs.SectionID = (SELECT TOP 1 c.SectionID FROM ES_ClassSections AS c 
				                        WHERE c.TermID=".$termid."
                                              AND c.CampusID = ".$campus."
                                              AND c.CurriculumID = ".$curriculum."
                                              AND c.YearLevelID = ".$yrlvl."
                                              AND c.IsDissolved = 0)";
	 
  $result = $this->db->query($str_query);
  return (($result)? $result->result() : false);
 }
 
 function get_StudGrades($stdno='')
 {
  $this->db->reconnect();
  $str_query = "SELECT GradeIDX,
						 g.ScheduleID,
						 g.SubjectID,
						 sub.SubjectCode,
						 sub.SubjectTitle,
						 g.MajorID,
						 ProgMajorID,
						 ProgClassID,
						 Midterm,
						 Final,
						 ReExam,
						 g.Remarks,
						 g.FinalRemarks,
						 g.RegTagID,
						 EquivalentSubjectID,
						 EquivalentMode,
						 NotCredited,
						 CreditedFromOtherSchool,
						 CurriculumSubjectID,
						 CurriculumSubjectIndexID,
						 GSFormat,
						 UnitXGrade,
						 g.YearLevelID,
						 yr.YearLevel,
						 g.RegID,
						 g.TermID as TermID,
						 dbo.fn_AcademicYearTerm(g.TermID) as AcademicYearTermTaken,
						 g.LastModifiedDate as DateEntered,
						 g.DatePosted as DatePosted,
						 NULL as ValidationDate 
					FROM ES_Students s 
			   LEFT JOIN ES_Grades AS g on s.StudentNo=g.StudentNo
			   LEFT JOIN ES_Subjects AS sub ON g.SubjectID=sub.SubjectID
			   LEFT JOIN ES_YearLevel AS yr ON yr.YearLevelID = g.YearLevelID
				   WHERE s.StudentNo='".$stdno."'
			   UNION ALL
				  SELECT GradeIDX,
						 cs.ScheduleID,
						 cs.SubjectID,
						 sub.SubjectCode,
						 sub.SubjectTitle,
						 g.MajorID,
						 ProgMajorID,
						 ProgClassID,
						 Midterm,
						 Final,
						 ReExam,
						 g.Remarks,
						 g.FinalRemarks,
						 rd.RegTagID,
						 EquivalentSubjectID,
						 EquivalentMode,
						 NotCredited,
						 CreditedFromOtherSchool,
						 CurriculumSubjectID,
						 CurriculumSubjectIndexID,
						 GSFormat,
						 UnitXGrade,
						 r.YearLevelID,
						 yr.YearLevel,
						 r.RegID,
						 r.TermID as TermID,
						 dbo.fn_AcademicYearTerm(r.TermID) as AcademicYearTermTaken,
						 g.LastModifiedDate as DateEntered,
						 g.DatePosted as DatePosted, 
						 r.ValidationDate
					FROM ES_Students s 
			  INNER JOIN ES_Registrations as r ON s.StudentNo=r.StudentNo 
			  INNER JOIN ES_RegistrationDetails as rd ON r.RegID=rd.RegID 
			  INNER JOIN ES_ClassSchedules as cs ON rd.ScheduleID=cs.ScheduleID 
			  INNER JOIN ES_Subjects  AS sub ON cs.SubjectID=sub.SubjectID
			   LEFT JOIN ES_Grades g on sub.SubjectID=g.SubjectID AND s.StudentNo=g.StudentNo
			   LEFT JOIN ES_YearLevel AS yr ON yr.YearLevelID = r.YearLevelID
				   WHERE s.StudentNo='".$stdno."' AND GradeIDX IS NULL";
  $result = $this->db->query($str_query);
  if($result)
	 return $result->result();
  else
	 return false;
 }
 
 function get_StudOtherGrades($stdno='')
 {
  $this->db->reconnect();
  $str_query = "SELECT GradeIDX,
					   0 as ScheduleID,
					   gd.EquivalentSubjectID as SubjectID,
					   sub.SubjectCode,
					   sub.SubjectTitle,
					   0 as MajorID,
					   0 as ProgMajorID,
					   0 as ProgClassID,
					   0 as Midterm,
					   gd.Final,
					   gd.ReExam,
					   gd.Remarks,
					   gd.Remarks as FinalRemarks,
					   0 as RegTagID,
					   '' as EquivalentSubjectID,
					   1 as EquivalentMode,
					   0 as NotCredited,
					   1 as CreditedFromOtherSchool,
					   EquivalentCurriculumID as CurriculumSubjectID,
					   gd.EquivalentSubjectID as CurriculumSubjectIndexID,
					   0 as GSFormat,
					   0 as UnitXGrade,
					   0 as YearLevelID,
					   '' as YearLevel,
					   0 as RegID,
					   gd.TermID as TermID,
					   dbo.fn_AcademicYearTerm(gd.TermID) as AcademicYearTermTaken,
					   gd.LastModifiedDate as DateEntered,
					   g.EncodedDate as DatePosted,
					   NULL as ValidationDate 
				  FROM ES_Students s 
			INNER JOIN ES_Grades_FromOtherSchool g ON s.StudentNo=g.StudentNo
			INNER JOIN ES_Grades_FromOtherSchoolDetails gd ON g.KeyID=gd.KeyID
			LEFT OUTER JOIN ES_Subjects  AS sub ON gd.EquivalentSubjectID = sub.SubjectID
				 WHERE s.StudentNo='".$stdno."'";
  $result = $this->db->query($str_query);
  if($result)
	 return $result->result();
  else
	 return false;
 }
 
 function get_FinalRemarks()
 {
  $this->db->reconnect();
  $str_query = "SELECT Remarks,
					   RemarkCode,
					   CreditTheUnit
				  FROM ES_GradeFinalRemark";
  $result = $this->db->query($str_query);
  if($result)
  {	  
   foreach($result->result() as $rs)
   {
	$remarks = ((property_exists($rs,'Remarks'))?$rs->Remarks:'');
	$code    = ((property_exists($rs,'RemarkCode'))?$rs->RemarkCode:'');   
	$credit  = ((property_exists($rs,'CreditTheUnit'))?$rs->CreditTheUnit:0);   
	if($remarks!='')
	{
	 $this->finalremark[$remarks] = array('Remarks'=>$remarks,'Code'=>$code,'Credit'=>$credit);	
	}	
   }
  }
  else
	 return false; 
 }
 
 function exec_Evaluation($stdno='',$trial=false)
 {
  log_message('error','Evaluating:'.$stdno);
  $output = array();   
  $this->get_FinalRemarks();	 
  $tmp_curr    = $this->get_StudCurriculum($stdno);
  $tmp_offered = $this->get_OfferedSubj($stdno);
  $i = 1;
  if($tmp_curr)
  {
   foreach($tmp_curr as $curr)
   {
	$this->curriculumid = ((property_exists($curr,'CurriculumID'))? $curr->CurriculumID : 0);
	$this->progid       = ((property_exists($curr,'ProgID'))? $curr->ProgID : 0);   
	$this->majorid      = ((property_exists($curr,'MajorID'))? $curr->MajorID : 0);   
	$this->termid       = ((property_exists($curr,'CurrentTermID'))? $curr->CurrentTermID : 0);
	$this->campusid     = ((property_exists($curr,'CurrentCampusID'))? $curr->CurrentCampusID : 0);
	$this->yrlvlid      = ((property_exists($curr,'AdvisedYearLevel'))? $curr->AdvisedYearLevel : 0);   
    $this->assume       = ((property_exists($curr,'AssumeUnpostedPassed'))? $curr->AssumeUnpostedPassed : 0);   
    $this->withinadv    = ((property_exists($curr,'WithinAdvisingPeriod'))? $curr->WithinAdvisingPeriod : 0);   
    $this->withinenr    = ((property_exists($curr,'WithinEnrollmentPeriod'))? $curr->WithinEnrollmentPeriod : 0);   
    $curr_id            = ((property_exists($curr,'CurriculumIndexID'))? $curr->CurriculumIndexID : 0);   
    if($curr_id>0)
    {
	 if(!array_key_exists($curr_id,$this->curriculum))
     {
	  $data = array();
	  $data['CurriculumID']         = ((property_exists($curr,'CurriculumID'))? $curr->CurriculumID : 0);   
	  $data['CurriculumIndexID']    = ((property_exists($curr,'CurriculumIndexID'))? $curr->CurriculumIndexID : 0);   
	  $data['YearTermID']           = ((property_exists($curr,'YearTermID'))? $curr->YearTermID : 0);   
	  $data['YearStandingID']       = ((property_exists($curr,'YearStandingID'))? $curr->YearStandingID : 0);   
	  $data['YearTerm']             = ((property_exists($curr,'YearTerm'))? $curr->YearTerm : 0);   
	  $data['SortOrder']            = $i;   
	  $data['SubjectID']            = ((property_exists($curr,'SubjectID'))? $curr->SubjectID : 0);   
	  $data['SubjectCode']          = ((property_exists($curr,'SubjectCode'))? $curr->SubjectCode : 0);   
	  $data['SubjectTitle']         = ((property_exists($curr,'SubjectTitle'))? $curr->SubjectTitle : 0);   
	  $data['SubjectMajor']         = ((property_exists($curr,'SubjectMajor'))? $curr->SubjectMajor : 0);   
	  $data['SubjectElective']      = ((property_exists($curr,'SubjectElective'))? $curr->SubjectElective : 0);   
	  $data['LecUnits']             = ((property_exists($curr,'AcadUnits'))? $curr->AcadUnits : 0);   
	  $data['LabUnits']             = ((property_exists($curr,'LabUnits'))? $curr->LabUnits : 0);   
	  $data['CreditUnits']          = ((property_exists($curr,'CreditUnits'))? $curr->CreditUnits : 0);
      $data['IsNonAcademic']        = ((property_exists($curr,'IsNonAcademic'))? $curr->IsNonAcademic : 0);
      $data['CreditTheUnit']	    = 0;
	  $data['Final']                = '';
	  $data['ReExam']               = '';
	  $data['Remarks']              = '';
	  $data['PreRequisites']        = '';
	  $data['PreRequisitePassed']   = 0;
	  $data['Equivalent']           = '';
	  $data['YearLevelTaken']       = '';
	  $data['TermID']               = 0;
	  $data['AcademicYearTermTaken']= '';
	  $data['TakenFromOtherSchool'] = 0;
	  $data['DateEntered']          = NULL;
	  $data['DatePosted']           = NULL;
	  $data['GradeIDX']             = 0;
	  $data['OfferedSubject']       = 0;
	  
	  if($data['YearTermID']==22 || strtoupper($data['YearTerm'])=='ELECTIVE COURSE')
	  {
	   $this->iselective[$data['SubjectID']]=1;
	  }	  
	  
	  $this->subjects[$data['SubjectID']]=array('ID'=>$data['SubjectID'],'Code'=>$data['SubjectCode'],'Title'=>$data['SubjectTitle'],'InCurriculum'=>1); 
	  $this->curriculum[$curr_id]=$data;
	  $i++;
	 }		 
	 $this->add_PreRequisite($curr);
	 $this->add_Equivalent($curr);
	}		
   }
   
   $tmp_equiv = $this->get_Equivalents($stdno);
   if($tmp_equiv)
   {
	foreach($tmp_equiv as $rs)
    {
	 $data = array();
	 $data['SubjectID']    = ((property_exists($rs,'SubjectID'))? $rs->SubjectID : 0);   
	 $data['SubjectCode']  = ((property_exists($curr,'SubjectCode'))? $rs->SubjectCode : 0);   
	 $data['SubjectTitle'] = ((property_exists($curr,'SubjectTitle'))? $rs->SubjectTitle : 0);   
	 $this->subjects[$data['SubjectID']]=array('ID'=>$data['SubjectID'],'Code'=>$data['SubjectCode'],'Title'=>$data['SubjectTitle']); 
	}	
   }	   
   
   $tmp_grade = $this->get_StudGrades($stdno);
   if($tmp_grade)
   {
	$this->add_Grades($tmp_grade);
   }	   
   
   $tmp_othergrade = $this->get_StudOtherGrades($stdno);
   if($tmp_othergrade)
   {
	$this->add_Grades($tmp_othergrade);
   }	   
   
   $inblock = $this->get_SubjectsInBlockSection($this->termid,$this->campusid,$this->curriculumid,$this->yrlvlid);
   if($inblock)
   { 
     foreach($inblock as $rs)
	 {
	   $data=array();
	   $data['SubjectID']  = ((@property_exists($rs,'SubjectID'))?($rs->SubjectID):0); 
	   $data['SectionID']  = ((@property_exists($rs,'SectionID'))?($rs->SectionID):0); 
	   $data['ScheduleID'] = ((@property_exists($rs,'ScheduleID'))?($rs->ScheduleID):0); 
	   if($data['SubjectID']!=0)
	   { 
	    $this->insection[$data['SubjectID']] = $data; 
	   }	 
	 } 
   }	   
   
   foreach($this->prerequisite as $k=>$arr)
   {
	 foreach($arr as $prereq => $passed)
     {
	  $subj_grade  = ((array_key_exists($prereq,$this->grades))? $this->grades[$prereq] : false);
	  $equiv_grade =((array_key_exists($prereq,$this->equivalent))?$this->equivalent[$prereq]:false); 
	  if($subj_grade)
	  {	  
	   $this->prerequisite[$k][$prereq] = $this->IsCredited($subj_grade);
	  }
	  
	  if($equiv_grade)
	  {
	   foreach($equiv_grade as $equiv_id => $val)
	   {
		$xsubj_grade  = ((@array_key_exists($equiv_id,$this->grades))? $this->grades[$equiv_id] : false);
	    $this->prerequisite[$k][$prereq] = (($xsubj_grade && $this->prerequisite[$k][$prereq]==0)? $this->IsCredited($xsubj_grade) : $this->prerequisite[$k][$prereq]);
	   }	  
	  }	  
	 }	 
   }
   
   if(count($this->felective)>0){
	 foreach($this->felective as $k=>$d){
		$indx = count($this->elective);
        $this->elective[$indx]=$d;		
	 }  
   }
   /**/
   $elect_indx = 0;
   foreach($this->curriculum as $curr_subjid=>$data)
   {
	 if($data['SubjectMajor']==1 && $data['SubjectElective']==1)
	 {
	   if(array_key_exists($elect_indx,$this->elective))
	   {
		$subjid = $this->elective[$elect_indx];
	    $elect_indx++;   
	   }	
       else	   
	    $subjid = $data['SubjectID'];
	   
	   $isgraded = $this->getSubjGrade($curr_subjid,$subjid);
	 //$this->curriculum[$curr_subjid]['SubjectCode']=$this->curriculum[$curr_subjid]['SubjectCode'].'[xELEC]';
	 }
     else	 
	  $isgraded = $this->getSubjGrade($curr_subjid,$data['SubjectID']);
	 
	 $ischeck  = $this->CheckPreRequisite($curr_subjid,$data['SubjectID']);
	 $addSum   = $this->generate_Summary($curr_subjid);
	 $isadv    = $this->isadvisable($curr_subjid);
   }
   
   if($trial)
   {
    echo '<pre>';
    print_r($this->elective);
    die();
   }
   
   $delete = $this-> delAcadEvaluation($stdno);
   if($delete)
   {	   
     foreach($this->curriculum as $curr_subjid=>$data)
     {
	  $data = array(
	           'Curriculum_IndexID'           => $data['CurriculumID'],
	           'SortOrder'                    => $data['SortOrder'],
	           'Semester'                     => $data['YearTerm'],
	           'StudentNo'                    => $stdno,
	           'CourseID'                     => $data['SubjectID'],
	           'CourseCode'                   => $data['SubjectCode'],
	           'CourseTitle'                  => str_replace("'","''",$data['SubjectTitle']),
	           'Final'                        => $data['Final'],
	           'ReExam'                       => $data['ReExam'],
	           'Remarks'                      => (($data['Remarks']==NULL)? '' :$data['Remarks']),
	           'CreditTheUnit'                => (($data['CreditTheUnit']==NULL)? 0 : $data['CreditTheUnit']),
	           'LectUnits'                    => (($data['LecUnits']==NULL)? 0 : $data['LecUnits']),
	           'LabUnits'                     => (($data['LabUnits']==NULL)? 0 : $data['LabUnits']),
	           'CreditUnits'                  => (($data['CreditUnits']==NULL)? 0 : $data['CreditUnits']),
	           'PreRequisitePassed'           => (($data['PreRequisitePassed']==NULL)? 0 : $data['PreRequisitePassed']),
	           'PreRequisites'                => (($data['PreRequisites']==NULL)? '' : $data['PreRequisites']),
	           'Equivalent'                   => (($data['Equivalent']==NULL)? '' : $data['Equivalent']),
	           'YearStanding'                 => (($data['YearStandingID']==NULL)? 0 : $data['YearStandingID']),
	           'AcademicYearTermTaken'        => (($data['AcademicYearTermTaken']==NULL)? '' : $data['AcademicYearTermTaken']),
	           'YearLevelTaken'               => (($data['YearLevelTaken']==NULL)? '' : $data['YearLevelTaken']),
	           'TakenFromOtherSchool'         => (($data['TakenFromOtherSchool']==NULL)? 0 : $data['TakenFromOtherSchool']),
	           'DateEntered'                  => $data['DateEntered'],
	           'DatePosted'                   => $data['DatePosted'],
			   'GradeIDX'                     => (($data['GradeIDX']==NULL)? 0 : $data['GradeIDX']),
			   'TermID'                       => (($data['TermID']==NULL)? 0 : $data['TermID']),
			   'ElectiveSubjects'             => (($data['SubjectElective']==1 && $data['SubjectMajor']==1)?1:0),
			   'CurrentlyEnroled'             => 0,
			   'OfferedSubject'               => (($data['OfferedSubject']==NULL)? 0 : $data['OfferedSubject']), //(($data['PreRequisitePassed']==1)? $data['OfferedSubject'] : 0),
	           'IsNonAcademic'                => (($data['IsNonAcademic']==NULL)? 0 : $data['IsNonAcademic']),
	           'CurriculumID'                 => (($data['CurriculumID']==NULL)? 0 : $data['CurriculumID']),
	           'YearTermID'                   => (($data['YearTermID']==NULL)? 0 : $data['YearTermID']),
	           'DateGenerated'                => date('Y-m-d h:i:00'),
			   
			   'TotalCourseInCurriculum'      => $this->totalcourse,
               'TotalLectUnitsInCurriculum'   => $this->totallecunit,
               'TotalLabUnitsInCurriculum'    => $this->totallabunit,
               'TotalCreditUnitsInCurriculum' => $this->totalcredit,
			   'TotalSubjectsEarned'          => $this->totalsubjearned,
			   'TotalLectUnitsEarned'         => $this->totallecearned,
			   'TotalLabUnitsEarned'          => $this->totallabearned,
			   'TotalCreditUnitsEarned'       => $this->totalcreditearn.' ('.(($this->totalcredit>0) ? number_format((($this->totalcreditearn/$this->totalcredit)*100),2) : '0').'%)',
			   'TotalSubjectsToEarn'          => $this->totalsubjto,
			   'TotalLectUnitsToEarn'         => $this->totallecto,
			   'TotalLabUnitsToEarn'          => $this->totallabto,
			   'TotalCreditUnitsToEarn'       => $this->totalcreditto.' ('.(($this->totalcredit>0) ? number_format((($this->totalcreditto/$this->totalcredit)*100),2) : '0').'%)',
			   );
	
	  $output[] = $data;		   		   
	  $result   = $this->insert_Evaluation($data); 		   
     }
	 //$this->db->insert_batch('sis_AcademicEvaluation',$output); <-- function is not existing in ODBC driver
    }
	
	if($this->isregistered==0){
		if((count($this->insection)>=count($this->advisable) && count($this->advisable)>0) && count($this->curriculum)>0){
		 $str_query = "DELETE FROM sis_StudentCurrentStatus WHERE StudentNo='".$stdno."'  
					   INSERT sis_StudentCurrentStatus(StudentNo,IsRegular,LastDateEvaluated) VALUES ('".$stdno."',1,GETDATE())";	
		}else{	
		 $str_query = "DELETE FROM sis_StudentCurrentStatus WHERE StudentNo='".$stdno."'  
					   INSERT sis_StudentCurrentStatus(StudentNo,IsRegular,LastDateEvaluated) VALUES ('".$stdno."',0,GETDATE())";	
		}	
	}else{
		 $str_query = "UPDATE sis_StudentCurrentStatus SET LastDateEvaluated=GETDATE() WHERE StudentNo='".$stdno."'";	
	}
	$exec = $this->db->query($str_query);
  }	  
  //return $this->curriculum;
  return $output;
 }
 
 function insert_Evaluation($data)
 {
   $this->db->reconnect();
   return $this->db->insert('sis_AcademicEvaluation', $data); 	 
 }
 
 function isadvisable($curr_subjid=0)
 {
  $subjid  = $this->curriculum[$curr_subjid]['SubjectID'];
  $offered = $this->curriculum[$curr_subjid]['OfferedSubject'];
  $prereq  = $this->curriculum[$curr_subjid]['PreRequisitePassed'];
  $yrlvl   = $this->curriculum[$curr_subjid]['YearStandingID'];
  
  if(@array_key_exists($subjid,$this->insection) && $offered>0 && $prereq==1 && $yrlvl==$this->yrlvlid)
  {
	$this->advisable[$subjid] = $this->insection[$subjid];  
  }	
  
  return true;
 }
 
 function generate_Summary($curr_subjid=0)
 {
   if($curr_subjid > 0 && array_key_exists($curr_subjid,$this->curriculum))
   {	   
    $data       = $this->curriculum[$curr_subjid];
	$yearterm   = $data['YearTerm'];
	$iscredited = $data['CreditTheUnit'];
    $lec        = floatval($data['LecUnits']);
    $lab        = floatval($data['LabUnits']);
    
	if(strtoupper($yearterm)!='ELECTIVE COURSES')
	{	
	 $this->totalcourse++;
	 $this->totallecunit = $this->totallecunit+$lec;
	 $this->totallabunit = $this->totallabunit+$lab;
	 $this->totalcredit  = $this->totalcredit+($lec+$lab);
	
	 if($iscredited==1)
	 {
	  $this->totalsubjearned++;
      $this->totallecearned  = $this->totallecearned+$lec;
      $this->totallabearned  = $this->totallabearned+$lab;
      $this->totalcreditearn = $this->totalcreditearn+($lec+$lab);
     }
     else if($iscredited==0)
	 {
	  $this->totalsubjto++;
      $this->totallecto    = $this->totallecto+$lec;
      $this->totallabto    = $this->totallabto+$lab;
      $this->totalcreditto = $this->totalcreditto+($lec+$lab);
	 }
    }	 
   }
   return true;
 }
 
 function CheckPreRequisite($curr_subjid=0,$subj_id=0)
 {
  if(@array_key_exists($subj_id,$this->prerequisite))
  {
    $noprereq = count($this->prerequisite[$subj_id]);
	$nopassed = array_sum($this->prerequisite[$subj_id]);
	$this->curriculum[$curr_subjid]['PreRequisitePassed']=(($noprereq==$nopassed)?1:0); 
	$this->curriculum[$curr_subjid]['PreRequisites']=$this->getDataImplode(',',array_keys($this->prerequisite[$subj_id]),'Code',$this->subjects);  
  }	
  else
  {
	$this->curriculum[$curr_subjid]['PreRequisites']='';  
	$this->curriculum[$curr_subjid]['PreRequisitePassed']=1;  
  }
  
  
  if(@array_key_exists($subj_id,$this->equivalent))
  {
   $this->curriculum[$curr_subjid]['Equivalent']=$this->getDataImplode(',',array_keys($this->equivalent[$subj_id],0),'Code',$this->subjects);    
  }
  return true;
 }
 
 function getDataImplode($glue=',',$key=array(),$str='Code',$from=array())
 {
  foreach($key as $i=>$id)
  {
   $key[$i] = ((@array_key_exists($id,$from))?$from[$id][$str]:$id);	  
  }
  
  return implode($glue,$key);  
 }
 
 function getSubjGrade($curr_subjid=0,$subj_id=0)
 {
  $final   = '';
  $reexam  = '';
  $remarks = '';
  $subj_grade  = ((array_key_exists($subj_id,$this->grades))? $this->grades[$subj_id] : false);
  $equiv_grade = ((array_key_exists($subj_id,$this->equivalent) && ($subj_grade['TermID']!=$this->termid))?$this->equivalent[$subj_id]:false); 
  if($subj_grade && ($subj_grade['DatePosted']!=NULL && $subj_grade['DatePosted']!=''))
  {	  
    $remarks   = $subj_grade['Remarks'];
	$credited  = ((array_key_exists($remarks,$this->finalremark))? $this->finalremark[$remarks]['Credit'] : 0); 
    $this->curriculum[$curr_subjid]['TermID']               = $subj_grade['TermID'];	
    $this->curriculum[$curr_subjid]['AcademicYearTermTaken']= $subj_grade['AcademicYearTermTaken'];	
    $this->curriculum[$curr_subjid]['YearLevelTaken']       = $subj_grade['YearLevel'];	
    $this->curriculum[$curr_subjid]['TakenFromOtherSchool'] = $subj_grade['CreditedFromOtherSchool'];	
	$this->curriculum[$curr_subjid]['GradeIDX']             = $subj_grade['GradeIDX'];
	$this->curriculum[$curr_subjid]['Final']                = $subj_grade['Final'];
	$this->curriculum[$curr_subjid]['ReExam']               = $subj_grade['ReExam'];
	$this->curriculum[$curr_subjid]['Remarks']              = $remarks;
	$this->curriculum[$curr_subjid]['CreditTheUnit']        = $credited;
	$this->curriculum[$curr_subjid]['DateEntered']          = $subj_grade['DateEntered'];
	$this->curriculum[$curr_subjid]['DatePosted']           = $subj_grade['DatePosted'];
	$this->curriculum[$curr_subjid]['OfferedSubject']       = (($this->termid>$subj_grade['TermID'] && $credited==0)?$this->checkifOffered($subj_id):0);		
  }else if($subj_grade && ($subj_grade['DatePosted']==NULL || $subj_grade['DatePosted']=='')){	  
    if($subj_grade['TermID']==$this->termid && $subj_grade['GradeIDX']==0)
	{
     if(array_key_exists('ValidationDate',$subj_grade))
	  $remarks   = (($subj_grade['ValidationDate']=='' || $subj_grade['ValidationDate']==NULL)?'*Registered':'*Enrolled'); 
     else
	  $remarks   = '';
  
     $this->isregistered = (($remarks=='*Registered' || $remarks=='*Enrolled' || $this->isregistered==1)?1:0);
	 $equiv_grade  = false;
	}
	else
	$remarks = '*UNPOSTED';
	
	$credited  = (($this->assume==1 && $this->withinadv==1 && $this->withinenr==0)? 1 : 0); 
    $this->curriculum[$curr_subjid]['TermID']               = $subj_grade['TermID'];	
    $this->curriculum[$curr_subjid]['AcademicYearTermTaken']= $subj_grade['AcademicYearTermTaken'];	
    $this->curriculum[$curr_subjid]['YearLevelTaken']       = $subj_grade['YearLevel'];	
    $this->curriculum[$curr_subjid]['TakenFromOtherSchool'] = $subj_grade['CreditedFromOtherSchool'];	
	$this->curriculum[$curr_subjid]['GradeIDX']             = $subj_grade['GradeIDX'];
	$this->curriculum[$curr_subjid]['Final']                = $subj_grade['Final'];
	$this->curriculum[$curr_subjid]['ReExam']               = $subj_grade['ReExam'];
	$this->curriculum[$curr_subjid]['Remarks']              = $remarks;
	$this->curriculum[$curr_subjid]['CreditTheUnit']        = $credited;
	$this->curriculum[$curr_subjid]['DateEntered']          = $subj_grade['DateEntered'];
	$this->curriculum[$curr_subjid]['DatePosted']           = $subj_grade['DatePosted'];
	$this->curriculum[$curr_subjid]['OfferedSubject']       = 0;		
  }
  else
  {
	$this->curriculum[$curr_subjid]['OfferedSubject']       = $this->checkifOffered($subj_id);		  
  }	  
  
  if($equiv_grade)
  {
	foreach($equiv_grade as $equiv_id => $val)
	{
     $xsubj_grade  = ((@array_key_exists($equiv_id,$this->grades))? $this->grades[$equiv_id] : false);
	 if($xsubj_grade && $xsubj_grade['DatePosted']!=NULL && $this->curriculum[$curr_subjid]['CreditTheUnit']==0)
	 {	  
	   $remarks   = $xsubj_grade['Remarks'];
	   $credited  = ((array_key_exists($remarks,$this->finalremark))? $this->finalremark[$remarks]['Credit'] : 0); 	
	   $this->curriculum[$curr_subjid]['TermID']               = $xsubj_grade['TermID'];
	   $this->curriculum[$curr_subjid]['AcademicYearTermTaken']= $xsubj_grade['AcademicYearTermTaken'];	
       $this->curriculum[$curr_subjid]['YearLevelTaken']       = $xsubj_grade['YearLevel'];	
	   $this->curriculum[$curr_subjid]['TakenFromOtherSchool'] = $xsubj_grade['CreditedFromOtherSchool'];	
	   $this->curriculum[$curr_subjid]['GradeIDX']             = $xsubj_grade['GradeIDX'];
	   $this->curriculum[$curr_subjid]['Final']                = $xsubj_grade['Final'];
	   $this->curriculum[$curr_subjid]['ReExam']               = $xsubj_grade['ReExam'];
	   $this->curriculum[$curr_subjid]['Remarks']              = $remarks;
	   $this->curriculum[$curr_subjid]['CreditTheUnit']        = $credited;
	   $this->curriculum[$curr_subjid]['DateEntered']          = $xsubj_grade['DateEntered'];
	   $this->curriculum[$curr_subjid]['DatePosted']           = $xsubj_grade['DatePosted'];
	   $this->curriculum[$curr_subjid]['OfferedSubject']       = (($this->termid>$xsubj_grade['TermID'] && $credited==0)?$this->checkifOffered($equiv_id):0);		
	 }  
	 else
	 {
	   $this->curriculum[$curr_subjid]['OfferedSubject']      = (($this->curriculum[$curr_subjid]['OfferedSubject']==0)? $this->checkifOffered($equiv_id) : $this->curriculum[$curr_subjid]['OfferedSubject']);		  
	 }	  
	}	  
  }
  
  return true; 
 }
 
 function IsCredited($grade=array())
 {
  $result = false;	 
  if($grade)
  {
	$dateposted = $grade['DatePosted']; 
	$notcredit  = $grade['NotCredited'];
	$remarks    = $grade['Remarks'];
	
	if($this->assume==1 && $this->withinadv==1 && $this->withinenr==0)
	  $credited =  1;
    else
      $credited = (($dateposted!=NULL && array_key_exists($remarks,$this->finalremark))? $this->finalremark[$remarks]['Credit'] : 0); 		
	
	return (($credited==1 && $notcredit==0)?1:0);		
  }	  
  return false;
 }
 
 function add_PreRequisite($obj=false)
 {
  if($obj)
  {
	$subjid   = ((property_exists($obj,'SubjectID'))?$obj->SubjectID:0);
	$prereqid = ((property_exists($obj,'PreRequisiteID'))?$obj->PreRequisiteID:0);  
	if($subjid>0 && $prereqid>0)
	{	
	 if(in_array($subjid,$this->prerequisite))
	 {
	  $this->prerequisite[$subjid]=array('Passed'=>0);	
	  $this->prerequisite[$subjid][$prereqid]=0;
	 }
     else
     {
	  $this->prerequisite[$subjid][$prereqid]=0;
	 }
    }	 
  }	  
 }
 
 function add_Equivalent($obj=false)
 {
  if($obj)
  {
	$subjid  = ((property_exists($obj,'SubjectID'))?$obj->SubjectID:0);
	$equivid = ((property_exists($obj,'EquivalentID'))?$obj->EquivalentID:0);  
	if($subjid>0 && $equivid>0)
	{	
	 if(!array_key_exists($subjid,$this->equivalent))
	 {
	  $this->equivalent[$subjid]=array();	
	  $this->equivalent[$subjid][$equivid]=0;
	 }
     else
     {
	  $this->equivalent[$subjid][$equivid]=0;
	 }
    }	 
  }	 
 }
 
 function add_Grades($tmp_grade)
 {
   if($tmp_grade)
   {
	 foreach($tmp_grade as $grad)  
	 {
	   $remarks  = '';
	   $xremarks = '';
	   $xtermid  = ((property_exists($grad,'TermID'))? $grad->TermID : '');   
       $gradeidx = ((property_exists($grad,'GradeIDX'))? $grad->GradeIDX : 0);   	 
	   $subjid   = ((property_exists($grad,'SubjectID'))? $grad->SubjectID : '');   
	   if($gradeidx>0 && $subjid>0)
       {
		$data = array();
		if(!array_key_exists($subjid,$this->grades))
		{	
		 $remarks  = ((property_exists($grad,'Remarks'))? $grad->Remarks : '');
		 $xremarks = ((property_exists($grad,'FinalRemarks'))? $grad->FinalRemarks : '');
		 $data['GradeIDX']                 = $gradeidx;
		 $data['SubjectID']                = $subjid;
		 $data['SubjectCode']              = ((property_exists($grad,'SubjectCode'))? $grad->SubjectCode : '');
		 $data['SubjectTitle']             = ((property_exists($grad,'SubjectCode'))? $grad->SubjectTitle : '');
		 $data['CurriculumSubjectID']      = ((property_exists($grad,'CurriculumSubjectID'))? $grad->CurriculumSubjectID : '');
		 $data['CurriculumSubjectIndexID'] = ((property_exists($grad,'CurriculumSubjectIndexID'))? $grad->CurriculumSubjectIndexID : '');
		 $data['Final']                    = ((property_exists($grad,'Final'))? $grad->Final : '');
		 $data['ReExam']                   = ((property_exists($grad,'ReExam'))? $grad->ReExam : '');
		 $data['Remarks']                  = (($xremarks!='')? $xremarks : $remarks);
		 $data['RegTagID']                 = ((property_exists($grad,'RegTagID'))? $grad->RegTagID : 0);
		 $data['EquivalentSubjectID']      = ((property_exists($grad,'EquivalentSubjectID'))? $grad->EquivalentSubjectID : 0);
		 $data['NotCredited']              = ((property_exists($grad,'NotCredited'))? $grad->NotCredited : 0);
		 $data['CreditedFromOtherSchool']  = ((property_exists($grad,'CreditedFromOtherSchool'))? $grad->CreditedFromOtherSchool : 0);
	     $data['GSFormat']                 = ((property_exists($grad,'GSFormat'))? $grad->GSFormat : 0);
	     $data['UnitXGrade']               = ((property_exists($grad,'UnitXGrade'))? $grad->UnitXGrade : 0);
	     $data['RegID']                    = ((property_exists($grad,'RegID'))? $grad->RegID : 0);
	     $data['TermID']                   = ((property_exists($grad,'TermID'))? $grad->TermID : 0);
	     $data['AcademicYearTermTaken']    = ((property_exists($grad,'AcademicYearTermTaken'))? $grad->AcademicYearTermTaken : '');
	     $data['YearLevelID']              = ((property_exists($grad,'YearLevelID'))? $grad->YearLevelID : 0);
	     $data['YearLevel']                = ((property_exists($grad,'YearLevel'))? $grad->YearLevel : '');
		 $data['DateEntered']              = ((property_exists($grad,'DateEntered'))? $grad->DateEntered : NULL);
		 $data['DatePosted']               = ((property_exists($grad,'DatePosted'))? $grad->DatePosted : NULL);
		 $data['ValidationDate']           = ((property_exists($grad,'ValidationDate'))? $grad->ValidationDate : NULL);
	     $this->grades[$subjid]=$data;
	    }
		else
		{
		 $prev_grad = $this->grades[$subjid];	
		 if((@array_key_exists($prev_grad['Remarks'],$this->finalremark) && $this->finalremark[$prev_grad['Remarks']]['Credit']==0))
		 {
		  $remarks  = ((property_exists($grad,'Remarks'))? $grad->Remarks : '');
		  $xremarks = ((property_exists($grad,'FinalRemarks'))? $grad->FinalRemarks : '');
		  $data['GradeIDX']                 = $gradeidx;
		  $data['SubjectID']                = $subjid;
		  $data['SubjectCode']              = ((property_exists($grad,'SubjectCode'))? $grad->SubjectCode : '');
		  $data['SubjectTitle']             = ((property_exists($grad,'SubjectCode'))? $grad->SubjectTitle : '');
		  $data['CurriculumSubjectID']      = ((property_exists($grad,'CurriculumSubjectID'))? $grad->CurriculumSubjectID : '');
		  $data['CurriculumSubjectIndexID'] = ((property_exists($grad,'CurriculumSubjectIndexID'))? $grad->CurriculumSubjectIndexID : '');
		  $data['Final']                    = ((property_exists($grad,'Final'))? $grad->Final : '');
		  $data['ReExam']                   = ((property_exists($grad,'ReExam'))? $grad->ReExam : '');
		  $data['Remarks']                  = (($xremarks!='')? $xremarks : $remarks);
		  $data['RegTagID']                 = ((property_exists($grad,'RegTagID'))? $grad->RegTagID : 0);
		  $data['EquivalentSubjectID']      = ((property_exists($grad,'EquivalentSubjectID'))? $grad->EquivalentSubjectID : 0);
		  $data['NotCredited']              = ((property_exists($grad,'NotCredited'))? $grad->NotCredited : 0);
		  $data['CreditedFromOtherSchool']  = ((property_exists($grad,'CreditedFromOtherSchool'))? $grad->CreditedFromOtherSchool : 0);
	      $data['GSFormat']                 = ((property_exists($grad,'GSFormat'))? $grad->GSFormat : 0);
	      $data['UnitXGrade']               = ((property_exists($grad,'UnitXGrade'))? $grad->UnitXGrade : 0);
	      $data['RegID']                    = ((property_exists($grad,'RegID'))? $grad->RegID : 0);
	      $data['TermID']                   = ((property_exists($grad,'TermID'))? $grad->TermID : 0);
	      $data['AcademicYearTermTaken']    = ((property_exists($grad,'AcademicYearTermTaken'))? $grad->AcademicYearTermTaken : '');
	      $data['YearLevelID']              = ((property_exists($grad,'YearLevelID'))? $grad->YearLevelID : 0);
	      $data['YearLevel']                = ((property_exists($grad,'YearLevel'))? $grad->YearLevel : '');
		  $data['DateEntered']              = ((property_exists($grad,'DateEntered'))? $grad->DateEntered : NULL);
		  $data['DatePosted']               = ((property_exists($grad,'DatePosted'))? $grad->DatePosted : NULL);
		  $data['ValidationDate']           = ((property_exists($grad,'ValidationDate'))? $grad->ValidationDate : NULL);
	      $data['Previous']                 = array('Final'=>$prev_grad['Final'],'ReExam'=>$prev_grad['ReExam'],'Remarks'=>$prev_grad['Remarks']);
	      $this->grades[$subjid]=$data;	 
		 }
         else
         {
		  $this->grades[$subjid]['Retaken'] = 1;	
		 }			 
		}
		
		if(@array_key_exists('SubjectID',$data) && @array_key_exists('CurriculumSubjectID',$data))
		{	
		 if(!array_key_exists($data['SubjectID'],$this->subjects))
		 {	
		  $this->subjects[$data['SubjectID']]=array('ID'=>$data['SubjectID'],'Code'=>$data['SubjectCode'],'Title'=>$data['SubjectTitle'],'InCurriculum'=>0);
	     }
		
		 if(($data['SubjectID']!=$data['CurriculumSubjectID'] && $data['SubjectID']>0 && $data['CurriculumSubjectID']>0))
		 {
		    $subjid  = $data['CurriculumSubjectID'];
			$equivid = $data['SubjectID'];  
			if($subjid>0 && $equivid>0)
			{	
			 if(!array_key_exists($subjid,$this->equivalent))
			 {
			  $this->equivalent[$subjid]=array();	
			  $this->equivalent[$subjid][$equivid]=1;
			 }
			 else
			 {
			  $this->equivalent[$subjid][$equivid]=1;
			 }
			}	
		 }
        
		}  
				
	   }
       else if($gradeidx==0 && $subjid>0)
       {
		 $remarks  = ((property_exists($grad,'Remarks'))? $grad->Remarks : '');
		 $xremarks = ((property_exists($grad,'FinalRemarks'))? $grad->FinalRemarks : '');
		 $data['GradeIDX']                 = 0;
		 $data['SubjectID']                = $subjid;
		 $data['SubjectCode']              = ((property_exists($grad,'SubjectCode'))? $grad->SubjectCode : '');
		 $data['SubjectTitle']             = ((property_exists($grad,'SubjectCode'))? $grad->SubjectTitle : '');
		 $data['CurriculumSubjectID']      = '';
		 $data['CurriculumSubjectIndexID'] = '';
		 $data['Final']                    = '';
		 $data['ReExam']                   = '';
		 $data['Remarks']                  = (($xremarks!='')? $xremarks : $remarks);
		 $data['RegTagID']                 = ((property_exists($grad,'RegTagID'))? $grad->RegTagID : 0);
		 $data['EquivalentSubjectID']      = 0;
		 $data['NotCredited']              = 0;
		 $data['CreditedFromOtherSchool']  = 0;
	     $data['GSFormat']                 = 0;
	     $data['UnitXGrade']               = 0;
	     $data['RegID']                    = ((property_exists($grad,'RegID'))? $grad->RegID : 0);
	     $data['TermID']                   = ((property_exists($grad,'TermID'))? $grad->TermID : 0);
	     $data['AcademicYearTermTaken']    = ((property_exists($grad,'AcademicYearTermTaken'))? $grad->AcademicYearTermTaken : '');
	     $data['YearLevelID']              = ((property_exists($grad,'YearLevelID'))? $grad->YearLevelID : 0);
	     $data['YearLevel']                = ((property_exists($grad,'YearLevel'))? $grad->YearLevel : '');
		 $data['DateEntered']              = NULL;
		 $data['DatePosted']               = NULL;
	     $this->grades[$subjid]=$data;
	   }	
       
       
	   if(array_key_exists($subjid,$this->iselective))
	   {
		 if($xremarks=='Failed'){
		  $indx = count($this->felective);
		  $this->felective[$indx]=$subjid; 
		 }else{
		  $indx = count($this->elective);
		  $this->elective[$indx]=$subjid; 
		 } 
	   }	 	   
	 }
   }
   return true;
 }
 
 function get_OfferedSubj($stdno='')
 {
  $result=false;
  $params = $this->get_OfferedSubj_Param($stdno); 
  if($params)
  {
    $termid    = ((property_exists($params,'TermID'))?$params->TermID:0);	  
    $progid    = ((property_exists($params,'ProgID'))?$params->ProgID:0);	  
    $currid    = ((property_exists($params,'CurriculumID'))?$params->CurriculumID:0);	  
    $campus    = ((property_exists($params,'CampusID'))?$params->CampusID:0);	  
    $withsched = ((property_exists($params,'WithScheduleOnly'))?$params->WithScheduleOnly:0);	  
	$this->db->reconnect();
	$query     = "SELECT subj.SubjectID,
		                 subj.ScheduleID,
		                 subj.Allowed,
		                 subj.Program,
		                 subj.Elective
				   FROM (SELECT CS.SubjectID,
								CS.ScheduleID,
								0 as Allowed,
								0 as Program,
								0 as Elective
						FROM    [ES_ClassSchedules] CS
						WHERE   CS.IsDissolved = 0
								AND LEN(CS.Sched_1) >= ".$withsched."
								AND CS.SectionID IN (
								SELECT  c.SectionID
								FROM    ES_ClassSections AS c
								WHERE   c.TermID = ".$termid."
										AND c.CampusID = ".$campus."
										AND c.ProgramID = ".$progid."
										AND c.CurriculumID = ".$currid."
										AND c.IsDissolved = 0 )
										AND (CS.SubjectID IN (
																SELECT SubjectID FROM ES_CurriculumDetails WHERE CurriculumID=".$currid."
																UNION ALL
																SELECT    P.SubjectID
																FROM      ES_Curriculums C
																		INNER JOIN ES_CurriculumDetails D ON C.IndexID = D.CurriculumID
																		RIGHT OUTER JOIN ES_PreRequisites P ON D.IndexID = P.CurriculumIndexID
																WHERE     P.Options = 'Equivalent' AND C.IndexID = ".$currid."
																UNION ALL
																SELECT    P.SubjectID
																FROM      ES_PreRequisites P
																WHERE     P.CurriculumIndexID IS NULL AND P.Options = 'Equivalent' 
																)
															)
						UNION ALL
						SELECT  
								c.SubjectID,
								a.ScheduleID,
								1 as Allowed,
								0 as Program,
								0 as Elective
						FROM    dbo.ES_ClassSections AS sc
								LEFT OUTER JOIN dbo.ES_ClassSchedules AS c ON sc.SectionID = c.SectionID
								LEFT OUTER JOIN dbo.ES_ClassSchedules_AllowedStudents AS a ON c.ScheduleID = a.ScheduleID
						WHERE   a.StudentNo = '".$stdno."'
								AND c.SubjectID IN (SELECT SubjectID FROM ES_CurriculumDetails WHERE CurriculumID=".$currid.")
								AND a.[Status] = 1
								AND sc.TermID = ".$termid."
								AND sc.CampusID = ".$campus."
								AND sc.CurriculumID=".$currid."
								AND sc.IsDissolved = 0
								AND c.IsDissolved = 0
								AND LEN(c.Sched_1) >= ".$withsched."
					
						UNION ALL
						SELECT  c.SubjectID,
								o.ScheduleID,
								0 as Allowed,
								1 as Program,
								0 as Elective
						FROM    dbo.ES_ClassSections AS sc
								LEFT OUTER JOIN dbo.ES_ClassSchedules AS c ON sc.SectionID = c.SectionID
								LEFT OUTER JOIN dbo.ES_ClassSchedules_AllowedOtherPrograms AS o ON c.ScheduleID = o.ScheduleID
						WHERE   c.SubjectID  IN (SELECT SubjectID FROM ES_CurriculumDetails WHERE CurriculumID=".$currid.")
								AND o.ProgramID = ".$progid."
								AND sc.TermID = ".$termid."
								AND sc.CampusID = ".$campus."
								AND sc.CurriculumID=".$currid."
								AND sc.IsDissolved = 0
								AND c.IsDissolved = 0
								AND LEN(c.Sched_1) >= ".$withsched."
				
						UNION ALL
						SELECT cur.SubjectID,
								CASE WHEN cur.SubjectElective = 1 AND cur.SubjectMajor = 1
									THEN cur.SubjectID
									ELSE 0
								END AS ScheduleID,
								0 as Allowed,
								0 as Program,
								1 as Elective
						FROM  ES_CurriculumDetails AS cur
						WHERE  cur.YearTermID = 22
								AND cur.SubjectID IN (
								SELECT  cs.SubjectID
								FROM    dbo.ES_ClassSchedules AS cs
								WHERE   cs.TermID = ".$termid."
										AND LEN(cs.Sched_1) >= ".$withsched."
										AND cs.SectionID IN (
										SELECT  c.SectionID
										FROM    dbo.ES_ClassSections AS c
										WHERE   c.SectionID = cs.SectionID
												AND c.CampusID = ".$campus."
												AND c.ProgramID = ".$progid."
												AND c.CurriculumID = cur.CurriculumID
												AND c.IsDissolved = 0 )
										AND cs.IsDissolved = 0 )
								AND cur.CurriculumID = ".$currid."
								) as subj";
	$result = $this->db->query($query);  		 
	if($result)
	{
	  foreach($result->result() as $rs)
	  {
		$subjid   = ((property_exists($rs,'SubjectID'))?$rs->SubjectID:0);	  
		$schedid  = ((property_exists($rs,'ScheduleID'))?$rs->ScheduleID:0);	  
		$allow    = ((property_exists($rs,'Allowed'))?$rs->Allowed:0);	  
		$program  = ((property_exists($rs,'Program'))?$rs->Program:0);	  
		$elective = ((property_exists($rs,'Elective'))?$rs->Elective:0);
        
		if($subjid>0 && $schedid>0)
		{	
		 if(!array_key_exists($subjid,$this->offeredsubj))
		   $this->offeredsubj[$subjid]=array($schedid=>array('Allowed'=>$allow,'Program'=>$program,'Elective'=>$elective));		
         else	  		
           $this->offeredsubj[$subjid][$schedid] = array('Allowed'=>$allow,'Program'=>$program,'Elective'=>$elective);	  			 
	    }
	  }
	  //return $result->result();
	  return $this->offeredsubj;
	}	
	else
     return false;		
  }
  else
   return false;	  
 }
 
 function checkifOffered($subjid=0)
 {
   if(@array_key_exists($subjid,$this->offeredsubj))
   {
	 foreach($this->offeredsubj[$subjid] as $key=>$val)
     {
	  return $key;
      break;	  
	 }	  
   }
   else
    return 0;	   
 }
 
 function get_OfferedSubj_Param($stdno='')
 {
  $this->db->reconnect();
  $query = "SELECT TOP 1 StudentNo,
		                 dbo.fn_sisConfigNetworkTermID() AS TermID,
			             ProgID,
			             CurriculumID, 
			             CampusID,
			             (SELECT CASE WHEN dbo.fn_ProgramClassCode(ProgID) > 50 THEN 0 ELSE dbo.fn_sisConfigHideOfferedCoursesWithoutSchedules() END) AS WithScheduleOnly
		  FROM ES_Students 
		 WHERE StudentNo='".$stdno."'";
  $result = $this->db->query($query);  		 
  if($result)
  {
	return $result->row();  
  }	
  else
   return false;	  
 }
 
 function save_EvaltoText($stdno='',$data='')
 {
   $path = str_replace("index.php","assets/evaluation/",$_SERVER['SCRIPT_FILENAME']);
   if(!file_exists($path)){mkdir($path); }
   if($stdno!='')
   {
	$path .= $stdno.'.txt';
	$file_handle = fopen($path, "w+");
	$data = str_replace('"result":false,','',$data);
	fwrite($file_handle, $data);
	fclose($file_handle);
	
	return ((file_exists($path))?true:false);
   }	   
   return false;
 }
 
 function read_TexttoEval($stdno='')
 {
   if($stdno!='')
   {
	$path = str_replace("index.php","assets/evaluation/",$_SERVER['SCRIPT_FILENAME']).$stdno.'.txt';
	$data = ((file_exists($path))?@file_get_contents($path):false);
	
	if($data)
    {
	 $filedate  = filemtime($path);
     $current   = intval(date('Ymd'));
     $tmpdate = intval(date('Ymd',$filedate));
     $age       = ($current - $tmpdate);
     return (($age<=2)? json_decode($data):false);
    }
    else
	 return false;
	
   }	   
   return false; 
 }
 
 function reevaluate_student($stdno='')
 {
   $rs      = '';
   $summary = '';
   $status  = 'failed';
   $this->db->reconnect();
   $eval = $this->studAcadEvaluation($stdno);
   if($eval)
   {	   
    $this->db->close();
    $this->db->reconnect();
    $row = $this->fetch_row($stdno);
    $status = (($row && $row!='')? 'success' : 'failed');
    if($status=='success')
    {
	 list($rs,$summary) =  $this->generate_table($row);
     $this->save_EvaltoText($stdno,json_encode(array('content'=>$rs,'summary'=>$summary))); 	 
    }
   }	
   return array($rs,$summary,$status);
 }

}
?>