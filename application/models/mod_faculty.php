<?php
Class mod_faculty extends CI_Model
{

 function sp_facultyportal($opt=0,$empid='',$campus=1,$termid=0)
 {
  $result=false;
  $xquery = "EXEC sis_FacultyPortalMgmt @Opt='".$opt."', @EmpID='".$empid."', @CampusID=".$campus.", @xID=".$termid.";";
		  
  if($opt!=0)
  {
   $result = $this->db->query($xquery);
   return $result->result(); 
  }
  return $result;
 }
 
 
 function sp_gradeencoding($schedid='',$opt='0.0',$empid='',$studno='',$cs='0',$exam='0',$grade='0',$tgrade='0',$final='',$rex='',$remark='',$noabs='0')
 {
  if($schedid!='')
  {
   $cs     = ((floatval($cs)>0)? $cs : 0);
   $exam   = ((floatval($exam)>0)? $exam : 0);
   $grade  = ((floatval($grade)>0)? $grade : 0);
   $tgrade = ((floatval($tgrade)>0)? $tgrade : 0);
   
   $query = "EXEC sis_GradeSheet @ScheduleID = '".$schedid."'
                                ,@Opt = '".$opt."'
                                ,@EmpID = '".$empid."'
                                ,@StudNo = '".$studno."'
                                ,@CS = '".$cs."'
                                ,@Exam = '".$exam."'
                                ,@Grade = '".$grade."'
                                ,@TGrade = '".$tgrade."'
                                ,@Final = '".$final."'
                                ,@Rex = '".$rex."'
                                ,@Remark = '".$remark."'
                                ,@NoAbs = '".$noabs."'
								 ";
   $result = $this->db->query($query);
   return $result->result();
  }
  return false;
 }
 
 function execute_query($xquery='')
 {
  $result=false;
  if($xquery!='')
  {
   $result = $this->db->query($xquery);
   return $result->result(); 
  }
  
  return $result;
 }
 
 function get_priviledges($empid='')
 {
  $row=$this->sp_facultyportal('0.1',$empid,0,0);
  return $row;
 }
 
 function get_listAYTERM($facultyid='')
 {
    $row=$this->sp_facultyportal('0.1.1','',0,0);
    if($row!='')
	{
	 $xselect='<select id="ayterm" class="form-control" onchange="xdisplayschedules()">';
	 $xselect.="<option value='' selected disabled>-- Select an Academic Year Term --</option>";
	 $prevterm = 0;
     foreach($row as $rs)
     {
	  if($prevterm == $rs->TermID)
        continue;
      else
        $prevterm = $rs->TermID;	

	  if($rs->ActiveTermID==1)
	  {$xselect.="<option value='".$rs->TermID."' selected>".$rs->AY."-".$rs->SchoolTerm."</option>";}
	  else
	  {$xselect.="<option value='".$rs->TermID."'>".$rs->AY."-".$rs->SchoolTerm."</option>";}
     }
	 $xselect.="</select>";
     return $xselect;	 
   }
   else
   {
    return '<select id="ayterm" class="form-control"><option selected disabled> -- No Academic Year & Term is available--</option></select>';
   }
 }
 
 
 function generate_classSched($empid='',$campus=1,$termid='',$mineonly=1)
 {
   $row=false;
   if($mineonly==1)
   {$row = $this->sp_facultyportal('0.2.1',$empid,$campus,$termid);}
   else
   {$row = $this->sp_facultyportal('0.2.2',$empid,$campus,$termid);}
   
   $xcount=0;
   $xdisplay='';
   $xoutput='';
   $tblcontent='';
   $scheduleid='';
   $totallecu = 0;
   $totallabu = 0;
   $totallech = 0;
   $totallabh = 0;
   
  if($row!='')
  {
   $xoutput = "<table id='xschedules' class='table table-bordered table-hover'>
                <thead>
				  <tr>
				  <th>CODE</th>
				  <th>SUBJECT</th>
				  <th>SECTION</th>
				  <th>ENROLLED STUDENTS</th>
				  <th>LEC</th>
				  <th>LAB</th>
				  <th>SHEDULE/ROOM</th>
				  <th>FACULTY</th>
				  </tr>
				</thead>
				<tbody>
				</tbody>
				</table>";
				
   foreach($row as $rs)
   {
    $xtermid = property_exists($rs,'TermID')? $rs->TermID : 0;
    $xfacultyid = property_exists($rs,'FacultyID')? $rs->FacultyID : 0;
    $xfaculty = property_exists($rs,'Faculty')? $rs->Faculty : '';
    $xschedid = property_exists($rs,'ScheduleID')? $rs->ScheduleID : 0;
    $xschedmode = property_exists($rs,'ScheduleMode')? $rs->ScheduleMode : 1;
    $xprogid = property_exists($rs,'ProgID')? $rs->ProgID : 0;
    $xprogcode = property_exists($rs,'ProgCode')? $rs->ProgCode : '';
    $xprogclass = property_exists($rs,'ProgClass')? $rs->ProgClass : 0;
    $xsubjid = property_exists($rs,'SubjectID')? $rs->SubjectID : 1;
    $xsubjcode = property_exists($rs,'SubjectCode')? $rs->SubjectCode : '';
    $xsubjtitle = property_exists($rs,'SubjectTitle')? $rs->SubjectTitle : '';
    $xlecunit = property_exists($rs,'AcadUnits')? $rs->AcadUnits : 0;
    $xlabunit = property_exists($rs,'LabUnits')? $rs->LabUnits : 0;
    $xcreditunit = property_exists($rs,'CreditUnits')? $rs->CreditUnits : 0;
    $xlechrs = property_exists($rs,'LectHrs')? $rs->LectHrs : 0;
    $xlabhrs = property_exists($rs,'LabHrs')? $rs->LabHrs : 0;
	$xsecid = property_exists($rs,'SectionID')? $rs->SectionID : 0;
	$xsecname = property_exists($rs,'SectionName')? $rs->SectionName : '';
	$xsched = property_exists($rs,'Schedule')? $rs->Schedule : '';
	$xroomid = property_exists($rs,'RoomID')? $rs->RoomID : '';
	$xroomno = property_exists($rs,'RoomNo')? $rs->RoomNo : '';
	$xroom = property_exists($rs,'RoomName')? $rs->RoomName : '';
	$xmidtermpost = property_exists($rs,'MidtermGradesPostingDate')? $rs->MidtermGradesPostingDate : '';
	$xfinaltermpost = property_exists($rs,'GradesPostingDate')? $rs->GradesPostingDate : '';
	$xsubmitgrade = property_exists($rs,'SubmitGradeDate')? $rs->SubmitGradeDate : '';
	
	$xown = ($xfacultyid==$empid)?1:0;
	
	if($xroom =='' && $xroomno!='')
	{$xroom = $xroomno;}
	$xtotalstud = property_exists($rs,'TotalStudents')? $rs->TotalStudents : 0;
   
	if($xschedid!='')
	{
	 //if(($mineonly==0) or ($mineonly==1 and $empid==$xfacultyid))
	 //{
	  $color = "txt-color-black";
	  if(trim($xmidtermpost)!='')
	  {$color = "txt-color-orange";}
	  if(trim($xfinaltermpost)!='')
	  {$color = "txt-color-red";}
	  if(trim($xsubmitgrade)!='')
	  {$color = "txt-color-purple";}
	 
	  $tblcontent.="<tr class='".$color."' data-pointer='".$xschedid."' data-progid='".$xprogid."' data-progclass='".$xprogclass."' data-mgmt='".$xown."' data-code='".$xsubjcode."' data-section='".$xsecname."' onclick='selectrow(this);dsply_gradeencode(".'"'.$xschedid.'",'.$xown.");'>
	                 <td id='scode'>".$xsubjcode."</td>
	                 <td><small id='stitle'>".$xsubjtitle."</small></td>
	                 <td><small id='ssec'>".$xsecname."</small></td>
	                 <td>".$xtotalstud."</td>
	                 <td>".$xlecunit."</td>
	                 <td>".$xlabunit."</td>
	                 <td><small>".$xsched."/ ".$xroom."</small></td>
	                 <td><small>".utf8_encode($xfaculty)."</small></td>
	               </tr>";
	  $xcount+=1;
      $totallecu = $totallecu + $xlecunit;
      $totallabu = $totallabu + $xlabunit;
      $totallech = $totallech + $xlechrs;
      $totallabh = $totallabh + $xlabhrs;
	 //}
	}
  }
  if($tblcontent!='')
  {
   $xdisplay .= $tblcontent;
  }
  
  if($xdisplay!='')
  {$xoutput = str_replace('</tbody>',$xdisplay.'</tbody>',$xoutput);}
  
  $xstats = '<table class="table table-striped">
					  <tr><td><small>Total Subject(s):</small></td><td>'.$xcount.'</td></tr>
					  <tr><td><small>Total Lecture Unit(s):</small></td><td>'.$totallecu.'</td></tr>
					  <tr><td><small>Total Lab Unit(s):</small></td><td>'.$totallabu.'</td></tr>
					  <tr><td><small>Total Lecture Hour(s):</small></td><td>'.$totallech.'</td></tr>
					  <tr><td><small>Total Lab Hour(s):</small></td><td>'.$totallabh.'</td></tr>
			 </table>';
  
  return array($xoutput,$xstats);
  }
  return $row; 
 }
 
 
 
 function generate_classGrade($xid=0,$schedid='')
 {
  $xdisplay='';
  $xthead='';
  $xsecid ="";
  $xfacid ="";
  $xisTrans =0;
  $xmposted =0;
  $xfposted =0; 
  
  $xmonperiod = 0;
  $xfonperiod = 0;
  
  $xprelimfile = '';
  $xmidtermfile = '';
  $xprefinfile = '';
  $xfinalfile = '';
  
  $xletteronly = 0; 
  
  $row = $this->sp_gradeencoding($schedid);
  if($row!='')
  {
   $count = 1;
   $xdisplay='<table id="gradetable'.$xid.'" class="table table-bordered" data-hardfile="" data-pointer="'.$schedid.'" data-handler="" data-target="" data-transmuted="0" data-onperioda="0" data-posteda="0" data-filea="" data-onperiodb="0" data-postedb="0" data-fileb="" data-lettergrade="0">
               <xthead>
               <tbody>';
   foreach($row as $rs)
   {
   $xfacid = property_exists($rs,'FacultyID')? $rs->FacultyID : '';
   
   $xsecid = property_exists($rs,'SectionID')? $rs->SectionID : '';
   
   $xregid = property_exists($rs,'RegID')? $rs->RegID : 0;
   
   $xgradeid = property_exists($rs,'GradeIDX')? $rs->GradeIDX : 0;
   
   $xstudno = property_exists($rs,'StudentNo')? $rs->StudentNo : 0;
   $xstudname = property_exists($rs,'StudentName')? $rs->StudentName : '';
   $xlastname = property_exists($rs,'LastName')? $rs->LastName : '';
   $xfirstname = property_exists($rs,'FirstName')? $rs->FirstName : '';
   $xmiddleint = property_exists($rs,'MiddleInitial')? $rs->MiddleInitial : '';
   $xgender = property_exists($rs,'Gender')? $rs->Gender : '';
   
   $xusername = property_exists($rs,'Username')? $rs->Username : '';
   $xprogname = property_exists($rs,'ProgName')?$rs->ProgName:'';
   
   $xPCS = property_exists($rs,'PrelimClassStanding')? $rs->PrelimClassStanding : '';
   $xHPCS = property_exists($rs,'PCS')? $rs->PCS : 1;
   $xPCSlbl = property_exists($rs,'PCSlbl')? $rs->PCSlbl : 'Prelim Class Standing';
   $xPE = property_exists($rs,'PrelimExam')? $rs->PrelimExam : '';
   $xHPE = property_exists($rs,'PE')? $rs->PE : 1;
   $xPElbl = property_exists($rs,'PElbl')? $rs->PElbl : 'Prelim Exam';
   $xPG = property_exists($rs,'PrelimCourseGrade')? $rs->PrelimCourseGrade : '';
   $xHPG = property_exists($rs,'PG')? $rs->PG : 1;
   $xPGlbl = property_exists($rs,'PGlbl')? $rs->PGlbl : 'Prelim Course Grade';
   $xPTG = property_exists($rs,'PrelimTransmutedGrades')? $rs->PrelimTransmutedGrade : '';
   $xHPTG = property_exists($rs,'PTG')? $rs->PTG : 1;
   $xPTGlbl = property_exists($rs,'PTGlbl')? $rs->PTGlbl : 'Prelim Transmuted Grade';
   
   $xMCS = property_exists($rs,'MidtermClassStanding')? $rs->MidtermClassStanding : '';
   $xHMCS = property_exists($rs,'MCS')? $rs->MCS : 1;
   $xMCSlbl = property_exists($rs,'MCSlbl')? $rs->MCSlbl : 'Midterm Class Standing';
   $xME = property_exists($rs,'MidtermExam')? $rs->MidtermExam : '';
   $xHME = property_exists($rs,'ME')? $rs->ME : 1;
   $xMElbl = property_exists($rs,'MElbl')? $rs->MElbl : 'Midterm Exam';
   $xMG = property_exists($rs,'MidtermCourseGrade')? $rs->MidtermCourseGrade : '';
   $xHMG = property_exists($rs,'MG')? $rs->MG : 1;
   $xMGlbl = property_exists($rs,'MGlbl')? $rs->MGlbl : 'Midterm Course Grade';
   $xMTG = property_exists($rs,'MidtermTransmutedGrade')? $rs->MidtermTransmutedGrade : '';
   $xHMTG = property_exists($rs,'MTG')? $rs->MTG : 1;
   $xMTGlbl = property_exists($rs,'MTGlbl')? $rs->MTGlbl : 'Midterm Transmuted Grade';
   
   $xFCS = property_exists($rs,'FinalClassStanding')? $rs->FinalClassStanding : '';
   $xHFCS = property_exists($rs,'FCS')? $rs->FCS : 1;
   $xFCSlbl = property_exists($rs,'FCSlbl')? $rs->FCSlbl : 'Final Class Standing';
   $xFE = property_exists($rs,'FinalExam')? $rs->FinalExam : '';
   $xHFE = property_exists($rs,'FE')? $rs->FE : 1;
   $xFElbl = property_exists($rs,'FElbl')? $rs->FElbl : 'Final Exam';
   $xFG = property_exists($rs,'FinalCourseGrade')? $rs->FinalCourseGrade : '';
   $xHFG = property_exists($rs,'FG')? $rs->FG : 1;
   $xFGlbl = property_exists($rs,'FGlbl')? $rs->FGlbl : 'Final Course Grade';
   $xFTG = property_exists($rs,'FinalTransmutedGrade')? $rs->FinalTransmutedGrade : '';
   $xHFTG = property_exists($rs,'FTG')? $rs->FTG : 1;
   $xFTGlbl = property_exists($rs,'FTGlbl')? $rs->FTGlbl : 'Final Transmuted Grade';
   
   $xprelim = property_exists($rs,'Prelim')? $rs->Prelim : '';
   $xHP = property_exists($rs,'P')? $rs->P : 1;
   $xPlbl = property_exists($rs,'Plbl')? $rs->Plbl : 'Prelim';
   $xmidterm = property_exists($rs,'Midterm')? $rs->Midterm : '';
   $xHM = property_exists($rs,'M')? $rs->M : 1;
   $xMlbl = property_exists($rs,'Mlbl')? $rs->Mlbl : 'Midterm';
   $xfinal = property_exists($rs,'Final')? $rs->Final : '';
   $xHF = property_exists($rs,'P')? $rs->F : 1;
   $xFlbl = property_exists($rs,'Flbl')? $rs->Flbl : 'Final';
   $xrex = property_exists($rs,'ReExam')? $rs->ReExam : '';
   $xHRE = property_exists($rs,'RE')? $rs->RE : 1;
   $xRElbl = property_exists($rs,'RElbl')? $rs->RElbl : 'ReExam';
   
   $xremarks = property_exists($rs,'Remarks')? $rs->Remarks : '';
   
   $xnoofabs = property_exists($rs,'NoofAbsences')? $rs->NoofAbsences : '';
   $xHABS = property_exists($rs,'ABS')? $rs->ABS : 1;
   
   $xdatepost = property_exists($rs,'DatePosted')? $rs->DatePosted : '';
   
   $xmonperiod = property_exists($rs,'MidtermOnPeriod')? $rs->MidtermOnPeriod : 0;
   $xmposted = property_exists($rs,'IsMidtermPosted')? $rs->IsMidtermPosted : 0;
   
   $xfonperiod = property_exists($rs,'FinalOnPeriod')? $rs->FinalOnPeriod : 0;
   $xfposted = property_exists($rs,'IsFinalPosted')? $rs->IsFinalPosted : 0;
   
   $xprelimfile = property_exists($rs,'PrelimFile')? $rs->PrelimFile : '';
   $xmidtermfile = property_exists($rs,'MidtermFile')? $rs->MidtermFile : '';
   $xprefinfile = property_exists($rs,'PreFinFile')? $rs->PreFinFile : '';
   $xfinalfile = property_exists($rs,'FinalFile')? $rs->FinalFile : '';
   
   $xIsWithdrawn = property_exists($rs,'IsWithdrawn')? $rs->IsWithdrawn : 0;
   $xisTrans = property_exists($rs,'IsTransmuted')?$rs->IsTransmuted:0;
   $xletteronly = property_exists($rs,'LetterGradeOnly')?$rs->LetterGradeOnly:0;
   
   //$xisTrans = 1;
   
   $xcol_class = ((($xmonperiod==0 && $xfonperiod==0) || $xfposted==1 || $xIsWithdrawn==1 || strtolower($xremarks)=='leave of absences' || strtolower($xremarks)=='dropped' || strtolower($xremarks)=='withdrawn' || $xdatepost!='')?'except':'');
   if($xIsWithdrawn==1 || strtolower($xremarks)=='withdrawn')
   {
	$xprelim    = 'W';
	$xmidterm   = 'W';
	$xfinal     = 'W';
	$xremarks   = 'Withdrawn';
	$xcol_class = 'except';
   }
   
   if(strtolower($xremarks)=='dropped')
   {
	$xprelim    = (($xprelim=='')?'DRP':$xprelim);
	$xmidterm   = (($xmidterm=='')?'DRP':$xmidterm);
	$xfinal     = (($xfinal=='')?'DRP':$xfinal);
	$xcol_class = 'except';
   }
   
   $xnum_class=(($xletteronly==0)?'':'except');
   
   $xthead = '<thead>
               <tr>
	             <th style="text-align:center;">#</th>
	             <th style="width:80px;text-align:center;">StudentNo</th>
	             <th style="text-align:center;">Name</th>
	             <th style="text-align:center;">Gender</th>'
	  
	           .(($xHMCS==0)?'<th style="text-align:center;">'.$xMCSlbl.'</th>':'')
			   .(($xHME==0)?'<th style="text-align:center;">'.$xMElbl.'</th>':'')
	           .(($xHMG==0)?'<th style="text-align:center;">'.$xMGlbl.'</th>':'')
	           .(($xHMTG==0)?'<th style="text-align:center;">'.$xMTGlbl.'</th>':'')
	  
	           .(($xHFCS==0)?'<th style="text-align:center;">'.$xFCSlbl.'</th>':'')
	           .(($xHFE==0)?'<th style="text-align:center;">'.$xFElbl.'</th>':'')
	           .(($xHFG==0)?'<th style="text-align:center;">'.$xFGlbl.'</th>':'')
	           .(($xHFTG==0)?'<th style="text-align:center;">'.$xFTGlbl.'</th>':'')
	  
	           .(($xHP==0)?'<th style="text-align:center;">'.$xPlbl.'</th>':'')
			   .(($xHM==0)?'<th style="text-align:center;">'.$xMlbl.'</th>':'')
			   .(($xHF==0)?'<th style="text-align:center;">'.$xFlbl.'</th>':'')
			   .(($xHRE==0)?'<th style="text-align:center;">'.$xRElbl.'</th>':'')
			   .'<th style="text-align:center;">Remarks</th>'
			   .(($xHABS==0)?'<th style="text-align:center;">No of Absences</th>':'')
			   .'<th style="text-align:center;">DatePosted</th>
			   </tr>	
			 </thead>';
	
    if($xMCS==0){$xMCS='';}else{$xMCS=number_format(round($xMCS,3)).'.000';}
    if($xME==0){$xME='';}else{$xME=number_format(round($xME,3)).'.000';}
    if($xMG==0){$xMG='';}else{$xMG=number_format(round($xMG,3)).'.000';}
    if($xMTG==0){$xMTG='';}else{$xMTG=number_format(round($xMTG,3));}
	
    if($xFCS==0){$xFCS='';}else{$xFCS=number_format(round($xFCS,3)).'.000';}
    if($xFE==0){$xFE='';}else{$xFE=number_format(round($xFE,3)).'.000';}
    if($xFG==0){$xFG='';}else{$xFG=number_format(round($xFG,3)).'.000';}
    if($xFTG==0){$xFTG='';}else{$xFTG=number_format(round($xFTG,3));}
	
   $xdisplay.='<tr id="stud'.$count.'" data-pointer="'.$xgradeid.'" data-std="'.$xstudno.'" data-name="'.utf8_encode($xstudname).'." data-gender="'.$xgender.'" data-prog="'.$xprogname.'" onclick="showstudinfo('.$count.');">
                <td><small>'.$count.'</small></td>
                <td style="text-align:center;"><small>'.$xstudno.'</small></td>
                <td style="white-space:nowrap;"><small>'.utf8_encode($xstudname).'</small></td>
                <td style="text-align:center;">'.$xgender.'</td>'
               .(($xHMCS==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="mcs'.$count.'" class="'.(($xcol_class!='')?$xcol_class:(($xnum_class!='')?$xnum_class:'')).'">'.$xMCS.'</span></td>':'')
               .(($xHME==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="me'.$count.'" class="'.(($xcol_class!='')?$xcol_class:(($xnum_class!='')?$xnum_class:'')).'">'.$xME.'</span></td>':'')
               .(($xHMG==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="mg'.$count.'" class="'.(($xcol_class!='')?$xcol_class:(($xnum_class!='')?$xnum_class:'')).'">'.$xMG.'</span></td>':'')
               .(($xHMTG==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="mtg'.$count.'" class="'.(($xcol_class!='')?$xcol_class:(($xnum_class!='')?$xnum_class:'')).'">'.$xMTG.'</span></td>':'')
				
               .(($xHFCS==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="fcs'.$count.'" class="'.(($xcol_class!='')?$xcol_class:(($xnum_class!='')?$xnum_class:'')).'">'.$xFCS.'</span></td>':'')
               .(($xHFE==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="fe'.$count.'" class="'.(($xcol_class!='')?$xcol_class:(($xnum_class!='')?$xnum_class:'')).'">'.$xFE.'</span></td>':'')
               .(($xHFG==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="fg'.$count.'" class="'.(($xcol_class!='')?$xcol_class:(($xnum_class!='')?$xnum_class:'')).'">'.$xFG.'</span></td>':'')
               .(($xHFTG==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="ftg'.$count.'" class="'.(($xcol_class!='')?$xcol_class:(($xnum_class!='')?$xnum_class:'')).'">'.$xFTG.'</span></td>':'')
				
               .(($xHP==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="pre'.$count.'" class="'.$xcol_class.'">'.$xprelim.'</span></td>':'')
               .(($xHM==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="mid'.$count.'" class="'.$xcol_class.'">'.$xmidterm.'</span></td>':'')
               .(($xHF==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="fin'.$count.'" class="'.$xcol_class.'">'.$xfinal.'</span></td>':'')
               .(($xHRE==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="rex'.$count.'" class="'.$xcol_class.'">'.$xrex.'</span></td>':'')
               .'<td><span id="remark'.$count.'" data-remark="'.$xremarks.'">'.$xremarks.'</span></td>'
               .(($xHABS==0)?'<td class="no-padding" style="width:50px;text-align:center;"><span id="noofabs'.$count.'" class="'.$xcol_class.'">'.$xnoofabs.'</span></td>':'')
               .'<td  style="white-space:nowrap;"><span id="datepost'.$count.'"><small>'.$xdatepost.'</small></span></td>
              <tr>';
   
   $count += 1;
   } 
   $xdisplay.='</tbody></table>';
   $xdisplay = str_replace('data-handler=""','data-handler="'.$xfacid.'"',$xdisplay);
   $xdisplay = str_replace('data-target=""','data-target="'.$xsecid.'"',$xdisplay);
   $xdisplay = str_replace('data-transmuted="0"','data-transmuted="'.$xisTrans.'"',$xdisplay);
   //finals
   $xdisplay = str_replace('data-onperioda="0" data-posteda="0"','data-onperioda="'.$xfonperiod.'" data-posteda="'.$xfposted.'"',$xdisplay);
   $xdisplay = str_replace('data-filea=""','data-filea="'.$xfinalfile.'"',$xdisplay);
   //midterm
   $xdisplay = str_replace('data-onperiodb="0" data-postedb="0"','data-onperiodb="'.$xmonperiod.'" data-postedb="'.$xmposted.'"',$xdisplay);
   $xdisplay = str_replace('data-fileb=""','data-fileb="'.$xmidtermfile.'"',$xdisplay);
   $xdisplay = str_replace('data-lettergrade="0"','data-lettergrade="'.$xletteronly.'"',$xdisplay);
   
   $xdisplay = str_replace('<xthead>',$xthead,$xdisplay);
   return $xdisplay;
  }
  return false;
 }

  
 function generate_gradingsystem($progclass=50)
 {
  $row = $this->sp_facultyportal('0.4',0,0,$progclass);
  $xdisplay='';
  if($row!='')
  {
   $xdisplay='<table id="tblgradesys" class="table table-striped table-bordered">
				<thead>
				 <tr><th><small>Grade</small></th><th><small>Equivalence</small></th><th><small>Description</small></th><th><small>Remarks</small></th><th><small>Letter Grade</small></th></tr>
				</thead>
				<tbody>';
	foreach($row as $rs)
    {
     $xgrade = property_exists($rs,'Grade')? $rs->Grade : '';
     $xmingrade = property_exists($rs,'MinGrade')? $rs->MinGrade : '';
     $xmaxgrade = property_exists($rs,'MaxGrade')? $rs->MaxGrade : '';
     $xequiv = property_exists($rs,'Equivalence')? $rs->Equivalence : '';
     $xgraddescid = property_exists($rs,'GradDescID')? $rs->GradDescID : '';
     $xgraddesc = property_exists($rs,'GradeDescription')? $rs->GradeDescription : '';
     $xlettergradid = property_exists($rs,'LetterGradeID')? $rs->LetterGradeID : '';
     $xlettergrad = property_exists($rs,'LetterGrade')? $rs->LetterGrade : '';
     $xremarkid = property_exists($rs,'FinalRemarkID')? $rs->FinalRemarkID : '';
     $xremark = property_exists($rs,'Remarks')? $rs->Remarks : '';
	 
	 if($xmingrade!=$xmaxgrade)
	 {$range = $xmingrade.' - '.$xmaxgrade;}
	 else
	 {$range = $xmingrade;} 
	 
	 $xdisplay.='<tr data-min="'.$xmingrade.'" data-max="'.$xmaxgrade.'" data-grade="'.$xgrade.'" data-remark="'.$xremark.'"><td><small>'.$xgrade.'</small></td><td><small>'.$range.'</small></td><td><small>'.$xgraddesc.'</small></td><td><small>'.$xremark.'</small></td><td><small>'.$xlettergrad.'</small></td></tr>';
    }					  
	$xdisplay.='</tbody></table>';
	return $xdisplay;		   
  }
  else
  {
   return '<div class="alert alert-warning"><i class="fa fa-warning"></i> No Data Loaded.</div>';
  }
 }

  
 function generate_transmutation()
 {
  $row = $this->sp_facultyportal('0.5',0,0,0);
  $xdisplay='';
  if($row!='')
  {
   $xdisplay='<table id="tblgradetrans" class="table table-striped table-bordered">
				<thead>
				 <tr><th><small>Grade</small></th><th><small>Transmutation</small></th></tr>
				</thead>
				<tbody>';
	foreach($row as $rs)
    {
     $xgrade = property_exists($rs,'CourseGrade')? $rs->CourseGrade : '';
     $xmingrade = property_exists($rs,'CourseGrade')? $rs->CourseGrade : 0;
     $xmaxgrade = property_exists($rs,'CourseGrade')? ($rs->CourseGrade+0.99) : 0;
     $xequiv = property_exists($rs,'TransmutedGrade')? $rs->TransmutedGrade : 0;
	 
	 $xdisplay.='<tr data-min="'.$xmingrade.'" data-max="'.$xmaxgrade.'" data-tgrade="'.$xequiv.'"><td><small>'.$xgrade.'</small></td><td><small>'.$xequiv.'</small></td></tr>';
    }					  
	$xdisplay.='</tbody></table>';
	return $xdisplay;		   
  }
  else
  {
   return '<div class="alert alert-warning"><i class="fa fa-warning"></i> No Data Loaded.</div>';
  }
 }

 function uploadfile($facultyid,$secid,$schedid,$filename,$remarks,$term,$uploader)
 {
  $success=false;
  $data = array(
		        'FacultyID' => $facultyid ,
		        'SectionID' => $secid,
		        'ScheduleID' => $schedid,
		        'Date' => date('Y-m-d H:i:s'),
		        'Attachment' => $filename,
		        'Remarks' => $remarks,
		        'UploadedBy' => $uploader,
		        'Term' => $term
		       );

  $success=$this->db->insert('sis_GradeSheetFiles', $data);
  return $success;
 }
 
}
?>