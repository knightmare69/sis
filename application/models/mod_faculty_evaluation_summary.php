<?php
Class mod_faculty_evaluation_summary extends CI_Model
{
    function get_employeelist($t,$d=0)
    {
        /*dbo.fn_EmployeeName(FacultyID)*/
        $query = "SELECT cs.FacultyID , dbo.fn_EmployeeName(cs.FacultyID) As Faculty, 
                dbo.fn_EmployeeDepartmentName(cs.facultyID) as Dept , count(fe.EntryID) As Respondent, isnull(avg(FinalRating),0) As Overall

                FROM es_classSchedules cs left join ES_FacultyEvaluation fe on cs.ScheduleID= fe.SchedID 
                WHERE   TermID = '{$t}' AND cs.FacultyID <> '' ANd cs.FacultyID Is NOT NULL GROUP BY cs.FacultyID ORDER BY Faculty ASC ;";
        $result = $this->db->query($query);
        
          return $result;
        
    } 
    
    function get_terms(){
        $this -> db -> select('TermID, dbo.fn_AcademicYearTerm(TermID) As YearTerm ');
        $this -> db -> from('es_ayterm');
        $this -> db -> where('Hidden', 0);
        $this -> db -> limit(10);
        $this->db->order_by('YearTerm','DESC');
        $query = $this -> db -> get();
        
        return $query->result();              
    }
    
    function get_evaluationSummary($t, $f){
       
        $query = " select QuestionID, td.Questions, AVG(d.Score) As Mean, td.CategoryID from ES_FacultyEvaluation m 
                left join ES_FacultyEvaluation_scores d on m.EntryID = d.RefID
                left join es_facultyevaluation_TemplateDetails td on d.QuestionID=td.IndexID
                left join ES_ClassSchedules cs on m.SchedID =cs.ScheduleID
                where m.facultyid = '{$f}' AND cs.TermID = '{$t}'
                and td.Questions is not null
                Group by QuestionID, td.Questions, td.CategoryID
                ORDER BY QuestionID";
        $result = $this->db->query($query);
        
        return $result;      
    }
	
    function get_comments($t, $f){
        $qry = "select m.EntryID, m.Comment from es_facultyevaluation m
			 left join ES_ClassSchedules cs on m.SchedID =cs.ScheduleID
				 where m.facultyid = '{$f}' and cs.termid = '{$t}'";
		$result = $this->db->query($qry);
        
        return $result;
    }
}
?>