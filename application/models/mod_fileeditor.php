<?php
Class mod_fileeditor extends CI_Model
{
 function generate_sub($path='')
 { 
  $output=array();
  $list = scandir($path);  	  
  $i=0;
  foreach($list as $k => $v)
  {
   if($v!='.' && $v!='..')
   { 
	if(is_dir($path.'/'.$v))
	 $output[$i]=array('name'=>$v, 'path'=>$path.'/'.$v, 'sub'=>$this->generate_sub($path.'/'.$v));
    else	
	 $output[$i]=array('name'=>$v, 'path'=>$path.'/'.$v);
	
	$i++;
   } 		
  }
  return $output;  
 }
 
 function generate_list($directory_path='',$withdetail=false)
 {
  if($directory_path==''){$directory_path=str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']); }
  $output=array();
  $i=0;
  if($withdetail==false)
  {	  
   $list = scandir($directory_path);  	  
   log_message('error','trial:'.$directory_path);
   foreach($list as $k => $v)
   {
    if($v!='.' && $v!='..')
    { 
	 if(is_dir($directory_path.$v))
	  $output[$i]=array('name'=>$v, 'path'=>$directory_path.$v, 'sub'=>$this->generate_sub($directory_path.$v));
     else	
	  $output[$i]=array('name'=>$v, 'path'=>$directory_path.$v);	
	
	 $i++;
    } 		
   }
  }
  else
  {
   $list = scandir($directory_path);  	  
   foreach($list as $k => $v)
   {
    if($v!='.' && $v!='..')
    {
	 $size=0;
	 $datemod='';
	 	 
	 if(is_dir($directory_path.$v))
	  $output[$i]=array('name'=>$v, 'path'=>$directory_path.$v.'/', 'sub'=>true, 'size'=>'','datemod'=>'');
     else if ($directory_path.$v =='C:/Documents and Settings')
	  $output[$i]=array('name'=>$v, 'path'=>'', 'sub'=>true, 'size'=>'','datemod'=>'');
     else if(is_dir($directory_path.$v) && (@filesize($directory_path.$v)==false && @filemtime($directory_path.$v)==false))
	  $output[$i]=array('name'=>$v, 'path'=>'', 'sub'=>true, 'size'=>'','datemod'=>'');
     else	
	 {	 
	  $size=((@filesize($directory_path.$v))?@filesize($directory_path.$v):'');
	  $datemod=((@filemtime($directory_path.$v))?date('Y-m-d h:i A',@filemtime($directory_path.$v)):'');
	  $output[$i]=array('name'=>$v, 'path'=>$directory_path.$v, 'size'=>$size.' bytes','datemod'=>$datemod);	
	 }
	 $i++;
    } 		
   }	  
  } 	  
  return $output;
 } 

}
?>