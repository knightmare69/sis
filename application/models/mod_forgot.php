<?php
Class mod_forgot extends CI_Model
{ 
  function get_question($idno='')
  {
	if($idno==''){return false; }
    $query = "SELECT TOP 1 CASE WHEN m.IDType=1 THEN 'Latest RegID' 
								WHEN (m.IDType=3 OR m.IDType='-1') AND e.DateOfBirth IS NOT NULL THEN 'BirthDate' 
								WHEN (m.IDType=3 OR m.IDType='-1') AND e.DateOfBirth IS NULL THEN 'BirthDate' 
								ELSE 'Account Creation Date'
							END as Question
						  ,CASE WHEN m.IDType=1 THEN 1
								WHEN (m.IDType=3 OR m.IDType='-1') AND e.DateOfBirth IS NOT NULL THEN 2 
								WHEN (m.IDType=3 OR m.IDType='-1') AND e.DateOfBirth IS NULL THEN 2
								ELSE 0
							END as QuestionID
			              ,m.IDType
			              ,m.UserID 
			              ,m.UserName 
			              ,m.Email  
                      FROM ES_Membership as m
                      LEFT JOIN ES_Students as s ON m.IDNo=s.StudentNo 
                      LEFT JOIN HR_Employees as e ON m.IDNo=e.EmployeeID 
                     WHERE ((IDNo='".$idno."' AND (m.IDType=1 OR m.IDType=3 OR m.IDType=-1)) OR ((m.UserName='".$idno."' OR m.Email='".$idno."') AND IDType=2)) 
					   AND UserName NOT LIKE 'trial%' 
					   AND UserName NOT LIKE 'student%'";
    $exec = $this->db->query($query);
    if($exec)
     return $exec->result();
    else
     return false;		
  }
  
  function validate_acct($uid='',$idno='',$type=0,$param='')
  {
	if($uid=='' || $idno=='' || $param==''){return false;}
	$query='';
	switch($type)
	{
	 case 1:
	  $param = ((is_numeric($param))?$param:0);
	  $query = "SELECT m.UserID,m.UserName,m.Email,m.IDNo,m.IsActivated 
				  FROM ES_Registrations as r
				 INNER JOIN ES_Membership as m ON r.StudentNo=m.IDNo AND m.UserID='".$uid."'
				 WHERE StudentNo='".$idno."' AND RegID='".$param."'";
     break;
     case 2:
	  $param = new DateTime($param);
	  $ans   = $param->format('m/d/Y');
	  $query = "SELECT m.UserID,m.UserName,m.Email,m.IDNo,m.IsActivated  
				  FROM HR_Employees as e
				 INNER JOIN ES_Membership as m ON e.EmployeeID=m.IDNo AND m.UserID='".$uid."'
				 WHERE EmployeeID='".$idno."' AND DateOfBirth='".$ans."'";
     break;
	 default:
	  $param = new DateTime($param);
	  $ans   = $param->format('m/d/Y');
      $query = "SELECT m.UserID,m.UserName,m.Email,m.IDNo,m.IsActivated
                  FROM ES_Membership as m
                 WHERE m.UserID='".$uid."' AND CONVERT(DATE,m.DateCreated)='".$ans."'";
     break;	 
	}
	
	if($query!='')
	{
	 $exec = $this->db->query($query);
     if($exec)
     {
	  $result = array();
      foreach($exec->result() as $rs)
      {
	    $result['UserID']   = is_key_exist($rs,'UserID','');	 
	    $result['UserName'] = is_key_exist($rs,'UserName','');	 
	  }
      return $result;		   
	 }
     else
      return false;		 
	}	
	else
     return false;
  }
  
  function get_userinfo($uname='',$email='')
  {
	$result = array();
	if($uname=='' && $email==''){return false; }
    if($uname!='')
     $query = "SELECT TOP 1 * FROM ES_Membership WHERE Username='".$uname."' OR CAST(UserID as VARCHAR(50))='".$uname."'";		
	else if($email!='')
     $query = "SELECT TOP 1 * FROM ES_Membership WHERE Username='".$email."' OR Email='".$email."'"	;
			
	$exec = $this->db->query($query);
	if($exec)
	{
	 foreach($exec->result() as $rs)
     {
	   $result['UserID']   = is_key_exist($rs,'UserID','');	 
	   $result['UserName'] = is_key_exist($rs,'UserName','');	 
	   $result['IDNo']     = is_key_exist($rs,'IDNo','');	 
	   $result['IDType']   = is_key_exist($rs,'IDType','');	 
	   $result['Email']    = is_key_exist($rs,'Email','');	 
	 }
     return $result;	 
	}
    else if($uname!='' && $email!='')
    {
	  return $this->get_userinfo('',$email); 	
	}		
  }
  
  function set_password($uid='',$idno='',$idtype='',$npwd='',$email='')
  {
	if($uid=='' && $npwd==''){return false; }
	$query = "UPDATE ES_Membership SET Password='".md5($npwd)."' ".(($email!='')?",Email='".$email."' ":"")." WHERE UserID='".$uid."';";
	if($idtype==3 || $idtype==-1)
	{
		
	} 	
	$exec  = $this->db->query($query);
    return $exec;	
  }
  
}
?>