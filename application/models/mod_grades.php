<?php
class mod_grades extends CI_MODEL
{
	
	function call_xtraDetails($param=null,$param1=87)
	{
	 $result = false;
	 if($param!='')
	  {
	   $query = "SELECT TOP 1 
						dbo.fn_StudentName(s.StudentNo) AS StudentName,
						dbo.fn_ProgramCollegeName(s.ProgID) AS CollegeName,
						dbo.fn_GetProgramNameWithMajor(s.ProgID,s.MajorDiscID) AS Program ,
						dbo.fn_CurriculumCode(s.CurriculumID) AS Curriculum,
						dbo.fn_ProgramClassCode(r.ProgID) as ProgClass,
						dbo.fn_YearLevel2(r.YearLevelID,dbo.fn_ProgramClassCode(r.ProgID)) as YearLevel,
						dbo.fn_SectionName(r.ClassSectionID) as SectionName
				 FROM ES_Students s
				 LEFT JOIN ES_Registrations r ON s.StudentNo=r.StudentNo and r.TermID='".$param1."' 
				 WHERE s.StudentNo = '".$param."';";
				 
	   $result = $this->db->query($query);
	   return $result->row();	
	  }  
	 return $result;
	} 
	
	function xtermID($studentno)
	{
		$this -> db -> select('TermID');
		$this -> db -> from('es_students');
		$this -> db -> where('studentno', $studentno);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1)
		{
			 $output = $query->result();
			 $x = $output[0]->TermID;
			 return $x;
		}
		else
		{
			  return false;
		}
	}
	
	function return_dataset($studentno='',$termid='',$progclass=50)
	{
		$query ='';
		$yearlevel ='';
		$section ='';
		$remarks ='Online View';
		
		if($studentno!='' && ($progclass>20 || $progclass==''))
		{
	     //$query = "EXEC po_reportofgrades @TermID='".$termid."', @StudentNo='".$studentno."';";
		 return $this->exec_Grade($studentno,$termid,$progclass);
	    }
		else if($studentno!='')
		{	
		 $this->db-reconnect();
		 $query = "EXEC ES_rptIBED_ReportCards @TermID='".$termid."', @YearLevelID='".$yearlevel."', @SectionID='".$section."', @StudentNo='".$studentno."',  @Remarks='".$remarks."'";
		 $result = $this->db->query($query);
		 return $result->result();	 
		}
		return false;
	}
	
	function call_sp2($param=null)
	{
	  $query = "EXEC sp_GradesAYTerm '".$param."';";
	  $result = $this->db->query($query);
	  return $result->result();	
	} 
	
	function return_TermID($param='')
	{
	  //$row = $this->call_sp2($param);
	  //return $row;
	  $this->db->reconnect();
	  $query = "EXEC sp_GradesAYTerm '".$param."';";
	  $result = $this->db->query($query);
	  return (($result)? $result->result() : false);	
	}
	
	function get_GradeSummary($studno,$termid){
	  //$termid=91;
	  $this->db->reconnect();
	  //$qry = "exec sp_GradeSummary '".$studno."','".$termid."'" ;
	  $qry = "SELECT summ.CreditUnitsEnrolled AS enrolled ,
					 summ.CreditUnitsEarned AS earned ,
					 ROUND(CAST(summ.GWA AS MONEY), 3) AS gwa ,
					 ROUND(CAST(( SELECT SUM(GradePoints) / SUM(CreditUnitsEnrolled) FROM ES_GradesSummary WHERE StudentNo = '".$studno."' AND GWA > 0 AND dbo.fn_AcademicSchoolTerm(TermID) <> 'Summer') AS MONEY), 3) AS cqpa1 ,
					 ROUND(CAST(( SELECT SUM(GradePoints) / SUM(CreditUnitsEnrolled) FROM ES_GradesSummary WHERE StudentNo = '".$studno."' AND GWA > 0) AS MONEY), 3) AS cqpa2
				FROM dbo.ES_GradesSummary AS summ
			   WHERE summ.StudentNo = '".$studno."' AND summ.TermID = '".$termid."'";
	  $result = $this->db->query($qry);                                    
	  return $result->result(); 
	}   
	
	function exec_Grade($stdno='',$termid=0,$progclass=50,$hostname='')
	{
	 if($stdno=='' || $termid==''){return false; }	
	 $query = "SELECT s.SubjectCode AS [Subject] ,
					  s.SubjectTitle AS Title ,
					  s.CreditUnits AS Unit ,
					  g.Prelim ,
					  g.Midterm ,
					  g.Final ,
					  g.ReExam , 
					  CASE WHEN g.FinalRemarks IS NULL OR g.FinalRemarks='' THEN g.Remarks ELSE g.FinalRemarks END AS GradeRemarks,
					  g.DatePosted,
					  g.SortOrder AS SortOrder,
					  cs.MidtermGradesPostingDate AS MidtermGradesPostingDate, 
					  st.StudentNo, 
					  st.Fullname,
					  p.ProgName,
					  dbo.fn_AcademicYearTerm(g.TermID) AS ayterm,
					  dbo.fn_YearLevel(g.YearLevelID) AS yearlevel,
					  cs.Sched_1,
					  cs.Sched_2,
					  cs.Sched_3,
					  cs.Sched_4,
					  cs.Sched_5
				 FROM ES_grades g
					  INNER JOIN ES_Subjects s ON s.SubjectID = g.SubjectID
					  LEFT OUTER JOIN ES_Students AS st ON st.StudentNo = g.StudentNo
					  LEFT OUTER JOIN ES_Programs AS p ON p.ProgID = g.ProgID
					  LEFT OUTER JOIN ES_ClassSchedules as cs ON cs.ScheduleID=g.scheduleid
			    WHERE g.studentno = '".$stdno."' AND g.TermID = ".$termid."
            
              UNION ALL
    
	          SELECT subj.SubjectCode AS [Subject] ,
                     subj.SubjectTitle AS Title ,
                     subj.CreditUnits AS Unit ,
                     '' AS Prelim ,
                     '' AS Midterm ,
                     'CE' AS Final ,
                     '' AS ReExam ,
                     '* Enrolled' AS GradeRemarks ,
                     NULL AS DatePosted,
			         rd.SeqNo AS SortOrder,
			         NULL AS MidtermGradesPostingDate, 
					 st.StudentNo, 
					 st.Fullname, 
					 p.ProgName,
					 dbo.fn_AcademicYearTerm(r.TermID) AS ayterm,
					 dbo.fn_YearLevel(r.YearLevelID) AS yearlevel,
					 cs.Sched_1,
					 cs.Sched_2,
					 cs.Sched_3,
					 cs.Sched_4,
					 cs.Sched_5
			   FROM ES_Registrations AS r
					LEFT OUTER JOIN ES_RegistrationDetails AS rd ON rd.RegID = r.RegID
					LEFT OUTER JOIN ES_ClassSchedules AS cs ON cs.ScheduleID = rd.ScheduleID
					LEFT OUTER JOIN ES_Subjects AS Subj ON Subj.SubjectID = cs.SubjectID
					LEFT OUTER JOIN ES_Students AS st ON st.StudentNo = r.StudentNo
					LEFT OUTER JOIN ES_Programs AS p ON p.ProgID = r.ProgID
			  WHERE r.StudentNo = '".$stdno."' AND r.TermID = ".$termid." AND r.ValidationDate IS NOT NULL AND r.IsWithdrawal = 0
					AND rd.RegTagID < 3
					AND ( NOT EXISTS ( SELECT TOP 1 GradeIDX FROM ES_Grades WHERE StudentNo = '".$stdno."' AND ScheduleID = rd.ScheduleID ))
			
              GROUP BY subj.SubjectID
			          ,subj.SubjectCode
					  ,Subj.SubjectTitle
					  ,subj.CreditUnits
					  ,rd.SeqNo
					  ,st.StudentNo
					  ,st.Fullname
					  ,p.ProgName
					  ,r.TermID
					  ,r.YearLevelID
					  ,cs.Sched_1
					  ,cs.Sched_2
					  ,cs.Sched_3
					  ,cs.Sched_4
					  ,cs.Sched_5 ";	 
	        
     if($query!='')
     {
	  $this->db->reconnect();
	  $result = $this->db->query($query);
	  return $result->result();		 
	 }
     else
	  return false;
	
	}
}
?>