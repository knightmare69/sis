<?php
        class mod_home extends CI_MODEL
        {
		        function exec_admindashboard($opt='0.1')
                {
				 $result = false;
				 if($opt != '')
				 {
					$query = "EXEC sis_DashboardCtrl @Opt ='".$opt."';";
					$result = $this->db->query($query);
					return $result->result();    
				 }
				}				
				
				
				function generate_stats()
				{
				  $date = array();
				  $acc_val = array();
				  $reg_val = array();
				  
				  $axes= $this->exec_admindashboard($opt='0.2');
				  $reg= $this->exec_admindashboard($opt='0.3');
				
				  if($axes)
				  {
				   foreach($axes as $a)
				   {
				    $period = $a->Period;
				    $access = $a->Access;
				   
				    if(!in_array($period,$date))
				     array_push($date,$period);
					 
				    $key = array_search($period, $date);
                    $acc_val[$key]=$access;	
                    $reg_val[$key]=0;				   
				   }
				  }
				  else
				  {
				   $period = date("Y-m-d");
				   
				   array_push($date,$period);
				   $key = array_search($period, $date);
                   $acc_val[$key]=0;	
                   $reg_val[$key]=0;
				  }
				  
				  if($reg)
				  {
				   foreach($reg as $r)
				   {
				    $period = $r->Period;
				    $access = $r->Access;
				   
				    if(!in_array($period,$date))
				     array_push($date,$period);
					 
				    $key = array_search($period, $date);
                    $reg_val[$key]=$access;				   
				    }
				  }
				  
				  return array(
				                'date' => join("|",$date)
				               ,'access' => join("|",$acc_val)
				               ,'reg' => join("|",$reg_val)
				              );
				}
				
                function progressbarinfo($studentno)
                {
                        $result = false;
                        if($studentno != '')
                        {
                                $query = "EXEC sis_SummaryOfEvaluationByYear @StudentNo ='".$studentno."';";
                                $result = $this->db->query($query);
                                return $result->result();    
                        }
                }
                
                function pieinfo($studentno)
                {
                        $result = false;
                        if($studentno != '')
                        {
                                $query = "EXEC sis_SummaryOfEvaluationRemarks @StudentNo ='".$studentno."';";
                                $result = $this->db->query($query);
                                return $result->result();    
                        }
                }
                 
                function academicinfo($studentno)
                {
                        $result = false;
                        
                        if($studentno!='')
                        {
                                $query = "EXEC sis_evaluation_r2 @StudentNo ='".$studentno."';";
                                $result = $this->db->query($query);
                                return $result->result();	
                        }
                        
                        return $result;
                }
                
                
        }
?>