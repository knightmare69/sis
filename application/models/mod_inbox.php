<?php
Class mod_inbox extends CI_Model
{
 function load_flags($userid='',$count=false)	
 {
   $select = (($count)?"COUNT(EntryID) as TotalItem " : "TOP 20 EntryID,i.DateCreated,DATEDIFF(".'"s"'.",i.DateCreated,GETDATE()) AS Age,xFrom,m.UserName,m.Email,xRecipient,ISNULL(ReplyFor,0) as ReplyFor,TypeID,xSubject,xContent,ISNULL(StatusID,0) as StatusID ");
   $source = "FROM sis_Inbox as i
              LEFT OUTER JOIN  sis_Inbox_Status as xs ON i.EntryID = xs.MsgID AND (xs.UserID='".$userid."' OR xs.UserID IS NULL)
              LEFT OUTER JOIN  ES_Membership as m ON i.xFrom = m.UserName ";
   $where  = (($count)?"WHERE ISNULL(StatusID,0)=0 AND (xRecipient='".$userid."' OR xRecipient='All') " : "WHERE ISNULL(StatusID,0)<>-1 AND (xRecipient='".$userid."' OR xRecipient='All') ");			  
   $order  = (($count)?"":"ORDER BY EntryID DESC");
   $query  = "SELECT ".$select." ".$source." ".$where." ".$order;
   $exec   = $this->db->query($query);
   return (($exec)?($exec->result()):false);
 }
 
 function load_mail($userid='',$type='mail',$limit=10,$offset=0,$count=false)
 {
   $fields = (($count)?"min(EntryID) as LastID,max(EntryID) as FirstID, COUNT(EntryID) as TotalID ":"EntryID,i.DateCreated,DATEDIFF(".'"s"'.",i.DateCreated,GETDATE()) AS Age,xFrom,m.UserName,m.Email,xRecipient,ISNULL(ReplyFor,0) as ReplyFor,TypeID,xSubject,xContent,ISNULL(StatusID,0) as StatusID ");	 
   $order  = (($count)?" ":"ORDER BY i.EntryID DESC OFFSET ".$offset." ROWS FETCH NEXT ".$limit." ROWS ONLY ");
   $source = '';
   switch($type)
   {
    case 'notify':	 
	 $source = "FROM sis_Inbox as i
                LEFT OUTER JOIN  sis_Inbox_Status as xs ON i.EntryID = xs.MsgID AND (xs.UserID='".$userid."' OR xs.UserID IS NULL)
                LEFT OUTER JOIN  ES_Membership as m ON i.xFrom = m.UserName
               WHERE i.TypeID=1 AND ISNULL(StatusID,0)<>-1 AND (xRecipient='".$userid."' OR xRecipient='All') ";
	 break;
    case 'sent':	 
	 $source = "FROM sis_Inbox as i
                LEFT OUTER JOIN  sis_Inbox_Status as xs ON i.EntryID = xs.MsgID AND i.xFrom=xs.UserID
			    LEFT OUTER JOIN  ES_Membership as m ON i.xRecipient = m.UserName
               WHERE ISNULL(StatusID,0)<>-1 AND (xFrom='".$userid."' AND i.Inactive=0) ";
	 break;
    case 'trash':	 
	 $source = "FROM sis_Inbox as i
                LEFT OUTER JOIN  sis_Inbox_Status as xs ON i.EntryID = xs.MsgID AND (i.xFrom=xs.UserID OR i.xRecipient=xs.UserID)
			    LEFT OUTER JOIN  ES_Membership as m ON i.xRecipient = m.UserName
               WHERE ISNULL(StatusID,0)=-1 AND xs.UserID='".$userid."'";
	 break;	   
    default:
     $source = "FROM sis_Inbox as i
                LEFT OUTER JOIN  sis_Inbox_Status as xs ON i.EntryID = xs.MsgID AND (xs.UserID='".$userid."' OR xs.UserID IS NULL)
                LEFT OUTER JOIN  ES_Membership as m ON i.xFrom = m.UserName
               WHERE i.TypeID=2 AND ISNULL(StatusID,0)<>-1 AND (xRecipient='".$userid."' OR xRecipient='All') ";
	 break;
   }
   
   $query = "SELECT ".$fields.$source.$order;
   
   $exec  = $this->db->query($query);
   return (($exec)?($exec->result()):false); 
 }
 
 function load_notify($userid='',$limit=10,$offset=0)
 {
   $query = "SELECT EntryID,i.DateCreated,DATEDIFF(".'"s"'.",i.DateCreated,GETDATE()) AS Age,xFrom,m.UserName,m.Email,xRecipient,ISNULL(ReplyFor,0) as ReplyFor,TypeID,xSubject,xContent,ISNULL(StatusID,0) as StatusID 
               FROM sis_Inbox as i
               LEFT OUTER JOIN  sis_Inbox_Status as xs ON i.EntryID = xs.MsgID AND (i.xRecipient=xs.UserID OR i.xRecipient='All')
               LEFT OUTER JOIN  ES_Membership as m ON i.xFrom = m.UserName
              WHERE i.TypeID=1 AND ISNULL(StatusID,0)<>-1 AND (xRecipient='".$userid."' OR xRecipient='All')
              ORDER BY i.DateCreated DESC
			  OFFSET ".$offset." ROWS
			  FETCH NEXT ".$limit." ROWS ONLY";
			  
   $exec  = $this->db->query($query);
   return (($exec)?($exec->result()):false); 
 }
 
 function load_sent($userid='',$limit=10,$offset=0)
 {
   $query = "SELECT EntryID,i.DateCreated,DATEDIFF(".'"s"'.",i.DateCreated,GETDATE()) AS Age,xFrom,m.UserName,m.Email,xRecipient,ISNULL(ReplyFor,0) as ReplyFor,TypeID,xSubject,xContent,ISNULL(StatusID,0) as StatusID 
               FROM sis_Inbox as i
               LEFT OUTER JOIN  sis_Inbox_Status as xs ON i.EntryID = xs.MsgID AND (i.xFrom=xs.UserID OR i.xFrom='All')
			   LEFT OUTER JOIN  ES_Membership as m ON i.xRecipient = m.UserName
              WHERE ISNULL(StatusID,0)<>-1 AND (xFrom='".$userid."' AND i.Inactive=0)
              ORDER BY i.DateCreated DESC
			  OFFSET ".$offset." ROWS
			  FETCH NEXT ".$limit." ROWS ONLY";
			  
   $exec  = $this->db->query($query);
   return (($exec)?($exec->result()):false); 
 }
 
 function load_trash($userid='',$limit=10,$offset=0)
 {
   $query = "SELECT EntryID,i.DateCreated,DATEDIFF(".'"s"'.",i.DateCreated,GETDATE()) AS Age,xFrom,m.UserName,m.Email,xRecipient,ISNULL(ReplyFor,0) as ReplyFor,TypeID,xSubject,xContent,ISNULL(StatusID,0) as StatusID 
               FROM sis_Inbox as i
               LEFT OUTER JOIN  sis_Inbox_Status as xs ON i.EntryID = xs.MsgID AND (i.xFrom=xs.UserID OR i.xRecipient=xs.UserID)
			   LEFT OUTER JOIN  ES_Membership as m ON i.xRecipient = m.UserName
              WHERE ISNULL(StatusID,0)=-1 AND (xFrom='".$userid."' AND i.Inactive=0)
              ORDER BY i.DateCreated DESC
			  OFFSET ".$offset." ROWS
			  FETCH NEXT ".$limit." ROWS ONLY";
			  
   $exec  = $this->db->query($query);
   return (($exec)?($exec->result()):false); 
 }
 
 function load_content($userid='',$msgid=0)
 {
  $query = "INSERT INTO sis_Inbox_Status(MsgID,UserID,StatusID) SELECT '".$msgid."' as MsgID,'".$userid."' as UserID,1 as StatusID WHERE (SELECT COUNT(*) as xCount FROM sis_Inbox_Status WHERE UserID='".$userid."' AND MsgID='".$msgid."')=0";
  $exec = $this->db->query($query);
  if($exec)
  {	  
   $this->db->close();
   $this->db->reconnect();
   $query = "SELECT EntryID,
				    xFrom,
				    m.IDNo,
				    (CASE WHEN m.IDType=1 THEN s.LastName WHEN m.IDType=3 OR m.IDType=-1 THEN e.LastName ELSE m.LastName END) as LastName,
				    (CASE WHEN m.IDType=1 THEN s.FirstName WHEN m.IDType=3 OR m.IDType=-1 THEN e.FirstName ELSE m.FirstName END) as FirstName,
				    m.Email,
				    xRecipient,
				    TypeID,
				    xSubject,
				    xContent,
				    inx.DateCreated,
				    inx.Inactive 
			   FROM sis_Inbox as inx
			  INNER JOIN ES_Membership as m ON inx.xFrom=m.UserName
			   LEFT OUTER JOIN ES_Students as s ON m.IDNo=s.StudentNo AND m.IDType=1
			   LEFT OUTER JOIN HR_Employees as e ON m.IDNo=e.EmployeeID AND (m.IDType=3 OR m.IDType=-1) 
			  WHERE EntryID='".$msgid."'";	 
   $exec = $this->db->query($query);
   if($exec)
    return $exec->result();
   else
	return false;
  }
  else
   return false;	  
 }
 
 function create_inbox($uid,$type,$subj,$content)
 {
   $session_data = $this->session->userdata('logged_in');
   $query = "INSERT INTO sis_Inbox(xFrom,xRecipient,TypeID,xSubject,xContent,DateCreated) VALUES('".$session_data['id']."','".$uid."','".$type."','".strsanitizer($subj)."','".strsanitizer($content)."',GETDATE())";	 
   $exec  = $this->db->query($query);
   return $exec;
 }
 
 function isread($uid,$mid)
 {
   $query = "INSERT INTO sis_Inbox_Status(MsgID,UserID,StatusID) SELECT '".$mid."' as MsgID,'".$uid."' as UserID,1 as StatusID WHERE (SELECT COUNT(*) as xCount FROM sis_Inbox_Status WHERE UserID='".$uid."' AND MsgID='".$mid."')=0";
   $exec  = $this->db->query($query);
   return $exec; 
 }
 
 function delete_inbox($uid,$mid)
 {
   $query = "INSERT INTO sis_Inbox_Status(MsgID,UserID,StatusID) SELECT '".$mid."' as MsgID,'".$uid."' as UserID,-1 as StatusID WHERE (SELECT COUNT(*) as xCount FROM sis_Inbox_Status WHERE UserID='".$uid."' AND MsgID='".$mid."')=0
             UPDATE sis_Inbox_Status SET StatusID=-1 WHERE UserID='".$uid."' AND MsgID='".$mid."'";
   $exec  = $this->db->query($query);
   return $exec;  
 }
 
}
?>