
<?php
Class mod_institution extends CI_Model
{
  
  function listofmods(){
    $modlist = array( '1' => array('key' => 'department', 'txt' => 'Department'),
										  '2' => array('key' => 'position', 'txt' => 'Position Title'),
										  '3' => array('key' => 'civilstatus', 'txt' => 'Civil Status'),
										  '4' => array('key' => 'nationality', 'txt' => 'Nationality'),
										  '5' => array('key' => 'religion', 'txt' => 'Religion'),
										  '6' => array('key' => 'otherschool', 'txt' => 'Other School'),
										  '7' => array('key' => 'building', 'txt' => 'Building'),
										  '8' => array('key' => 'rooms', 'txt' => 'Rooms'));
    return $modlist;	
  }
  
   /* Returns all menus under setup manager/academic institution */
	function menus()
	{		
		return $this->db->get_where('es_menus', array('moduleid' => 5,'level'=>2))->result();
	}
	
}
?>