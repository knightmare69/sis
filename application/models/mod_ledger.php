<?php
Class mod_ledger extends CI_Model
{

 function call_SP($param=null)
 {
  $result = false;
  if($param!='')
   {
    $query = "EXEC dbo.sp_studentledger @StudentNo ='".$param."';";
    //$query = "EXEC dbo.sp_subsidiaryledger @StudentNo ='".$param."';";
    $result = $this->db->query($query);
    return $result->result();	
   }  
  return $result;
 }
 
 function fetch_row($param='')
 {
  $row = $this->call_SP($param);
  return $row;
 }
 

 
 function generate_tablev1($row) //sp_studentledger
 {
	$xdisplay ="<table class='table table-bordered'>
                <thead><tr>
                           <th align='center'><small>AY TERM</small></th>
                           <th>DATE</th>
                           <th>TRANS. CODE</th>
                           <th>REF. NO.</th>
                           <th>DEBIT</th>
                           <th>CREDIT</th>
                           <th>BALANCE</th>
                           <th>REMARKS</th>
						   <th>POSTED</th>
						   <th>DATE POSTED</th>
                        </tr></thead><tbody><tr><td colspan='10' class='text-center'> <i class='fa fa-warning fa-2x txt-color-yellow'></i> Nothing to display!. No Record is Found.</td></tr></tbody></table>";
  if($row && $row!='')
  {
   $xrow_span = 1;
   
   $totaldebit = 0.00;
   $totalcredit = 0.00;
   $totalbalance = 0.00;
   $initial = '';
   
   $xgroupdetail = '';
   $xdisplay = "<table class='table table-bordered'>";
   $xdisplay .="<thead><tr>
                           <th align='center'><small>AY TERM</small></th>
                           <th>DATE</th>
                           <th>TRANS. CODE</th>
                           <th>REF. NO.</th>
                           <th>DEBIT</th>
                           <th>CREDIT</th>
                           <th>BALANCE</th>
                           <th>REMARKS</th>
						   <th>POSTED</th>
						   <th>DATE POSTED</th>
                        </tr></thead><tbody>";
   
   $xfcss = $this->colorcoding('Footer');						
   foreach($row as $rs)
   {
    $xayterm  = property_exists($rs,'AcademicYearTerm')? $rs->AcademicYearTerm : '';
	$xtransdate = property_exists($rs,'TransDate')? $rs->TransDate : '';
	$xtransid     = property_exists($rs,'TransID')? $rs->TransID : '';
	$xrefno    = property_exists($rs,'Referenceno')? $rs->Referenceno : '';
	$xdebit   = property_exists($rs,'Debit')? $rs->Debit : '0.00';
	if($xdebit==0.00){$xdebit='0.00';}
	$xcredit   = property_exists($rs,'Credit')? $rs->Credit : '0.00';
	if($xcredit==0.00){$xcredit='0.00';}
	$xbalance  = property_exists($rs,'Balance')? $rs->Balance : '0.00';
	if($xbalance==0.00){$xbalance='0.00';}
	$xremarks  = property_exists($rs,'Remarks')? $rs->Remarks : '';
	$xposted  = property_exists($rs,'Posted')? $rs->Posted : 0;
	$xchkposted  = ($xposted==1)? "<input type='checkbox' onclick='return false;' onchange='return false;' checked/>" : "<input type='checkbox' onclick='return false;' onchange='return false;' />";
	$xdatepost  = property_exists($rs,'DatePosted')? $rs->DatePosted : '';
	$xnonledger  = property_exists($rs,'NonLedger')? $rs->NonLedger : 0;
	
	if($xposted== 1 and $xnonledger==0)
	{$xcss = $this->colorcoding('Posted');}
	elseif(($xposted== 1 and $xnonledger==1)||($xposted== 0 and $xnonledger==1))
	{$xcss = $this->colorcoding('NonLedger');
	 $xremarks  = $xremarks.' <b>(Non-Legder)</b>';
	}
	else
	{$xcss = '';}
	
    if(($initial=='' || ($initial != '' && $initial != $xayterm))&& $xtransid!='')
    {
	 if($initial != '' && $initial != $xayterm)
	 {
	  $totalbalance = $totaldebit - $totalcredit;
	  $totalbalance = round($totalbalance, 2);
	  if($totalbalance<0)
	  {
	   $totalbalance = $totalbalance*-1;
	   $totalbalance = '('.$this->PutDecimal($totalbalance).')';
	  }
	  else
	  {
	   $totalbalance = $this->PutDecimal($totalbalance);
	  }
	  $xrow_span = $xrow_span + $xcount;
	  $xgroupdetail.=  "<tr ".$xfcss.">
                         <td colspan=4><center> *** Ending Balance for <b>".$initial."</b>***</center></td>
                         <td>Php</td>
                         <td><b>".$totalbalance."</b></td>
                         <td>Term Balance:<b>".$totalbalance."</b></td>
						 <td></td>
						 <td></td>
                      </tr>";
	 $xdisplay .= str_replace("(xmarker)","rowspan='".$xrow_span."'",$xgroupdetail);
	 $xrow_span = 1;
	 }
	$initial = $xayterm;
	
	if($xposted==1)
	{
	$totaldebit = $totaldebit + $xdebit;
	$totalcredit = $totalcredit + $xcredit;
	$totalbalance = $totaldebit - $totalcredit;
	$totalbalance = round($totalbalance, 2);
	 if($totalbalance<0)
	 {
	  $totalbalance = $totalbalance*-1;
	  $totalbalance = '('.$this->PutDecimal($totalbalance).')';
	 }
	 else
	 {
	  $totalbalance = $this->PutDecimal($totalbalance);
	 }
	}
	$xcount = 1;
	
	$xtrim_ayterm = str_replace(" ","<br>",$xayterm);
	
	$xgroupdetail = "<tr ".$xcss.">
                         <td (xmarker) class='bg-color-white' align='center' style='alignment-adjust: central;' ><b>".$xtrim_ayterm."</b></td>
                         <td>".$xtransdate."</td>
                         <td>".$xtransid."</td>
                         <td>".$xrefno."</td>
                         <td>".$this->PutDecimal($xdebit)."</td>
                         <td>".$this->PutDecimal($xcredit)."</td>
                         <td>".$totalbalance."</td>
                         <td>".$xremarks."</td>
						 <td><center>".$xchkposted."</center></td>
						 <td>".$xdatepost."</td>
                   </tr>";
    }
	elseif($initial != '' && $initial == $xayterm && $xtransid!='')
    {
	
	if($xposted==1)
	{
	$totaldebit = $totaldebit + $xdebit;
	$totalcredit = $totalcredit + $xcredit;
	$totalbalance = $totaldebit - $totalcredit;
	$totalbalance = round($totalbalance, 2);
	 if($totalbalance<0)
	 {
	  $totalbalance = $totalbalance*-1;
	  $totalbalance = '('.$this->PutDecimal($totalbalance).')';
	 }
	 else
	 {
	  $totalbalance = $this->PutDecimal($totalbalance);
	 }
	}
	$xcount = $xcount + 1;
    
	$xgroupdetail.= "<tr ".$xcss.">
                         <td>".$xtransdate."</td>
                         <td>".$xtransid."</td>
                         <td>".$xrefno."</td>
                         <td>".$this->PutDecimal($xdebit)."</td>
                         <td>".$this->PutDecimal($xcredit)."</td>
                         <td>".$totalbalance."</td>
                         <td>".$xremarks."</td>
						 <td><center>".$xchkposted."</center></td>
						 <td>".$xdatepost."</td>
                   </tr>";
    }
   }
   if($xgroupdetail!='')
   {
	 $totalbalance = $totaldebit - $totalcredit;
	 $totalbalance = round($totalbalance, 2);
	 if($totalbalance<0)
	 {
	  $totalbalance = $totalbalance*-1;
	  $totalbalance = '('.$this->PutDecimal($totalbalance).')';
	 }
	 else
	 {
	  $totalbalance = $this->PutDecimal($totalbalance);
	 }
	 $xrow_span = $xrow_span + $xcount;
	 $xgroupdetail.=  "<tr ".$xfcss.">
                         <td colspan=4><center> *** Ending Balance for <b>".$initial."</b>***</center></td>
                         <td>Php</td>
                         <td><b>".$totalbalance."</b></td>
                         <td>Term Balance:<b>".$totalbalance."</b></td>
						 <td></td>
						 <td></td>
                      </tr>";
	 $xdisplay .= str_replace("(xmarker)","rowspan='".$xrow_span."'",$xgroupdetail);
	 $xrow_span = 1;
	 $xgroupdetail='';
   }
   $xdisplay .="<tbody></table>";
  }
  return $xdisplay;
 }
 
 
 
 function putDecimal($value) 
 {
  return number_format($value, 2, '.', '');
 }
 
 function colorcoding($value)
 {
  if($value == '' || $value == ' ' || $value=='Blank')
  {
   return "class='bg-color-white'";
  }
  elseif($value == 'Footer' || $value == 'footer')
  {
   return "class='bg-color-blueLight' style='font-size:8pt;'";
  }
  elseif($value == 'Posted' || $value == 'posted')
  {
   return "style='background-color:#F3F9FF;'";
  }
  elseif($value == 'NonLedger' || $value == 'nonledger')
  {
   return "style='background-color:#FF8080;'";
  }
 }
 
}
?>