<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mod_regmonitoring extends CI_Model
{
 function sp_GetListofRegistration($opt=0,$termid=0,$progid=0,$regid=0,$limit=0)
 {
  $query = 'EXEC sisGetRegistrations @opt='.$opt.',@TermID='.$termid.',@ProgID='.$progid.',@RegID='.$regid.',@Limit='.$limit.';';
  $result = $this->db->query($query);
  return $result;
 }
 
 function GenerateTable($termid=0,$progid=0,$regid=0,$count=1)
 {
  $xinit=0;
  $xlast=0;
  
  $table = '<table id="tblist" class="table table-bordered" data-initial="<xinit>" data-initial="<xlast>">
             <thead>
			  <tr>
			   <th>Reg.No</th>
			   <th>Reg Date</th>
			   <th>StudentNo</th>
			   <th>FullName</th>
			   <th>Program</th>
			   <th>Year Level</th>
			   <th>Total Assessment</th>
			   <th>Validation Date</th>
			   <th>Online Enrolled</th>
			  </tr>
			 </thead>
			 <tbody>
			  <xdata>
			 </tbody>
			</table>';
  $tdata = '<tr><td colspan="9"><center><i class="fa fa-warning"></i> No Data Available.</center></td></tr>';
  $result = $this->sp_GetListofRegistration(0,$termid,$progid,$regid,50)->result();
  if($result)
  {
   $tmpdata ='';
   foreach($result as $rs)
   {
    $regid = property_exists($rs,'RegID')? $rs->RegID : '';
    $regdate = property_exists($rs,'RegDate')? $rs->RegDate : '';
    $studno = property_exists($rs,'StudentNo')? $rs->StudentNo : '';
    $stdname = property_exists($rs,'Fullname')? $rs->Fullname : '';
    $progcode = property_exists($rs,'Program')? $rs->Program : '';
    $yrlvl = property_exists($rs,'YearLevel')? $rs->YearLevel : '';
    $totass = property_exists($rs,'TotalAssessment')? $rs->TotalAssessment : '0.00';
    $validdate = property_exists($rs,'ValidationDate')? $rs->ValidationDate : '';
    $isonline = property_exists($rs,'IsOnlineEnrolment')? $rs->IsOnlineEnrolment : 0;
    $tmpdata.='<tr data-count="'.$count.'" data-count="'.$count.'">
			   <td>'.$regid.'</td>
			   <td>'.$regdate.'</td>
			   <td>'.$studno.'</td>
			   <td>'.$stdname.'</td>
			   <td>'.$progcode.'</td>
			   <td>'.$yrlvl.'</td>
			   <td>'.$totass.'</td>
			   <td>'.$validdate.'</td>
			   <td>'.$isonline.'</td>
			  </tr>';
			  
	if($xinit==0)
    {$xinit=$count;}
    else
    {$xlast=$count;}	
	
    $count++;			  
   }
   if($tmpdata!='')
   {$tdata=$tmpdata;}
  }
  $table=str_replace('<xinit>',$xinit,$table);
  $table=str_replace('<xlast>',$xlast,$table);
  $table=str_replace('<xdata>',$tdata,$table);
  return $table;
 }

 function GetTotalCount($termid=0,$progid=0,$regid=0)
 {
  $result = $this->sp_GetListofRegistration(1,$termid,$progid,$regid,0)->row();
  if($result)
  {
   $count = property_exists($result,'RCount')? $result->RCount : 0;
  }
  return $count;
 }
}
?>
