
<?php
Class mod_religion extends CI_Model
{
  
  	/* Returns all the religion */
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('es_religion');				
		$this->db->order_by("religion", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get()->result();
	}
	
	function del_record($indexid=0){
		$this->db->where('IndexID', $indexid);
		return $this->db->delete('es_religion');
	}
	
	function sav_record($tid=-1,$name='', $short='', $active=0){
		
		$success=false;
		
		if($tid>0){
			$data = array(
               'Religion' => $name,
               'ShortName' => $short,
               'Inactive' => $active
            );

			$this->db->where('IndexID', $tid);
			$success = $this->db->update('es_religion', $data);

		}
		else
		{
			$data = array('Religion' => $name ,'ShortName' => $short);
			$success = $this->db->insert('es_religion', $data); 
		}
		return $success;
	}
}
?>