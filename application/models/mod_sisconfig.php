<?php
Class mod_sisconfig extends CI_Model
{
 function sp_configMgmt($opt=0,$key='',$value='',$sec='',$caption='',$type='',$remarks='',$seqno=0,$listopt=1,$group='',$global=0,$hidden=0)
 {
  $query = "EXEC sp_ConfigMgmt @Opt='".$opt."',@Key='".$key."',@Section='".$sec."',@Caption='".$caption."',@DataType='".$type."',@Value='".$value."',@Remarks='".$remarks."',@SeqNo=".$seqno.",@ListOption=".$listopt.",@Group='".$group."',@Global=".$global.",@IsHidden=".$hidden.";";
  $result = $this->db->query($query);
  return $result;
 }
 
 function sp_xconfigMgmt($indx=0,$value='')
 {
   $query = "UPDATE ES_Configuration SET Value='".$value."' WHERE IndexID=".$indx;
   $result = $this->db->query($query);
   return $result;
 }
 
 function loadlist($opt=0)
 {
  $data='';
  $tbl='<xdata>';
  $result=false;
  
  if($opt==1)
  {
   $data='<tr><td colspan="2"><i class="fa fa-warning"></i> No Data available.</td></tr>';
   $tbl='<table class="table table-bordered">
		 <thead><tr><th>Config</th><th>Value</th></tr></thead>
		 <tbody><xdata></tbody>
		 </table>';
   $result=$this->sp_configMgmt()->result();		
  }
  elseif($opt==2)
  {
   $data='';
   $tbl='<select class="form-control"><option value="" selected disabled> -Select One- </option><xdata></select>';
   $result=$this->sp_configMgmt('0.1')->result();
  }
  elseif($opt==3)
  {
   $data='<tr><td colspan="2"><i class="fa fa-warning"></i> No Data available.</td></tr>';
   $tbl='<table class="table table-bordered">
		 <thead><tr><th>Config</th><th>Value</th></tr></thead>
		 <tbody><xdata></tbody>
		 </table>';
   $result=$this->sp_configMgmt('0.3')->result();
  }
  
  if($result)
  {
   $tmpdata='';
   foreach($result as $rs)
   {
    $seqno= property_exists($rs,'SeqNo')? $rs->SeqNo :0;
    $indx= property_exists($rs,'IndexID')? $rs->IndexID :0;
    $key= property_exists($rs,'Key')? $rs->Key :'';
    $section=property_exists($rs,'Section')? $rs->Section :'';
    $type=property_exists($rs,'DataType')? $rs->DataType :'';
    $listopt=property_exists($rs,'ListOption')? $rs->ListOption :'';
    $caption=property_exists($rs,'Caption')? $rs->Caption :'';
    $group=property_exists($rs,'GroupName')? $rs->GroupName :'';
    $value=property_exists($rs,'Value')? $rs->Value :'';
    $remarks=property_exists($rs,'Remarks')? $rs->Remarks :'';
    //$global=property_exists($rs,'Global')? $rs->Global :0;
    $hidden=property_exists($rs,'IsHidden')? $rs->IsHidden :0;
    
	if($opt==1 || $opt==3)
	{
	 if($caption!='' && $hidden==0 && $key!='' && $seqno!=0)
	 {
	  if($type=='header')
	   $tmpdata.='<tr style="background-color:#95bdc5;"><td colspan="2"><b>'.$caption.'</b></td></tr>';
	  else
	   $tmpdata.='<tr><td>'.utf8_encode($caption).'</td><td>'.$this->getListOption($listopt,(($opt==1)?$indx:$key),$value).'</td></tr>';
	 }
	}
	else
	{
	 $tmpdata.='<option value="'.$caption.'">'.$caption.'</option>';
	}
   }
   
   if($tmpdata!='')
   {$data=$tmpdata;}
  }
  
  return str_replace('<xdata>',$data,$tbl);
 }
 
 
 function getListOption($opt=0,$key='',$value=0)
 {
  $this->load->model('mod_main');
  $input='';
  if($opt==4)
  {
   if($key=='UseLocalConfig')
   {
   $input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this)">
              <option value="0" '. ($value==1?'':'selected') .'>Yes</option>
			  <option value="1" '. ($value==1?'selected':'') .'>No</option>
			</select>';
   }
   else
   {
   $input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this)">
              <option value="0" '. ($value==1?'':'selected') .'>No</option>
			  <option value="1" '. ($value==1?'selected':'') .'>Yes</option>
			</select>';
   }   
  }
  elseif($opt==0)
  {
   $campus = $this->mod_main->get_campuslist();
   $input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this)"><xdata></select>';
   $data='';
   foreach($campus as $rs)
   {$data.='<option value="'.$rs->CampusID.'" '. ($value==$rs->CampusID?'selected':'') .'>'.$rs->CampusName.'</option>';}   
   $input = str_replace('<xdata>',$data,$input);
  }   
  elseif($opt==1 or $opt==2 or $opt==3)
  {
   $this->load->model('mod_main');
   $ayterm = $this->mod_main->get_ayterm(1000);
   $input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);"><xdata></select>';
   $data='';
   foreach($ayterm as $rs)
   {$data.='<option value="'.$rs->TermID.'" '. ($value==$rs->TermID?'selected':'') .'>'.$rs->AcademicYearTerm.'</option>';}   
   $input = str_replace('<xdata>',$data,$input);
  }   
  elseif($opt==6)
  {$input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);">
              <option value="0" '. ($value==0?'selected':'') .'>Manual</option>
			  <option value="1" '. ($value==1?'selected':'') .'>Compute base on StudentNo</option>
			  <option value="2" '. ($value==2?'selected':'') .'>Compute by Scholastic Deliquency</option>
			  <option value="3" '. ($value==3?'selected':'') .'>Plus 1 Every 1st Semester</option>
			</select>';}
  elseif($opt==12)
  {$input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);">
              <option value="0" '. ($value==0?'selected':'') .'>No Charge</option>
			  <option value="1" '. ($value==1?'selected':'') .'>Charge Once</option>
			  <option value="2" '. ($value==2?'selected':'') .'>Charge per Section</option>
			  <option value="3" '. ($value==3?'selected':'') .'>Charge per Transaction</option>
			</select>';}
  elseif($opt==13)
  {$input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);">
              <option value="1" '. ($value==1?'selected':'') .'>51x51mm</option>
			  <option value="0" '. ($value==1?'':'selected') .'>45x35 mm</option>
			</select>';}
  elseif($opt==14)
  {$input ='<select data-pointer="'.$key.'" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" onchange="modifyvalue(this);">
              <option value="0" '. ($value==0?'selected':'') .'>Year/Term, SeqNo</option>
			  <option value="1" '. ($value==1?'selected':'') .'>Year/Term, Code, Title</option>
			  <option value="2" '. ($value==2?'selected':'') .'>Year/Term, Title, Code</option>
			</select>';}
  elseif($opt==9)
  {$input ='<input data-pointer="'.$key.'" type="text" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" value="'.$value.'" onchange="modifyvalue(this);"/>';}
  else
  {$input ='<input data-pointer="'.$key.'" type="text" style="width:100%;height:100%;border:none;outline:none;box-shadow:none;" value="'.$value.'" onchange="modifyvalue(this);"/>';}
 
  return $input;
 }
 
}
?>