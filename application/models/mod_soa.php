<?php
Class mod_soa extends CI_Model
{
 
 function sp_AYTerms($studentno='')
 {
  $this->db->select('RegID,TermID,dbo.fn_AcademicYearTerm(TermID) AS AYTerm');
  $this->db->from('ES_Registrations');
  $this->db->where('StudentNo',$studentno);
  $this->db->ORDER_BY('RegID', 'desc');
  return $this->db->get()->result();
 }
 
 function sp_GetRegInfo($studentno='',$regid=0)
 {
  $this->db->select('TermID,dbo.fn_AcademicYearTerm(TermID) AS AYTerm,
                     CampusID,dbo.fn_CampusName(CampusID) AS Campus,
	                 CollegeID,dbo.fn_CollegeName(CollegeID) AS College,
	                 ProgID,dbo.fn_ProgramName(ProgID) AS Program,
	                 YearLevelID,dbo.fn_YearLevel(YearLevelID) AS YearLevel');
  $this->db->from('ES_Registrations');
  $this->db->where('StudentNo',$studentno);
  $this->db->where('RegID',$regid);
  return $this->db->get()->row();
 }
 
 function sp_SOA($studentno='',$campusid=0,$termid=0,$collegeid=0,$programid=0,$yrlvlid=0)
 {
  $dateasof = date('Y/m/d H:i:s A');
  $query="EXEC dbo.ES_rptStatementofAccount_College @CampusID='".$campusid."', @TermID=".$termid.", @FilterBy = 0, @Bulk = 0, @ByLevel = 0, @CollegeID = 0, @ProgramID = 0, @YearLevelID = 0, @StudentNo = '".$studentno."', @DateAsOF = '".$dateasof."', @IsMidtermFinal = 2, @PrintSOALetter = 0, @PrintedBy = 'allen'";
  $result = $this->db->query($query);
  if($result)
  {return $result;}
  else
  {return false;}
 }
 
 function sp_Assessment($studentno='',$campusid=0,$termid=0,$collegeid=0,$programid=0,$yrlvlid=0)
 {
  $dateasof = date('Y/m/d H:i:s A');
  $query="EXEC dbo.ES_rptStatementofAccount_Assessment_College @CampusID=".$campusid.", @TermID = ".$termid.", @FilterBy = 0, @Bulk = 0, @ByLevel = 0, @CollegeID=".$collegeid.", @ProgramID =".$programid.", @YearLevelID=".$yrlvlid.", @StudentNo='".$studentno."', @DateAsOF = '".$dateasof."'";
  $result = $this->db->query($query);
  if($result)
  {return $result;}
  else
  {return false;}
 }
 
 function sp_Payment($studentno='',$campusid=0,$termid=0,$collegeid=0,$programid=0,$yrlvlid=0)
 {
  $dateasof = date('Y/m/d H:i:s A');
  $query="EXEC dbo.ES_rptStatementofAccount_OfficialReceipts_College @CampusID=".$campusid.", @TermID = ".$termid.", @FilterBy = 0, @Bulk = 0, @ByLevel = 0, @CollegeID=".$collegeid.", @ProgramID =".$programid.", @YearLevelID=".$yrlvlid.", @StudentNo='".$studentno."', @DateAsOF = '".$dateasof."'";
  $result = $this->db->query($query);
  if($result)
  {return $result;}
  else
  {return false;}
 }
 
 function cboRegAYTerms($studentno='',$select=0)
 {
  $cbo = "<select id='cboayterm' name='cboayterm' class='form-control'><option value='' selected disabled>Select one</option><xdata></select>";
  $data="";
  $aytermopt = $this->sp_AYTerms($studentno); 
  if($aytermopt)
  {
   foreach($aytermopt as $rs)
   {
    $regid = property_exists($rs,'RegID')? $rs->RegID : 0;
    $termid = property_exists($rs,'TermID')? $rs->TermID : 0;
    $ayterm = property_exists($rs,'AYTerm')? $rs->AYTerm : '';
	$selected = ($regid==$select)?'selected':'';
    $data.= "<option value='".$regid."' ".$selected.">".$ayterm."</option>";
   }
  } 
  return str_replace('<xdata>',$data,$cbo);
 } 
 
 function tblAssessment($studentno='',$campusid=0,$termid=0,$collegeid=0,$programid=0,$yrlvlid=0)
 {
  $assessment = $this->sp_Assessment($studentno,$campusid,$termid,$collegeid,$programid,$yrlvlid)->result();
  $table = "<table class='table table-bordered'>
			 <thead>
			   <th>Accounts</th>
			   <th align='right'>Assessed</th>
			   <th align='right'>Financial Aid</th>
			   <th align='right'>Net Assessed</th>
			   <th align='right'>Payment</th>
			   <th align='right'>Balance</th>
			 </thead>
			<tbody><xdata></tbody>
			</table>";
  $data = "<tr><td colspan='8'>No Data Available</td></tr>";		
  if($assessment)
  {
   $tmpdata="";
   $maintitle="";
   $title="";
   $totalassess = 0;
   $totalaid = 0;
   $totalnet = 0;
   $totalpay = 0;
   $totalbalance = 0;
   
   foreach($assessment as $rs)
   {
    $xassess=0;
    $xtransid = property_exists($rs,'TransID')? $rs->TransID : 0;
    $xtermid = property_exists($rs,'TermID')? $rs->TermID : 0;
    $xid = property_exists($rs,'IDNo')? $rs->IDNo : '';
    $xtransname = property_exists($rs,'TransName')? $rs->TransName : '';
    $xrefno = property_exists($rs,'ReferenceNo')? $rs->ReferenceNo : 0;
    $xacctname = property_exists($rs,'AcctName')? $rs->AcctName : '';
    $xassfee = property_exists($rs,'Assess Fee')? $rs->{'Assess Fee'} : 0;
    $xdebit = property_exists($rs,'Debit')? $rs->Debit : 0;
    $xcredit = property_exists($rs,'Credit')? $rs->Credit : 0;
    $xdiscount = property_exists($rs,'Discount')? $rs->Discount : 0;
    $xdmcmrefno = property_exists($rs,'DMCMRefNo')? $rs->DMCMRefNo : 0;
    $xtransrefno = property_exists($rs,'TransRefNo')? $rs->TransRefNo : 0;
    $xnonledger = property_exists($rs,'NonLedger')? $rs->NonLedger : 0;
    $xactualpay = property_exists($rs,'ActualPayment')? $rs->ActualPayment : 0;
    $xseqno = property_exists($rs,'SeqNo')? $rs->SeqNo : 0;
    $xspecsubjtransno = property_exists($rs,'SpecialSubjectTransNo')? $rs->SpecialSubjectTransNo : 0;
	
	if($xtransid=2 and $xspecsubjtransno > 0)
	  $title= $xtransname.' #:'.$xrefno.' Special Subject #:'.str_replace('.00','',$xspecsubjtransno);
	else
	  $title= $xtransname.' #:'.$xrefno;
	  
	if ($xassfee != 0)
	  $xassess = $xassfee;
	else
	  $xassess = $xdebit;
	
    $netass = $xassess - $xdiscount;
	$balance = $netass-$xactualpay;
	
    if(($maintitle=="" and $title!="")or $maintitle!=$title)
	{
	 $tmpdata.= "<tr><td colspan='6'><strong>".$title."</strong></td></tr>";
	 $tmpdata.= "<tr><td><strong>".$xacctname."</strong></td>
	              <td align='right'><strong>".number_format($xassess,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($xdiscount,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($netass,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($xactualpay,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($balance,4,".",",")."</strong></td></tr>";
	 $maintitle = $title;
	}
	else
	{
	 $tmpdata.="<tr><td><strong>".$xacctname."</strong></td>
	              <td align='right'><strong>".number_format($xassess,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($xdiscount,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($netass,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($xactualpay,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($balance,4,".",",")."</strong></td></tr>";
	}
	$totalassess += $xassess;
    $totalaid += $xdiscount;
    $totalnet += $netass;
    $totalpay += $xactualpay;
    $totalbalance += $balance;
   }
	
   if($tmpdata!="")
   {
     $tmpdata.="<tr><td><strong>TOTAL</strong></td>
	              <td align='right'><strong>".number_format($totalassess,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($totalaid,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($totalnet,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($totalpay,4,".",",")."</strong></td>
	              <td align='right'><strong>".number_format($totalbalance,4,".",",")."</strong></td></tr>";
    $data=$tmpdata;
   }
  }
  return str_replace('<xdata>',$data,$table);
 }
 
 function tblPayment($studentno='',$campusid=0,$termid=0,$collegeid=0,$programid=0,$yrlvlid=0)
 {
  $payment = $this->sp_Payment($studentno,$campusid,$termid,$collegeid,$programid,$yrlvlid)->result();
  $table = "<table class='table table-bordered'>
			 <thead>
			   <th>OR No</th>
			   <th>Date</th>
			   <th align='right'>Amount</th>
			   <th>Payment Type</th>
			   <th>Non-Ledger Amount</th>
			   <th>Non-Ledger Account</th>
			 </thead>
			<tbody><xdata></tbody>
			</table>";
  $data = "<tr><td colspan='6'>No Data Available</td></tr>";		
  if($payment)
  {
   $tmpdata="";
   $maintitle="";
   $totalpay = 0;
   
   foreach($payment as $rs)
   {
    $title="";
    $refno = property_exists($rs,'ReferenceNo')? $rs->ReferenceNo : 0;
    $date = property_exists($rs,'DateOfPayment')? $rs->DateOfPayment : '';
    $idno = property_exists($rs,'IDNo')? $rs->IDNo : 0;
    $xamount = property_exists($rs,'Amount')? $rs->Amount : 0;
    $xremarks = property_exists($rs,'Remarks')? $rs->Remarks : '';
    $xnonledgeramt = property_exists($rs,'NonLedgerAmount')? $rs->NonLedgerAmount : 0;
    $xnonledgeracct = property_exists($rs,'NonLedgerAccountName')? $rs->NonLedgerAccountName : '';
	
	if($xremarks=="Check Payment (0)")
	{$xremarks="Check Payment";}
	
	if($refno!=0 and $refno!="" and $date!="")
	{$title = $refno." - ".$date;}
	
	if($date!=null and $date!="")
	{$date = date("d/m/Y",strtotime($date));}
	
	if(($maintitle=="" and $title!="")or $maintitle!=$title)
	{
	 $tmpdata.="<tr><td><strong>".$refno."</strong></td><td><strong>".$date."</strong></td><td></td><td></td><td></td><td></td></tr>";
	 $tmpdata.="<tr><td></td>
	                <td></td>
	                <td align='right'><strong>".number_format($xamount,4,".",",")."</strong></td>
	                <td>".$xremarks."</td>
	                <td>".$xnonledgeramt."</td>
	                <td>".$xnonledgeracct."</td></tr>";
					
	 $maintitle = $title;				
	}
	else
	{
	 $tmpdata.="<tr><td></td>
	                <td></td>
	                <td align='right'><strong>".number_format($xamount,4,".",",")."</strong></td>
	                <td>".$xremarks."</td>
	                <td align='right'>".number_format($xnonledgeramt,4,".",",")."</td>
	                <td>".$xnonledgeracct."</td></tr>";
	}
	
   }
	
   if($tmpdata!="")
   {
    $data=$tmpdata;
   }
  }
  return str_replace('<xdata>',$data,$table);
 }
 
}
?>