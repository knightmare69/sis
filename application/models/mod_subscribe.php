<?php
class mod_subscribe extends CI_MODEL
{
	function get_StudentInfo($studentno, $limit=10000, $offset=0)
	{
		$this->db->from('vw_Students');
	    $this->db->where('StudentNo', $studentno);
	    $this->db->limit($limit);
	    $this->db->offset($offset);
	    return $this->db->get()->result();
    }
        
    /* Returns all the ayterms */
	function get_ayterms()
	{
		$query = $this->db->query('SELECT TOP 5 * FROM ES_AYTerm ORDER BY  AcademicYear DESC, SchoolTerm DESC');						
		return $query->result();
	}
        
    function validate($studentno, $birthdate, $termid, $regid, $receiptno='', $date='')
    {
		if(is_numeric($regid)==false)
		{$regid=0;}
		
		if($termid=='')
		{$termid=0;}
		
		if($receiptno=='')
		{$receiptno=0;}
		
		if($date!='' && strtotime($date))
		{
		 $tmpdate = new DateTime($date);
         $date    = $tmpdate->format('m-d-Y');		 
		}	
		else
		 $date    = '';	
	 
		 $query = $this->db->query('SELECT [dbo].[fn_ParentStudentValidation](\'' . $studentno . '\', \'' . $birthdate . '\', ' . $termid . ', ' . $regid . ', \'' . $receiptno . '\', \'' . $date . '\') AS \'Validate\'');						
	     return $query->result();
    }
        
    function icreate($parentid, $studentno, $relationship)
    {
		$success = false;
		
		$data = array(
		'ParentID' => $parentid,
		'StudentNo' => $studentno,
		'Relationship' => $relationship,
		);
		
		$success = $this->db->insert('ES_ParentChildren', $data);
                        
	    return $success;
    }
}
?>