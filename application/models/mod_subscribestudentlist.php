<?php
    class mod_subscribestudentlist extends CI_MODEL
    {
        function get_StudentInfo($username)
        {
            $this->db->from('vw_ParentChildren');
            $this->db->where('ParentID', $username);

	    return $this->db->get()->result();
        }
        
        function unsubscribe($indexid=0){
		$this->db->where('IndexID', $indexid);
		return $this->db->delete('ES_ParentChildren');
	}
    }
?>