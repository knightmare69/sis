
<?php
Class mod_sysusers extends CI_Model
{
  
  function menus(){
    $modlist = array( '1' => array('key' => 'employees', 'txt' => 'Employees'),
					  '2' => array('key' => 'useraccount', 'txt' => 'User Account'),
					  '3' => array('key' => 'usergroups', 'txt' => 'User Groups'));
    return $modlist;	
  }
  
  function cs_getfields($rec){
  	
	switch($rec){
		case 'department':
			$query = "select departmentid as 'ID', DepartmentCode as 'Code', DepartmentName as 'Name', CampusID as 'CampusID' from es_departments";
			break;
		case 'position':
			$query = "select PosnTitleID as 'ID', PositionCode as 'Code', PositionDesc as 'Name' from es_positiontitles";
			break;
		case 'civilstatus':
			$query = "select statusid as ID, StatusDesc as 'Status', Inactive  from es_civilstatus";
			break;
		case 'nationality':
			$query = "SELECT NationalityID AS 'ID', Nationality, Shortname as 'Short Name', Inactive , IsDefault from es_nationalities";
			break;
		default:
			break;
	}
	
	
  	return $this->db->query($query);
  }
  
 function cs_getdata($rec){
 	
	switch($rec){
		case 'department':
			$query = "departmentid as 'ID', DepartmentCode as 'Code', DepartmentName as 'Name', CampusID as 'CampusID'";
			$table='es_departments';
			break;
		case 'position':
			$query = "PosnTitleID as 'ID', PositionCode as 'Code', PositionDesc as 'Name'";
			$table='es_positiontitles';
			break;
		case 'civilstatus':
			$query = "StatusID as 'ID', StatusDesc as 'Status', Inactive";
			$table='es_civilstatus';
			break;
		case 'nationality':
			$query = "NationalityID AS 'ID', Nationality, Shortname as 'Short Name', Inactive , IsDefault";
			$table='es_nationalities';
			break;			
		default:
			break;
	}
	
	$this->db->select($query);
	
  	return $this->db->get($table)->result();
 }
  
  
}
?>