<?php
class mod_translogs extends CI_MODEL
{
 function getTranslogs($where='',$count=500,$order='desc')
 {
  $this->db->select('TOP '.$count.' *');
  $this->db->from('sis_TransactionsLog');
  if($where!=''){$this->db->where($where);}
  $this->db->order_by('EventDate',$order);
  return $this->db->get()->result();	
 }
 
 function xgetTranslogs($where='',$limit=0,$offset=0)
 {
  $this->db->reconnect();
  $query ="SELECT * FROM sis_TransactionsLog ".(($where!='')?" WHERE ".$where:"")." ORDER BY TransID DESC OFFSET ".$offset." ROWS FETCH NEXT ".$limit." ROWS ONLY"; 
  //die($query);
  $result = $this->db->query($query);
  return $result->result();		 
 }
 
 function generate_where($where='')
 {
  $result = '';	 
  if($where!='')
  {
	  
  }	  
  
  return $result;	 
 }	 
 
 function processtranslog($row)
 {
  date_default_timezone_set("Asia/Taipei");
  $data='';
  $lastrow='';
  $latestrow='';
  $xid = '';
  
  if($row)
  {
   foreach($row as $rs)
	{
	  $transid   = $rs->TransID;
	  $user      = $rs->UserID;
	  $date      = $rs->EventDate;
	  $device    = $rs->UserAgent;
	  $module    = $rs->Module;
	  $action    = $rs->Action;
	  $parameter = $rs->Parameters;
	  
	  if($date!='')
	  {
	  $date = strtotime($date);
	  $date = date('Y-m-d H:i:s',$date);
	  }
	  
	  
	  $device = $this->defineuagent($device);
	  
	  if($latestrow=='')
	  {$latestrow=$transid;}
	  $xid.=$transid.'|';
	  $lastrow = $transid;
	  $data.='<tr data-row="'.$transid.'">
			  <td>'.$date.'</td>
			  <td>'.$rs->UserID.'</td>
			  <td>'.$rs->ComputerName.'</td>
			  <td>'.$device.'</td>
			  <td>'.$rs->Module.'</td>
			  <td>'.$rs->Action.'</td>
			  <td>'.$rs->Parameters.'</td>
		     </tr>';
	 
	}
  }
  
  return array($data,$lastrow,$latestrow,$xid);
 }
 
 function producetranslog($row)
 {
  date_default_timezone_set("Asia/Taipei");
  $data='';
  $lastrow='';
  $count=0;
  if($row)
  {
   foreach($row as $rs)
   {
	  $transid   = $rs->TransID;
	  $user      = $rs->UserID;
	  $date      = $rs->EventDate;
	  $device    = $rs->UserAgent;
	  $module    = $rs->Module;
	  $action    = $rs->Action;
	  $parameter = $rs->Parameters;
	  $device = $this->defineuagent($device);
	  
	  if($date!=''){$date = date('Y-m-d H:i:s',strtotime($date)); }
	  $lastrow = $transid;
	  $data[$count] = array($transid
	                         ,$date
							 ,$rs->UserID
							 ,$rs->ComputerName
							 ,$device
							 ,$rs->Module
							 ,$rs->Action
							 ,$rs->Parameters);
      $count++;
   }
  }
  return array($data,$lastrow);
 }
 
 function defineuagent($device='')
 {
  $browser ='';
  if(strpos($device,'Safari')>0)
  {$browser = 'Safari';}
  if(strpos($device,'Mobile Safari')>0)
  {$browser = 'Mobile Safari';}
  if(strpos($device,'Opera')>0)
  {$browser = 'Opera';}
  if(strpos($device,'Firefox')>0)
  {$browser = 'Firefox';}
  if(strpos($device,'Chrome/')>0)
  {$browser = 'Chrome';}

  if($browser!='')
  {$browser ='('.$browser.')';}

  if(strpos($device,'Android')>0)
  {$device = 'Android'.$browser;}
  if(strpos($device,'iPhone')>0)
  {$device = 'iPhone'.$browser;}
  if(strpos($device,'iPad')>0)
  {$device = 'iPad'.$browser;}
  if(strpos($device,'Mac')>0)
  {$device = 'Mac'.$browser;}
  if(strpos($device,'Windows')>0)
  {$device = 'Windows'.$browser;}
  
  return $device;
 }
 
 function getcount()
 {
  $query = "SELECT SUM (row_count) AS RCount FROM sys.dm_db_partition_stats WHERE object_id=OBJECT_ID('sis_TransactionsLog');";
  $row = $this->db->query($query);
  if($row->num_rows()>0)
  {
   $rs = $row->row(); 
   return $rs->RCount;
  }
  else
  {return 'failed';}
 }
 
}
?>	