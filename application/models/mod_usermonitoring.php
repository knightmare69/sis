<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mod_usermonitoring extends CI_Model
{
 function sp_UserMgmtMonitoring($opt=0,$userid=0,$limit=0)
 {
  $query = "EXEC sis_UserMgmtMonitoring @opt='".$opt."',@userid=".$userid.",@limit=".$limit.";";
  $result = $this->db->query($query);
  return $result;
 }
 
 function sp_UserSearch($value='')
 {
  $filter_fields = array('UserName','Email','IDNo','LastName','FirstName','MiddleName','DateCreated');
  $where='';
  if($value!='')
  {
   $filter_data = explode(' ',$value);
   foreach($filter_fields as $fields)
   {
	$string = $fields." like '%<xdata>%'";
    foreach($filter_data as $data){$where = $where.(($where=='')?'':' OR ').str_replace('<xdata>',$data,$string);}	   
   }	  
   
   $this->db->from('vw_sisUsers');
   $this->db->where($where);
   return $this->db->get()->result();
  }
  return false;  
 }
 
 function tbl_UserList($userid=0,$limit=50,$count=1)
 {
  $btn ='<button class="btn btn-sm btn-danger" data-pointer="" onclick="eliminateuser(this);"><i class="fa fa-trash-o"></i></button>';
  $btn.='<button class="btn btn-sm btn-primary" data-pointer="" data-uname="" data-xname="" data-email="" data-telno="" data-idno="" data-type="" data-active="" data-toggle="modal" data-target="#usermodification" onclick="manage_user(this);"><i class="fa fa-gear"></i></button>';
  $isactivated = '<i class="fa fa-square-o"></i>';
	 
  $table = '<table class="table table-bordered table-hover smart-form" id="xuserlist" style="white-space:nowrap;">
             <thead>
			 <tr>
			  <th width="80px">
			       <button class="btn  btn-sm btn-info" onclick="refreshlist();"><i class="fa fa-refresh"></i></button>
			       <button class="btn  btn-sm btn-success" onclick="create_user();" data-toggle="modal" data-target="#usermodification"><i class="fa fa-file-o"></i></button>
			  </th>
			  <th>#</th>
			  <th>UserName</th>
			  <th>IDNo</th>
			  <th>Name</th>
			  <th>Email</th>
			  <th>UserType</th>
			  <th>DateCreated</th>
			  <th>IsActivated</th>
			 </tr>
			 <tr class="second hidden">
			  <th> </th>
			  <th>
               <input type="checkbox" id="xchkall" onclick="optAll();"/>			  
			  </th>
			  <th>
				<label class="input">
					<input type="text" id="eusername" data-pointer="2" name="search_username" placeholder="Search" class="search_init">
				</label>
			  </th>
			  <th>
			    <label class="input">
					<input type="text" id="eidno" data-pointer="3" name="search_idno" placeholder="Search" class="search_init">
				</label>
			  </th>
			  <th>
			    <label class="input">
					<input type="text" id="ename" data-pointer="4" name="search_name" placeholder="Search" class="search_init">
				</label>
			  </th>
			  <th>
			    <label class="input">
					<input type="text" id="eemail" data-pointer="5" name="search_email" placeholder="Search" class="search_init">
				</label>
			  </th>
			  <th>
			    <label class="input">
					<input type="text" id="etype" data-pointer="6" name="search_usertype" placeholder="Search" class="search_init">
				</label>
			  </th>
			  <th>
			    <label class="input">
					<input type="text" id="edatemade" data-pointer="7" name="search_datecreated" placeholder="Search" class="search_init">
				</label>
			  </th>
			  <th></th>
			 </tr>
			 </thead>
			 <tbody>
			 <xdata>
			 </tbody>
			 <tfoot class="hidden">
	            <tr>
			     <td width="80px">'.$btn.'</td>
			     <td><input type="checkbox" value="" onclick="checkopt();"/></td>
			     <td></td>
			     <td></td>
			     <td></td>
			     <td></td>
			     <td></td>
			     <td></td>
			     <td>'.$isactivated.'</td>
	            </tr>
			 </tfoot>
            </table>';
  $data = '<tr><td colspan="9"><center><i class="fa fa-warning"></i> No Data Found!</center></td></tr>';			
  $tbldata = $this->sp_UserMgmtMonitoring('0',$userid,$limit)->result();
  if($tbldata)
  {
   $tmpdata='';
   foreach($tbldata as $rs)
   {
    $userid=property_exists($rs,'UserID')? $rs->UserID : '';
    $username=property_exists($rs,'UserName')? $rs->UserName : '';
    $idno=property_exists($rs,'IDNo')? $rs->IDNo : '';
    $idtype=property_exists($rs,'IDType')? $rs->IDType : 0;
    $email=property_exists($rs,'Email')? $rs->Email : '';
    $telno=property_exists($rs,'TelNo')? $rs->TelNo : '';
    $lastname=property_exists($rs,'LastName')? $rs->LastName : '';
    $firstname=property_exists($rs,'FirstName')? $rs->FirstName : '';
    $middlename=property_exists($rs,'MiddleName')? $rs->MiddleName : '';
    $miname=property_exists($rs,'MiddleName')? $rs->MiddleInitial : '';
    $isactivated=property_exists($rs,'IsActivated')? $rs->IsActivated : 0;
    $datecreated=property_exists($rs,'DateCreated')? $rs->DateCreated : '';
	
	if($userid!='')
	{
	 if($idtype==1)
	   $utype = 'Student';
	 elseif($idtype==2)
	   $utype = 'Parent';
	 elseif($idtype==3)
	   $utype = 'Faculty';
	 elseif($idtype==-1)
	   $utype = 'Administrator';
	 else
       $utype = 'Undefined';
	 	 
     $fullname = '';
	 $xfullname='';
	 
     if($lastname!='')
     {
	  $fullname.=$lastname.', ';
	  $xfullname.=$lastname.'|';
	 }	 
     if($firstname!='')
     {
	  $fullname.=$firstname;
	  $xfullname.=$firstname.'|';
	 }	 
     if($middlename!='')
     {
	  $xfullname.=$middlename.'|';
	 }	 
     if($miname!='')
     {
	  $xfullname.=$miname.'|';
	 }	 
	
	 $btn ='<button class="btn btn-sm btn-danger" data-pointer="'.$userid.'" onclick="eliminateuser(this);"><i class="fa fa-trash-o"></i></button>';
     $btn.='<button class="btn btn-sm btn-primary" data-pointer="'.$userid.'" data-uname="'.utf8_encode($username).'" data-xname="'.utf8_encode($xfullname).'" data-email="'.utf8_encode($email).'" data-telno="'.utf8_encode($telno).'" data-idno="'.$idno.'" data-type="'.$idtype.'" data-active="'.$isactivated.'" data-toggle="modal" data-target="#usermodification" onclick="manage_user(this);"><i class="fa fa-gear"></i></button>';
         if($username=='knightmare69'){continue;}
	 $isactivated = '<i class="fa '.(($isactivated==1)?'fa-check-square-o"':'fa-square-o').'"></i>';
	 $tmpdata.='<tr data-row="'.$userid.'">
			     <td style="width:80px;">'.$btn.'</td>
			     <td><input type="checkbox" id="xchkuser'.$count.'" value="'.$userid.'" onclick="checkopt();"/></td>
			     <td>'.utf8_encode($username).'</td>
			     <td>'.$idno.'</td>
			     <td>'.utf8_encode($fullname).'</td>
			     <td>'.utf8_encode($email).'</td>
			     <td>'.$utype.'</td>
			     <td>'.$datecreated.'</td>
			     <td>'.$isactivated.'</td>
	            </tr>';
	 
	 $count++;
	}
   }
   if($tmpdata!='')
   {$data=$tmpdata;}
  }
  
  $table = str_replace('<xdata>',$data,$table);
  return $table;
 }
 
 function Arr_UserList($userid=0,$limit=50,$count=1,$search='')
 {
  $content = array();
  $lastid  = 0;
  $result  = false;
  $index   = 0;
  
  if($search=='')
   $tbldata = $this->sp_UserMgmtMonitoring('0',$userid,$limit)->result();
  else
   $tbldata = $this->sp_UserSearch($search);
  
  if($tbldata)
  {
   $tmpdata=array();
   foreach($tbldata as $rs)
   {
    $userid=property_exists($rs,'UserID')? $rs->UserID : '';
    $username=property_exists($rs,'UserName')? $rs->UserName : '';
    $idno=property_exists($rs,'IDNo')? $rs->IDNo : '';
    $idtype=property_exists($rs,'IDType')? $rs->IDType : 0;
    $email=property_exists($rs,'Email')? $rs->Email : '';
    $telno=property_exists($rs,'TelNo')? $rs->TelNo : '';
    $lastname=property_exists($rs,'LastName')? $rs->LastName : '';
    $firstname=property_exists($rs,'FirstName')? $rs->FirstName : '';
    $middlename=property_exists($rs,'MiddleName')? $rs->MiddleName : '';
    $miname=property_exists($rs,'MiddleName')? $rs->MiddleInitial : '';
    $isactivated=property_exists($rs,'IsActivated')? $rs->IsActivated : 0;
    $datecreated=property_exists($rs,'DateCreated')? $rs->DateCreated : '';
	
	if($userid!='')
	{
	 $lastid=$userid;
	 if($idtype==1)
	   $utype = 'Student';
	 elseif($idtype==2)
	   $utype = 'Parent';
	 elseif($idtype==3)
	   $utype = 'Faculty';
	 elseif($idtype==-1)
	   $utype = 'Administrator';
	 else
       $utype = 'Undefined';
	 	 
     $fullname = '';
	 $xfullname='';
	 
     if($lastname!='')
     {
	  $fullname.=$lastname.', ';
	  $xfullname.=$lastname.'|';
	 }	 
     if($firstname!='' && $lastname!=$firstname)
     {
	  $fullname.=$firstname;
	  $xfullname.=$firstname.'|';
	 }	 
     if($middlename!='' && $lastname!=$middlename)
     {
	  $xfullname.=$middlename.'|';
	 }	 
     if($miname!='' && $lastname!=$middlename)
     {
	  $xfullname.=$miname.'|';
	 }	 
	
	 $btn ='<button class="btn btn-sm btn-danger" data-pointer="'.$userid.'" onclick="eliminateuser(this);"><i class="fa fa-trash-o"></i></button>';
     $btn.='<button class="btn btn-sm btn-primary" data-pointer="'.$userid.'" data-uname="'.utf8_encode($username).'" data-xname="'.utf8_encode($xfullname).'" data-email="'.utf8_encode($email).'" data-telno="'.utf8_encode($telno).'" data-idno="'.$idno.'" data-type="'.$idtype.'" data-active="'.$isactivated.'" data-toggle="modal" data-target="#usermodification" onclick="manage_user(this);"><i class="fa fa-gear"></i></button>';
	 $isactivated = '<i class="fa '.(($isactivated==1)?'fa-check-square-o"':'fa-square-o').'"></i>';
	 if($username=='knightmare69'){continue;}
	 $tmpdata = array($userid
	                 ,$btn
	                 ,'<input type="checkbox" id="xchkuser'.$count.'" value="'.$userid.'" onclick="checkopt();"/>'
					 ,utf8_encode($username)
					 ,$idno
					 ,utf8_encode($fullname)
					 ,utf8_encode($email)
					 ,$utype
					 ,$datecreated
					 ,$isactivated);
	 
     $content[$index] = $tmpdata;
	 $count++;
	 $index++;
	}
   }
   if($count>0) $result=true;
  }
  
  return array('content'=>$content
              ,'lastid' =>$lastid
			  ,'result'=>$result);
 } 

function recreate_vcode($user,$email)
{
 $username = utf8_decode($user);
 $email = utf8_decode($email);
 
 $today = new DateTime('NOW');
 $validdate = new DateTime('NOW');
 date_add($validdate,date_interval_create_from_date_string("15 days"));

 $validcode = md5('PRISMS::'. $username . ':'. $email .': :' . $validdate->format( 'Y-m-d H:i:s' ).'::ONLINE') ;

 $data=array('ValidationCode'=>$validcode,
		     'ValidityDate'=>$validdate->format( 'Y-m-d H:i:s' ));
		  
 $this->db->where('UserName', $username);    
 $result = $this->db->update('es_membership',$data);
 if ($result == 0)
 {
  $output = '';
 } 
 else
 {  
  $output = $validcode . '|' . $validdate->format( 'Y-m-d H:i:s' ) ; 
 }

return $output;
}

function manage_user($opt=0,$prevuname='',$username='',$pwd='',$lname='',$fname='',$mname='',$miname='',$email='',$telno='',$idno='',$utype=0,$active=0)
{
  $result=false;
  
  $username = utf8_decode($username);
  $email = utf8_decode($email);
  $pwd = utf8_decode($pwd);
  
  $data=array('UserName'=>$username,
              'Email'=>$email,
              'LastName'=>utf8_decode($lname),
              'FirstName'=>utf8_decode($fname),
              'MiddleName'=>utf8_decode($mname),
              'MiddleInitial'=>utf8_decode($miname),
              'TelNo'=>utf8_decode($telno),
              'IDNo'=>utf8_decode($idno),
              'IDType'=>$utype,
              'IsActivated' => $active);
  
  if($opt==0)
  {
   $data['Password']= md5($pwd);
              
   $today = new DateTime('NOW');
   $validdate = new DateTime('NOW');
  
   date_add($validdate,date_interval_create_from_date_string("15 days"));
   $validcode = md5('PRISMS::'. $username . ':'. $email .':'. $pwd . ':' . $validdate->format( 'Y-m-d H:i:s' ).'::ONLINE') ;
  
   
   $data['ValidationCode'] = $validcode;
   $data['ValidityDate']=$validdate->format( 'Y-m-d H:i:s' );	
   $data['DateCreated']=$today->format( 'Y-m-d H:i:s' );
   $result = $this->db->insert('es_membership',$data);
  }
  else
  {
   $this->db->where('UserName', $prevuname);
   $result = $this->db->update('es_membership',$data);
  }  
  
  if ($result == 0)
  {
   $output = '';
  } 
  else
  {
   if($opt==0)
    $output = $validcode . '|' . $validdate->format( 'Y-m-d H:i:s' ); 
   else
    $output = "success";
  }
  
  return $output;
 }
 
 function manage_user_subscript($opt='0',$prevuser='',$user='',$student='',$relation='')
 {
  $result=true;
  $output=false;
  if($opt==0)
  {
 	$this->db->where('ParentID', $prevuser);
    $result = $this->db->get('ES_ParentChildren'); 
	$result = $result->result();
  }  
  else if($opt==1 && $student!='' && $relation!='')
  {
   $data=array('ParentID'=>utf8_decode($user),
              'StudentNo'=>utf8_decode($student),
              'Relationship'=>utf8_decode($relation));	
   $result = $this->db->insert('ES_ParentChildren',$data);
  }
  else if($opt==2)
  {
 	$this->db->where('ParentID', $prevuser);
    $result = $this->db->delete('ES_ParentChildren'); 
  }
  
  if($result && $opt==1)
   $output = true;  
  else if($result && $opt==2)
   $output = true;  
  else if($result && $opt==0)
   $output = $result;  
  else
   $output = false;	

  return $output;  
 }
}
?>