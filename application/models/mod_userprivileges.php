<?php
Class mod_userprivileges extends CI_Model
{
 private $pages      = array();
 private $privileges = array();
 
 function gen_page_list()
 {
  $result=false;
  $page_path = str_replace("index.php","application/third_party/access/pages.php",$_SERVER['SCRIPT_FILENAME']);
  if(file_exists($page_path))
  {
   include_once($page_path);	  
   $result=((isset($pages) && is_array($pages))?$pages:false);	  
  }
  return $result;
 }
 
 function get_privileges_list($userid='',$idno='',$idtype='')
 {
  $filepath='';
  if($userid!='' && $idno!=''){$filepath=str_replace("index.php","application/third_party/access/".md5($userid.$idno).".php",$_SERVER['SCRIPT_FILENAME']); } 
  if($idtype!='' && !file_exists($filepath))
  {
   if($idtype=='-1')
    $filepath=str_replace("index.php","application/third_party/access/default_admin.php",$_SERVER['SCRIPT_FILENAME']);
   else if($idtype=='1')
    $filepath=str_replace("index.php","application/third_party/access/default_student.php",$_SERVER['SCRIPT_FILENAME']);
   else if($idtype=='2')
    $filepath=str_replace("index.php","application/third_party/access/default_parent.php",$_SERVER['SCRIPT_FILENAME']);
   else if($idtype=='3')
    $filepath=str_replace("index.php","application/third_party/access/default_faculty.php",$_SERVER['SCRIPT_FILENAME']);
  }
  else  	   
   $filepath = str_replace("index.php","application/third_party/access/default.php",$_SERVER['SCRIPT_FILENAME']);		 
  
  if(!file_exists($filepath)){$filepath = str_replace("index.php","application/third_party/access/default.php",$_SERVER['SCRIPT_FILENAME']); }
	  
  if(file_exists($filepath))
  {
   include_once($filepath); 
   return ((isset($privileges))?$privileges:'');
  }
  else
   return '';	 	 
 }
 
 function generate_privileges($id='')
 {
  $output='';
  if($id==''){return ''; }	 
  $privileges = $this->privileges;
  if($privileges==''){return '<tr class=""><td><i class="btn btn-danger btn-xs btndelete fa fa-trash-o" data-id="'.$id.'" data-action="access"></i> Access Page <input class="chkprivilege pull-right"  page-id="'.$id.'" name="access" desc="Access Page" type="checkbox"/></td></tr>'; }  
  $key = array_search($id,array_column($privileges,'p_id'));
  if($key===false){return '<tr class=""><td><i class="btn btn-danger btn-xs btndelete fa fa-trash-o" data-id="'.$id.'" data-action="access"></i> Access Page <input class="chkprivilege pull-right" page-id="'.$id.'" name="access" desc="Access Page" type="checkbox"/></td></tr>';}
  foreach($privileges[$key] as $k => $val)
  {
   if($k!='p_id'){$output.='<tr class=""><td><i class="btn btn-danger btn-xs btndelete fa fa-trash-o"  data-id="'.$id.'" data-action="'.$k.'"></i> '.$val['desc'].' <input class="chkprivilege pull-right" page-id="'.$id.'" name="'.$k.'" desc="'.$val['desc'].'" type="checkbox"/></td></tr>';	 }		
  }
  return $output;
 }
 //ABS-206 ENJOY HI-SCHOOL 07 Karen Aoki
 //JUC-694 Our Son's Balls Definitely Aren't Small - Hisae Yabe
 //WANZ-080 Female Teacher in Pantyhose's Unaware Panty Shot Temptation - Natsume Inagawa
 //GG-064 Surveillance Records Of The Widow Next Door With The Sexy Ass Filmed By The Building Manager.
 //IPTD-947 Sex Entertainment Rape Club – Kaho Kasumi
 //SPRD-360 At Home the Bath Was Always Mixed Jun Azabu
 
 function gen_display()
 {
  $this->pages = $this->gen_page_list();
  $this->privileges = $this->get_privileges_list(); 
  $output='';
  if($this->pages==false) return '';
  $menu = array();
  $sub  = array();
  $priv  = array();
  foreach($this->pages as $k=>$row)
  {
   if($row['menu']==1 && $row['sub']==0)
   {
    $output.='<tr class="table-parent"><td><i class="fa fa-plus"></i> '.$row['label'].'</td></tr>
              <tr class="table-child hidden"><td><table class="table table-condense xmargin-bottom-5"><xsub'.$row['id'].'></table></td></tr>';   
   }
   if(!array_key_exists($row['m_id'],$sub)){$sub[$row['m_id']] = array();}
   $sub[$row['m_id']][count($sub[$row['m_id']])]=$k;
   if($row['page']==1){$priv[$row['id']]=$this->generate_privileges($row['id']); }
  }
  if(count($sub)>0)
  {
   foreach($sub as $key => $xdata)
   {
	$sub_display='';
    foreach($xdata as $k=>$val)
	{
     $row = $this->pages[$val];
	 if($row['page']==1 && $row['inactive']==0)
	 {	 
      $child = ((array_key_exists($row['id'],$sub))?('<xsub'.$row['id'].'>'):('<xpriv'.$row['id'].'>'));	 
      $sub_display.='<tr class="table-parent"><td><i class="fa fa-plus"></i> '.$row['label'].'</td></tr>
                     <tr class="table-child hidden"><td><table class="table table-condense xmargin-bottom-5">'.$child.'</table></td></tr>';   
     } 
	}
	$output=str_replace('<xsub'.$key.'>',$sub_display,$output);
   }   
  }
  foreach($priv as $k => $p)
  {
	$output=str_replace('<xsub'.$k.'>',$p,$output);
	$output=str_replace('<xpriv'.$k.'>',$p,$output);
  }  
  return $output;
 }
 
 function save_privileges($userid='',$idno='',$idtype='',$data='')
 {
  $content='';
  if($data==''){return false;}
  $list = explode('|',$data);
  foreach($list as $k => $v)
  {
   $sub_content='';
   $list_data=explode(',',$v);
   foreach($list_data as $kd =>$vd)
   {
	$col=explode(':',$vd);
	if(strpos($col[1],'#')>0)
	{
	 $tmp_col='';
	 $sub=explode('#',$col[1]);
     foreach($sub as $ks=>$vs)
     {
	  $sdetail=explode('&',$vs);	 
	  $tmp_col.=(($tmp_col=='')?'':',')."'".$sdetail[0]."'=>'".$sdetail[1]."'"; 
	 }
     if($tmp_col!=''){$col[1]='array('.$tmp_col.')';}	 
	}
	else
	{
	 $col[1]="'".$col[1]."'";	
	}	
    $sub_content.=(($sub_content=='')?'':',')."'".$col[0]."'=>".$col[1]; 
   }
   $content.=(($content=='')?'':',')."array(".$sub_content.")";   
  }
  
  if($content==''){return false; }
  $content='$privileges=array('.$content.')';
  $filepath='';
  if($userid!='' && $idno!='')
   $filepath=str_replace("index.php","application/third_party/access/".md5($userid.$idno).".php",$_SERVER['SCRIPT_FILENAME']);
  elseif($idtype!='')
  {
   if($idtype=='-1')
    $filepath=str_replace("index.php","application/third_party/access/default_admin.php",$_SERVER['SCRIPT_FILENAME']);
   else if($idtype=='1')
    $filepath=str_replace("index.php","application/third_party/access/default_student.php",$_SERVER['SCRIPT_FILENAME']);
   else if($idtype=='2')
    $filepath=str_replace("index.php","application/third_party/access/default_parent.php",$_SERVER['SCRIPT_FILENAME']);
   else if($idtype=='3')
    $filepath=str_replace("index.php","application/third_party/access/default_faculty.php",$_SERVER['SCRIPT_FILENAME']);
  }
  else  	   
   $filepath=str_replace("index.php","application/third_party/access/default.php",$_SERVER['SCRIPT_FILENAME']);
  
  if($filepath==''){return false; }	  
  if(file_exists($filepath)){unlink($filepath); }
  if(!$fp = @fopen($filepath, FOPEN_WRITE_CREATE)){return FALSE; }
  fwrite($fp, '<?php'.PHP_EOL);
  fwrite($fp, $content.PHP_EOL);
  fwrite($fp, '?>');
  fclose($fp);
  return true;  
 }
 
 function get_custom_user()
 {
  $custom_list=array();
  $filepath=str_replace("index.php","application/third_party/access/users.php",$_SERVER['SCRIPT_FILENAME']);
  if(!file_exists($filepath)){return false; }	  
  $content = file_get_contents($filepath);
  //die($content);
  $list = explode('|',$content);  
  $i=0;
  foreach($list as $k => $v)
  {
   $details = explode(',',$v);
   if($details[0]!=''){$custom_list[$i]=$details; }
   $i++;   
  }
  return $custom_list;  
 }
 
 function save_custom_user($userid='',$idno='',$idtype='')
 {
  if($userid=='' || $idno=='' || $idtype==''){return false; }	 
  $filepath=str_replace("index.php","application/third_party/access/users.php",$_SERVER['SCRIPT_FILENAME']);
  if(!$fp = @fopen($filepath, FOPEN_WRITE_CREATE)){return FALSE; }
  $content = $userid.','.$idno.','.$idtype.','.md5($userid.$idno).'|';
  flock($fp, LOCK_EX);
  if(!fwrite($fp, $content.PHP_EOL)){log_message('error','Failed to Write User('.$userid.')!');}
  flock($fp, LOCK_EX);
  fclose($fp);
  return true;  
 }	 
}
?>