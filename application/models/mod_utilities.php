
<?php
Class mod_utilities extends CI_Model
{
  
  function listofmods(){
    $modlist = array( '1' => array('key' => 'department', 'txt' => 'Department'),
										  '2' => array('key' => 'position', 'txt' => 'Position Title'),
										  '3' => array('key' => 'civilstatus', 'txt' => 'Civil Status'),
										  '4' => array('key' => 'nationality', 'txt' => 'Nationality'),
										  '5' => array('key' => 'religion', 'txt' => 'Religion'),
										  '6' => array('key' => 'otherschool', 'txt' => 'Other School'),
										  '7' => array('key' => 'building', 'txt' => 'Building'),
										  '8' => array('key' => 'rooms', 'txt' => 'Rooms'));
    return $modlist;	
  }
  
  function cs_getfields($rec){
  	
	switch($rec){
		case 'department':
			$query = "select departmentid as 'ID', DepartmentCode as 'Code', DepartmentName as 'Name', '' as 'Campus' from es_departments";
			break;
		case 'position':
			$query = "select PosnTitleID as 'ID', PositionCode as 'Code', PositionDesc as 'Name' from es_positiontitles";
			break;
		case 'civilstatus':
			$query = "select statusid as ID, StatusDesc as 'Status', Inactive  from es_civilstatus";
			break;
		case 'nationality':
			$query = "SELECT NationalityID AS 'ID', Nationality, Shortname as 'Short Name', Inactive , IsDefault from es_nationalities";
			break;
		case 'otherschool':
			$query = "SELECT IndexID as 'ID', SchoolCode AS 'Code', SchoolName AS 'School'  from es_otherschools  Limit 1;";
			break;
		default:
			break;
	}
	
	
  	return $this->db->query($query);
  }
  
 function cs_getdata($rec, $limit = 10, $offset=0){
 	
	switch($rec){
		case 'department':
			$query = "departmentid as 'ID', DepartmentCode as 'Code', DepartmentName as 'Name', fn_GetCampusName(CampusID) as 'Campus'";
			$table='es_departments';
			break;
		case 'position':
			$query = "PosnTitleID as 'ID', PositionCode as 'Code', PositionDesc as 'Name'";
			$table='es_positiontitles';
			break;
		case 'civilstatus':
			$query = "StatusID as 'ID', StatusDesc as 'Status', Inactive";
			$table='es_civilstatus';
			break;
		case 'nationality':
			$query = "NationalityID AS 'ID', Nationality, Shortname as 'Short Name', Inactive , IsDefault";
			$table='es_nationalities';
			break;			
		case 'otherschool':
			$query = "IndexID as 'ID', SchoolCode AS 'Code', SchoolName AS 'School' ";
			$table='es_otherschools';
			break;
		default:
			break;
	}
	
	$this->db->select($query);
	
  	return $this->db->get($table,$limit,$offset)->result();
 }
  
  
}
?>