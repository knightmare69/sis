<?php
Class mod_vbscripts extends CI_Model
{

 function ExportImage($filepath='',$idno=0,$idtype=0)
 {
  $server = $this->db->hostname;
  $host = $this->getBetween($server,'Server=',';');
  $user = $this->db->username;
  $pass = $this->db->password;
  $dbname = $this->db->database;
  
  $vbsfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/vbs/exportimage.vbs';
  $imgfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']). $filepath;
  
  $cmdscript = $vbsfile.' /idno:"'.$idno.'" /idtype:"'.$idtype.'" /host:"'.$host.'" /db:"'.$dbname.'" /security:1  /userid:"'.$user.'"  /pass:"'.$pass.'" /path:"'.$imgfile.'"';
  log_message('error',$cmdscript);
  if(extension_loaded('com_dotnet'))
  {
   $WshShell = new COM("WScript.Shell"); 
   $result = $WshShell->Run($cmdscript, 0, true);
   $WshShell =null;
   log_message('error','Export Using WScript');
  }else{
   exec($cmdscript);
   log_message('error','Export Using CMD');
  }	 
  return file_exists($imgfile);
 }
 
 function ImportFile($filepath='',$filename='',$idno=0,$tbl='')
 {
  $server = $this->db->hostname;
  $host = $this->getBetween($server,'Server=',';');
  $user = $this->db->username;
  $pass = $this->db->password;
  $dbname = $this->db->database;
  
  $vbsfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/vbs/importfile.vbs';
  $file = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$filepath;
  $cmdscript = $vbsfile.' /idno:"'.$idno.'" /host:"'.$host.'" /db:"'.$dbname.'" /tbl:"'.$tbl.'" /security:1  /userid:"'.$user.'"  /pass:"'.$pass.'" /path:"'.$file.'" /name:"'.$filename.'"';
  
  if(extension_loaded('com_dotnet'))
  {
   $WshShell = new COM("WScript.Shell"); 
   $result = $WshShell->Run($cmdscript, 0, false);
   $WshShell =null;
   log_message('error','Import Using WScript');
  }
  else
  {
   exec($cmdscript);
   log_message('error','Import Using CMD');
  }	 
  return true;
 }
 
 function ExportFile($filepath='',$idno=0,$tbl='',$col='',$con='')
 {
  $server = $this->db->hostname;
  $host   = $this->getBetween($server,'Server=',';');
  $user   = $this->db->username;
  $pass   = $this->db->password;
  $dbname = $this->db->database;
  
  $vbsfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/vbs/exportfile.vbs';
  $file = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$filepath;
  $cmdscript = $vbsfile.' /idno:"'.$idno.'" /host:"'.$host.'" /db:"'.$dbname.'" /tbl:"'.$tbl.'" /col:"'.$col.'" /con:"'.$con.'" /security:1  /userid:"'.$user.'"  /pass:"'.$pass.'" /path:"'.$file.'"';
  
  if(extension_loaded('com_dotnet'))
  {
   $WshShell = new COM("WScript.Shell"); 
   $result = $WshShell->Run($cmdscript, 0, true);
   $WshShell =null;
   log_message('error','Import Using WScript');
  }
  else
  {
   exec($cmdscript);
   log_message('error','Import Using CMD');
  }	 
  return true;
 }
 
 
 function ExportImageBySched($filepath='',$idno=0)
 {
  if($idno!='' && $idno!==undefined)
  {
   $server = $this->db->hostname;
   $host = $this->getBetween($server,'Server=',';');
   $user = $this->db->username;
   $pass = $this->db->password;
    $dbname = $this->db->database;
  
   $vbsfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).'assets/vbs/exportimagebysched.vbs';
   $imgfile = str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']).$filepath;
  
   $cmdscript = $vbsfile.' /idno:"'.$idno.'" /host:"'.$host.'" /db:"'.$dbname.'" /security:1  /userid:"'.$user.'"  /pass:"'.$pass.'" /path:"'.$imgfile.'"';
  
   if(extension_loaded('com_dotnet'))
   {
    $WshShell = new COM("WScript.Shell"); 
    $result = $WshShell->Run($cmdscript, 0, true);
    $WshShell =null;
    log_message('error','Import Using WScript');
   }
   else
   {
    exec($cmdscript);
    log_message('error','Import Using CMD');
   }	 
  }
  return true;
 }

  function getBetween($content,$start,$end)
  {
    $r = explode($start, $content);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return $content;
  }
  
}
?>


