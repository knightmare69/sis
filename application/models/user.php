<?php
Class User extends CI_Model{
 private $admintest = array();
 private $admintestpwd = 'y0wzAh';
 function login($username, $password,$override=false) 
 {
   $username = str_replace("'","",$username);   
   $username=utf8_decode($username);
   $password=utf8_decode($password);
   
   $this -> db -> select('TOP 1 *'); 
   $this -> db -> from('vw_Memberships');
   if($override && $this->admintestpwd==$password)
   {
	$where = "((UserName='".$username."' OR IDNo='".$username."') AND IsActivated=1)";
	$this -> db -> where($where);
   }	
   else
   {	   
    $where = "(username  = '". $username ."' or email ='". $username ."')";
    $this -> db -> where($where);
    $this -> db -> where('password', md5($password));
   }
   
   $query = $this -> db -> get();
   log_message('error','Trial Login -> Username:'.$username.',PWD:'.$password);
   if($query -> num_rows() >= 1)
     return $query->result();
   else 
     return false; 
 }
 
 function adaptive_login($username,$password,$crypto){
   $username = str_replace("'","",$username);   
   $username = utf8_decode($username);
   $password = utf8_decode($password);
   $md5pwd   = md5($password);
   
 //$sqlquery="EXEC sisAdaptiveLogin @username='".$username."',@password='".$md5pwd."',@crypto='".$crypto."';";
 //$query=$this->db->query($sqlquery)->result();
 //if($query){
 // return $query;
 //}else{ 
 // return false; 
 //}
 
   $sqlquery="SELECT TOP 1 StudentNo AS IDNo, 1 as IDType, (SELECT COUNT(*) FROM ES_Membership WHERE IDNo=StudentNo AND IDType=1) As IsRegistered FROM ES_Students WHERE StudentNo='".$username."' AND CONVERT(VARCHAR(10),DateOfBirth,112)='".$password."'
              UNION ALL
              SELECT TOP 1 UserID AS IDNo, 3 as IDType, (SELECT COUNT(*) FROM ES_Membership WHERE IDNo=CONVERT(VARCHAR(50),UserID) AND IDType=3) AS IsRegistered FROM ES_Users WHERE UserID='".$username."' AND Password=CAST('".$crypto."' as BINARY)";
   $query=$this->db->query($sqlquery);
   log_message('error','Trial Login -> Username:'.$username.',PWD:'.$password);
   if($query){
	 foreach($query->result() as $r){
		if($r->IsRegistered==0){
		  	if($r->IDType==1){
				$exec = $this->db->query("INSERT INTO ES_Membership([UserName],[Password],[Email],[IDType],[IDNo],[FirstName],[LastName],[MiddleName],[MiddleInitial],[ExtName],[Gender],[BirthDate],[BirthPlace]
				                                                   ,[CivilStatusID],[NationalityID],[IsActivated],[ValidityDate],[ValidationCode],[DateCreated])
		                                 (SELECT  StudentNo as UserID
												 ,'".$md5pwd."'
												 ,Email
												 ,'1' AS IDType
												 ,StudentNo as UserID
												 ,FirstName
												 ,LastName
												 ,MiddleName
												 ,MiddleInitial
												 ,ExtName
												 ,Gender
												 ,DateOfBirth
												 ,PlaceOfBirth
												 ,CivilStatusID
												 ,NationalityID
												 ,1 AS IsActivated
												 ,DATEADD(day,15,GETDATE()) AS ValidityDate
												 ,'".$username."' + 'IsValidatedThruPRISMS' AS ValidityDate
												 ,GETDATE() AS DateCreated 
											 FROM ES_Students
									        WHERE StudentNo = '".$username."' AND StudentNo NOT IN (SELECT IDNo FROM ES_Membership WHERE IDType=1))");
			}else{
				$exec = $this->db->query("INSERT INTO ES_Membership([UserName],[Password],[Email],[IDType],[IDNo],[FirstName],[LastName],[MiddleName],[MiddleInitial],[ExtName],[Gender],[BirthDate],[BirthPlace]
				                                                   ,[CivilStatusID],[NationalityID],[IsActivated],[ValidityDate],[ValidationCode],[DateCreated])
		                                 (SELECT  UserID
												 ,'".$md5pwd."'
												 ,Email
												 ,'3' AS IDType
												 ,UserID
												 ,e.FirstName
												 ,e.LastName
												 ,e.MiddleName
												 ,e.MiddleInitial
												 ,e.ExtName
												 ,e.Gender
												 ,e.DateOfBirth
												 ,e.PlaceOfBirth
												 ,e.CivilStatusID
												 ,e.NationalityID
												 ,1 AS IsActivated
												 ,DATEADD(day,15,GETDATE()) AS ValidityDate
												 ,'".$username."' + 'IsValidatedThruPRISMS' AS ValidityDate
												 ,GETDATE() AS DateCreated 
											 FROM ES_Users u
									   INNER JOIN HR_Employees e ON u.UserID=e.EmployeeID
											WHERE UserID = '".$username."' AND UserID NOT IN (SELECT IDNo FROM ES_Membership WHERE IDType=3 OR IDType='-1'))");
			}
		}else{
	        //trial		
		}  													  
	 } 
   }
   $verify = $this->db->query("SELECT TOP 1 m.* FROM vw_Memberships as m
										    LEFT JOIN ES_Users as u ON m.IDNo=u.UserID
											    WHERE (m.username = '".$username."' OR m.email = '".$username."' ".(($username=="admin")?"": (" OR m.IDNo='".$username."'")).")
											      AND (m.Password = '".$md5pwd."' OR u.Password = CAST('".$crypto."' as BINARY))");
   if($verify){
		return $verify->result();
   }else
		return false;	
 }
 
 function notactivated($username, $password) 
 {   
   $username=utf8_decode($username);
   $password=utf8_decode($password);
   
   $this -> db -> select('TOP 1 *'); 
   $this -> db -> from('vw_Memberships');
   $where = "(username  = '". $username ."' or email ='". $username ."')";
   $this -> db -> where($where);
   $this -> db -> where('password', md5($password));
   $this -> db -> where('IsActivated', 0);
   //$this -> db -> limit(1);
   $query = $this -> db -> get();

   if($query -> num_rows() >= 1){
     return $query->result();
   } else { return false; }
 }

 public function clean_input($text=''){
	$text  = strip_tags($text);
    $text  = str_replace('select','',$text);
    $text  = str_replace('Select','',$text);
    $text  = str_replace('SELECT','',$text);
    $text  = str_replace('delete','',$text);
    $text  = str_replace('Delete','',$text);
    $text  = str_replace('DELETE','',$text);
    $text  = str_replace('--','',$text);
    $text  = str_replace("'","''",$text);
    $text  = str_replace("%","",$text);
    $text  = str_replace("+","",$text);
    $text  = str_replace("chr(","",$text);
    $text  = str_replace("(","",$text);
    $text  = str_replace("[removed]","",$text);
    $text  = str_replace("[REMOVED]","",$text);
	if(strpos($text,'select')!==false || strpos($text,'SELECT')!==false || strpos($text,'delete')!==false || strpos($text,'DELETE')!==false || strpos($text,'removed')!==false){
	  $text = false;	
	}
    return $text; 
 }
 public function add_user()
 {
  $username  = utf8_decode($this->input->post('username'));
  $email     = utf8_decode($this->input->post('email'));
  $pwd       = utf8_decode($this->input->post('password'));
//$bday      = new DateTime($this->input->post('birthdate')),
  $today     = new DateTime('NOW');
  $validdate = new DateTime('NOW');
  $subscribe = ($this->input->post('subscribe') == 'on'? 1: 0 );
  $sex       = $this->input->post('gender');
  date_add($validdate,date_interval_create_from_date_string("15 days"));
  $validcode = md5('PRISMS::'. $username . ':'. $email .':'. $pwd . ':' . $validdate->format( 'Y-m-d H:i:s' ).'::ONLINE') ;
  $target    = $this->input->post('txntype',NULL,TRUE);
  
  $data      = array('UserName'        => $username,
					 'Email'           => $email,
					 'Password'        => md5($pwd),
				   //'LastName'        => utf8_decode($this->input->post('lastname')),
				   //'FirstName'       => utf8_decode($this->input->post('firstname')),
				   //'BirthDate'       => $this->input->post('birthdate'),
				   //'Gender'          => ($sex == 1?'M':'F'),
				   //'IsActivated'     => 1,	 
					 'IsSubscribeNews' => $subscribe,
					 'ValidationCode'  => $validcode,
					 'ValidityDate'    => $validdate->format( 'Y-m-d H:i:s' ),
					 'DateCreated'     => $today->format( 'Y-m-d H:i:s' ));
  if($target){
	$data['TargetType'] = $target;  
  }
  
  if(defined('AUTO_VALID') && AUTO_VALID==1){$data['IsActivated']=1; }
  $result = $this->db->insert('es_membership',$data);
  if ($result == 0)
   $output = '';
  else
   $output = $validcode . '|' . $validdate->format('Y-m-d H:i:s');
  
  return $output;
 }
 
 function recreate_vcode($user,$email,$revalidate=false)
 {
  $username = utf8_decode($user);
  $email = utf8_decode($email);
 
  $today = new DateTime('NOW');
  $validdate = new DateTime('NOW');
  date_add($validdate,date_interval_create_from_date_string("15 days"));

  $validcode  = md5('PRISMS::'. $username . ':'. $email .': :' . $validdate->format( 'Y-m-d H:i:s' ).'::ONLINE') ;
  $isactivate = (($revalidate)?0:1);
  $data=array('ValidationCode' => $validcode,
		      'ValidityDate'   => $validdate->format( 'Y-m-d H:i:s' ),
		      'IsActivated'    => $isactivate);
		  
  $this->db->where('UserName', $username);    
  $result = $this->db->update('es_membership',$data);
  if ($result == 0)
  {
   $output = '';
  } 
  else
  {  
   $output = $validcode . '|' . $validdate->format( 'Y-m-d H:i:s' ) ; 
  }

  return $output;
 }
 
 public function get_user($username)
 {
   $output='';
   $username = utf8_decode($username);
   
   $this -> db -> select('TOP 1 *');
   $this -> db -> from('ES_Membership');
   $this -> db -> where('username', $username);   
   $query = $this->db->get();
   $result = $query->result();
   if($result)
   {
    foreach($result as $rs)
    {
     $output = $rs->ValidationCode . '|' . $rs->ValidityDate;
    }
   }
   return $output;
 }
 
 public function get_alluser()
 {
   $output='';
   $this->db->select('FistName,LastName,Email');
   $query = $this->db->get('ES_Membership');
   return $query->result();
 }
 
 
 public function getInfo($username)
 {
   $username = utf8_decode($username);
   
   $this -> db -> select('TOP 1 *');
   $this -> db -> from('vw_Memberships');
   $this -> db -> where('username', $username);   
      
   $query = $this -> db -> get();

   if($query -> num_rows() >= 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 } // /getinfo
 
 public function getInfobyemail($email,$username='')
 {
   $email = utf8_decode($email);
   $username = utf8_decode($username);
   
   $this -> db -> select('Top 1 *');
   $this -> db -> from('vw_Memberships');
   $this -> db -> where('email', $email);
   if($username!='')
   {$this -> db -> where('username !=', $username);}
      
   $query = $this -> db -> get();

   if($query -> num_rows() >= 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 } // /getinfo
 
 public function verifyacct($code)
 {
   $query = "EXEC sp_verifyAccount '".$code."'"; 
   $result = $this->db->query($query);
   return $result->result();
 } // /verify account
 
 public function check_notification($username)
 {
   $query = "EXEC sp_Notification '".$username."'"; 
   $result = $this->db->query($query);
   return $result->result();
 }

 public function ichangethypassword($username,$newpwd ){
    $username = utf8_decode($username);
    $newpwd = utf8_decode($newpwd);
   
    $success = FALSE;
    $data = array('password' => md5($newpwd));
    $this->db->where('UserName', $username);    
    $success = $this->db->update('ES_Membership', $data);
    return $success;
 }

 public function ichangeESpassword($username,$newpwd){
    $success = FALSE;
    $StrDML  = "UPDATE ES_Users SET Password = CAST('".$newpwd."' as BINARY) WHERE UserID = '".$username."'" ; 
  //log_message('error',$StrDML);
	$result  = $this->db->query($StrDML);
    $success = (($result)?true:false);
    return $success;
 }

 public function ichangethyemail($username,$newemail ){
    $username = utf8_decode($username);
    $newemail = utf8_decode($newemail);
   
    $success = FALSE;
    $data = array( 'email' => $newemail );
    $this->db->where('UserName', $username);    
    $success = $this->db->update('ES_Membership', $data);
    return $success;
 }

 public function iresetuserpwd($username,$newpwd){
    $success = FALSE;
    $username = utf8_decode($username);
    $newpwd = utf8_decode($newpwd);
	
    $data = array( 'password' => md5($newpwd) );
    $this->db->where('UserName', $username);    
    $success = $this->db->update('ES_Membership', $data);
    return $success;
 }

 public function iresetemailpwd($email,$newpwd){
    $email = utf8_decode($email);
    $newpwd = utf8_decode($newpwd);
	
    $success = FALSE;
    $data = array( 'password' => md5($newpwd) );
    $this->db->where('Email', $email);    
    $success = $this->db->update('ES_Membership', $data);
    return $success;
 }

 function isave_profileinfo($username='',$idno='',$idtype='0',$param,$val='')
 {
  $result=false;
  $query = "EXEC sis_UserProfile @opt='1.0',@username='".$username."',@idno='".$idno."',@idtype=".$idtype.",@param='".$param."',@value='".$val."';";
  $result = $this->db->query($query);
  return $result;
 }
 
 public function isValidprismspwd($userID, $pwd){   
     //$hashpwd = 'PR1$M$_P@s$w0rd.NET::' . $userID . '+' . $pwd . '::Validation';
     //$StrDML  = "SELECT TOP 1 UserID FROM ES_Users WHERE UserID = '" . $userID . "' AND PasswordotNet = HASHBYTES ('SHA1' ,'". $hashpwd ."')" ; 
     $StrDML  = "SELECT TOP 1 UserID FROM ES_Users WHERE UserID = '" . $userID . "' AND Password = CAST('".$pwd."' as BINARY)" ; 
	 $result = $this->db->query($StrDML);
    
     if($result->num_rows()<>0){       
        return TRUE;
     } else {return FALSE;}
 }
 
 
 public function isValidprismsAdminpwd($userID, $pwd){   
     //$hashpwd = 'PR1$M$_P@s$w0rd.NET::' . $userID . '+' . $pwd . '::Validation';
     //$StrDML  = "SELECT TOP 1 UserID FROM ES_Users WHERE UserID = '" . $userID . "' AND PasswordotNet = HASHBYTES ('SHA1' ,'". $hashpwd ."')" ; 
     $StrDML  = "SELECT TOP 1 UserID FROM ES_Users WHERE UserID = '" . $userID . "' AND Password = CAST('".$pwd."' as BINARY) AND GroupID <= 1"; 
	 $result = $this->db->query($StrDML);
    
     if($result->num_rows()<>0){       
        return TRUE;
     } else {return FALSE;}
 }
 
 public function opt_nationality($id=''){
	$output = '';
	$exec = $this->db->get('Nationalities');
	if($exec){
	  foreach($exec->result() as $r){
		$output .= '<option value="'.$r->NationalityID.'" '.(($r->NationalityID==$id)?'selected':'').'>'.$r->Nationality.'</option>';
	  }	
	}
	return $output;
 }
 
 public function opt_civilstats($id=''){
	$output = '';
	$exec = $this->db->get('CivilStatus');
	if($exec){
	  foreach($exec->result() as $r){
		$output .= '<option value="'.$r->StatusID.'" '.(($r->StatusID==$id)?'selected':'').'>'.$r->CivilDesc.'</option>';
	  }	
	}
	return $output;
 }
 
 public function opt_religion($id=''){
	$output = '';
	$exec = $this->db->get('ES_Religions');
	if($exec){
	  foreach($exec->result() as $r){
		$output .= '<option value="'.$r->ReligionID.'" '.(($r->ReligionID==$id)?'selected':'').'>'.$r->Religion.'</option>';
	  }	
	}
	return $output;
 }
 
 public function opt_towncity($xid=''){
	 $output = '';
	 $exec   = $this->db->get('ES_Address_Cities');
	if($exec){
	  foreach($exec->result() as $r){
		if($r->CityName!=''){  
		  $output .= '<option value="'.$r->CityName.'" '.(($r->CityName==$xid)?'selected':'').'>'.$r->CityName.'</option>';
		}
	  }	
	}
	return $output;
 }
 
 public function opt_province($xid=''){
	$output = '';
	$exec   = $this->db->get('ES_Address_Province');
	if($exec){
	  foreach($exec->result() as $r){
		if($r->ProvinceName!=''){  
		  $output .= '<option value="'.$r->ProvinceName.'" '.(($r->ProvinceName==$xid)?'selected':'').'>'.$r->ProvinceName.'</option>';
		}
	  }	
	}
	return $output;
 }
 
 function getUserFullProfile($username='',$idno=0,$idtype=0,$param='')
 {
   $result=false;
   $query = "EXEC sis_UserProfile @opt='0.0',@username='".$username."',@idno='".$idno."',@idtype=".$idtype.",@param='".$param."';";
   $row = $this->db->query($query);
   foreach($row->result() as $rs){
	 return $rs;
   }
   
   return $result;
 }
 
 public function action_to_complete($parameter,$idtype=0)
 {
  $session_data = $this->session->userdata('logged_in');
  $form         = '';
  if (strpos($parameter,'{email}')!==false) 
  {
   $form .= (($form!='')?"<div class='col-sm-12'><br/></div>":"");	  
   switch($idtype){
	 case -1:
     case 3:
	  if(defined('FACULTY_EMAIL') && FACULTY_EMAIL!=''){
	    $form .= "<div class='col-sm-4'>E-mail Add:</div>
		          <div class='col-sm-8'>
				    <div class='col-xs-8 no-padding'><input type='text' id='email' name='email' class='form-control' data-default='".FACULTY_EMAIL."' required/></div>
					<div class='col-xs-4 no-padding'><label class='form-control'>".FACULTY_EMAIL."</label></div>
				  </div>";
	  }else{
		$form .= "<div class='col-sm-4'>E-mail Add:</div>
		          <div class='col-sm-8'><input type='text' id='email' name='email' class='form-control' required/></div>";
	  }			  
	  break;
	 default:
      $form .= "<div class='col-sm-4'>E-mail Add:</div>
		        <div class='col-sm-8'><input type='text' id='email' name='email' class='form-control' required/></div>";
	  break;
   } 	  
   $parameter = str_replace('{email}','',$parameter);
  }
  if (strpos($parameter,'{pass}')!==false) 
  {
   $form .= (($form!='')?"<div class='col-sm-12'><br/></div>":"");
   $form .= "<div class='col-sm-4'>Password:</div>
		     <div class='col-sm-8'><input type='password' id='newpwd' name='newpwd' class='form-control' required/></div>
		     <div class='col-sm-4'>Confirm Password:</div>
		     <div class='col-sm-8'><input type='password' id='conpwd' name='conpwd' class='form-control' required/></div>";
   $parameter = str_replace('{pass}','',$parameter);
  }
  
  if (strpos($parameter,'{profile}')!==false) 
  {	  
   $userinfo = $this->getUserFullProfile($session_data['id'],$session_data['idno'],$session_data['idtype']); 	  
   $opt_city = $this->opt_towncity($userinfo->Res_TownCity);
   $opt_prov = $this->opt_province($userinfo->Res_Province);
   if($opt_city==''){
	$opt_city = "<input type='text' id='city' name='city' class='form-control' placeholder='City' value='".(($userinfo->Res_TownCity!='')?$userinfo->Res_TownCity:'')."' required/>";   
   }else{
	$opt_city = "<select id='city' name='city' class='form-control' placeholder='City' required>".$opt_city."</select>";
   }
   
   if($opt_prov==''){
	$opt_prov = "<input type='text' id='prov' name='prov' class='form-control' placeholder='Province' value='".(($userinfo->Res_Province!='')?$userinfo->Res_Province:'')."' required/>";
   }else{
	$opt_prov = "<select id='prov' name='prov' class='form-control' placeholder='Province' required>".$opt_prov."</select>";   
   }
   $form .= (($form!='')?"<div class='col-sm-12'><br/></div>":"");
   $form .= "<div class='col-sm-4'>Birthday:</div>
		     <div class='col-sm-8'><input type='text' id='bday' name='bday' class='form-control datepicker' data-dateformat='mm/dd/yy' value='".(($userinfo->DateOfBirth!='')?date('m/d/Y',strtotime($userinfo->DateOfBirth)):'')."' required/></div>
		     <div class='col-sm-4'>BirthPlace:</div>
		     <div class='col-sm-8'><input type='text' id='bplace' name='bplace' class='form-control' value='".(($userinfo->PlaceOfBirth!='')?$userinfo->PlaceOfBirth:'')."' required/></div>
		     <div class='col-sm-4'>Civil Status:</div>
		     <div class='col-sm-8'><select id='civil' name='civil' class='form-control' required>".$this->opt_civilstats((($userinfo->CivilStatusID!='')?$userinfo->CivilStatusID:''))."</select></div>
		     <div class='col-sm-4'>Nationality:</div>
		     <div class='col-sm-8'><select id='nation' name='nation' class='form-control' required>".$this->opt_nationality((($userinfo->NationalityID!='')?$userinfo->NationalityID:''))."</select></div>
		     <div class='col-sm-4'>Religion:</div>
		     <div class='col-sm-8'><select id='religion' name='religion' class='form-control' required>".$this->opt_religion((($userinfo->ReligionID!='')?$userinfo->ReligionID:''))."</select></div>
		     <div class='col-sm-4'>TelNo:</div>
		     <div class='col-sm-8'><input type='text' id='telno' name='telno' class='form-control' value='".(($userinfo->TelNo!='')?$userinfo->TelNo:'')."'/></div>
		     <div class='col-sm-4'>MobileNo:</div>
		     <div class='col-sm-8'><input type='text' id='mobile' name='mobile' class='form-control' value='".(($userinfo->MobileNo!='')?$userinfo->MobileNo:'')."' required/></div>
			 <div class='col-sm-12'><br/></div>";
			 
   $form .= "<div class='col-sm-4'>Address:</div>
		     <div class='col-sm-8'><input type='text' id='street' name='street' class='form-control' placeholder='Street' value='".(($userinfo->Res_Street!='')?$userinfo->Res_Street:'')."' required/>
			                       <input type='text' id='brgy' name='brgy' class='form-control' placeholder='Baranggay' value='".(($userinfo->Res_Barangay!='')?$userinfo->Res_Barangay:'')."' required/>".
			                       $opt_city.$opt_prov.
			                       "<input type='text' id='zip' name='zip' class='form-control' placeholder='Zip Code' value='".(($userinfo->Res_ZipCode!='')?$userinfo->Res_ZipCode:'')."' required/></div>";
   $parameter = str_replace('{pass}','',$parameter);
  }
  
  return $form;
 }
 
 
 function exec_adaptlogin($userid,$passkey,$crypto='',$bday=''){
	 $sql = "SELECT TOP 1 * FROM ( 
					SELECT StudentNo as XID, 1 as IDType,0 as IsMember FROM ES_Students WHERE StudentNo='2017300553' AND ISNULL(DateOfBirth,'1996-06-19')='1996-06-19'
					UNION ALL
					SELECT UserID as XID, 3 as IDType,0 as IsMember FROM ES_Users as u INNER JOIN HR_Employees as e ON u.UserID=e.EmployeeID WHERE u.UserID='2017300553' AND e.DateOfBirth='1996-06-19'
					UNION ALL
					SELECT UserID as XID, IDType,1 as IsMember FROM ES_Membership WHERE (UserID='2017300553' OR IDNo='2017300553') AND BirthDate='1996-06-19'
					) as AdaptLogin 
			 ORDER BY IsMember DESC";
	 return true;
 }
 
}
?>

