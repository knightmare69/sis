<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Admission</li>
		<li>Application</li>
	</ol>
</div>   
<div id="content">	 
	<div class="row">
	  <div class="col-xs-12 col-sm-3">
	   Campus:
	   <select class="form-control" id="campus" name="campus">
		   <option value="-1" selected disabled> - Select One - </option>
		   <?php if(isset($rscampus)){
			   foreach($rscampus as $c){
				   echo '<option value="'.$c->CampusID.'">'.$c->ShortName.'</option>';
			   }
		   }?>
	   </select>
	  </div>
	  <div class="col-xs-12 col-sm-3">
	   Acad. Year And Term:
	   <select class="form-control" id="ayterm" name="ayterm">
		   <option value="-1" selected disabled> - Select One - </option>
		   <?php if(isset($rsayterm)){
			   foreach($rsayterm as $a){
				   echo '<option value="'.$a->TermID.'">'.$a->AcademicYearTerm.'</option>';
			   }
		   }?>
	   </select>
	  </div>
	  <div class="col-xs-12 col-sm-3">
	   Application Type:
	   <select class="form-control" id="apptype" name="apptype">
		   <option value="-1" selected disabled> - Select One - </option>
		   <?php if(isset($rsapptypes)){
			   foreach($rsapptypes as $at){
				   echo '<option value="'.$at->TypeID.'">'.$at->ApplicationType.'</option>';
			   }
		   }?>
	   </select>
	  </div>
	  <div class="col-xs-12 col-sm-3">
	   <br/>
	   <div class="col-sm-8">
	   <div class="input-group">
	      <input class="form-control txtparam" type="text"/>
		  <span class="input-group-addon btnsearch">
		      <i class="fa fa-search"></i>
		  </span>
	   </div>
	   </div>
	   <div class="col-sm-4">
	     <div class="btn-group">
		    <button class="btn btn-default" dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i> Option</button>
			<ul class="dropdown-menu">
			   <li class="btnnew"><a href="javascript:void(0);"><i class="fa fa-file"></i> New</a></li>
			   <li class="btnsave"><a href="javascript:void(0);"><i class="fa fa-save"></i> Save</a></li>
			   <li class="btnadmit"><a href="javascript:void(0);"><i class="fa fa-plus"></i> Admit</a></li>
			   <li class="btndeny"><a href="javascript:void(0);"><i class="fa fa-minus"></i> Deny</a></li>
			   <li class="btnremove"><a href="javascript:void(0);"><i class="fa fa-times"></i> Remove</a></li>
			</ul>
		 </div>
	   </div>
	  </div>
	  <div class="col-sm-12" style="margin-top:10px !important;">
	     <ul id="adm_tab" class="nav nav-tabs bordered">
			<li class="active"><a href="#s1" data-toggle="tab">Personal Info.</a></li>
			<li class=""><a href="#s2" data-toggle="tab">Family Background</a></li>
			<li class=""><a href="#s3" data-toggle="tab">Educ. Background</a></li>
			<li class=""><a href="#s5" data-toggle="tab">Exam/Medical Schedule</a></li>
			<li class=""><a href="#s4" data-toggle="tab">Documents</a></li>
			<li class="dropdown hidden">
				<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Exam/Medical/Interview <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#s5" data-toggle="tab">Examination</a></li>
					<li><a href="#s6" data-toggle="tab">Medical</a></li>
					<li><a href="#s6" data-toggle="tab">Interview</a></li>
				</ul>
			</li>
			<li class="pull-right hidden">TBA</li>
		 </ul>
		 <div id="adm_tabcontent" class="tab-content padding-10">
			<div class="tab-pane fade in active" id="s1">
			<form class="form-horizontal">
			   <div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4">
					   <div class="col-sm-6 col-sm-offset-3">
				         <a href="javascript:void(0);" data-toggle="tab"><span class="profile-edit">Edit</span></a>				
						 <img class="img-responsive img-profile xcenter-element" alt="" src="http://localhost:69/sis/assets/img/profile/2d99e9fc422c212d82c143b843ff3c8a.png?9129" height="80px">
					   </div>	
				    </div>
					<div class="col-sm-8 col-md-8">
					   <div class="form-group">
					        <br/>
							<label class="col-md-2 control-label">LastName:</label>
							<div class="col-md-10">
								<input id="lname" name="lname" class="form-control" placeholder="LastName" type="text">
							</div>
					   </div>
					   <div class="form-group">
							<label class="col-md-2 control-label">FirstName:</label>
							<div class="col-md-10">
								<input id="fname" name="fname" class="form-control" placeholder="FirstName" type="text">
							</div>
					   </div>
					   <div class="form-group">
							<label class="col-md-2 control-label">MiddleName:</label>
							<div class="col-md-7">
								<input id="mname" name="mname" class="form-control" placeholder="MiddleName" type="text">
							</div>
							<div class="col-md-3">
								<input id="miname" name="miname" class="form-control" placeholder="Middle Initial" type="text">
							</div>
					   </div>
					   <div class="form-group">
							<label class="col-md-2 control-label">ExtName:</label>
							<div class="col-md-4">
								<input id="ename" name="ename" class="form-control" placeholder="ExtName" type="text">
							</div>
					   </div>
					</div>
					<div class="col-sm-12"><br/></div>
					<div class="col-sm-12">
					   <div class="form-group">
					        <div class="col-sm-4">
								<label class="col-md-4 control-label">Gender:</label>
								<div class="col-md-8">
									<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					        <div class="col-sm-4">
								<label class="col-md-2 control-label">Birthday:</label>
								<div class="col-md-10">
									<input id="bdate" name="bdate" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					        <div class="col-sm-4">
								<label class="col-md-4 control-label">Place Of Birth:</label>
								<div class="col-md-8">
									<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					   </div>
					</div>
					<div class="col-sm-12">
					   <div class="form-group">
					        <div class="col-sm-4">
								<label class="col-md-4 control-label">Civil Status:</label>
								<div class="col-md-8">
									<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					        <div class="col-sm-4">
								<label class="col-md-2 control-label">Nation:</label>
								<div class="col-md-10">
									<input id="bdate" name="bdate" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					        <div class="col-sm-4">
								<label class="col-md-4 control-label">Religion:</label>
								<div class="col-md-8">
									<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					   </div>
					</div>
					<div class="col-sm-12">
					   <div class="form-group">
					        <div class="col-sm-4">
								<label class="col-md-4 control-label">TelNo:</label>
								<div class="col-md-8">
									<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					        <div class="col-sm-4">
								<label class="col-md-2 control-label">Mobile:</label>
								<div class="col-md-10">
									<input id="bdate" name="bdate" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					        <div class="col-sm-4">
								<label class="col-md-4 control-label">Email:</label>
								<div class="col-md-8">
									<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
								</div>
							</div>
					   </div>
					</div>
					<div class="col-sm-12" style="margin-top:10px;">
					   <legend>Present Address</legend>
					   <div class="form-group">
					        <div class="col-sm-4">
								<label class="control-label">Residence:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-4">
								<label class="control-label">Street:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-4">
								<label class="control-label">Baranggay:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-4">
								<label class="control-label">Town/City:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-4">
								<label class="control-label">Province:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-2">
								<label class="control-label">Zip Code:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					   </div>
					</div>
					<div class="col-sm-12" style="margin-top:10px;">
					   <legend>Permanent Address</legend>
					   <div class="form-group">
					        <div class="col-sm-4">
								<label class="control-label">Residence:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-4">
								<label class="control-label">Street:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-4">
								<label class="control-label">Baranggay:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-4">
								<label class="control-label">Town/City:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-4">
								<label class="control-label">Province:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					        <div class="col-sm-2">
								<label class="control-label">Zip Code:</label>
								<input id="civil" name="civil" class="form-control" placeholder="Default Text Field" type="text">
							</div>
					   </div>
					</div>
			   </form>		
			   </div>
			</div>
			<div class="tab-pane fade" id="s2">
				<form class="form-horizontal">
				   <div class="row">
					<div class="col-sm-6">
					    <legend>Father:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Father:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Occupation:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Company:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Comp. Address:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Email:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">TelNo:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						
						<legend>Mother:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Mother:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Occupation:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Company:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Comp. Address:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Email:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">TelNo:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
					    <legend>Guardian:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Guardian:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Relation:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Address:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Email:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">TelNo:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Occupation:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Company:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						
						<legend>Emergency Contact Person:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Contact Person:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Address:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Email:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">TelNo:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
					</div>
					
				   </div>
				</form>
			</div>
			<div class="tab-pane fade" id="s3">
				<form class="form-horizontal">
				   <div class="row">
					<div class="col-sm-6">
					    <legend>Elementary:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Name of School:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Address:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Inclusive Dates:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Awards/Honors:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						
					    <legend>Secondary:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Name of School:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Address:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Inclusive Dates:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Awards/Honors:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						
					    <legend>Vocational/Trade Course:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Name of School:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Address:</label>
							<div class="col-md-9">
								<input id="vocaddr" name="vocaddr" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Inclusive Dates:</label>
							<div class="col-md-9">
								<input id="vocdate" name="vocdate" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Awards/Honors:</label>
							<div class="col-md-9">
								<input id="vocaward" name="vocaward" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
					</div>	
					
					<div class="col-sm-6">
					    <legend>College:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Name of School:</label>
							<div class="col-md-9">
								<input id="colname" name="colname" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Address:</label>
							<div class="col-md-9">
								<input id="coladdr" name="coladdr" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Degree/Course:</label>
							<div class="col-md-9">
								<input id="coldegree" name="coldegree" class="form-control" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Inclusive Dates:</label>
							<div class="col-md-9">
								<input id="coldate" name="coldate" class="form-control" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Awards/Honors:</label>
							<div class="col-md-9">
								<input id="colaward" name="colaward" class="form-control" type="text">
							</div>
						</div>
						
					    <legend>Graduate Studies:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Name of School:</label>
							<div class="col-md-9">
								<input id="gsname" name="gsname" class="form-control" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Address:</label>
							<div class="col-md-9">
								<input id="gsaddr" name="gsaddr" class="form-control" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Degree/Course:</label>
							<div class="col-md-9">
								<input id="gsdegree" name="gsdegree" class="form-control" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Inclusive Dates:</label>
							<div class="col-md-9">
								<input id="gsdate" name="gsdate" class="form-control" type="text">
							</div>
						</div>
					</div>	
				  </div>
                </form> 				  
			</div>
			<div class="tab-pane fade" id="s4">
				<form class="form-horizontal">
				   <div class="row">
					<div class="col-sm-12">
					    <legend>Documents:</legend>
						<div class="form-group hidden">
							<label class="col-md-3 control-label">Form 137:</label>
							<div class="col-md-9">
								<input id="filea" name="filea" class="form-control" placeholder="Default Text Field" type="file">
							</div>
						</div>
				    </div>
                   </div>
                </form>				   
			</div>
			<div class="tab-pane fade" id="s5">
				<form class="form-horizontal">
				   <div class="row">
					<div class="col-sm-6">
					    <legend>Examination:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Schedule Date:</label>
							<div class="col-md-9">
							    <div class="input-group">
								  <input id="examid" name="examid" type="hidden" value="0">
								  <input id="examschedule" name="examschedule" class="form-control" placeholder="Default Text Field" type="text">
								  <span class="input-group-addon">
								      <i class="fa fa-search btn-search" data-filter="exam_sched"></i>
								  </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Result:</label>
							<div class="col-md-9">
								<input id="examresult" name="examresult" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Take E10:</label>
							<div class="col-md-9">
								<input id="takee10" name="takee10" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Remarks:</label>
							<div class="col-md-9">
								<input id="examremarks" name="examremarks" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
				    </div>
					<div class="col-sm-6">
					    <legend>Medical:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Schedule Date:</label>
							<div class="col-md-9">
							    <div class="input-group">
								  <input id="medicid" name="examid" type="hidden" value="0">
								  <input id="medshedule" name="medschedule" class="form-control" type="text">
								  <span class="input-group-addon">
								      <i class="fa fa-search btn-search" data-filter="medic_sched"></i>
								  </span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Result:</label>
							<div class="col-md-9">
								<input id="medresult" name="medresult" class="form-control" placeholder="Passed/Failed" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Remarks:</label>
							<div class="col-md-9">
								<input id="medremarks" name="medremarks" class="form-control" placeholder="Remarks" type="text">
							</div>
						</div>
				    </div>
                   </div>
                </form>				   
			</div>
			<div class="tab-pane fade" id="s6">
				<form class="form-horizontal">
				   <div class="row">
					<div class="col-sm-6">
					    <legend>Elementary:</legend>
						<div class="form-group">
							<label class="col-md-3 control-label">Name of School:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Address:</label>
							<div class="col-md-9">
								<input id="bplace" name="bplace" class="form-control" placeholder="Default Text Field" type="text">
							</div>
						</div>
				    </div>
                   </div>
                </form>				   
			</div>
			
		 </div>
	  </div>
	  
	  <div class="col-sm-6" style="margin-top:10px;">
		 <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" role="widget" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
			<header role="heading">
			    <div class="jarviswidget-ctrls" role="menu"></div>
				<h2><strong>Choice 1:</strong> <i></i></h2>				
			    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
            <div role="content">
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body">
				   <form class="form-horizontal">
			         <div class="col-sm-12">
					   <div class="form-group">
					        <label class="col-md-4 control-label">Course/Program:</label>
							<div class="col-md-8">
								<select id="proga" name="proga" class="form-control">
								  <option value="-1" selected disabled> - Select atleast one - </option>
								</select>
							</div>
					   </div>
					   <div class="form-group">
					        <label class="col-md-4 control-label">Major:</label>
							<div class="col-md-8">
								<select id="maja" name="maja" class="form-control">
								  <option value="-1" selected disabled> - Select atleast one - </option>
								</select>
							</div>
					   </div>
			         </div>
                    </form>			   
				</div>
			</div>
		</div>
	  </div>
	  
	  <div class="col-sm-6" style="margin-top:10px;">
		 <div class="jarviswidget jarviswidget-sortable" id="wid-id-0" role="widget" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
			<header role="heading">
			    <div class="jarviswidget-ctrls" role="menu"></div>
				<h2><strong>Choice 2:</strong> <i></i></h2>				
			    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
            <div role="content">
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body">
				   <form class="form-horizontal">
			         <div class="col-sm-12">
					   <div class="form-group">
					        <label class="col-md-4 control-label">Course/Program:</label>
							<div class="col-md-8">
								<select id="progb" name="progb" class="form-control">
								  <option value="-1" selected disabled> - Select atleast one - </option>
								</select>
							</div>
					   </div>
					   <div class="form-group">
					        <label class="col-md-4 control-label">Major:</label>
							<div class="col-md-8">
								<select id="majb" name="majb" class="form-control">
								  <option value="-1" selected disabled> - Select atleast one - </option>
								</select>
							</div>
					   </div>
			         </div>
                    </form>			   
				</div>
			</div>
		</div>
	  </div>
	  
	 
	<!-- widget options:
		usage: <div class="jarviswidget" id="wid-id-0">
		
		data-widget-colorbutton="false"	
		data-widget-editbutton="false"
		data-widget-togglebutton="false"
		data-widget-custombutton="false"
		data-widget-collapsed="true" 
		data-widget-sortable="false"
		
	-->
	  
    </div>	  
</div>	