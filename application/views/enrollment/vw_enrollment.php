<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Enrollment</li>
	</ol>
</div>   
<div id="content">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
 <?php if(isset($studentnolist) || isset($studentfilter)){echo $this->load->view('include/studentinfo','',true);}?>
</div>
<section id="widget-grid">
<div class="row">
<?php if(!isset($studentnolist) && !isset($studentfilter)){?>
<div class="col-sm-12">
<div class="well">
	<div id="studentinfo" name="studentinfo">
	    <input type="hidden" id="studentno" value="<?php echo ((isset($studentno))?$studentno:'');?>">
		<p><small>Student Name : <strong><?php echo utf8_encode($xdetail->StudentName);?></strong></small></p>
		<p><small>College Name : <strong><?php echo $xdetail->CollegeName;?></strong></small></p>
		<p><small>Academic Program : <strong><?php echo $xdetail->Program;?></strong></small></p>
		<p><small>Program Curriculum : <strong><?php echo $xdetail->Curriculum;?></strong></small></p>
	</div>
</div>
</div>
<?php } ?>
<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"  data-widget-deletebutton="false">
<header role="heading">			
<h2>Enrollment Information</h2>
<div class="widget-toolbar" role="menu">
 <button id='btnpreg' class="btn btn-primary"<?php echo ((!$buttons)?"disabled":"");?>><i class='fa fa-print'></i> Print PreReg</button>
 <button id='btncor' class="btn btn-warning"<?php echo ((!$buttons || $validation=='')?"disabled":"");?>><i class='fa fa-print'></i> Print C.O.R.</button>
</div>		
</header>
<div role="content">
 <?php echo $schedule;?>
</div> 
</div>
</article>
</div>
</section>
</div>