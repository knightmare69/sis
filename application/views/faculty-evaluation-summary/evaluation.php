<?php 

function getRating($score){
	$ret = "Poor";
	if ( floatval($score) > 4.5){
		$ret = "Excellent";
	}elseif(floatval($score) > 3.5){
		$ret = "Very Good";
	}elseif(floatval($score) > 2.5){
		$ret = "Good";
	}elseif(floatval($score) > 1.5){
		$ret = "Fair";
	}
	return $ret;
} 
$i=1; 
$tmp = "";
 foreach($data->result() as $d): 
    //$name = str_replace('"','',json_encode($d['Faculty']));
    ////$name = $data->row($i)['Faculty'];
    $name = $d->Questions;
    if($tmp != $d->CategoryID ){
    	$tmp = $d->CategoryID;
    	echo "<tr><td colspan='4' class='bold text-center'> Category ".$d->CategoryID." </td></tr>";
    }
?>  
<tr data-id="<?= $d->QuestionID ?>">
	<td class="autofit"><?= $i ?>.</td>
	<td class=""><?php echo mb_convert_encoding($name,'UTF-8') ?></td>		
    <td class="autofit mean text-center"><?= number_format($d->Mean,2) ?></td>
    <td><?= getRating(number_format($d->Mean,2))?></td>
</tr>
<?php $i++; endforeach; ?> 
    
<?php if(empty($data)): ?>    
<tr><td colspan="4"> No Record Found! </td></tr>
<?php endif; ?>