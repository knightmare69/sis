<?php
	$this->load->view('include/header',$data);
	$this->load->view('templates/mainmenu',$data);
    // print_r($data)
?>

<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Faculty Evaluation Summary</li>
	</ol>
</div>  

<div id="content">
    <div class="faculty-list">
    
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" role="widget">
								<!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"

								-->
								<header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li><li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li><li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li><li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li><li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li><li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li><li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li><li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li><li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li><li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li><li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li><li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li><li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li><li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li><li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li><li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li><li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li><li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li><li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li></ul></div>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Summary of Faculty Evaluation</h2>

								<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

								<!-- widget div-->
								<div role="content">

									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->

									</div>
									<!-- end widget edit box -->

									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="widget-body-toolbar">
											
                                            
											<div class="row ">
												
                                                
												<div class="col-xs-8 col-sm-4 col-md-4 col-lg-4">
                                                <div class="smart-form">
                                                
													<label class="select">
											         <select name="term" id="term">
                                                        <option value="0" selected="" disabled="">Academic Year & Term</option>                                                     
												        <?php foreach($data['terms'] as $t):?>
                                                        <option value="<?= $t->TermID ?>"><?= $t->YearTerm ?></option>
                                                        <?php endforeach; ?>
											         </select> <i></i> </label> 
                                                                                                
												</div>
												</div>
                                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
													
													<button class="btn btn-sm" id="btnrefresh">
														<i class="fa fa-refresh"></i> <span class="hidden-mobile">Refresh</span>
													</button>
													
													<button class="btn btn-sm btn-warning" id="btnsummary">
														<i class="fa fa-print"></i> <span class="hidden-mobile">Print</span>
													</button>
													
												</div>
												<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right pull-right">                                                                                                
                                                    <div class="input-group input-group-sm">
														<span class="input-group-addon"><i class="fa fa-search"></i></span>
														<input class="form-control input-sm filter-table" id="prepend" placeholder="Filter" type="text">
													</div>
                                                </div>                                                    
											</div>																					
										</div>                                        
										
                                        
                                        
										<div class="table-responsive">										
											<table id="tblfaculty" class="table table-bordered table-hover table-striped" style="cursor: pointer;" >
												<thead>
													<tr>
														<th class="text-center" width="80px">Action</th>
														<th>Faculty Name</th>
														<th>Department</th>
														<th class="bold text-center">Respondent</th>
                                                        <th class="bold text-center">Overall</th>
                                                        <th>Descriptive</th>
													</tr>
												</thead>
												<tbody>
                                                   <tr><td colspan="7"> No Record Found! </td></tr>
												</tbody>
											</table>
											
										</div>
                                        
									</div>
									<!-- end widget content -->

								</div>
								<!-- end widget div -->

							</div>
                            
       </div>
       <div class="evaluation-form">
       
       <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" role="widget">
								<!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"

								-->
								<header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li><li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li><li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li><li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li><li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li><li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li><li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li><li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li><li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li><li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li><li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li><li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li><li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li><li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li><li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li><li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li><li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li><li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li><li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li></ul></div>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Evaluation Form</h2>

								<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

								<!-- widget div-->
								<div role="content">

									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->

									</div>
									<!-- end widget edit box -->

									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="widget-body-toolbar">
											
                                            
											<div class="row ">
												
                                                
												<div class="col-xs-8 col-sm-5 col-md-5 col-lg-5">
                                                   <span class="bold font-lg">Name : <span id="selEmployee"></span></span>
												</div>
                                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
												 <span>Respondents : <span id="respondent">0</span>
												</div>
												<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-right pull-right">                                                                                                
                                                    	<button class="btn btn-sm btn-info" data-id="" data-term="" data-resp="0" id="btnprint">
															<i class="fa fa-print"></i> <span class="hidden-mobile">Print</span>
														</button>                                                                                                
                                                    	<button class="btn btn-sm" id="btncancel">
															<i class="fa fa-ban"></i> <span class="hidden-mobile">Cancel</span>
														</button>
                                                </div>                                                    
											</div>																					
										</div>                                        
										
                                        
                                        
										<div class="table-responsive">										
											<table id="tblquestions" class="table table-bordered table-hover table-striped" style="cursor: pointer;" >
												<thead>
													<tr>
														<th class="autofit">#</th>
														<th>Questions</th>														
                                                        <th class="bold text-center">Means</th>
                                                        <th class="bold text-center">Remarks</th>                                     
													</tr>
												</thead>
												<tbody>
                                                   <tr><td colspan="4"> No Record Found! </td></tr>
												</tbody>
											</table>

											<table id="tblcomment" class="table table-bordered table-hover table-striped" style="cursor: pointer;" >
												<thead>
													<tr>
														<th class="autofit">#</th>
														<th>Comment</th>													                                                                                       
													</tr>
												</thead>
												<tbody>
                                                   <tr><td colspan="2"> No Record Found! </td></tr>
												</tbody>
											</table>

											
										</div>                                        
									</div>
									<!-- end widget content -->

								</div>
								<!-- end widget div -->

							</div>
                            
                            
            
        </div>
                                        
                            
</div>
