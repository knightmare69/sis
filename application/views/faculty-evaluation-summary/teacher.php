<?php 
function getRating($score){
	$ret = "Poor";
	if ( floatval($score) > 4.5){
		$ret = "Excellent";
	}elseif(floatval($score) > 3.5){
		$ret = "Very Good";
	}elseif(floatval($score) > 2.5){
		$ret = "Good";
	}elseif(floatval($score) > 1.5){
		$ret = "Fair";
	}
	return $ret;
} 

$i=0; ?>
<?php  foreach($data->result() as $d): 
    //$name = str_replace('"','',json_encode($d['Faculty']));
    ////$name = $data->row($i)['Faculty'];
    $name = mb_convert_encoding($d->Faculty,'UTF-8');
    
?>  
<tr data-id="<?= $d->FacultyID ?>">
	<td><a href="javascript:void(0);" data-resp="<?= $d->Respondent ?>" data-id="<?= $d->FacultyID ?>" data-empn="<?= base64_encode($d->FacultyID) ?>" data-name="<?= $name; ?>" class="view">View</a>
	    <a href="javascript:void(0);" data-resp="<?= $d->Respondent ?>" data-id="<?= $d->FacultyID ?>" data-empn="<?= base64_encode($d->FacultyID) ?>" data-name="<?= $name; ?>" class="print">Print</a></td>
	<td class="autofit name"><?php echo $name; ?></td>	
	<td><?= $d->Dept ?></td>
    <td class="autofit bold text-center"><?= $d->Respondent ?></td>
    <td class="overall bold text-center"><?= number_format($d->Overall,2) ?></td>
    <td class="descriptive">
    	<?php if(floatval($d->Respondent) > 0 ) {
    		echo getRating(number_format($d->Overall,2));
    	}?>
    </td>    
</tr>
<?php $i++; endforeach; ?> 
    
<?php if(empty($data)): ?>    
<tr><td colspan="7"> No Record Found! </td></tr>
<?php endif; ?>        
    

