<?php
	$this->load->view('include/header',$data);
	$this->load->view('templates/mainmenu',$data);
?>
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><a id="mnufeval" href="<?php echo site_url('faculty-evaluation'); ?>">Faculty Evaluation</a></li>
	</ol>
</div>  
<div id="content">
    <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" role="widget">
								<header role="heading">
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Faculty Evaluation</h2>

								<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

								<!-- widget div-->
								<div role="content">

									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->

									</div>
									<!-- end widget edit box -->

									<!-- widget content -->
									<div class="widget-body">
									  <div class="tbl-list">
									    <button class="btn btn-warning btn-submit pull-right hidden"><i class="fa fa-lock"></i> Submit All</button>
									    <button class="btn btn-info btn-refresh pull-right">Refresh</button>
										<p>Click Evaluate to begin</p>
										<div class="table-responsive">
											<table class="table table-bordered table-hover table-striped" style="cursor: pointer;" >
												<thead>
													<tr>
														<th class="autofit">Action</th>
														<th>Faculty Name</th>
														<th>Subject Handle</th>
														<th class="autofit">Final Rating</th>
                                                        <th>Comment</th>
                                                        <th>Date Evaluated</th>
                                                        <th>Date Submitted</th>
													</tr>
												</thead>
												<tbody>
                                                    <?php echo $this->load->view('faculty-evaluation/list',$data,true); ?>
												</tbody>
											</table>
											
										</div>
									  </div>
									  <div class="frm-eval hidden">
									    <button class="btn btn-sm btn-success btn-save pull-right hidden"><i class="fa fa-save"></i> Save</button>
									    <button class="btn btn-sm btn-warning btn-submitthis pull-right"><i class="fa fa-lock"></i> Submit</button>
									    <p>You can score answer the following question with this rating scheme:</p>
										<div class="col-sm-4 col-md-2">Never or Very Rarely <b>1</b></div>
										<div class="col-sm-4 col-md-2">Sometimes <b>2</b></div>
										<div class="col-sm-4 col-md-2">Regularly <b>3</b></div>
                                                                                <div class="col-sm-2 hidden-md hidden-lg"></div>
										<div class="col-sm-4 col-md-2">Often <b>4</b></div>
										<div class="col-sm-4 col-md-2">Very Often or Always <b>5</b></div><br/><br/>
										
										<div class="table-responsive">
										    <input type="hidden" id="xid" name="xid" value="new"/>
										    <input type="hidden" id="template" name="template" value="1"/>
										    <input type="hidden" id="faculty" name="faculty"/>
										    <input type="hidden" id="schedule" name="schedule"/>
											<table class="table table-bordered table-hover" style="cursor: pointer;" >
												<thead>
													<tr>
														<th class="autofit">###</th>
														<th>Question</th>
														<th>Score</th>
													</tr>
												</thead>
												<tbody>
                                                    <?php echo $this->load->view('faculty-evaluation/eval',$data,true); ?>
												</tbody>
												<tfoot>
												  <td colspan="3">
												     <label>Comment:</label>
													 <input type="text" class="ptc_tbltextbox" id="eval_comment" name="eval_comment"/>
												  </td>
												</tfoot>
											</table>
										</div>
									  </div>
									</div>
									<!-- end widget content -->

								</div>
								<!-- end widget div -->

							</div>
                            
</div>

