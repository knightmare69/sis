
<?php 
    $i = 0;
    $s = -1;
    foreach($faculty as $r): 
     $s = (($s==-1)?0:$s);
     if(strtotime($r->DateSubmitted)){
       $s++;
     }
    $i++;
?>
<tr>
	<td class="text-center" ><a class="<?= ((strtotime($r->DateSubmitted))?'hidden':'btn-evaluate') ?>" href="javascript:void(0);" data-entry="<?= (($r->EntryID=='' || $r->EntryID==0)?'new':($r->EntryID));?>" data-id="<?= $r->EmployeeID;?>" data-sched="<?= $r->ScheduleID;?>">Evaluate</a> </td>
	<td class="bold"><?= $r->LastName.', '.$r->FirstName?></td>
	<td><?= $r->SubjectTitle?></td>
	<td class="autofit text-center"><?= floatval($r->FinalRating)?></td>
	<td><?= $r->Comment?></td>
	<td><?= ((strtotime($r->EvaluationDate))? date('m/d/Y',strtotime($r->EvaluationDate)):'') ?></td>
	<td><?= ((strtotime($r->DateSubmitted))? date('m/d/Y',strtotime($r->DateSubmitted)):'') ?></td>
</tr>
<?php 
endforeach; 
if($i==$s){
  echo '<tr><td colspan="7"><div class="alert alert-success"><i class="fa-fw fa fa-check"></i><strong>Done!</strong> You have submitted all your faculty evaluation. You can view your grades now.</div></td></tr>';
}
if($s=='-1'){
  echo '<tr><td colspan="7"><div class="alert alert-warning"><i class="fa-fw fa fa-check"></i><strong>Alert!</strong> Faculties to evaluate might not encode grades yet.</div></td></tr>';
}
?>