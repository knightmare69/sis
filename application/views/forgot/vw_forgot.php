
	<body id="login">
		<header id="header">
			<div id="logo-group"><span id="logo"> <img src="<?php echo base_url('assets/img/logo-login.png').'?'.rand(100,200); ?>" alt="PRISMS"> </span></div>
			<span id="login-header-space"> <span class="hidden-mobile">Already have an account?</span> <a href="<?php echo site_url('login'); ?>" class="btn btn-danger">Sign In</a> </span>
		</header>
		<div id="main" role="main">			
			<!-- MAIN CONTENT -->
			<div id="content" class="container">
                <?php
				if(isset($reseted) && ($reseted=='1' || $reseted==date('Ymd')))
				{
				?>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				  <div class="well" style="alignment-adjust: central;">
				   <h1><i class="fa fa-warning fa-2x" style="color:orange;"></i> Warning!</h1>
				   <p>You already request for reset password of your account. Please check your email for reset password email!</p>
				   <p><small>(If something went wrong, Please contact this email (<?php echo ((defined('HELP_DESK'))?HELP_DESK:'jhe69samson@gmail.com');?>))</small></p>
				   <p><a href="<?php echo site_url('login'); ?>" class="btn btn-info btn-sm" role="button">Back to log in</a></p>
				  </div>
				 </div>
				<?php	
				}	
				else
				{	
				?>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
		              <?php $this->load->view('include/description');?>
	                </div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
						<div class="well no-padding">							
                           <?php echo form_open('forgot/reset',array('id'=>'reset-form','class' => 'smart-form client-form','onsubmit'=>'return false;')); ?>
								<header>
									Forgot Password or Username?
								</header>
								 <fieldset class="optstylesel">
									<section>
									 <label class="label">I forgot:</label>
									 <button class="btn btn-default btn-sm btnpass" type="button" data-target="optstyle0"><i class="fa fa-lock"></i> my account password</button>
									 <button class="btn btn-default btn-sm btninfo" type="button" data-target="optstyle1"><i class="fa fa-envelope"></i> my username / email</button>
									</section>
								 </fieldset>	
								 <fieldset class="optstyle0 hidden">
									<section>									
										<label class="label">Your Email:<i>(Be sure this is the exact email in your account)</i></label>
										<label class="input"> <i class="icon-append fa fa-envelope"></i>
											<input type="email" name="email" id="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter email address for password reset</b></label>
									</section>
									<section>
										<span class="timeline-seperator text-center text-primary"> <span class="font-sm">OR</span></span>
									</section>
									<section>
										<label class="label">Your Username</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="username" id="username">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Enter your username</b> </label>
									</section>
							     </fieldset>
							     <fieldset class="optstyle1 hidden">
									<section>
										<label class="label">Your IDNo</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="idno" id="idno">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Enter your idno</b> 
										</label>
									</section>
									<div class="optadditional hidden">
									<section>
										<label class="label">What is your <b class="question">latest RegID</b>?</label>
										<label class="input"> <i class="icon-append fa fa-question"></i>
											<input type="text" name="answer" id="answer">
											<b class="tooltip tooltip-top-right"><i class="fa fa-question txt-color-teal"></i> Enter your answer</b> 
										</label>
									</section>
									<section>
										<label class="label">Active Email:</label>
										<label class="input"> <i class="icon-append fa fa-envelope"></i>
											<input type="text" name="amail" id="amail">
											<b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Enter your active email</b> 
										</label>
									</section>
									<section class='hidden'>
										<label class="checkbox">
											<input type="checkbox" name="emailme" id="emailme" checked="true">
											 <i></i> Email Me
											<b class="tooltip tooltip-top-right"><i class="fa fa-question txt-color-teal"></i></b> 
										</label>
									</section>
									</div>
							   </fieldset>
							   <fieldset class="optloading hidden">
								 <section><i class="fa fa-refresh fa-spin"></i> Loading...</section>
							   </fieldset>	
							   <fieldset class="opterror no-padding hidden">
								 <div class="alert alert-danger" style="margin-bottom:2px;">
									<i class="fa-fw fa fa-warning"></i><strong>Error!</strong>
									<b class="alert-content">Failed to reset your password.</b>
								 </div>
							   </fieldset>	
							   <footer>
								    <div class="note">
									   <a href="<?php echo site_url('login'); ?>">I remembered my password!</a>
								    </div>
									<button type="button" class="btn btn-warning btnback pull-left hidden" data-target="optstylesel"><i class="fa fa-toggle-left"></i> Back</button>
									<button type="button" class="btn btn-warning btnnext hidden" data-target="optstylesel"><i class="fa fa-toggle-right"></i> Next</button>
									<button type="submit" class="btn btn-primary btnsubmit hidden"><i class="fa fa-refresh"></i> Reset Password</button>
							   </footer>
							</form>

						</div>
						
						
					</div>
				</div>
				<?php
				}
				?>
			</div>

		</div>


		</div>
		<!--anti depressant-->
		<?php $this->load->view('templates/antidepressant',array('hidden'=>true));?>
		<!-- end of anti depressant-->

		<!-- For Local Used -->
		<script src="<?php echo base_url('assets/js/libs/jquery-2.0.2.min.js') ; ?>"></script>
		<script src="<?php echo base_url('assets/js/libs/jquery-ui-1.10.3.min.js') ; ?>"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

		<!-- BOOTSTRAP JS -->		
		<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url('assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js') ?>"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url('assets/js/plugin/fastclick/fastclick.js') ?>"></script>

		<!--[if IE 7]>    
		 <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>    
		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url('assets/js/app.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/utilities/forgot.js') ?>" type="text/javascript"></script>
	</body>