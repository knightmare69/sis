<div id="content" class="container">
	<div class="row">
	 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden-xs hidden-sm">
	  <div class="well" style="alignment-adjust: central;">
		<h1>Your Password Reset was a success!</h1>
		<?php
		if(isset($issent) && $issent)
		 echo '<p>You will receive an email containing your new password.</p>';
		else if(isset($uname) && isset($npwd) && $uname!='' && $npwd!='')
		{
         echo '<p>Try this..</p>';
         echo '<p>Username:<b>'.((isset($uname))?$uname:'').'</b></p>';
         echo '<p>Password:<b>'.((isset($npwd))?$npwd:'').'</b></p>';
         echo '<br/><i>*Note:Please check your spelling and case.</i>';
		}
		else
		{
		 echo '<p>Please communicate with the administrator for your password.</p>';	
		}	
		?>
		
		<p><a href="<?php echo site_url('login'); ?>" class="btn btn-info btn-sm" role="button">Back to log in</a></p>
	  </div>
	 </div>
	</div>
</div>
