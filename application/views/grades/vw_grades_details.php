      <?php
	  if($progclass > 20)
	  {	  
	  ?>
	  <div class="hidden printeronly">
	  <?php if(isset($printheader)) echo $printheader;?>
	  </div>
      <table  id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
	    <thead>
			<tr class="odd gradeX">
				<th><strong>#</strong></th>
				<th><strong>Code</strong></th>
				<th><strong>Descriptive Title</strong></th>
				<th class="text-align-center"><strong>Unit</strong></th>
				<th class="text-align-center"><strong>Schedule</strong></th>
				<th class="text-align-center"><strong>Midterm</strong></th>
				<th class="text-align-center"><strong>Final</strong></th>
				<th class="text-align-center"><strong>Re-Exam</strong></th>
				<th><strong>Remarks</strong></th>
				<th><strong>Midterm Date Posted</strong></th>
				<th><strong>Final Date Posted</strong></th>
			</tr>
		</thead>

		 <?php 
		 $i = 1;
		 if($ds)
		 {	 
	      $disp_mid = 0;
	      $disp_fin = 0;
		  /*
		  foreach($ds as $rs)
		  {
			$final = property_exists($rs,'Final')?$rs->Final:'';
			$midterm =property_exists($rs,'Midterm')? $rs->Midterm:'';
			$graderemarks = property_exists($rs,'GradeRemarks')?$rs->GradeRemarks:'';
			$midtermposting = property_exists($rs,'MidtermGradesPostingDate')? $rs->MidtermGradesPostingDate : '';
			$finalposting = property_exists($rs,'DatePosted')? $rs->DatePosted : '';
		  }
		  */
		  foreach($ds as $rs)
		  {
			$final = property_exists($rs,'Final')?$rs->Final:'';
			$midterm =property_exists($rs,'Midterm')? $rs->Midterm:'';
			$graderemarks = property_exists($rs,'GradeRemarks')?$rs->GradeRemarks:'';
			$midtermposting = property_exists($rs,'MidtermGradesPostingDate')? $rs->MidtermGradesPostingDate : '';
			$finalposting = property_exists($rs,'DatePosted')? $rs->DatePosted : '';
			
			if(trim($finalposting)==''){
			 $final = "";
			 $graderemarks = "";
			}
                        if(trim($midtermposting)==''){
			 $midterm = "";
			}
			
			$sched = 'TBA';
			$midtermposting = (($midtermposting!='')?date('Y-m-d',strtotime($midtermposting)):'');
			$finalposting   = (($finalposting!='')?date('Y-m-d',strtotime($finalposting)):'');
			
			if($rs->Sched_1!=''){
			  $sched = $rs->Sched_1;	
			}
			if($rs->Sched_2!=''){
			  $sched = (($sched!='' && $sched!='TBA')? ($sched.'<br/>'):'').$rs->Sched_2;	
			}
			if($rs->Sched_3!=''){
			  $sched = (($sched!='' && $sched!='TBA')? ($sched.'<br/>'):'').$rs->Sched_3;	
			}
			if($rs->Sched_4!=''){
			  $sched = (($sched!='' && $sched!='TBA')? ($sched.'<br/>'):'').$rs->Sched_4;	
			}
			if($rs->Sched_5!=''){
			  $sched = (($sched!='' && $sched!='TBA')? ($sched.'<br/>'):'').$rs->Sched_5;	
			}
			
			
			echo '<tr data-grade="1" class="'.(($graderemarks=='Failed' || $graderemarks=='Withdrawn' || $graderemarks=='Dropped')?"txt-color-red font-bold":"").'">
			        <td class="autofit text-center font-xs"><small>'.$i.'</small></td>
    				<td ><small>'.trim($rs->Subject).'</small></td>
    				<td><small>'.trim($rs->Title).'</td>
    				<td class="text-center"><small>'.trim($rs->Unit).'</small></td>				
    				<td class="text-center"><small>'.trim($sched).'</small></td>				
    				<td class="text-center autofit"><small>'.trim($midterm).'</small></td>
    				<td class="text-center font-bold"><small>'.trim($final).'</small></td>
    				<td class="text-center"><small>'.trim($rs->ReExam).'</small></td>
    				<td><small><b>'.trim($graderemarks).'</b></small></td>
    				<td data-midpost="'.((trim($midtermposting)!='')?1:0).'"><small>'.trim($midtermposting).'</small></td>			
    				<td data-finpost="'.((trim($finalposting)!='')?1:0).'"><small>'.trim($finalposting).'</small></td>	
			     </tr>';
		  $i++;
		  }
		 } 
		?>
	  </table>
	  <?php
	  }
	  else
	  {
	   if(!defined('GRADES_GSHS_GRADE1'))  define('GRADES_GSHS_GRADE1',0);
	   if(!defined('GRADES_GSHS_LGRADE1')) define('GRADES_GSHS_LGRADE1',0);
	   if(!defined('GRADES_GSHS_GRADE2'))  define('GRADES_GSHS_GRADE2',0);
	   if(!defined('GRADES_GSHS_LGRADE2')) define('GRADES_GSHS_LGRADE2',0);
	   if(!defined('GRADES_GSHS_GRADE3'))  define('GRADES_GSHS_GRADE3',0);
	   if(!defined('GRADES_GSHS_LGRADE3')) define('GRADES_GSHS_LGRADE3',0);
	   if(!defined('GRADES_GSHS_GRADE4'))  define('GRADES_GSHS_GRADE4',0);
	   if(!defined('GRADES_GSHS_LGRADE4')) define('GRADES_GSHS_LGRADE4',0);
	   if(!defined('GRADES_GSHS_GRADEF'))  define('GRADES_GSHS_GRADEF',0);
	   if(!defined('GRADES_GSHS_LGRADEF')) define('GRADES_GSHS_LGRADEF',0);
	   
	   if(!defined('GRADES_GSHS_CONDUCT1'))  define('GRADES_GSHS_CONDUCT1',0);
	   if(!defined('GRADES_GSHS_LCONDUCT1')) define('GRADES_GSHS_LCONDUCT1',0);
	   if(!defined('GRADES_GSHS_CONDUCT2'))  define('GRADES_GSHS_CONDUCT2',0);
	   if(!defined('GRADES_GSHS_LCONDUCT2')) define('GRADES_GSHS_LCONDUCT2',0);
	   if(!defined('GRADES_GSHS_CONDUCT3'))  define('GRADES_GSHS_CONDUCT3',0);
	   if(!defined('GRADES_GSHS_LCONDUCT3')) define('GRADES_GSHS_LCONDUCT3',0);
	   if(!defined('GRADES_GSHS_CONDUCT4'))  define('GRADES_GSHS_CONDUCT4',0);
	   if(!defined('GRADES_GSHS_LCONDUCT4')) define('GRADES_GSHS_LCONDUCT4',0);
	   
       $ACAD_COLUMNS = GRADES_GSHS_GRADE1+GRADES_GSHS_LGRADE1+GRADES_GSHS_GRADE2+GRADES_GSHS_LGRADE2+GRADES_GSHS_GRADE3+GRADES_GSHS_LGRADE3+GRADES_GSHS_GRADE4+GRADES_GSHS_LGRADE4+GRADES_GSHS_GRADEF+GRADES_GSHS_LGRADEF+1;
	   $COND_COLUMNS = GRADES_GSHS_CONDUCT1+GRADES_GSHS_LCONDUCT1+GRADES_GSHS_CONDUCT2+GRADES_GSHS_LCONDUCT2+GRADES_GSHS_CONDUCT3+GRADES_GSHS_LCONDUCT3+GRADES_GSHS_CONDUCT4+GRADES_GSHS_LCONDUCT4;
	   
       if(isset($withchinese) && $withchinese==true)
       {		   
	  ?>
	   <div class="tabbable">
       <ul class="nav nav-tabs">
         <li class="pull-right"><a data-toggle="tab" href="#cs"><i class="fa fa-book"></i> Chinese Subjects</a></li>
         <li class="pull-right active"><a data-toggle="tab" href="#rs"><i class="fa fa-book"></i> English Subjects</a></li>
       </ul>
       <div class="tab-content">
       <div id="rs" class="tab-pane fade in active">
       <?php 
	   }
	   ?>
	   <table  id="dt_basic2"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
	    <thead>
			<tr class="odd gradeX">
				<th class="text-center"><strong>SUBJECTS</strong></th>
				<th <?php echo (($ACAD_COLUMNS>1)?'colspan="'.$ACAD_COLUMNS.'" class="text-center"':(($ACAD_COLUMNS<1)?'class="hidden"':'class="text-center"'));?>><strong>ACADEMIC</strong></th>
				<th <?php echo (($COND_COLUMNS>1)?'colspan="'.$COND_COLUMNS.'" class="text-center"':(($COND_COLUMNS<1)?'class="hidden"':'class="text-center"'));?>><strong>CONDUCT</strong></th>
			</tr>
			<tr class="odd gradeX">
			    <th></th>
				<th <?php echo ((GRADES_GSHS_GRADE1==1 && GRADES_GSHS_LGRADE1==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>1st Quarter</strong></th>
				<th <?php echo ((GRADES_GSHS_GRADE2==1 && GRADES_GSHS_LGRADE2==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>2nd Quarter</strong></th>
				<th <?php echo ((GRADES_GSHS_GRADE3==1 && GRADES_GSHS_LGRADE3==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>3rd Quarter</strong></th>
				<th <?php echo ((GRADES_GSHS_GRADE4==1 && GRADES_GSHS_LGRADE4==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>4th Quarter</strong></th>
				<th <?php echo ((GRADES_GSHS_GRADEF==1 && GRADES_GSHS_LGRADEF==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>Final Grade</strong></th>
				<th <?php echo ((GRADES_GSHS_CONDUCT1==1 && GRADES_GSHS_LCONDUCT1==1)?'colspan="2" class="text-center"':((GRADES_GSHS_CONDUCT1==0 && GRADES_GSHS_LCONDUCT1==0)?'class="hidden"':'class="text-center"')); ?>><strong>1st</strong></th>
				<th <?php echo ((GRADES_GSHS_CONDUCT2==1 && GRADES_GSHS_LCONDUCT2==1)?'colspan="2" class="text-center"':((GRADES_GSHS_CONDUCT2==0 && GRADES_GSHS_LCONDUCT2==0)?'class="hidden"':'class="text-center"')); ?>><strong>2nd</strong></th>
				<th <?php echo ((GRADES_GSHS_CONDUCT3==1 && GRADES_GSHS_LCONDUCT3==1)?'colspan="2" class="text-center"':((GRADES_GSHS_CONDUCT3==0 && GRADES_GSHS_LCONDUCT3==0)?'class="hidden"':'class="text-center"')); ?>><strong>3rd</strong></th>
				<th <?php echo ((GRADES_GSHS_CONDUCT4==1 && GRADES_GSHS_LCONDUCT4==1)?'colspan="2" class="text-center"':((GRADES_GSHS_CONDUCT1==0 && GRADES_GSHS_LCONDUCT4==0)?'class="hidden"':'class="text-center"')); ?>><strong>4th</strong></th>
				<th><strong>Remarks</strong></th>
			</tr>
		</thead>
		<tbody>
		 <?php
		 $supp1=false;
		 $avg1=0;
		 $supp2=false;
		 $avg2=0;
		 $supp3=false;
		 $avg3=0;
		 $supp4=false;
		 $avg4=0;
		 $suppf=false;
		 $avgf=0;
		 
		 $avgremarks='';
		 $footer = '';
		 
		 function sort_objects($a, $b)
		 {
	      if($a->SeqNo == $b->SeqNo){ return 0 ; }
	      return ($a->SeqNo < $b->SeqNo) ? -1 : 1;
		 }
		 usort($ds,'sort_objects');
		 foreach($ds as $rs)
		 {   
		     $weight         =((property_exists($rs,'weight'))?($rs->weight):0);
			 $units          =((property_exists($rs,'Units'))?($rs->Units):0);
			 $IsChinese      = ((property_exists($rs,'IsChinese'))?($rs->IsChinese):0);
			 $ChineseSubject = ((property_exists($rs,'ChineseSubject'))?($rs->ChineseSubject):'');
			 if($IsChinese==0 && trim($ChineseSubject)=='')
			 {
				echo '<tr>
				      <td width="20%" units="'.(double)$weight.'">'.$rs->SubjectTitle.((trim($ChineseSubject)!='')?'('.$ChineseSubject.')':'').'</td>
				      '.((GRADES_GSHS_GRADE1==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->Grade1 > 1)?number_format((double)$rs->Grade1, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADE1==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADE1==1)?'colspan="2" grade="'.$rs->Grade1.'"':'').'>'.(($rs->Grade1>1)?$rs->LGrade1:'').'</td>':'').'
				      '.((GRADES_GSHS_GRADE2==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->Grade2 > 1)?number_format((double)$rs->Grade2, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADE2==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADE2==1)?'colspan="2" grade="'.$rs->Grade2.'"':'').'>'.(($rs->Grade2>1)?$rs->LGrade2:'').'</td>':'').'
				      '.((GRADES_GSHS_GRADE3==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->Grade3 > 1)?number_format((double)$rs->Grade3, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADE3==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADE3==1)?'colspan="2" grade="'.$rs->Grade3.'"':'').'>'.(($rs->Grade3>1)?$rs->LGrade3:'').'</td>':'').'
				      '.((GRADES_GSHS_GRADE4==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->Grade4 > 1)?number_format((double)$rs->Grade4, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADE4==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADE4==1)?'colspan="2" grade="'.$rs->Grade4.'"':'').'>'.(($rs->Grade4>1)?$rs->LGrade4:'').'</td>':'').'
				      '.((GRADES_GSHS_GRADEF==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->FinalGrade > 1)?number_format((double)$rs->FinalGrade, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADEF==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADEF==1)?'colspan="2"':'').'>'.(($rs->FinalGrade>1)?$rs->LFinalGrade:'').'</td>':'').'
				      '.((GRADES_GSHS_CONDUCT1==1)?'<td width="5%" class="text-center">'.$rs->Con1.'</td>':'').'
				      '.((GRADES_GSHS_LCONDUCT1==1)?'<td width="5%" class="text-center">'.$rs->vCon1.'</td>':'').'
				      '.((GRADES_GSHS_CONDUCT2==1)?'<td width="5%" class="text-center">'.$rs->Con2.'</td>':'').'
				      '.((GRADES_GSHS_LCONDUCT2==1)?'<td width="5%" class="text-center">'.$rs->vCon2.'</td>':'').'
				      '.((GRADES_GSHS_CONDUCT3==1)?'<td width="5%" class="text-center">'.$rs->Con3.'</td>':'').'
				      '.((GRADES_GSHS_LCONDUCT3==1)?'<td width="5%" class="text-center">'.$rs->vCon3.'</td>':'').'
				      '.((GRADES_GSHS_CONDUCT4==1)?'<td width="5%" class="text-center">'.$rs->Con4.'</td>':'').'
				      '.((GRADES_GSHS_LCONDUCT4==1)?'<td width="5%" class="text-center">'.$rs->vCon4.'</td>':'').'
				      <td width="10%">'.$rs->Remarks.'</td>
				     </tr>'; 
			    
				if(GRADES_GSHS_PERIOD_AVE==1)
		        {
				 if($rs->YearLevelID==7 && $rs->ProgClassID==20)
				 {
				   $avg1 = $avg1+((double)$units * (double)$rs->Grade1);
		           $avg2 = $avg2+((double)$units * (double)$rs->Grade2);
		           $avg3 = $avg3+((double)$units * (double)$rs->Grade3);
		           $avg4 = $avg4+((double)$units * (double)$rs->Grade4);
				 }
				 else
				 {
		           $avg1 = $avg1+((double)$weight * (double)$rs->Grade1);
		           $avg2 = $avg2+((double)$weight * (double)$rs->Grade2);
		           $avg3 = $avg3+((double)$weight * (double)$rs->Grade3);
		           $avg4 = $avg4+((double)$weight * (double)$rs->Grade4);
				 }
				 
				 
				 if($rs->Grade1<=1) $supp1=true;
				 if($rs->Grade2<=1) $supp2=true;
				 if($rs->Grade3<=1) $supp3=true;
				 if($rs->Grade4<=1) $supp4=true;
				 
				 if($supp1) $avg1=0;
				 if($supp2) $avg2=0;
				 if($supp3) $avg3=0;
				 if($supp4) $avg4=0;
				 
				 $avgf= (($avg1!=0 && $avg2!=0 && $avg3!=0 && $avg4!=0)?(($avg1+$avg2+$avg3+$avg4)/4):'');
				 $avgremarks = (($avg1!=0 && $avg2!=0 && $avg3!=0 && $avg4!=0)?(($avgf>=74.5)?'PASSED':'FAILED'):'');
				 
				 $footer =  '<footer>
							  <tr>
							  <td width="20%" class="text-center"><strong>PERIODIC AVERAGE</strong></td>
							  '.((GRADES_GSHS_GRADE1==1 || GRADES_GSHS_LGRADE1==1)?'<td width="10%" colspan="2" class="text-center">'.(($avg1>0)?number_format((double)$avg1, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_GRADE2==1 || GRADES_GSHS_LGRADE2==1)?'<td width="10%" colspan="2" class="text-center">'.(($avg2>0)?number_format((double)$avg2, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_GRADE3==1 || GRADES_GSHS_LGRADE3==1)?'<td width="10%" colspan="2" class="text-center">'.(($avg3>0)?number_format((double)$avg3, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_GRADE4==1 || GRADES_GSHS_LGRADE4==1)?'<td width="10%" colspan="2" class="text-center">'.(($avg4>0)?number_format((double)$avg4, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_GRADEF==1 || GRADES_GSHS_LGRADEF==1)?'<td width="10%" colspan="2" class="text-center">'.(($avgf>0)?number_format((double)$avgf, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_CONDUCT1==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_LCONDUCT1==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_CONDUCT2==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_LCONDUCT2==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_CONDUCT3==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_LCONDUCT3==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_CONDUCT4==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_LCONDUCT4==1)?'<td width="5%" class="text-center"></td>':'').'
							  <td width="10%"><strong>'.$avgremarks.'</strong></td>
							 </tr>
							 </footer>';	
		        }
			 }
		 }	 
		 ?>
		</tbody>
		<?php
		if(GRADES_GSHS_PERIOD_AVE==1){echo $footer;}
		?>
        </table>
		</div>
	   <?php
	   if(isset($withchinese) && $withchinese==true)
       {		   
	   ?>
       <div id="cs" class="tab-pane fade">
       <table  id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
	    <thead class="text-center">
			<tr class="odd gradeX">
				<th class="text-center"><strong>SUBJECTS</strong></th>
				<th <?php echo (($ACAD_COLUMNS>1)?'colspan="'.$ACAD_COLUMNS.'" class="text-center"':(($ACAD_COLUMNS<1)?'class="hidden"':'class="text-center"'));?>><strong>ACADEMIC</strong></th>
				<th <?php echo (($COND_COLUMNS>1)?'colspan="'.$COND_COLUMNS.'" class="text-center"':(($COND_COLUMNS<1)?'class="hidden"':'class="text-center"'));?>><strong>CONDUCT</strong></th>
			</tr>
			<tr class="odd gradeX">
			    <th></th>
				<th <?php echo ((GRADES_GSHS_GRADE1==1 && GRADES_GSHS_LGRADE1==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>1st Quarter</strong></th>
				<th <?php echo ((GRADES_GSHS_GRADE2==1 && GRADES_GSHS_LGRADE2==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>2nd Quarter</strong></th>
				<th <?php echo ((GRADES_GSHS_GRADE3==1 && GRADES_GSHS_LGRADE3==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>3rd Quarter</strong></th>
				<th <?php echo ((GRADES_GSHS_GRADE4==1 && GRADES_GSHS_LGRADE4==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>4th Quarter</strong></th>
				<th <?php echo ((GRADES_GSHS_GRADEF==1 && GRADES_GSHS_LGRADEF==1)?'colspan="2" class="text-center"':'class="text-center"'); ?>><strong>Final Grade</strong></th>
				<th <?php echo ((GRADES_GSHS_CONDUCT1==1 && GRADES_GSHS_LCONDUCT1==1)?'colspan="2" class="text-center"':((GRADES_GSHS_CONDUCT1==0 && GRADES_GSHS_LCONDUCT1==0)?'class="hidden"':'class="text-center"')); ?>><strong>1st</strong></th>
				<th <?php echo ((GRADES_GSHS_CONDUCT2==1 && GRADES_GSHS_LCONDUCT2==1)?'colspan="2" class="text-center"':((GRADES_GSHS_CONDUCT2==0 && GRADES_GSHS_LCONDUCT2==0)?'class="hidden"':'class="text-center"')); ?>><strong>2nd</strong></th>
				<th <?php echo ((GRADES_GSHS_CONDUCT3==1 && GRADES_GSHS_LCONDUCT3==1)?'colspan="2" class="text-center"':((GRADES_GSHS_CONDUCT3==0 && GRADES_GSHS_LCONDUCT3==0)?'class="hidden"':'class="text-center"')); ?>><strong>3rd</strong></th>
				<th <?php echo ((GRADES_GSHS_CONDUCT4==1 && GRADES_GSHS_LCONDUCT4==1)?'colspan="2" class="text-center"':((GRADES_GSHS_CONDUCT1==0 && GRADES_GSHS_LCONDUCT4==0)?'class="hidden"':'class="text-center"')); ?>><strong>4th</strong></th>
				<th><strong>Remarks</strong></th>
			</tr>
		</thead>
		<tbody>
		 <?php
		 $avg1=0;
		 $avg2=0;
		 $avg3=0;
		 $avg4=0;
		 $avgf=0;
		 
		 $avgremarks='';
		 $footer = '';
		 
		 foreach($ds as $rs)
		 {   
		     $weight         =((property_exists($rs,'weight'))?($rs->weight):0);
			 $units          =((property_exists($rs,'Units'))?($rs->Units):0);
			 $IsChinese      = ((property_exists($rs,'IsChinese'))?($rs->IsChinese):0);
			 $ChineseSubject = ((property_exists($rs,'ChineseSubject'))?($rs->ChineseSubject):'');
			 $Ampersand      = ((property_exists($rs,'Ampersand'))?($rs->Ampersand):'');
		     if($IsChinese==1 || trim($ChineseSubject)!='')
			 {
				echo '<tr>
				      <td width="20%">'.$rs->SubjectTitle.((trim($ChineseSubject)!='')?'('.$Ampersand.')':'').'</td>
				      '.((GRADES_GSHS_GRADE1==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->Grade1 > 1)?number_format((double)$rs->Grade1, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADE1==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADE1==1)?'colspan="2" grade="'.$rs->Grade1.'"':'').'>'.(($rs->Grade1>1)?$rs->LGrade1:'').'</td>':'').'
				      '.((GRADES_GSHS_GRADE2==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->Grade2 > 1)?number_format((double)$rs->Grade2, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADE2==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADE2==1)?'colspan="2" grade="'.$rs->Grade2.'"':'').'>'.(($rs->Grade2>1)?$rs->LGrade2:'').'</td>':'').'
				      '.((GRADES_GSHS_GRADE3==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->Grade3 > 1)?number_format((double)$rs->Grade3, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADE3==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADE3==1)?'colspan="2" grade="'.$rs->Grade3.'"':'').'>'.(($rs->Grade3>1)?$rs->LGrade3:'').'</td>':'').'
				      '.((GRADES_GSHS_GRADE4==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->Grade4 > 1)?number_format((double)$rs->Grade4, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADE4==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADE4==1)?'colspan="2" grade="'.$rs->Grade4.'"':'').'>'.(($rs->Grade4>1)?$rs->LGrade4:'').'</td>':'').'
				      '.((GRADES_GSHS_GRADEF==1 && $rs->IsNonAcademic==0)?'<td width="5%" class="text-center">'.(($rs->FinalGrade > 1)?number_format((double)$rs->FinalGrade, 2, '.', ','):'').'</td>':'').'
				      '.((GRADES_GSHS_LGRADEF==1 || $rs->IsNonAcademic==1)?'<td width="5%" class="text-center" '.(($rs->IsNonAcademic==1 && GRADES_GSHS_LGRADEF==1)?'colspan="2"':'').'>'.(($rs->FinalGrade>1)?$rs->LFinalGrade:'').'</td>':'').'
				      '.((GRADES_GSHS_CONDUCT1==1)?'<td width="5%" class="text-center">'.$rs->Con1.'</td>':'').'
				      '.((GRADES_GSHS_LCONDUCT1==1)?'<td width="5%" class="text-center">'.$rs->vCon1.'</td>':'').'
				      '.((GRADES_GSHS_CONDUCT2==1)?'<td width="5%" class="text-center">'.$rs->Con2.'</td>':'').'
				      '.((GRADES_GSHS_LCONDUCT2==1)?'<td width="5%" class="text-center">'.$rs->vCon2.'</td>':'').'
				      '.((GRADES_GSHS_CONDUCT3==1)?'<td width="5%" class="text-center">'.$rs->Con3.'</td>':'').'
				      '.((GRADES_GSHS_LCONDUCT3==1)?'<td width="5%" class="text-center">'.$rs->vCon3.'</td>':'').'
				      '.((GRADES_GSHS_CONDUCT4==1)?'<td width="5%" class="text-center">'.$rs->Con4.'</td>':'').'
				      '.((GRADES_GSHS_LCONDUCT4==1)?'<td width="5%" class="text-center">'.$rs->vCon4.'</td>':'').'
				      <td width="10%">'.$rs->Remarks.'</td>
				     </tr>'; 
			    
				
				if(GRADES_GSHS_PERIOD_AVE==1)
		        {
				  $avg1 = $avg1+((double)$weight * (double)$rs->Grade1);
		          $avg2 = $avg2+((double)$weight * (double)$rs->Grade2);
		          $avg3 = $avg3+((double)$weight * (double)$rs->Grade3);
		          $avg4 = $avg4+((double)$weight * (double)$rs->Grade4);
				 
				 if($rs->Grade1<=1) $supp1=true;
				 if($rs->Grade2<=1) $supp2=true;
				 if($rs->Grade3<=1) $supp3=true;
				 if($rs->Grade4<=1) $supp4=true;
				 
				 if($supp1) $avg1=0;
				 if($supp2) $avg2=0;
				 if($supp3) $avg3=0;
				 if($supp4) $avg4=0;
				 
				 $avgf= (($avg1!=0 && $avg2!=0 && $avg3!=0 && $avg4!=0)?(($avg1+$avg2+$avg3+$avg4)/4):'');
				 $avgremarks = (($avg1!=0 && $avg2!=0 && $avg3!=0 && $avg4!=0)?(($avgf>=74.5)?'PASSED':'FAILED'):'');
				 
				 $footer =  '<footer>
							  <tr>
							  <td width="20%" class="text-center"><strong>PERIODIC AVERAGE</strong></td>
							  '.((GRADES_GSHS_GRADE1==1 || GRADES_GSHS_LGRADE1==1)?'<td width="10%" colspan="2" class="text-center">'.(($avg1>0)?number_format((double)$avg1, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_GRADE2==1 || GRADES_GSHS_LGRADE2==1)?'<td width="10%" colspan="2" class="text-center">'.(($avg2>0)?number_format((double)$avg2, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_GRADE3==1 || GRADES_GSHS_LGRADE3==1)?'<td width="10%" colspan="2" class="text-center">'.(($avg3>0)?number_format((double)$avg3, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_GRADE4==1 || GRADES_GSHS_LGRADE4==1)?'<td width="10%" colspan="2" class="text-center">'.(($avg4>0)?number_format((double)$avg4, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_GRADEF==1 || GRADES_GSHS_LGRADEF==1)?'<td width="10%" colspan="2" class="text-center">'.(($avgf>0)?number_format((double)$avgf, 2, '.', ','):'').'</td>':'').'
							  '.((GRADES_GSHS_CONDUCT1==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_LCONDUCT1==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_CONDUCT2==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_LCONDUCT2==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_CONDUCT3==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_LCONDUCT3==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_CONDUCT4==1)?'<td width="5%" class="text-center"></td>':'').'
							  '.((GRADES_GSHS_LCONDUCT4==1)?'<td width="5%" class="text-center"></td>':'').'
							  <td width="10%"><strong>'.$avgremarks.'</strong></td>
							 </tr>
							 </footer>';	
		        }
			 }
		 }		 
		 ?>
		</tbody>
		<?php
		if(GRADES_GSHS_PERIOD_AVE==1){echo $footer;}
		?>
        </table>
		</div>
	   </div>
	   </div>
	  <?php
	   }
	  }
	  ?>	
	
	<!--
	
	-->