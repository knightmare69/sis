
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Dashboard</li>
	</ol>
</div>   
<div id="content">	 
	 <div class="row">
		<div class="col-lg-12">		  
		  <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
			 <?php  if($userinfo['idtype'] == 5){
				  echo "<ul id='sparks' class=''>
				 <li class='sparks-info'>
					 <h5> Today's Income <span class='txt-color-blue'>$47,171</span></h5>
					 <div class='sparkline txt-color-blue hidden-mobile hidden-md hidden-sm'>
						 1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700, 3631, 2471
					 </div>
				 </li>
				 <li class='sparks-info'>
					 <h5> Enrolled <span class='txt-color-purple'><i class='fa fa-arrow-circle-up'></i>&nbsp;45%</span></h5>
					 <div class='sparkline txt-color-purple hidden-mobile hidden-md hidden-sm'>
						 110,150,300,130,400,240,220,310,220,300, 270, 210
					 </div>
				 </li>
				 <li class='sparks-info'>
					 <h5> Registered <span class='txt-color-greenDark'><i class='fa fa-shopping-cart'></i>&nbsp;2447</span></h5>
					 <div class='sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm'>
						 110,150,300,130,400,240,220,310,220,300, 270, 210
					 </div>
				 </li>
			 </ul>";
				
				}
				?>
			 									
		  </div>
		</div>
	 </div><!-- /.row -->	 
	 <div class="row">
		<section id="widget-grid" class="">		  
		  <?php
		  
			 switch($userinfo['idtype']){
								
				case 1: 
				      /*
				      echo '<div class="well no-padding">
                            <div class="bar-holder">
                            <div class="progress">
                            <div class="progress-bar bg-color-red" aria-valuetransitiongoal="25"></div>
                            </div>
                            </div>
                            </div>';
					 */		
				  
				  break;
				case 2: // Parent
				  break;
				case 3: // Alumni
				  break;
				case 4: // Faculty
				  break;
				case 5: // Administratives
						echo '<article class="col-sm-12">';
						$this->load->view('widgets/livefeed');
						echo '</article>';				
						echo '<article class="col-sm-12 col-md-12 col-lg-6">' . $this->load->view('widgets/events') . '</article>';
						echo '<article class="col-sm-12 col-md-12 col-lg-6">' . $this->load->view('widgets/sischat') . '</article>';				
				  break;
				default:
				  echo '<article class="col-sm-12 col-md-12 col-lg-6">';
						 echo $this->load->view('widgets/sischat');
						 echo '</article>';
				  break;
			 }		
		  ?>
		  <!--
		  <article class="col-sm-12 col-md-12 col-lg-6">
			<?php //$this->load->view('include/sischat');?>
			</article>
		  -->	
		  <article class="col-sm-12 col-md-12 col-lg-6">
		  	<?php //$this->load->view('include/events');?> 
		  </article>
		</section>		
	 </div>
	 <!-- /.row -->
  </div>
   