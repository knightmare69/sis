  <h1 class="txt-color-red login-header-big">PRISMS</h1>
  <div class="hero">
	  <div class="pull-left login-desc-box-l">
		  <h4 class="paragraph-header">It's a genius solutions. Experience the convenience of PRISMS Portal, everywhere you go!</h4>
		  <div class="login-app-icons">
			 <a href="<?php echo INST_SITE;?>" class="btn btn-danger btn-sm">Visit <?php echo INST_CODE;?> Website</a>
			 <a href="http://www.princetech.com.ph" class="btn btn-danger btn-sm">Visit Princetech Website</a>
		  </div>
	  </div>		   
	  <img src="<?php echo base_url('assets/img/demo/iphoneview.png').'?'.rand(100,200); ?>" class="pull-right display-image" alt="" style="width:210px">
  </div>
  <div class="row">
	  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		  <h5 class="about-heading">About PRISMS - Are you up to date?</h5>
		  <p>
			  PRISMS is your all in one system from Admission to Graduation and now it is invading the world wide web!
		  </p>
	  </div>
	  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		  <h5 class="about-heading">Why do you have to fall in line?</h5>
		  <p>
			  PRISMS Portal offers you a full convenient package for your teachers & students during enrollment period.
		  </p>
	  </div>
  </div>
  