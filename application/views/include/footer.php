</div>
		<div id="shortcut">
			<ul>
			   <!--
				<li>
					<a href="<?php //echo site_url('inbox'); ?>" class="jarvismetro-tile big-cubes selected bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
				</li>
				<li>
					<a href="<?php //echo site_url('calendar'); ?>" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
				</li>
				-->
				<li>
					<a href="<?php echo site_url('profile'); ?>" class="jarvismetro-tile big-cubes bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
				</li>
			</ul>
		</div>
		<!-- END SHORTCUT AREA -->
		<!--anti depressant-->
		<?php $this->load->view('templates/antidepressant');?>
        <?php $this->load->view('templates/selectmodal');?>
		<!-- end of anti depressant-->
      
<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)		
<script src="<?php //echo base_url('assets/js/plugin/pace/pace.min.js');  ?>"></script>
-->

<!-- For Local Used -->
<script src="<?php echo base_url('assets/js/libs/jquery-2.0.2.min.js') ; ?>"></script>
<script src="<?php echo base_url('assets/js/libs/jquery-ui-1.10.3.min.js') ; ?>"></script>


<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
<script src="<?php echo base_url('assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') ; ?>"></script> -->

<!-- BOOTSTRAP JS -->		
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url('assets/js/notification/SmartNotification.min.js') ?>"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo base_url('assets/js/smartwidgets/jarvis.widget.min.js') ?>"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url('assets/js/plugin/bootstrap-slider/bootstrap-slider.js') ?>"></script>

<!-- browser msie issue fix -->
<script src="<?php echo base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url('assets/js/plugin/fastclick/fastclick.js') ?>" charset='UTF-8'></script>

<!--[if IE 7]>
     
     <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
     
<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url('assets/js/app.js') ?>" charset='UTF-8'></script>

   <?php
      if (isset($jslink)){
        foreach($jslink as $script){        
		  $script = ((strpos($script,'?')>-1)?$script:($script.'?'.rand(0000,9999)));
          echo "<script type='text/javascript' src='" . base_url('assets/js/' . $script ) . "' charset='UTF-8'></script>";          
        }
      }
    ?>

 <script type="text/javascript">
  runAllForms();
  $(document).ready(function() {
	console.log('info',"Time until DOMready: ", Date.now()-timerStart);
  });
  $(window).load(function() {
	console.log('info',"Time until everything loaded: ", Date.now()-timerStart);
  });
</script>
</body>
</html>
