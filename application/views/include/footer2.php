<!--anti depressant-->
<?php $this->load->view('templates/antidepressant',array('hidden'=>true));?>
<!-- end of anti depressant-->
	
<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->		
<script src="<?php echo base_url('assets/js/plugin/pace/pace.min.js');  ?>"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- <script> if (!window.jQuery) { document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');} </script>
-->
<script> if (!window.jQuery) { document.write('<script src="<?php echo base_url('assets/js/libs/jquery-2.0.2.min.js') ; ?>"><\/script>');} </script>

<?php
//echo "<script> if (!window.jQuery) { document.write(""<script src='" . base_url('assets/libs/jquery-2.0.2.min.js') .  "'<\/script>""); } </script> " ;
?>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script> if (!window.jQuery.ui) { document.write('<script src="<?php echo base_url('assets/js/libs/jquery-ui-1.10.3.min.js') ; ?>"><\/script>');} </script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->		
<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url('assets/js/notification/SmartNotification.min.js') ?>"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo base_url('assets/js/smartwidgets/jarvis.widget.min.js') ?>"></script>
        
<!-- SPARKLINES -->
<script src="<?php echo base_url('assets/js/plugin/sparkline/jquery.sparkline.min.js') ?>"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url('assets/js/plugin/masked-input/jquery.maskedinput.min.js') ?>"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url('assets/js/plugin/select2/select2.min.js') ?>"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url('assets/js/plugin/bootstrap-slider/bootstrap-slider.js') ?>"></script>

<!-- browser msie issue fix -->
<script src="<?php echo base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url('assets/js/plugin/fastclick/fastclick.js') ?>"></script>

<!--[if IE 7]>
     
     <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
     
<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url('assets/js/app.js') ?>"></script>

   <?php
      if (isset($jslink)){
        foreach($jslink as $script){        
          echo "<script type='text/javascript' src='" . base_url('assets/js/' . $script ) . "'></script>";          
        }
      }
    ?>
    	
	<?php
      if (isset($jsvalues)){
	    echo "<script type='text/javascript'></script>";
		echo "$(document).ready(function() {";
        foreach($jsvalues as $jsval){echo $jsval;}
		echo "}</script>";
      }
    ?>
		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
				_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
				_gaq.push(['_trackPageview']);
			
			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>
      

</body>
</html>
