<!DOCTYPE html>
<html lang="en">
<head>
        <meta http-equiv="Content-type" value="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="expires" content="<?php echo date('D, d M Y');?> 00:00:00 GMT"/>
        <meta http-equiv="pragma" content="no-cache" />
		
		<title> PRISMS Online </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keywords" content="enrollment system, school information system, online enrollment, student portal">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/bootstrap.css').'?'.date('Ymd'); ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/font-awesome.css').'?'.date('Ymd'); ?>">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-production.css').'?'.date('Ymd'); ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-skins.css').'?'.date('Ymd'); ?>">	
		
		<?php if (isset($csslink)){foreach($csslink as $css){echo '<link rel="stylesheet" type="text/css" media="screen" href="'.base_url().'assets/css/'.$css.'?'.date('Ymd').'">'; } } ?>
		<!-- Custom CSS-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/prince.css').'?'.date('Ymd'); ?>">
		
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?> " type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
        
		<script type="text/javascript">
		 var base_url = '<?php echo base_url(); ?>'; 
		 var enable_remad = '<?php echo ((isset($enable_remad))?$enable_remad:1); ?>';
		 var promptwarn = '<?php echo ((isset($userinfo) && $userinfo['idtype']==1 && defined('PROMPT_WARN'))?PROMPT_WARN:''); ?>';
		 var timerStart = Date.now();
		</script>
		<!-- GOOGLE FONT
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
        -->

</head>
<body class="fixed-header smart-style-3 menu-on-top <?php echo ((isset($userinfo))?$userinfo['minify']:'minify'); ?>">
  <header id="header" class="pull-left">
       <div id="logo-group">            
            <span id="logo"> <img src="<?php echo base_url('assets/img/logo.png').'?'.rand(100,200); ?>" alt="PRISMS"> </span>           
            <span id="activity" class="activity-dropdown <?php if(isset($maintenance) && $maintenance==1){echo 'hidden'; }?>" data-id="<?php echo site_url('inbox/txn/get/flags'); ?>"> 
			 <i class="fa fa-user"></i> <b class="badge flagbadge hidden">0</b>
			</span>
            <div class="ajax-dropdown">
                 <div class="btn-group btn-group-justified" data-toggle="buttons">
                   <a href="javascript:void(0);" class="pull-right btnmarkall">Mark all as read</a>
                 </div>
                 <div class="ajax-notifications custom-scroll">
                      <div class="alert alert-transparent">
                           <h4>Click a button to show messages here</h4>
                           This blank page message helps protect your privacy, or you can show the first message here automatically.
                      </div>
                       <i class="fa fa-lock fa-4x fa-border"></i>
                 </div>
                 <span> 
				      Last updated on: <b class="lastupdate"><?php echo date('m/d/Y h:i A');?></b></a>
                      <button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right notifyrefresh">
                          <i class="fa fa-refresh"></i>
                      </button> 
				 </span>
            </div>
            <!-- END AJAX-DROPDOWN -->
       </div>
       <!-- projects dropdown
       <div id="project-context">

            <span class="label">Themes:</span>
            <span id="project-selector" class="popover-trigger-element dropdown-toggle" data-toggle="dropdown">SBCA Themes <i class="fa fa-angle-down"></i></span>
            <ul class="dropdown-menu">
                 <li>
                      <a href="javascript:void(0);">Bold & Beautiful</a>
                 </li>
                 <li>
                      <a href="javascript:void(0);">Dark Elegance</a>
                 </li>
                 <li>
                      <a href="javascript:void(0);">Ultra Light</a>
                 </li>
                 <li class="divider"></li>
                 <li>
                      <a href="javascript:void(0);"> Default</a>
                 </li>
                 
                 <li> </li>
                 
            </ul>
            <!-- end dropdown-menu

       </div>
       <!-- end projects dropdown -->

       <!-- pulled right: nav area -->
       <div class="pull-right">
         <?php
			if(isset($userinfo)){
		 ?>
            <!-- collapse menu button -->
            <div id="hide-menu" class="btn-header hidden-md hidden-lg pull-right">
                 <span> <a href="javascript:void(0);" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
            </div>
            <!-- end collapse menu -->

            <!-- logout button -->
            <div id="logout" class="btn-header transparent pull-right">
                 <span> <a href="<?php echo site_url('logout'); ?>" title="Sign Out"><i class="fa fa-sign-out"></i></a> </span>
            </div>
            <!-- end logout button -->

            <!-- lock button -->
            <div id="lock" class="btn-header transparent pull-right">
                 <span> <a href="<?php echo site_url('lock'); ?>" title="Lock"><i class="fa fa-lock"></i></a> </span>
            </div>
			<!-- end lock button -->
            
			<div id="fullscreen" class="btn-header transparent pull-right">
				<span> <a href="javascript:void(0);" onclick="launchFullscreen(document.documentElement);" title="Full Screen"><i class="fa fa-fullscreen" style="vertical-align:middle"></i></a> </span>
			</div>
         <?php 
			}
		 ?> 			
       </div>
       <!-- end pulled right: nav area -->

  </header>
  <!-- END HEADER -->
<!--<div id="main" role="main">	-->	

               
 