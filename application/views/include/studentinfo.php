	<?php
	if(isset($info_request) && $info_request==true)
	{
	 $xprogclass = ((isset($progclass))?$progclass:((isset($xdetail))?($xdetail->ProgClass):'30'));
	 if($xprogclass > 20)
	 {	
	?>
	  <div class="form-group">
	   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">College Name : </label></div>	
	   <div class="col-xs-8 col-md-10"><label><strong id="xcollege"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'CollegeName'))?$xdetail->CollegeName:'-');?></strong></label></div>
	   <br/>
	  </div>
	  <div class="form-group">
	   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Academic Program : </label></div>
	   <div class="col-xs-8 col-md-10"><label><strong id="xprogram"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'Program'))?$xdetail->Program:'-');?></strong></label></div>
	   <br/>
	  </div>
	  <div class="form-group">
	   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Program Curriculum : </label></div>
	   <div class="col-xs-8 col-md-10"><label><strong id="xcurriculum"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'Curriculum'))?$xdetail->Curriculum:'-');?></strong></label></div>
	  </div>
	<?php
	 }
	 else{
	?>	
	  <div class="form-group">
	   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Academic Program : </label></div>
	   <div class="col-xs-8 col-md-10"><label><strong id="xprogram"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'Program'))?$xdetail->Program:'-');?></strong></label></div>
	   <br/>
	  </div>
 	  <div class="form-group">
	   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Year Level : </label></div>
	   <div class="col-xs-8 col-md-10"><label><strong id="xyrlvl"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'YearLevel'))?$xdetail->YearLevel:'-');?></strong></label></div>
	   <br/>
	  </div>
	  <div class="form-group">
		<div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Class Section : </label></div>
		<div class="col-xs-8 col-md-10"><label><strong id="xsection"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'SectionName'))?$xdetail->SectionName:'-');?></strong></label></div>
	  </div>
	<?php
	 }
	}
    else if(isset($studentnolist) || isset($studentfilter))
	{ //Display for Administrator and Parent
	?>
	<div class="panel panel-default">
	 <div class="panel-body">
	  <div class="col-xs-12 col-sm-3 col-md-2 xprofile-pic text-align-center xmargin-rigth-5">
	   <img id="student-avatar" data-default="<?php echo base_url();?>assets/img/avatars/empty.png" src="<?php echo base_url().(($studentno<>'')?'home/pics/id pics/1/'.$studentno : 'assets/img/avatars/empty.png');?>"/>
	  </div>
	  <div class="col-xs-12 col-sm-9 col-md-10 no-padding">
	  <fieldset>
	   <div class="form-group">
		<div class="col-xs-4 col-sm-2"><label style="white-space:nowrap">Student Name : </label></div>
		<div class="col-xs-8 col-md-10">
		<?php if(isset($studentfilter)){?>
		 <div class="input-group">
		  <input class="form-control no-input" id="studentfilter" name="studentfilter" type="text" onclick="$('.btnstudsearch').click();"/><span class="input-group-addon btnstudsearch"><i class="fa fa-search"></i></span>
		  <input type='hidden' id='studentno' name='studentno' class="filteridno"/>     
		 </div>                
		<?php 
		 }
		 else if(isset($studentnolist)){ 
	     ?>
		 <select class='form-control filteridno' id='studentno' name='studentno'>                     
		   <?php
			 foreach ($studentnolist as $studentnolist)
			   echo "<option value='". $studentnolist->StudentNo ."' ".((isset($selstudentno) && $studentnolist->StudentNo==$selstudentno)? "selected":"").">". $studentnolist->StudentNameNo . "</option>";
			?>
		  </select>
		<?php
		 } 
		 else 
		  echo '<label><strong>'.$xdetail->StudentName.'</strong></label>';
	    ?>
		</div>
	   </div>
	   <div id="details" data-class="<?php echo ((isset($progclass))?$progclass:((isset($xdetail))?($xdetail->ProgClass):'30'));?>">
  	   	<?php
		 $xprogclass = ((isset($progclass))?$progclass:((isset($xdetail))?($xdetail->ProgClass):'30'));
		 if($xprogclass > 20)
		 {	
		?>
		  <div class="form-group">
		   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">College Name : </label></div>	
		   <div class="col-xs-8 col-md-10"><label><strong id="xcollege"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'CollegeName'))?$xdetail->CollegeName:'-');?></strong></label></div>
		   <br/>
		  </div>
		  <div class="form-group">
		   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Academic Program : </label></div>
		   <div class="col-xs-8 col-md-10"><label><strong id="xprogram"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'Program'))?$xdetail->Program:'-');?></strong></label></div>
		   <br/>
		  </div>
		  <div class="form-group">
		   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Program Curriculum : </label></div>
		   <div class="col-xs-8 col-md-10"><label><strong id="xcurriculum"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'Curriculum'))?$xdetail->Curriculum:'-');?></strong></label></div>
		  </div>
		<?php
		 }
		 else{
		?>	
		  <div class="form-group">
		   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Academic Program : </label></div>
		   <div class="col-xs-8 col-md-10"><label><strong id="xprogram"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'Program'))?$xdetail->Program:'-');?></strong></label></div>
		   <br/>
		  </div>
		  <div class="form-group">
		   <div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Year Level : </label></div>
		   <div class="col-xs-8 col-md-10"><label><strong id="xyrlvl"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'YearLevel'))?$xdetail->YearLevel:'-');?></strong></label></div>
		   <br/>
		  </div>
		  <div class="form-group">
			<div class="col-xs-4 col-md-2"><label style="white-space:nowrap">Class Section : </label></div>
			<div class="col-xs-8 col-md-10"><label><strong id="xsection"><?php echo ((isset($xdetail) && is_object($xdetail) && property_exists($xdetail,'SectionName'))?$xdetail->SectionName:'-');?></strong></label></div>
		  </div>
		<?php
		 }
		?>
	   </div>
	  </fieldset>
	  </div>
     </div>
    </div>	  
    <?php	
	}	
	?>
	