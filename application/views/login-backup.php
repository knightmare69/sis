
<!DOCTYPE html>
<html lang="en">
<head>
        <!--<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title> PRISMS Online : Log In </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="expires" content="<?php echo date('D, d M Y');?> 00:00:00 GMT"/>
        <meta http-equiv="pragma" content="no-cache" />

		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/font-awesome.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-production.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-skins.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/prince.css') ?>">	
		
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
        
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
        
		<script type="text/javascript"> 
		 var base_url = '<?php echo base_url(); ?>'; 
		 var enable_remad = '<?php echo ((isset($enable_remad))?$enable_remad:1); ?>';
		</script>
</head>
<body id="login" class="animated fadeInDown">
  <header id="header">
    <div id="logo-group"><span id="logo"> <img src="<?php echo base_url('assets/img/logo-login.png').'?'.rand(100,200); ?>" alt="PRISMS"> </span></div>
	<?php 
	if(defined('CREATE_APP') && CREATE_APP==1){
	   echo '<span id="login-header-space"> <span class="visible-md visible-lg" style="width:400px;">New Applicant? <a href="'.site_url('admission/welcome').'" class="btn btn-danger">Application Form</a></span> <span class="visible-xs visible-sm"><a href="'.site_url('admission/welcome').'" class="btn btn-danger">Admission</a></span></span>';
	}else{
	   echo '<span id="login-header-space"> <span class="visible-md visible-lg" style="width:400px;">Need an account? <a href="'.site_url('register').'" class="btn btn-danger">Create Account</a></span> <span class="visible-xs visible-sm"><a href="'.site_url('register').'" class="btn btn-danger">Register</a></span></span>';
	}
	?>
  </header>
  <div id="main" role="main">
    <div id="content" class="container">
	 <div class="row">
	   <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
		<?php $this->load->view('include/description');?>
	   </div>
	   <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
		<div class="well no-padding">
		  <?php echo form_open('login/verifylogin',array('id'=>'login-form','class' => 'smart-form client-form','onsubmit'=>'return false;','autocomplete'=>'off')); ?>
		    <header> Sign In </header>
		    <input type='hidden' name='submitted' id='submitted' value='1' style="visibility:hidden;"  />                
		    <fieldset>
			 <section>
			   <label class="label">Username or E-mail</label>
			   <label class="input"> <i class="icon-append fa fa-user"></i>
			   <input type="text" name="username">
			   <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
			 </section>
			 <section>
			   <label class="label">Password</label>
			   <label class="input"> <i class="icon-append fa fa-lock"></i>
				   <input type="password" name="password" value=''>
				   <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> 
			   </label>
			  </section>
			  <!--
			  <section>
				   <label class="label">Birthday</label>
				   <label class="input"> <i class="icon-append fa fa-calendar"></i>
				   <input type="text" name="bday" class="datepicker numberonly" data-dateformat="mm/dd/yy" placeholder="mm/dd/YY">
				   <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter your birthday</b></label>
			   </section>
			   -->
			   <section>
			    <label class="checkbox"><input type="checkbox" name="remember" checked=""><i></i>Stay signed in</label>
			   </section>
		    </fieldset>
		    <footer>
              <div class="alert alert-login alert-danger fade in <?php echo ((isset($error) && $error!='')?'':' hidden');?>">
               <strong><i class="fa fa-warning"></i> Error!</strong>
               <b class="alert-content"><?php echo ((@isset($error))?$error:''); ?></b>
              </div>
              <div class="note pull-left">
			    <a href="<?php echo site_url('forgot');?>">Forgot password?</a>
			  </div>
			  <button type="submit" class="btn btn-primary btnsubmit">Sign in</button>
          </footer>
		  </form>
		</div>		
	   </div>	   
	 </div>
    </div>
  </div>
  <!--anti depressant-->
	<?php $this->load->view('templates/antidepressant',array('hidden'=>true));?>
  <!-- end of anti depressant-->
  
  <!-- For Local Used -->
  <script src="<?php echo base_url('assets/js/libs/jquery-2.0.2.min.js') ; ?>"></script>
  <script src="<?php echo base_url('assets/js/libs/jquery-ui-1.10.3.min.js') ; ?>"></script>
  
  <!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
  <script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

  <!-- BOOTSTRAP JS -->		
  <script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>
   
  <!-- JQUERY VALIDATE -->
  <script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>
  
  <!-- JQUERY UI + Bootstrap Slider -->
  <script src="<?php echo base_url('assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js') ?>"></script>
  
  <!-- browser msie issue fix -->
  <script src="<?php echo base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>
  
  <!-- FastClick: For mobile devices -->
  <script src="<?php echo base_url('assets/js/plugin/fastclick/fastclick.js') ?>"></script>
  
  <!--[if IE 7]>    
     <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>    
  <![endif]-->

  <!-- MAIN APP JS FILE -->
  <script src="<?php echo base_url('assets/js/app.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/utilities/login.js'); ?>" type="text/javascript"></script>
  </body>
</html>

