

    <div class="container">

      <form id='login' action="" method="post" class="form-signin" role="form">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type='hidden' name='submitted' id='submitted' value='1' style="visibility:hidden;"  />                
        <input type="text" name='username' id='username' value="" class="form-control" placeholder="User Account" required autofocus>
        <input type="password" id="password" name='password' maxlength="50" class="form-control" placeholder="Password" required>
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit" id="Submit" value="Submit">Sign in</button>
        <div class="error"></div>
      </form>
    </div> <!-- /container -->

