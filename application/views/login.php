
<!DOCTYPE html>
<html lang="en">
<head>
        <!--<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title> PRISMS Online : Log In </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="expires" content="<?php echo date('D, d M Y');?> 00:00:00 GMT"/>
        <meta http-equiv="pragma" content="no-cache" />

		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/font-awesome.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-production.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-skins.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/prince.css') ?>">	
		
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
        
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
        
		<script type="text/javascript"> 
		 var base_url = '<?php echo base_url(); ?>'; 
		 var enable_remad = '<?php echo ((isset($enable_remad))?$enable_remad:1); ?>';
		</script>
</head>
<body id="login" class="animated fadeInDown" style="background-image:url(<?php echo base_url('assets/img/demo/s0.jpg').'?'.rand(100,200);?>) !important;background-size: 100% !important;">
  <header id="header" class="visible-xs visible-sm hidden-md hidden-lg">
	<?php 
	if(defined('CREATE_APP') && CREATE_APP==1){
	   echo '<span id="login-header-space" style="width:100px !important;"> <span class="visible-md visible-lg" style="width:400px;">New Applicant? <a href="'.site_url('admission/welcome').'" class="btn btn-danger">Application Form</a></span> <span class="visible-xs visible-sm"><a href="'.site_url('admission/welcome').'" class="btn btn-danger">Admission</a></span></span>';
	}else{
	   echo '<span id="login-header-space" style="width:100px !important;"> <span class="visible-md visible-lg" style="width:400px;">Need an account? <a href="'.site_url('register').'" class="btn btn-danger">Create Account</a></span> <span class="hidden"><a href="'.site_url('register').'" class="btn btn-danger">Register</a></span></span>';
	}
	?>
    <div id="logo-group pull-right"><span id="logo"> <img src="<?php echo base_url('assets/img/logo-login.png').'?'.rand(100,200); ?>" alt="PRISMS"> </span></div>
  </header>
  <header id="header" class="visible-md visible-lg hidden-xs hidden-sm text-center" style="height:100px !important;margin-bottom:0px !important;">
	  <span id="logo" style="width:520px !important;margin-top:10px;"><a href="<?php echo INST_SITE;?>"><img  class="" src="<?php echo base_url('assets/img/BEDISTA.png').'?'.rand(100,200); ?>" style="width:98% !important;" alt="PRISMS"></a> </span>
  </header>
  <div id="main" role="main" style="background-color:rgba(255,255,255,0.6) !important;padding-top:50px !important;height:85% !important;">
    <div id="content" class="container">
	 <div class="row">
	   <div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" style="margin-bottom:2px !important;">
		<div class="well no-padding">
		  <?php echo form_open('login/verifylogin',array('id'=>'login-form','class' => 'smart-form client-form','onsubmit'=>'return false;','autocomplete'=>'off')); ?>
		    <header class="text-center"><img  class="" src="<?php echo base_url('assets/img/BEDISTA_portal_logo.png').'?'.rand(100,200); ?>" style="width:80% !important;" alt="PRISMS"></header>
		    <input type='hidden' name='submitted' id='submitted' value='1' style="visibility:hidden;"  />                
		    <fieldset>
			 <section>
			   <label class="label">Username or E-mail</label>
			   <label class="input"> <i class="icon-append fa fa-user"></i>
			   <input type="text" name="username">
			   <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
			 </section>
			 <section>
			   <label class="label">Password</label>
			   <label class="input"> <i class="icon-append fa fa-lock"></i>
				   <input type="password" name="password" value=''>
				   <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> 
			   </label>
			  </section>
			  <!--
			  <section>
				   <label class="label">Birthday</label>
				   <label class="input"> <i class="icon-append fa fa-calendar"></i>
				   <input type="text" name="bday" class="datepicker numberonly" data-dateformat="mm/dd/yy" placeholder="mm/dd/YY">
				   <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter your birthday</b></label>
			   </section>
			   -->
			   <section>
			    <label class="checkbox hidden"><input type="checkbox" name="remember" checked=""><i></i>Stay signed in</label>
			    <label class=""><a href="<?php echo site_url('forgot');?>">Forgot password?</a></label><br/> 
			    <label class="">Having trouble logging in? <a href="javascript:void(0);" class="text-danger btncontact" onclick="$('#contactmodal').modal('show');">Contact Us</a></label>
			   </section>
		    </fieldset>
		    <footer>
              <div class="alert alert-login alert-danger fade in <?php echo ((isset($error) && $error!='')?'':' hidden');?>">
               <strong><i class="fa fa-warning"></i> Error!</strong>
               <b class="alert-content"><?php echo ((@isset($error))?$error:''); ?></b>
              </div>
              <div class="note pull-left">
				<p>
				<?php 
				if(defined('CREATE_APP') && CREATE_APP==1){
				 echo '<span class="visible-md visible-lg">New Applicant? <a href="'.site_url('admission/welcome').'" class="text-danger">Apply Here!</a></span>';
				}else{
				 echo '<span class=""><strong>Need an account?<br/><a href="'.site_url('register').'" class="text-danger" style="font-size:16px !important;">Create New User</a></strong></span>';
				}
				?>
				</p>
			  </div>
			  <button type="submit" class="btn btn-primary btnsubmit">Log In</button>
          </footer>
		  </form>
		</div>
	   </div>				
	   <div class="col-xs-12 col-sm-12 text-center" style="margin-bottom:2px !important;">
			 <a href="<?php echo INST_SITE;?>" class="btn btn-danger btn-sm">Visit <?php echo INST_CODE;?> Website</a>
			 <a href="http://www.princetech.com.ph" class="btn btn-danger btn-sm">Visit Princetech Website</a>
		  <br/>
       </div>
	   <div class="col-xs-12 col-sm-12 text-center">
	     <br/>
         <a href="javascript:void(0);" class="text-danger" onclick="$('#privacymodal').modal('show');"><b>Data Privacy Statement<b></a>
	   </div>	
	   </div>
	 </div>
  </div>
  <!--anti depressant-->
  <?php $this->load->view('templates/antidepressant',array('hidden'=>true));?>
  <div class="modal fade" id="privacymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:80% !important;">
	<div class="modal-content">
	 <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	  <h4 class="modal-title" id="lblmwindow"><strong class="lblmode">Data Privacy Policy</strong></h4>
	 </div>	               
	 <div class="modal-body xpadding-10">
	  <div class="row">
	   <div class="col-sm-12 no-padding">
	       <embed src="<?php echo base_url('assets/Data Privacy Policy.pdf');?>#zoom=125" width="98%" height="600px" type='application/pdf'>
	   </div>
	  </div>
     </div>
    </div>
  </div>
  </div>  

  <div class="modal fade" id="contactmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:500px !important;">
	<div class="modal-content">
	 <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	  <h4 class="modal-title" id="lblmwindow"><small>Having trouble loggin in your BEDISTA Portal?</small><br/><strong class="lblmode">Contact Us</strong></h4>
	 </div>	               
	 <div class="modal-body xpadding-10">
	  <div class="row">
	   <div class="col col-sm-12">
	   <form class="smart-form" id="frmcontact">
	     <fieldset>
			 <section>
			   <label class="label">Complete Name:</label>
			   <label class="input"> <i class="icon-prepend fa fa-user"></i>
			   <input name="txtname" id="txtname" class="valid" type="text">
			   </label>
			 </section>
			 <section>
			   <label class="label">Student Number:</label>
			   <label class="input"> <i class="icon-prepend fa fa-user"></i>
			   <input name="txtstdno" id="txtstdno" class="valid" type="text">
			   </label>
			 </section>
			 <section>
			   <label class="textarea">
			   <textarea name="txtmsg" id="txtmsg" maxlength="2500"></textarea>
			   </label>
			 </section>
		 </fieldset>	 
	   </form>
	   </div>
	  </div>
	  <div class="modal-footer">
	    <button class="btn btn-info btnsend"><i class="fa fa-send-o"></i> Send</button>
	  </div>
     </div>
    </div>
  </div>
  </div> 
  <!-- end of anti depressant-->
  
  <!-- For Local Used -->
  <script src="<?php echo base_url('assets/js/libs/jquery-2.0.2.min.js') ; ?>"></script>
  <script src="<?php echo base_url('assets/js/libs/jquery-ui-1.10.3.min.js') ; ?>"></script>
  
  <!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
  <script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

  <!-- BOOTSTRAP JS -->		
  <script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>
   
  <!-- JQUERY VALIDATE -->
  <script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>
  
  <!-- JQUERY UI + Bootstrap Slider -->
  <script src="<?php echo base_url('assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js') ?>"></script>
  
  <!-- browser msie issue fix -->
  <script src="<?php echo base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>
  
  <!-- FastClick: For mobile devices -->
  <script src="<?php echo base_url('assets/js/plugin/fastclick/fastclick.js') ?>"></script>
  
  <!--[if IE 7]>    
     <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>    
  <![endif]-->

  <!-- MAIN APP JS FILE -->
  <script src="<?php echo base_url('assets/js/app.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/utilities/login.js'); ?>" type="text/javascript"></script>
  </body>
</html>
