<h2 class="email-open-header">
	Compose
	<a href="javascript:void(0);" rel="tooltip" data-placement="left" data-original-title="Print" class="txt-color-darken pull-right hidden"><i class="fa fa-print"></i></a>	
</h2>
<form enctype="multipart/form-data" action="dummy.php" method="POST" class="form-horizontal" id="email-compose-form">
	<div class="inbox-info-bar no-padding">
		<div class="row">
			<div class="form-group">
				<label class="control-label col-md-1"><strong>To:</strong></label>
				<div class="col-md-11">
					<div class="xrecipient xrecipientto custom-scroll" id="xrecipientto" style="width:99%;max-height:40px;overflow:auto">
					 <ul class="xrecipient-choices">
					 </ul>
					</div>
					<em><a href="javascript:void(0);" class="show-next hidden" rel="tooltip" data-placement="bottom" data-original-title="Carbon Copy">CC</a></em>
				</div>
			</div>
		</div>	
	</div>
	<div class="inbox-info-bar no-padding hidden">
		<div class="row">
			<div class="form-group">
				<label class="control-label col-md-1"><strong>CC:</strong></label>
				<div class="col-md-11">
					<div class="xrecipient xrecipientcc custom-scroll" id="xrecipientcc" style="width:99%;max-height:40px;overflow:auto">
					 <ul class="xrecipient-choices">
					 </ul>
					</div>
					<em><a href="javascript:void(0);" class="show-next" rel="tooltip" data-placement="bottom" data-original-title="Blind Carbon Copy">BCC</a></em>
				</div>
			</div>
		</div>	
	</div>

	<div class="inbox-info-bar no-padding hidden">
		<div class="row">
			<div class="form-group">
				<label class="control-label col-md-1"><strong>BCC:</strong></label>
				<div class="col-md-11">
					<div class="xrecipient xrecipientbc custom-scroll" id="xrecipientbc" style="width:99%;max-height:40px;overflow:auto">
					 <ul class="xrecipient-choices">
					 </ul>
					</div>
				</div>
			</div>
		</div>	
	</div>
	
	<div class="inbox-info-bar no-padding">
		<div class="row">
			<div class="form-group">
				<label class="control-label col-md-1"><strong>Type:</strong></label>
				<div class="col-md-11">
					<select class="form-control xtype" style="border:none;width:99%;">
					 <option value="2">Message</option>
					 <option value="1">Notification</option>
					</select>
				</div>
			</div>
		</div>	
	</div>
	<div class="inbox-info-bar no-padding">
		<div class="row">
			<div class="form-group">
				<label class="control-label col-md-1"><strong>Subject:</strong></label>
				<div class="col-md-11">
					<input id="xsubj" class="form-control" type="text" style="border:none;width:99%;">
					<em><a href="javascript:void(0);" class="show-next hidden" rel="tooltip" data-placement="bottom" data-original-title="Attachments"><i class="fa fa-paperclip fa-lg"></i></a></em>
				</div>
			</div>
		</div>	
	</div>
	<div class="inbox-info-bar no-padding hidden">
		<div class="row">
			<div class="form-group">
				<label class="control-label col-md-1"><strong>Attachments</strong></label>
				<div class="col-md-11">
					<input class="form-control fileinput" type="file" multiple="multiple">
				</div>
			</div>
		</div>	
	</div>
	
	<div class="inbox-message no-padding">	
		<div id="emailbody">        
        </div>	
	</div>
	
	<div class="inbox-compose-footer">
	 <div class="row">
    <!--
	<button class="btn btn-danger" type="button">Disregard</button>
	<button class="btn btn-info" type="button">Draft</button>
     -->	 
	 <button class="btn btn-default pull-right btncancel" type="button">Cancel</button>
     <button data-loading-text="<i class='fa fa-refresh fa-spin'></i> &nbsp; Sending..." class="btn btn-primary exempt pull-right" type="button" id="btnsend">
		Send <i class="fa fa-arrow-circle-right fa-lg"></i>
	 </button>
	 </div>
	</div>

</form>

<script type="text/javascript">
	var xtargetlist = '';
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
    runAllForms();

	 // PAGE RELATED SCRIPTS
    $(".table-wrap [rel=tooltip]").tooltip();
    
	$('#emailbody').summernote({
        height: "300px",
        focus: false,
        tabsize: 2
    });


	$(".show-next").click(function () {
	    $this = $(this);
	    $this.hide();
	    $this.parent().parent().parent().parent().parent().next().removeClass("hidden");
	})

	$("#send").click(function () {

	});
	
	$('body').on('click','.btncancel',function(){$('.inbox-load').click(); });
	
	$('body').on('click','.xrecipient-choice-close',function(){
	  $(this).closest('li').remove();	
	});
	
	$('body').on('click','.xrecipient, .xrecipient-choices',function (e) {
	  if(e.target!==this){return; }
      xtargetlist = (($(this).is('.xrecipient-choices'))?$(this).closest('.xrecipient').attr('id'):$(this).attr('id'));
      $('#selectmodal').find('#data').html('Member');
	  $('#selectmodal').find('#data-detail').html('a member from the list.');
	  $('.select-list').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> No Data Available.</div>');
	  $('#selectmodal').modal('show');
	  $('#selectfilter').attr('search-type','user');
	  $('#selectfilter').val('');
	  setTimeout(function(){$('#selectfilter').focus();},1000);
	});
   
   $('#btnselectitem').click(function(){
	  var count = $('.select-list').find('.info').length;
	  var elem  = $('.select-list').find('.info');
	  if(count<=0 || count>1){return false; }
	  if(xtargetlist==undefined && xtargetlist==''){return false; }
	  
	  var xid   = elem.attr('id');
	  var uid   = elem.find('td:nth-child(2)').html();
	  var email = elem.find('td:nth-child(3)').html();
	  if($('#'+xtargetlist).find('[data-email="'+email+'"]').length<=0)
	  {	  
	   $('#'+xtargetlist).find('ul').append('<li class="xrecipient-choice" data-id="'+uid+'" data-email="'+email+'"><div>'+email+' </div><a href="javascript:void(0);" class="xrecipient-choice-close fa fa-times-circle"/></li>'); 
	  }
	  $('#selectmodal').modal('hide');			 
   });
   
   $('#btnsend').click(function(){
	 var btn     = $(this) 
	 var elemto  = $('#xrecipientto').find('li');
	 var elemcc  = $('#xrecipientcc').find('li');
	 var elembc  = $('#xrecipientbc').find('li');
	 var content = $('#emailbody').code();
     var subj    = $('#xsubj').val();	 
     var xtype   = $('.xtype').val();
     var xid     = [];	 
	 var xto     = '';
	 var xcc     = '';
	 var xbc     = '';
	 
	 for(i=0,len = elemto.length; i<len; i++)
	 {
	   var indx = xid.length;
	   xto       = ((xto=='' || xto==undefined)? $(elemto[i]).attr('data-email') : xto+','+$(elemto[i]).attr('data-email') );	 
	   xid[indx] = $(elemto[i]).attr('data-id');
	 }	 
	 
     btn.button('loading')
	 $.ajax({
	   type: "POST",         
	   url: base_url+'inbox/txn/set/send',
	   data: {xtype:xtype,xsubj:subj,xcontent:content,uid:xid,xto:xto},
	   dataType: "JSON",
	   async:true,
	   success: function (rs){
				 alertmsg('success','Success');
				 btn.button('reset')
				 $('.inbox-load').click();
			    },
       error: function(err){
			  alertmsg('danger','Error While Sending');
			  btn.button('reset')
		     }	   
	 });
	 
   });
   
   $('.btn-prev').attr('disabled',true);
   $('.btn-next').attr('disabled',true);
   $('.deletebutton').attr('disabled',true);
   console.clear();
</script>
