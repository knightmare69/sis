<div>
<?php
if(isset($content))
{
 $count = 0;
 foreach($content as $rs)
 {
  $typeid = is_key_exist($rs,'TypeID',0);
  $icon   = '';
  switch($typeid)
  {
	case 1:
     $icon  = '<i class="fa fa-bullhorn"></i> ';
     break;
    case 2:
     $icon  = '<i class="fa fa-envelope"></i> ';
  	 break;
  }
  echo (($count==0)?'<h2 class="email-open-header">'.$icon.is_key_exist($rs,'xSubject','').'</h2>':'');
  $count++;
  $uid    = ((isset($uid))?$uid:'');  	
  $class  = (($count<count($content))?'hidden':'');
  $sender = (($uid==is_key_exist($rs,'xFrom',''))?'me':is_key_exist($rs,'xFrom','').' <b class="hidden-xs hidden-sm">&lt;'.is_key_exist($rs,'Email','').'&gt;</b>');
  $recip  = (($uid==is_key_exist($rs,'xRecipient',''))?'me':is_key_exist($rs,'xRecipient',''));
  
  echo '<div class="inbox-info-bar" id="msg'.is_key_exist($rs,'EntryID','').'">
		<div class="row">
		 <div class="col-sm-10">
		   <img src="'.base_url().'assets/img/avatars/empty.png" alt="me" class="away"/>
		   <strong>'.$sender.'</strong>
		   <span class="hidden-xs hidden-sm"> to <strong>'.$recip.'</strong></span>
		   <i class="pull-right">'.datewformat(is_key_exist($rs,'DateCreated',false),'m/d/Y h:i A').'</i>
		 </div>   
		 <div class="col-sm-2 text-right btncontent '.$class.'" '.((is_key_exist($rs,'TypeID',0)==1)?'style="display:none;"':'').'>
		  <button class="btn btn-primary btnreply" data-id="'.is_key_exist($rs,'xFrom','').'" data-email="'.is_key_exist($rs,'Email','').'">Reply</button>
		 </div>
		</div>
		</div>
		<div class="inbox-message '.$class.'" id="msg'.is_key_exist($rs,'EntryID','').'">'.is_key_exist($rs,'xContent','<p>- Message End Here -</p>').'</div>';
 }
}
else
 echo '<i class="fa fa-warning"></i> Failed to loading content.'; 
?>
</div>
<script type="text/javascript">
	$('.btnreply').click(function(){
	   var uid   = $(this).attr('data-id'); 
	   var email = $(this).attr('data-email'); 
	   var xsubj = $('.email-open-header').text();
	   setTimeout(function(){
	     $('#compose-mail').click();
	     $('#xrecipientto').find('ul').append('<li class="xrecipient-choice" data-id="'+uid+'" data-email="'+email+'"><div>'+email+' </div><a href="javascript:void(0);" class="xrecipient-choice-close fa fa-times-circle"/></li>'); 
	     $('#xsubj').val('RE:'+xsubj);
	     $('.note-editable').focus();
	   },1000);
	});
   
   $('.btn-prev').attr('disabled',true);
   $('.btn-next').attr('disabled',true);
   $('.deletebutton').attr('disabled',true);
</script>