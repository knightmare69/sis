<ul class="notification-body" style="cursor: pointer;" >
	<?php
	    if(isset($notes))
		{
			foreach($notes as $n)
			{
				$timeframe = "seconds ago";
				$seconds   = is_key_exist($n,'Age',1);
				$age = $seconds;
				if ($age < 60) 
				 $timeframe = "seconds ago";
				elseif ($age >= 60 && $age <3600 ) 
				{
				 $age = round($seconds / 60); //Mins
				 $timeframe = "mins ago";
				}
				elseif ($age >= 3600 && $age < 86400) 
				{
				 $age = round($seconds / 60/ 60); //Hrs
				 $timeframe = "hrs ago";
				}
				else
				{
				 $age = datewformat(is_key_exist($n,'DateCreated',''),'m/d/Y');
				 $timeframe = '';
				}
				
				$age = ((datewformat($n->DateCreated,'m/d/Y')!=date('m/d/Y'))? datewformat($n->DateCreated,'m/d/Y') : $age);
				$timeframe = ((datewformat($n->DateCreated,'m/d/Y')!=date('m/d/Y'))? '' : $timeframe);
				
				echo '<li data-id="'.$n->EntryID.'" data-stats="'.$n->StatusID.'" data-link="'.base_url().'inbox?page=content&id='.$n->EntryID.'">';
				echo '<span class="padding-10 '. ($n->StatusID == 0 ? 'unread':'') .'">';
				switch($n->TypeID)
				{
				 case 1:	
				  echo '<em class="badge padding-5 no-border-radius bg-color-yellow pull-left margin-right-5">';
				  echo '<i class="fa fa-bullhorn fa-fw fa-2x"></i>';
				  echo '</em>';
				  break;
				 case 2:	
				  echo '<em class="badge padding-5 no-border-radius bg-color-blueLight pull-left margin-right-5">';
				  echo '<i class="fa fa-envelope fa-fw fa-2x"></i>';
				  echo '</em>';
				  break; 
				}
				echo '<span><strong>'.takestrpart($n->xSubject,20,' ...').'</strong><br>'. '<span class="pull-right font-xs text-muted"><i>'. $age . ' ' . $timeframe . '</i></span></span>'  ;
				echo takestrpart(strip_tags(is_key_exist($n,'xContent','')),30,' ...');
				echo '</span>';
				echo '</li>';
			}
		} 
		else 
		{ 
	      echo '<li>
		         <span class="bg-color-yellow text-align-center">
				   <strong>No Notification Found</strong>
                 </span>
			   </li>'; 
	    }
	?>	
</ul>