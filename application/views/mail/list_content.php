<table id="inbox-table" class="table table-striped table-hover">
	<tbody>
        <?php
		$lastid    = 0;
		$firstid   = 0;
		$totalitem = 0;
		
		if(isset($count) && (count($count)>0))
		{
		 foreach($count as $c)
         {
		  $lastid    = is_key_exist($c,'LastID',0);
		  $firstid   = is_key_exist($c,'FirstID',0);
		  $totalitem = is_key_exist($c,'TotalID',0);
         }			 
		}
		
		if(isset($list) && (count($list)>0))
		{
		 foreach($list as $m)
         {
		   $uid     = ((isset($uid))?$uid:'');  	
		   $direct  = ((isset($direct))?$direct:'from'); 
		   $fromlbl = '';
		   $desc    = strip_tags($m->xContent);
		   	   
           switch($direct)
           {
			 case 'from':
			  $fromlbl = (($uid==$m->xFrom)?'me':$m->xFrom);
			  break;
			 case 'to':	
              $fromlbl = 'To:'.((($uid==$m->xRecipient)?'me':($m->xRecipient)));		  
			  break;
			 default:
			  if($uid==$m->xFrom)
			   $fromlbl = 'To:'.((($uid==$m->xRecipient)?'me':($m->xRecipient)));		  
              else		   
			   $fromlbl = (($uid==$m->xFrom)?'me':$m->xFrom);
			  
			  break;
		   }
		   
		   echo '<tr id="msg'.$m->EntryID.'" class="unread" data-last="'.(($m->EntryID==$lastid)?'true':'false').'" data-first="'.(($m->EntryID==$firstid)?'true':'false').'">
					<td class="inbox-table-icon">
						<div class="checkbox">
						 <label><input type="checkbox" class="chkmsg checkbox style-2"><span></span> </label>
						</div>
					</td>
					<td class="inbox-data-from hidden-xs hidden-sm">
						<div>'.$fromlbl.'</div>
					</td>
					<td class="inbox-data-message">
						<div>
						 <span>'.$m->xSubject.'</span>'.takestrpart($desc,40,' ...').'
						</div>
					</td>
					<td class="inbox-data-attachment hidden-xs">
						<div class="hidden">
							<a href="javascript:void(0);" rel="tooltip" data-placement="left" data-original-title="FILES: rocketlaunch.jpg, timelogs.xsl" class="txt-color-darken"><i class="fa fa-paperclip fa-lg"></i></a>
						</div>
					</td>
					<td class="inbox-data-date hidden-xs">
						<div>'.datewformat($m->DateCreated,((datewformat($m->DateCreated,'m/d/Y')!=date('m/d/Y'))? 'm/d/Y': 'h:i A')).'</div>
					</td>
				</tr>';
		 }		 
		}
        else
           echo '<tr data-last="true" data-first="true"><td class="text-align-center"><i class="fa fa-warning"></i> Nothing to load.</td></td>';			
		?>
		
	</tbody>
</table>

<script>
	//Gets tooltips activated
	$("#inbox-table [rel=tooltip]").tooltip();

	$("#inbox-table input[type='checkbox']").change(function() {
		$(this).closest('tr').toggleClass("highlight", this.checked);
	});

	

	$('.inbox-table-icon input:checkbox').click(function() {
		enableDeleteButton();
	});

	$(".deletebutton").click(function() {
		$('#inbox-table td input:checkbox:checked').parents("tr").rowslide();
	});

	function enableDeleteButton() {
		var isChecked = $('.inbox-table-icon input:checkbox').is(':checked');

		if (isChecked) {
			$(".inbox-checkbox-triggered").addClass('visible');
			//$("#compose-mail").hide();
		} else {
			$(".inbox-checkbox-triggered").removeClass('visible');
			//$("#compose-mail").show();
		}
	}
	
	$(".inbox-data-message,.inbox-data-from").click(function() {
		var item = $(this).closest('tr').attr('id');
		if(item!='' && item!=undefined)
		{	
		 item = item.replace('msg','');
		 getMail(item);
		} 
	});
	
	function getMail(xitem) {
	 if(xitem!='' && xitem!=undefined)
     {
	   loadURL(base_url+'inbox/txn/get/content/'+xitem, $('#inbox-content > .table-wrap'));	
	 }
	}
	
	if($('[data-first="true"]').length>0)
	 $('.btn-prev').attr('disabled',true);
    else 
	 $('.btn-prev').removeAttr('disabled');
 
	if($('[data-last="true"]').length>0)
	 $('.btn-next').attr('disabled',true);
    else 
	 $('.btn-next').removeAttr('disabled');
    
	if($('[id^="msg"]').length>0)
	 $('.deletebutton').removeAttr('disabled');	
    else
	 $('.deletebutton').attr('disabled',true);	
    	
	console.clear();
</script>
