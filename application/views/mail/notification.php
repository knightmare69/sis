<table id="inbox-table" class="table table-striped table-hover">
	<tbody>
        <?php
		if(isset($notify))
		{
		 $count =1;
		 foreach($notify as $n)
         {
		   echo '<tr id="notify'.$n->EntryID.'" class="unread">
					<td class="inbox-table-icon">
						<div class="checkbox">
						 <label><input type="checkbox" class="checkbox style-2"><span></span> </label>
						</div>
					</td>
					<td class="inbox-data-from hidden-xs hidden-sm">
						<div>'.$n->xFrom.'</div>
					</td>
					<td class="inbox-data-message">
						<div>
							<span>'.$n->xSubject.'</span>
						</div>
					</td>
					<td class="inbox-data-attachment hidden-xs">
						<div class="hidden">
							<a href="javascript:void(0);" rel="tooltip" data-placement="left" data-original-title="FILES: rocketlaunch.jpg, timelogs.xsl" class="txt-color-darken"><i class="fa fa-paperclip fa-lg"></i></a>
						</div>
					</td>
					<td class="inbox-data-date hidden-xs">
						<div>'.datewformat($n->DateCreated,(($n->Age>86400)?'m/d/Y': 'h:i A')).'</div>
					</td>
				</tr>';
		 }		 
		}
        else
           echo 'ay wala';			
		?>
		
	</tbody>
</table>

<script>
	//Gets tooltips activated
	$("#inbox-table [rel=tooltip]").tooltip();

	$("#inbox-table input[type='checkbox']").change(function() {
		$(this).closest('tr').toggleClass("highlight", this.checked);
	});

	
	$('.inbox-table-icon input:checkbox').click(function() {
		enableDeleteButton();
	});

	$(".deletebutton").click(function() {
		$('#inbox-table td input:checkbox:checked').parents("tr").rowslide();
	});

	function enableDeleteButton() {
		var isChecked = $('.inbox-table-icon input:checkbox').is(':checked');

		if (isChecked) {
			$(".inbox-checkbox-triggered").addClass('visible');
			//$("#compose-mail").hide();
		} else {
			$(".inbox-checkbox-triggered").removeClass('visible');
			//$("#compose-mail").show();
		}
	}
	
	$(".inbox-data-message,.inbox-data-from").click(function() {
		var item = $(this).closest('tr').attr('id');
		if(item!='' && item!=undefined)
		{	
		 item = item.replace('notify','');
		 getMail(item);
		} 
	});
	
	function getMail(xitem) {
	 if(xitem!='' && xitem!=undefined)
     {
	   loadURL(base_url+'inbox/txn/get/content/'+xitem, $('#inbox-content > .table-wrap'));	
	 }
	}
</script>
