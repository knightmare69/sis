
	
<ul class="notification-body" style="cursor: pointer;" >
	
	<?php
	    if($notes)
		{
			foreach($notes as $n){
				
				$timeframe = "seconds";
				$seconds = $n->Age;
				$age = $seconds;
				if ($age < 60) {					
					$timeframe = "seconds";
				}
				elseif ($age >= 60 && $age <3600 ) {
					$age = round($seconds / 60) ; //Mins
					$timeframe = "mins";
				}
				elseif ($age >= 3600 && $age <86400 ) {
					$age = round($seconds / 60/ 60)  ; //Mins
					$timeframe = "mins";
				}
				
				
				
				echo '<li><span class="padding-10 '. ($n->Unread == 1 ? 'unread':'') .'">';
				echo '<em class="badge padding-5 no-border-radius bg-color-green pull-left margin-right-5">';
				echo '<i class="fa fa-check fa-fw fa-2x"></i>';
				echo '</em>';
				echo '<span>' . $n->Message . '<br>'. '<span class="pull-right font-xs text-muted"><i>'. $age . ' ' . $timeframe . ' ago </i></span>'  ;
				echo '</span>';
				echo '</span>';
				echo '</li>';
				
			}
		} 
		else 
		{ 
	      echo '<li>
		         <span class="bg-color-yellow text-align-center">
				   <strong>No Notification Found</strong>
                 </span>
			   </li>'; 
	    }
	?>	
</ul>