<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>User Privileges</li>
	</ol>
</div>   
<div id="content">	 
 <article class="col-sm-3 no-padding">
  <div class="jarviswidget jarviswidget-color-greenLight xmargin-right-5" id="wid-id-0" data-widget-editbutton="false" role="widget">
   <header role="heading">
    <span class="widget-icon"><i class="fa fa-lg fa-user"></i></span><h2>User List</h2>
	<div class="widget-toolbar" role="menu">
	</div>
   </header>
   <div role="content">
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body">
	 <table class="table table-condense table-treeview">
	  <tbody>
	   <tr class="table-parent"><td><i class="fa fa-plus"></i> Users</td></tr>
	   <tr class="table-child hidden">
	    <td class="td-users no-padding">
		 <table class="table table-condense">
		  <tbody class="custom-user-list">
		   <tr><td class="adduser default"><i class="fa fa-plus txt-color-green"></i> Add User</td></tr>
		   <?php
		    if(isset($custom_user) && is_array($custom_user))
			{
			 foreach($custom_user as $k => $v)
			 {
			  if(trim($v[0])!='')
			   echo '<tr><td class="uusers" data-uid="'.trim($v[0]).'" data-idno="'.trim($v[1]).'" data-idtype="'.trim($v[2]).'"><i class="fa fa-user"></i> '.trim($v[0]).'</td></tr>';	
			 }
			}
            else
			 echo '<tr><td class="nousers default"><i class="fa fa-warning txt-color-yellow"></i> No User With Custom Privileges</td></tr>';	
		   ?>
		  </tbody>
		 </table>
		</td>
	   </tr>	
	   <tr class="table-parent active"><td><i class="fa fa-minus"></i> User Type</td></tr>
	   <tr class="table-child">
	    <td class="td-types no-padding">
		 <table class="table table-condense">
		  <tbody>
		   <tr><td class="udefault" data-idtype="-1"><i class="fa fa-group"></i> Administrator</td></tr>
		   <tr><td class="udefault" data-idtype="1"><i class="fa fa-group"></i> Student</td></tr>
		   <tr><td class="udefault" data-idtype="2"><i class="fa fa-group"></i> Parent</td></tr>
		   <tr><td class="udefault" data-idtype="3"><i class="fa fa-group"></i> Faculty</td></tr>
		  </tbody>
		 </table>
		</td>
	   </tr>
	  </tbody>
	 </table>
    </div>
   </div>
  </div> 
 </article>
 <article class="col-sm-9 no-padding">
  <div class="jarviswidget jarviswidget-color-orange" id="wid-id-0" data-widget-editbutton="false" role="widget">
   <header role="heading">
    <span class="widget-icon"><i class="fa fa-lg fa-user"></i></span><h2>User Privileges <i class="txtuser"></i></h2>
	<div class="widget-toolbar" role="menu">
	 <button class="btn btn-primary btnreset"><i class="fa fa-refresh"></i></button>
	 <button class="btn btn-success btnsave"><i class="fa fa-save"></i> Save</button>
	</div>
   </header>
   <div role="content">
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body">
	 <?php echo $this->load->view('privileges/vw_privileges','',true);?>
    </div>
   </div>
  </div> 
 </article>
</div>