		<form class="smart-form">
		<header style="margin-left:2px;">Personal Information</header>
		<div class="row" style="padding-bottom: 5px;">
			<div class="col-sm-6 hidden">
			    <i id='xenable' class="hidden"><?php if(isset($allow)){ echo $allow;} else{ echo 0;}?></i>
				<?php
				$link = ((isset($allow) and $allow==1)?"form-x-editable.html#":"javascript:void(0);");
				?>
			</div>
		</div>
		
		<table id="user" class="table table-condensed table-striped" style="clear: both; cursor: pointer;">
			<tbody>
				<tr>
					<td style="width:150px;">Last Name</td>
					<td ><a href="<?php echo $link;?>" id="lastname" name="lastname" data-type="text" data-pk="1" data-original-title="Enter Last Name">
					<?php echo $userinfo['lastname']; ?></a></td>
				</tr>
				<tr>
					<td>First Name</td>
					<td><a href="<?php echo $link;?>" id="firstname" name="firstname" data-type="text" data-pk="1" data-original-title="Enter First Name"><?php echo $userinfo['firstname']; ?></a></td>
				</tr>
				<tr>
					<td>Middle Name</td>
					<td><a href="<?php echo $link;?>" id="middlename" name="middlename" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-original-title="Enter your Middle Name"><?php echo $userinfo['middlename']; ?></a></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td><a href="<?php echo $link;?>" id="sex" data-type="select" data-pk="1" data-value="<?php echo $userinfo['gender']; ?>" data-original-title="Select sex"><?php echo $userinfo['gender']; ?></a></td>
				</tr>
				<tr>
					<td>Date of Birth</td>
					<td><a href="<?php echo $link;?>" id="dob" data-type="combodate" data-value="<?php echo $userinfo['birthdate']; ?>" data-format="MM-DD-YYYY" data-viewformat="MM/DD/YYYY" data-template="MMM / D / YYYY" data-pk="1" data-original-title="Select Date of birth"><?php echo $userinfo['birthdate']; ?></a></td>
				</tr>
				<tr>
					<td>Place of Birth</td>
					<td><a href="<?php echo $link;?>" id="birthplace" data-type="text" data-pk="1" data-placement="right" data-original-title="Start typing State.."><?php echo utf8_encode($userinfo['birthplace']); ?></a></td>
				</tr>
				
				<tr>
					<td>Civil Status</td>
					<td><a href="<?php echo $link;?>" id="csgroup" name="status" data-type="select" data-pk="1" data-value="<?php echo $userinfo['cstatusid']; ?>" data-source="/csgroups" data-original-title="Select Status"><?php echo $userinfo['cstatus']; ?></a></td>
				</tr>		
				<tr>
					<td>Nationality</td>
					<td><a href="<?php echo $link;?>" id="natgroup" name="nationality" data-type="select" data-pk="1" data-value="<?php echo $userinfo['natid']; ?>" data-source="/natgroups" data-original-title="Select Nationality"><?php echo $userinfo['nation']; ?></a></td>
				</tr>									
				<tr>
					<td>Religion</td>
					<td><a href="<?php echo $link;?>" id="relgroup" name = "religion" data-type="select" data-pk="1" data-value="<?php echo $userinfo['relid']; ?>" data-source="/relgroups" data-original-title="Select group"><?php echo $userinfo['religion']; ?></a></td>
				</tr>
				<tr>
					<td>Mobile No.</td>
					<td><a href="<?php echo $link;?>" id="mobileno" name="mobileno" data-type="text" data-pk="1" data-placement="right" data-placeholder="Mobile no" data-original-title="Enter your Mobile number"><?php echo $userinfo['mobile']; ?></a></td>
				</tr>
				<tr>
					<td>Telephone No.</td>
					<td><a href="<?php echo $link;?>" id="telno" name="telno" data-type="text" data-pk="1" data-placement="right" data-placeholder="" data-original-title="Enter your Telephone number"><?php echo $userinfo['telno']; ?></a></td>
				</tr>
				<?php
				if($userinfo['idtype']!=3 && $userinfo['idtype']!=-1)
				{
				?>
				<tr>
					<td>Residential Address</td>
					<td>
					<a href="<?php echo $link;?>" id="resaddress" data-type="address" data-pk="1" data-original-title="Please, fill address" data-value="{city:'<?php  echo utf8_encode($userinfo['res_city']);?>', prov:'<?php echo utf8_encode( $userinfo['res_prov']);?>', brgy:'<?php echo $userinfo['res_brgy'];?>', address:'<?php echo $userinfo['res_addr'];?>',zip:'<?php echo $userinfo['res_zip'];?>'}">
					<?php
					if($userinfo['res_addr'].$userinfo['res_strt'].$userinfo['res_brgy'].$userinfo['res_city'].$userinfo['res_prov']!='')											
                    {echo utf8_encode($userinfo['res_addr'].' '.$userinfo['res_strt'].' '.$userinfo['res_brgy'].' '.$userinfo['res_city'].', '.$userinfo['res_prov']);} 
					?>
					</a>
					</td>
				</tr>
                <?php
				}
				?>															
				<tr>
					<td>Permanent Address</td>
					<td>
					<a href="<?php echo $link;?>" id="permaddress" data-type="address" data-pk="1" data-original-title="Please, fill address" data-value="{city:'<?php echo $userinfo['perm_city'];?>', prov:'<?php echo $userinfo['perm_prov'];?>', brgy:'<?php echo $userinfo['perm_brgy'];?>', address:'<?php echo $userinfo['perm_addr'];?>',zip:'<?php echo $userinfo['perm_zip'];?>'}">
					<?php
					if($userinfo['perm_addr'].$userinfo['perm_strt'].$userinfo['perm_brgy'].$userinfo['perm_city'].$userinfo['perm_prov']!='')											
                    {echo utf8_encode($userinfo['perm_addr'].' '.$userinfo['perm_strt'].' '.$userinfo['perm_brgy'].' '.$userinfo['perm_city'].', '.$userinfo['perm_prov']);} 
					?>
					</a></td>
				</tr>
			</tbody>
		</table>
		</form>

