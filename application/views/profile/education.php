<div id="educationalbg" class="tab-pane fade in">
	<div class="row">
        <?php 
	     if(isset($allow) && $allow==1)
	     {
		 ?>
		 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xmargin-bottom-10">
		   <button class="btn btn-sm btn-success pull-right" type="button" id="xsaveall_educ" onclick="savealleduc();"><i class="fa fa-save"></i> Save All</button>
		 </div>
		 <?php
		 }
		 ?>
		 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			<div class="well">
				<form id="xform_elem" class="smart-form" onsubmit="return false;">
					<?php 
	                if(isset($allow) && $allow==1)
	                 echo '<button class="btn btn-sm btn-primary pull-right" type="button" id="xsave_be" onclick="saveeleminfo();"><i class="fa fa-save"></i> Save</button>';
					?>
					<h3 class="princeh1">Basic Education</h3>
					
					<fieldset>
						<section>
							<label class="label">School Name</label>
							<label class="input">
								<input name="elem_school" class="input-xs" type="text" value="<?php echo (isset($Elem_School))?$Elem_School:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Address</label>
							<label class="input">
								<input name="elem_address" class="input-xs" type="text" value="<?php echo (isset($Elem_Addr))?$Elem_Addr:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Inclusive Dates</label>
							<label class="input">
								<input name="elem_incldates" class="input-xs" type="text" value="<?php echo (isset($Elem_InclDates))?$Elem_InclDates:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Awards/Honors</label>
							<label class="input">
								<input name="elem_awardhonor" class="input-xs" type="text" value="<?php echo (isset($Elem_AwardHonor))?$Elem_AwardHonor:'';?>">	
							</label>
						</section>												
					</fieldset>
				</form>									
			</div>					
		</div><!-- elem End -->												
		<!-- Highschool -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			<div class="well">
				<form id="xform_hs" class="smart-form" onsubmit="return false;">
					<?php 
	                if(isset($allow) && $allow==1)
	                 echo '<button class="btn btn-sm btn-primary pull-right" type="button" id="xsave_se" onclick="savehsinfo();"><i class="fa fa-save"></i> Save</button>';
					?> 
					<h3 class="princeh1">Secondary Education</h3>
					<fieldset>
						<section>
							<label class="label">School Name</label>
							<label class="input">
								<input name="hs_school" class="input-xs" type="text" value="<?php echo (isset($HS_School))?$HS_School:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Address</label>
							<label class="input">
								<input name="hs_address" class="input-xs" type="text" value="<?php echo (isset($HS_Addr))?$HS_Addr:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Inclusive Dates</label>
							<label class="input">
								<input name="hs_incldates" class="input-xs" type="text" value="<?php echo (isset($HS_InclDates))?$HS_InclDates:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Awards/Honors</label>
							<label class="input">
								<input name="hs_awardhonor" class="input-xs" type="text" value="<?php echo (isset($HS_AwardHonor))?$HS_AwardHonor:'';?>">	
							</label>
						</section>												
					</fieldset>
				</form>									
			</div>					
		</div><!-- hs End -->
		<!-- College -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			<div class="well">
				<form id="xform_college" class="smart-form" onsubmit="return false;">
					<?php 
	                if(isset($allow) && $allow==1)
	                 echo '<button class="btn btn-sm btn-primary pull-right" type="button" id="xsave_col" onclick="savecollegeinfo();"><i class="fa fa-save"></i> Save</button>';
					?> 
					<h3 class="princeh1">Baccalaureate Degree/College</h3>
					
					<fieldset>
						<section>
							<label class="label">School Name</label>
							<label class="input">
								<input name="college_school" class="input-xs" type="text" value="<?php echo (isset($College_School))?$College_School:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Address</label>
							<label class="input">
								<input name="college_address" class="input-xs" type="text" value="<?php echo (isset($College_Addr))?$College_Addr:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Degree/Course</label>
							<label class="input">
								<input name="college_degree" class="input-xs" type="text" value="<?php echo (isset($College_Degree))?$College_Degree:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Inclusive Dates</label>
							<label class="input">
								<input name="college_incldates" class="input-xs" type="text" value="<?php echo (isset($College_InclDates))?$College_InclDates:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Awards/Honors</label>
							<label class="input">
								<input name="college_awardhonor" class="input-xs" type="text" value="<?php echo (isset($College_AwardHonor))?$College_AwardHonor:'';?>">	
							</label>
						</section>												
					</fieldset>
				</form>									
			</div>					
		</div><!-- college End -->												
		<!-- Vocational Trades -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="well">
				<form id="xform_vocational" class="smart-form" onsubmit="return false;">
					<?php 
	                if(isset($allow) && $allow==1)
	                 echo '<button class="btn btn-sm btn-primary pull-right" type="button" id="xsave_voc" onclick="savevocationalinfo();"><i class="fa fa-save"></i> Save</button>';
					?> 
					<h3 class="princeh1">Vocational/Trade Courses</h3>
					<fieldset>
						<section>
							<label class="label">School Name</label>
							<label class="input">
								<input name="vocational" class="input-xs" type="text" value="<?php echo (isset($Vocational_School))?$Vocational_School:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Address</label>
							<label class="input">
								<input name="vocational_address" class="input-xs" type="text" value="<?php echo (isset($Vocational_Addr))?$Vocational_Addr:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Degree/Course</label>
							<label class="input">
								<input name="vocational_degree" class="input-xs" type="text" value="<?php echo (isset($Vocational_Degree))?$Vocational_Degree:'';?>">	
							</label>
						</section>
						<section>
							<label class="label">Inclusive Dates</label>
							<label class="input">
								<input name="vocational_incldates" class="input-xs" type="text" value="<?php echo (isset($Vocational_InclDates))?$Vocational_InclDates:'';?>">	
							</label>
						</section>									
					</fieldset>
				</form>									
			</div>					
		</div><!-- Vocational End -->						
										
	</div>							
</div>