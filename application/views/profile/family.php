<?php 
$link = ((isset($allow) and $allow==1)?"form-x-editable.html#":"javascript:void(0);");
if($userinfo['idtype']==3 || $userinfo['idtype']==-1)
{
?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-bottom: 20px;">
<form id="xform_parent" class="smart-form" onsubmit="return false;">	
<header style="margin-left:2px;">Family Information</header>
<br/>
<table class="table table-condensed table-striped" style="clear: both; cursor: pointer;">
 <tbody>
   <tr>
	<td style="width:150px;">Father's Name.</td>
	<td><a class="editable editable-click" href="<?php echo $link;?>" id="fathern" name="fathern" data-type="fname" data-pk="1" data-placement="right" data-placeholder="" data-original-title="This is a trial input"><?php echo $userinfo['lastname']; ?></a></td>
   </tr>
   <tr>
	<td>Mother's Name.</td>
	<td><a href="<?php echo $link;?>" id="mothern" name="mothern" data-type="mname" data-pk="1" data-placement="right" data-placeholder="" data-original-title="This is a trial input"><?php echo $userinfo['lastname']; ?></a></td>
   </tr>
 </tbody>
</table>
<br/>
<br/>
<table class="table table-condensed table-striped" style="clear: both; cursor: pointer;">
 <tbody>
   <tr>
	<td style="width:150px;">Spouse Name.</td>
	<td><a href="<?php echo $link;?>" id="sname" name="sname" data-type="fname" data-pk="1" data-placement="right" data-placeholder="" data-original-title="This is a trial input"><?php echo $userinfo['lastname']; ?></a></td>
   </tr>
   <tr>
	<td>Occupation:</td>
	<td><a href="<?php echo $link;?>" id="soccup" name="soccup" data-type="text" data-pk="1" data-placement="right" data-placeholder="" data-original-title="This is a trial input"><?php echo $userinfo['lastname']; ?></a></td>
   </tr>
   <tr>
	<td>Company:</td>
	<td><a href="<?php echo $link;?>" id="soccup" name="soccup" data-type="text" data-pk="1" data-placement="right" data-placeholder="" data-original-title="This is a trial input"><?php echo $userinfo['lastname']; ?></a></td>
   </tr>
   <tr>
	<td>Company Addr.:</td>
	<td><a href="<?php echo $link;?>" id="soccup" name="soccup" data-type="text" data-pk="1" data-placement="right" data-placeholder="" data-original-title="This is a trial input"><?php echo $userinfo['lastname']; ?></a></td>
   </tr>
   <tr>
	<td>Tel No.:</td>
	<td><a href="<?php echo $link;?>" id="soccup" name="soccup" data-type="text" data-pk="1" data-placement="right" data-placeholder="" data-original-title="This is a trial input"><?php echo $userinfo['lastname']; ?></a></td>
   </tr>
 </tbody>
</table>
					
</form>															
</div>
<?php
}
elseif($userinfo['idtype']==1)
{
?>
<!-- Father -->
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-bottom: 20px;">
		<div class="well">
			<form id="xform_father" class="smart-form" onsubmit="return false;">
			    <?php 
		        if(isset($allow) && $allow==1)
		          echo '<button class="btn btn-sm btn-primary pull-right" type="button" id="xsave_stud_f" onclick="savefatherinfo();"><i class="fa fa-save"></i> Save</button>';
				?>
				<header>Father Information</header>
				<fieldset>
					<section>
						<label class="label">Complete Name</label>
						<label class="input">
						<input name="father" class="input-xs" type="text" value="<?php echo (isset($Father_Name))?$Father_Name:'';?>">
						</label>
					</section>
					<section>
						<label class="label">Occupation</label>
						<label class="input">
							<input name="father_occupation" class="input-xs" type="text" value="<?php echo (isset($Father_Occupation))?$Father_Occupation:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Company</label>
						<label class="input">
							<input name="father_company" class="input-xs" type="text" value="<?php echo (isset($Father_Company))?$Father_Company:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Company Address</label>
						<label class="input">
							<input name="father_companyaddress" class="input-xs" type="text" value="<?php echo (isset($Father_CompanyAddr))?$Father_CompanyAddr:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Email</label>
						<label class="input">
							<input name="father_email" class="input-xs" type="text" value="<?php echo (isset($Father_Email))?$Father_Email:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Telephone no.</label>
						<label class="input">
							<input name="father_telno" class="input-xs" type="text" value="<?php echo (isset($Father_TelNo))?$Father_TelNo:'';?>">	
						</label>
					</section>											
				</fieldset>
	      </form>															
		</div>					
	</div><!-- Father End -->
	<!-- Mother -->
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-bottom: 20px;">
		<div class="well">
			<form id="xform_mother" class="smart-form" onsubmit="return false;">
			    <?php 
		        if(isset($allow) && $allow==1)
		          echo '<button class="btn btn-sm btn-primary pull-right" type="button" id="xsave_stud_m" onclick="savemotherinfo();"><i class="fa fa-save"></i> Save</button>';
				?>
				<header>Mother Information</header>
				<fieldset>
					<section>
						<label class="label">Complete Name</label>
						<label class="input">
						<input name="mother" class="input-xs" type="text" value="<?php echo (isset($Mother_Name))?$Mother_Name:'';?>">
						</label>
					</section>
					<section>
						<label class="label">Occupation</label>
						<label class="input">
							<input name="mother_occupation" class="input-xs" type="text" value="<?php echo (isset($Mother_Occupation))?$Mother_Occupation:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Company</label>
						<label class="input">
							<input name="mother_company" class="input-xs" type="text" value="<?php echo (isset($Mother_Company))?$Mother_Company:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Company Address</label>
						<label class="input">
							<input name="mother_companyaddress" class="input-xs" type="text" value="<?php echo (isset($Mother_CompanyAdd))?$Mother_CompanyAdd:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Email</label>
						<label class="input">
							<input name="mother_email" class="input-xs" type="text" value="<?php echo (isset($Mother_Email))?$Mother_Email:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Telephone no.</label>
						<label class="input">
							<input name="mother_telno" class="input-xs" type="text" value="<?php echo (isset($Mother_TelNo))?$Mother_TelNo:'';?>">	
						</label>
					</section>
					
				</fieldset>
				
	      </form>			
		</div>
	</div><!-- /Mother -->
	<!-- Guardian -->
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-bottom: 20px;">								
		<div class="well">									
			<form id="xform_guardian" class="smart-form" onsubmit="return false;">
				<?php 
                 if(isset($allow) && $allow==1)
                    echo '<button class="btn btn-sm btn-primary pull-right" type="button" id="xsave_stud_g" onclick="saveguardianinfo();"><i class="fa fa-save"></i> Save</button>';
				?>
				<header>Guardian Information</header>
				<fieldset>
					<section>
						<label class="label">Complete Name</label>
						<label class="input">
							<input name="guardian" class="input-xs" type="text" value="<?php echo (isset($Guardian_Name))?$Guardian_Name:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Relationship</label>
						<label class="input">
							<input name="guardian_relationship" class="input-xs" type="text" value="<?php echo (isset($Guardian_Relation))?$Guardian_Relation:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Occupation</label>
						<label class="input">
							<input name="guardian_occupation" class="input-xs" type="text" value="<?php echo (isset($Guardian_Occupation))?$Guardian_Occupation:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Company</label>
						<label class="input">
							<input name="guardian_agency" class="input-xs" type="text" value="<?php echo (isset($Guardian_Company))?$Guardian_Company:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">E-Mail</label>
						<label class="input">
							<input name="guardian_email" class="input-xs" type="text" value="<?php echo (isset($Guardian_Email))?$Guardian_Email:'';?>">	
						</label>
					</section>							
					<section>
						<label class="label">Telephone no.</label>
						<label class="input">
							<input  name="guardian_telno"  class="input-xs" type="text" value="<?php echo (isset($Guardian_TelNo))?$Guardian_TelNo:'';?>">	
						</label>
					</section>				
				</fieldset>
			</form>
		</div>								
	</div><!-- /Guardian -->
	<!-- Emergency -->
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding-bottom: 20px;">
		<div class="well">									
			<form id="xform_emergenry" class="smart-form" onsubmit="return false;">
				<?php 
                 if(isset($allow) && $allow==1)
                   echo '<button class="btn btn-sm btn-primary pull-right" type="button" id="xsave_stud_e" onclick="saveemergencyinfo();"><i class="fa fa-save"></i> Save</button>';
				?>
				<header>Emergency Information</header>
				<fieldset>
					<section>
						<label class="label">Complete Name</label>
						<label class="input">
							<input name="emergency_contact" class="input-xs" type="text" value="<?php echo (isset($Emergency_Contact))?$Emergency_Contact:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Address</label>
						<label class="input">
							<input name="Emergency_Address" class="input-xs" type="text" value="<?php echo (isset($Emergency_Address))?$Emergency_Address:'';?>">	
						</label>
					</section>
					<section>
						<label class="label">Mobile No.</label>
						<label class="input">
							<input name="Emergency_mobile" class="input-xs" type="text" value="<?php echo (isset($Emergency_Mobile))?$Emergency_Mobile:'';?>">	
						</label>
					</section>	
					<section>
						<label class="label">Telephone no.</label>
						<label class="input">
							<input name="Emergency_TelNo" class="input-xs" type="text"  value="<?php echo (isset($Emergency_TelNo))?$Emergency_TelNo:'';?>">	
						</label>
					</section>											
				</fieldset>
			</form>
		</div>	
	</div>									
<?php
}
?>
