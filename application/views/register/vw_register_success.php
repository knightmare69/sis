<!DOCTYPE html>
<html lang="en">
<head>
        <!--<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title> PRISMS Online : Register </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="expires" content="<?php echo date('D, d M Y');?> 00:00:00 GMT"/>
        <meta http-equiv="pragma" content="no-cache" />

		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/font-awesome.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-production.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-skins.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/prince.css') ?>">	
		
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
        
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
        
		<script type="text/javascript"> 
		 var base_url = '<?php echo base_url(); ?>'; 
		 var enable_remad = '<?php echo ((isset($enable_remad))?$enable_remad:1); ?>';
		</script>
</head>
<body id="login" class="animated fadeInDown" style="background-image:url(<?php echo base_url('assets/img/demo/s1.jpg').'?'.rand(100,200);?>) !important;background-size:100% !important;height:132% !important;">
	  <header id="header" class="visible-xs visible-sm hidden-md hidden-lg">
		<div id="logo-group pull-right"><span id="logo"> <img src="<?php echo base_url('assets/img/logo-login.png').'?'.rand(100,200); ?>" alt="PRISMS"> </span></div>
	  </header>
	  <header id="header" class="visible-md visible-lg hidden-xs hidden-sm text-center" style="height:100px !important;margin-bottom:0px !important;">
		  <span id="logo" style="width:520px !important;margin-top:10px;"><a href="<?php echo INST_SITE;?>"><img  class="" src="<?php echo base_url('assets/img/BEDISTA.png').'?'.rand(100,200); ?>" style="width:98% !important;" alt="PRISMS"></a></span>
	  </header>
		<div id="main" role="main" style="background-color:rgba(255,255,255,0.2) !important;padding-top:50px !important;height:96% !important;">			
			<div id="content" class="container">
				<div class="row">
				  <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                  <div class="well" style="alignment-adjust: central;">
                    <h1>Thank you!</h1>
                    <p>You have successfully signed up in our <b class="text-danger">BEDISTA Portal</b>.</p>
					<?php
					if(isset($error) && $error['email']=='failed'){
					 echo '<p>Please inform the administrator to activate your account.</p>';
					 echo '<p>Having trouble?<br/>Contact us at (+632) 735-6011 to 15 local 3117 or e-mail us at <b>bedistaportal.help@sanbeda.edu.ph</b></p>';
					}else{
					 echo '<p><ul><li>We`ve sent a confirmation e-mail to '.((isset($reginfo) && $reginfo['email']!='')?('<b>'.$reginfo['email'].'</b>'):('your registered e-mail address')).'</li>
					              <li>Please check your INBOX after a few minutes. It contains instructions on how to <b>verify</b> and <b>activate</b>  your account.</li>
					              <li>If you do not receive our confirmation e-mail, please check your <b class="text-danger">spam/junk mail</b>  folder.</li>
					              <li>Please note that your account will be deleted automatically if you do not verify within the next 24 hours.</li></ul>
						   </p>';
					 echo '<p>Didn`t receive our confirmation e-mail? <a class="resend" href="javascript:void(0);">Resend it now.</a></p>';	   
					 echo '<p>Still having trouble?<br/>Contact us at (+632) 735-6011 to 15 local 3117 or e-mail us at <b>bedistaportal.help@sanbeda.edu.ph</b></p>';	   
					}
					echo '<br/>';
					?>
					<p class="text-right">Read our &nbsp;<a href="javascript:void(0);" class="text-danger pull-right" onclick="$('#privacymodal').modal('show');"><i>Data Privacy	Notice</i></a></p>
                  </div>
				  </div>	
				</div>
			</div>

		</div>
        <div class="modal fade" id="privacymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog" style="width:80% !important;">
			<div class="modal-content">
			 <div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			  <h4 class="modal-title" id="lblmwindow"><strong class="lblmode">Data Privacy Policy</strong></h4>
			 </div>	               
			 <div class="modal-body xpadding-10">
			  <div class="row">
			   <div class="col-sm-12 no-padding">
				   <embed src="<?php echo base_url('assets/Data Privacy Policy.pdf');?>#zoom=125" width="98%" height="600px" type='application/pdf'>
			   </div>
			  </div>
			 </div>
			</div>
		  </div>
		</div>
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->		
		<script src="<?php echo base_url('assets/js/plugin/pace/pace.min.js') ?>"></script>
		
	    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script> if (!window.jQuery) { document.write('<script src="<?php echo base_url('assets/js/libs/jquery-2.0.2.min.js') ?>"><\/script>');} </script>

	    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script> if (!window.jQuery.ui) { document.write('<script src="<?php echo base_url('assets/js/libs/jquery-ui-1.10.3.min.js') ?>"><\/script>');} </script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

		<!-- BOOTSTRAP JS -->		
		<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url('assets/js/notification/SmartNotification.min.js') ?>"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url('assets/js/smartwidgets/jarvis.widget.min.js') ?>"></script>
			   
		
		
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>
		
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url('assets/js/plugin/masked-input/jquery.maskedinput.min.js') ?>"></script>
		
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url('assets/js/plugin/select2/select2.min.js') ?>"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url('assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js') ?>"></script>
		
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>
		
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url('assets/js/plugin/fastclick/fastclick.js') ?>"></script>
		
		<!--[if IE 7]>
			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
			
		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url('assets/js/app.js') ?>"></script>

		<script type="text/javascript">
			runAllForms();
			$('.resend').click(function(){
			  var xlink = base_url+"usermonitoring/regenerate/";
			  var x = "<?php echo $reginfo['username'];?>";
			  var y = "<?php echo $reginfo['email'];?>";
			  if(x=='' || y==''){alert('E-mail resend!!');}
			  $.ajax({
				 type: "POST",
				 url: xlink,
				 data: {username:"'"+x+"'",email:"'"+y+"'"},
				 dataType: "text",
				 success: function (result)
						  {
							var str = result.trim();
							if(str!='' && str!='nodata'){
							 alert('E-mail resend!');
							}				
						  }
				 ,error: function (XHR, status, response){
				   console.log(response);
				 }
				}); 
			});
			
			// Model i agree button
			$("#i-agree").click(function(){
				$this=$("#terms");
				if($this.checked) {
					$('#myModal').modal('toggle');
				} else {
					$this.prop('checked', true);
					$('#myModal').modal('toggle');
				}
			});
			
			// Validation
			$(function() {
				// Validation
				$("#smart-form-register").validate({

					// Rules for form validation
					rules : {
						username : {
							required : true
						},
						email : {
							required : true,
							email : true
						},
						password : {
							required : true,
							minlength : 3,
							maxlength : 20
						},
						passwordConfirm : {
							required : true,
							minlength : 3,
							maxlength : 20,
							equalTo : '#password'
						},
						firstname : {
							required : true
						},
						lastname : {
							required : true
						},
						gender : {
							required : true
						},
						terms : {
							required : true
						}
					},

					// Messages for form validation
					messages : {
						login : {
							required : 'Please enter your login'
						},
						email : {
							required : 'Please enter your email address',
							email : 'Please enter a VALID email address'
						},
						password : {
							required : 'Please enter your password'
						},
						passwordConfirm : {
							required : 'Please enter your password one more time',
							equalTo : 'Please enter the same password as above'
						},
						firstname : {
							required : 'Please select your first name'
						},
						lastname : {
							required : 'Please select your last name'
						},
						gender : {
							required : 'Please select your gender'
						},
						terms : {
							required : 'You must agree with Terms and Conditions'
						}
					},

					// Ajax form submition
					submitHandler : function(form) {
						$(form).ajaxSubmit({
							success : function() {
								$("#smart-form-register").addClass('submited');
							}
						});
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});

			});
		</script>

	</body>
</html>