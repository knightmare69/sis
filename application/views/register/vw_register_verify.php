<!DOCTYPE html>
<html lang="en">
<head>
        <!--<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title> PRISMS Online : Register </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="expires" content="<?php echo date('D, d M Y');?> 00:00:00 GMT"/>
        <meta http-equiv="pragma" content="no-cache" />

		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/font-awesome.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-production.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-skins.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/prince.css') ?>">	
		
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('assets/img/favicon/favicon.ico').'?'.rand(100,200); ?>" type="image/x-icon">
        
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
        
		<script type="text/javascript"> 
		 var base_url = '<?php echo base_url(); ?>'; 
		 var enable_remad = '<?php echo ((isset($enable_remad))?$enable_remad:1); ?>';
		</script>
</head>
<body id="login" class="animated fadeInDown" style="background-image:url(<?php echo base_url('assets/img/demo/s1.jpg').'?'.rand(100,200);?>) !important;background-repeat:no-repeat;background-size:contain !important;height:132% !important;">
  <header id="header" class="visible-xs visible-sm hidden-md hidden-lg">
	<div id="logo-group pull-right"><span id="logo"> <img src="<?php echo base_url('assets/img/logo-login.png').'?'.rand(100,200); ?>" alt="PRISMS"> </span></div>
  </header>
  <header id="header" class="visible-md visible-lg hidden-xs hidden-sm text-center" style="height:100px !important;margin-bottom:0px !important;">
	  <span id="logo" style="width:520px !important;margin-top:10px;"><a href="<?php echo INST_SITE;?>"><img  class="" src="<?php echo base_url('assets/img/BEDISTA.png').'?'.rand(100,200); ?>" style="width:98% !important;" alt="PRISMS"></a></span>
  </header>
  <div id="main" role="main" style="background-color:rgba(255,255,255,0.2) !important;padding-top:50px !important;height:96% !important;">			
			<div id="content" class="container">
				<div class="row">
				  <div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4 hidden-xs hidden-sm">
                  <div class="well" style="alignment-adjust: central;">
				    <div class="row"><div class="col-sm-12 col-md-12"> 
					  <header class="text-center">
					   <img  class="" src="<?php echo base_url('assets/img/BEDISTA_portal_logo.png').'?'.rand(100,200); ?>" style="width:80% !important;" alt="PRISMS">
					  </header>
					  <br/>
                      <?php
					  if ($verify == 0 || $verify == '0'){                        
                          if ($activated == 1 || $activated == '1'){
                            //echo '<div class="alert alert-info">Already Verified/Activated!</h3>';
                            //echo '<p>Your account already activated!</p>';
                            //echo '<p>You can use your existing username and password. Thank you!</p>';
                            echo '<p>For New Applicant/Enrollee/Transferee:
                                    <div class="text-center" >
				                      <h3> <a class="btn btn-sm btn-danger welcome_campus" data-name="manila"><i class="fa fa-building"></i> MANILA CAMPUS</a>&nbsp;&nbsp;&nbsp;&nbsp;
	                                  <a class="btn btn-sm btn-danger welcome_campus" data-name="taytay"><i class="fa fa-building"></i> RIZAL CAMPUS</a> </h3>
                                    </div></p>';
                            echo '<p>For SBU Student/Parent Portal Activation:
								   <div class="text-center" >
									<h3><a class="btn btn-sm btn-default" href="'.base_url('activation/index/1').'"><i class="fa fa-user"></i> Student Portal</a>&nbsp;&nbsp;&nbsp;&nbsp;
									<a class="btn btn-sm btn-default" href="'.base_url('activation/index/2').'"><i class="fa fa-user"></i> Parent Portal</a> </h3>
								   </div>
								  </p>';
                            echo '<p><a href="'. site_url('login').'" class="btn btn-danger btn-sm pull-right" role="button">Log In Now!</a></p><br/><br/>';                      
                          } else {
                            echo '<h3>Account Verification Failed!</h3>';
                            echo '<p>Your account must have been expired!</p>';
                            echo '<p>Please sign up again to continue using PRISMS Portal. Thank you!</p>';
                            echo '<p><a href="'. site_url('register').'" class="btn btn-warning btn-sm pull-right" role="button">Sign Up Now!</a></p>';                          
                          }
                      } else {
                           //echo '<h3>Account Verification Success!</h3>';
                           //echo '<p>You have successfully activate your account!</p>';
                           //echo '<p>If your an applicant/new enrollee? Click here: <a href="'. site_url('admission/welcome').'" class="btn btn-danger btn-xs" role="button">Admission</a></p>';                        
                           //echo '<p>If you want to access Student Portal? Click here: <a href="'. site_url('activation').'" class="btn btn-warning btn-xs" role="button">Portal Activate</a></p>';                        
                           echo '<p>For New Applicant/Enrollee/Transferee:
                                    <div class="text-center" >
				                      <h3> <a class="btn btn-sm btn-danger welcome_campus" data-name="manila"><i class="fa fa-building"></i> MANILA<br/>CAMPUS</a>&nbsp;&nbsp;&nbsp;&nbsp;
	                                  <a class="btn btn-sm btn-danger welcome_campus" data-name="taytay"><i class="fa fa-building"></i> RIZAL<br/>CAMPUS</a> </h3>
                                    </div></p>';
                            echo '<p>For SBU Student/Parent Portal Activation:
								   <div class="text-center" >
									<h3><a class="btn btn-sm btn-default" href="'.base_url('activation/index/1').'"><i class="fa fa-user"></i> Student Portal</a>&nbsp;&nbsp;&nbsp;&nbsp;
									<a class="btn btn-sm btn-default" href="'.base_url('activation/index/2').'"><i class="fa fa-user"></i> Parent Portal</a> </h3>
								   </div>
								  </p>';
                           echo '<p><a href="'. site_url('login').'" class="btn btn-danger btn-sm pull-right" role="button">Log In Now!</a></p><br/><br/>';                      
                      }                                        
                    ?>
					</div></div>
                  </div>
				  </div>
				</div>
			</div>

		</div>

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->		
		<script src="<?php echo base_url('assets/js/plugin/pace/pace.min.js') ?>"></script>
		
	    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script> if (!window.jQuery) { document.write('<script src="<?php echo base_url('assets/js/libs/jquery-2.0.2.min.js') ?>"><\/script>');} </script>

	    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script> if (!window.jQuery.ui) { document.write('<script src="<?php echo base_url('assets/js/libs/jquery-ui-1.10.3.min.js') ?>"><\/script>');} </script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="<?php echo base_url('assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') ?>"></script> -->
		
		<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/notification/SmartNotification.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/smartwidgets/jarvis.widget.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>
		
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url('assets/js/plugin/masked-input/jquery.maskedinput.min.js') ?>"></script>
		
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url('assets/js/plugin/select2/select2.min.js') ?>"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url('assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js') ?>"></script>
		
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>
		
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url('assets/js/plugin/fastclick/fastclick.js') ?>"></script>
		
		<!--[if IE 7]>			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>			
		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url('assets/js/app.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/utilities/admission.js').'?t='.rand(1000,9999); ?>"></script>

		<script type="text/javascript">
			runAllForms();			
		</script>

	</body>
</html>