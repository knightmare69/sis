<HTML>
<HEAD>
<META NAME="Prince" CONTENT="Pre-Registrations">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
</HEAD>
<BODY>
<style>

html,body{
    height:297mm;
    width:210mm;
}

  div {position:absolute; z-index:25}
    a {text-decoration:none}
    a img {border-style:none; border-width:0}
    .PageTitle {font-size:9pt;color:#000000;font-family:Verdana;font-weight:bold;}
    .fci3837egruf4-1 {font-size:8pt;color:#000000;font-family:Arial;font-weight:bold;}
    .rptheader {font-size:10pt;color:#000000;font-family:Old English Text MT;font-weight:bold;}
    .fci3837egruf4-3 {font-size:7pt;color:#000000;font-family:Arial;font-weight:normal;}
    .fci3837egruf4-4 {font-size:7pt;color:#000000;font-family:Arial;font-weight:bold;}
    .fci3837egruf4-5 {font-size:7pt;color:#000000;font-family:Arial Narrow;font-weight:bold;}
    .fci3837egruf4-6 {font-size:7pt;color:#000000;font-family:Arial Narrow;font-weight:normal;}
    .PageTitle {border-color:#000000;border-left-width:0;border-right-width:0;border-top-width:0;border-bottom-width:0;}
    .adi3837egruf4-1 {border-color:#000000;border-style:solid;border-width:0px;border-left-width:0;border-right-width:0;border-top-style:solid;border-top-width:1;border-bottom-width:0;}
    .box {z-index:10; position: static;width:10px;height:10px;border-color:#000000;border-style:solid;border-width:1px;}
    .hozline {z-index:30;position: static;border-color:#808080;border-style:dotted;border-width:0px;border-left-width:1px;clip:rect(0px,1px,50px,0px);height:100%;}
</style>
  <div style="z-index:30;top:588px;left:552px;border-color:#000000;border-style:solid;border-width:0px;border-top-width:1px;width:176px;"></div>
  
  <div style="z-index:3;clip:rect(0px,797px,109px,0px);top:0px;left:0px;width:797px;height:109px;"></div>
  <div title="Text Object" class="PageTitle" style="top:50px;left:43px;width:708px;height:18px;">
    <table width="708px" border="0" cellpadding="0" cellspacing="0">
      <td align="center">
        <span align="center" class="PageTitle"> PRE-REGISTRATION/ASSESSMENT FORM</span>
      </td>
    </table>
  </div>
  <div title="Academic Year & Term" class="PageTitle" style="top:73px;left:223px;width:348px;height:16px;text-align:center;">
    <span align="center" class="fci3837egruf4-1"><?php echo $studentinfo->AyTerm;?></span>
  </div>
  <div title="Valid Until" class="PageTitle" style="top:84px;left:582px;width:223px;height:18px;clip:rect(0px,215px,18px,0px);">
    <table width="223px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-1"></span><span class="fci3837egruf4-1"></span>
      </td>
    </table>
  </div>
  <div title="Institution Name" class="PageTitle" style="top:0px;left:43px;width:708px;height:18px;text-align:center;">
    <span align="center" class="rptheader">San Beda College Alabang</span>
  </div>  
  <div title="Address" class="PageTitle" style="top:22px;left:43px;width:708px;height:18px;text-align:center;">
    <span align="center" class="fci3837egruf4-3">Alabang Hills Village, Muntinlupa City</span>
  </div>  
  <div style="z-index:3;clip:rect(0px,700px,98px,0px);top:109px;left:0px;width:700px;height:98px;"></div>
  <div title="Text Object" class="PageTitle" style="top:169px;left:63px;width:78px;height:13px;">
    <table width="78px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">Program</span><span class="fci3837egruf4-3">:</span>
      </td>
    </table>
  </div>
  <div title="Text Object" class="PageTitle" style="top:152px;left:63px;width:68px;height:13px;">
    <table width="68px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">College</span><span class="fci3837egruf4-3">:</span>
      </td>
    </table>
  </div>
  <div title="Text Object" class="PageTitle" style="top:152px;left:467px;width:71px;height:15px;">
    <table width="71px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">Year&nbsp;Level</span><span class="fci3837egruf4-3">:</span>
      </td>
    </table>
  </div>
  <div title="Label for Schorship" class="PageTitle" style="top:169px;left:467px;width:85px;height:13px;">
    <table width="85px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">Scholarship</span><span class="fci3837egruf4-3">:</span>
      </td>
    </table>
  </div>
  <div title="Year Level" class="PageTitle" style="top:151px;left:552px;width:169px;height:15px;">
    <span class="fci3837egruf4-1"><?php echo $studentinfo->Yearlvl;?></span>
  </div>
  <div title="College Name" class="PageTitle" style="top:152px;left:139px;width:125px;height:12px;">
    <span class="fci3837egruf4-3"><?php echo $studentinfo->College;?></span>
  </div>
  <div title="Text Object" class="PageTitle" style="top:113px;left:63px;width:64px;height:13px;">
    <table width="64px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">Student No :</span><span class="fci3837egruf4-3">:</span>
      </td>
    </table>
  </div>
  <div title="Text Object" class="PageTitle" style="top:130px;left:467px;width:95px;height:13px;">
    <table width="95px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">Reg</span><span class="fci3837egruf4-3">.</span><span class="fci3837egruf4-3">Date</span><span class="fci3837egruf4-3">:</span>
      </td>
    </table>
  </div>
  <div title="Registration Date" class="PageTitle" style="top:132px;left:552px;width:170px;height:13px;">
    <span class="fci3837egruf4-4"><?php echo $studentinfo->RegDate; ?></span>
  </div>
  <div title="Student Number" class="PageTitle" style="top:112px;left:139px;width:128px;height:13px;">
    <span class="fci3837egruf4-4"><?php echo $studentinfo->StudentNo; ?></span>
  </div>
  <div title="Text Object" class="PageTitle" style="top:130px;left:63px;width:95px;height:18px;">
    <table width="95px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">Student&nbsp;Name</span><span class="fci3837egruf4-3">:</span>
      </td>
    </table>
  </div>
  <div title="Student Name" class="PageTitle" style="top:131px;left:139px;width:372px;height:15px;">
    <span class="fci3837egruf4-1"><?php echo $studentinfo->LastName.', '.$studentinfo->Firstname.' '.$studentinfo->Middlename ;?></span>
  </div>
  <div title="Academic Program" class="PageTitle" style="top:169px;left:139px;width:328px;height:13px;">
    <span class="fci3837egruf4-3"><?php echo $studentinfo->Program;?></span>
  </div>
  <div title="Text Object" class="PageTitle" style="top:187px;left:63px;width:78px;height:15px;">
    <table width="78px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">Major</span><span class="fci3837egruf4-3">:</span>
      </td>
    </table>
  </div>  
  <div title="Major Study" class="PageTitle" style="top:187px;left:139px;width:327px;height:13px;">
    <span class="fci3837egruf4-3"></span>
  </div>
  <div title="Registration Number" class="PageTitle" style="top:113px;left:552px;width:134px;height:13px;">
    <span class="fci3837egruf4-4"><?php echo $studentinfo->RegID; ?></span>
  </div>
  <div title="Text Object" class="PageTitle" style="top:113px;left:467px;width:113px;height:15px;">
    <table width="113px" border="0" cellpadding="0" cellspacing="0">
      <td align="left">
        <span class="fci3837egruf4-3">Registration&nbsp;No</span><span class="fci3837egruf4-3">.:</span>
      </td>
    </table>
  </div>
  <div style="z-index:3;clip:rect(0px,797px,123px,0px);top:207px;left:0px;width:797px;height:123px;"></div>
  <div title="" class="PageTitle" style="top:207px;left:40px;width:736px;height:123px;"></div>
  
  <div style="z-index:30;top:243px;left:45px;border-color:#808080;border-style:dotted;border-width:0px;border-top-width:1px;width:690px;"></div>
  
  <div title="Text Object" class="PageTitle" style="top:207px;left:49px;width:683px;height:11px;text-align:center;">
    <table width="683px" border="0" cellpadding="0" cellspacing="0">
      <td align="center">
        <span class="fci3837egruf4-5">REGISTERED SUBJECTS</span>
      </td>
    </table>
  </div>
  <div style="z-index:3;clip:rect(0px,736px,16px,0px);top:246px;left:40px;width:736px;height:16px;">    
  </div>
  <div title=" (String)" class="PageTitle" style="top:207px;left:45px; " >
  
  <div title=" (String)" class="PageTitle" style="position: relative; margin-bottom: 5px; border-color:#808080;border-style:dotted;border-width:1px;" >
    <br>
    <div style="z-index:30;  border-color:#808080;border-style:dotted;border-width:0px;border-top-width:1px;width:100%;"></div>
    <table  width="686px" class="fci3837egruf4-6" >
      <tr>
        <td class="fci3837egruf4-5">CODE</td>
        <td class="fci3837egruf4-5">SUBJECT TITLE</td>
        <td class="fci3837egruf4-5">UNIT</td>
        <td class="fci3837egruf4-5">SECTION</td>
        <td class="fci3837egruf4-5">SCHEDULE</td>
        <td class="fci3837egruf4-5" style="width:65px;">ROOM</td>
      </tr>
<?php

  $totallec = 0;

  foreach($subjects as $rs)
  {
	$xcsid = property_exists($rs,'ScheduleID')? $rs->ScheduleID : '';
	$xsubjid = property_exists($rs,'SubjectID')? $rs->SubjectID : '';   
   $xcode = property_exists($rs,'SubjectCode')? $rs->SubjectCode : '';
   $xtitle = property_exists($rs,'SubjectTitle')? $rs->SubjectTitle : '';
   $xsection = property_exists($rs,'SectionName')? $rs->SectionName : '';
   $xlec = property_exists($rs,'AcadUnits')? $rs->AcadUnits : 0;
   $xlab = property_exists($rs,'LabUnits')? $rs->LabUnits : 0;
   $xsched = property_exists($rs,'Sched_1')? $rs->Sched_1 : '';
   $room = property_exists($rs,'Room1')? $rs->Room1 : '';
	$xsched2 = property_exists($rs,'Sched_2')? $rs->Sched_2 : '';
   $room2 = property_exists($rs,'Room2')? $rs->Room2 : '';
	$xsched3 = property_exists($rs,'Sched_3')? $rs->Sched_3 : '';
   $room3 = property_exists($rs,'Room3')? $rs->Room3 : '';
	$xsched4 = property_exists($rs,'Sched_4')? $rs->Sched_4 : '';
   $room4 = property_exists($rs,'Room4')? $rs->Room4 : '';
	$xsched5 = property_exists($rs,'Sched_5')? $rs->Sched_5 : '';
   $room5 = property_exists($rs,'Room5')? $rs->Room5 : '';
   
   if ($xlec > 0){ $totallec += $xlec; }
	
   
	//$xsched = ($xsched <>""? $xsched . " / " .  $room : "") . ($xsched2 <> ""? "<br>" . $xsched2 . " / " . $room2 :"" );
   
    echo '<tr>';
    echo '<td style="width:60px;">'.$xcode.'</td>';
    echo '<td style="width:250px;">'.$xtitle.'</td>';
    echo '<td style="width:30px;">'. ($xlec < 0 ? '('. abs($xlec) .')' :  $xlec ) .'</td>';
    echo '<td style="width:61px;">'.$xsection.'</td>';
    echo '<td style="width:165px;">'.$xsched.'</td>';
    echo '<td >'.$room.'</td>';
    echo '</tr>';
  }
    echo '<tr>';
    echo '<td style="width:60px;"></td>';
    echo '<td style="width:250px;" align="right"><b>Total :</b></td>';
    echo '<td style="width:30px;"><b>'. $totallec .'</b></td>';
    echo '<td style="width:61px;"></td>';
    echo '<td style="width:165px;"></td>';
    echo '<td style="width:61px;"></td>';
    echo '</tr>';
    

?>
 </table>  
</div>

<div style="z-index:10;border-color:#808080;border-style:dotted;border-width:1px; text-align: center;">
  <span  class="fci3837egruf4-5">ASSESSED&nbsp;FEES</span>
  <div style="z-index:30;border-color:#808080;border-style:dotted;border-width:0px;border-top-width:1px;width:100%;"></div>  
  
    <table width="493px" class="fci3837egruf4-6" >
      <tr>
        <td class="fci3837egruf4-5" style="width: 200px;" align="center">ACCOUNT</td>
        <td class="fci3837egruf4-5" style="width: 50px;">FULL PYMT</td>
        <td class="fci3837egruf4-5" style="width: 50px;">1ST PYMT</td>
        <td class="fci3837egruf4-5" style="width: 50px;">2ND PYMT</td>
        <td class="fci3837egruf4-5" style="width: 50px;">3RD PYMT</td>
      </tr>
      <tr>
        <td>
          <div style="z-index:30;border-color:#808080;border-style:dotted;border-width:0px;border-top-width:1px;width:99%;"></div>  
        </td>
      </tr>
      <?php
       $assessedfees = 0;
       $totalfirst = 0;
       $totalsecond = 0;
       $totalthird = 0;
       
      foreach($fees as $f){
        $xacct = property_exists($f,'AcctName')? $f->AcctName : '';
        $xdebit = property_exists($f,'Debit')? $f->Debit : 0;
        $xuno = property_exists($f,'1st Payment')? $f->{'1st Payment'} : 0;
        $xdos = property_exists($f,'2nd Payment')? $f->{'2nd Payment'} : 0;
        $xtres = property_exists($f,'3rd Payment')? $f->{'3rd Payment'} : 0;
        
        $assessedfees = property_exists($f,'AssessedFees')? $f->{'AssessedFees'} : 0;
        
        if ( $xuno  ==0 && $xdos == 0 && $xtres == 0){
          $xuno = $xdebit;
        }
        $totalfirst += $xuno;
        $totalsecond += $xdos;
        $totalthird += $xtres;
        
        $xdebit = number_format($xdebit, 2, '.', '');
        $xuno = number_format($xuno, 2, '.', '');
        $xdos = number_format($xdos, 2, '.', '');
        $xtres = number_format($xtres, 2, '.', '');
        
        $assessedfees = number_format($assessedfees, 2, '.', '');
        $totalfirst = number_format($totalfirst, 2, '.', '');
        $totalsecond = number_format($totalsecond, 2, '.', '');
        $totalthird = number_format($totalthird, 2, '.', '');
        
        echo '<tr>';
        echo '<td>'.$xacct.'</td>';
        echo '<td align="right">'.$xdebit.'</td>';
        echo '<td align="right" > '.$xuno.'</td>';
        echo '<td align="right" >'.$xdos.'</td>';
        echo '<td align="right">'.$xtres.'</td>';
        echo '</tr>';
        
      }
      
      ?>
      <tr>
        <td colspan=5>
          <div style="z-index:30;position: static;border-color:#808080;border-style:dotted;border-width:0px;border-top-width:1px;"></div>          
        </td>
      </tr>
      <tr>
        <td align="right" class="fci3837egruf4-5">TOTAL
          
        </td>
        <td align="right" class="hozline"><strong><?php echo $assessedfees;?></strong></td>
        <td align="right"><strong><?php echo $totalfirst; ?></strong></td>
        <td align="right"><strong><?php echo $totalsecond; ?></strong></td>
        <td align="right"><strong><?php echo $totalthird; ?></strong></td>
      </tr>
            <tr>
        <td colspan=5>
          <div style="z-index:30;position: static;border-color:#808080;border-style:dotted;border-width:0px;border-top-width:1px;"></div>          
        </td>
      </tr>
      <tr>
        <td align="right" class="fci3837egruf4-5">SCHEDULE OF PAYMENT</td>
        <td align="right" class="hozline"><strong><?php echo $assessedfees;?></strong></td>
        <td align="right"><strong><?php echo $totalfirst; ?></strong></td>
        <td align="right"><strong><?php echo $totalsecond; ?></strong></td>
        <td align="right"><strong><?php echo $totalthird; ?></strong></td>       
      </tr>
            <tr>
        <td colspan=5>
          <div style="z-index:30;position: static;border-color:#808080;border-style:dotted;border-width:0px;border-top-width:1px;"></div>          
        </td>
      </tr>
      <tr>
        <td align="right" class="fci3837egruf4-5">PAYMENT MODE</td>
        <td align="center" class="hozline">                     
           <div class="box" style="float: left; margin-left: 10px;"></div>
           <span class="fci3837egruf4-5"> Full </span>
        </td>
        <td  align="left" colspan=3 >
          <div class="box" style="float: left; margin-left: 10px;"></div>
        <span class="fci3837egruf4-5" style="margin-left: 10px;"> Installment Basis </span>
        </td>
        
        
      </tr>
    </table>
    

  </div>
</div>
  
<div title="Text Object" class="PageTitle" style="top:502px;left:548px;width:76px;height:16px;">
<table width="76px" border="0" cellpadding="0" cellspacing="0">
<td align="left">
<span class="fci3837egruf4-6">Checked&nbsp;By</span><span class="fci3837egruf4-6">:</span></td></table>
</div>
<div title="Text Object" class="PageTitle" style="top:637px;left:549px;width:62px;height:16px;">
<table width="62px" border="0" cellpadding="0" cellspacing="0">
<td align="left">
<span class="fci3837egruf4-6">Conforme</span><span class="fci3837egruf4-6">:</span></td></table></div>
<div title="Text Object" class="PageTitle" style="top:592px;left:549px;width:178px;height:36px;">
<table width="178px" border="0" cellpadding="0" cellspacing="0">
<tr>
<td align="left">
<span class="fci3837egruf4-6">Kindly&nbsp;sign&nbsp;below&nbsp;to&nbsp;attest&nbsp;the&nbsp;correctness&nbsp;of&nbsp;</span></td></tr><tr>
<td align="left">
<span class="fci3837egruf4-6">all&nbsp;entries</span><span class="fci3837egruf4-6">.</span></td></tr></table></div>
<div title="Text Object" class="PageTitle" style="top:438px;left:547px;width:185px;height:58px;">
<table width="185px" border="0" cellpadding="0" cellspacing="0">
<tr>
<td align="left">
<span class="fci3837egruf4-6">Please&nbsp;check&nbsp;whether&nbsp;all&nbsp;entries&nbsp;in&nbsp;this&nbsp;</span></td></tr><tr>
<td align="left">
<span class="fci3837egruf4-6">Pre</span><span class="fci3837egruf4-6">-</span><span class="fci3837egruf4-6">Registration&nbsp;</span><span class="fci3837egruf4-6">/&nbsp;</span><span class="fci3837egruf4-6">Assessment&nbsp;Form&nbsp;are&nbsp;</span></td></tr><tr>
<td align="left">
<span class="fci3837egruf4-6">accurate&nbsp;and&nbsp;correct</span><span class="fci3837egruf4-6">.</span></td></tr></table></div>

<div title=" (String)" class="PageTitle" style="top:575px;left:521px;width:238px;height:16px;text-align:center;">
<span align="center" class="fci3837egruf4-5"><?php echo $studentinfo->LastName.', '.$studentinfo->Firstname.' '.$studentinfo->Middlename ;?></span></div>
<div style="z-index:3;clip:rect(0px,797px,49px,0px);top:1238px;left:0px;width:797px;height:49px;">

</div>

<div title="" class="PageTitle" style="top:1040px;left:403px;width:328px;height:13px;text-align:right;">
<span align="right" class="fci3837egruf4-3">Print Info: <?php $today = new DateTime('NOW'); echo $today->format( 'Y-m-d H:i:s' ) . ' [' . gethostbyaddr($_SERVER['REMOTE_ADDR']) . ']'; ?> </span></div>


</BODY>
</HTML>