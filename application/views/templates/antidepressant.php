<div class="xdivMessageBox animated fadeIn fast <?php echo ((isset($hidden) && $hidden)?'hidden':'');?>" id="antidepress">
 <div class="xMessageBoxContainer animated fadeIn fast" id="antidepressmsg">
   <div class="MessageBoxMiddle">
    <span class="MsgTitle">
    <center><i class="fa fa-spinner fa-spin fa-2x" style="color: blue;"></i> <strong id="antidepress_content">Loading... Please Wait...</strong></center>
    </span>
   </div>
 </div>
</div>
