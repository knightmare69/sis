<?php
if((isset($userinfo) && !isset($sidebar)) || (isset($sidebar) && $sidebar==true)){	
$sidebar = true;
?>
<aside id="left-panel">
	<div class="row" id="profile-info">
		<div class="text-align-center" style="padding-top: 10px; padding-bottom: 10px;" >                
		 <div class="image-div" style="background-image:url('<?php echo $userinfo['photo'].'?t='.rand(8888,9999); ?>'); background-size:180px;"></div>
		</div>
		<div class="text-align-center" style="padding-top: 10px; padding-bottom: 10px;" >
			<div class="text-primary font-bold uppercase" >
				<a href="<?php echo site_url('profile') ; ?>" id="username">
					<?php echo utf8_encode(trim($userinfo['username'])); ?>
				</a>
			</div>
			<div class="text-primary font-bold uppercase"><?php //echo trim($userinfo['CourseCode']).' / '.trim($userinfo['YearLevel']) ; ?>  </div>
		 </div>
	</div>
	
	<!-- User info -->
	<div class="login-info">
	 <span> <!-- User image size is adjusted inside CSS, it should stay as it --> 					
	   <a href="javascript:void(0);" <?php if($userinfo['idtype']!=0){echo 'id="show-shortcut"';}?> >
		<img src="<?php echo  $userinfo['photo'].'?'.rand(8888,9999); ?>" alt="me" class="online" style="max-height:50px;max-width:50px;"/> 
		 <span id='username'><?php echo utf8_encode(trim($userinfo['username'])); ?></span>
		 <?php if($userinfo['idtype']!=0){echo '<i class="fa fa-angle-down"></i>';}?>
	   </a>
       <?php echo(($title=='Dashboard' || $title=='Profile')?'<i class="xuserdata hidden" idtype="'.$userinfo['idtype'].'"></i>':''); ?>
	 </span>
	</div>
	<!-- end user info-->
	<nav>
		<ul>
			<?php
			    $prof_cls = ((defined('SHOW_PROFILE') && SHOW_PROFILE==0)?'mnuprofile':'mprofile');
				switch($userinfo['idtype']){
				  case -1://Administrator
				  /* Dashboard */
				    echo '<li ' . ($title == 'Dashboard'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a id="mnuhome" href="'.site_url('home') . '" title="Dashboard" ><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>';
					echo '</li>';
					
					/* Profile Menu */
					echo '<li ' . ($title == 'Profile'? 'class="'.$prof_cls.' active"':'class="'.$prof_cls.'"' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('profile') . '"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Profile</span></a>';
					echo '</li>';
                    
					/* Inbox Menu */
					echo '<li ' . ($title == 'Inbox'? 'class="active"':'class=""' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('inbox') . '"><i class="fa fa-lg fa-fw fa-envelope"></i> <span class="menu-item-parent">Inbox</span></a>';
					echo '</li>';
					
					/* Admission Menu*/
					if($this->mod_main->get_local_security('Admission')=='1'){
					echo '<li>';
					echo '<a href="#"><i class="fa fa-lg fa-fw fa-file"></i> <span class="menu-item-parent">Admission</span></a>';
					 echo '<ul>';
					  echo '<li onclick="antidepressant();">';
					  echo '<a href="'.site_url('admission/admission_list') . '"><i class="fa fa-lg fa-fw fa-users"></i> Applicants List</a>';
					  echo '</li>';
					  echo '<li onclick="antidepressant();">';
					  echo '<a href="'.site_url('admission/admission_officer') . '"><i class="fa fa-lg fa-fw fa-pencil"></i> Applicants Profile</a>';
					  echo '</li>';
					 echo '</ul>';
					echo '</li>';
					}
					
					/* Grades Menu */
					if($this->mod_main->get_local_security('mnuGradeCard')=='1'){
					echo '<li ' . ($title == 'Grades'||$title == 'Report of Grades'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('grades') . '"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">Grades</span></a>';
					echo '</li>';
					}
					
					/* Evaluation Menu */
					if($this->mod_main->get_local_security('mnuAcadEvaluation')=='1'){
					echo '<li ' . (($title == 'Evaluation' ||$title == 'Student Evaluation')? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('evaluation') . '"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">Evaluation</span></a>';
					echo '</li>';
					}
					
					/* Student Advising Menu */
					if($this->mod_main->get_local_security('mnuStudentAdvising')=='1' || $this->mod_main->get_local_security('mnuEnrollment')=='1' ){
					echo '<li ' . ($title == 'Advising'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('advising') . '"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Advising</span></a>';
					echo '</li>';
					}
					
					/* Online Payment Menu - mnuCashiering*/
					if($this->mod_main->get_local_security('mnuCashiering')=='1' ){
					echo '<li ' . ($title == 'Online Payment'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('epayment') . '"><i class="fa fa-lg fa-fw fa-money"></i> <span class="menu-item-parent">Online Payment</span></a>';
					echo '</li>';
                    }

                    /* Faculty Menu*/ 
                    if($this->mod_main->get_local_security(' mnuFacultyClassSchedule')=='1' ){
					echo '<li ' . ($title == 'Faculty'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('faculty') . '"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Grade Encoding</span></a>';
					echo '</li>';
                    }

					/* Summary Of Faculty Evaluation - mnuEmployees*/
                    if($this->mod_main->get_local_security('mnuEmployees')=='1' ){
					echo '<li ' . ($title == 'Summary'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('faculty_evaluation_summary') . '"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Evaluation Summary</span></a>';
					echo '</li>';
					}

					/* Email Blast*/
					echo '<li onclick="antidepressant();">';
					echo '<a href="'.site_url('enotify') . '"><i class="fa fa-lg fa-fw fa-envelope"></i> <span class="menu-item-parent">Email Notification</span></a>';
					echo '</li>';
					
					/* Admin Controls */
					echo '<li ' . ($title == 'Admin Controls'? 'class="active"':'' ) . '>';
					echo '<a href="#"><i class="fa fa-lg fa-fw fa-gears"></i> <span class="menu-item-parent">Admin Controls</span> <b class="collapse-sign"></b></a>';	
					echo '<ul>';
					
					echo '<li onclick="antidepressant();">';
					echo '<a href="'.site_url('usermonitoring') . '"><i class="fa fa-lg fa-fw fa-users"></i> User Monitoring</a>';
					echo '</li>';
					
					echo '<li>';
					echo '<a href="#"><i class="fa fa-lg fa-fw fa-gears"></i> <span>Configurations</span></a>';
					 echo '<ul>';
					  echo '<li onclick="antidepressant();">';
					  echo '<a href="'.site_url('sisconfig') . '"><i class="fa fa-lg fa-fw fa-gears"></i> System Config</a>';
					  echo '</li>';
					  echo '<li onclick="antidepressant();">';
					  echo '<a href="'.site_url('facultyconfig') . '"><i class="fa fa-lg fa-fw fa-gears"></i> Faculty Config</a>';
					  echo '</li>';
					  echo '<li onclick="antidepressant();">';
					  echo '<a href="'.site_url('advschedctrl') . '"><i class="fa fa-lg fa-fw fa-gears"></i> Online Sched. Config</a>';
					  echo '</li>';
					 echo '</ul>';
					echo '</li>';
					
					echo '<li onclick="antidepressant();">';
					echo '<a href="'.site_url('translogs') . '"><i class="fa fa-lg fa-fw fa-table"></i> Transactions Logs</a>';
					echo '</li>';
					
					echo '<li onclick="antidepressant();">';
					echo '<a href="'.site_url('errorlogs') . '"><i class="fa fa-lg fa-fw fa-warning"></i> Errors Logs</a>';
					echo '</li>';
					
					echo '</ul>';
					echo '</li>';
				   break;
				  case 0: /* Applicant */

					 if ($userinfo['appcount'] <> 0  )
					 {
						/* Admission Menu */
						echo '<li ' . ($title == 'Admission'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('admission') . '" title="Admission"><i class="fa fa-lg fa-fw fa-gavel"></i> <span class="menu-item-parent">Admission</span></a>';
						echo '</li>';
										  
						/* Download Menu */
						echo '<li ' . ($title == 'Download'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('download') . '"><i class="fa fa-lg fa-fw fa-download"></i> <span class="menu-item-parent">Downloads</span></a>';
						echo '</li>';
				  
				  
					 }
					 else 
					 {

						/* Activation mnu */
						echo '<li ' . ($title == 'Activation'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('activation') . '"><i class="fa fa-lg fa-fw fa-flash"></i> <span class="menu-item-parent">Portal Activation</span></a>';
						echo '</li>';
				  
					 }					 
				  
					 break;
				  case 1:
						
						/* Dashboard */
						echo '<li ' . ($title == 'Dashboard'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a id="mnuhome" href="'.site_url('home') . '" title="Dashboard" ><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>';
						echo '</li>';
						
						/* Profile Menu */
						echo '<li ' . ($title == 'Profile'? 'class="mnuprofile active"':'class="mnuprofile"' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('profile') . '"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Profile</span></a>';
						echo '</li>';

						/* Grades Menu */
						echo '<li ' . ($title == 'Grades'||$title == 'Report of Grades'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('grades') . '"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">Grades</span></a>';
						echo '</li>';
						
						/* Evaluation Menu */
						echo '<li ' . (($title == 'Evaluation' ||$title == 'Student Evaluation')? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('evaluation') . '"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">Evaluation</span></a>';
						echo '</li>';
						
						/* Student Advising Menu */
                                               if($this->mod_main->checkIfCOM()==4){
						echo '<li ' . ($title == 'Enrollment'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('enrollment') . '"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Enrollment</span></a>';
						echo '</li>';
						}else{
						echo '<li ' . ($title == 'Advising'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('advising') . '"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Advising</span></a>';
						echo '</li>';
                                                }

						/* Student Ledger Menu 
						echo '<li ' . ($title == 'Ledger'||$title == 'Student Ledger'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('ledger') . '"><i class="fa fa-lg fa-fw fa-file-text-o"></i> <span class="menu-item-parent">Student Ledger</span></a>';
						echo '</li>';
						*/

					        /* Faculty Evaluation
       					        echo '<li ' . ($title == 'Faculty Evaluation'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					        echo '<a href="'.site_url('faculty_evaluation') . '"><i class="fa fa-lg fa-fw fa-tasks"></i> <span class="menu-item-parent">Faculty Evaluation</span></a>';
					        echo '</li>'; */

						/* Accountabilities Menu */
						echo '<li ' . ($title == 'Accountabilities'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('accountabilities') . '"><i class="fa fa-lg fa-fw fa-exclamation-circle"></i> <span class="menu-item-parent">Accountabilities</span></a>';
						echo '</li>';
						
						/* Calendar Menu
						echo '<li ' . ($title == 'Calendar'? 'class="active"':'' ) . '>';
						echo '<a href="'.site_url('calendar') . '"><i class="fa fa-lg fa-fw fa-calendar"><em>3</em></i> <span class="menu-item-parent">Calendar</span></a>';
						echo '</li>';
						*/
						
						/* Download Menu */
						echo '<li ' . ($title == 'Download'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('download') . '"><i class="fa fa-lg fa-fw fa-download"></i> <span class="menu-item-parent">Downloads</span></a>';
						echo '</li>';						  
                                               
						/* Download Menu */
						echo '<li onclick="rem_antidepr();">';
						echo '<a href="'.base_url().'downloads/Policies on School Fees, Scholarships and Payments.pdf'.'"><i class="fa fa-lg fa-fw fa-download"></i> <span class="menu-item-parent">Terms And Conditions</span></a>';
						echo '</li>';		
					 break;
				  case 2:
					/* Profile Menu */
						echo '<li ' . ($title == 'Profile'? 'class="mnuprofile active"':'class="mnuprofile"' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('profile') . '"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Profile</span></a>';
						echo '</li>';
					/* Grades Menu */
						echo '<li ' . ($title == 'Grades'||$title == 'Report of Grades'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('grades') . '"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">Grades</span></a>';
						echo '</li>';	
					/* Evaluation Menu */
						echo '<li ' . (($title == 'Evaluation' ||$title == 'Student Evaluation')? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('evaluation') . '"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">Evaluation</span></a>';
						echo '</li>';
						
					/* Student Ledger Menu 
						echo '<li ' . ($title == 'Ledger'||$title == 'Student Ledger'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('ledger') . '"><i class="fa fa-lg fa-fw fa-file-text-o"></i> <span class="menu-item-parent">Student Ledger</span></a>';
						echo '</li>';
					*/

					/* Accountabilities Menu
						echo '<li ' . ($title == 'Accountabilities'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('accountabilities') . '"><i class="fa fa-lg fa-fw fa-exclamation-circle"></i> <span class="menu-item-parent">Accountabilities</span></a>';
						echo '</li>'; 
					*/
					 
					/* Subscription Menu */
						echo '<li ' . ($title == 'Subscription'? 'class="active"':'' ) . '>';
						echo '<a href="#"><i class="fa fa-lg fa-fw fa-rss"></i> <span class="menu-item-parent">Subscription</span><b class="collapse-sign"></b></a>';
						echo '<ul>';
						
						echo '<li ' . ($title == 'Subscribe'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('subscribe') . '">Subscribe</a>';
						echo '</li>';
						
						echo '<li ' . ($title == 'Student List'? 'class="active"':'' ) . ' onclick="antidepressant();">';
						echo '<a href="'.site_url('subscribestudentlist') . '">Student List</a>';
						echo '</li>';
						
						echo '</ul>';
						echo '</li>';

					break;
				  case 3:
					/* Dashboard */
					echo '<li ' . ($title == 'Dashboard'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a id="mnuhome" href="'.site_url('home') . '" title="Dashboard" ><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>';
					echo '</li>';
						
					/* Profile Menu */
					echo '<li ' . ($title == 'Profile'? 'class="mnuprofile active"':'class="mnuprofile"' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('profile') . '"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Profile</span></a>';
					echo '</li>';

					/* Faculty Menu*/ 
					echo '<li ' . ($title == 'Faculty'? 'class="active"':'' ) . ' onclick="antidepressant();">';
					echo '<a href="'.site_url('faculty') . '"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Grade Encoding</span></a>';
					echo '</li>';
				   
				   break;
					 
				  default:
					 break;
				}
			?>					
			<li class='hidden' <?php //echo $title == 'About'||$title == 'About Us'? 'class="active"':'';?>>
				<a href="javascript:void(0);"><i class="fa fa-lg fa-fw fa-info"></i> <span class="menu-item-parent">About</span></a>
				<ul>
					<li><a href="<?php echo site_url('aboutus');?>">About Us</a></li>
					<li><a href="javascript:void(0);">Contact Us</a></li>
					<li><a href="javascript:void(0);">FAQs</a></li>
				</ul>
			</li>
		</ul>
	</nav>
	<span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>
</aside>
<?php
}
?>
<!-- END NAVIGATION -->
<div id="main" role="main" <?php echo((isset($sidebar) && $sidebar)?'':'style="margin-left:1px !important;"'); ?>>		