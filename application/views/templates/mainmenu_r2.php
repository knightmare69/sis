<aside id="left-panel">
 <div class="row" id="profile-info">
  <div class="text-align-center" style="padding-top: 10px; padding-bottom: 10px;" >                
   <div class="image-div" style="background-image:url('<?php echo $userinfo['photo'].'?t='.rand(8888,9999); ?>'); background-size:180px;"></div>
  </div>
  <div class="text-align-center" style="padding-top: 10px; padding-bottom: 10px;" >
   <div class="text-primary font-bold uppercase" >
	<a href="<?php echo ((isset($privileges) && array_key_exists('002000',$privileges) && $privileges['002000']==1)?site_url('profile'):'#'); ?>"><?php echo trim(utf8_encode($userinfo['username'])) ; ?></a>
   </div>
   <div class="text-primary font-bold uppercase"><?php //echo trim($userinfo['CourseCode']).' / '.trim($userinfo['YearLevel']) ; ?>  </div>
  </div>
 </div>
 <!-- User info -->
 <div class="login-info">
  <span> <!-- User image size is adjusted inside CSS, it should stay as it --> 					
   <a href="javascript:void(0);" <?php if($userinfo['idtype']!=0){echo 'id="show-shortcut"';}?> >
	<img src="<?php echo  $userinfo['photo'].'?'.rand(8888,9999); ?>" alt="me" class="online" style="max-height:50px;max-width:50px;"/> 
	<span id='username'><?php echo $userinfo['username']; ?></span>
	<?php if($userinfo['idtype']!=0){echo '<i class="fa fa-angle-down"></i>';}?>
   </a> 					
  </span>
 </div>
 <!-- end user info-->
 <nav>
  <ul>
   <?php
    /* Activation mnu */
	if(!isset($privileges) || $privileges==''){	
	 echo '<li ' . ($title == 'Activation'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('activation') . '"><i class="fa fa-lg fa-fw fa-flash"></i> <span class="menu-item-parent">Portal Activation</span></a>';
	 echo '</li>';
    }
	
	/* Admission Menu */
	if(isset($privileges) && array_key_exists('000000',$privileges) && $privileges['000000']==1){
	 echo '<li ' . ($title == 'Admission'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('admission') . '" title="Admission"><i class="fa fa-lg fa-fw fa-gavel"></i> <span class="menu-item-parent">Admission</span></a>';
	 echo '</li>';
	}	
	
	/* Dashboard */
	if(isset($privileges) && array_key_exists('001000',$privileges) && $privileges['001000']==1){
	 echo '<li ' . ($title == 'Dashboard'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a id="mnuhome" href="'.site_url('home') . '" title="Dashboard" ><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>';
	 echo '</li>';
	}
	/* Profile Menu */
	if(isset($privileges) && array_key_exists('002000',$privileges) && $privileges['002000']==1){
	 echo '<li ' . ($title == 'Profile'? 'class="mnuprofile active"':'class="mnuprofile"' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('profile') . '"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">Profile</span></a>';
	 echo '</li>';
    }
	
	/* Grades Menu */
	if(isset($privileges) && array_key_exists('003001',$privileges) && $privileges['003001']==1){
	 echo '<li ' . ($title == 'Grades'||$title == 'Report of Grades'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('grades') . '"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">Grades</span></a>';
	 echo '</li>';
	}
	
	/* Evaluation Menu */
	if(isset($privileges) && array_key_exists('003002',$privileges) && $privileges['003002']==1){
	 echo '<li ' . (($title == 'Evaluation' ||$title == 'Student Evaluation')? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('evaluation') . '"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">Evaluation</span></a>';
	 echo '</li>';
    }
	
	/* Student Ledger Menu */
	if(isset($privileges) && array_key_exists('003003',$privileges) && $privileges['003003']==1){
	 echo '<li ' . ($title == 'Ledger'||$title == 'Student Ledger'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('ledger') . '"><i class="fa fa-lg fa-fw fa-file-text-o"></i> <span class="menu-item-parent">Student Ledger</span></a>';
	 echo '</li>';
	}
	
    /* Accountabilities Menu */
	if(isset($privileges) && array_key_exists('003004',$privileges) && $privileges['003004']==1){
	 echo '<li ' . ($title == 'Accountabilities'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('accountabilities') . '"><i class="fa fa-lg fa-fw fa-exclamation-circle"></i> <span class="menu-item-parent">Accountabilities</span></a>';
	 echo '</li>';
	}
	
	/* Student Advising Menu */
	if(isset($privileges) && array_key_exists('003005',$privileges) && $privileges['003005']==1){
	 echo '<li ' . ($title == 'Advising'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('advising') . '"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Advising</span></a>';
	 echo '</li>';
	}
	
	/* Faculty Menu*/ 
	if(isset($privileges) && array_key_exists('005000',$privileges) && $privileges['005000']==1){
	 echo '<li ' . ($title == 'Faculty'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('faculty') . '"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Grade Encoding</span></a>';
	 echo '</li>';
    }
	
    /* Calendar Menu */
	if(isset($privileges) && array_key_exists('011000',$privileges) && $privileges['011000']==1){
	 echo '<li ' . ($title == 'Calendar'? 'class="active"':'' ) . '>';
	 echo '<a href="'.site_url('calendar') . '"><i class="fa fa-lg fa-fw fa-calendar"><em class="hidden">3</em></i> <span class="menu-item-parent">Calendar</span></a>';
	 echo '</li>';
	}
	
	/* Subscription Menu */
	if(isset($privileges) && array_key_exists('004000',$privileges) && $privileges['004000']==1){
	 echo '<li ' . ($title == 'Subscription'? 'class="active"':'' ) . '>';
	 echo '<a href="#"><i class="fa fa-lg fa-fw fa-rss"></i> <span class="menu-item-parent">Subscription</span><b class="collapse-sign"></b></a>';
	 echo '<ul>';
	}
	 if(isset($privileges) && array_key_exists('004001',$privileges) && $privileges['004001']==1){
	  echo '<li ' . ($title == 'Subscribe'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	  echo '<a href="'.site_url('subscribe') . '">Subscribe</a>';
	  echo '</li>';
	 }
	 
	 if(isset($privileges) && array_key_exists('004002',$privileges) && $privileges['004002']==1){
	  echo '<li ' . ($title == 'Student List'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	  echo '<a href="'.site_url('subscribestudentlist') . '">Student List</a>';
	  echo '</li>';
	 }
	 
	if(isset($privileges) && array_key_exists('004000',$privileges) && $privileges['004000']==1){
	 echo '</ul>';
	 echo '</li>';
    }
	
	/* Email Blast*/
	if(isset($privileges) && array_key_exists('006000',$privileges) && $privileges['006000']==1){
	 echo '<li onclick="antidepressant();">';
	 echo '<a href="'.site_url('enotify') . '"><i class="fa fa-lg fa-fw fa-envelope"></i> <span class="menu-item-parent">Email Notification</span></a>';
	 echo '</li>';
    }
	
	/* Admin Controls */
	if(isset($privileges) && array_key_exists('007000',$privileges) && $privileges['007000']==1){
	echo '<li ' . ($title == 'Admin Controls'? 'class="active"':'' ) . '>';
	echo '<a href="#"><i class="fa fa-lg fa-fw fa-gears"></i> <span class="menu-item-parent">Admin Controls</span> <b class="collapse-sign"></b></a>';	
	echo '<ul>';
	
	echo '<li onclick="antidepressant();">';
	echo '<a href="'.site_url('usermonitoring').'"><i class="fa fa-lg fa-fw fa-users"></i> User Monitoring</a>';
	echo '</li>';
	
	echo '<li>';
	echo '<a href="#"><i class="fa fa-lg fa-fw fa-gears"></i> <span>Configurations</span></a>';
	echo '<ul>';
	echo '<li onclick="antidepressant();">';
	echo '<a href="'.site_url('sisconfig') . '"><i class="fa fa-lg fa-fw fa-gears"></i> System Config</a>';
	echo '</li>';
	echo '<li onclick="antidepressant();">';
	echo '<a href="'.site_url('facultyconfig') . '"><i class="fa fa-lg fa-fw fa-gears"></i> Faculty Config</a>';
	echo '</li>';
	echo '</ul>';
	echo '</li>';
	
	echo '<li onclick="antidepressant();">';
	echo '<a href="'.site_url('translogs') . '"><i class="fa fa-lg fa-fw fa-table"></i> Transactions Logs</a>';
	echo '</li>';
	
	echo '<li onclick="antidepressant();">';
	echo '<a href="'.site_url('errorlogs') . '"><i class="fa fa-lg fa-fw fa-warning"></i> Errors Logs</a>';
	echo '</li>';
	
	echo '</ul>';
	echo '</li>';
    }
	
	/* Download Menu */
	if(isset($privileges) && array_key_exists('008000',$privileges) && $privileges['008000']==1){
	 echo '<li ' . ($title == 'Download'? 'class="active"':'' ) . ' onclick="antidepressant();">';
	 echo '<a href="'.site_url('download') . '"><i class="fa fa-lg fa-fw fa-download"></i> <span class="menu-item-parent">Downloads</span></a>';
	 echo '</li>';						  
    }
   ?>
  	
   <li class='hidden' <?php //echo $title == 'About'||$title == 'About Us'? 'class="active"':'';?>>
	<a href="javascript:void(0);"><i class="fa fa-lg fa-fw fa-info"></i> <span class="menu-item-parent">About</span></a>
	<ul>
	 <li><a href="<?php echo site_url('aboutus');?>">About Us</a></li>
	 <li><a href="javascript:void(0);">Contact Us</a></li>
	 <li><a href="javascript:void(0);">FAQs</a></li>
	</ul>
   </li>
  </ul>
 </nav>
 <span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>
</aside>
<!-- END NAVIGATION -->
<div id="main" role="main">		