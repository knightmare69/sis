<div style="display:block; margin-top:80px; min-height:500px; padding:0;">	
 <div class="row text-align-center error-box">
  <h1 class="error-text shake animated"><i class="fa fa-warning text-warning error-icon-shadow"></i> Under Maintenance!</h1>
  <?php 
   $msg = ((defined('MAINTENANCE_MSG'))?MAINTENANCE_MSG:'');
   $msg = str_replace('@MAINTENANCEMODE',date('h:i A',strtotime(MAINTENANCE_MODE)),$msg);
   $msg = str_replace('@MAINTENANCESTART',date('h:i A',strtotime(MAINTENANCE_START)),$msg);
   if($msg!=''){echo '<h2 class="font-xl"><strong>'.$msg.'</strong></h2><br/>'; }
  ?>
  <p class="lead semi-bold">
	<strong>We are currently working on something. Please wait for a while.</strong>
  </p>
 </div> 
</div>