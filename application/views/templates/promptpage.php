<div id="ribbon">
  <span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
   <ol class="breadcrumb">
	<li><a id="mnuhome" href="<?php echo site_url('."home."'); ?>"> Home</a></li>
	<li id="mnutitle" <?php echo((isset($ribbontarget))? 'data-target="'.$ribbontarget.'"':''); ?>><?php echo((isset($ribbonpage))? $ribbonpage:''); ?></li>
  </ol>
 </div>   
 <div id="content">	
   <div class="row">
   <div class="col-lg-12">
	<?php 
	  if(isset($promptmsg))
	  {
	   echo $promptmsg;
	  }
	?>
   </div>
   </div>
 </div>