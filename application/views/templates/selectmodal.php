<div class="modal fade" id="selectmodal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	 <div class="modal-dialog" style="width:600px;">
		<div class="modal-content">
		  <div class="modal-header">
			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			 <h4 class="modal-title" id="lblmwindow"><h4>Select <i id="data"></i></h4>
		  </div>	               
			 <div class="modal-body">
			  <div class="row">
				<div class="alert alert-info no-margin fade in">
					<p>Kindly select <b id="data-detail"></b>.</p>
				</div>
				<div class="col-sm-12 no-padding">
				 <div class="input-group">
				  <input class="form-control" id="selectfilter" name="selectfilter" type="text"/><span class="input-group-addon btnselectfilter"><i class="fa fa-search"></i></span>
				 </div>
			    </div>
				<div class="col-sm-12 table-responsive no-padding">
				 <div class="select-list" style="max-height:400px; overflow:auto;">
					<table class="table table-bordered table-hover" style="font-size:small;">
						<thead>
							<tr class="success">
								<th>StudentNo.</th>
                                <th>Name</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody id="osubjIC">
							<tr>
								<td colspan=3><center>No Data Available.</center></td>
							</tr>							
						</tbody>
					</table>
                 </div>					
				</div>
			  </div>
			 </div>
			 <div class="modal-footer xmargin-top-5">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button id = "btnselectitem" type="button" class="btn btn-primary" disabled="disabled">Select</button>						
			 </div>		  
		</div>
	 </div>
  </div>
  <!--
  -->