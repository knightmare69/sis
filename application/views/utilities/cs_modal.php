<!-- Create Modal -->
  <div class="modal fade" id="mwindow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	 <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			 <h4 class="modal-title" id="lblmwindow">Create New : <?php echo $module; ?></h4>
		  </div>	     
          <?php echo form_open('utilities/civilstatus',array('class' => 'form-horizontal','id'=>'religion')); ?>                      
			 <div class="modal-body">
				  <?php $_SESSION['actionid'] = mt_rand(1, 1000); ?>
				  <input type='hidden' name='actionid' id='actionid' value='<?php echo $_SESSION["actionid"] ?>'/>
				  <input type='hidden' name='tid' id='tid' value='0'/>
				  <div class="form-group">
					 <label for="name" class="col-sm-3 control-label">Name</label>
					 <div class="col-sm-4"><input type="text" class="form-control" id="name" name="name" placeholder="(e.g. Jews)" required></div>
					 <div class="col-sm-4">
                  <div class="checkbox">							 								
                     <label>
                       <input type="checkbox" name="chkinactive" id="chkinactive" onclick="setInactive()"> In-active
                       <input type='hidden' name='inactive' id='inactive' value='0'/>
                     </label>								
                   </div>
					 </div>
				  </div>
				  <div class="form-group">
					 <label for="short" class="col-sm-3 control-label">Short Name</label>
					 <div class="col-sm-4">
					  <input type="text" class="form-control" id="short" name="short" placeholder="short name" required>								  
					 </div>							 								  
				  </div>
			 </div>
			 <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" value="submit" name="submit" >Save changes</button>
			 </div>
		  </form>
		</div>
	 </div>
  </div>
  