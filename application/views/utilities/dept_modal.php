
  <!-- ModalCreateEdit -->
    <div class="modal fade" id="mwindow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="MCETitle">Create New : Department</h4>
	  </div>
	  
	  <?php echo form_open('utilities/department/save',array('class' => 'form-horizontal','id'=>'department')); ?>
     
	    <div class="modal-body">
	      
	      <?php $_SESSION['actionid'] = mt_rand(1, 1000); ?>
	      
	      <input type='hidden' name='actionid' id='actionid' value='<?php echo $_SESSION["actionid"] ?>'/>
	      <input type='hidden' name='editid' id='editid' value=''/>
	      <input type='hidden' name='selectedid' id='selectedid' value=''/>
		
	     <div class="form-group">
			 <label for="lblcode" class="col-sm-2 control-label">Code</label>		  
			 <div class="col-sm-6">
			   <input type="text" class="form-control" id="code" name="code" placeholder="(e.g. Cas)" required>
			 </div>
	     </div>
	     <div class="form-group">
			 <label for="lblname" class="col-sm-2 control-label">Name</label>
			 <div class="col-sm-6">
				<input type="text" class="form-control" id="name" name="name" placeholder="(e.g. Cashier)" required>
			 </div>
	     </div>
	      
	     <div class="form-group">
			 <label for="lblcampus" class="col-sm-2 control-label">Campus</label>		  
			 <div class="col-sm-6">
		  <?php
      
           echo "<select class='form-control' id='campusid' name='campusid' required>";
		
            foreach ($rscampus as $rscampus){
                  echo "<option value='". $rscampus->CampusID ."'>". $rscampus->ShortName ."</option>";
            }
            
            echo "</select>";
		  ?>
		</div>
	      </div>
				  		
	    </div>
	    
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default" id="cmdClose" data-dismiss="modal">Close</button>
	      <button type="submit" class="btn btn-primary" id="cmdSave" value="cmdSave" name="cmdSave" >Save</button>
	    </div>
	    
	  </form>
	  
	</div>
      </div>
    </div>
	 
	   <!-- ModalDelete -->
  <div id="ModalDelete" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">	 
      <div class="modal-content">
	
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	  <h4 class="modal-title">Remove Department</h4>
	</div>
	
	<?php $_SESSION['remid'] = mt_rand(1, 1000); ?>
	
	<div class="modal-body">
	  <p>Do you want to remove the record?</p>
	</div>
	
	<?php echo form_open('utilities/department/delete',array('class' => 'form-horizontal','id'=>'department')); ?>
	
	  <input type='hidden' name='remid' id='remid' value='<?php echo $_SESSION["remid"] ?>'/>
	  <input type='hidden' name='tindex' id='tindex' value='0'/>
	  
	  <div class="modal-footer">
	    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">No</button>
	    <button type="submit" class="btn btn-primary btn-sm" value="remove" name="remove" >Yes</button>
	  </div>
	  
	</form>
	 
      </div>
    </div>
  </div>
  