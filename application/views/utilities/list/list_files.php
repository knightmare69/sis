<?php
 $dirs='';
 $files='';
 foreach($directory as $k => $v)
 {
  if(array_key_exists('sub',$v) && $v['sub']==true)
  {	  
   if($v['path']=='')
	$dirs.='<tr data-path="'.$v['path'].'" data-name="'.$v['name'].'" data-sub="1"><td><i class="fa fa-lock txt-color-yellow"></i> '.$v['name'].'</td><td>'.$v['size'].'</td><td>'.$v['datemod'].'</td></tr>';	 
   else
	$dirs.='<tr data-path="'.$v['path'].'" data-name="'.$v['name'].'" data-sub="1"><td><i class="fa fa-folder txt-color-yellow"></i> '.$v['name'].'</td><td>'.$v['size'].'</td><td>'.$v['datemod'].'</td></tr>';	 
  }
  else
  {
   $icon = 'fa-file-o';
   $editable = true;
   if(strpos($v['name'],'.rar')>0 || strpos($v['name'],'.zip')>0)
   {
    $icon = 'fa-th-list txt-color-purple';
    $editable = false;	   
   }
   else if(strpos($v['name'],'.jpeg')>0 || strpos($v['name'],'.jpg')>0 || strpos($v['name'],'.png')>0 || strpos($v['name'],'.gif')>0 || strpos($v['name'],'.bmp')>0)
   {
    $icon = 'fa-file-image-o';
    $editable = false;	   
   }
   
   $files.='<tr data-editable="'.$editable.'" data-path="'.$v['path'].'" data-name="'.$v['name'].'" data-sub="0"><td><i class="fa '.$icon.'"></i> '.$v['name'].'</td><td>'.$v['size'].'</td><td>'.$v['datemod'].'</td></tr>';	  
  }
 }
 echo $dirs.$files;
?>