<table class="table table-hover table-bordered">
<thead>
<tr>
 <th>#</th>
 <th>StudentNo</th>
 <th>FullName</th>
</tr>
</thead>
<tbody>
<?php
$i = 1;
if(count($list)<=0)
 echo '<tr colspan="3"><center></center></tr>';	
else
{	
 foreach($list as $rs)
 {
  echo '<tr id="'.$rs->StudentNo.'">
         <td>'.$i.'</td>
         <td class="studno">'.$rs->StudentNo.'</td>
         <td class="studname">'.$rs->LastName.(($rs->LastName!= $rs->FirstName)?', '.$rs->FirstName:'').(($rs->LastName!=$rs->FirstName && $rs->LastName!='' && $rs->FirstName!='')?' '.$rs->MiddleName:'').'</td>
        </tr>';	
  $i++;		
 }
}
?>
</tbody>
</table>