<table class="table table-hover table-bordered" style="white-space:nowrap;">
<thead>
<tr>
 <th>#</th>
 <th>UserName</th>
 <th>Email</th>
 <th>FullName</th>
 <th>IDNo</th>
 <th>IDType</th>
</tr>
</thead>
<tbody>
<?php
$i = 1;
if(count($list)<=0)
 echo '<tr colspan="3"><center></center></tr>';	
else
{	
 foreach($list as $rs)
 {
  $userid   = ((property_exists($rs,'UserID'))?($rs->UserID):'');
  $username = ((property_exists($rs,'UserName'))?($rs->UserName):'');
  $email    = ((property_exists($rs,'Email'))?($rs->Email):'');
  $idno     = ((property_exists($rs,'IDNo'))?($rs->IDNo):'');
  $idtype   = ((property_exists($rs,'IDType'))?($rs->IDType):0);
  if($idtype=='-1')
   $idtypename='Admin';
  else if($idtype=='1')
   $idtypename='Student';
  else if($idtype=='2')
   $idtypename='Parent';
  else if($idtype=='3')
   $idtypename='Faculty';
  else
   $idtypename='Undefined';
	  
  echo '<tr id="'.$userid.'">
         <td>'.$i.'</td>
         <td class="username">'.$username.'</td>
         <td class="email">'.$email.'</td>
         <td class="fullname">'.$rs->LastName.(($rs->LastName!= $rs->FirstName)?', '.$rs->FirstName:'').(($rs->LastName!=$rs->FirstName && $rs->LastName!='' && $rs->FirstName!='')?' '.$rs->MiddleName:'').'</td>
         <td class="idno">'.$idno.'</td>
         <td class="idtype" data="'.$idtype.'">'.$idtypename.'</td>
        </tr>';	
  $i++;		
 }
}
?>
</tbody>
</table>