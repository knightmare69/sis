
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> <i class="fa fa-home"></i> Dashboard</a></li>
		
	</ol>
</div>   
<div id="content">	 
	 <div class="row">
		<section id="widget-grid" class="">		  
		  <?php
		    if($userinfo['idtype']==1)
			{
			  echo '<article class="col-sm-12 col-md-6 col-lg-8">';	
			  echo $this->load->view('widgets/academicinfo','',true);
			  echo '</article>';
			  echo '<article class="col-sm-12 col-md-6 col-lg-4">';	
			  echo $this->load->view('widgets/evaldonut','',true);
			  echo $this->load->view('widgets/curryrlvl','',true);
			  echo '</article>';
		    }
		    else if($userinfo['idtype']==2)// Parent
		    {
		
		    }
		    else if($userinfo['idtype']==3)// Faculty
		    {
		
		    }
		    else if($userinfo['idtype']==-1)// Administratives
		    {
			 echo '<article class="col-sm-12 col-md-6 col-lg-6 hidden">';	
			 echo '</article>';
             echo '<article class="col-sm-12 col-md-6 col-lg-6">';
		     echo $this->load->view('widgets/systemstats','',true);
		     echo $this->load->view('widgets/enrollstats','',true);
		     echo $this->load->view('widgets/gradeencoding','',true);
			 echo '</article>';
		    }
		    else
		    {
		      echo '<article class="col-sm-12 col-md-12 col-lg-6">';
		      echo $this->load->view('widgets/sischat');
		      echo '</article>';
		    }		
		  ?>
		  <!--
		  <article class="col-sm-12 col-md-12 col-lg-6">
			<?php //$this->load->view('include/sischat');?>
			</article>
		  -->
		  <article class="col-sm-12 col-md-12 col-lg-6">
		  	<?php //$this->load->view('include/events');?> 
		  </article>
		</section>		
	 </div>
	 <!-- /.row -->
  </div>
   