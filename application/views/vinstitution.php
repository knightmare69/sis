
	
	
  <div id="page-wrapper">
	 

	 <div class="row">
		<div class="col-lg-12">
		  <h1><?php echo $nheader;?></h1>
		  <ol class="breadcrumb">
			 <li><a href="<?php echo site_url('home'); ?>"><i class="fa fa-cog fa-fw"></i> Module Setup</a></li>
			 <li><a href="<?php echo site_url('institution'); ?>"> Academic Institution</a></li>
			 <li class="active"><i class="fa fa-table"></i> <?php echo $nheader;?></li>
		  </ol>
		  <div class="alert alert-info alert-dismissable hide">
			 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			 We're using <a class="alert-link" href="http://tablesorter.com/docs/">Tablesorter 2.0</a> for the sort function on the tables. Read the documentation for more customization options or feel free to use something else!
		  </div>
		</div>
	 </div><!-- /.row -->
	 
    <div class="row">
      <div class="col-xs-3">
	<div class="list-group">
	 
	 <?php
		foreach ($menus as $menu) {						
			 echo "<a href='" . site_url('institution/' .  $menu->key ) . "' class='list-group-item ". ($nactive == $menu->key ? "active" : "") ."'>". $menu->caption ."</a>" ;						
		}		 
	?>
  
	</div>      
      </div>
      <div class="col-md-9">
	<div class="panel panel-info" >
	  <div class="panel-heading"> <h3 class="panel-title"><?php echo $nheader;?></h3> </div>
	      <div class="panel-body">
          
         </div>
	  </div>
      </div>
    </div>
  </div>
