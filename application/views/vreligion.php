	
	
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <div class="list-group">
			 <a href="<?php echo site_url('utilities/department'); ?>" class="list-group-item">Department</a>
				<a href="<?php echo site_url('utilities/position'); ?>" class="list-group-item">Position Title</a>
				<a href="<?php echo site_url('utilities/civilstatus'); ?>" class="list-group-item">Civil Status</a>
				<a href="<?php echo site_url('utilities/nationality'); ?>" class="list-group-item">Nationality</a>
				<a href="<?php echo site_url('utilities/religion'); ?>" class="list-group-item">Religion</a>
				<a href="<?php echo site_url('utilities/otherschool'); ?>" class="list-group-item">Other School</a>
				<a href="<?php echo site_url('utilities/building'); ?>" class="list-group-item">Building</a>
				<a href="<?php echo site_url('utilities/rooms'); ?>" class="list-group-item">Rooms</a>
        </div>      
      </div>
      <div class="col-md-10">
        <div class="panel panel-info" >
          <div class="panel-heading"> <h3 class="panel-title">Religion</h3>
          	<div style="float:right; margin-top: -17px;">
				  <a href="#" onclick='newrecord(this)' ><span class="glyphicon glyphicon-plus"></span></a> &nbsp;
				  <a href="<?php echo $_SERVER['PHP_SELF'] ?>"><span class="glyphicon glyphicon-refresh"></span></a> 
				</div>
                     
          </div>
          <div class="panel-body">
            <p>This module allows you to add, edit delete the system table for religion</p>
            <div class="bs-example">
				<table id="tbl_religion" class="table table-hover" style="cursor: pointer;">
					 <thead>
						<tr>
						  <th>#</th>
						  <th class='col-xs-1'>Ref.ID</th>						  
						  <th>Religion</th>
						  <th>Short Name</th>
						  <th>In-active</th>
						  <th>Option</th>
						</tr>
					 </thead>
					 <tbody>
                  <?php
                  $i = 1;
                  foreach($religion as $religion)
                  {
                    echo "<tr onclick='selectrow(this)' ondblclick='editrecord(this)'>";
                    echo "<td>" . $i. "</td>";
                    echo "<td>" . $religion->IndexID . "</td>";
                    echo "<td>" . $religion->Religion . "</td>";
                    echo "<td>" . $religion->ShortName . "</td>";            
                    if ($religion->Inactive == 0){
                      echo "<td><input id='chk". $i ."' type='checkbox' value='". $religion->Inactive ."' disabled='disabled'></td>";  
                    }else {
                      echo "<td><input id='chk". $i ."' type='checkbox' value='". $religion->Inactive ."' disabled='disabled' Checked ></td>";  
                    }								
                    echo "<td><a id='e" . $i .  "' href='#' onclick='editopt_onclick(this)'><span class='glyphicon glyphicon-pencil'></a> &nbsp;";
                    echo "<a id='r" . $religion->IndexID .  "' href='#' onclick='deleterecord(this)'><span class='glyphicon glyphicon-remove-sign'></a></td>";
                    echo "</tr>";
                        
                    $i=$i+1;
                  }
                  ?>
                  

																		
					 </tbody>
				  </table>
				</div>
                      
         </div>
        </div>
      </div>
    </div>
  </div>
  
  
<!-- Delete Modal -->
<div id="mymodal" class="modal fade bs-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">	 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Remove Religion</h4>
		</div>
		<?php $_SESSION['remid'] = mt_rand(1, 1000); ?>
		<div class="modal-body">
		  <p>Do you want to remove the record?</p>
		</div>;
		
      
      <?php echo form_open('religion/delete'); ?>
      
		<input type='hidden' name='remid' id='remid' value='<?php echo $_SESSION["remid"] ?>'/>
		<input type='hidden' name='tindex' id='tindex' value='0'/>
		<div class="modal-footer">
		  <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">No</button>
		  <button type="submit" class="btn btn-primary btn-sm" value="remove" name="remove" >Yes</button>
		</div>
	   
        
		</form>
	 
    </div>
  </div>
</div>

  <!-- Create Modal -->
  <div class="modal fade" id="mwindow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	 <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			 <h4 class="modal-title" id="lblmwindow">Create New : Religion</h4>
		  </div>	     
        <?php echo form_open('utilities/religion',array('class' => 'form-horizontal','id'=>'religion')); ?>                      
			 <div class="modal-body">
				  <?php $_SESSION['actionid'] = mt_rand(1, 1000); ?>
				  <input type='hidden' name='actionid' id='actionid' value='<?php echo $_SESSION["actionid"] ?>'/>
				  <input type='hidden' name='tid' id='tid' value='0'/>
				  <div class="form-group">
					 <label for="name" class="col-sm-3 control-label">Religion Name</label>
					 <div class="col-sm-4">
					  <input type="text" class="form-control" id="name" name="name" placeholder="(e.g. Jews)" required>
					 </div>
					 <div class="col-sm-4">
					 <div class="checkbox">							 								
						 <label>
							<input type="checkbox" name="chkinactive" id="chkinactive" onclick="setInactive()"> In-active
							<input type='hidden' name='inactive' id='inactive' value='0'/>
						 </label>								
					  </div>
					 </div>
				  </div>
				  <div class="form-group">
					 <label for="short" class="col-sm-3 control-label">Short Name</label>
					 <div class="col-sm-4">
					  <input type="text" class="form-control" id="short" name="short" placeholder="short name" required>								  
					 </div>							 								  
				  </div>
			 </div>
			 <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" value="submit" name="submit" >Save changes</button>
			 </div>
		  </form>
		</div>
	 </div>
  </div>
  
		  
	  
     
