<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><?php echo $title; ?></li>
	</ol>
</div>   
<div id="content">	 
	 <div class="row">
		<div class="col-lg-12">
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-clipboard"></i> <!--?php echo $title; ?> <span>--> Subscribe to Student<!--/span--></h1>
                        </div>
		</div>
                <div class="col-lg-12">
			<div class="alert alert-info fade in">
				<!--button class="close" data-dismiss="alert">
							�
				</button>
				<i class="fa-fw fa fa-info"></i-->
				<strong>Note!</strong> Please fill up all the necessary information. If student is scholarship, please provide Validation Date otherwise the O.R No of the last payment posted on the Official Receipt
			</div>

                        <div class="well">
                        <div class="row">
                                
                        <form class="form-horizontal" role="form" id="studentsearch" name="studentsearch" autocomplete="off" method="post">
                                <div>
                                        <div class="form-group">
						<label for="term" class="col-sm-2 control-label">Student No</label>  
						<div class="col-sm-3">
                                                        <input id="studentno" name="studentno" class="form-control" type="text" maxlength="10" placeholder="ex. 2001100102" value="<?php echo $studentno; ?>" required>
						</div>
                                                <button type="submit" class="btn btn-primary" id="cmdDisplay" onclick="displaystudentinfo()" value="cmdDisplay" name="cmdDisplay">Display Student Information</button>
					</div>
                                </div>
                        </form>
                                
                        <?php echo form_open('Subscribe/create',array('class' => 'form-horizontal','id'=>'subscribe_validationinfo')); ?>
                                <div>
					<input type='hidden' name='hiddenstudentno' id='hiddenstudentno' value='<?php echo $studentno; ?>'/>
					<input type='hidden' name='userid' id='userid' value='<?php echo $userid; ?>'/>
                                        <div class="form-group">
						<label for="term" class="col-sm-2 control-label">Student Name</label>  
						<div class="col-sm-8">
                                                        <label for="studentname" name="studentname" id="studentname" class="col-sm-8 control-label" style="text-align: left;"><strong><?php echo $name; ?></strong></label>
                                                </div>	         
					</div>
                                                
                                        <div class="form-group">
						<label for="term" class="col-sm-2 control-label">Degree</label>  
						<div class="col-sm-8">
                                                        <label for="term" class="col-sm-8 control-label" style="text-align: left;"><i><?php echo $degree; ?></i></label>
						</div>	         
					</div>
                                                
                                        <div class="form-group">
						<label for="term" class="col-sm-2 control-label">Student Date of Birth</label>  
						<div class="col-sm-3">
                                                        <input id="dateofbirth" name="dateofbirth" class="form-control datepicker" data-dateformat='mm/dd/yy' type="text" autoccomplete="off" placeholder="ex. 08/23/1982" value="" data-mask="99/99/9999" data-mask-placeholder= "-" required>
						</div>	         
					</div>				
					
					<div class="form-group">
						<label for="term" class="col-sm-2 control-label">AY Term</label>  
						<div class="col-sm-3">
                            <select class='form-control input-sm' id='termid' name='termid' required>                     
								<?php
									foreach ($ayterm as $ayterm)
									{
									  echo "<option value='". $ayterm->TermID ."'>". $ayterm->AcademicYear . "  " . $ayterm->SchoolTerm . "</option>";
									}
								?>
							</select>
						</div>	         
					</div>
                                                
                                        <div class="form-group">
						<label for="term" class="col-sm-2 control-label">Registration ID</label>  
						<div class="col-sm-3">
                                                        <input id="registrationid" name="registrationid" class="form-control" type="text" autocomplete="off" placeholder="" value="" required>
						</div>	         
					</div>
					
					<div class="form-group">
						<label for="term" class="col-sm-2 control-label">Student Last Payment Posted Official Receipt</label>  
						<div class="col-sm-3">
                                                        <input id="receipt" name="receipt" class="form-control" type="text" autocomplete="off" placeholder="" value="">
						</div>	         
					</div>
					
					<div class="form-group">
						<label for="term" class="col-sm-2 control-label">Student Last Payment Posted Official Date</label>  
						<div class="col-sm-3">
							<input type="text" id="date"  name="date" placeholder="ex. 10/26/1990" class="form-control datepicker" data-dateformat='mm/dd/yy' data-mask="99/99/9999" data-mask-placeholder= "-">	
						</div>	         
					</div>
					                                        
                                        <div class="form-group">
						<label for="term" class="col-sm-2 control-label">Relationship</label>  
						<div class="col-sm-3">
                                                        <select class='form-control input-sm' id='relationship' name='relationship' required>                     
								<option value="Family">Family</option>
                                                                <option value="Guardian">Guardian</option>
                                                                <option value="Parent">Parent</option>
							</select>
						</div>	         
					</div>
                                        
                                        <div class="form-group">
                                                <div class="col-sm-offset-2">
                                                        <button type="button" class="btn btn-primary" id="cmdSubcribe" name="cmdSave" onclick="submitform()" >Subscribe</button>
                                                        <button type="button" class="btn btn-primary" id="cmdCancel" name="cmdCancel">Cancel</button>
                                                </div>
                                        </div>
                                </div>
                                
                        </form>
                        </div>
			</div>
                </div>
	 </div>
  </div>
