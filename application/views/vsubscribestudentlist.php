
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><?php echo $title; ?></li>
	</ol>
</div>   
<div id="content">	 
	 <div class="row">
		<div class="col-lg-12">
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                                <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-clipboard"></i> <!--?php echo $title; ?> <span>--> List of Student<!--/span--></h1>
                        </div>
		</div>
		
		<div class="col-lg-12">
                <div class="well">

		<?php echo form_open('SubscribeStudentList/unsubscribe',array('class' => 'form-horizontal','id'=>'subscribestudentlist_unsubscribe')); ?>
			<input type='hidden' name='indexid' id='indexid' value=''/>
                </form>
	 
		<div class="row">      
			<div class="col-md-12">
				<div class="bs-example">			 
					<div class="table-responsive">
				
				<table id="dt_studentlist" class="table table-hover" cellspacing="0" width="100%">
					<thead>
						<tr style="text-align: center;">						
							<th>Student No</th>						
							<th>Name</th>
							<th>Degree</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach($student as $student)
							{
								echo "<tr>";
								echo "<td>" . $student->StudentNo . "</td>";						
								echo "<td>" . $student->StudentName . "</td>";
								echo "<td>" . $student->ProgName . "</td>";
								echo "<td align='center'><input id='e" . $student->IndexID . "' class='btn btn-primary btn-sm' type='button' onclick='unsubscribe(" . $student->IndexID . ")' value = 'Unsubscribe'></td>";
								echo "</tr>"; 
							}
						?>							 
					</tbody>
				</table>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
	 </div>
  </div>