
	
	
  <div id="page-wrapper">  
	 <div class="row">
		<div class="col-lg-12">
		  <h1><?php echo $module;?></h1>
		  <ol class="breadcrumb">
			 <li><a href="<?php echo site_url('home'); ?>"><i class="fa fa-wrench fa-fw"></i> Utilites</a></li>
			 <li class="active"><i class="fa fa-table"></i> <?php echo $module;?></li>
		  </ol>
		  <div class="alert alert-info alert-dismissable hide">
			 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			 We're using <a class="alert-link" href="http://tablesorter.com/docs/">Tablesorter 2.0</a> for the sort function on the tables. Read the documentation for more customization options or feel free to use something else!
		  </div>
		</div>
	 </div><!-- /.row -->
		  
    <div class="row">
      <div class="col-md-12">
		  <div class="panel panel-info" >
			 <div class="panel-heading"> <h3 class="panel-title"><?php echo $module;?></h3>
			   <div style="float:right; margin-top: -17px;">
				  <a href="#" onclick='newrecord(this)' ><span class="glyphicon glyphicon-plus"></span></a> &nbsp;
				  <a href="<?php echo $_SERVER['PHP_SELF'] ?>"><span class="glyphicon glyphicon-refresh"></span></a> 
				</div>
			 </div>
			 <div class="panel-body">
				<p>This module allows you to add, edit delete the system table for <?php echo $module;?></p>
				<div class="bs-example">
					<table id="tbldata" class="table table-hover" style="cursor: pointer;">
					<thead>
						<tr>
							<th class='col-xs-1'>#</th>
							<th class='col-xs-1'>Option</th>
						    <?php 
								$col=0; 	
								//$key[]= array();
							  	foreach ($fields->list_fields() as $field){
	   								if ($field<>'ID') { echo "<th>". $field ."</th>" ; }
									$key[] = $field;
									$col++;
								}
							  ?>
						  
						</tr>
					</thead>
					<tbody>
					   <?php
		                  $i = 1;
		                  foreach($recordset as $rs)
		                  {
		                    echo "<tr onclick='selectrow(this)' ondblclick='editrecord(this)'>";
							echo "<td class='col-xs-1' >" . $i. "</td>";
							echo "<td><a id='e" . $i .  "' href='#' onclick='editopt_onclick(this)'><span class='glyphicon glyphicon-pencil'></a> &nbsp;";
							echo "<a id='r" . $rs->ID .  "' href='#' onclick='deleterecord(this)'><span class='glyphicon glyphicon-remove-sign'></a></td>";		                    
								foreach ($key as $k => $value){
							   		//echo $k . ' ' . $value . '<br>';												  
									if($k>0){
										echo "<td>" . $rs->$value . "</td>";		                    			
									}
									
								}								                    	 		                    
		                    echo "</tr>";
		                        
		                    $i=$i+1;
		                  }
		                  ?>									
					</tbody>
					</table>
				</div>
			 </div>
		  </div>
      </div>
    </div>
  </div>
