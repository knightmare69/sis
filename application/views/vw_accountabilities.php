
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Accountabilities</li>
	</ol>
</div>   
    <div id="content">	 
	 <div class="row">
	    <div class="col-sm-12">
        <?php 
	     if($idtype=='-1') 
		  echo $this->load->view('include\studentinfo','',true);
	     else if($idtype=='1')
		  echo '<input type="hidden" id="studentno" value="'.$studentno.'"/>';
	     else if($idtype=='2') 
		  echo $this->load->view('include\studentinfo','',true);
	    ?>
	    </div>
        <div class="col-sm-12 xpadding-5">
		 <span class="pull-right">
		  <label>Uncleared Only: <input type="checkbox" class="unclearonly" checked="true"></label>
		 </span>
        </div>        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
         <table  id="dt_basic"  class="table table-striped table-bordered table-hover" >
		  <thead>
		   <tr class="odd gradeX">
            <th>Reference No</th>
            <th>Academic Year and Term</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Date Entered</th>
            <th>Date Update</th>
		   </tr>
		  </thead>
	      <tbody class="tbl-accounts">
		   <?php echo $this->load->view('vw_accountabilities_details','',true);?>
		   </tbody>
          </table>
        </div>
	</div>
   </div><!-- /.row -->
  </div>
                
                
    
		
