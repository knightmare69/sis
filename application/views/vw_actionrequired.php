<div id="ribbon" style="margin-top:50px;">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>" onclick='javascript.void(0);'> Action Required</a></li>
	</ol>
</div>   
<div id="content" style="max-height:90%;overflow:auto;">	 
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
         <div class="jarviswidget jarviswidget-color-blue" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">		 
			 <header role="heading">
			  <h2>
			   <span class="fa fa-user"></span>
			   Action Required
			  </h2>
			 </header>
			 <div role="content" class="jarviswidget-body">
			  <div class="widget-body">
			  <div class="row">
              <?php			  
			  if(isset($actionform) and $actionform!='')
			  {
			  echo '<div class="alert alert-warning">
			         <p><i class="fa fa-warning"></i> <strong>Please</strong> complete this form. The Administrators wants you to change or complete the following data.</p>
			        </div>';
			  echo '<form id="xactionreq" name="xactionreq" method="post" action="'.site_url('actionrequired/process').'" onsubmit="return checkfields();">';
			  echo '<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			         '.$actionform.' 
			        </div>
                    <div class="col-sm-12"> 					
					 <div class="pull-right">
					  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
					 </div>
					</div>';
			  echo '</form>';		
			  }
			  else
			  {
			   echo "<p>No Data Available.</p>";
			  }
			  ?>
			  </div>
              </div>			  
            </div>
          </div>			 
		</div>
	</div>
	
</div>