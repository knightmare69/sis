
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Portal Activation</li>
	</ol>
</div>   
<div id="content">	 
	 <div class="row">
		<div class="col-lg-12">
		  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			 <h1 class="page-title txt-color-blueDark hidden"><i class="fa-fw fa fa-flash"></i> <?php echo $title; ?> <span>> Portal Activation</span></h1>
		  </div>
		  <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">

		  </div>
		</div>
	 </div><!-- /.row -->
     <div class="row padding">
	  <div class="alert alert-info fade in <?php echo ((isset($target) && $target!='0' && $target>0 && $target!='')?'hidden '.$target:$target);?>">
       <i class="fa-fw fa fa-lg fa-info"></i><strong>Select a tab of portal to activate based on your user type.</strong>
	   <?php
	   if(ACTIVE_ADMISSION==1)
	    echo '<p style="text-indent:25px;">  -If you are a new enrollee/transferee, select "NEW ENROLLEE/TRANSFEREE".</p>';
	   
	   if(ACTIVE_STUDENT==1)
	    echo '<p style="text-indent:25px;">  -If you are a student, select "STUDENT".</p>';
		
	   if(ACTIVE_PARENT==1)
	    echo '<p style="text-indent:25px;">  -If you are a parent of a student, select "PARENT".</p>';
		
	   if(ACTIVE_FACULTY==1)
	    echo '<p style="text-indent:25px;">  -If your already a member of the faculty, select "Faculty".</p>';
       ?>
	  </div>
     </div>	 
	 <div class="row">		
		<article class="col-sm-12 col-md-12 col-lg-12">			
			<div class="jarviswidget well" id="wid-id-1" data-widget-colorbutton="true" data-widget-editbutton="false" data-widget-togglebutton="true" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">				
				<header>
					<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
					<h2>Please Select Portal </h2>
				</header>				
				<div>					
					<div class="jarviswidget-editbox"></div>
					<div class="widget-body">						
						
						<ul id="myTab1" class="nav nav-tabs bordered">
						    <?php
						    $s1 = '';
							$s2 = '';
							$s3 = '';
							$s4 = '';
							$s5 = '';
							
							if(isset($postdata)){
							 if($postdata['pageid']==1)
							 {$s1='active';}
							 if($postdata['pageid']==2)
							 {$s2='active';}
							 if($postdata['pageid']==3)
							 {$s3='active';}
							 if($postdata['pageid']==4)
							 {$s4='active';}
							 if($postdata['pageid']==5)
							 {$s5='active';}
							}else{
							 $s1='active';
							}
							
						    switch($target){
								case '1':
								   $s1 = 'hidden';
								   $s2 = 'active';
								   $s3 = 'hidden';
								   $s4 = 'hidden';
								   $s5 = 'hidden';
								break;
								case '2':
								   $s1 = 'hidden';
								   $s2 = 'hidden';
								   $s3 = 'active';
								   $s4 = 'hidden';
								   $s5 = 'hidden';
								break;
							}
							/**/
							if($xact_admission==1)
							 echo '<li class="'.((isset($s1))?$s1:'').'"><a href="#s1" data-toggle="tab" class="label bg-color-blue txt-color-white" style="font-size:16px !important;"><i class="fa fa-fw fa-lg fa-user"></i> New Enrollee/ Transferee </a></li>';
							
							if($xact_student==1)
							 echo '<li class="'.((isset($s2))?$s2:'').'"><a href="#s2" data-toggle="tab" class="label bg-color-green txt-color-white" style="font-size:16px !important;"><i class="fa fa-fw fa-lg fa-user"></i> Student </a></li>';
							
							if($xact_parent==1)
							 echo '<li class="'.((isset($s3))?$s3:'').'"><a href="#s3" data-toggle="tab"  class="label bg-color-yellow txt-color-white" style="font-size:16px !important;"><i class="fa fa-fw fa-lg fa-user"></i> Parent </a></li>';
							
							if($xact_faculty==1)
							 echo '<li class="'.((isset($s4))?$s4:'').'"><a href="#s4" data-toggle="tab"><i class="fa fa-fw fa-lg fa-user"></i> Faculty </a></li>';
							
							if($xact_admin==1)
							 echo '<li class="'.((isset($s5))?$s5:'').'"><a href="#s5" data-toggle="tab"><i class="fa fa-fw fa-lg fa-user"></i> Administrative </a></li>';
							
							?>
							
						</ul>

						<div id="myTabContent1" class="tab-content padding-10">
							<?php 
							if(isset($xact_admission) && $xact_admission==1)
							{
							?>
							<div class="tab-pane fade in <?php if(isset($s1)){echo $s1;}?>" id="s1">
							    <div class="row">
									<div class="col-sm-12">										
										<div class="well">
											<button class="close" data-dismiss="alert">x</button>
											<h1 class="semi-bold">Are you a New Enrollee/Applicant/Transferee?</h1>
											<p>click a branch to proceed.</p>
											<div class="text-center" >
											  <h3> <a class="btn btn-lg btn-danger welcome_campus" data-name="manila"><i class="fa fa-building"></i> MANILA</a>&nbsp;&nbsp;&nbsp;&nbsp;
											  <a class="btn btn-lg btn-danger welcome_campus" data-name="taytay"><i class="fa fa-building"></i> RIZAL</a> </h3>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
								   <?php //echo $this->load->view('vw_welcome','',true);?>
								</div>
								<!--
								<br>
								<h1>Admission Instructions</h1>
								<p>
									<h2><strong>College Freshmen / Transferees</strong></h2>
									<ol>
										<li>Accomplish application form and submit to the Admissions Office together with the complete documents. <a href="#">(Please refer to list of requirements)</a>.</li>
										<li>Pay the admission fee (P500.00) at the Cashier, Finance Department.</li>
										<li>Admission's staff will issue test permit.</li>
										<li>Take the entrance examination at the Testing Center (College Building)
										 on the scheduled date and present test permit. <br>
											<small><strong>Note: Test rescheduling will be allowed only once.</strong></small>
										</li>
										<li>Results of entrance test and other information will be posted on the school's
										 website, or inquire personally at the Admissions Office.</li>
										<li>If <b>qualified</b>, present test result at the Cashier, Finance Department,
										 and pay the medical fee of <b>P500.00</b></li>
										<li>Present receipt to Admissions Office for schedule of medical.</li>
										<li>Proceed to the College of Arts and Sciences Medical Clinic (College Building)
										 for medical examination on the date specified.
										 <br>
										 <small><b>Note: Applicant will be notified if there is a problem with his/her medical result
													 A letter is required for those who will be requesting for medical examination
													 rescheduling.</b></small>
										</li>
										<li>Submit Medical Clearance to the Admissions Office and get student number.</li>
										<li>Take note of enrollment dates and prepare requirements for enrollment.</li>
									 </ol>
									
								</p>
								<hr>
								<p>
									<h2><strong>Foreign Applicants</strong></h2>
									<ol>
										<li>Accomplish application form and submit to the Admissions Office together with the complete documents. <a href="#">(Please refer to list of requirements)</a>.</li>
										<li>Pay the admission fee <b>($50.00)</b> at the Cashier, Finance Department.</li>
										<li>Admission's staff will issue test permit.</li>
										<li>Take the entrance examination at the Testing Center (College Building)
										 on the scheduled date and present test permit. <br>
											<small><strong>Note: Test rescheduling will be allowed only once.</strong></small>
										</li>
										<li>Results of entrance test and other information will be posted on the school's
										 website, or inquire personally at the Admissions Office.</li>
										<li>If <b>qualified</b>, present test result at the Cashier, Finance Department,
										 and pay the medical fee of <b>$50.00</b></li>
										<li>Present receipt to Admissions Office for schedule of medical.</li>
										<li>Proceed to the College of Arts and Sciences Medical Clinic (College Building)
										 for medical examination on the date specified.
										 <br>
										 <small><b>Note: Applicant will be notified if there is a problem with his/her medical result
													 A letter is required for those who will be requesting for medical examination
													 rescheduling.</b></small>
										</li>
										<li>Submit Medical Clearance to the Admissions Office and get student number.</li>
										<li>Take note of enrollment dates and prepare requirements for enrollment.</li>
									 </ol>
								</p>
								<section>
									<label class="checkbox">
										<input type="checkbox" name="remember" >
										<i></i>I Understand and follow all the instructions stated above.</label>
								 </section>
								<a  href="<?php echo site_url('admission/applicant'); ?>" title="Applicant" class="btn btn-primary" > Proceed </a>
								-->
							</div>						
							<?php
							}
							
							if(isset($xact_student) && $xact_student==1)
							{
							?>
							<div class="tab-pane fade in <?php if(isset($s2)){echo $s2;}?>" id="s2">
								<div class="row">
									<div class="col-sm-12">										
										<div class="well">
											<button class="close" data-dismiss="alert">x</button>
											<h1 class="semi-bold">Are you a certified <?php echo INST_STUD;?>?</h1>
											<p>
												Use the form below to activate your account. Ensure that all the required fields are properly filled up. 												
											</p>
										
											<p>
												For more information please visit <a href="#ajax/form-templates.html" title="Go to PRISMS FAQs">FAQs</a>  
											</p>
										</div>
									</div>
								</div>
								<br>
								<?php echo form_open('activation/verifystudent',array('id'=>'student_portal','class' => 'smart-form', 'novalidate'=>'novalidate')); ?>								
									<div class="well well-light" >
									<?php
							         if(isset($postdata) && $postdata['pageid']==2)
							         {
									  $userinfo['lastname']= array_key_exists('lname',$postdata)? $postdata['lname'] : '';
									  $userinfo['firstname']=array_key_exists('fname',$postdata)?$postdata['fname'] : '';
									  $userinfo['birthdate']=array_key_exists('bday',$postdata)?$postdata['bday'] : '';
									  $userinfo['gender']=array_key_exists('gender',$postdata)?$postdata['gender'] : '';
									  $userinfo['idno']=array_key_exists('studno',$postdata)?$postdata['studno'] : '';
									  $userinfo['program']=array_key_exists('program',$postdata)?$postdata['program'] : '';
									  $userinfo['ayterm']=array_key_exists('ayterm',$postdata)?$postdata['ayterm'] : '';
									  $userinfo['regno']=array_key_exists('regno',$postdata)?$postdata['regno'] : '';
									  $userinfo['orno']=array_key_exists('orno',$postdata)?$postdata['orno'] : '';
									  $userinfo['ordt']=array_key_exists('ordt',$postdata)?$postdata['ordt'] : '';
							         }
							         ?>
										<br> 
										<h3 style="margin-left: 15px;"><strong> Student </strong> - Activation</h3>																										
										<!-- wizard form starts here -->										
																			
										<fieldset>
											<!-- Basic information -->
											<div class="row hidden">
												<section class="col col-4">
													<label class="input">
														<input type="text" id="lname"  placeholder="Last name" value="<?php if(isset($userinfo['lastname'])) echo $userinfo['lastname']; ?>">
													</label>
												</section>											
												<section class="col col-4">
													<label class="input">
														<input type="text" id="fname" placeholder="First name" value="<?php if(isset($userinfo['firstname']))  echo $userinfo['firstname']; ?>">
													</label>
												</section>
											</div>
											<div class="row">
											    <section class="col col-4">
													<label class="input">
														<input type="text" name="studno" id="studno" placeholder="Student number" value="<?php if(isset($userinfo['idno'])) echo $userinfo['idno']; ?>">
													</label>
												</section>																						
												<section class="col col-8">																	
												 <section class="col col-5 no-padding">
													<label class="input"> <i class="icon-append fa fa-calendar"></i>
														<input type="text" id="bday" name="bday" placeholder="Date of Birth (mm/dd/yyyy)" class="datepicker" data-dateformat='mm/dd/yy' 
                                                                                                                             data-mask="99/99/9999" data-mask-placeholder= "-" value="<?php  if(isset($userinfo['birthdate'])) echo $userinfo['birthdate']; ?>">
													</label>
												 </section>																		
												 <section class="col col-1">
													<label class="text-align-center">OR</label>
												 </section>
												 <section class="col col-5 no-padding">												   
													<label class="input">
														<input type="text" name="orno" id="orno" value="<?php if(isset($userinfo['orno'])){ echo $userinfo['orno'];}?>" placeholder="Official receipt number (blank if scholar)">
													</label>
												 </section>																
												</section>		
											</div>																						
											<div class="row hidden">																								
												<section class="col col-4">
													<label class="select">
														<select name="gender">
															<?php
															$gender ='';
															if(isset($userinfo['gender']))
															{$gender = trim($userinfo['gender']) ;}
															
															echo  '<option value="0" ' . ($gender == ''? 'selected':'') . ' disabled>Gender</option>
																   <option value="M" ' . ($gender == 'M'? 'selected':'') . ' >Male</option>
																   <option value="F" ' . ($gender == 'F'? 'selected':'') . ' >Female</option>' ;
																	 
															?> 
														</select> <i></i> </label>
												</section>
											</div>																						
											<div class="row hidden">
												<section class="col col-4">
													<label class="select">
														<select id="program">
															<option value="0" selected="" disabled=""> - Currently Enrolled Course - </option>
															<?php
																foreach ($program as $p){
																 if(isset($userinfo['program']) && $userinfo['program']==$p->ProgID )
																 {
																 echo "<option value='". $p->ProgID ."' selected='selected'>". $p->ProgCode . ' : ' . $p->ProgName . ($p->Major ==''? '': ' <i>Major in</i> '.$p->Major ) ."</option>";
																 }
																 else
																 {
																 echo "<option value='". $p->ProgID ."'>". $p->ProgCode . ' : ' . $p->ProgName . ($p->Major ==''? '': ' <i>Major in</i> '.$p->Major ) ."</option>";
																 }
																}																		
															?>													
														</select> <i></i>
													</label>													
												</section>
												<section class="col col-4">
													<label class="select">
														<select id="ayterm">
															<option value="0" selected disabled=""> - Semester Enrolled -</option>
															<?php																														
																foreach ($ayterm as $aytermstud){
																 if(isset($userinfo['ayterm']) && $userinfo['ayterm']==$aytermstud->TermID )
																 {
																 echo "<option value='". $aytermstud->TermID ."' selected='selected'>". $aytermstud->AcademicYear . " " . $aytermstud->SchoolTerm. "</option>";
																 }
																 else
																 {
																 echo "<option value='". $aytermstud->TermID ."'>". $aytermstud->AcademicYear . " " . $aytermstud->SchoolTerm. "</option>";
																 }
																}															
															?> 
														</select> <i></i>
													</label>
												</section>												
											</div>											
											<div class="row">												
												<section class="col col-4 hidden">
													<label class="input">
														<!--<input type="text" name="regno" id="regno" placeholder="Please enter your access code from evaluation.">-->
														<input type="text" id="regno" value="<?php if(isset($userinfo['regno'])){ echo $userinfo['regno'];}?>" onblur="checkifscholar(this);" placeholder="Please enter your registration number.">
													</label>
												</section>						
											</div>
											<div class="row hidden">
												<section class="col col-4">
													<label class="input">
														<input type="text" id="ordt" value="<?php if(isset($userinfo['ordt'])){ echo $userinfo['ordt'];}?>" placeholder="Official Receipt Date" data-mask="99/99/9999" data-mask-placeholder= "-">
													</label>
												</section>						
											</div>
											<button class="btn btn-sm btn-primary" value="submit">
													Submit
											</button>	
										</fieldset>
																			
									</div>	
								</form>
							</div>
							<?php
							}
							
							if(isset($xact_parent) && $xact_parent==1)
							{
							?>
							<div class="tab-pane fade in <?php if(isset($s3)){echo $s3;}?>" id="s3">
							   <div class="row">
									<div class="col-sm-12">										
										<div class="well">
											<button class="close" data-dismiss="alert">x</button>
											<h1 class="semi-bold">Are you a <?php echo INST_PAR;?>?</h1>
											<p>Enter the StudentNo of your child and click "Display Student Information" to verify if you input a valid studentno.</p>
											<p>Complete the rest of the form. Finally click the "Activate"</p>
										</div>
									</div>
								</div>
								<br>
	                         <div class="well">
		                      <h3 style="margin-left: 15px;"><strong> Parent </strong> - Activation</h3>
							  <fieldset class="row">
								  <div class="col-sm-12">										
									<form class="form-horizontal" role="form" id="studentsearch" name="studentsearch" autocomplete="off" method="post">
    									<div>
											<div class="form-group">
												<label for="term" class="col-sm-2 control-label">Student No</label>  
												<div class="col-sm-3">
													<input id="studentno" name="studentno" class="form-control" type="text" maxlength="10" placeholder="ex. 2001100102" value="<?php echo $studentno; ?>" required>
												</div>
												<div class="col-sm-6">
												  <button type="button" class="btn btn-primary" id="cmdDisplay" onclick="displaystudentinfo()" value="cmdDisplay" name="cmdDisplay">Display Student Information</button>
												</div>
											</div>
										</div>
									</form>
									
									<?php echo form_open('activation/verifyparents',array('id'=>'parent_portal','class' => 'form-horizontal', 'novalidate'=>'novalidate')); ?>
								
									<div>
										<input type='hidden' name='hiddenstudentno' id='hiddenstudentno' value='<?php echo $studentno; ?>'/>
										<input type='hidden' name='userid' id='userid' value='<?php echo $userid; ?>'/>
										
										<div class="form-group">
											<label for="term" class="col-sm-2 control-label">Student Name</label>  
											<div class="col-sm-8">
												<label for="studentname" name="studentname" id="studentname" class="col-sm-8 control-label" style="text-align: left;"><strong><?php echo $name; ?></strong></label>
											</div>	         
										</div>
											
										<div class="form-group">
											<label for="term" class="col-sm-2 control-label">Degree</label>  
											<div class="col-sm-8">
												<label for="degree" id="degree" class="col-sm-8 control-label" style="text-align: left;"><i><?php echo $degree; ?></i></label>
											</div>	         
										</div>
										
										<div class="form-group">
											<label for="term" class="col-sm-2 control-label">Student Date of Birth (mm/dd/yyyy)</label>  
											<div class="col-sm-3">
												<input id="dateofbirth" name="dateofbirth" class="form-control datepicker" data-dateformat='mm/dd/yy' type="text" autoccomplete="off" placeholder="ex. 08/23/1982" value="" data-mask="99/99/9999" data-mask-placeholder= "-" required>
											</div>	         
										</div>				
										
										<div class="form-group">
											<label for="term" class="col-sm-2 control-label">AY Term</label>  
											<div class="col-sm-3">
												<select class='form-control input-sm' id='termid' name='termid' required>                     
													<?php
														foreach ($ayterm as $ayterm)
														{
															echo "<option value='". $ayterm->TermID ."' >". $ayterm->AcademicYear . "  " . $ayterm->SchoolTerm . "</option>";
														}
													?>
												</select>
											</div>	         
										</div>
											
										<div class="form-group">
											<label for="term" class="col-sm-2 control-label">Registration ID</label>  
											<div class="col-sm-3">
												<input id="registrationid" name="registrationid" class="form-control" type="text" autocomplete="off" placeholder="" value="" required>
											</div>	         
										</div>
										
										<div class="form-group">
											<label for="term" class="col-sm-2 control-label">Student Last Payment Posted Official Receipt</label>  
											<div class="col-sm-3">
												<input id="receipt" name="receipt" class="form-control" type="text" autocomplete="off" placeholder="" value="">
											</div>	         
										</div>
										
										<div class="form-group">
											<label for="term" class="col-sm-2 control-label">Student Last Payment Posted Official Date</label>  
											<div class="col-sm-3">
												<input type="text" id="date"  name="date" placeholder="ex. 10/26/1990" class="form-control datepicker" data-dateformat='mm/dd/yy' data-mask="99/99/9999" data-mask-placeholder= "-">	
											</div>	         
										</div>
															
										<div class="form-group">
											<label for="term" class="col-sm-2 control-label">Relationship</label>  
											<div class="col-sm-3">
												<select class='form-control input-sm' id='relationship' name='relationship' required>                     
													<option value="Family">Family</option>
													<option value="Guardian">Guardian</option>
													<option value="Parent">Parent</option>
												</select>
											</div>	         
										</div>
										
										<div class="form-group">
											<div class="col-sm-2 col-sm-offset-2">
												<button type="button" class="btn btn-primary" id="cmdActivate" name="cmdActivate"  onclick="submitform()">Activate</button>
												<button type="button" class="btn btn-primary" id="cmdCancel" name="cmdCancel">Cancel</button>
											</div>
										</div>
									</div>
                                    </form>
									
									</div>
		                      </fieldset>
							  </div>
							</div>
							<?php
							}
							
							if(isset($xact_faculty) && $xact_faculty==1)
							{
							?>
							<div class="tab-pane fade in  <?php if(isset($s4)){echo $s4;}?>" id="s4">
						
								    <div class="row">
									  <div class="col-sm-12">										
										<div class="well">
											<button class="close" data-dismiss="alert">x</button>
											<h1 class="semi-bold">Are you a <?php echo INST_FAC;?>?</h1>
											<p>Complete the whole form and click "Submit".</p>
										</div>
									  </div>
								    </div>
								    <br>
								    <?php echo form_open('activation/verifyfaculty',array('id'=>'faculty_portal','class' => 'smart-form', 'novalidate'=>'novalidate')); ?>
									<div class="well well-light" >
									<br>
										<h3 style="margin-left: 15px;"><strong> Faculty </strong> - Activation</h3>																										
										<!-- wizard form starts here -->										
										<fieldset>
											<!-- Basic information -->
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="flname" id="flname"  placeholder="Last name" value="<?php if(isset($userinfo['lastname'])) echo $userinfo['lastname']; ?>">
													</label>
												</section>											
												<section class="col col-4">
													<label class="input">
														<input type="text" name="ffname" id="ffname" placeholder="First name" value="<?php if(isset($userinfo['firstname'])) echo $userinfo['firstname']; ?>">
													</label>
												</section>
											</div>
											<div class="row">
												<section class="col col-4">
													<label class="input"> <i class="icon-append fa fa-calendar"></i>
														<input type="text" id="fbday" name="fbday" placeholder="Date of Birth (mm/dd/yyyy)" class="datepicker" data-dateformat='mm/dd/yy' data-mask="99/99/9999" data-mask-placeholder= "-" value="<?php if(isset($userinfo['birthdate'])) echo $userinfo['birthdate']; ?>">
													</label>
												</section>																								
												<section class="col col-4">
													<label class="select">
														<select name="fgender">
															<?php
															$gender ='';
															if(isset($userinfo['gender']))
															{$gender = trim($userinfo['gender']);}
															
															 echo  '<option value="0" ' . ($gender == ''? 'selected':'') . ' disabled>Gender</option>
																	  <option value="M" ' . ($gender == 'M'? 'selected':'') . ' >Male</option>
																     <option value="F" ' . ($gender == 'F'? 'selected':'') . ' >Female</option>' ;
															?> 
														</select> <i></i> </label>
												</section>
											</div>																						
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="prismsid" id="prismsid" placeholder="PRISMS User id" value="<?php if(isset($userinfo['idno'])){echo $userinfo['idno'];} ?>">
													</label>
												</section>																						
												<section class="col col-4">
													<label class="input">
														<input type="password" name="prismspwd" id="prismspwd" placeholder="PRISMS Password" value="">
													</label>
												</section>												
											</div>																				
											<button class="btn btn-sm btn-primary" value="submit">
													Submit
											</button>	
										</fieldset>
										
																					
									</div>	
									
								</form>
							</div>
							<?php
							}
							
							if(isset($xact_admin) && $xact_admin==1)
							{
							?>
							<div class="tab-pane fade in <?php if(isset($s5)){echo $s5;}?>" id="s5">
							 
								    <div class="row">
									  <div class="col-sm-12">										
										<div class="well">
											<button class="close" data-dismiss="alert">x</button>
											<h1 class="semi-bold">Are you a <?php echo INST_ADM;?>?</h1>
											<p>Complete the whole form and click the "Submit".</p>
											<p>(<i class="fa fa-warning"></i> WARNING! If we identify that you are doing an illegal activity, our administrators will block you from this site.) </p>
										</div>
									  </div>
								    </div>
								    <br>
								<?php echo form_open('activation/verifyadmin',array('id'=>'admin_portal','class' => 'smart-form', 'novalidate'=>'novalidate')); ?>								
									<div class="well well-light" >
										<br> 
										<h3 style="margin-left: 15px;"><strong> Administratives </strong> - Activation</h3>																																				
										<fieldset>										
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="flname" id="flname"  placeholder="Last name" value="<?php if(isset($userinfo['lastname'])) echo $userinfo['lastname']; ?>">
													</label>
												</section>											
												<section class="col col-4">
													<label class="input">
														<input type="text" name="ffname" id="ffname" placeholder="First name" value="<?php if(isset($userinfo['firstname'])) echo $userinfo['firstname']; ?>">
													</label>
												</section>
											</div>
											<div class="row">
												<section class="col col-4">
													<label class="input"> <i class="icon-append fa fa-calendar"></i>
														<input type="text" id="fbday" name="fbday" placeholder="Date of Birth (mm/dd/yyyy)" class="datepicker" data-dateformat='mm/dd/yy' data-mask="99/99/9999" data-mask-placeholder= "-" value="<?php if(isset($userinfo['birthdate'])) echo $userinfo['birthdate']; ?>">
													</label>
												</section>																								
												<section class="col col-4">
													<label class="select">
														<select name="fgender">
															<?php
															$gender ='';
															if(isset($userinfo['gender']))
															{$gender = trim($userinfo['gender']);}
															
															 echo  '<option value="0" ' . ($gender == ''? 'selected':'') . ' disabled>Gender</option>
																	  <option value="M" ' . ($gender == 'M'? 'selected':'') . ' >Male</option>
																     <option value="F" ' . ($gender == 'F'? 'selected':'') . ' >Female</option>' ;
															?> 
														</select> <i></i> </label>
												</section>
											</div>																						
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="prismsid" id="prismsid" placeholder="PRISMS User id" value="<?php if(isset($userinfo['idno'])){echo $userinfo['idno'];} ?>">
													</label>
												</section>																						
												<section class="col col-4">
													<label class="input">
														<input type="password" name="prismspwd" id="prismspwd" placeholder="PRISMS Password" value="">
													</label>
												</section>												
											</div>																				
											<button class="btn btn-sm btn-primary" value="submit">Submit</button>	
										</fieldset>							
									</div>										
								</form>
							</div>
						    <?php
							}
							?>							
						</div>

					</div>
					<!-- end widget content -->
				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->
	 </div>
  </div>