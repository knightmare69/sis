<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Portal Activation</li>
	</ol>
</div>   
<div class="content">
   <div class="row">
		<div class="col-lg-12">
		  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			 <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-flash"></i> <?php echo $title; ?> <span>> Portal Activation</span></h1>
		  </div>
		  <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">

		  </div>
		</div>
	 </div><!-- /.row -->
    <div class="row">
      <article class="col-sm-12 col-md-12 col-lg-12">
         <div class="well">            
            <?php
               if($result > 0) 
			   {
                  echo "<h1><strong>Congratulations!</strong></h1>";
                  echo "<p>You have successfully activated your account in PRISMS Portal! Please log out and sign in again to see the changes!:)</p>";
                  echo "<a class='btn btn-primary btn-large' href='". site_url('home/logout') ."'> <i class='fa fa-sign-out fa-lg'></i> Sign Out Now?</a>";
               }		   
			   elseif($result == 0) 
			   {
                  echo "<h1><strong>Oops Sorry!</strong></h1>";
                  echo "<p>You may have a problem on your details... Kindly check your information and try again :(</p>";
                  echo ((isset($recommendation) && $recommendation!='')?("<p><i>".$recommendation."</i></p>"):'');
				  echo "<a class='btn btn-primary btn-large' href='". site_url('activation') ."'>Try Again...</a>";
               }
			   elseif($result < 0) 
			   {
                  echo "<h1><strong>Oops Sorry!</strong></h1>";
                  echo "<p>Your Data for Activation is already in used. :(</p>";
                  echo "<a class='btn btn-primary btn-large' href='". site_url('activation') ."'>Try Again...</a>";
               }
            ?>                        
         </div>      
      </article>      
    </div>
</div>
