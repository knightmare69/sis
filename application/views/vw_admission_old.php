
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Admission</li>
	</ol>
</div>   
<div id="content">	 
	<div class="row">
	  <div class="col-lg-12">
		 <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-gavel"></i> <?php echo $title; ?> <span>> Application</span></h1>
		 </div>
		 <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
			<div class="well no-padding pull-right">
				<a id="btncancel" class="btn btn-sm btn-success" href="<?php echo site_url('admission'); ?>">
					Cancel Application
				</a>					
			</div>
		 </div>
	  </div>
	</div><!-- /.row -->	
	<!-- widget grid -->
	<section id="widget-grid" class="">
		<div class="row">						
			<article class="col-sm-12 col-md-12 col-lg-12">					
				<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-deletebutton="false">					
					<header>	<h2>Admission Wizard </h2>	</header>					
					<div>							
						<div class="jarviswidget-editbox"> </div>									
						<div class="widget-body fuelux">	
							<div class="wizard">
								<ul class="steps">
									<li data-target="#step1" class="active">
										<span class="badge badge-info">1</span>Step 1<span class="chevron"></span>
									</li>
									<li data-target="#step2">
										<span class="badge">2</span>Step 2<span class="chevron"></span>
									</li>
									<li data-target="#step3">
										<span class="badge">3</span>Step 3<span class="chevron"></span>
									</li>
									<li data-target="#step4">
										<span class="badge">4</span>Step 4<span class="chevron"></span>
									</li>
									<li data-target="#step5">
										<span class="badge">5</span>Step 5<span class="chevron"></span>
									</li>
									<li data-target="#step6">
										<span class="badge">6</span>Step 6<span class="chevron"></span>
									</li>
								</ul>
								<div class="actions">
									<button type="button" class="btn btn-sm btn-primary btn-prev">
										<i class="fa fa-arrow-left"></i>Prev
									</button>
									<button type="button" class="btn btn-sm btn-success btn-next" data-last="Finish">
										Next<i class="fa fa-arrow-right"></i>
									</button>
								</div>
							</div>
							
							<div class="step-content">
								<!-- <form class="smart-form" id="fuelux-wizard" method="post" novalidate="novalidate"> -->
								<?php echo form_open('admission/submit',array('id'=>'fuelux-wizard','class' => 'smart-form', 'novalidate'=>'novalidate')); ?>
									
									<div class="step-pane active" id="step1">
										<br>
										<h3><strong>Step 1 </strong> - Basic Information</h3>
										<!-- wizard form starts here -->
										
										<fieldset>
											<!-- Basic information -->
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="lname" id="lname"  placeholder="Last name" value="<?php if(isset($userinfo['lastname'])) echo $userinfo['lastname']; ?>">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="fname" id="fname" placeholder="First name" value="<?php if(isset($userinfo['firstname'])) echo $userinfo['firstname']; ?>">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="mname" id="mname" placeholder="Middle name" value="<?php if(isset($userinfo['middlename'])) echo $userinfo['middlename']; ?>">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="ext" id="ext" placeholder="Extension name" value="<?php if(isset($userinfo['extname'])) echo $userinfo['extname']; ?>">
													</label>
												</section>												
											</div>
											<div class="row">												
												<section class="col col-4">
													<label class="select">
														<select name="gender">
															<?php
															$gender ='';
															if(isset($userinfo['gender']))
															{$gender = trim($userinfo['gender']) ;}
															
															 echo  '<option value="0" ' . ($gender == ''? 'selected':'') . ' disabled>Gender</option>
																	  <option value="M" ' . ($gender == 'M'? 'selected':'') . ' >Male</option>
																     <option value="F" ' . ($gender == 'F'? 'selected':'') . ' >Female</option>' ;
															?> 
														</select> <i></i> </label>
												</section>
												<section class="col col-4">
													<label class="input"> <i class="icon-append fa fa-calendar"></i>
														<input type="text" id="birthdate" name="birthdate" placeholder="Date of Birth (mm/dd/yyyy)" class="datepicker" data-dateformat='mm/dd/yy' data-mask="99/99/9999" data-mask-placeholder= "_" value="<?php if(isset($userinfo['birthdate'])) echo $userinfo['birthdate']; ?>">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="birthplace" id="birthplace" placeholder="Birth place" value="<?php if(isset($userinfo['birthplace'])) echo $userinfo['birthplace']; ?>">
													</label>
												</section>												
											</div>
											<div class="row">
												<!-- Civil Status -->
												<section class="col col-4">
													<label class="select">
														<select name="civilstatus">
															<option value="0" disabled=""> - Civil Status -</option>
															<?php
															    $res = '';
																if(isset($userinfo['cstatus']))
																{$res = trim($userinfo['cstatus']);}
																
																foreach ($rscs as $rscs)
																{
																 echo "<option value='". $rscs->StatusID ."' " . ($res == $rscs->StatusID ? 'selected':'') . " >". $rscs->CivilDesc."</option>";
																}
																
															?>
														</select> <i></i> </label>
												</section>
												<!-- Religion -->
												<section class="col col-4">
													<label class="select">
														<select name="religion">
															<option value="0" selected="" disabled="">Religion</option>
															<?php
																$selrel = '';
																if(isset($userinfo['relid']))
																{$selrel = trim($userinfo['relid']);}
																
																foreach ($rsreligion as $rsreligion){
																 echo "<option value='". $rsreligion->ReligionID ."'".  ($selrel == $rsreligion->ReligionID ? 'selected':'') .">". $rsreligion->ShortName."</option>";
																}																		
															?>
														</select> <i></i> </label>
												</section>
												<!-- Nationality -->
												<section class="col col-4">
													<label class="select">
														<select name="nationality">
															<option value="0" selected="" disabled="">Nationality</option>
															<?php
																$selnat = '';
																if(isset($userinfo['natid']))
																{$selnat = trim($userinfo['natid']);}
																foreach ($rsnat as $rsnat){
																 echo "<option value='". $rsnat->NationalityID ."'". ($selnat == $rsnat->NationalityID ? 'selected':'')  .">". $rsnat->Nationality."</option>";
																}																		
															?>
														</select> <i></i> </label>
												</section>
											</div>
											<div class="row">
												<section class="col col-2">
													<label class="input">
														<input type="text" name="resno" id="resno" placeholder="Residence">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="resstreet" id="resstreet" placeholder="Street">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="rescity" id="rescity" placeholder="Town/City">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="resprovince" id="resprovince" placeholder="Province/Region">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="reszip" id="reszip" placeholder="Postal Code">
													</label>
												</section>												
											</div>											
											<div class="row">
												<section class="col col-2">
													<label class="input">
														<input type="text" name="permno" id="permno" placeholder="Permanent">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="permstreet" id="permstreet" placeholder="Street">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="permcity" id="permcity" placeholder="Town/City">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="permprovince" id="permprovince" placeholder="Province/Region">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="permzip" id="permzip" placeholder="Postal Code">
													</label>
												</section>												
											</div>											
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="telephone" id="telephone" placeholder="Telephone number">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="mobile" id="mobile" placeholder="Mobile number">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="email" id="email" placeholder="Email">
													</label>
												</section>						
											</div>			
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="disability" id="disability" placeholder="Physical/Medical Disability">
													</label>
												</section>	
                                            </div> 													
										</fieldset>
                                        <fieldset class="foreigndata hidden">
											<header style="margin-top:-10px;margin-left:-10px;">Foreign Student</header><br/>					
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="passport" id="passport" placeholder="Passport No.">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="dateissue" id="dateissue" placeholder="Date Isssued">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="placeissue" id="placeissue" placeholder="Place Isssued">
													</label>
												</section>						
											</div>
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="visatype" id="visatype" placeholder="Type of Visa">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="visastats" id="visastats" placeholder="Visa Status">
													</label>
												</section>
												<section class="col col-4">
													<label class="input">
														<input type="text" name="icardno" id="icardno" placeholder="I-Card No.">
													</label>
												</section>						
											</div>	
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="stayfrom" id="stayfrom" placeholder="Stay From">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="stayto" id="stayto" placeholder="Stay To">
													</label>
												</section>
												<section class="col col-3 pull-right">
													<label class="input">
														<input type="text" name="remarks" id="remarks" placeholder="Remarks">
													</label>
												</section>
											</div>	
                                        </fieldset>										
									</div>	
									<div class="step-pane" id="step2">
										<br>
										<h3><strong>Step 2 </strong> - Family Background </h3>
										<fieldset>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="father" id="father"  placeholder="Father's Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="fjob" id="fjob"  placeholder="Father Occupation" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="fcontact" id="fcontact"  placeholder="Father Contact number" value="">
													</label>
												</section>	
												<section class="col col-3">
												   <div class="inline-group">
													<label class="checkbox">
														<input type="checkbox" name="fofw" id="fofw"> Is OFW
														<i></i>
													</label>
													<label class="checkbox">
														<input type="checkbox" name="falive" id="falive"> Is Alive
														<i></i>
													</label>
												  </div>	
												</section>	
                                            </div>
                                            <div class="row">											
												<section class="col col-3">
													<label class="input">
														<input type="text" name="fnation" id="fnation"  placeholder="Father's Nationality" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="faddr" id="faddr"  placeholder="Father's Address" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="femail" id="femail"  placeholder="Father's Email Addr" value="">
													</label>
												</section>													
											</div>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="mother" id="mother"  placeholder="Mother's Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="mjob" id="mjob"  placeholder="Mother Occupation" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="mcontact" id="mcontact"  placeholder="Mother Contact number" value="">
													</label>
												</section>	
												<section class="col col-3">
												   <div class="inline-group">
													<label class="checkbox">
														<input type="checkbox" name="mofw" id="mofw"> Is OFW
														<i></i>
													</label>
													<label class="checkbox">
														<input type="checkbox" name="malive" id="malive"> Is Alive
														<i></i>
													</label>
												  </div>	
												</section>											
											</div>
                                            <div class="row">											
												<section class="col col-3">
													<label class="input">
														<input type="text" name="mnation" id="mnation"  placeholder="Mother's Nationality" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="maddr" id="maddr"  placeholder="Mother's Address" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="memail" id="memail"  placeholder="Mother's Email Addr" value="">
													</label>
												</section>													
											</div>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="guardian" id="guardian"  placeholder="Guardian's Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="gaddress" id="gaddress"  placeholder="Guardian Address" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="gcontact" id="gcontact"  placeholder="Guardian Contact number" value="">
													</label>
												</section>											
											</div>			
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="nosibling" id="nosibling"  placeholder="Number of Siblings" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="brothers" id="brothers"  placeholder="Brothers" value="">
													</label>
												</section>
                                            </div>			
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="spouse" id="spouse"  placeholder="Spouse's Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="spouse_age" id="spouse_age"  placeholder="Age" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="spouse_occup" id="spouse_occup"  placeholder="Occupation" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="nochildren" id="nochildren"  placeholder="No. of Children" value="">
													</label>
												</section>
                                            </div>											
										</fieldset>
                                        <fieldset class="relativedata">
											<header style="margin-top:-10px;margin-left:-10px;">Relative Background</header><br/>					
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="rnamea" id="rnamea" placeholder="Name">
													</label>
												</section>
												<section class="col col-5">
													<label class="input">
														<input type="text" name="rrelationa" id="rrelationa" placeholder="Relationship">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="rdatea" id="rdatea" placeholder="Date">
													</label>
												</section>
										    </div>				
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="rnameb" id="rnameb" placeholder="Name">
													</label>
												</section>
												<section class="col col-5">
													<label class="input">
														<input type="text" name="rrelationb" id="rrelationb" placeholder="Relationship">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="rdateb" id="rdateb" placeholder="Date">
													</label>
												</section>
										    </div>				
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="rnamec" id="rnamec" placeholder="Name">
													</label>
												</section>
												<section class="col col-5">
													<label class="input">
														<input type="text" name="rrelationc" id="rrelationc" placeholder="Relationship">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="rdatec" id="rdatec" placeholder="Date">
													</label>
												</section>
										    </div>		
											<div class="row">
												<section class="col col-4">
													<label class="input">
														<input type="text" name="rnamed" id="rnamed" placeholder="Name">
													</label>
												</section>
												<section class="col col-5">
													<label class="input">
														<input type="text" name="rrelationd" id="rrelationd" placeholder="Relationship">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="rdated" id="rdated" placeholder="Date">
													</label>
												</section>
										    </div>		
										</fieldset>		
										<br>
										
									</div>
									<div class="step-pane" id="step3">
									    <br/>
									    <h3><strong>Step 3 </strong> - Educational Attainment </h3>
										<fieldset>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="elem" id="elem"  placeholder="Elementary School Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="elemaddress" id="elemaddress"  placeholder="Elementary School Address" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="elemgraduate" id="elemgraduate"  placeholder="Elementary Year Graduated" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="elemaward" id="elemaward"  placeholder="Award/Honor" value="">
													</label>
												</section>

											</div>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="hs" id="hs"  placeholder="Secondary School Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="hsaddress" id="hsaddress"  placeholder="Secondary School Address" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="hsgraduate" id="hsgraduate"  placeholder="Secondary Year Graduated" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="hsaward" id="hsaward"  placeholder="Award/Honor" value="">
													</label>
												</section>

											</div>
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="college" id="college"  placeholder="College School Name" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="collegeaddress" id="collegeaddress"  placeholder="College School Address" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="collegegrad" id="collegegrad"  placeholder="College Year Graduated" value="">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="collegeaward" id="collegeaward"  placeholder="Award/Honor" value="">
													</label>
												</section>

											</div>											
											<div class="row">
												<section class="col col-6">
													<label class="input">
														<input type="text" name="collegedegree" id="collegedegree"  placeholder="College Degree" value="">
													</label>
												</section>
											</div>
										</fieldset>
										<fieldset>											
											<div class="row">
												<section class="col col-6">
													<label class="checkbox">
													    Have you been placed under probation?&nbsp 
														<input type="checkbox" name="collegedegree" id="collegedegree"  placeholder="College Degree" value="">
														<i></i>
													</label>
												</section>
												<section class="col col-6">
													<label class="checkbox">
													    Have you ever been dismissed?&nbsp 
														<input type="checkbox" name="collegedegree" id="collegedegree"  placeholder="College Degree" value="">
														<i></i>
													</label>
												</section>
											</div>											
											<div class="row">
												<section class="col col-6">
													<label class="checkbox">
													    Membership in Professional, Civil Societies, Associations, Labor Unios, etc: 
														<input type="checkbox" name="collegedegree" id="collegedegree"  placeholder="College Degree" value="">
														<i></i>
													</label>
												</section>
											</div>
										</fieldset>
                                        <fieldset class="workdata">
											<header style="margin-top:-10px;margin-left:-10px;">Work Experience(start from the present)</header><br/>					
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="wnamea" id="wnamea" placeholder="Company Name">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="waddra" id="waddra" placeholder="Company Address">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wpositiona" id="wpositiona" placeholder="Position">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wdateempa" id="wdateempa" placeholder="Date Employed">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wcontacta" id="wcontacta" placeholder="Tel./Cel./Fax No.">
													</label>
												</section>
										    </div>					
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="wnameb" id="wnameb" placeholder="Company Name">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="waddrb" id="waddrb" placeholder="Company Address">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wpositionb" id="wpositionb" placeholder="Position">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wdateempb" id="wdateempb" placeholder="Date Employed">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wcontactb" id="wcontactb" placeholder="Tel./Cel./Fax No.">
													</label>
												</section>
										    </div>		
											<div class="row">
												<section class="col col-3">
													<label class="input">
														<input type="text" name="wnamec" id="wnamec" placeholder="Company Name">
													</label>
												</section>
												<section class="col col-3">
													<label class="input">
														<input type="text" name="waddrc" id="waddrc" placeholder="Company Address">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wpositionc" id="wpositionc" placeholder="Position">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wdateempc" id="wdateempc" placeholder="Date Employed">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="wcontactc" id="wcontactc" placeholder="Tel./Cel./Fax No.">
													</label>
												</section>
										    </div>		
										</fieldset>	
									</div>
	
									<div class="step-pane" id="step4">	
										<br>
										<h3><strong>Step 4 </strong> - Course Preferences </h3>
										<br>
										<fieldset>
											<div class="row">
												<section class="col col-6">
													<label>Academic Year & Term :</label>
													<label class="select">
														<select name="ayterm">
															<option value="0" selected="" disabled=""> - Select Academic Year & Term - </option>
															<?php
																foreach ($rsayterm as $rsayterm){
																 echo "<option value='". $rsayterm->TermID ."' selected>". $rsayterm->AcademicYearTerm."</option>";
																}																		
															?>
														</select> <i></i> </label>
												</section>											
											</div>											
											<div class="row">
												<section class="col col-6">
													<label>Application Type :</label>
													<label class="select">
														<select name="apptype">
															<option value="0" selected="" disabled=""> - Select Application Type - </option>
															<?php
																foreach ($rsapptypes as $rsapptypes){
																 echo "<option value='". $rsapptypes->TypeID ."'>". $rsapptypes->ApplicationType ."</option>";
																}																		
															?>
														</select> <i></i> </label>
												</section>
											</div>
											<div class="row">
												<section class="col col-6">
													<label>Select Campus :</label>
													<label class="select">
														<select name="campus">
															<option value="0" selected="" disabled=""> - Select Campus - </option>
															<?php
																foreach ($rscampus as $rscampus){
																 echo "<option value='". $rscampus->CampusID ."' selected>". $rscampus->ShortName ."</option>";
																}																		
															?>
														</select> <i></i> </label>
												</section>
											</div>
											<div class="row">
												<section class="col col-10">
													<label>First Choice :</label>
													<label class="select">
														<select name="choice1">
															<option value="0" selected="" disabled=""> - Select First Course - </option>
															<?php
																foreach ($rschoice1 as $rschoice1){
																 echo "<option value='". $rschoice1->ProgID ."' data-college='".$rschoice1->CollegeID."'>". $rschoice1->ProgCode . ' : ' . $rschoice1->ProgName . ($rschoice1->Major ==''? '': ' <i>Major in</i> '.$rschoice1->Major ) ."</option>";
																}																		
															?>													
														</select> <i></i> </label>
												</section>												
											</div>
											<div class="row">
												<section class="col col-10">
													<label>Second Choice :</label>
													<label class="select">
														<select name="choice2">
															<option value="0" selected="" disabled=""> - Select Second Course - </option>
															<?php
																foreach ($rschoice2 as $rschoice2){
																 echo "<option value='". $rschoice2->ProgID ."' data-college='".$rschoice2->CollegeID."'>". $rschoice2->ProgCode . ' : ' . $rschoice2->ProgName . ($rschoice2->Major ==''? '': ' <i>Major in</i> '.$rschoice2->Major )  ."</option>";
																}																		
															?>													
														</select> <i></i> </label>
												</section>												
											</div>
											<div class="row">
												<section class="col col-10">
													<label>Third Choice :</label>
													<label class="select">
														<select name="choice3">
															<option value="0" selected="" disabled=""> - Select Second Course - </option>
															<?php
																foreach ($rschoice3 as $rschoice3){
																 echo "<option value='". $rschoice3->ProgID ."'>". $rschoice3->ProgCode . ' : ' . $rschoice3->ProgName . ($rschoice3->Major ==''? '': ' <i>Major in</i> '.$rschoice3->Major )  ."</option>";
																}																		
															?>													
														</select> <i></i> </label>
												</section>												
											</div>
											
										</fieldset>	
									</div>
	
									<div class="step-pane" id="step5">
										<br>
										<h3><strong>Step 5 </strong> - Document Requirements </h3>
										<fieldset>
												<div class="row">
													<section>
														<label class="label">Birth Certificate</label>
														<div class="input input-file">
															<span class="button"><input type="file" id="birthcert" name="birthcert" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
														</div>
													</section>
												</div>
												<div class="row">
													<section>
														<label class="label">FORM 137</label>
														<div class="input input-file">
															<span class="button"><input type="file" id="form137" name="form137" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
														</div>
													</section>
												</div>
												<div class="row">
													<section>
														<label class="label">FORM 138</label>
														<div class="input input-file">
															<span class="button"><input type="file" id="form138" name="form138" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
														</div>
													</section>
												</div>
												<div class="row">
													<section>
														<label class="label">True Copy of Report Card</label>
														<div class="input input-file">
															<span class="button"><input type="file" id="reportcard" name="reportcard" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">
														</div>
													</section>
												</div>												
											</fieldset>		
									</div>
	
									<div class="step-pane" id="step6">
									    <h3><strong>Step 6 </strong> - Finished!</h3>
										<br>
										<fieldset>	
										    <div class="row">
												<section class="col col-12">
												    How did you come to know San Beda College?:<br/>
													<div class="col-sm-3">
														<label class="checkbox">
															<input type="checkbox" name="knowbya" id="knowbya"/> from parents/siblings
															<i></i>
														</label>
													</div>	
													<div class="col-sm-3">
														<label class="checkbox">
															<input type="checkbox" name="knowbyb" id="knowbyb"/> from my friends/classmates
															<i></i>
														</label>
													</div>	
													<div class="col-sm-3">
														<label class="checkbox">
															<input type="checkbox" name="knowbyc" id="knowbyc"/> from my own initiative
															<i></i>
														</label>
													</div>	
													<div class="col-sm-3">
														<label class="checkbox">
															<input type="checkbox" name="knowbyd" id="knowbyd"/> from teachers
															<i></i>
														</label>
													</div>
													<div class="col-sm-3">
														<label class="checkbox">
															<input type="checkbox" name="knowbye" id="knowbye"/> from the internet/webpage
															<i></i>
														</label>
													</div>	
													<div class="col-sm-3">
														<label class="checkbox">
															<input type="checkbox" name="knowbyf" id="knowbyf"/> from SBC brochures/poster
															<i></i>
														</label>
													</div>	
													<div class="col-sm-3">
														<label class="checkbox">
															<input type="checkbox" name="knowbyg" id="knowbyg"/> from career orientation talks
															<i></i>
														</label>
													</div>	
													<div class="col-sm-3">
														<label class="checkbox">
															<input type="checkbox" name="knowbyh" id="knowbyh"/> others(pls. specify)
															<i></i>
														</label>
													</div>
												</section>
											</div>
										</fieldset>
                                        <fieldset class="examdata hidden"> 										
											<div class="row">
												<section class="col col-3">
													<label class="checkbox">
														Have you taken <b class="examname">NMAT</b>?
														<input type="checkbox" name="examtaken" id="examtaken"/>
														<i></i>
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="examscore" id="examscore" placeholder="Exam Score">
													</label>
												</section>
												<section class="col col-2">
													<label class="input">
														<input type="text" name="examdate" id="examdate" placeholder="Date Taken">
													</label>
												</section>
										    </div>
											
											<div class="row">
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherapply" id="otherapply" placeholder="What other medical/law schools have you applied into?">
													</label>
												</section>	
                                            </div>			
											<div class="row">
												<section class="col col-6">
													<label class="checkbox">
														Have you previously enrolled to other <b class="examschool">medical/law</b> school?
														<input type="checkbox" name="otherenrolled" id="otherenrolled"/> 
														<i></i>
													</label>
												</section>
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherenroll" id="otherenroll" placeholder="If yes, indicate school">
													</label>
												</section>
											</div>	
											
											<div class="row">
												<section class="col col-6">
													<label class="checkbox">
														Have you ever been dismissed or disqualified from enrolling in that medical/law school by reason of scholastic standing or disciplinary actions?
														<input type="checkbox" name="otherdismiss" id="otherdismiss"/> 
														<i></i>
													</label>
												</section>
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherexplain" id="otherexplain" placeholder="Explain">
													</label>
												</section>
											</div>	
											
											<div class="row">
												<section class="col col-6">
													<label class="input">
														<input type="text" name="otherlicense" id="otherlicense" placeholder="Licensing Exam's Passed">
													</label>
												</section>
											</div>	
										</fieldset>	
										<fieldset class="hidden">	
										<h1 class="text-center text-success"><i class="fa fa-check"></i> Congratulations!
										<br>
										<small>Click finish to end wizard</small></h1>
										<br>
										<br>
										<br>
										<br>
										</fieldset>
									</div>
	
								</form>
							</div>
	
						</div>
						<!-- end widget content -->
	
					</div>
					<!-- end widget div -->
	
				</div>
				<!-- end widget -->
	
			</article>
			<!-- WIDGET END -->
	
		</div>
	
		<!-- end row -->
	
	</section>
	<!-- end widget grid -->
					
  </div>