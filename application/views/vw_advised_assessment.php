<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><a id = "advurl" href="<?php echo site_url('advising/schedules'); ?>">Advising</a></li>
		<li>Assessment</li>		
	</ol>
</div>   
<div id="content">	 
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<?php 
		$default = '<div class="alert alert-warning fade in">
			         <i class="fa-fw fa fa-exclamation"></i> <strong>Please</strong> if you don'."'".'t have assessment or subject in the list, try registering again to process your registration correctly.
		            </div>';
					
		if(isset($opt) && $opt==1)
		{
		echo '<div class="alert alert-success fade in" style="margin-bottom:5px;">
				<button class="close" data-dismiss="alert">x</button>
				<i class="fa-fw fa fa-check"></i><strong>Success</strong> Your schedule has been saved!.
	          </div>';
		}
        elseif(isset($opt) && $opt==2)
		{
		echo '<div class="alert alert-warning fade in" style="margin-bottom:5px;">
				<button class="close" data-dismiss="alert">x</button>
				<i class="fa-fw fa fa-warning"></i><strong>Success</strong> Your schedule has been saved!.
				<strong>Please</strong> settle your balance or accountabilities to generate assesstment!.
	          </div>';
		}
        elseif(isset($opt) && $opt== 2.1)
		{
		echo '<div class="alert alert-warning fade in" style="margin-bottom:5px;">
				<button class="close" data-dismiss="alert">x</button>
				<i class="fa-fw fa fa-warning"></i> <strong>Please</strong> settle your balance or accountabilities to generate assesstment!.
	          </div>';
		}
        elseif(isset($opt) && $opt== 2.2)
		{
		echo '<div class="alert alert-danger fade in" style="margin-bottom:5px;">
				<button class="close" data-dismiss="alert">x</button>
				<i class="fa-fw fa fa-warning"></i> <strong>Please</strong> enroll subjects for this semester in order to be assessed!.
	          </div>';
		}
		echo $default;
		?>
		</div>
        
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
				<div class="jarviswidget jarviswidget-color-maroon" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="false" data-widget-custombutton="false">
					<header role="heading">
						<h2>Registration</h2>
						<div class="jarviswidget-ctrls" role="menu">
							<a id='xmin' href="#" onclick="$('#loader').fadeIn();$('#studinfo').slideToggle(500);$('#xmin').hide();$('#xmax').show();$('#loader').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
							<a id='xmax' style='display:none;' href="#" onclick="$('#loader').fadeIn();$('#studinfo').slideToggle(500);$('#xmin').show();$('#xmax').hide();$('#loader').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>
						</div>
						<span id='loader' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
						<div class="widget-toolbar" role="menu">	
						   <a id = "btnpreg" class="btn btn-primary" href="#"><i class="fa-fw fa fa-print"></i> Print Form</a>
						   <a id = "btnpayment" class="btn btn-success hidden" href="<?php echo site_url('advising/payment'); ?>"><i class="fa-fw fa fa-money"></i> Payment</a>			 								
						</div>
					</header>
					<div id='studinfo' role="content">	
						<div class="jarviswidget-body no-padding">
							<div></div>
							<table class='table table-striped'>
								<tr><td>Academic Year & Term :</td><td><?php echo (is_object($studentinfo) && property_exists($studentinfo,'AyTerm'))?$studentinfo->AyTerm:'';?></td></tr>
                                <tr><td>Registration No. :</td><td><?php echo (is_object($studentinfo) && property_exists($studentinfo,'RegID'))?$studentinfo->RegID:'';?></td></tr>
								<tr><td>Student Name :</td><td><strong><?php echo (is_object($studentinfo) && property_exists($studentinfo,'LastName'))?$studentinfo->LastName.', '.$studentinfo->Firstname.' '.$studentinfo->Middlename . ' [' . $studentinfo->StudentNo .  ']':'';?></strong> </td></tr>					
								<tr><td>Program/Course :</td><td><?php echo (is_object($studentinfo) && property_exists($studentinfo,'Program'))?$studentinfo->Program:'';?></td></tr>
								<tr><td>Curriculum :</td><td><?php echo (is_object($studentinfo) && property_exists($studentinfo,'Curriculum'))?$studentinfo->Curriculum:'';?></td></tr>					
								<tr><td>Year Level :</td><td><?php echo (is_object($studentinfo) && property_exists($studentinfo,'Yearlvl'))?$studentinfo->Yearlvl:'';?></td></tr>
								<tr><td>Fees Template :</td><td><?php echo (is_object($studentinfo) && property_exists($studentinfo,'FeesTemplate'))?$studentinfo->FeesTemplate:'';?></td></tr>
							</table>
							<br>	   
						</div>
					</div>		  
				</div>
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
               <header role="heading"><h2>Registered Subject(s)</h2>					
                  <div class="jarviswidget-ctrls" role="menu">						
                     <a id='xmin1' href="#" onclick="$('#loader1').fadeIn();$('#subjinfo').slideToggle(500);$('#xmin1').hide();$('#xmax1').show();$('#loader1').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
                     <a id='xmax1' style='display:none;' href="#" onclick="$('#loader1').fadeIn();$('#subjinfo').slideToggle(500);$('#xmin1').show();$('#xmax1').hide();$('#loader1').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>						
                  </div>				
                  <span id='loader1' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
               </header>
               <div id="subjinfo">			
                  <div class="widget-body no-padding">
                     <?php if($row){echo $row;}?>
                     <br>	   
                  </div>   
               </div>
            </div>           
         </div>		  
		  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
		  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
			<header role="heading">
				<h2>Assessed Fees</h2>
			<div class="jarviswidget-ctrls" role="menu">
				<a id='xmin2' href="#" onclick="$('#loader2').fadeIn();$('#feesinfo').slideToggle(500);$('#xmin2').hide();$('#xmax2').show();$('#loader2').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
				<a id='xmax2' style='display:none;' href="#" onclick="$('#loader2').fadeIn();$('#feesinfo').slideToggle(500);$('#xmin2').show();$('#xmax2').hide();$('#loader2').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>
			</div>
			<span id='loader2' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
			</header>
		   <div id='feesinfo' role="content">

		   <div class="jarviswidget-body no-padding">
		     <?php if($fees){echo $fees;}?>
			<br>	   
		   </div>
		   </div>
		  
		  </div>
		  </div>
		</div>
	 </div><!-- /.row -->
  </div>

<!-- Create Modal -->
  <div class="modal fade" id="mwindow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	 <div class="modal-dialog" style="width:900px;">
		<div class="modal-content">
		  <div class="modal-header">
			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			 <h4 class="modal-title" id="lblmwindow"><strong>Offered</strong> <i>Schedules</i></h4>
		  </div>	               
			 <div class="modal-body">
				<div class="alert alert-info no-margin fade in">
					<p>
					Kindly select your schedule. <i class="fa-fw fa fa-check-circle" style="color:Green;" ></i> Approved
					| <i class="fa-fw fa fa-times-circle" style="color:Red;" ></i> Disapproved |
					<i class="fa-fw fa fa-spinner" style="color:Blue;" ></i> Wait
					</p>
				</div>
				<div class="custom-scroll table-responsive" style=" cursor: pointer; margin-top: 10px; height:330px; width: 850px; overflow-y: scroll;">
					<table id="oschedule" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>No.</th>
                        <th>S.ID</th>
								<th>Program</th>
								<th>Section</th>
								<th>Course</th>
								<th>Lec</th>
								<th>Lab</th>
								<th>Limit</th>
								<th>Schedule</th>								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>C87E48EF-605A-B4FF</td>
								<td>erat@montesnasceturridiculus.org</td>
								<td>10/03/13</td>
							</tr>
							
						</tbody>
					</table>							
				</div>						
							
				
			</div>
			<div class="modal-footer">				
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button id = "btnsubmit" type="button" onclick="setschedule()" class="btn btn-primary disabled" value="submit" name="submit" >Select</button>						
				<button id="btnreq" type="button" class="btn btn-warning pull-left disabled" >Send Request</button>								
			</div>		  
		</div>
	 </div>
  </div>