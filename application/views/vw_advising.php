<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><a href="<?php echo site_url('advising'); ?>">Advising</a></li>
		<li>Adviser</li>
	</ol>
</div>   
<div id="content">	 
	<div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<section id="widget-grid" class="">
			<div class="row">
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"  data-widget-deletebutton="false">
					<header role="heading">
					 <h2>Advised Courses</h2>
					 <div class="widget-toolbar" role="menu">
					  <button class="btn btn-primary btnreadvise"><i class="fa fa-refresh"></i> Re-Advise</button>
					 </div>
					</header>		
					<div id="xstdadv">
					<form id="advisedform" name="advisedform" method="post" action="<?php echo site_url('advising');?>" onsubmit="return checkadvisedsubj();">
						<div class="col-sm-12 col-md-12 col-lg-12 well no-padding">
						<div class="alert alert-info xmargin-bottom-10">
						 <div class="row">
						  <table><tr><td><i class="fa-fw fa fa-info fa-2x"></i></td><td>The subject(s) listed in here is/are generated base on your curriculum.<br/>Click <b>'Accept Advised Subject(s)'</b> or <b>'Proceed to Registration'</b> to accept and save the subject listed in order to proceed in selection of schedules.</td></tr></table>
						 </div>
						</div>
						<table class='table table-condense'>
							<tr><td style="width: 15%;">AY Term :</td><td style="width: 35%;">
							   <input type="hidden" id="aytermid" name="aytermid" readonly="readonly" value="<?php echo $xdetail->TermID;?>"/>
							   <input type="text" id="aytermname" name="aytermname" class="form-control input-xs" readonly="readonly" value="<?php echo $xdetail->AyTerm;?>"/>
							   </td>
							   
								<td style="width: 15%;">Min/Max :</td>
								<td>									
									<div class="form-inline">
										<strong><input type="text" id="min" name="min"  class="form-control input-xs" style="width: 60px; margin: 0px; " readonly="readonly"/> / <input type="text" id="max" name="max"  class="form-control input-xs" style="width: 60px; " readonly="readonly"/> </strong>										
									</div>									
								 </td>
							</tr>			
							<tr><td>Degree :</td><td><?php echo $xdetail->Program;?></td>
								<td>Subject(s) :</td>
								<td>
									<input type="text" id="totalsubj" name="totalsubj" class="form-control input-xs" readonly="readonly"/>
								</td>
							</tr>
							<tr><td>Curriculum :</td><td><?php echo $xdetail->Curriculum;?></td>
								<td>Lec/Lab :</td><td>
									<div class="form-inline">
									<input type="text" id="totallec" name="totallec" class="form-control input-xs" style="width: 60px; margin: 0px; " readonly="readonly"/> /
									<input type="text" id="totallab" name="totallab" class="form-control input-xs" style="width: 60px; margin: 0px; " readonly="readonly"/>
									</div>
								</td>
							</tr>					
							<tr><td>Year Level :</td><td><input type="text" id="yrlvl" name="yrlvl" class="form-control input-xs" readonly="readonly" value="<?php echo $xdetail->YrlvlID;?>"/></td>
								<td>Unit(s) :</td><td><input type="text" id="totalcunit" name="totalcunit" class="form-control input-xs" readonly="readonly"/></td>
							</tr>				
						</table>
						     <div class="alert alert-danger xmargin-bottom-5 <?php echo ((isset($onenperiod) && $onenperiod==1)?'hidden':'');?>"><i class="fa fa-fw fa-info fa-lg"></i> During Advising Period, All Unposted Grades Are Assumed Passed. No Assessment/Billing Yet. All pre-requisites will be checked during Enrollment Period.</div>
							 <input type="hidden" id="campusid" name="campusid" value="<?php echo $xdetail->CampusID;?>"/>
							 <input type="hidden" id="collegeid" name="collegeid" value="<?php echo $xdetail->CollegeID;?>"/>
							 <input type="hidden" id="progid" name="progid" value="<?php echo $xdetail->ProgID;?>"/>
							 <input type="hidden" id="feeid" name="feeid" value="<?php echo $xdetail->FeesID;?>"/>
						</div>					
						<div id="xtbladv" class="col-sm-12 col-md-12 col-lg-12 well no-padding" style="overflow:scroll;max-height:400px">
							<?php if($rs2) {echo $rs2;}?>
						</div>			 
					</div>
					<div class="col-sm-12 col-md-12 col-lg-12">
					 <i id="xlock" class="hidden"><?php if(isset($onperiod)){echo $onperiod;}?></i>
					 <?php 
					  if(isset($onperiod) && $onperiod!=0)
					  {
                      ?> 					 
					  <div id='xaddwarn' class='alert alert-warning col-xs-6 col-sm-8 col-md-9 col-lg-9'>
					  <p><i class="fa fa-warning"></i> Make sure that you check all the advised subjects listed before proceeding.</p>
					  <p><i class="fa fa-warning"></i> If you have concerns about it ask the registrar.</p>
					  </div>
					  <div class="form-group col-xs-6 col-sm-4 col-md-3 col-lg-3">
					    <button id="xsave" type="button" class="btn btn-success pull-right hidden" onclick="antidepressant();" disabled="disabled"><span class="fa fa-fw fa-save"></span> Accept Advised Subject(s)</button>
                                            <button id="xproceedreg" type="button" class="btn btn-primary pull-right hidden" disabled="disabled"><span class="fa fa-fw fa-angle-double-right"></span> Proceed to Registration</button>
					  </div>
					  <?php
					  }
					  ?>
					</div>			
				 </form>			
				</div>
			</article>
				 
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			 <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-evaluation" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false" data-widget-deletebutton="false">
				<header role="heading">			
				 <h2>Academic Evaluation</h2>		
				 <div class="widget-toolbar" role="menu">
				  <button class="btn btn-warning btn-sm toogle-widget-sidepanel" data-target=".eval_summary">Evaluation Summary</button>
				  <div class="btn-group ">
				   <a data-html="true" data-content= "<div class='row txt-color-black' style='width:280px;'> <?php   if($lgnd) {echo  str_replace('"',"'",$lgnd);}?> </div>" data-original-title="Color Legend" data-placement="bottom" rel="popover" class="btn btn-default btn-xs" href="javascript:void(0);" > <strong>Color</strong> <i>legend</i> <b class="caret"></b> </a>
				  </div>
				 </div>
				 </header>
			   <div class="no-padding" role="content">
			    <div class="eval_summary" style="display:none;">
				 <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 no-padding pull-left">
				  <div class="panel panel-greenLight">
				  <div class="panel-heading">
				   <h2 class="panel-title">Evaluation Summary <b class="pull-right dismiss-widget-sidepanel" data-target=".eval_summary">&times</b></h2>
				  </div>
				  <div class="panel-body no-padding text-align-left txt-color-black" id="xstdsum">
			      <?php if(isset($summary) && $summary){echo $summary;}?>	
 			      </div>
				  </div>
				 </div>
				</div>
				<div class='table-responsive' style="max-height:500px;overflow:scroll;" id="xstdeval">
				<?php if($rs){echo $rs;}?>
				</div>
			   </div>				   
			   <i id="xlink" class="hidden"><?php echo site_url('advising/advisedinfo');?></i>
			 </div>
			 </article>	
			</div>
		    </section>
			<div id="xpage" class="hidden">2</div>
			<div id="xstats" class="hidden" <?php if(isset($isreg)){echo 'data-reg="'.$isreg.'"';}?>></div> 
			<?php
			$xschedadv=(isset($xalert) and array_key_exists('schedadv',$xalert))?$xalert['schedadv']:0;
			$xschedenr=(isset($xalert) and array_key_exists('schedenr',$xalert))?$xalert['schedenr']:0;
			$xregonly=(isset($xalert) and array_key_exists('regonly',$xalert))?$xalert['regonly']:0;
			$xbalance=(isset($xalert) and array_key_exists('balance',$xalert))?$xalert['balance']:0;
			$xaccount=(isset($xalert) and array_key_exists('account',$xalert))?$xalert['account']:0;
			$xinactive=(isset($xalert) and array_key_exists('inactive',$xalert))?$xalert['inactive']:0;
			echo '<div id="xwarningid" class="hidden" data-schedadv="'.$xschedadv.'" data-schedenr="'.$xschedenr.'" data-regonly="'.$xregonly.'" data-balance="'.$xbalance.'" data-account="'.$xaccount.'" data-inactive="'.$xinactive.'"></div>';
            ?>
		</div>
	  </div>
	</div><!-- /.row -->
</div>

<div class="modal fade" id="OneSubjOnly" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
<div class="modal-dialog">
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h4 class="modal-title" id="myModalLabel">Please Select One</h4>
</div>
<div class="modal-body">
<div class="alert alert-info">
 <i class="fa fa-info"></i> All subject listed here are group into one. One subject is necessary.
</div>

<div class="row">
<div class="col-md-12">
 <table id="xonesubjtbl" class="table table-bordered">
  <thead>
   <tr>
    <th></th>
    <th>COURSE CODE</th>
    <th>TITLE</th>
   </tr>
  </thead>
  <tbody>
  </tbody>
 </table>
</div>
</div>

</div>
<div class="modal-footer">
<button type="button" id="xonesubjprocess" class="btn btn-primary"><i class="fa fa-gear"></i> Process</button>
</div>

</div>
</div>
</div>
<!--

-->