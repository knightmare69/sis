<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><a id = "advurl" href="<?php echo site_url('advising'); ?>">Advising</a></li>
		<li>Scheduling</li>		
	</ol>
</div>   
<div id="content">	 
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">          		  
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-maroon" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="false" data-widget-custombutton="false">
					<header role="heading">
						<h2>Student Information</h2>
				        <div class="jarviswidget-ctrls" role="menu">
							<a id='xmin' href="#" onclick="$('#loader').fadeIn();$('#studinfo').slideToggle(500);$('#xmin').hide();$('#xmax').show();$('#loader').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
							<a id='xmax' style='display:none;' href="#" onclick="$('#loader').fadeIn();$('#studinfo').slideToggle(500);$('#xmin').show();$('#xmax').hide();$('#loader').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>
						</div>
						<span id='loader' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
						<div class="widget-toolbar" role="menu">
						<?php if($xprt==true){echo '<a id="xvwassessment" class="btn btn-primary" href="'.site_url('advising/assessment').'"> <i class="fa-fw fa fa-print"></i> Assessment </a>';} ?>
						</div>
					</header>
					<div id='studinfo' role="content">	
						<div class="jarviswidget-body no-padding">
							<div></div>
							 <table class='table table-striped'>
							    <tr><td width="20%">Academic Year & Term :</td><td colspan="3"><?php echo ((is_array($studentinfo) && array_key_exists('activeterm',$studentinfo))?$studentinfo['activeterm']:'');?></td></tr>
								<tr><td width="20%">Student Name :</td><td colspan="3"><strong><?php echo ((is_array($studentinfo) && array_key_exists('studentname',$studentinfo))?($studentinfo['studentname'].'['.$studentinfo['studentno'].']'):('['.$studentinfo['studentno'].']'));?></strong> </td></tr>					
								<tr><td width="20%">Program/Course :</td><td colspan="3"><?php echo ((is_array($studentinfo) && array_key_exists('program',$studentinfo))?$studentinfo['program']:'');?></td></tr>
								<tr><td width="20%">Curriculum :</td><td colspan="3"><?php echo ((is_array($studentinfo) && array_key_exists('curriculum',$studentinfo))?$studentinfo['curriculum']:'');?></td></tr>					
								<tr><td width="20%">Year Level :</td><td colspan="3"><?php echo ((is_array($studentinfo) && array_key_exists('yrlvl',$studentinfo))?$studentinfo['yrlvl']:'');?></td></tr>
								<tr><td width="20%">Fees Template :</td><td colspan="3"><?php echo ((is_array($studentinfo) && array_key_exists('tempcode',$studentinfo))?$studentinfo['tempcode']:'');?></td></tr>
								<tr><td width="20%">Min Load Units :</td><td id="minload"><?php echo ((is_array($studentinfo) && array_key_exists('minunit',$studentinfo))?$studentinfo['minunit']:'');?></td>
								    <td width="20%">Max Load Units :</td><td id="maxload"><?php echo ((is_array($studentinfo) && array_key_exists('maxunit',$studentinfo))?$studentinfo['maxunit']:'');?></td></tr>
								<tr class="<?php echo ((is_array($studentinfo) && array_key_exists('retentionid',$studentinfo) && $studentinfo['retentionid']==1)?'hidden':'');?>"><td width="20%">Retention :</td><td id="retention" colspan="3" data="<?php echo ((is_array($studentinfo) && array_key_exists('retentionid',$studentinfo))?$studentinfo['retentionid']:'');?>"><?php echo ((is_array($studentinfo) && array_key_exists('retention',$studentinfo))?$studentinfo['retention']:'');?></td></tr>
								<tr><td width="20%">Status :</td><td id="acad_stats" colspan="3"><?php echo ((is_array($studentinfo) && array_key_exists('status',$studentinfo))?$studentinfo['status']:'Regular');?></td></tr>
							 </table>
							<br>	   
						</div>
					</div>		  
				</div>
		  </div>			
					
		  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
				<header role="heading"><h2>Schedule for Advised Subjects</h2>					
					<div class="jarviswidget-ctrls" role="menu">						
						<a id='xmin1' href="#" onclick="$('#loader1').fadeIn();$('#subjinfo').slideToggle(500);$('#xmin1').hide();$('#xmax1').show();$('#loader1').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
						<a id='xmax1' style='display:none;' href="#" onclick="$('#loader1').fadeIn();$('#subjinfo').slideToggle(500);$('#xmin1').show();$('#xmax1').hide();$('#loader1').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>						
					</div>				
					<span id='loader1' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
				</header>
				<div class="row">
					<?php echo form_open('advising',array('id'=>'register','onsubmit'=>'return minloadwarning();')); ?>					
						<input type="hidden" id="hfAdviseID" name="adviseid" value="<?php echo ((is_array($studentinfo) && array_key_exists('advisedid',$studentinfo))?$studentinfo['advisedid']:'');?>">
						<input type="hidden" id="hfTermID" name="termid" value="<?php echo ((is_array($studentinfo) && array_key_exists('activetermid',$studentinfo))?$studentinfo['activetermid']:'');?>">
						<input type="hidden" id="hfcampusid" name="campusid" value="<?php echo ((is_array($studentinfo) && array_key_exists('campusid',$studentinfo))?$studentinfo['campusid']:'');?>">
						<input type="hidden" id="hfProgID" name="progid" value="<?php echo ((is_array($studentinfo) && array_key_exists('progid',$studentinfo))?$studentinfo['progid']:'');?>">
						<input type="hidden" id="hfCurrID" name="currid" value="<?php echo ((is_array($studentinfo) && array_key_exists('curriculumid',$studentinfo))?$studentinfo['curriculumid']:'');?>">
			            <input type="hidden" id="hfToDelete" name="todelete" value=""/>
					
					<div class="widget-body no-padding" role="content">
					 <?php
					    $instruc    = '';
					    $selsection = '';
					    $buttons    = '';
					    $optctrl    = (($optctrl==2)?$studentinfo['isreg']:$optctrl);
						
						if(isset($optctrl) && $optctrl==0)
						{
						 $instruc='<div class="alert alert-info no-margin fade in"><button class="close" data-dismiss="alert"> x </button><i class="fa-fw fa fa-info"></i> Double Click the subject to select the schedules. Make sure to fill all subjects with schedules. Then click Register to finalize your registration.</div>';
						 $buttons='<button id="btnchange" class="btn btn-sm btn-warning pull-left disabled" type="button" onclick="popschedule()" > Change Schedule </button>
								   <button id="btnremove" class="btn btn-sm btn-warning pull-left disabled" style="margin-left: 5px;" type="button" onclick="remschedule()"> Remove Schedule </button>';																									
						
						 if(isset($snic) and $snic==1)
						 {$buttons.='<button id="btnadd" class="btn btn-sm btn-primary pull-left" style="margin-left: 5px;" type="button" onclick="addsbj()" ><i class="fa fa-plus-circle"></i> Add Subject </button>
									 <button id="btndelete" class="btn btn-sm btn-danger pull-left" disabled="disabled" style="margin-left: 5px;" type="button" onclick="deladvsbj()"><i class="fa fa-times-circle"></i> Delete Subject</button>';}
					 
						}elseif(isset($optctrl) && $optctrl==1){
						 $instruc = '<div class="alert alert-info no-margin fade in"><button class="close" data-dismiss="alert"> &times </button><i class="fa-fw fa fa-info"></i> Select a section from the dropdown. Then click Register to finalize your registration.</div>
									 <div class="alert alert-warning no-margin fade in"><button class="close" data-dismiss="alert"> x </button><i class="fa-fw fa fa-warning"></i> If the columns schedule and section stays blank after selecting a section, it may be full or not available for the selected section.</div>';
						 $optsection= (isset($optsection))? $optsection:'';
						 $selsection = '<div class="col-sm-12">
										<table width="100%"><tr><td width="5%"><label>Section:</label></td><td><select id="optsection" class="form-control" onchange="optsectionselect()"><option value="" disabled selected>Select a class section</option>'.$optsection.'</select></td></tr></table>
										<br/>
									   </div>';
						}
					 ?>
						<div class="instruc_overall">
						<?php echo $instruc;?>
						<?php echo $selsection;?>						
						</div>
						<div class="instruc_reten hidden">
						<div class="alert alert-danger no-margin fade in"><button class="close" data-dismiss="alert"> &times </button><i class="fa-fw fa fa-warning"></i> Delete a subject by clicking the (<i class="fa fa-lg fa-times"></i>) to fit the 'UNITS TO ENROLL' to 'MAX LOAD UNITS'.</div>
						</div>
						<?php if($row){echo $row;}?>
						<div>
						<table class="table table-bordered" style="background-color:#d6d6d6; font-size: x-small; font-weight: bold;">
						<tr>
						<td width="55%">-</td>
						<td>Total Subject(s) to Enroll:</td><td id="totalesbj" width="10%" style="font-size:13px;">0</td>
						<td>Total Unit(s) to Enroll:</td><td id="totaleunit" width="10%" style="font-size:13px;">0.0</td>
						</tr>
						</table>
						</div>	   
						
					<div class="widget-footer">
						<button id="btnregister" type="submit" class="btn btn-labeled btn-success"> <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>Register </button>						
						<?php echo $buttons;?>
					</div> <!-- widget-footer-->
					</div>
					
					</form>
				</div>
			</div>
		  </div>
		  

		</div>
	 </div><!-- /.row -->
  </div>
  <div id="xpage" class="hidden">3</div>
<!-- Create Modal -->
  <div class="modal fade" id="mwindow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	 <div class="modal-dialog" style="width:900px;">
		<div class="modal-content">
		  <div class="modal-header">
			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			 <h4 class="modal-title" id="lblmwindow"><strong>Offered</strong> <i>Schedules</i></h4>
		  </div>	               
			 <div class="modal-body">
				<div class="alert alert-info no-margin fade in">
					<p>
					Kindly select your schedule. <i class="fa-fw fa fa-check-circle" style="color:Green;" ></i> Approved
					| <i class="fa-fw fa fa-times-circle" style="color:Red;" ></i> Disapproved |
					<i class="fa-fw fa fa-spinner" style="color:Blue;" ></i> Waiting
					<label style="color: red;" >&nbsp;&nbsp; <small> Note: There are <b>2</b> valid request for each subject. Thank you </small>  </label> 
					</p>
					
				</div>
				<div class="custom-scroll table-responsive" style=" cursor: pointer; margin-top: 10px; height:330px; width: 850px; overflow-y: scroll;">
					<table id="oschedule" class="table table-bordered table-hover">
						<thead>
							<tr class="success">
								<th>No.</th>
                                <th>S.ID</th>
								<th>Program</th>
								<th>Section</th>
								<th>Course</th>
								<th align="center">Lec</th>
								<th align="center">Lab</th>
								<th align="center">Limit</th>
								<th>Schedule</th>								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>C87E48EF-605A-B4FF</td>
								<td>erat@montesnasceturridiculus.org</td>
								<td>10/03/13</td>
							</tr>							
						</tbody>
					</table>							
				</div>						
			</div>
			<div class="modal-footer">				
				<button id="btnrequest" type="button" class="btn btn-warning pull-left disabled" rel="tooltip" data-placement="top" data-original-title="<i class='fa fa-check text-success'></i> You have no request schedule(s)" data-html="true" >Send Request</button>				
				<button id="btnprepared" type="button" class="btn pull-left disabled">					
					<i class="glyphicon glyphicon-ok"></i>
					preferred block section
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button id = "btnsubmit" type="button" class="btn btn-primary" value="submit" name="submit" >Select</button>						
			</div>		  
		</div>
	 </div>
  </div>

  <div class="modal fade" id="addsubjmodal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	 <div class="modal-dialog" style="width:600px;">
		<div class="modal-content">
		  <div class="modal-header">
			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			 <h4 class="modal-title" id="lblmwindow"><strong>Offered</strong> <i>Subjects</i></h4>
		  </div>	               
			 <div class="modal-body">
				<div class="alert alert-info no-margin fade in">
					<p>
					Kindly select your subject.
					</p>
				</div>
				<br>
				<ul id="subjtab" class="nav nav-tabs bordered">
				 <li id="subjtab1" class="active">
				  <a href="#divsubjIC" data-toggle="tab">
				  Subject(s) In Curriculum
				  </a>
				 </li>
				 <li id="subjtab2">
				  <a href="#divsubjNIC" data-toggle="tab">
				   Subject(s) Not In Curriculum
				  </a>
				</li>
				</ul>
				<div class='tab-content padding-10' style="height:330px;">
				<div  id ="divsubjIC" class="tab-pane fade in active" style="height:300px; cursor: pointer; overflow: scroll;">
				<div class="table-responsive">
					<table class="table table-bordered table-hover" style="font-size:small;">
						<thead>
							<tr class="success">
								<th>No.</th>
                                <th>Code</th>
								<th>Description</th>
								<th align="center">Lec</th>
								<th align="center">Lab</th>				
							</tr>
						</thead>
						<tbody id="osubjIC">
							<tr>
								<td colspan=5><center>No Subject available.</center></td>
							</tr>							
						</tbody>
					</table>							
				</div>
				</div>
				
				<div id ="divsubjNIC" class="tab-pane fade in" style="height:300px; cursor: pointer; overflow: scroll;">
				<div  class="table-responsive">
					<table class="table table-bordered table-hover" style="font-size:small;">
						<thead>
							<tr class="success">
								<th>No.</th>
                                <th>Code</th>
								<th>Description</th>
								<th align="center">Lec</th>
								<th align="center">Lab</th>									
							</tr>
						</thead>
						<tbody id="osubjNIC">
							<tr>
								<td colspan=5><center>No Subject available.</center></td>
							</tr>							
						</tbody>
					</table>							
				</div>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button id = "btnsubjadd" type="button" class="btn btn-primary" value="submit" name="submit" disabled="disabled" onclick="checknaddsubj();">Select</button>						
			</div>		  
		</div>
	 </div>
  </div>
  
  
  