
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>">Home</a></li>
		<li><a id = "advurl" href="<?php echo site_url('advising'); ?>">Advising</a></li>
	</ol>
</div>   
<div id="content">	 
	<div class="row">
	    <div class="col-sm-12">
	    <?php if($idtype==-1 || $idtype==2){echo $this->load->view('include\studentinfo','',true);}?>
		</div>
		<section id="widget-grid" class="">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
         <div class="jarviswidget jarviswidget-color-blue" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">		 
			 <header role="heading">
			  <h2>
			   <span class="fa fa-user"></span>
			   Enrollment Information
			  </h2>
			  <!--
			  <div class="widget-toolbar" role="menu">
			   <button class="btn btn-default btntrial" onclick="alertmsg('danger','Trial lang po!');">Trial</button>
			  </div>
			  -->
			 </header>
			 
			 <div id='advisinginfo' role="content" class="jarviswidget-body">
			  <div class="row">
				<?php
				If(isset($megalock) && ($megalock==1 || $megalock==true))
				{
				 $btnadv='id="xadvise" class="btn btn-default xmargin-left-2 disabled" disabled="disabled"';
				 $btnreg='id="xreg" class="btn btn-default xmargin-left-2 disabled" disabled="disabled"';
				 $btnpurge='id="xpurge" class="btn btn-default xmargin-left-2 disabled" disabled="disabled"';
				 $btnprint='disabled="disabled"';
				 $btnpreg='id="btnpreg" class="btn btn-default xmargin-left-2 disabled" disabled="disabled"';
				 $lipreg='id="btnpreg" disabled="disabled"';
				 $btncor='id="prntcor" class="btn btn-default xmargin-left-2 disabled" disabled="disabled"';
				 $licor='id="printcor" disabled="disabled"';
				 $btnpay='id="payment" class="btn btn-default xmargin-left-2 disabled" disabled="disabled"';
				 
				 if(isset($advising_data['xadv']) && ($advising_data['xadv']==1 || $advising_data['xadv']==true))
				 {$btnadv='id="xadvise" class="btn btn-primary xmargin-left-2"';}
				 if(isset($advising_data['xenr']) && ($advising_data['xenr']==1 || $advising_data['xenr']==true))
				 {$btnreg='id="xreg" class="btn btn-success xmargin-left-2"';}
				 if(isset($advising_data['xprt']) && ($advising_data['xprt']==1 || $advising_data['xprt']==true))
				 {$btnpreg='id="btnpreg" class="btn btn-default xmargin-left-2"';$lipreg='id="btnpreg"';$btnprint='';}
				 if(isset($advising_data['xcor']) && ($advising_data['xcor']==1 || $advising_data['xcor']==true))
				 {$btncor='id="prntcor" class="btn btn-default xmargin-left-2"';$licor='id="printcor"'; $btnprint='';}
				 if(isset($advising_data['xpay']) && ($advising_data['xpay']==1 || $advising_data['xpay']==true))
				 {$btnpay='id="payment" class="btn btn-default xmargin-left-2"';}
				 
				 
				 echo '<div class="btn-advctrl xmargin-bottom-5 xmargin-right-5 pull-right">
						<button type="button" '.$btnadv.'><span class="fa fa-fw fa-list-alt"></span> Advising</button>
						<button type="button" '.$btnreg.'><span class="fa fa-fw fa-table"></span> Enrollment</button>
						<button type="button" class="btn btn-danger xmargin-left-3 hidden" disabled="disabled" style="display:none;">Purge Enrollment</button>
						<span class="hidden-xs">
						<button type="button" '.$btnpreg.'><span class="fa fa-fw fa-print"></span> Print Pre-Reg</button></a>
						<button type="button" '.$btncor.'><span class="fa fa-fw fa-print"></span> Print C.O.R</button>
						</span>
						<span class="btn-group hidden-sm hidden-md hidden-lg">
				         <button class="btn btn-default" data-toggle="dropdown" '.$btnprint.'><i class="fa fa-print"></i> Print <i class="fa fa-caret-down"></i></button>
				         <ul class="dropdown-menu pull-right text-align-left txt-color-black bg-color-white" style="width:240px !important;">
					      <li><a href="#" '.$lipreg.'><i class="fa fa-file"></i> Pre-Registration</a></li>
						  <li><a href="#" '.$licor.'><i class="fa fa-file"></i> Certificate of Registration</a></li>
						 </ul>
						</span>
						'.((isset($advising_data['xpay']))?'<button type="button" '.$btnpay.'><span class="fa fa-fw fa-money"></span> Payment</button>':'').
					   '</div>';
				}
				?>
                <div class="alert alert-maintenance alert-warning <?php echo (($megalock==1)?'hidden':'');?>"><i class="fa-fw fa fa-warning"></i> This Module is disabled by the Administrator.</div>				
			   </div>
			   <div class="row <?php echo (($megalock==0)?'hidden':'');?>">
			   <?php if(isset($this->RegOnly) and $this->RegOnly==1){?>
			      <div class="alert alert-info xmargin-bottom-5"><i class="fa-fw fa fa-exclamation"></i> Online Advising is only applicable for students that are qualify from the criteria given by the registar.</div><?php }?>
			   <?php if($idtype > -1 && isset($advschedctrl) and $advschedctrl['inperiod']==0){?>
			      <div class="alert alert-danger"><i class="fa fa-warning"></i> Online Advising Period for your Academic Program is not yet open. 
				  <?php echo (($advschedctrl['dis_datestart']!='')? 'Your Academic Program is scheduled from <strong>'.$advschedctrl['dis_datestart'].'</strong> to <strong>'.$advschedctrl['dis_dateend'].'</strong>':'');?></div><?php } ?>
			   </div>
			   <!--<div class="alert alert-warning"><i class="fa-fw fa fa-warning"></i> Online Printing is under maintenance.</div>-->
			   <div id="advisingcontent" class="tab-content">
                <table class="table table-bordered">
				 <tbody>
				  <tr><td width="30%">Academic Year & Term:</td><td id="xayterm"><?php if(isset($advising_data['activeterm'])){ echo $advising_data['activeterm'];}?></td></tr>
				  <tr><td width="30%">Campus Name:</td><td id="xcampus"><?php if(isset($advising_data['campus'])){ echo $advising_data['campus'];}?></select></td></tr>
				  <tr><td width="30%">Advising Period:</td><td id="xadvising"><?php if(isset($advising_data['advperiod'])){ echo $advising_data['advperiod'];}?></td></tr>
				  <tr><td width="30%">Enrollment Period:</td><td id="xenrollment"><?php if(isset($advising_data['enperiod'])){ echo $advising_data['enperiod'];}?></td></tr>
				  <tr><td width="30%">StudentNo:</td><td id="xstudno"><?php if(isset($studentno)){ echo $studentno;}?></td></tr>
				  <tr><td width="30%">Registration ID:</td><td id="xregid"><?php if(isset($advising_data['regid'])){ echo $advising_data['regid'];}?></td></tr>
				  <tr><td width="30%">Registration Date:</td><td id="xregdate"><?php if(isset($advising_data['regdate'])){ echo $advising_data['regdate'];}?></tr>
				  <tr><td width="30%">College:</td><td id="xcollege"><?php if(isset($advising_data['college'])){ echo $advising_data['college'];}?></td></tr>
				  <tr><td width="30%">Academic Program:</td><td id="xprogram"><?php if(isset($advising_data['program'])){ echo $advising_data['program'];}?></td></tr>
				  <tr><td width="30%">Curriculum:</td><td id="xcurriculum"><?php if(isset($advising_data['curriculum'])){ echo $advising_data['curriculum'];}?></td></tr>
				  <tr><td width="30%">Year Level:</td><td id="xyrlvl"><?php if(isset($advising_data['yrlvl'])){ echo $advising_data['yrlvl'];}?></td></tr>
				  <tr><td width="30%">Accountabilities:</td><td id="xaccount"><?php if(isset($advising_data['accounts'])){ echo $advising_data['accounts'];}?></td></tr>
				  <tr class="<?php echo ((isset($advising_data['retentionid']) && $advising_data['retentionid']!=1)?'':'hidden');?>"><td width="30%">Retention:</td><td id="xretention" data-retention="<?php echo $advising_data['retentionid'];?>"><?php echo $advising_data['retention']; ?></td></tr>
				  <tr><td width="30%">Fees Template:</td><td id="xfeestemp"><?php if(isset($advising_data['tempcode'])){ echo $advising_data['tempcode'];}?></td></tr>
				  <tr><td width="30%">Outstanding Balance:</td><td id="xbalance"><?php if(isset($advising_data['balance'])){ echo $advising_data['balance'];}?></td></tr>
				  <tr><td width="30%">Status:</td><td><strong id="xstats" data-reg="<?php if(isset($advising_data['isreg'])){echo $advising_data['isreg'];}?>"><?php if(isset($advising_data['status'])){ echo $advising_data['status'].' '.(($advising_data['btnstatus'])?$advising_data['btnstatus']:'');}?></strong></td></tr>
				  <?php echo ((isset($pstats) && $pstats!='')?('<tr><td>Payment Status:</td><td><strong id="xpstats">'.$pstats.'</strong></td></tr>'):'');?>
				 </tbody>
				</table>
			   </div>
            </div>
          </div>			 
		</div>
	</div>
	
	
</div>
<div id="xpage" class="hidden">1</div>
<?php
 $xschedadv=(isset($xalert) and array_key_exists('schedadv',$xalert))?$xalert['schedadv']:0;
 $xschedenr=(isset($xalert) and array_key_exists('schedenr',$xalert))?$xalert['schedenr']:0;
 $xregonly=(isset($xalert) and array_key_exists('regonly',$xalert))?$xalert['regonly']:0;
 $xbalance=(isset($xalert) and array_key_exists('balance',$xalert))?$xalert['balance']:0;
 $xaccount=(isset($xalert) and array_key_exists('account',$xalert))?$xalert['account']:0;
 $xinactive=(isset($xalert) and array_key_exists('inactive',$xalert))?$xalert['inactive']:0;
 echo '<div id="xwarningid" class="hidden" data-schedadv="'.$xschedadv.'" data-schedenr="'.$xschedenr.'" data-regonly="'.$xregonly.'" data-balance="'.$xbalance.'" data-account="'.$xaccount.'" data-inactive="'.$xinactive.'"></div>';
?>

