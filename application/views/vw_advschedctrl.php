<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>">Home</a></li>
		<li><a id="setup" href="javascript:void(0);">Advising</a></li>
		<li><a id="setup" href="<?php echo site_url('advschedctrl'); ?>">Advising Schedule Ctrl</a></li>
	</ol>
</div>   
<div id="content">	
<div class="row">
 <div class="col-sm-7">
 <div class="panel panel-darken">
	<div class="panel-heading">
	<h3 class="panel-title">Advising Schedule</h3>
	</div>
	<div class="panel-body">
	 <div class="row">
	  <div class="col-sm-4">
	   <button class="btn btn-primary btnrefresh"><i class="fa fa-refresh"></i> Refresh</button>
	   <button class="btn btn-success btnnew hidden"><i class="fa fa-file"></i> New</button>
	  </div>
	  <div class="col-sm-8">
	  AY Term:
	  <select class="form-control" name="term" id="term">
	  <option value="-1" disabled selected>- Select One Term -</option>
	  <?php 
	   if(isset($ayterm))
	   {
		 foreach($ayterm as $term)
	     {
	      if(@property_exists($term,'TermID') && property_exists($term,'AcademicYearTerm'))
		  {	  
		   echo '<option value="'.$term->TermID.'">'.$term->AcademicYearTerm.'</option>';
		  } 
		 }		 
	   }	   
	  ?>
	  </select>
	  </div>
	 </div>
	 <br/>
	 <div class="row" style="min-height:250px; max-height:600px;overflow:auto;">
	 <table class="table table-bordered table-condense" id="tblist">
	  <thead>
	   <th width="80px"></th>
	   <th>Program</th>
	   <th>YearLevel</th>
	   <th>Start</th>
	   <th>End</th>
	   <th>Inactive</th>
	  </thead>
	  <tbody>
	  </tbody>
	 </table>
	 </div>
	</div>
 </div>
 </div>
 <div class="col-sm-5">
   <div class="panel panel-darken">
	<div class="panel-heading">
	<h3 class="panel-title">Form</h3>
	</div>
	<div class="panel-body">
	 <form class="form-horizontal" onsubmit="return false;">
	  <div class="form-group">
       <label class="col-md-2 control-label">Program:</label>
       <div class="col-md-10">
        <select class="form-control" name="program" id="program" placeholder="Select one program" required>
		 <option value="-1" selected disabled> -Select One Program- </option>
		 <?php
		  if(isset($program))
		  {
			foreach($program as $prog)
            {
			  echo '<option value="'.$prog->ProgID.':'.$prog->MajorID.'" data-prog="'.$prog->ProgID.'" data-major="'.$prog->MajorID.'">'.$prog->ProgName.(($prog->MajorID!=0)? (' Major in '.$prog->MajorName) : '' ).'</option>';	
			}			
		  }	  
		 ?>
		</select>
       </div>
	  </div>
	  <div class="form-group">
       <label class="col-md-2 control-label">YearLevel:</label>
       <div class="col-md-10">
        <select class="form-control" name="yrlvl" id="yrlvl" placeholder="Select one yearlevel" required>
		 <option value="-1" selected disabled> -Select One Yearlevel- </option>
		 <option value="0"> All </option>
		 <?php
		  $yrlvl = $this->db->query("SELECT YearLevelID,YearLevel FROM ES_YearLevel");
		  foreach($yrlvl->result() as $yr)
          {
		    echo '<option value="'.$yr->YearLevelID.'">'.$yr->YearLevel.'</option>';	
		  }	  
		 ?>
		</select>
       </div>
	  </div>
	  <div class="form-group">
	   <div class="col-sm-6">
        <label class="col-md-4 control-label">From:</label>
        <div class="col-md-8">
         <input class="form-control datepicker" name="datestart" id="datestart" data-dateformat="mm/dd/yy" required/>
         <input class="form-control timepicker" id="timestart" type="text" placeholder="Select time"/>
        </div>
       </div>
	   <div class="col-sm-6">
        <label class="col-md-2 control-label">To:</label>
        <div class="col-md-10">
         <input class="form-control datepicker" name="dateend" id="dateend" data-dateformat="mm/dd/yy" required/>
         <input class="form-control timepicker" id="timeend" type="text" placeholder="Select time"/>
        </div>
	   </div>	
      </div>	
      <div class="form-group">
       <label class="col-md-2 control-label"></label>
       <div class="col-md-10">
		<div class="checkbox">
		<label style="z-index:1">
		<input type="checkbox" class="checkbox style-0" name="inactive" id="inactive" style="z-index:-1"/>
		<span style="z-index:1">Inactive</span>
		</label>
		</div>
       </div>
      </div>
      <div class="form-group">
	   <div class="col-sm-12">
	    <button class="btn btn-default pull-right" type="button" id="btncancel">Cancel</button>
	    <button class="btn btn-warning pull-right" type="submit" id="btnsave"><i class="fa fa-save"></i> Save</button>
       </div>
	  </div>	  
	 </form>
	</div>
   </div>	
 </div>

</div>
</div>