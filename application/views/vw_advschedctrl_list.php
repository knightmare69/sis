 <?php
  if(isset($list))
  {	  
   foreach($list as $rs)
   {
    $indx      = ((property_exists($rs,'IndexID'))? ($rs->IndexID) : 'new');
    $termid    = ((property_exists($rs,'TermID'))? ($rs->TermID) : -1);
    $progid    = ((property_exists($rs,'ProgID'))? ($rs->ProgID) : -1);
    $progname  = ((property_exists($rs,'ProgName'))? ($rs->ProgName) : '');
    $majorid   = ((property_exists($rs,'MajorID'))? ($rs->MajorID) : 0);
    $yrlvl     = ((property_exists($rs,'YearLevelID'))? ($rs->YearLevelID) : 0);
    $yrlvlname = ((property_exists($rs,'YearLevel'))? ($rs->YearLevel) : '');
    $majorname = ((property_exists($rs,'MajorName'))? ($rs->MajorName) : '');
    $dstart    = ((property_exists($rs,'DateStart'))? ($rs->DateStart) : '');
    $dend      = ((property_exists($rs,'DateEnd'))? ($rs->DateEnd) : '');
    $inactive  = ((property_exists($rs,'Inactive'))? ($rs->Inactive) : 0);
	
	if($dstart!='')
	{
	  $tmpdate = new DateTime($dstart);
      $dstart = $tmpdate->format('m/d/Y h:i A');	  
	}	
	
	if($dend!='')
	{
	  $tmpdate = new DateTime($dend);
      $dend    = $tmpdate->format('m/d/Y h:i A');	  
	}	
	
	if($majorname!='')
	{
	  $progname = $progname.'<br/> Major in '.$majorname;	
	}	
	
	if($progid!='' and $progid!='-1')
	{	 
     $btn = '';
	 if($indx=='new' || $indx=='')
	  $btn = '<button class="btn btn-sm btn-warning btnedit"><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-danger btnremove" disabled><i class="fa fa-trash-o"></i></button>';	 
	 else
	  $btn = '<button class="btn btn-sm btn-warning btnedit"><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-danger btnremove"><i class="fa fa-trash-o"></i></button>';	 
	 	 
	 echo '<tr id="'.(($indx=='')?'new':$indx).'" data-prog="'.$progid.'" data-major="'.$majorid.'" data-yrlvl="'.$yrlvl.'" data-inactive="'.$inactive.'">
	        <td width="80px">'.$btn.'</td>
	        <td>'.$progname.'</td>
	        <td>'.(($yrlvl<=0)?'All':$yrlvlname).'</td>
	        <td>'.$dstart.'</td>
	        <td>'.$dend.'</td>
	        <td class="text-center" ><i class="fa '.(($inactive==0)?'fa-square-o':'fa-check-square-o').'"></i></td>
           </tr>'; 
    }
	
   }		 
  }	   
 ?>	   
