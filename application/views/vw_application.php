
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Admission</li>
		<li>Application</li>
	</ol>
</div>
<!-- MAIN CONTENT -->
<div id="content">
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw "></i> Admission <span>>
				Applications </span></h1>
		</div>
	<?php
	if($userinfo['appcount']==0) 
	echo '<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
		   <div class="well no-padding pull-right">
			<a class="btn btn-sm btn-warning" href="'.site_url('activation').'">
				<strong>Are you a <?php echo INST_STUD;?>?</strong>
			</a>					
		   </div>
		 </div>';
	?>
	</div>
	<!-- widget grid -->
	<section id="widget-grid" class="">
		<div class="row">		
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" data-widget-deletebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>Application List</h2>
					</header>					
					<div>						
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body">
							<div class="alert alert-info fade in">
								<button class="close" data-dismiss="alert">
									x
								</button>
								<i class="fa-fw fa fa-info"></i>
								Kindly click <strong>"New Application"</strong> to begin your admission									
							</div>
							<table class="table table-bordered table-striped table-hover" style="cursor: pointer;">
								<thead>
									<tr>
										<th>Applicant No.</th>
										<th>Year & Term</th>							
										<th>Date</th>							
										<th>Type</th>
										<th>Course 1</th>
										<th>Course 2</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>						
										<?php
												foreach ($app as $rows){
													echo  '<tr id="'. $rows->Appno  .'">';
													echo  '<td><a href="'.base_url().'admission/applicant/'.$rows->Appno.'" ><i class="fa fa-lg fa-fw fa-edit"></i></a>
													           <a href="'.base_url().'admission/printapp/'.$rows->Appno.'" ><i class="fa fa-lg fa-fw fa-print"></i></a>
													           <a href="'.base_url().'admission/printout/'.$rows->Appno.'" ><i class="fa fa-lg fa-fw fa-print"></i></a>'. $rows->Appno . '</td>';
													echo  '<td>'. $rows->AYTerm . '</td>';
													echo  '<td>'. $rows->Appdate . '</td>';
													echo  '<td>'. $rows->Applicationtype . '</td>';
													echo  '<td>'. $rows->Acadprogram1 . '</td>';
													echo  '<td>'. $rows->Acadprogram2 . '</td>';
													echo  '<td>'. $rows->AdmStatus . '</td>';
													echo  '</tr>';
												}
										?>							
								</tbody>
							</table>
									
							<div class="well well-sm no-padding">						
								<a class="btn btn-sm btn-success hidden" style="margin: 10px;" href="<?php echo site_url('admission/application'); ?>">
									New Application
								</a>						
							</div>								
						</div> <!-- end widget content -->
					</div>
					<!-- end widget div -->
				</div>
				<!-- end widget -->
			</article>
			<!-- WIDGET END -->
		</div>
		<!-- end row -->
	</section>
	<!-- end widget grid -->
	</div>





