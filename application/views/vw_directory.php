<table class="table table-condense" style="white-space:nowrap;">
 <tbody>
 <?php
  function sub_list($sub=array())
  {
   $xmain_path=str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']);
   $output='';
   $output.='<table class="table table-condense" style="white-space:nowrap;"><tbody>';
   $dirs='';
   $files='';
   foreach($sub as $k=>$v)
   {
	if(!array_key_exists('sub',$v))
	{
	 $link = str_replace($xmain_path,base_url(),$v['path']);	
     if(strpos($v['name'],'.zip') >0 || strpos($v['name'],'.rar') >0)
	  $files.='<tr class="table-file" data-editable="false" data-name="'.$v['name'].'" data-path="'.$v['path'].'" link-path="'.$link.'"><td><i class="fa fa-th-list txt-color-purple"></i> '.$v['name'].'</td></tr>';
     else if(strpos($v['name'],'.jpeg') >0 || strpos($v['name'],'.jpg') >0 || strpos($v['name'],'.png') >0 || strpos($v['name'],'.png') >0 || strpos($v['name'],'.gif') >0)
	  $files.='<tr class="table-file" data-editable="false" data-name="'.$v['name'].'" data-path="'.$v['path'].'" link-path="'.$link.'"><td><i class="fa fa-file-image-o"></i> '.$v['name'].'</td></tr>';
     else
	  $files.='<tr class="table-file" data-editable="true" data-name="'.$v['name'].'" data-path="'.$v['path'].'" link-path="'.$link.'"><td><i class="fa fa-file-o"></i> '.$v['name'].'</td></tr>';
      	
	}
    else
	{
	 $dirs.='<tr class="table-parent" data-name="'.$v['name'].'" data-path="'.$v['path'].'"><td><i class="fa fa-folder txt-color-yellow"></i> '.$v['name'].'</td></tr>';		
     $dirs.='<tr class="table-child hidden"><td class="no-padding">'.sub_list($v['sub']).'</td></tr>'; 	
	}
   }
   $output.=$dirs.$files.'</tbody></table>';   
   return $output;
  }
  
  if(isset($directory))
  {
   $xmain_path=str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']);	  
   $dirs='';
   $files='';
   foreach($directory as $k=>$v)
   {
	if(!array_key_exists('sub',$v))
	{
     $link = str_replace($xmain_path,base_url(),$v['path']);	
     if(strpos($v['name'],'.zip') >0 || strpos($v['name'],'.rar') >0)
	  $files.='<tr class="table-file" data-editable="false" data-name="'.$v['name'].'" data-path="'.$v['path'].'" link-path="'.$link.'"><td><i class="fa fa-th-list txt-color-purple"></i> '.$v['name'].'</td></tr>';
     else if(strpos($v['name'],'.jpeg') >0 || strpos($v['name'],'.jpg') >0 || strpos($v['name'],'.png') >0 || strpos($v['name'],'.png') >0 || strpos($v['name'],'.gif') >0)
	  $files.='<tr class="table-file" data-editable="false" data-name="'.$v['name'].'" data-path="'.$v['path'].'" link-path="'.$link.'"><td><i class="fa fa-file-image-o"></i> '.$v['name'].'</td></tr>';
     else
	  $files.='<tr class="table-file" data-editable="true" data-name="'.$v['name'].'" data-path="'.$v['path'].'" link-path="'.$link.'"><td><i class="fa fa-file-o"></i> '.$v['name'].'</td></tr>';
     
	}
	else
	{
	 $dirs.='<tr class="table-parent" data-name="'.$v['name'].'" data-path="'.$v['path'].'"><td><i class="fa fa-folder txt-color-yellow"></i> '.$v['name'].'</td></tr>';		
     $dirs.='<tr class="table-child hidden"><td class="no-padding">'.sub_list($v['sub']).'</td></tr>'; 	
	}
   }   
   echo $dirs.$files;
  }	  
 ?>
 </tbody>
</table>