
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><a href="<?php echo site_url('download'); ?>"> Downloads</a></li>
	</ol>
	</ol>
</div>	
<div id="content">
		<div class="row">
			
			<div class="col-lg-12">
				<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
					 <h2 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-clipboard"></i> <?php echo $title; ?> <span></span></h2>
				</div>
			</div>
		<!--here -->
				<div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<table id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
	
							<table id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
							 <thead>
								<th><h4><strong>POLICIES ON SCHOOL FEES, SCHOLARSHIPS AND PAYMENTS</strong></h4></th>	 
							 </thead>									
								<tr><td><a href="<?php echo base_url().'downloads/Policies on School Fees, Scholarships and Payments.pdf'; ?>">Terms And Conditions</a> </td></tr>									
								
					</table>

						<table id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
	
							<table id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
							 <thead>
								<th><h4><strong>APPLICATION AND RECOMMENDATION FORMS</strong></h4></th>	 
							 </thead>									
								<tr><td><a href="<?php echo site_url('download/AARF1'); ?>">Admission Application Form</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/AARF2'); ?>">Class Adviser or Guidance Counselor's Recommendation Form </a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/AARF3'); ?>">Principal's Recommendation Form </a> </td></tr>										
								
					</table>
						
							<table id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
						
							 <thead>
								<th><h4><strong>IBED PRIMARY GRADE SCHOOL APPLICANTS</strong></h4></th>
								 
							 </thead>
							 <tbody>
								<tr><td><a href="<?php echo site_url('download/ibed11'); ?>">Admission Requirements</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed12'); ?>">Admission Procedure</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed13'); ?>">Schedule of Admission Test</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed14'); ?>">List of Qualified Examinees</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed15'); ?>">Schedule of Enrollment</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed16'); ?>">Enrollment Procedure</a> </td></tr>
							</tbody>
						</table>
						
						<table id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
							<thead>
								<th><h4><strong>IBED HIGHSCHOOL APPLICANTS</strong></h4></th>	 
							</thead>
							<tbody>
								<tr><td><a href="<?php echo site_url('download/ibed31'); ?>">Admission Requirements</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed32'); ?>">Admission Procedure</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed33'); ?>">Schedule of Admission Test</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed34'); ?>">List of Qualified Examinees</a> </td></tr>										
								<tr><td><a href="<?php echo site_url('download/ibed35'); ?>">Schedule of Enrollment</a> </td></tr>																		<tr><td><a href="<?php echo base_url(); ?>index.php/download/ibed36/">Enrollment Procedure</a> </td></tr>
							</tbody>
						</table>				
						<table id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
							 <thead>
								<th><h4><strong>IBED MIDDLE GRADE SCHOOL</strong></h4></th>								 
							 </thead>
									
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/ibed21/">Admission Requirements</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/ibed22/">Admission Procedure</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/ibed23/">Schedule of Admission Test</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/ibed24/">List of Qualified Examinees</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/ibed25/">Schedule of Enrollment</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/ibed26/">Enrollment Procedure</a> </td></tr>
								
																						
						</table>
						
						<table id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
							 <thead>
								<th><h4><strong>COLLEGE OF ARTS AND SCIENCE APPLICANTS</strong></h4></th>								 
							 </thead>
									
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/CASA1/">Admission Requirements</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/CASA2/">Admission Procedure</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/CASA3/">Schedule of Admission Test</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/CASA4/">List of Qualified Examinees</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/CASA5/">Schedule of Enrollment</a> </td></tr>										
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/CASA6/">Enrollment Procedure</a> </td></tr>
								<tr><td><a href="<?php echo base_url(); ?>index.php/download/CASA7/">Add and Drop Schedule and Procedure</a> </td></tr>
								
																						
						</table>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-4 col-lg-3">
					</div>
				</div>

		</div>
</div><!-- /.row -->
  