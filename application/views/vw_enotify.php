<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><a href="javascript:void(0);"> ENotify</a></li>
	</ol>
</div>   
<div id="content">	 
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		    <section id="widget-grid" class="">
			<div class="row">
			<article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-email0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"  data-widget-deletebutton="false">
					<header role="heading">
					 <h2><i class="fa fa-users"></i> Members</h2>
					 <div class="widget-toolbar" role="menu">
					  <button class="btn btn-primary btn-sm btnrefresh"><i class="fa fa-refresh"></i></button>
					 </div>
					</header>
					<div class="widget-body">
					<div class="col-xs-4">
                      <label><input type='checkbox' id='xselect' onclick="selectall();"/> Select all</label> 
                    </div>				
                    <div class="col-xs-8" style="padding-right:1px;">
                      <div class="input-group">
					   <input type='text' id='xfilter' data-target="#tblmember" class="form-control input-sm"/>
					   <span class="input-group-addon">
						<i class="fa fa-search"></i>
					   </span>
					  </div> 
                    </div>					
					<div id="xmembers" style="padding:2.5px;overflow:scroll;height:425px;">
					 <?php echo $xtable; ?>
                    </div>
					</div>
                </div>
            </article>
			<article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-email1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"  data-widget-deletebutton="false">
					<header role="heading">
					 <h2><i class="fa fa-envelope"></i> EMail</h2>
					 <div class="widget-toolbar" role="menu">
					 <?php
					 if(EMAIL_NOTIFY==1)
					 {	 
                      echo '<button id="xsend" class="btn btn-success" onclick="sendmail();" style="margin-right:2px;"><i class="fa fa-envelope"></i> Send</button>';
                      echo '<button id="xclear" class="btn btn-danger"><i class="fa fa-times"></i> Clear</button>';
					 }
					 ?>
                     </div>
					</header>		
					<div id="xemail" style="height:480px;">
						<table class="table table-bordered">
						<tbody>
						 <tr><td width="100px">Subject</td><td><input type="text" id="xsubj" class="form-control"/></td></tr>
						 <tr class="defaulteditor hidden"><td>Message</td><td><textarea id="xmsg" rows="20" class="form-control"></textarea></td></tr>
						 <tr class="summereditor"><td colspan="2"><div id="emailbody"></div></td></tr>
						</tbody>
						</table>
                    </div>
                </div>
            </article>
			</div>
            </section>		
		</div>
		<div id="xdiv">
		</div>
    </div>
</div>	