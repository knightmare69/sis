<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Registration</li>
	</ol>
</div>
<div id="content">
  	<div class="row">
		<div class="col-lg-12">
		  <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
			 <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> <?php echo $title; ?> <span>> Registrations</span></h1>
		  </div>
		  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			 <span class="pull-right" style="margin-top: 13px;">
            <?php  echo $_COOKIE["minified"]; ?>
				<button type="button" class="btn btn-primary"><i class="fa-fw fa fa-register"></i> Advising</button>				
			 </span>
		  </div>
		</div>
	</div>
</div>