<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>EPayment</li>		
	</ol>
</div>   
<div id="content" style="overflow:auto;">
 <div class="col-sm-12">
 <div class="jarviswidget jarviswidget-color-green xmargin-bottom-10 hidden" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="false" data-widget-custombutton="false">
	<header role="heading">
		<h2><i class="fa fa-user"></i> Student Info</h2>
		<div class="jarviswidget-ctrls" role="menu">
			<a id='xmin' href="#" onclick="$('#loader').fadeIn();$('#studinfo').slideToggle(500);$('#xmin').hide();$('#xmax').show();$('#loader').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
			<a id='xmax' style='display:none;' href="#" onclick="$('#loader').fadeIn();$('#studinfo').slideToggle(500);$('#xmin').show();$('#xmax').hide();$('#loader').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>
		</div>
		<span id='loader' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
		<div class="widget-toolbar hidden" role="menu">	
		</div>
	</header>
	<div id='studinfo' role="content">	
		<div class="jarviswidget-body no-padding">
			<div class="row" style="white-space:nowrap;">
			 <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 xprofile-pic text-align-center">
			  <img id="student-avatar" data-default="<?php echo base_url();?>assets/img/avatars/empty.png" src="<?php echo base_url().((is_key_exist($studentinfo,'StudentNo')<>'')?'home/pics/id pics/1/'.is_key_exist($studentinfo,'StudentNo') : 'assets/img/avatars/empty.png');?>"/>
			 </div>
			 <div class="col-xs-12 col-sm-12 col-md-8 col-lg-10">
			   <div class="col-sm-4 col-lg-2">Academic Year & Term :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'AyTerm','');?></div>
			   <div class="col-sm-4 col-lg-2">Registration No. :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'RegID','');?></div>
			   <div class="col-sm-4 col-lg-2">Student Name :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo ((is_key_exist($studentinfo,'StudentNo'))?is_key_exist($studentinfo,'LastName','').', '.is_key_exist($studentinfo,'Firstname','').' '.is_key_exist($studentinfo,'Middlename','').' ['.is_key_exist($studentinfo,'StudentNo').']':'');?></div>
			   <div class="col-sm-4 col-lg-2">Program/Course :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'Program');?></div>
			   <div class="col-sm-4 col-lg-2">Curriculum :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'Curriculum','');?></div>
			   <div class="col-sm-4 col-lg-2">Year Level :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'Yearlvl','');?></div>
			   <div class="col-sm-4 col-lg-2">Fees Template :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'FeesTemplate','');?></div>
			 </div>
			</div>
			<br>	   
		</div>
	</div>		  
 </div>
 </div>
 <div class="col-sm-12 col-md-12">
 <link rel="stylesheet" href="<?php echo base_url('assets/resources/style.css');?>">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
 <section class="shopping-cart dark">
	 		<div class="container">
		        <div class="block-heading text-center">
		          <p><img src="<?php echo base_url('assets/resources/logo-large.png');?>" style="width: 25%;"/></p>
		        </div>
		        <div class="content">
					<?php
						$_mid       = "000000110118836A18E8"; //<-- your merchant id
						$_requestid = substr(uniqid(), 0, 13);
						$_ipaddress = "192.168.10.1";
						$_noturl    = ""; // url where response is posted
						$_resurl    = base_url(); //url of merchant landing page
						$_cancelurl = base_url(); //url of merchant landing page
						$_fname     = is_key_exist($studentinfo,'Firstname',''); // kindly set this to first name of the cutomer
						$_fname     = (($_fname=='')?'Not Applicable':$_fname);
						$_mname     = is_key_exist($studentinfo,'Middlename',''); // kindly set this to middle name of the cutomer
						$_mname     = (($_mname=='')?'Not Specified':$_mname);
						$_lname     = is_key_exist($studentinfo,'LastName',''); // kindly set this to last name of the cutomer
						$_lname     = (($_lname=='')?'Not Specified':$_lname);
						$_addr1     = is_key_exist($studentinfo,'Perm_Address',''); // kindly set this to address1 of the cutomer
						$_addr1     = (($_addr1=='')?'638 Mendiola St':$_addr1);
						$_addr2     = is_key_exist($studentinfo,'Perm_Street','').' '.is_key_exist($studentinfo,'Perm_Barangay','');// kindly set this to address2 of the cutomer
						$_addr2     = ((trim($_addr2)=='')?'San Miguel':$_addr2);
						$_city      = is_key_exist($studentinfo,'Perm_TownCity',''); // kindly set this to city of the cutomer
						$_city      = (($_city=='')?'Manila':$_city);
						$_state     = is_key_exist($studentinfo,'Perm_Province',''); // kindly set this to state of the cutomer
						$_state     = (($_state=='')?'Metro Manila':$_state);
						$_country   = "PH"; // kindly set this to country of the cutomer ex. PH
						$_zip       = is_key_exist($studentinfo,'Perm_ZipCode',''); // kindly set this to zip/postal of the cutomer
						$_zip       = (($_zip=='')?'1005':$_zip);
						$_sec3d     = "try3d"; // 
						$_email     = is_key_exist($studentinfo,'Email',''); // kindly set this to email of the cutomer
						$_phone     = is_key_exist($studentinfo,'TelNo',''); // set to this ID number
						$_mobile    = is_key_exist($studentinfo,'MobileNo',''); // kindly set this to mobile number of the cutomer
						if(($_phone=='' || $_phone=='-') && ($_mobile!='' && $_mobile!='-')){
						    $_phone = $_mobile;
						}
						if($_phone=='' || $_phone=='-'){
						    $_phone = '027356011';
						}
						$_clientip  = $_SERVER['REMOTE_ADDR'];
						$t_amount   = is_key_exist($studentinfo,'Balance','0.00'); // value of payment
						$_amount    = number_format($t_amount,2, '.', ''); 
						$_currency  = "PHP"; //PHP or USD
						$forSign    = $_mid . $_requestid . $_ipaddress . $_noturl . $_resurl .  $_fname . $_lname . $_mname . $_addr1 . $_addr2 . $_city . $_state . $_country . $_zip . $_email . $_phone . $_clientip . $_amount . $_currency . $_sec3d;
						
						$cert       = "B850B12E400080A23AC282ABACE77C60"; //<-- your merchant key
                        $_sign      = hash("sha512", $forSign.$cert);
						$xmlstr     = "";
						$strxml     = "";
					?>
	 				<div class="row">
	 					<div class="col-md-12 col-lg-8">
	 						<div class="info-details">
								<h3 class="title">Payment Information</h3>
								<div class="row">
								  <div class="form-group col-sm-4">
									<label for="name-holder">First Name</label>
									<input id="fname" name="txtfname" type="text" class="form-control" placeholder="First Name" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_fname;?>" disabled>
								  </div>

								  <div class="form-group col-sm-4">
									<label for="name-holder">Middle Name</label>
									<input id="mname" name="txtmname" type="text" class="form-control" placeholder="Middle Name" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_mname;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-4">
									<label for="name-holder">Last Name</label>
									<input id="lname" name="txtlname" type="text" class="form-control" placeholder="Last Name" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_lname;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Address 1</label>
									<input id="address1" name="txtaddress1" type="text" class="form-control" placeholder="Address 1" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_addr1;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Address 2</label>
									<input id="address2" name="txtaddress2" type="text" class="form-control" placeholder="Address 2" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_addr2;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">City</label>
									<input id="city" name="txtcity" type="text" class="form-control" placeholder="City" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_city;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Country</label>
									<input id="country" name="txtcountry" type="text" class="form-control" placeholder="country" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_country;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Zip Code</label>
									<input id="zipcode" name="txtzip" type="text" class="form-control" placeholder="country" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_zip;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Email</label>
									<input id="email" name="txtemail" type="text" class="form-control" placeholder="Email" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_email;?>" disabled>
								  </div>
								  
								  <div class="form-group col-sm-6">
									<label for="name-holder">Mobile Number</label>
									<input id="number" name="txtnumber" type="text" class="form-control" placeholder="Mobile Number" aria-label="info Holder" aria-describedby="basic-addon1" value="<?php echo  $_mobile;?>" disabled>
								  </div>
								  
								</div>
							 </div>
			 			</div>
			 			<div class="col-md-12 col-lg-4">
			 				<div class="summary">
			 					<h3>Total Payment</h3>
								<div class="summary-item"><span class="price">PHP : <?php echo number_format($t_amount,2); ?></span></div>
								<!--For Payment-->
								<?php
									$strxml = $strxml . "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
									$strxml = $strxml . "<Request>";
									$strxml = $strxml . "<orders>";
									$strxml = $strxml . "<items>";
									$strxml = $strxml . "<Items>";
									$strxml = $strxml . "<itemname>item 1</itemname><quantity>1</quantity><amount>".$_amount."</amount>"; // pls change this value to the preferred item to be seen by customer. (eg. Room Detail (itemname - Beach Villa, 1 Room, 2 Adults       quantity - 0       amount - 10)) NOTE : total amount of item/s should be equal to the amount passed in amount xml node below. 
									$strxml = $strxml . "</Items>";
									$strxml = $strxml . "</items>";
									$strxml = $strxml . "</orders>";
									$strxml = $strxml . "<mid>" . $_mid . "</mid>";
									$strxml = $strxml . "<request_id>" . $_requestid . "</request_id>";
									$strxml = $strxml . "<ip_address>" . $_ipaddress . "</ip_address>";
									$strxml = $strxml . "<notification_url>" . $_noturl . "</notification_url>";
									$strxml = $strxml . "<response_url>" . $_resurl . "</response_url>";
									$strxml = $strxml . "<cancel_url>" . $_cancelurl . "</cancel_url>";
									$strxml = $strxml . "<mtac_url></mtac_url>"; // pls set this to the url where your terms and conditions are hosted
									$strxml = $strxml . "<descriptor_note>''</descriptor_note>"; // pls set this to the descriptor of the merchant ""
									$strxml = $strxml . "<fname>" . $_fname . "</fname>";
									$strxml = $strxml . "<lname>" . $_lname . "</lname>";
									$strxml = $strxml . "<mname>" . $_mname . "</mname>";
									$strxml = $strxml . "<address1>" . $_addr1 . "</address1>";
									$strxml = $strxml . "<address2>" . $_addr2 . "</address2>";
									$strxml = $strxml . "<city>" . $_city . "</city>";
									$strxml = $strxml . "<state>" . $_state . "</state>";
									$strxml = $strxml . "<country>" . $_country . "</country>";
									$strxml = $strxml . "<zip>" . $_zip . "</zip>";
									$strxml = $strxml . "<secure3d>" . $_sec3d . "</secure3d>";
									$strxml = $strxml . "<trxtype>sale</trxtype>";
									$strxml = $strxml . "<email>" . $_email . "</email>";
									$strxml = $strxml . "<phone>" . $_phone . "</phone>";
									$strxml = $strxml . "<mobile>" . $_mobile . "</mobile>";
									$strxml = $strxml . "<client_ip>" . $_clientip . "</client_ip>";
									$strxml = $strxml . "<amount>" . $_amount . "</amount>";
									$strxml = $strxml . "<currency>" . $_currency . "</currency>";
									$strxml = $strxml . "<mlogo_url>http://www.sanbeda.edu.ph/img/preloadlogo.png</mlogo_url>";// pls set this to the url where your logo is hosted
									$strxml = $strxml . "<pmethod></pmethod>";
									$strxml = $strxml . "<signature>" . $_sign . "</signature>";
									$strxml = $strxml . "</Request>";
									$b64string =  base64_encode($strxml);
											//echo "<pre>" . $strxml . "</pre><hr />";
											//echo $b64string . "<hr />";
								?>
			 					<div class="summary-item"><span class="text"></span><span class="price"></span></div>
			 					<form name="form1" method="post" action="https://testpti.payserv.net/webpaymentv2/default.aspx">
									<input type="hidden" name="paymentrequest" id="paymentrequest" value="<?php echo $b64string ?>" style="width:800px; padding: 10px;">
									<input type="submit" class="btn btn-primary btn-lg btn-block" value="Pay Now" style="margin-top: 50px;">
								</form>
				 			</div>
			 			</div>
		 			</div> 
		 		</div>
	 		</div>
		</section>
 </div>
	 
</div>

<script src="<?php echo base_url('assets/resources/jquery-3.2.1.min.js');?>"></script>
<script src="<?php echo base_url('assets/resources/bootstrap.min.js');?>""></script>