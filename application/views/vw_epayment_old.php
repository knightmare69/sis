<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>EPayment</li>		
	</ol>
</div>   
<div id="content" style="overflow:auto;">
 <div class="col-sm-12">
 <div class="jarviswidget jarviswidget-color-green xmargin-bottom-10" id="wid-id-0" data-widget-colorbutton="true" data-widget-editbutton="false" data-widget-custombutton="false">
	<header role="heading">
		<h2><i class="fa fa-user"></i> Student Info</h2>
		<div class="jarviswidget-ctrls" role="menu">
			<a id='xmin' href="#" onclick="$('#loader').fadeIn();$('#studinfo').slideToggle(500);$('#xmin').hide();$('#xmax').show();$('#loader').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
			<a id='xmax' style='display:none;' href="#" onclick="$('#loader').fadeIn();$('#studinfo').slideToggle(500);$('#xmin').show();$('#xmax').hide();$('#loader').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>
		</div>
		<span id='loader' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
		<div class="widget-toolbar hidden" role="menu">	
		</div>
	</header>
	<div id='studinfo' role="content">	
		<div class="jarviswidget-body no-padding">
			<div class="row" style="white-space:nowrap;">
			 <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 xprofile-pic text-align-center">
			  <img id="student-avatar" data-default="<?php echo base_url();?>assets/img/avatars/empty.png" src="<?php echo base_url().((is_key_exist($studentinfo,'StudentNo')<>'')?'home/pics/id pics/1/'.is_key_exist($studentinfo,'StudentNo') : 'assets/img/avatars/empty.png');?>"/>
			 </div>
			 <div class="col-xs-12 col-sm-12 col-md-8 col-lg-10">
			   <div class="col-sm-4 col-lg-2">Academic Year & Term :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'AyTerm','');?></div>
			   <div class="col-sm-4 col-lg-2">Registration No. :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'RegID','');?></div>
			   <div class="col-sm-4 col-lg-2">Student Name :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo ((is_key_exist($studentinfo,'StudentNo'))?is_key_exist($studentinfo,'LastName','').', '.is_key_exist($studentinfo,'Firstname','').' '.is_key_exist($studentinfo,'Middlename','').' ['.is_key_exist($studentinfo,'StudentNo').']':'');?></div>
			   <div class="col-sm-4 col-lg-2">Program/Course :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'Program');?></div>
			   <div class="col-sm-4 col-lg-2">Curriculum :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'Curriculum','');?></div>
			   <div class="col-sm-4 col-lg-2">Year Level :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'Yearlvl','');?></div>
			   <div class="col-sm-4 col-lg-2">Fees Template :</div>
			   <div class="col-sm-8 col-lg-10 font-bold"><?php echo is_key_exist($studentinfo,'FeesTemplate','');?></div>
			 </div>
			</div>
			<br>	   
		</div>
	</div>		  
 </div>
 </div>
 <div class="col-sm-12 col-md-6">
  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
	<header role="heading">
		<h2>Assessed Fees</h2>
	<div class="jarviswidget-ctrls" role="menu">
		<a id='xmin2' href="#" onclick="$('#loader2').fadeIn();$('#feesinfo').slideToggle(500);$('#xmin2').hide();$('#xmax2').show();$('#loader2').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
		<a id='xmax2' style='display:none;' href="#" onclick="$('#loader2').fadeIn();$('#feesinfo').slideToggle(500);$('#xmin2').show();$('#xmax2').hide();$('#loader2').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>
	</div>
	<span id='loader2' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
	</header>
   <div id='feesinfo' role="content">

   <div class="jarviswidget-body no-padding">
	 <?php if($fees){echo $fees;}?>
	<br>	   
   </div>
   </div>
  </div>
 </div>
 <div class="col-sm-12 col-md-6">
   <div class="jarviswidget jarviswidget-color-blue" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
	<header role="heading">
		<h2><i class="fa fa-credit-card"></i> Online Payment Option</h2>
	<div class="jarviswidget-ctrls" role="menu">
		<a id='xmin2' href="#" onclick="$('#loader2').fadeIn();$('#feesinfo').slideToggle(500);$('#xmin2').hide();$('#xmax2').show();$('#loader2').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Minimize"><i class="fa fa-minus"></i></a>
		<a id='xmax2' style='display:none;' href="#" onclick="$('#loader2').fadeIn();$('#feesinfo').slideToggle(500);$('#xmin2').show();$('#xmax2').hide();$('#loader2').fadeOut(1500);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Maximize"><i class="fa fa-refresh"></i></a>
	</div>
	<span id='loader2' class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
	</header>
   <div id='feesinfo' role="content">

   <div class="jarviswidget-body no-padding">
      <div class="row" style="margin-bottom:5px;">
	  <div class="col-sm-6">
       Total Payment: <strong class="font-md txt-color-blue"><?php echo putDecimal($payment);?></strong>
	  </div>
	  <div class="col-sm-6">
	   Total Balance:<strong class="font-md txt-color-red"><?php echo putDecimal($balance);?></strong>
	  </div>
	  <br/>
	  <?php
	  if($total>0)
	  {	  
	  ?>
	  <form class="smart-form">
	  <fieldset>
	   <section style="margin-bottom:2px;">
	    <div class="row">
	    <div class="col col-6">
		 <label class="label">Select amount to pay:</label>
		 <label class="radio">
		  <input type="radio" name="scheme" checked="checked"/>
		  <i></i>
		  Full Payment: <b><?php echo putDecimal($balance);?></b>
		 </label>
		 <label class="radio">
		  <input type="radio" name="scheme"/>
		  <i></i>
		  Partial Payment: <b><?php echo putDecimal($ondue);?></b>
		 </label>
		 <br/>
		</div>
	    <div class="col col-6">
		 <label class="label">Select a online payment service:</label>
		 <label class="radio">
		  <input type="radio" name="service" checked="checked"/>
		  <i></i>
		  7-Connect - Pay at any 711 Store
		 </label>
		 <label class="radio">
		  <input type="radio" name="service"/>
		  <i></i>
		  DragonPay
		 </label>
		 <br/>
		</div>
		</div>
	   </section>
	  </fieldset>
      <fieldset style="padding:4px;">	  
	   <section class="text-align-center" style="margin-bottom:2px;">
	    <?php 
		 echo (($total>0 && $pending=='')?'<button type="button" class="btn btn-info btn-sm btngenerate">Generate</button>':'');
		?>
	   </section>
	  </fieldset>
	  </form>
	  <?php
	  }
	  ?>
   </div>	  
   </div>
   </div>
  
  </div>
 </div>
	 
</div>