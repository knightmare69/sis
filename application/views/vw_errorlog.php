<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Errors Log</li>
	</ol>
</div>   
<div id="content">
<div class="row">
	<div class="col-lg-12">
	  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
	      <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-warning"></i> Error Log<span></span></h1>
	  </div>
    </div>
	<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	<section id="widget-grid" class="">
		
			<!-- row -->
			<div class="row">
		
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i> </span>
							<h2>Error Logs</h2>
		
						</header>
		
						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
		
							</div>
							<!-- end widget edit box -->
		
							<!-- widget content -->
							<div class="widget-body no-padding">
								<div class="alert alert-info fade in <?php if(isset($note) && $note==0){echo 'hidden';}?>">
                                <i class="fa-fw fa fa-info"></i>Error logs as of <b id="logdate"><?php echo Date('m/d/Y H:i:s');?>.</b>
								</div>
								
								<!-- <div class="widget-body-toolbar"></div> -->
								
								<div role="content">
								<div class="widget-body withscroll">
								<table id="dt_basic" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>EventDate</th>
											<th>IP Address</th>
											<th>Error</th>
										</tr>
									</thead>
									<tbody id='transbody' style="font-size:10px;">
									    <?php 
										if(isset($errors))
										{echo $errors;}
										?>
									</tbody>
								</table>
								</div>
								</div>
                            </div>
                        </div>							
	                </div>
				</article>
            </div>
    </section>			
	<!-- <p>Bilis...</p> -->
	</div>
</div>		
</div>