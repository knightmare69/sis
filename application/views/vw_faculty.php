<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li id="mnufac" <?php if(isset($employeeid)){echo 'data-pointer="'.$employeeid.'"';}?>>Faculty Portal</li>
	</ol>
</div>   
<div id="content">	 
	 <div class="row">
		<div class="col-lg-12">
		  <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		    <h2 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-user"></i> <?php echo $title; ?> <span></span></h2>
		  </div>
		  <div class="col-xs-12 col-sm-12">
		    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
			 <header role="heading">
			 <h2>
			  <span class="fa fa-user"></span>
			  Faculty Module
			 </h2>
			 <ul class="nav nav-tabs pull-right in" id="xtabsnav">
			  <li class="active">
			   <a data-toggle="tab" href="#s0" id="cs0" class="">
			   <i class="fa fa-calendar"></i>
			   <span class="hidden-mobile hidden-tablet"><small>Class Schedules</small></span>
			   </a>
			  </li>
			  <li id="tl0">
			   <a data-toggle="tab" href="#g0" id="tg0" class="">
			   <i class="fa fa-table"></i>
			   <span class="hidden-mobile hidden-tablet"><small>Grade Encoding</small></span>
			   </a>
			  </li>
			  
			 </ul>
			 </header>
			 
			 <div id='facultyinfo' role="content" data-multitab="<?php echo $xMultiTab;?>" data-up-post="<?php echo $xUplBefPost;?>"  data-up-owrite="<?php echo $xOverwrite;?>">	
		      <div class="widget-body">
			   <div id="facultycontent" class="tab-content">
				
				<!-- Class Schedules -->
			    <div class="tab-pane fade no-padding padding-bottom active in" id="s0">
				 <div class="row">
				 <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				  <?php 
				  if(isset($campus))
				  {
				  echo '<div class="col-sm-6">';
				  echo '<label><b>Campus:</b></label>';
				  echo $campus;
				  echo '<br></div>';
				  }
				  
				  if(isset($ayterm))
				  {
				  echo '<div class="col-sm-6">';
				  echo '<label><b>Academic Year/Term:</b></label>';
				  echo $ayterm;
				  echo '<br></div>';
				  }
				  ?>
				 </div>
				 <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				  <?php
				  if(isset($xViewOther) && $xViewOther==1)
				  {
				  echo '<div class="pull-left">';
				  echo '<label><input id="userscheds" type="checkbox" onclick="$('."'#xreload'".').click();" checked/><small>Show Only My Schedules</small></label>';
				  echo '</div>';
				  }
				  ?>
				  <div class="pull-right">
				   <button id="xreload" class="btn btn-default" type="button"><span class="fa fa-refresh"></span> Refresh</button>
				   
				  
				    <?php
				    if($btnPrint=='0' || $btnSubPrint=='1')
				    {					
				    ?>
				   <div class="btn-group">
					<button class="btn btn-default dropdown-left dropdown-toggle" data-toggle="dropdown">
					 <i class="fa fa-print"></i> Print
					 <span class="caret"></span>
					</button>
					<ul class="dropdown-menu pull-right bg-color-white">
				    <?php
				    if($btnPrint=='0')
				    {					
				    ?>
   				     <li>
					 <a href="javascript:void(0);" onclick="printgradesheet(-2);">Grade Sheet - Midterm(Bulk)</a>
					 </li>
					 <li>
					 <a href="javascript:void(0);" onclick="printgradesheet(-1);">Grade Sheet - Final(Bulk)</a>
					 </li>
					<?php
				    }
					if($btnSubPrint=='1')
				    {					
				    ?>
   				      <li>
					 <a href="javascript:void(0);" onclick="printsubmitmonitor();">Monitoring - Submission of Grades</a>
					 </li>
					<?php
				    }					
				    ?>
					</ul>
				   </div>
				   <?php
				   }					
				   ?>
				   <!--<button id="xtrial" class="btn btn-default" type="button" onclick="addtab();"><span class="fa fa-plus"></span> Trial</button>-->
				  </div> 
				 </div>
				 </div>
				 <div class="row">
				 <hr>
				 </div>
				 <div class="container-fluid">
				 <div class="table-responsive col-xs-12 col-sm-12 col-md-8 col-lg-9" style="float:left;">
				 <div id="xsched" style="overflow:auto;max-height:500px;">
				    <br>
				    <?php
				     if(isset($ayterm))
				      {echo '<center>Select an Academic Year & Term.</center>';}
				     else
				      {echo '<center>No Class Schedule is available.</center>';}
				    ?>
				    <br><br>
				 </div>
				 </div>
				 <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 no-padding pull-right" style="float:right;">
				 	 <div class="jarviswidget jarviswidget-color-blueLight col-xs-6 col-sm-6 col-md-12 col-lg-12"  id="wid-sub-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
					 <header role="heading"><h2><span class="fa fa-fw fa-tasks"></span>Statistic</h2></header>
					  <div id="xstats" class="jarviswidget-body no-padding">
					  <table class="table table-striped">
					  <tr><td><small>Total Subject(s):</small></td><td>0</td></tr>
					  <tr><td><small>Total Lecture Unit(s):</small></td><td>0</td></tr>
					  <tr><td><small>Total Lab Unit(s):</small></td><td>0</td></tr>
					  <tr><td><small>Total Lecture Hour(s):</small></td><td>0</td></tr>
					  <tr><td><small>Total Lab Hour(s):</small></td><td>0</td></tr>
					  </table>
					  </div>
					 </div>
					 
					 <div class="jarviswidget jarviswidget-color-blueLight  col-xs-6 col-sm-6 col-md-12 col-lg-12"  id="wid-sub-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
					 <header role="heading"><h2><span class="fa fa-fw fa-tasks"></span>Legend</h2></header>
					  <div id="xlegend" class="jarviswidget-body no-padding">
					  <table class="table table-striped">
					  <tr><td class="txt-color-black"><i class="fa fa-square fa-lg"></i></td><td><small>No Changes</small></td></tr>
					  <tr><td class="txt-color-orange"><i class="fa fa-square fa-lg"></i></td><td><small>Mid-Term Posted</small></td></tr>
					  <tr><td class="txt-color-red"><i class="fa fa-square fa-lg"></i></td><td><small>Final Posted</small></td></tr>
					  <tr><td class="txt-color-purple"><i class="fa fa-square fa-lg"></i></td><td><small>Post Date Grade Submission</small></td></tr>
					  </table>
					  </div>
					 </div>
				 </div>
				 </div>
				</div>
				
				<!-- Grade Encoding -->
				<div class="tab-pane fade" id="g0">
				 <div class="col-sm-12 col-md-12 col-lg-12 padding">
				 
				  <div id="xnote" class="col-sm-8 col-md-8 col-lg-8 pull-left no-padding">
				  
				  </div>
				  
				  <div class="pull-right no-padding">
				   <button id="xgraderefresh" class="btn btn-info disabled" onclick="refreshgrade();">Refresh</button>
				   
				   <?php
				   if(isset($xUplBefPost) && $xUplBefPost==1)
				     echo '<button id="xupload" class="btn btn-default disabled hidden" type="button" data-toggle="modal" data-target="#myModal"><span class="fa fa-upload"></span> Upload</button>';
				   ?>
				   
				   <div class="btn-group">
					<button id="xprintgrade" class="btn btn-default dropdown-toggle disabled" data-toggle="dropdown">
					 <i class="fa fa-print"></i> Print
					 <span class="caret"></span>
					</button>
					<ul class="dropdown-menu pull-right bg-color-white">
					<?php
                    if($btnPrint=='0')
                    {					
					?>
					 <li>
					 <a href="javascript:void(0);" onclick="printgradesheet(1);">Grade Sheet - Midterm</a>
					 </li>
					 <li>
					 <a href="javascript:void(0);" onclick="printgradesheet(0);">Grade Sheet - Final</a>
					 </li>
					<?php
                    }
					
					if($btnPrint=='1')
                    {					
					?>
					 <li>
					 <a href="javascript:void(0);" onclick="printgradesheet(0);">Grade Sheet</a>
					 </li>
					<?php 
					}
					?>
					</ul>
				   </div>
					
				   <button id="xsave" class="btn btn-success disabled"><i class="fa fa-save"></i> Save</button>
				   <?php
				   if(isset($xSinglePost)&& $xSinglePost==1)
				   {
				    echo '<button id="xpostf" class="btn btn-warning disabled hidden"><i class="fa fa-lock"></i> Post</button>';
				    if(isset($xUnpost) && $xUnpost==1){echo '<button id="xunpost" class="btn btn-warning disabled hidden"><i class="fa fa-unlock"></i> Unpost</button>';}
				   }
				   else
				   {
				    echo '<button id="xpostm" class="btn btn-warning disabled hidden">Post(Midterm)</button>';
				    if(isset($xUnpost) && $xUnpost==1){echo '<button id="xunpostm" class="btn btn-warning disabled hidden">Unpost(Midterm)</button>';}
				    echo ' <button id="xpostf" class="btn btn-warning disabled hidden">Post(Finals)</button>';
				    if(isset($xUnpost) && $xUnpost==1){echo '<button id="xunpostf" class="btn btn-warning disabled hidden">Unpost(Finals)</button>';}
				   }
				   ?>
				  </div>
				  
				  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
				   <br>
				   <div class="alert alert-info hidden" id="xsheetinfo"></div>
				  </div>
				  
				  <br><br><br>
				  
				</div>
				 
			    <div id="gradeside" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 well pull-right no-padding">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bg-color-white">
				 <label class="col-xs-3 col-sm-6 col-md-12 col-lg-12"><small>Direction: </small></label>
				 <div class="col-xs-9 col-sm-6 col-md-12 col-lg-12 no-padding">
				 	 <select id="edirection" class="form-control" style="font-size:x-small;" onchange="managetabindex();">
						<option value="1" selected>Move Cursor Across</option>
						<option value="2">Move Cursor Down</option>
					 </select>
				 <br/>	 
				 </div>														  
				</div>
				<ul id="GESideTab" class="nav nav-tabs bordered bg-color-blueLight">
					<li class="active"><a href="#s1" data-toggle="tab"><small><i class="fa fa-table"></i> Grade Sys.</small></a></li>
					<li><a href="#s2" data-toggle="tab"><small><i class="fa fa-table"> Computation</i></small></a></li>
					<li><a href="#s3" data-toggle="tab"><small><i class="fa fa-table"> View</i></small></a></li>
				</ul>
                <div id="GESideTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="s1">				
				  	 <div class="jarviswidget jarviswidget-color-blueLight col-xs-6 col-sm-6 col-md-12 col-lg-12"  id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
					 <header role="heading"><h2><span class="fa fa-fw fa-tasks"></span>Statistic</h2></header>
					  <div id="xclassstats" class="jarviswidget-body no-padding">
					  <table class="table table-striped">
					  <tr><td><small>Total Male Students:</small></td><td id="statsMale">0</td></tr>
					  <tr><td><small>Total Female Students:</small></td><td id="statsFemale">0</td></tr>
					  <tr><td><small>Total Student:</small></td><td id="statsTStud">0</td></tr>
					  <tr><td></td><td></td></tr>
					  <tr><td><small>Total Passed:</small></td><td id="statsPass">0</td></tr>
					  <tr><td><small>Total Failed:</small></td><td id="statsFailed">0</td></tr>
					  <tr><td><small>Total INC.:</small></td><td id="statsINC">0</td></tr>
					  <tr><td><small>Total Drop:</small></td><td id="statsDrop">0</td></tr>
					  <tr><td><small>Total NoGrade:</small></td><td id="statsNoGrad">0</td></tr>
					  </table>
					  </div>
					 </div>
					 
                     <div class="jarviswidget jarviswidget-color-blueLight col-xs-6 col-sm-6 col-md-12 col-lg-12"  id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
					 <header role="heading"><h2><span class="fa fa-fw fa-tasks"></span>Grading System</h2></header>
					  <div id="xgradesys" class="jarviswidget-body no-padding">
					  <div class="no-padding" style="overflow:scroll;max-height:250px;white-space:nowrap;">
					  <?php
					   if(isset($gradesys)){echo $gradesys;}
					  ?>
					  </div>
					  </div>
					 </div>
                  					 
		         </div>	 
				 
                 <div class="tab-pane fade in" id="s2">	
				 <small>
				 <h6>Grade Computation</h6>
				 
				 <?php
				 echo '<div class="jarviswidget jarviswidget-color-blueLight col-xs-6 col-sm-6 col-md-12 col-lg-12"  id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
					    <header role="heading"><h2><span class="fa fa-fw fa-tasks"></span>Grade Computation</h2></header>
					    <div id="xgradecomp" class="jarviswidget-body no-padding">
					    <div class="no-padding" style="overflow:scroll;max-height:250px;white-space:nowrap;">';
						
				 if(isset($xPreDesc) && trim($xPreDesc)!='')
				 {
				  echo '<p><strong>Prelim Grade: </strong></p>';
				  echo '<p id="prelimdesc" data-grade="'.$xPGComp.'" data-tgrade="'.$xPTGComp.'"  data-fgrade="'.$xPrelimComp.'">'.$xPreDesc.'<p>';
				 }
				 
				 if(isset($xMidDesc) && trim($xMidDesc)!='')
				 {
				  echo '<p><strong>Midterm Grade: </strong></p>';
				  echo '<p id="middesc" data-grade="'.$xMGComp.'" data-tgrade="'.$xMTGComp.'"  data-fgrade="'.$xMidtermComp.'">'.$xMidDesc.'<p>';
				 }
				 
				 if(isset($xPreFinDesc) && trim($xPreFinDesc)!='')
				 {
				  echo '<p><strong>Pre Final Grade: </strong></p>';
				  echo '<p id="prefindesc" data-grade="'.$xPFGComp.'" data-tgrade="'.$xPFTGComp.'"  data-fgrade="'.$xPreFinComp.'">'.$xPreFinDesc.'<p>';
				 }
				 
				 if(isset($xFinDesc) && trim($xFinDesc)!='')
				 {
				  echo '<p><strong>Final Grade: </strong></p>';
				  echo '<p id="findesc" data-grade="'.$xFGComp.'" data-tgrade="'.$xFTGComp.'"  data-fgrade="'.$xFinComp.'">'.$xFinDesc.'<p>';
				 }
				 
				 echo '</div>
				        </div>
						</div>';
				 
				 if(isset($gradetrans) && trim($gradetrans)!='')
				 {
				  echo '<div class="jarviswidget jarviswidget-color-blueLight col-xs-6 col-sm-6 col-md-12 col-lg-12"  id="wid-id-4" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
					    <header role="heading"><h2><span class="fa fa-fw fa-tasks"></span>Transmutation</h2></header>
					    <div id="xgradetrans" class="jarviswidget-body no-padding">
					    <div class="no-padding" style="overflow:scroll;max-height:250px;white-space:nowrap;">';
				  echo $gradetrans;
				  echo '</div>
				        </div>
						</div>';
				 }
				 ?>
				 </small>
				 </div>
				 
                 <div class="tab-pane fade in" id="s3">
                  <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12">				 
				  <table class="table table-bordered">
				  <tbody>
				  <tr><td colspan="2" width="50%" height="40%"><center><img width="60%" height="50%" id="stdpic" src="<?php echo base_url('assets/img/avatars/empty.png');?>"/></center></td></tr>
				  </tbody>
				  </table>
				  </div>
				  
				  <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12 no-padding">
				  <table class="table table-bordered">
				  <tbody>
				  <tr><td width="50px">StudentNo:</td><td id="stdnum">-</td></tr>
				  <tr><td width="50px">Name:</td><td id="stdname">-</td></tr>
				  <tr><td width="50px">Gender:</td><td id="stdsex">-</td></tr>
				  <tr><td width="50px">Program:</td><td id="stdprog">-</td></tr>
				  </tbody>
				  </table>
				  </div>
				 </div>
				
		         </div>	     					 
		         </div>	    
				
				 <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8  pull-left table-responsive">
				  <div id="xgradeclass" class="well no-padding" style="overflow:scroll;max-height:600px;" data-singlep="<?php if(isset($xSinglePost)){echo $xSinglePost;}?>" data-allow="<?php //if(isset($xEncodeOther)){echo $xEncodeOther;}?>"  data-xpost="<?php if(isset($xPostOther)){echo $xPostOther;}?>">
       			   <p><center><i class="fa fa-warning"></i> No Class Schedule is selected.</center></p>
				  </div> 
			     </div>
				 
			   
			    </div>
			  
			  
			  </div>
			 </div>
			
			</div>
		  </div>
		</div>
	 </div>
</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<form method="POST" action="<?php echo preg_replace('/ faculty.*/', '',current_url()).'/uploadgradesheet';?>" enctype="multipart/form-data" onsubmit="antidepressant();">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><?php echo utf8_decode("×");?></button>
<h4 class="modal-title" id="myModalLabel">Upload Grade Sheet</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group">
<p>Upload your grade sheets in allowed formats.<br>(PDF, Word Document(.doc, .docx), Excel(.xls,.xlsx))</p>
<input type="file" class="form-control" id="ugradesheet" name="ugradesheet" accept=".doc,.docx,.xls,.xlsx,.pdf" placeholder="File(Word,Excel,PDF)" required="true"  onchange="spellcaster(this);"/>
</div>

<div id="div_usheetterm" class="form-group">
<p>Select for which is the upload for</p>
<select type="file" class="form-control" id="usheetterm" name="usheetterm" required="true"></select>
</div>
</div>
</div>

</div>
<div class="modal-footer">
<input  type="hidden" id="uschedid" name="uschedid"/>
<input  type="hidden" id="ufacid" name="ufacid"/>
<input  type="hidden" id="usecid" name="usecid"/>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<button type="submit" class="btn btn-primary">Upload Gradesheet</button>
</div>
</form>
</div>
</div>
</div>
