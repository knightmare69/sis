<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>File Editor</li>
	</ol>
</div>   
<div id="content">		
<div class="row"> 
 <article class="col-sm-3 col-md-3 col-lg-2 article_dir no-padding">
  <div class="jarviswidget jarviswidget-color-purple xmargin-right-5" id="wid-id-0" data-widget-editbutton="false" role="widget">
   <header role="heading">
    <span class="widget-icon"><i class="fa fa-lg fa-folder"></i></span><h2>Directory</h2>
	<div class="widget-toolbar" role="menu">
	 <a class="btn btn-default btn-sm btnreload" href="javascript:void(0);"><i class="fa fa-refresh"></i></a>
	 <a class="btn btn-default btn-sm btnhide" href="javascript:void(0);"><i class="fa fa-angle-double-left fa-md"></i></a>
	</div>
   </header>
   <div role="content" class="no-padding">
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body adaptivediv withscroll" style="height:400px !important;">
	 <table class="table table-condense table-treeview">
	  <tbody>
	   <tr class="table-parent active main-dir" data-path="<?php echo str_replace("index.php","",$_SERVER['SCRIPT_FILENAME']); ?>"><td><i class="fa fa-folder-open txt-color-yellow"></i> Main Directory</td></tr>
	   <tr class="table-child"><td class="main-files"><?php //echo $this->load->view('vw_directory','',true);?></td></tr>
	  </tbody>
	 </table>
    </div>
   </div>
  </div> 
 </article>		 
 <article class="col-sm-9 col-md-9 col-lg-10 article_main no-padding">
  <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false" role="widget">
   <header role="heading">
    <div class="widget-toolbar" role="menu"></div>
	<ul class="nav nav-tabs pull-left">
     <li class="tbxplore active"><a data-toggle="tab" href="#xplore"><i class="fa fa-md fa-folder txt-color-yellow"></i><span class="hidden-mobile"> Explorer</span></a></li>
	 <li class="tbfile"><a data-toggle="tab" href="#xfile"><i class="fa fa-md fa-edit txt-color-blueLight"></i><span class="hidden-mobile"> File</span></a></li>
	</ul>
   </header>
   <div role="content">
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body no-padding adaptivediv" style="height:400px !important;">
	<div class="tab-content">
    <div class="tab-pane active" id="xplore">
	 <div class="col-sm-12 btn-bar no-padding">
	   <button class="btn btn-default btn-sm btnback" rel="tooltip" data-placement="bottom" data-original-title="Back" data-path=""><i class="fa fa-lg fa-arrow-circle-left txt-color-green"></i></button>
	   |
	   <button class="btn btn-default btn-sm btnnew" rel="tooltip" data-placement="bottom" data-original-title="Add File"><i class="fa fa-file-o"></i></button>
	   <button class="btn btn-default btn-sm btnfadd" rel="tooltip" data-placement="bottom" data-original-title="Add Folder"><i class="fa fa-folder txt-color-yellow"></i></button>
	   <button class="btn btn-default btn-sm btnfdel" rel="tooltip" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times fa-lg txt-color-red"></i></button>
	   <button class="btn btn-default btn-sm btnshow" rel="tooltip" data-placement="bottom" data-original-title="Show List"><i class="fa fa-list"></i></button>
	   |
	   <span class="">
	   <button class="btn btn-default btn-sm btnfpaste disabled"><i class="fa fa-paste"></i></button>
	   <button class="btn btn-default btn-sm btnfcopy"><i class="fa fa-copy"></i></button>
	   <button class="btn btn-default btn-sm btnfcut"><i class="fa fa-cut"></i></button>
	   </span>
	   |
	   <a class="btn btn-default btn-sm btnfdownload disabled" href="javascript:void(0);" default-url="javascript:void(0);" rel="tooltip" data-placement="bottom" data-original-title="Download File"><i class="fa fa-download"></i></a> |
	   <button class="btn btn-default btn-sm btnbackup" rel="tooltip" data-placement="bottom" data-original-title="Backup"><i class="fa fa-refresh"></i></button>
	   <button class="btn btn-default btn-sm btnfupload" data-toggle="dropdown" rel="tooltip" data-placement="bottom" data-original-title="Upload"><i class="fa fa-upload"></i></button>
	 </div>
	 <div class="col-sm-12 tblxplore xpadding-5 adaptivediv withscroll" style="height:400px !important;overflow:scroll;">
		 <table class="table table-condense no-wrap">
		  <thead>
		   <tr>
		    <th>File Name</th>
		    <th>File Size</th>
		    <th>Date Modified</th>
		   </tr>
		  </thead>
		  <tbody class="open-files">
		  </tbody>
		 </table>
	 </div>
	</div>
    <div class="tab-pane adaptivediv" id="xfile">
     <div class="col-sm-12 btn-bar no-padding">
	   <button class="btn btn-default btn-sm btnnew" rel="tooltip" data-placement="bottom" data-original-title="New"><i class="fa fa-file-o"></i></button>
	   <button class="btn btn-default btn-sm btnopen txt-color-yellow" rel="tooltip" data-placement="bottom" data-original-title="Open"><i class="fa fa-folder-open"></i></button>
	   <button class="btn btn-default btn-sm btnsave disabled" rel="tooltip" data-placement="bottom" data-original-title="Save"><i class="fa fa-save"></i></button>
	   |
	   <button class="btn btn-default btn-sm btndelete txt-color-red disabled" rel="tooltip" data-placement="bottom" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
	   |
	   <span class="hidden">
	   <button class="btn btn-default btn-sm btnpaste"><i class="fa fa-paste"></i></button>
	   <button class="btn btn-default btn-sm btncopy"><i class="fa fa-copy"></i></button>
	   <button class="btn btn-default btn-sm btncut"><i class="fa fa-cut"></i></button>
	   |
	   </span>
	   <a class="btn btn-default btn-sm btndownload disabled" href="javascript:void(0);" default-url="javascript:void(0);" rel="tooltip" data-placement="bottom" data-original-title="Download File"><i class="fa fa-download"></i></a>
	   |
	   Editor:<i class="file-selected"></i>
	   <button class="btn btn-danger btnclose pull-right" rel="tooltip" data-placement="bottom" data-original-title="Close"><i class="fa fa-lg fa-times"></i></button>
	 </div>
	 <textarea class="txtfile" wrap="soft" rows="5" cols="40" disabled="true" style="overflow:auto;margin-bottom:10px;"></textarea>
	 <div class="col-sm-12 imgpreview hidden">
	  <img src="" style="max-height:300px !important;"/>
	 </div>
	</div>
    </div>	
	</div>
   </div>
  </div> 
 </article>
 <!-- DONT CROSS THIS LINE-->
 <div class="modal fade" id="filemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:500px !important;">
	<div class="modal-content">
	 <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	  <h4 class="modal-title" id="lblmwindow"><strong class="lblmode">Open</strong> <i id="data"></i></h4>
	 </div>	               
	 <div class="modal-body xpadding-10">
	  <div class="row">
	   <div class="col-sm-12 no-padding">
	    <label class="col-sm-2">Folder:</label><div class="col-sm-10"><input class="open-dir form-control"/></div>
	   </div>
	   <div class="col-sm-12 xpadding-10">
	    <div class="well no-padding xmargin-bottom-5" style="height:250px !important;overflow:scroll;">
		 <table class="table table-condense no-wrap">
		  <thead>
		   <tr>
		    <th>File Name</th>
		    <th>File Size</th>
		    <th>Date Modified</th>
		   </tr>
		  </thead>
		  <tbody class="open-files">
		  </tbody>
		 </table>
		</div>
	   </div>
	  </div>
	 </div>
	 <div class="modal-footer xmargin-top-5">
	  <div class="col-sm-12 xpadding-5 divfilename text-left hidden">
	   <label>File Name:</label>
	   <input type="text" class="txtfilename form-control"/>
	  </div>
	  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	  <button id="btnselectfile" type="button" class="btn btn-primary" disabled="disabled"><i class="fa fa-folder-open"></i> Open</button>						
	  <button id="btnsavefile" type="button" class="btn btn-primary hidden"><i class="fa fa-save"></i> Save</button>						
	 </div>	
    </div>
  </div>
 </div>	 
</div>
</div>

<div class="modal fade" id="uploadmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:500px !important;">
	<div class="modal-content">
	 <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	  <h4 class="modal-title" id="lblmwindow"><strong class="lblmode">Upload</strong> <i id="data"></i></h4>
	 </div>	               
	 <div class="modal-body xpadding-10">
	  <form id="upform">
	  <div class="row">
	   <div class="col-sm-12 no-padding">
	    <label class="col-sm-2">Folder:</label><div class="col-sm-10"><input id="xpath" name="xpath" class="open-dir form-control"/></div>
	   </div>
	   <div class="col-sm-12 no-padding">
	    <label class="col-sm-2">File:</label><div class="col-sm-10"><input type="file" id="ufile" name="ufile" class="form-control"/></div>
	   </div>
	  </div>
	  <div class="modal-footer xmargin-top-5">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	    <button type="submit" id="uploadfile" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>						
	  </div>	 
	  </form>
	 </div>	 
	</div> 
  </div>	
</div>	  