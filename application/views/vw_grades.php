
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Grades</li>
	</ol>
</div>   
<div id="content">
	<?php
	//$disabled=true;
		
	if($disabled!=false && $disabled!=0)
	{
	echo '<div class="alert alert-warning"><i class="fa fa-warning"></i> Grades Module is disabled by the Administrators for a while. Please bare with us.</div>';
	}
	else
	{
	?>	 
	 <div class="row">
		<div class="col-lg-12 no-padding">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					 <h2 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-clipboard"></i> <?php echo $title; ?></h2>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-padding">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="table-responsive pull-right">
					<table  id="dt_basic"  class="display" style="cursor: pointer;" >
						<tr><td nowrap>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<table class="xtable">
								 <tbody>
								    <tr>
									<td>
										<div>
										<center><h6> Gen. Weighted Ave.<br><b class='txt-color-blue'><?php echo $gwa ; ?> </b></h6></center>					 
										</div>
									</td>
									<td>
										<div>
										<center><h6> Enrolled <br><b class='txt-color-purple'><?php echo $enrolled ; ?> <small>Unit(s)</small></b></h6></center>					 
										</div>
									</td>
									<td>
										<div>
										<center><h6> Earned <br><b class='txt-color-greenDark'><?php echo $earned ; ?> <small>Unit(s)</small></b></h6></center>					
										</div>
									</td>
									<td>
										<div>
										<center><h6> CQPA (w/ Summer Terms) <br><b class='txt-color-redLight'><?php echo $cqpa1 ; ?></b></h6></center>					
										</div>
									</td>
									<td>
										<div>
										<center><h6> CQPA (All Academic Year/Term) <br><b class='txt-color-greenLight'><?php echo $cqpa2 ; ?></b></h6></center>					
										</div>
									</td>
									</tr>
								</tbody>
								</table>
							</div>
						</td></tr>
					</table>
				</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 xmargin-bottom-10">
			<button class="btn btn-primary pull-right" onclick="printgrades();"><i class="fa fa-print"></i> Print</button>
		</div> 
		<div class="col-lg-12">
		<?php 
		   if(isset($studentnolist)) 
		   {?>
			<div class='well'>
				<form role="form" class="form-horizontal">
					<fieldset>
						<div class="form-group">
							<label class="col-md-1 control-label" style="white-space:nowrap">Student Name: </label>
							
							<div class="col-md-11">				  				  
								<select class='form-control' id='studentno' name='studentno' onchange="this.form.submit()" required>                     
									<?php
										foreach ($studentnolist as $studentnolist)
										{
											echo "<option value='". $studentnolist->StudentNo ."' ".($studentnolist->StudentNo==$selstudentno ? "selected":"").">". $studentnolist->StudentNameNo . "</option>";
										}
									?>
								</select>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
			<?php 
			}
			?>
		</div>
		<div class="col-lg-12">
			<?php if(isset($studentnolist)) 
			{
			?>
			<div class="well" >
				<fieldset>
					<div class="form-group">
						<div class="col-md-2">
							<label style="white-space:nowrap">Student Name : </label>
						</div>
						<div class="col-md-10">
							<label><strong><?php echo $xdetail->StudentName;?></strong></label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2">
							<label style="white-space:nowrap">College Name : </label>
						</div>	
						<div class="col-md-10">
							<label><strong><?php echo $xdetail->CollegeName;?></strong></label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-md-2">
							<label style="white-space:nowrap">Academic Program : </label>
						</div>
						<div class="col-md-10">
							<label><strong><?php echo $xdetail->Program;?></strong></label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-md-2">
							<label style="white-space:nowrap">Program Curriculum : </label>
						</div>
						<div class="col-md-10">
							<label><strong><?php echo $xdetail->Curriculum;?></strong></label>
						</div>
					</div>
					
				</fieldset>
			</div>
			<?php
			}
			?>
		</div>
	 </div><!-- /.row -->

	 <div class="row">
		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			<input type="hidden"  id="stermid" value=0>
		  <div class="list-group">
			 <a href="#" class="list-group-item list-group-item-success">
				 <strong><small>Academic Year & Term</small></strong>				 
			 </a>			
			 
			 <?PHP
			    $printheader='';
				$i = 1;
					foreach($ds2 as $rs1){
						
						if ($xtermid == -1 && $i == 1)
						{
						  echo '<a class="list-group-item active" onclick="seltermid(this)"	href="'.site_url("grades/term/".$rs1->TermID."").'"  id = "'. $rs1->TermID  .'"  > <small>'. $rs1->AYTerm .'</small></a>';
						} 
						else 
						{
							echo '<a class="list-group-item '. ($xtermid== $rs1->TermID ? 'active' :'')  . '" onclick="seltermid(this)"	href="'.site_url("grades/term/".$rs1->TermID."").'"  id = "'. $rs1->TermID  .'"  > <small>'. $rs1->AYTerm .' </small></a>';
						}
                        
						if($xtermid== $rs1->TermID)
						 $printheader='<div class="alert alert-info"><center>'.$rs1->AYTerm.'</center></div>';
						
						$i++;
					}
			?>
			 
			 
		  </div>		  
		</div>
		<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10" id="xgrade">
		  <input id="studentno"type="hidden" value="<?php if(isset($studentno)) echo $studentno;?>"/>
		  <input id="termid"type="hidden" value="<?php if(isset($xtermid)) echo $xtermid;?>"/>
		  
		  <div class="table-responsive">
		  <div class="hidden printeronly">
          <?php if(isset($printheader)) echo $printheader;?>
		  </div>		  
		  <table  id="dt_basic"  class="table table-striped table-bordered table-hover" style="cursor: pointer;" >
			<thead>
				<tr class="odd gradeX">
					<th><strong>Code</strong></th>
					<th><strong>Descriptive Title</strong></th>
					<th><strong>Unit</strong></th>
					<th><strong>Midterm</strong></th>
					<th><strong>Final</strong></th>
					<th><strong>Re-Exam</strong></th>
					<th><strong>Remarks</strong></th>
					<th><strong>Midterm Date Posted</strong></th>
					<th><strong>Final Date Posted</strong></th>
				</tr>
			</thead>
	
			 <?php 
			 //print_r($ds);
			 
			 foreach($ds as $rs)
			 {
			    $final = property_exists($rs,'Final')?$rs->Final:'';
				$midterm =property_exists($rs,'Midterm')? $rs->Midterm:'';
				$graderemarks = property_exists($rs,'GradeRemarks')?$rs->GradeRemarks:'';
				$midtermposting = property_exists($rs,'MidtermGradesPostingDate')? $rs->MidtermGradesPostingDate : '';
				$finalposting = property_exists($rs,'DatePosted')? $rs->DatePosted : '';
				
			    if(trim($finalposting)=='')
				{
				 $final = "";
				 $graderemarks = "";
				}
				
				if(trim($midtermposting)=='' and trim($finalposting)=='')
				{
				 $final = "";
				 $midterm = "";
				 $graderemarks = "";
				}
				
				echo '<tr>
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small>'.$rs->Subject.'</small></font></td>
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small>'.$rs->Title.'</small></font></td>
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small>'.$rs->Unit.'</small></font></td>				
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small>'.$midterm.'</small></font></td>
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small>'.$final.'</small></font></td>
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small>'.$rs->ReExam.'</small></font></td>
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small><b>'.$graderemarks.'</b></small></font></td>
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small>'.$midtermposting.'</small></font></td>			
				<td><font  ' . ($rs->GradeRemarks=='Failed'? 'class="txt-color-red"':'') . '><small>'.$finalposting.'</small></font></td>			
				</tr>';
			 }
			?>
		  </table>
			</div>		
		</div>
	 </div>
	 <?php 
	 }
	 ?>
  </div>