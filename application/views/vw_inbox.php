
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Inbox</li>
	</ol>
</div>   
<div id="content">
    <div class="inbox-nav-bar no-content-padding">   
      <h1 class="page-title txt-color-blueDark hidden-tablet"><i class="fa fa-fw fa-inbox"></i> Inbox</h1>
      <div class="btn-group hidden-desktop visible-tablet">
         <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <b class="inbox-menu-label">Inbox</b> <i class="fa fa-caret-down"></i>
         </button>
         <ul class="dropdown-menu pull-left inbox-menu-sm">
            <li>
               <a href="javascript:void(0);" class="inbox-load-mini" onclick="$('.inbox-load').click();"><?php echo ((isset($page) && $page=='inbox')?'<i class="fa fa-check"></i>':'');?> Inbox</a>
            </li>
            <li>
               <a href="javascript:void(0);" class="inbox-notify-mini" onclick="$('.inbox-notify').click();"><?php echo ((isset($page) && $page=='notify')?'<i class="fa fa-check"></i>':'');?> Notification</a>
            </li>
            <li>
               <a href="javascript:void(0);" class="inbox-sent-mini" onclick="$('.inbox-sent').click();"><?php echo ((isset($page) && $page=='sent')?'<i class="fa fa-check"></i>':'');?> Sent</a>
            </li>
            <li class="divider"></li>
            <li>
               <a href="javascript:void(0);" class="inbox-trash-mini" onclick="$('.inbox-trash').click();"><?php echo ((isset($page) && $page=='trash')?'<i class="fa fa-check"></i>':'');?> Trash</a>
            </li>
         </ul>
   
      </div>
      <a href="javascript:void(0);" class="btn btn-primary hidden-desktop visible-tablet" onclick="$('#compose-mail').click();"> <strong><i class="fa fa-file fa-lg"></i></strong> </a>
      <div class="inbox-checkbox-triggered">
         <div class="btn-group">
            <a href="javascript:void(0);" rel="tooltip" title="" data-placement="bottom" data-original-title="Mark Important" class="hidden btn btn-default"><strong><i class="fa fa-exclamation fa-lg text-danger"></i></strong></a>
            <a href="javascript:void(0);" rel="tooltip" title="" data-placement="bottom" data-original-title="Move to folder" class="hidden btn btn-default"><strong><i class="fa fa-folder-open fa-lg"></i></strong></a>
            <a href="javascript:void(0);" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete" class="deletebutton btn btn-default"><strong><i class="fa fa-trash-o fa-lg"></i></strong></a>
         </div>
      </div>
      
	  <div class="btn-group pull-right inbox-paging" data-page="0" data-target="inbox">
         <a href="javascript:void(0);" class="btn btn-default btn-sm btn-prev" data-process="-" disabled="true"><strong><i class="fa fa-chevron-left"></i></strong></a>
         <a href="javascript:void(0);" class="btn btn-default btn-sm btn-next" data-process="+" disabled="true"><strong><i class="fa fa-chevron-right"></i></strong></a>
      </div>
      <span class="pull-right hidden"><strong>1-10</strong> of <strong>10</strong></span>
   
   </div>
  <div id="inbox-content" class="inbox-body no-content-padding">
     <div class="inbox-side-bar">
        <a href="javascript:void(0);" data-info="<?php echo site_url('inbox/txn/get/compose'); ?>" id="compose-mail" class="btn btn-primary btn-block"> <strong>Compose</strong> </a>
        <h6> Folder <a href="javascript:void(0);" rel="tooltip" title="" data-placement="right" data-original-title="Refresh" class="pull-right txt-color-darken"><i class="fa fa-refresh"></i></a></h6>
		<ul class="inbox-menu-lg">
           <li class="<?php echo ((isset($page) && $page=='inbox')?'active':'');?>">
              <a id="<?php echo site_url('inbox/txn/get/mail'); ?>" class="inbox-load inbox-menu" data-target="inbox" href="javascript:void(0);"> Inbox</a>
           </li>
           <li class="<?php echo ((isset($page) && $page=='notify')?'active':'');?>">
              <a id="<?php echo site_url('inbox/txn/get/notify'); ?>" class="inbox-notify inbox-menu" data-target="notify" href="javascript:void(0);">Notification</a>
           </li>
           <li class="<?php echo ((isset($page) && $page=='sent')?'active':'');?>">
              <a id="<?php echo site_url('inbox/txn/get/sent'); ?>" class="inbox-sent inbox-menu" data-target="sent" href="javascript:void(0);">Sent</a>
           </li>
           <li class="<?php echo ((isset($page) && $page=='trash')?'active':'');?>">
              <a id="<?php echo site_url('inbox/txn/get/trash'); ?>" class="inbox-trash inbox-menu" data-target="trash" href="javascript:void(0);">Trash</a>
           </li>
        </ul>
     </div>
  
     <div class="table-wrap custom-scroll animated fast fadeInRight" data-isloaded="<?php echo ((isset($display))?'true':'false');?>">
	  <?php 
	   if(isset($display))
	    echo $display;
	   else
	    $this->load->view('mail/list_content');
	  ?>
     </div>
  </div> 
</div>



