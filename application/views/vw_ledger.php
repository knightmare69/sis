
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Student Ledger</li>
	</ol>
</div>   
<div id="content">	 
 <div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
   <?php 
	if($idtype=='-1') 
	 echo $this->load->view('include\studentinfo','',true);
	else if($idtype=='1')
	 echo '<input type="hidden" id="studentno" value="'.$studentno.'"/>';
	else if($idtype=='2') 
	 echo $this->load->view('include\studentinfo','',true);
   ?>	 
  </div>   
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
   <div class="table-responsive ledger-data"> 
	<?php echo $row;?>
   </div>
  </div>
 </div><!-- /.row -->
</div>