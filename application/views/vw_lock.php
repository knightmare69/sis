<!DOCTYPE html>
<html lang="en-us">
	<head>
  <meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> PRISMS Online : Lock </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">	
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/font-awesome.css') ?>">		
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-production.css') ?>">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/smartadmin-skins.css') ?>">	
		
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/css/lockscreen.css') ?>">	

		
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon/favicon.ico') ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('assets/img/favicon/favicon.ico') ?>" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

	</head>	
	<body>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<?php echo form_open('lock/access',array('id'=>'syslock','class' => 'lockscreen animated flipInY')); ?>			
				<div class="logo">
					<h1 class="semi-bold" style="font-family: 'OLD_ENG1';"><img src="<?php echo base_url('assets/img/logo-o.png') ?>" alt="" /> <?php echo INSTITUTION;?></h1>
				</div>
				<div>
					<img class="img-responsive img-profile" src="<?php echo $photo; ?>" alt="" width="120" height="120" />
					<div>
						<h1><i class="fa fa-user fa-3x text-muted air air-top-right hidden-mobile"></i><?php echo $username; ?> <small><i class="fa fa-lock text-muted"></i> &nbsp;Locked</small></h1>
						<p class="text-muted">
							<a href="mailto:ar@princetech.com.ph"><?php echo $userinfo['email']; ?></a>
						</p>
						<div class="input-group">
							<input class="form-control" name="pwd" type="password" placeholder="Password">
							<div class="input-group-btn">
								<button class="btn btn-primary" type="submit">
									<i class="fa fa-key"></i>
								</button>
							</div>
						</div>
						<p class="no-margin margin-top-5">
							Logged as someone else? <a href="<?php echo site_url('home/logout'); ?>"> Click here</a>
						</p>
					</div>

				</div>
				<p class="font-xs margin-top-5">
					Copyright Prince Tech 2014-2020.
				</p>
			</form>

		</div>

		<!--================================================== -->	

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
		<script src="<?php echo base_url('assets/js/plugin/pace/pace.min.js') ?>"></script> -->		
		
	    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script> if (!window.jQuery) { document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');} </script>

	    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script> if (!window.jQuery.ui) { document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 				
		<script src="<?php echo base_url('assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') ?>"></script>
		-->

		<!-- BOOTSTRAP JS -->		
		<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url('assets/js/notification/SmartNotification.min.js') ?>"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url('assets/js/smartwidgets/jarvis.widget.min.js') ?>"></script>
			   
		<!-- SPARKLINES -->
		<script src="<?php echo base_url('assets/js/plugin/sparkline/jquery.sparkline.min.js') ?>"></script>
		
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url('assets/js/plugin/jquery-validate/jquery.validate.min.js') ?>"></script>
		
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url('assets/js/plugin/masked-input/jquery.maskedinput.min.js') ?>"></script>
		
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url('assets/js/plugin/select2/select2.min.js') ?>"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url('assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js') ?>"></script>
		
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url('assets/js/plugin/msie-fix/jquery.mb.browser.min.js') ?>"></script>
		
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url('assets/js/plugin/fastclick/fastclick.js') ?>"></script>
		
		<!--[if IE 7]>			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>			
		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url('assets/js/app.js') ?>"></script>
		<script>
			
		</script>
	</body>
</html>