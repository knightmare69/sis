
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Payment</li>
	</ol>
</div>
<div id="content">
  <section id="widget-grid" class="">
    <div class="row">
       <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="jarviswidget well jarviswidget-color-darken" id="wid-id-0" data-widget-sortable="false" data-widget-deletebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false">             
             <header>
                <span class="widget-icon"> <i class="fa fa-barcode"></i> </span>
                <h2>Item #44761 </h2>
             </header>        
             <div>              
                <div class="jarviswidget-editbox"></div>                            
                <div class="widget-body no-padding"> 
                   <div class="widget-body-toolbar">
                      <div class="row">
                         <div class="col-sm-4">
                            <div class="input-group">
                               <input class="form-control" type="text" placeholder="Type invoice number or date...">
                               <div class="input-group-btn">
                                  <button class="btn btn-default" type="button">
                                     <i class="fa fa-search"></i> Search
                                  </button>
                               </div>
                            </div>
                         </div>
 
                         <div class="col-sm-8 text-align-right">
 
                            <div class="btn-group">
                               <a href="javascript:void(0)" class="btn btn-sm btn-primary"> <i class="fa fa-edit"></i> Edit </a>
                            </div>
 
                            <div class="btn-group">
                               <a href="javascript:void(0)" class="btn btn-sm btn-success"> <i class="fa fa-plus"></i> Create New </a>
                            </div>
 
                         </div>
 
                      </div>
 
                   </div>
 
                   <div class="padding-10">
                      <br>
                      <div class="pull-left">
                        <!--<img src="img/logo-blacknwhite.png" width="150" height="32" alt="invoice icon">-->
 
                         <address>
                            <br>
                            <strong>San Beda College Alabang</strong>
                            <br>                            
                            8 Don Manolo Blvd., Alabang Hills Village
                            <br>
                            Alabang, 1770 Muntinlupa City
                            <br>
                            P.O. Box 402 Ayala Alabang                            
                            <br>
                            <abbr title="Phone">Tel No:</abbr> (123) 456-7890
                         </address>
                      </div>
                      <div class="pull-right">
                         <h1 class="font-400">Assessment</h1>
                      </div>
                      <div class="clearfix"></div>
                      <br>
                      <br>
                      <div class="row">
                         <div class="col-sm-9">
                            <h4 class="semi-bold">Allan Robert B. Sanchez</h4>
                            <address>
                               <strong>Student</strong> 
                               <br>
                               342 Mirlington Road,
                               <br>
                               Calfornia, CA 431464
                               <br>
                               <abbr title="Phone">P:</abbr> (467) 143-4317
                            </address>
                         </div>
                         <div class="col-sm-3">
                            <div>
                               <div>
                                  <strong>Registration No :</strong>
                                  <span class="pull-right"> #AA-454-4113-00 </span>
                               </div>
 
                            </div>
                            <div>
                               <div class="font-md">
                                  <strong>REG. DATE :</strong>
                                  <span class="pull-right"> <i class="fa fa-calendar"></i> 15/02/13 </span>
                               </div>
 
                            </div>
                            <br>
                            <div class="well well-sm  bg-color-darken txt-color-white no-border">
                               <div class="fa-lg">
                                  Total Due :
                                  <span class="pull-right"> 4,972 Pesos** </span>
                               </div>
 
                            </div>
                            <br>
                            <br>
                         </div>
                      </div>
                      <table class="table table-hover">
                         <thead>
                            <tr>
                               <th class="text-center">Units</th>
                               <th>ITEM</th>
                               <th>DESCRIPTION</th>
                               <th>AMOUNT</th>
                               <th>SUBTOTAL</th>
                            </tr>
                         </thead>
                         <tbody>
                            <tr>
                               <td class="text-center"><strong>21</strong></td>
                               <td><a href="javascript:void(0);">Tuition Fee</a></td>
                               <td>You have 8 subjects enrolled under the course of bachelor of science in computer science</td>
                               <td>1,300.00</td>
 
                               <td>1,300.00</td>
                            </tr>
                            <tr>
                               <td class="text-center"><strong>6</strong></td>
                               <td><a href="javascript:void(0);">Laboratory Fee</a></td>
                               <td>computer laboratory fee, special lab fee, science lab fee</td>
                               <td>1,400.00</td>
                               <td>1,400.00</td>
                            </tr>
                            <tr>
                               <td class="text-center"><strong></strong></td>
                               <td><a href="javascript:void(0);">Miscellaneaus Fee</a></td>
                               <td>registration fee, cultural fee & etch.</td>
                               <td>1,700.00</td>
                               <td>1,700.00</td>
                            </tr>
                            <tr>
                               <td colspan="4">Total</td>
                               <td><strong>4,400.00</strong></td>
                            </tr>
                         </tbody>
                      </table>
 
                      <div class="invoice-footer">
 
                         <div class="row">
 
                            <div class="col-sm-7">
                               <div class="payment-methods">
                                  <h5>Payment Methods</h5>
                                  <img src="<?php echo base_url('assets/img/invoice/paypal.png') ?>" width="64" height="64" alt="paypal">
                                  <img src="<?php echo base_url('assets/img/invoice/americanexpress.png') ?>" width="64" height="64" alt="american express">
                                  <img src="<?php echo base_url('assets/img/invoice/mastercard.png') ?>" width="64" height="64" alt="mastercard">
                                  <img src="<?php echo base_url('assets/img/invoice/visa.png') ?>" width="64" height="64" alt="visa">
                               </div>
                            </div>
                            <div class="col-sm-5">
                               <div class="invoice-sum-total pull-right">
                                  <h3><strong>Total: <span class="text-success">Php4,972.00</span></strong></h3>
                               </div>
                            </div>
 
                         </div>
                         
                         <div class="row">
                            <div class="col-sm-12">
                               <p class="note">**To avoid any excess penalty charges, please make payments within 30 days of the due date. There will be a 2% interest charge per month on all late invoices.</p>
                            </div>
                         </div>
 
                      </div>
                   </div>
 
                </div>
                <!-- end widget content -->
 
             </div>
             <!-- end widget div -->
 
          </div>
          <!-- end widget -->
 
       </article>
       <!-- WIDGET END -->
 
    </div>
 
    <!-- end row -->
   
   </section>
   <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->