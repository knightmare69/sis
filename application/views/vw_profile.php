
<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><a id="thyHome" href="<?php echo site_url('profile'); ?>"> User Profile</a></li>
	</ol>
</div>   
<div id="content">	 
	<section id="widget-grid" class="">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-2 no-padding">
                <div class="col-xs-6 col-sm-12 col-md-12">
				<?php
				 if($userinfo['idtype']!=0){echo '<a href="#s3" data-toggle="tab" ><span class="profile-edit">Edit</span></a>';}
				?>
				<img class="img-responsive img-profile xcenter-element" height="200px" alt="" src="<?php echo $userinfo['photo'].'?'.rand(8888,9999); ?>">
				</div>
				<div class="col-xs-6 col-sm-12 col-md-12">
				<div class="list-group nav nav-tabs">
				    <a class="list-group-item active font-sm" href="#s1" data-toggle="tab">Overview</a>				                        
                    <a class="list-group-item font-sm " href="#s2" data-toggle="tab"><i class="fa fa-user"></i> Basic Information </a>                                                                                
                    <?php if($userinfo['idtype']!= 0){echo '<a href="#s3" data-toggle="tab" class="list-group-item font-sm"  ><i class="fa fa-picture-o"></i> Profile Picture </a>'; }?>	
                    <a class="list-group-item font-sm "  href="#s4" data-toggle="tab"><i class="fa fa-lock "></i> Change Password </a>
                    <a class="list-group-item font-sm" href="#s5" data-toggle="tab"><i class="fa fa-envelope "></i> Change Email </a>
                    <a class="list-group-item font-sm hidden" href="#s6" data-toggle="tab"><i class="fa fa-cog "></i> Settings </a>
                </div>
				</div>
                                    
            </div>
			<div class="col-xs-12 col-sm-8 col-md-10">								 					                    
                    <div class="well well-light no-padding clearfix">
						<div id="protabcont" class="tab-content">
							<div class="tab-pane fade in active padding-10" id="s1">
								
									
									
									    <?php
										if($userinfo['idtype']==1)
										{
										 echo '<h1 class="princeh1"><b>'.utf8_encode((property_exists($studentinfo[0],'LastName')?$studentinfo[0]->LastName:'').'</b>, '. (property_exists($studentinfo[0],'FirstName')?$studentinfo[0]->FirstName:'').' '. (property_exists($studentinfo[0],'ExtName')?$studentinfo[0]->ExtName:'').' '. (property_exists($studentinfo[0],'MiddleInitial')?$studentinfo[0]->MiddleInitial:'')).'</h1>'; 
										 echo '<p>I am a student of <b>'.(property_exists($studentinfo[0],'ProgName')?$studentinfo[0]->ProgName:'').'</b></p>';
                                         $this->mod_main->Translog($userinfo['idno'],'View Profile','StudentNo:'.$studentinfo[0]->StudentNo);
										}
										else
										{
										 echo '<h1 class="princeh1"><b>'.(array_key_exists('lastname',$userinfo)?$userinfo['lastname']:'').'</b>, '.(array_key_exists('firstname',$userinfo)?$userinfo['firstname']:'').'</h1>'; 
										}
										
										if(array_key_exists('idno',$userinfo) and $userinfo['idno']!='' and $userinfo['idtype']!=0)
										{echo '<p><i class="fa fa-user"></i> '.$userinfo['idno'].'</p>';}
										
										if((array_key_exists('res_addr',$userinfo) or array_key_exists('res_strt',$userinfo) or array_key_exists('res_brgy',$userinfo) or array_key_exists('res_city',$userinfo) or array_key_exists('res_prov',$userinfo)) and trim($userinfo['res_addr'].$userinfo['res_strt'].$userinfo['res_brgy'].$userinfo['res_city'].$userinfo['res_prov'])!='')											
										{
										  echo '<p> <i class="fa fa-map-marker fa-muted"></i>  '. utf8_encode($userinfo['res_addr'].' '.$userinfo['res_strt'].' '.$userinfo['res_brgy']) .'<br />';
                                          echo utf8_encode($userinfo['res_city'].', '.$userinfo['res_prov']) .'</p>';
                                        } 
										
										if($userinfo['telno']!='' and $userinfo['telno']!=' ')
									    {echo '<p><i class="fa fa-phone"></i> '.$userinfo['telno'].'</p>';}
									    ?>
										<p><i class="fa fa-envelope-o"></i><a href="javascript:void(0);"> <?php echo $userinfo['email']; ?></a></p>
										
										<?php
										if(isset($history))
										{
										?>
                                        <div class="well well-light well-sm">
                                            <h3 class="princeh1">Enrollment History</h3>
    										<div class="table-responsive">
    											<table class="table table-hover table-bordered table-striped" style="white-space: nowrap;">
    												<thead>
    													<tr>
    													<th>Academic Year and Term</th>
    													<th>Reg. ID</th>
    													<th>Reg. Date</th>
    													<th>College</th>
    													<th>Program</th>
    													<th>Major</th>
    													<th>Year Level</th>
    													</tr>
    												</thead>
    												<tbody>
    													<?php
    														foreach($history as $row)
    														{
    														  echo "<tr>
    														         <td>".$row->Ayterm."</td>
    														         <td>".$row->RegID."</td>
    														         <td>".$row->RegDate."</td>															 
    															     <td>".$row->CollegeCode."</td>															 
    														   	     <td>".$row->ProgramCode."</td>															 
    																 <td>".$row->Major."</td>															 
    																 <td>".$row->YearLevel."</td>															 
    																</tr>";
    														}
    													?>
    													</tbody>
    											</table>
    										</div>
                                        </div>
										<?php									
										}
										?>
										
										<?php 
										if(isset($balance))
										{
										?>	
                                        <div class="well well-light well-sm">
                                            <h3 class="princeh1">
											 Current Balance 
											 <strong> 
											 <?php 
											 $bal_count=0;
											 if(isset($balance) && $balance)
											 {
											  foreach($balance as $bal)
											  {
												echo 'As of '.$bal->AYTerm.' : '.number_format($bal->Balance,2,'.',',');	
                                                if($bal->Balance>0){$bal_count++; }	
                                                break;												
											  }
											 }	 
											 else
											  echo ' 0.00';
											 ?>
											 </strong>
											</h3>
											<?php echo (($bal_count>0)?'<h5><i>(<i class="fa fa-warning"></i> Please make sure to settle your balance before the semester ends, otherwise, you will not be able to enroll for the succeeding term)</i></h5>':''); ?>
										</div>
										<?php
										}
										?>
                                        
										<?php 
										if(isset($accounts))
										{
										?>
										<div class="well well-light well-sm">
                                            <h3 class="princeh1">Accountabilities</h3>
											<?php echo ((count($accounts)>0)?'<h5><i>(<i class="fa fa-warning"></i> Make sure to fix this before enrollment start, or you wont be able to enroll)</i></h5>':''); ?>
											<table class="table table-hover table-bordered table-striped" style="white-space: nowrap;">
											  <thead>
												 <tr>
													<th>Academic Year and Term</th>
													<th>Department/Reason</th>
												 </tr>
											  </thead>
											  <tbody>
											  <?php
											   foreach($accounts as $accts)
											   {
												 if($accts->Status!=1)
												 {	 
												  echo '<tr>
												         <td>'.$accts->AYTerm.'</td>
												         <td>'.$accts->Reason.'</td>
												        </tr>';  
												 }		
											   }
											  ?>
											  </tbody>
											</table>		
										</div>
										<?php
										}
										?>
								
							</div>
							<div class="tab-pane fade in padding-10" id="s2">
							 <?php 
							   echo $this->load->view('profile/basic_info'); 
							   echo '<div class="row">';
							   echo (($userinfo['idtype']!=0 && $userinfo['idtype']!= 2)? $this->load->view('profile/family') : '');
                               echo (($userinfo['idtype']==1) ? $this->load->view('profile/education') : '');
							   echo '</div>';
						     ?>
							</div>
                            <div class="tab-pane fade in padding-10" id="s3">
                             <div class="col-sm-12">    
								<img id="xprofile" class="img-responsive img-profile" alt="" src="<?php echo $userinfo['photo'].'?'.rand(8888,9999); ?>">
								<br>
								<?php echo form_open_multipart('profile/pic_upload');?>
								<div class="form-group">
								<label>Choose a New Image</label>
								<img id="temp_pic" class="img-responsive hidden">
								<input id="xprofile_up" name="pic_file" type="file" onchange="spellcaster(this,'xprofile');" accept="image/*">
								<p class="help-block">
								<i class="fa fa-warning"></i>
								Image must be no larger than 500x500 pixels. Supported formats: JPG, GIF, PNG
								</p>
								<button id="xpic_upload" class="btn btn-default" type="submit" value="upload" disabled="disabled" onclick="antidepressant();">Update Profile Picture</button>
								<button class="btn btn-green">Cancel</button>
								</div>
								</form>
                             </div>
							</div>
                            <div class="tab-pane fade in padding-10" id="s4">
                                
									
                                    <?php echo form_open('profile/ChangePassword',array('id'=>'palitpwd','class' => 'smart-form client-form')); ?>													
                                      <header>
								        Change Password
							          </header>
                                      <fieldset >
										<section>
											<label class="input"> <i class="icon-append fa fa-lock"></i>
												<input type="password" name="pwd" placeholder="Current Password" id="pwd">
												<b class="tooltip tooltip-bottom-right">Enter your current password</b> </label>
										</section>
										<section>
											<label class="input"> <i class="icon-append fa fa-lock"></i>
												<input id="npwd" type="password" name="npwd" placeholder="New Password" >
												<b class="tooltip tooltip-bottom-right">Enter your new password</b> </label>
										</section>
										<section>
											<label class="input"> <i class="icon-append fa fa-lock"></i>
												<input id="cpwd" type="password" name="cpwd" placeholder="Re-type Password" >
												<b class="tooltip tooltip-bottom-right">Kindly retype your new password</b> </label>
										</section>
									 </fieldset>
									 <footer>
										<button type="submit" class="btn btn-primary">
										 Update Password
										</button>
									 </footer>													
									</form>													
								
                                                
                            </div>
                            <div class="tab-pane fade in padding-10" id="s5">
                                
                                <?php echo form_open('profile/changeEmail',array('id'=>'palitemail','class' => 'smart-form client-form')); ?>
                                    <header>
								        Change Email
							         </header>
                                													
									<fieldset>
									<section>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="email" name="email" placeholder="Current Email" id="pwd">
											<b class="tooltip tooltip-bottom-right">Enter your current email</b> </label>
									</section>
									<section>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input id="nemail" type="email" name="nemail" placeholder="New Email" >
											<b class="tooltip tooltip-bottom-right">Enter your new email</b> </label>
									</section>
									<section>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input id="cemail" type="email" name="cemail" placeholder="Re-type Email" >
											<b class="tooltip tooltip-bottom-right">Kindly retype your new email</b> </label>
									</section>
									</fieldset>
									<footer>
										<button id="btnchangeemail" type="submit" class="btn btn-primary">
											Change Email
										</button>
									</footer>													
								</form>				
                                                    
                            </div>
						</div>
                    </div>
					
				
			</div>			
		</div>
	</section> <!-- end widget grid -->
</div>
<i id="csopt" class="hidden"><?php if(isset($csoption)) echo $csoption;?></i>
<i id="natopt" class="hidden"><?php if(isset($natoption)) echo $natoption;?></i>
<i id="relopt" class="hidden"><?php if(isset($reloption)) echo $reloption;?></i>