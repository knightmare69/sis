<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Registration Monitoring</li>
	</ol>
</div>   
<div id="content">	 
	 <div class="row">
		<div class="col-lg-12 no-padding">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					 <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-group"></i> <?php echo $title; ?></h1>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
			<section id="widget-grid" class="">
		     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="jarviswidget jarviswidget-color-blue" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">		 
			   <header role="heading">
			    <h2>
			     <span class="fa fa-user"></span>
			      List of Registrations
			    </h2>
			   </header>
			   <div role="content" class="jarviswidget-body">
			    <div class="row">
			    <div class="col-sm-12">
			     <div class="control-group">
				  <div class="col-sm-4"><label class="col-sm-3">AY Term: </label><div class="col-sm-8"><select class="form-control"><option disabled selected>Select one</option></select></div></div>
				  <div class="col-sm-4"><label class="col-sm-3">Program: </label><div class="col-sm-8"><select class="form-control"><option disabled selected>Select one</option></select></div></div>
				 </div>
				
				 <div class="btn-group pull-right inbox-paging">
				  <a href="javascript:void(0);" class="btn btn-default btn-sm"><strong><i class="fa fa-chevron-left"></i></strong></a>
				  <a href="javascript:void(0);" class="btn btn-default btn-sm"><strong><i class="fa fa-chevron-right"></i></strong></a>
				 </div>
				 <span class="pull-right">
				  <strong>1-4</strong> of <strong><?php echo $tbcount;?></strong>
				 </span>	
			    </div>
				<br>
				<br>
				</div>
			    <div id='reglist' class="col-sm-12">
			    <?php echo $tbdata;?>
			    </div>
			   </div>
			  </div>
			 </div>
			</section> 
			</div>
		</div>
     </div>
</div>	 