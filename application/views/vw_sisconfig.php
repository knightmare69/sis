<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Config</li>
	</ol>
</div>   
<div id="content">	 
	<div class="row">
	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
         <div class="jarviswidget jarviswidget-color-blue" id="wid-id-config" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">		 
			 <header role="heading">
			  <h2>
			   <span class="fa fa-gears"></span>
			   Configuration
			  </h2>
			 </header>
			 <div class="widget-body">
			 <div role="content">
			  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			  <?php 
			   if(isset($configlist))
			   {
			    echo form_open($xlink,array('id' => 'configform'));
				echo '<div class="row" data-pointer="tblconfig" style="max-height:500px;overflow:auto;">';
			    echo $configlist;
				echo '</div>';
				echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="pull-right"><button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button></div></div>';
				echo '</form>';
			   }
			  ?>
			  </div>
			 </div>
			 </div>
          </div>			 
		</div>
	</article>
	</div>
</div>	