<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li><a href="javascript:void(0);"> SOA</a></li>
	</ol>
</div>   
<div id="content">	 
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">		
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
				<h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Statement of Accounts</h1>
			</div>		 		 
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<section id="widget-grid" class="">
			<div class="row">
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-soa0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"  data-widget-deletebutton="false">
					<header role="heading">
					 <h2>Registration Information</h2>
					 <div class="widget-toolbar" role="menu">
                     <button id='xreSOA' class="btn btn-primary"><i class='fa fa-refresh'></i> Refresh</button>
                     <button id='xprintSOA' class="btn btn-warning" onclick="printSOA();"><i class='fa fa-print'></i> Print SOA</button>
					 </div>
					</header>		
					<div id="xstdSOA">
						<table class="table table-bordered">
						<tbody>
						 <tr><td width="35%">Academic Year & Term:</td><td><?php if(isset($cboAYTerm)){echo $cboAYTerm;}?></td></tr>
						 <tr><td width="35%">Name:</td><td><?php if(isset($studentinfo) && property_exists($studentinfo,'Student')){echo $studentinfo->Student;}?></td></tr>
						 <tr><td width="35%">StudentNo:</td><td><?php if(isset($studentinfo) && property_exists($studentinfo,'StudentNo')){echo $studentinfo->StudentNo;}?></td></tr>
						 <tr><td width="35%">Campus:</td><td><?php if(isset($reginfo) && property_exists($reginfo,'Campus')){echo $reginfo->Campus;}?></td></tr>
						 <tr><td width="35%">College:</td><td><?php if(isset($reginfo) && property_exists($reginfo,'College')){echo $reginfo->College;}?></td></tr>
						 <tr><td width="35%">Program:</td><td><?php if(isset($reginfo) && property_exists($reginfo,'Program')){echo $reginfo->Program;}?></td></tr>
						 <tr><td width="35%">Year Level:</td><td><?php if(isset($reginfo) && property_exists($reginfo,'YearLevel')){echo $reginfo->YearLevel;}?></td></tr>
						</tbody>
						</table>
                    </div>
                </div>
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-soa1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"  data-widget-deletebutton="false">
					<header role="heading">
					 <h2>Assessment</h2>
					</header>		
					<div id="xstdAssess">
					<?php if(isset($assessment)){echo $assessment;}?>
                    </div>
                </div>
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-soa2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false"  data-widget-deletebutton="false">
					<header role="heading">
					 <h2>Payment</h2>
					</header>		
					<div id="xstdpayment">
					<?php if(isset($payment)){echo $payment;}?>
                    </div>
                </div>
            </article>
			</div>
            </section>			
		</div>
    </div>
</div>	