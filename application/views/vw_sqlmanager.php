<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>SQL Manager</li>
	</ol>
</div>   
<div id="content">
 <div class="row">
  <div class="well xmargin-bottom-5">
   <button class="btn btn-sm btn-success btnsave" disabled="true"><i class="fa fa-save"></i> Save</button>
   <button class="btn btn-sm btn-default btnexecute"><i class="fa fa-exclamation txt-color-red"></i> Execute</button>
   <button class="btn btn-sm btn-danger btnstop" disabled="true"><i class="fa fa-stop"></i> Stop</button>
   <button class="btn btn-sm btn-primary btnrefresh" disabled="true"><i class="fa fa-refresh"></i> Refresh</button>
  </div>
  <div class="well no-padding xmargin-bottom-5">
   <textarea id="query" class="form-control" rows='9999' style="height:200px !important; overflow:auto;"></textarea>
  </div>
  <div id="result" class="well no-padding" style="height:200px !important; overflow:auto;"></div>
 </div> 
</div>