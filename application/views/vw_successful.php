<div class="row">
  <img class="img-responsive center-block" style="margin-top:110px; height:150px;" src="<?php echo base_url('assets/img')?>/SBU SEAL.png">
  <h1 style="font-size:3.0em;color:#C00000;font-family:'Goudy Old Style Regular';" class="text-center"> SAN BEDA UNIVERSITY</h1>
  <h1 style="font-size:2.0em;" class="text-center"> Admissions and Placement Center</h1>
  <br/>

  <h1 style="font-size:2em;" class="text-center text-success"><i class="fa fa-check"></i> Congratulations <?php if(isset($name)) echo $name; ?> !
  <br/>

  <small>Your application was successfully submitted.. <br/> Kindly proceed to Admission Office for your verification of application. Thank you!</small></h1>

  <br/>

  <h1 style="font-size:3em;" class="text-center text-danger"><i class="fa fa-info-circle"></i> Your Application number is <strong> <?php if(isset($appno)) echo $appno; ?> <strong> </h1>
  <?php echo ((isset($sent) && $sent!=false)?'<br/><h2 class="text-center">Note: A copy of your filled-up Online Application Form was sent to your registered e-mail address.</h2>':''); ?>
  <br/><h2 class="text-center text-danger">Click here to print your application. <a href="<?php echo base_url('admission/printapp').'/'.$appno;?>" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Print</a> </h2>

</div>
