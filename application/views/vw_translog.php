<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>Transactions Log</li>
	</ol>
</div>   
<div id="content">
<div class="row">
	<div class="col-lg-12">
	  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
	      <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-bar-chart-o"></i> Transactions Log<span></span></h1>
	  </div>
    </div>
	<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
	<section id="widget-grid" class="">
		
			<!-- row -->
			<div class="row">
		
				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		
					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
					 <header>
					   <span class="widget-icon"> <i class="fa fa-table"></i> </span>
					     <h2>Transactions Logs</h2>
		             </header>
		
						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
		
							</div>
							<!-- end widget edit box -->
		
							<!-- widget content -->
							<div class="widget-body no-padding">
								<!--
								<div class="alert alert-info fade in">
                                <i class="fa-fw fa fa-info"></i>Transactions log as of <b id="logdate"><?php echo Date('m/d/Y H:i:s');?>.</b>								
								<small id="tblnote"><i class="fa fa-spinner fa-spin"></i> Still Loading..</small>
								</div>
								-->
								<div class="widget-body-toolbar">
								 <div class="row">
								  <div class="col-sm-12 col-md-4 col-lg-3">
		                           <div class="input-group">
								    <span class="input-group-addon"><i class="fa fa-calendar"></i> Start</span>
		                            <input type="text" id="eventstart" class="form-control datepicker" data-dateformat="yy-mm-dd" data-time="00:00:00" value="<?php echo date('Y-m-d').' 00:00:00'; ?>"/>
		                           </div>
								  </div>
								  <div class="col-sm-12 col-md-4 col-lg-3">
								   <div class="input-group">
								    <span class="input-group-addon"><i class="fa fa-calendar"></i> End</span>
		                            <input type="text" id="eventend" class="form-control datepicker" data-dateformat="yy-mm-dd" data-time="23:59:59" value="<?php echo date('Y-m-d').' 23:59:59'; ?>"/>
							       </div>
								  </div>
								  <div class="xmargin-right-10 pull-right">
								   <button class="btn btn-warning btnsearch"><i class="fa fa-search"></i></button>
				                   <button class="btn btn-primary btnrefresh"><i class="fa fa-refresh"></i></button>
				                   <button class="btn btn-default btnprev"><i class="fa fa-chevron-left"></i></button>
				                   <button class="btn btn-default btnnext"><i class="fa fa-chevron-right"></i></button>
				                  </div>
								 </div>
								</div>
								<div role="content">
								<div style="overflow:scroll;white-space:nowrap;">
								<table id="dt_basic" style="white-space:nowrap;" class="table table-bordered table-condensed table-hover smart-form" data-count="<?php if(isset($rcount)){echo $rcount;}?>" data-xinit="<?php if(isset($latestrow)){echo $latestrow;}?>" data-xlast="<?php if(isset($lastrow)){echo $lastrow;}?>">
									<thead>
										<tr>
											<th>EventDate</th>
											<th>Username/Email</th>
											<th>IPAddress</th>
											<th>UserAgent</th>
											<th>Module</th>
											<th>Action</th>
											<th>Parameters</th>
										</tr>
										<tr class="second">
											<th></th>
											<th>
												<label class="input">
													<input type="text" id="uname" name="search_username" placeholder="Search" class="search_init">
												</label>	
											</th>
											<th>
												<label class="input">
													<input type="text" id="ipadd" name="search_ipaddress" placeholder="Search" class="search_init">
												</label>	
											</th>
											<th>
												<label class="input">
													<input type="text" id="uagent" name="search_useragent" placeholder="Search" class="search_init">
												</label>	
											</th>
											<th>
												<label class="input">
													<input type="text" id="xmodule" name="search_module" placeholder="Search" class="search_init">
												</label>	
											</th>
											<th>
												<label class="input">
													<input type="text" id="xaction" name="search_action" placeholder="Search" class="search_init">
												</label>	
											</th>
											<th>
												<label class="input">
													<input type="text" id="xparam" name="search_parameters" placeholder="Search" class="search_init">
												</label>	
											</th>
										</tr>
									</thead>
									<tbody id='transbody' style="font-size:10px;">
									    <?php 
										if(isset($logs))
										{echo $logs;}
										?>
									</tbody>
									<tfoot class="hidden">
									  <tr class='template'>
									    <td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									  </tr>
									</tfoot>
								</table>
								</div>
								<i id='xlast' class='hidden'><?php if(isset($lastrow)){echo $lastrow;}?></i>
								</div>
                            </div>
                        </div>							
	                </div>
				</article>
            </div>
    </section>			
	<!-- <p>Bilis...</p> -->
	</div>
</div>		
</div>