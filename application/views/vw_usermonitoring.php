<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>User Monitoring and Management</li>
	</ol>
</div>   
<div id="content">	 
	 <div class="row">
		<div class="col-lg-12 no-padding">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					 <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-group"></i> <?php echo $title; ?> <!--span>> Report of Grades</span--></h1>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
			<section id="widget-grid" class="">
		     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="jarviswidget jarviswidget-color-blue" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false">		 
			   <header role="heading">
			    <h2>
			     <span class="fa fa-user"></span>
			      User List
			    </h2>
			    <div class="widget-toolbar" role="menu">
				 <button class="btn btn-danger" onclick="bulkmgmt(1);"><i class="fa fa-trash-o"></i> Bulk Delete</button>
				 <button class="btn btn-success" onclick="bulkmgmt(2);"><i class="fa fa-check"></i> Bulk Validate</button>
			    </div>
			   </header>
			  
			  <div class="jarviswidget-body">
			   <div class="widget-body-toolbar">
			    <div class="col-xs-4 col-sm-3 col-md-2 no-padding pull-left">
				 <div class="input-group">
				  <input id="txtsearch" class="form-control" type="text"/>
				  <span class="input-group-addon btnsearch"><i class="fa fa-search"></i></span>
				 </div>
				</div>
				<div class="pull-right">
				 <button class="btn btn-default btnprev"><i class="fa fa-chevron-left"></i></button>
				 <button class="btn btn-default btnnext"><i class="fa fa-chevron-right"></i></button>
				</div>
			   </div>
			   <div role="content" id="userlist">
			    <div style="overflow:scroll;">
			    <?php echo $tbdata;?>
				<div class="alert alert-danger alert-nodata hidden"><i class="fa fa-warning fa-lg txt-color-yellow"></i> No Data Found.</div>
			    </div>
			   </div>
			  </div>
			  </div>
			 </div>
			</section> 
			</div>
		</div>
     </div>
</div>	 

<div class="modal fade" id="usermodification" tabindex="-1" role="dialog" aria-labelledby="User Modification" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<form id="xusermgmt" method='POST' onsubmit="return xvalidate_form();">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h4 class="modal-title" id="myModalLabel">User Management</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<p>Create or Modify User in this modal.</p>
<div class="form-group">
<input type="hidden" id="prevuname" name="prevuname"/>
<input type="text" class="form-control state-error" id="uname" name="uname" placeholder="Username" required="true"/>
</div>
<div id="pass" class="row no-padding"></div>
<hr/>
<div class="form-group">
<p>User Info</p>
<input type="text" class="form-control" id="lname" name="lname" placeholder="Lastname" required="true"/>
<input type="text" class="form-control" id="fname" name="fname" placeholder="Firstname" required="true"/>
<input type="text" class="form-control" id="mname" name="mname" placeholder="Middlename"/>
<input type="text" class="form-control" id="miname" name="miname" placeholder="M.I"/>
</div>
<hr/>
<div class="form-group">
<p>Contact Info</p>
<input type="text" class="form-control" id="email" name="email" placeholder="Email" />
<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile/Tel. No"/>
</div>
<hr/>
<div class="form-group">
<p>Account Type Info</p>
<input type="text" class="form-control" id="uidno" name="uidno" placeholder="PRISMS ID No. or StudentNo"/>
<select class="form-control" id="uidtype" name="uidtype" required="true">
<option value="0" disabled selected>-Select Type-</option>
<?php
if(ENABLE_STUDENT==1)echo '<option value="1">Student</option>';
if(ENABLE_PARENT==1)echo '<option value="2">Parent</option>';
if(ENABLE_FACULTY==1)echo '<option value="3">Faculty</option>';
if(ENABLE_ADMIN==1)echo '<option value="-1">Administrator</option>';
?>
</select>
<label>Validated: <input type="checkbox" id="uactive" name="uactive"/></label>
</div>
<hr>
<div class="form-group sub_list hidden">
<p>Subscription List</p>
<div class="table-responsive" style="max-height:120px;overflow:auto;">
<table id="subscription" class="table table-bordered">
<thead>
 <tr>
  <th width="60px">
     <a class="btn btn-info btn-xs btn-refresh_sub"><i class="fa fa-refresh"></i></a>
     <a class="btn btn-success btn-xs btn-new_sub"><i class="fa fa-file"></i></a>
  </th>
  <th>StudentNo</th>
  <th width="30%">Relationship</th>
 </tr>
</thead>
<tbody>
</tbody>
<tfoot class="hidden">
 <tr class="sub_pattern">
 <td><a class="btn btn-danger btn-xs btn-delete_sub"><i class="fa fa-trash-o"></i> Delete</a></td>
 <td><input name="studno" type=text class="ptc_tbltextbox" style="box-shadow:none;" value=""/></td>
 <td>
   <select name="relation" type=text class="ptc_tbltextbox" style="box-shadow:none;">
    <option value="Family">Family</option>
    <option value="Parent">Parent</option>
    <option value="Guardian">Guardian</option>
   </select>
 </td>
 </tr>
</tfoot>
</table>
</div>
</div>
</div>

</div>

</div>
<div class="modal-footer">
<input type="hidden" id="opt" name="opt" value="0"/>
<button type="button" class="btn btn-success" onclick="manage_save(0);"><i class="fa fa-save"></i> Save and Add New</button>
<button type="button" class="btn btn-primary" onclick="manage_save(1);"><i class="fa fa-save"></i> Save</button>
<button type="button" id="cancel" class="btn btn-default" data-dismiss="modal">Cancel</button>
</div>
</form>
</div>
</div>
</div>