<div id="ribbon">
	<span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true"><i class="fa fa-refresh"></i></span> </span>
	<ol class="breadcrumb">
		<li><a id="mnuhome" href="<?php echo site_url('home'); ?>"> Home</a></li>
		<li>User Privileges</li>
	</ol>
</div>   
<div id="content">	 
 <article class="col-sm-3 no-padding">
  <div class="jarviswidget jarviswidget-color-orange xmargin-right-5" id="wid-id-0" data-widget-editbutton="false" role="widget">
   <header role="heading">
    <span class="widget-icon"><i class="fa fa-lg fa-user"></i></span><h2>User List</h2>
	<div class="widget-toolbar" role="menu">
	</div>
   </header>
   <div role="content">
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body" style="height:400px !important;">
	 <table class="table table-condense table-treeview">
	  <tbody>
	   <tr class="table-parent"><td><i class="fa fa-plus"></i> Users</td></tr>
	   <tr class="table-child hidden"><td></td></tr>	
	   <tr class="table-parent"><td><i class="fa fa-plus"></i> User Type</td></tr>
	   <tr class="table-child hidden">
	    <td>
		 <table class="table table-condense">
		  <tbody>
		   <tr><td><i class="fa fa-group"></i> Administrator</td></tr>
		   <tr><td><i class="fa fa-group"></i> Student</td></tr>
		   <tr><td><i class="fa fa-group"></i> Parent</td></tr>
		   <tr><td><i class="fa fa-group"></i> Faculty</td></tr>
		  </tbody>
		 </table>
		</td>
	   </tr>
	  </tbody>
	 </table>
    </div>
   </div>
  </div> 
 </article>
 <article class="col-sm-9 no-padding">
  <div class="jarviswidget jarviswidget-color-orange" id="wid-id-0" data-widget-editbutton="false" role="widget">
   <header role="heading">
    <span class="widget-icon"><i class="fa fa-lg fa-user"></i></span><h2>User Privileges</h2>
	<div class="widget-toolbar" role="menu">
	</div>
   </header>
   <div role="content">
    <div class="jarviswidget-editbox"></div>
    <div class="widget-body" style="height:400px !important;">
	 <table class="table table-condense table-treeview">
	  <tbody>
	   <tr><td><i class="fa fa-plus"></i> Home</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Profile</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Grades</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Evaluation</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Student Ledger</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Accountabilities</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Advising</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Grade Encoding</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Subscription</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Email Notification</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Admin Controls</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Downloads</td></tr>
	   <tr><td><i class="fa fa-plus"></i> Extra</td></tr>
	  </tbody>
	 </table>
    </div>
   </div>
  </div> 
 </article>
</div>