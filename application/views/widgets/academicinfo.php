<div class="jarviswidget" id="wid-id-acadinfo" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false">
<header>
 <h2>Academic Information</h2>
</header>
<div>
 <div class="jarviswidget-editbox"><!-- This area used as dropdown edit box --></div>
 <div class="widget-body no-padding">
	<div class="no-padding">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-4">
				 <div style="padding-top: 15px;">
				  <img class="img-responsive img-profile" alt="" src="<?php echo $userinfo['photo'];?>">
				 </div>
				</div>
				<div class="col-sm-8">
					<h1 class="princeh1"><?php echo '<b>' . $studentinfo[0]->LastName . '</b>, ' . $studentinfo[0]->FirstName . ' ' . $studentinfo[0]->ExtName . ' ' . $studentinfo[0]->MiddleInitial; ?></h1>
					<p>I am a student of <b><?php echo $studentinfo[0]->ProgName;?></b>.</p>
					<ul class="list-inline">
						<li><i class="fa fa-map-marker fa-muted"></i> <?php echo $studentinfo[0]->Res_Address;?></li>

					</ul>
					</br>
					<p><b>Other Informations</b></p>
					<p><b><?php echo $studentinfo[0]->MajorDiscDesc;?></b></p>
					<p>College: <b><?php echo $studentinfo[0]->CollegeName;?></b></p>
					<p>Program Curriulum : <b><?php echo $studentinfo[0]->CurriculumCode;?></b></p>
					<p>Total Credit Earned : <b><?php echo (isset($academicinfo) && array_key_exists('0',$academicinfo) && property_exists($academicinfo[0],'totalCreditU'))?$academicinfo[0]->totalCreditU:'run Evaluation to Generate Data';?></b></p>
					<p>Total Credit Units Earned :<b><?php echo (isset($academicinfo) && array_key_exists('0',$academicinfo) && property_exists($academicinfo[0],'totalUnitsE'))?$academicinfo[0]->totalUnitsE:'run Evaluation to Generate Data';?></b></p>
					<p>Total Credit Units to Earn :<b><?php echo (isset($academicinfo) && array_key_exists('0',$academicinfo) && property_exists($academicinfo[0],'CreToEarn'))?$academicinfo[0]->CreToEarn:'run Evaluation to Generate Data';?></b></p>
				</div>
			</div>
		</div>
	</div>
 </div>
</div>
</div>