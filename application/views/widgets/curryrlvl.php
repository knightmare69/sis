<div class="jarviswidget" id="wid-id-curryrlvl" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false">
<header><h2>Curricular Year Level</h2></header>
<div>
<div class="jarviswidget-editbox">
		<!-- This area used as dropdown edit box -->
</div>
<div class="widget-body no-padding">
	<div class="no-padding">
		<div class="bar-holder">
			<b>First Year</b>
			<div class="progress progress-md progress-striped active">
				<div title="<?php echo (array_key_exists('0',$progressbar))?number_format((float)$progressbar[0]->Percentage, 2, '.', ''):0; ?>%" class="progress-bar bg-color-greenLight" style="width:<?php echo (array_key_exists('0',$progressbar))?number_format((float)$progressbar[0]->Percentage, 2, '.', ''):0; ?>%;" aria-valuetransitiongoal="<?php echo (array_key_exists('0',$progressbar))?number_format((float)$progressbar[0]->Percentage, 2, '.', ''):0; ?>"><?php echo (array_key_exists('0',$progressbar))?number_format((float)$progressbar[0]->Percentage, 2, '.', ''):0; ?>%</div>
			</div>
		</div>
		
		
		<div class="bar-holder" style="padding-top: 3px;">
			<b>Second Year</b>
			<div class="progress progress-md progress-striped active">
				<div title="<?php echo (array_key_exists('1',$progressbar))?number_format((float)$progressbar[1]->Percentage, 2, '.', ''):0; ?>%" class="progress-bar bg-color-yellow" style="width:<?php echo (array_key_exists('1',$progressbar))?number_format((float)$progressbar[1]->Percentage, 2, '.', ''):0; ?>%;" aria-valuetransitiongoal="<?php echo (array_key_exists('1',$progressbar))?number_format((float)$progressbar[1]->Percentage, 2, '.', ''):0; ?>"><?php echo (array_key_exists('1',$progressbar))?number_format((float)$progressbar[1]->Percentage, 2, '.', ''):0; ?>%</div>
			</div>
		</div>
		
		<div class="bar-holder" style="padding-top: 3px;">
			<b>Third Year</b>
			<div class="progress progress-md progress-striped active">
				<div title="<?php echo (array_key_exists('2',$progressbar))?number_format((float)$progressbar[2]->Percentage, 2, '.', ''):0; ?>%" class="progress-bar bg-color-blue" style="width:<?php echo (array_key_exists('2',$progressbar))?number_format((float)$progressbar[2]->Percentage, 2, '.', ''):0; ?>%;" aria-valuetransitiongoal="<?php echo (array_key_exists('2',$progressbar))?number_format((float)$progressbar[2]->Percentage, 2, '.', ''):0; ?>"><?php echo (array_key_exists('2',$progressbar))?number_format((float)$progressbar[2]->Percentage, 2, '.', ''):0; ?>%</div>
			</div>
		</div>
		<div class="bar-holder" style="padding-top: 3px;">
			<b>Fourth Year</b>
			<div class="progress progress-md progress-striped active">
				<div title="<?php echo (array_key_exists('3',$progressbar))?number_format((float)$progressbar[3]->Percentage, 2, '.', ''):0; ?>%" class="progress-bar bg-color-redLight" style="width:<?php echo (array_key_exists('3',$progressbar))?number_format((float)$progressbar[3]->Percentage, 2, '.', ''):0; ?>%;" aria-valuetransitiongoal="<?php echo (array_key_exists('3',$progressbar))?number_format((float)$progressbar[3]->Percentage, 2, '.', ''):0; ?>"><?php echo (array_key_exists('3',$progressbar))?number_format((float)$progressbar[3]->Percentage, 2, '.', ''):0; ?>%</div>
			</div>
		</div>
	
	    <?php 
	     if ($yearcount > 4)
	     {
	     echo '<div class="bar-holder" style="padding-top: 3px;">
			    <b>Fifth Year</b>
			    <div class="progress progress-md progress-striped active">
				 <div title="'. ((array_key_exists('4',$progressbar))?number_format((float)$progressbar[4]->Percentage, 2, '.', ''):0). '%" class="progress-bar bg-color-darken" style="width: ' . ((array_key_exists('4',$progressbar))?number_format((float)$progressbar[4]->Percentage, 2, '.', ''):0) .  '%;" aria-valuetransitiongoal="' . ((array_key_exists('4',$progressbar))?number_format((float)$progressbar[4]->Percentage, 2, '.', ''):0) .  '">' . ((array_key_exists('4',$progressbar))?number_format((float)$progressbar[4]->Percentage, 2, '.', ''):0) .  '%</div>
			    </div>
		       </div>';
	     }
	    ?>	
	</div>
</div>
</div>
</div>