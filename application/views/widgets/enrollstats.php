<div class="col-sm-12 col-md-6 no-padding">
<div class="jarviswidget jarviswidget-color-blueDark xmargin-right-10" id="wid-id-enroll" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false">
 <header>
  <h2><i class="fa fa-bar-chart-o"></i> Enrollment Statistics</h2>
  <div class="widget-toolbar" role="menu">
  </div>
 </header>
 <div>
  <div class="jarviswidget-editbox"><!-- This area used as dropdown edit box --></div>
  <div class="widget-body no-padding">
   <table class="table table-condense">
    <tr><td> AYTerm:<td><td><?php echo ((isset($enroll))?($enroll->AYTerm):'Undefined'); ?></td></tr>
	<tr><td> No. of Advised Students:<td><td><?php echo ((isset($enroll))?($enroll->Advised):'Undefined'); ?></td></tr>
	<tr><td> No. of Registered Students:<td><td><?php echo ((isset($enroll))?($enroll->Registered):'Undefined'); ?></td></tr>
    <tr><td> No. of Registered Thru Online:<td><td><?php echo ((isset($enroll))?($enroll->IsOnline):'Undefined'); ?></td></tr>
    <tr><td> No. of Validated Registration:<td><td><?php echo ((isset($enroll))?($enroll->Validated):'Undefined'); ?></td></tr>
   </table>
  </div>
 </div>
 </div>
 </div>