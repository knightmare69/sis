 <div class="col-sm-12 col-md-6 no-padding">
 <div class="jarviswidget jarviswidget-color-blueDark xmargin-right-10" id="wid-id-grades" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false">
 <header>
  <h2><i class="fa fa-bar-chart-o"></i> Grade Encoding Statistics</h2>
  <div class="widget-toolbar" role="menu">
  </div>
 </header>
 <div>
  <div class="jarviswidget-editbox"><!-- This area used as dropdown edit box --></div>
  <div class="widget-body no-padding">
   <table class="table table-condense">
    <tr><td> AYTerm:<td><td><?php echo ((isset($grades))?($grades->AYTerm):'Undefined'); ?></td></tr>
	<tr><td> Encoding Period:<td><td><?php echo ((isset($grades))?($grades->GradeEncoding):'Undefined'); ?></td></tr>
	<tr><td> No. of ClassSchedules:<td><td><?php echo ((isset($grades))?($grades->Schedules):'Undefined'); ?></td></tr>
    <tr><td> No. of Posted Gradesheets:<td><td><?php echo ((isset($grades))?($grades->Posted):'Undefined'); ?></td></tr>
   </table>
  </div>
 </div>
 </div>
 </div>
 