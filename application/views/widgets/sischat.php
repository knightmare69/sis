
<!-- new widget -->
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-chat" data-widget-editbutton="false" data-widget-fullscreenbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>
		<h2> PRISMS Chat </h2>
		<div class="widget-toolbar">
			
		</div>
	</header>

	<!-- widget div-->
	<div>
		
		<div class="jarviswidget-editbox">
			<div>
				<label>Title:</label>
				<input type="text" />
			</div>
		</div>
		<!-- end widget edit box -->
		<div class="widget-body widget-hide-overflow no-padding">					  
			<div id="chat-container">
				<span class="chat-list-open-close"><i class="fa fa-user"></i><b>!</b></span>
				<div class="chat-list-body custom-scroll">
					<ul id="chat-users">
						<li>
							<a href="javascript:void(0);"><img src="img/avatars/5.png" alt="">Robin Berry <span class="badge badge-inverse">23</span><span class="state"><i class="fa fa-circle txt-color-green pull-right"></i></span></a>
						</li>
						<li>
							<a href="javascript:void(0);"><img src="img/avatars/male.png" alt="">Mark Zeukartech <span class="state"><i class="last-online pull-right">2hrs</i></span></a>
						</li>
						<li>
							<a href="javascript:void(0);"><img src="img/avatars/male.png" alt="">Belmain Dolson <span class="state"><i class="last-online pull-right">45m</i></span></a>
						</li>
						<li>
							<a href="javascript:void(0);"><img src="img/avatars/male.png" alt="">Galvitch Drewbery <span class="state"><i class="fa fa-circle txt-color-green pull-right"></i></span></a>
						</li>
						<li>
							<a href="javascript:void(0);"><img src="img/avatars/male.png" alt="">Sadi Orlaf <span class="state"><i class="fa fa-circle txt-color-green pull-right"></i></span></a>
						</li>
						<li>
							<a href="javascript:void(0);"><img src="img/avatars/male.png" alt="">Markus <span class="state"><i class="last-online pull-right">2m</i></span> </a>
						</li>
						<li>
							<a href="javascript:void(0);"><img src="img/avatars/sunny.png" alt="">Sunny <span class="state"><i class="last-online pull-right">2m</i></span> </a>
						</li>
						<li>
							<a href="javascript:void(0);"><img src="img/avatars/male.png" alt="">Denmark <span class="state"><i class="last-online pull-right">2m</i></span> </a>
						</li>
					</ul>
				</div>
				<div class="chat-list-footer">
					<div class="control-group">
						<form class="smart-form">
							<section>
								<label class="input">
									<input type="text" id="filter-chat-list" placeholder="Filter">
								</label>
							</section>
						</form>
					</div>
				</div>
			</div>

			<!-- CHAT BODY -->
			<div id="chat-body" class="chat-body custom-scroll">
				<ul>
					<li class="message">
						 
						<img src="img/avatars/5.png" class="online" alt="">
						<div class="message-text">
							<time>
								2014-01-13
							</time> 
							<a href="javascript:void(0);" class="username">Administrator</a> Work in Progress							
						</div>
					</li>
				</ul>
			</div>
			<!-- CHAT FOOTER -->
			<div class="chat-footer">
				<!-- CHAT TEXTAREA -->
				<div class="textarea-div">
					<div class="typearea">
						<textarea placeholder="Write a reply..." id="textarea-expand" class="custom-scroll ptc_tbltextbox"></textarea>
					</div>
				</div>
				<!-- CHAT REPLY/SEND -->
				<span class="textarea-controls">
					<button class="btn btn-sm btn-primary pull-right">
						Reply
					</button> <span class="pull-right smart-form" style="margin-top: 3px; margin-right: 10px;"> <label class="checkbox pull-right">
							<input type="checkbox" name="subscription" id="subscription">
							<i></i>Press <strong> ENTER </strong> to send </label> </span> </span>

			</div>
			<!-- end content -->
		</div>
	</div>			  <!-- end widget div -->		  
</div> <!-- end widget -->