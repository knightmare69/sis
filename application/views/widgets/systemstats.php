<div class="col-sm-12 col-md-12 no-padding">
<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-sysstats" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false">
 <header>
  <h2><i class="fa fa-bar-chart-o"></i> System Statistics</h2>
  <div class="widget-toolbar" role="menu">
   <button class="btn btn-primary" onclick="$('.stat-activated').attr('data-percent',90); runAllCharts(); alert('trial');"><i class="fa fa-refresh"></i> Trial</button>
  </div>
 </header>
 <div>
  <div class="jarviswidget-editbox"><!-- This area used as dropdown edit box --></div>
  <div class="widget-body no-padding">
    <div class="col-sm-12">
	 <p>Statistics as of <?php echo date("Y-m-d H:i:s");?></p>
	 <div class="col-sm-12">
	  <div id="statsChart" class="chart has-legend-unique" data-ticks="<?php echo $schart['date'];?>" data-content1="<?php echo $schart['access'];?>" data-content2="<?php echo $schart['reg'];?>"></div>
	 </div>
				
	<div class="show-stat-microcharts">
	  <div class="col-xs-6 col-sm-4">
	    <div class="easy-pie-chart txt-color-blueLight" data-percent="<?php echo $stats->Validated;?>" data-pie-size="50">
		 <span class="percent percent-sign"><?php echo $stats->Validated;?></span>
		</div>
		<span> Validated</span>
	  </div>
      <div class="col-xs-6 col-sm-4">
	    <div class="easy-pie-chart txt-color-greenLight stat-activated" data-percent="<?php echo $stats->Activated;?>" data-pie-size="50">
	     <span class="percent percent-sign"><?php echo $stats->Activated;?></span>
	    </div>
		<span> Activated</span>
	  </div>
	  <div class="col-xs-12 col-sm-4">
		<table class="table table-condensed">
		  <tr><td width="40%"><small>Accessing:<small></td><td><strong><?php echo round($stats->Access,2);?></strong></td></tr>
		  <tr><td width="40%"><small>Online User:</small></td><td><strong><?php echo round($stats->Online,2);?>/<?php echo round($stats->Registered,2);?></strong></td></tr>
		</table>
	  </div>
	</div>
	</div>
  </div>
 </div>
</div>
</div>