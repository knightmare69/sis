$(function () {
     function calculate() {
         $(".item").each(function(){
             var $start = $(this).find(".start"),
                 $end = $(this).find(".end"),
                 $result = $(this).find(".hours"),
                 time1 = $start.val().split(':'),
                 time2 = $end.val().split(':'),
                 hours1 = parseInt(time1[0], 10), 
                 hours2 = parseInt(time2[0], 10),
                 mins1 = parseInt(time1[1], 10),
                 mins2 = parseInt(time2[1], 10);
                 hours = hours2 - hours1,
                 mins = 0;

             if(hours < 0) hours = 24 + hours;
             if(mins2 >= mins1) {
                 mins = mins2 - mins1;
             } else {
                 mins = (mins2 + 60) - mins1;
             }

             // the result
             $result.val(hours + ':' + mins);   
         });
     }
    $(".start,.end").on("change", calculate);
    $("#calculate").on("click", calculate);
    calculate();

});