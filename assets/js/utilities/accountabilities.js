$(document).ready(function(){
	$('#studentno').change(function(){
	 var studno = $(this).val();
	 antidepressant();
	 
	 $.ajax({
	  type:"POST",
	  url:base_url+'accountabilities/data/info',
	  data:{student:studno},
	  dataType: "JSON",
	  async:true,
	  success: function (result){
	   if(result.success)
	   {
	    $('#details').html(result.content);
	    load_accounts(studno);
	   }
	   else
	   {
		alertmsg('danger','Failed To Load Data!');   
	    rem_antidepr();
	   }   
	  },
      error:function(result){
	   alertmsg('danger','Failed To Load Data!');
	   rem_antidepr();
	  }
	 });		 
	});
	
	$('body').on('click','#btnselectitem',function(){
	 var studno = $('.select-list .info').attr('id');
	 var studname = $('.select-list .info').find('.studname').html();
	 
	 $('#studentfilter').attr('data-id',studno);
	 $('#studentfilter').val(studname);
	 $('#studentno').val(studno);
	 $('#studentno').change();
	 $('#selectmodal').modal('hide');
	});
	
	$('.unclearonly').change(function(){
	 var ischecked = $(this).prop('checked');
	 if(ischecked)
	  $('[data-clear="1"]').addClass('hidden');
     else
	  $('[data-clear="1"]').removeClass('hidden');
     	 
	});
		
});	

function load_accounts(xstudno)
{
 if(xstudno==undefined){rem_antidepr(); return false; }	
 $.ajax({
  type:"POST",
  url:base_url+'accountabilities/data/accounts',
  data:{student:xstudno},
  dataType: "JSON",
  async:true,
  success: function (xdata){
   if(xdata.success)
   {
	$('.tbl-accounts').html(xdata.content);
	$(this).prop('checked',true);
	rem_antidepr();
   }
   else
   {
	alertmsg('danger','Failed To Load Data!');   
	rem_antidepr();
   }   
  },
  error:function(result){
   alertmsg('danger','Failed To Load Data!');
   rem_antidepr();
  }
 });
}