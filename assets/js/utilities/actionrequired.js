$('document').ready(function(){
  
});

function checkfields()
{
 if($('#email').length > 0)
 {
  if ($('#email').val().trim()=='')
  {
   $('#email').attr('onfocus','removewarning(this)');
   $('#email').addClass('bg-color-danger');
   alert('No email');
   return false;
  }
 }
 
 if($('#newpwd').length > 0 && $('#conpwd').length > 0)
 {
  if ($('#newpwd').val().trim()=='')
  {
   $('#newpwd').attr('onfocus','removewarning(this)');
   $('#newpwd').addClass('bg-color-danger');
   alert('No Password');
   return false;
  }
  else if ($('#conpwd').val().trim()=='')
  {
   $('#conpwd').attr('onfocus','removewarning(this)');
   $('#conpwd').addClass('bg-color-danger');
   alert('No Password Confirmation');
   return false;
  }
  else if ($('#newpwd').val().trim()!=$('#conpwd').val().trim())
  {
   $('#newpwd').attr('onfocus','removewarning(this)');
   $('#conpwd').attr('onfocus','removewarning(this)');
   $('#newpwd').addClass('bg-color-danger');
   $('#conpwd').addClass('bg-color-danger');
   alert('Check your password');
   return false;
  }
 }
 
 return true;
}

function removewarning(e)
{
 var xid = e.id;
 xid='#'+xid;
 $(xid).removeAttr('onfocus');
 $(xid).removeClass('bg-color-danger');
}