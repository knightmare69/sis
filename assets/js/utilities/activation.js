function displaystudentinfo() {
        var studentno = document.getElementById('studentno').value;
        //var activeTabIndex = $.cookie("activeTabIndex");
        //document.getElementById('hiddenstudentno').value=studentno;
        
        //document.forms['studentsearch'].submit(function(event){
        //event.preventDefault();
        //$("#myTab1").tabs("option", "active", 2);
        //$("#myTab1").tabs('active', 1);
        //});
        
        $.ajax({
				type: "POST",
				url: document.URL + '/searchstudentinfo',
				data: {studno:studentno}, 
				success:function(output)
				{
										var studinfo = output.split(':')
										document.getElementById('studentname').innerHTML= studinfo[0];
										document.getElementById('degree').innerHTML= studinfo[1];
										document.getElementById('hiddenstudentno').value= studentno;
				},
				error: function (XHR, status, response) {
																
				$.smallBox({
						title : "Internal Error!",
						content : "<i class='fa fa-clock-o'></i> <i>You pressed Submit...</i>",
						color : "#C46A69",
						iconSmall : "fa fa-times fa-2x fadeInRight animated",
						timeout : 6000
						});	  
				}
											
			});

        
}

function submitform() {
         
        var studentname = document.getElementById('studentname').innerText;
        var studentno = document.getElementById('hiddenstudentno').value;
        var parentid = document.getElementById('userid').value;
        var birthdate = document.getElementById('dateofbirth').value;
        var termid = document.getElementById('termid').value;
        var regid = document.getElementById('registrationid').value;
        var receipt = document.getElementById('receipt').value;
        var date = document.getElementById('date').value;
        
        if (studentno == '') {
                  $.smallBox({
										title : "You failed to submit valid informations. Portal Activation is not successful!",
										content : "<i class='fa fa-clock-o'></i> <i> You pressed Submit...</i>",
										color : "#C46A69",
                                        iconSmall : "fa fa-times fa-2x fadeInRight animated",           
										timeout : 6000
									});      
        }
        else{
        

        $.SmartMessageBox({
				title : "Parent Portal Activation!",
				content : "You are now about to activate Parent Portal using  " + studentname + " informations.",
				buttons : '[Cancel][Activate]',
				
			}, function(ButtonPressed) {
				if (ButtonPressed === "Activate") {
                                                $.ajax({
                                                type: "POST",
                                                url: document.URL + '/validatestudentinfo',
                                                data: {studno:studentno, birth:birthdate, term:termid, registration:regid, receiptno:receipt, payment:date}, 
                                                success:function(res)
                                                {
                                                    if (res== 1){
                                                                    $.smallBox({
																		title : "Portal Activation is successful!",
																		content : "<i class='fa fa-clock-o'></i> <i> You pressed Submit...</i>",
																		color : "#659265",
																		iconSmall : "fa fa-check fa-2x fadeInRight animated",
																		timeout : 40000
																	});
																	antidepressant();
																	setTimeout(function(){document.forms['parent_portal'].submit(function(event){event.preventDefault();});},2500);
                                                                }
                                                            else{
                                                                   $.smallBox({
																		title : "You failed to submit valid informations. Portal Activation is not successful!",
																		content : "<i class='fa fa-clock-o'></i> <i> You pressed Submit...</i>",
																		color : "#C46A69",
																		iconSmall : "fa fa-times fa-2x fadeInRight animated",
																		timeout : 8000
																	});
                                                                }
                                                                                                
                                                },
                                                error: function (XHR, status, response) {
                                                                                                
                                                $.smallBox({
                                                        title : "Internal Error!",
                                                        content : "<i class='fa fa-clock-o'></i> <i>You pressed Submit...</i>",
                                                        color : "#C46A69",
                                                        iconSmall : "fa fa-times fa-2x fadeInRight animated",
                                                        timeout : 4000
                                                        });	  
                                                }
                                                        
                        });
                                        										
				}
				if (ButtonPressed === "Cancel") {
					$.smallBox({
						title : "Portal Activation has been cancelled",
						content : "<i class='fa fa-clock-o'></i> <i>You pressed Cancel...</i>",
						color : "#C46A69",
						iconSmall : "fa fa-times fa-2x fadeInRight animated",
						timeout : 4000
					});
				}
	
			});
			e.preventDefault();
}
}

function checkifscholar(regid)
{
  var val = regid.value;
  if(val!='' && val!=' ')
  {
  var addr = $("#student_portal").attr('action');
  addr = addr.replace('verifystudent','checkifscholar');
  //alert(val);
  $.ajax({
			type: "POST",
			url: addr,
			data: {reg:val}, 
			success:function(output)
			{
				var ifscholar = output;
				
                if(ifscholar==1 || ifscholar=='1')
				{
				 //alert(ifscholar);
				 //$('#orno').removeAttr('required'); 
				 //$('#ordt').removeAttr('required');
				}
                else
                {
				 //$('#orno').attr('required',true); 
				 //$('#ordt').attr('required',true);                  
			    }				
			},
			error: function (XHR, status, response) {
															
			$.smallBox({
					title : "Internal Error!",
					content : "<i class='fa fa-clock-o'></i> <i>Undefined Server Response.</i>",
					color : "#C46A69",
					iconSmall : "fa fa-times fa-2x fadeInRight animated",
					timeout : 6000
					});	  
			}
         });
  }
}


$(document).ready(function() {
			console.log(localStorage.getItem("campus"));
	        $('.welcome_campus').on('click', function() {
			  localStorage.setItem("campus", $(this).data('name'));
			  var c =	localStorage.getItem("campus");
			  window.location.href = base_url+'admission/applicant';
			});
			
			pageSetUp();					

			  var $validator = $("#student_portal").validate({
			    
			    rules: {
			      //fname:  { required: true },
			      //lname:  { required: true },			      
			      //gender: { required: true },
                  bday: { required: true },
                  studno: { required: true }//,
                  //program: { required: true },
                  //ayterm:{ required: true  },
                  //regno: { required: true },                  
                  //orno:{ required: true  },                  
                  //ordt:{ required: true }                
                  
			    },
			    
			    messages: {
			        //fname: "Please specify your First name",
			        //lname: "Please specify your Last name",
                    studno: "Please specify your Student number",
					bday: "Please specify your birthday"
                    //program: "Please specify your Academic program",
			        //email: {
			        //required: "We need your email address to contact you",
			        //email: "Your email address must be in the format of name@domain.com"
			        //}
			    },			   
					
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
				
			  });	  

	$('#proceed').on('click', function(){
		if ($('input[name="remember"]').prop('checked')) {
			window.location.href = base_url+'admission/applicant';
		}else {
		   alert('alert');
		}
    });
                        
});
