var course_opt = '';
$(document).ready(function() {
            console.log(localStorage.getItem("campus"));
	        $('body').on('change','#Choice2_Course',function(){
			   var tmpA = $('#Choice1_Course').val();
			   var tmpB = $(this).val();
			   if(tmpB>0 && (tmpA<=0 || tmpA==undefined)){
				   alert('You must select First Choice before you select your Second Choice!');
				   $('#Choice1_Course').focus();
				   $(this).val(0);
			   }
	        });
			
			$('body').on('change','#ApplyTypeID',function(){
			   $('.for_transferee').addClass('hidden');
			   if($(this).val()==2){
			     $('.for_transferee').removeClass('hidden');
			   }
			});
			
	        $('body').on('change','#Choice1_Course',function(){
			   var tmpA = $(this).val();
			   var tmpB = $(this).find('option[value="'+tmpA+'"]').attr('data-college');
			   course_opt = $(this).html();
			   $('#Choice2_Course').html(course_opt);
			   $('#Choice2_Course').find('option[data-college!="'+tmpB+'"]').remove();
			   $('#Choice2_Course').prepend('<option value="0" selected="" disabled=""> - Select Course - </option>');
			   $('#Choice3_Course').html(course_opt);
			   $('#Choice3_Course').find('option[data-college!="'+tmpB+'"]').remove();
			   $('#Choice3_Course').prepend('<option value="0" selected="" disabled=""> - Select Course - </option>');
			   if(tmpB==3 || tmpB==4){
				   $('#Choice2_Course').closest('section').addClass('hidden');
				   $('#Choice2_Course').val(tmpA);
				   if(tmpB==3){
				   $('#College_School').closest('.row').removeClass('hidden');
				   }
			   }else if(tmpB<3){
				   $('#Choice2_Course').closest('section').removeClass('hidden');
				   $('#Choice2_Course').val(0);
               }
	        });
	        
			$('body').on('change','#Res_TownCity',function(){
			   var tmpA = $(this).val();
			   var tmpB = $(this).find('option[value="'+tmpA+'"]').attr('data-prov');
			   var tmpC = $('#Res_Province').find('option[data-prov="'+tmpB+'"]').attr('value');
			   $('#Res_Province').val(tmpC);
            });
	        
			$('body').on('change','#Perm_TownCity',function(){
			   var tmpA = $(this).val();
			   var tmpB = $(this).find('option[value="'+tmpA+'"]').attr('data-prov');
			   var tmpC = $('#Perm_Province').find('option[data-prov="'+tmpB+'"]').attr('value');
			   $('#Perm_Province').val(tmpC);
            });
			
			$('body').on('change','#NationalityID',function(){
			   var tmpA = $(this).val();
			   $('#Father_Nationality').val(tmpA);
			   $('#Mother_Nationality').val(tmpA);
			});
			
			$('body').on('change','#drp_ExtName',function(){
			  var tmpA = $(this).val();
              if(tmpA!='-1'){
                $('#ExtName').attr('type','hidden');
			    $('#ExtName').val(tmpA);
			  }else{ 
			   $('#ExtName').val('');
			   $('#ExtName').attr('type','text');
			   $('#ExtName').focus();
			   $(this).addClass('hidden');
			  }
			});
			
			$('body').on('blur','#ExtName',function(){
			  var tmpA = $(this).val();
              if(tmpA=='' || tmpA==''){
                $('#ExtName').attr('type','hidden');
			    $('#drp_ExtName').removeClass('hidden');
			    $('#drp_ExtName').val('');
			  }
			});
			
			$('body').on('change','#ReligionID',function(){
			  var tmpA = $(this).val();
              if(tmpA!='-1'){
                $('#ReligionOther').attr('type','hidden');
			    $('#ReligionID').val(tmpA);
				$(this).closest('label').attr('class','select');
			  }else{ 
                $('#ReligionOther').val('');
                $('#ReligionOther').attr('type','text');
                $('#ReligionOther').focus();
                $(this).addClass('hidden');
				$(this).closest('label').attr('class','input');
              }
			});
			
			
			$('body').on('blur','#ReligionOther',function(){
			  var tmpA = $(this).val();
			  console.log(tmpA);
			});
			
	        $('.welcome_campus').on('click', function() {
			  localStorage.setItem("campus", $(this).data('name'));
			  var c =	localStorage.getItem("campus");
			  window.location.href = base_url+'admission/applicant';
			});
			
			pageSetUp();
			init();

			  var $validator = $("#fuelux-wizard").validate({

			    rules: {
						FirstName: {
							required: true,
							maxlength:60,
							remote: {
								url: base_url+"admission/namevalidation",
								data: {
										termid: function() {
	            				return $("#TermID").val();
										},
										LastName: function() {
	            				return $("#LastName").val();
										},
										FirstName: function() {
	            				return $("#FirstName").val();
										},
										DateOfBirth: function() {
	            				return $("#DateOfBirth").val();
										},

								},
							 type: "post"}
						},
						MiddleName: {
								maxlength:60
						},
						ExtName: {
								maxlength:60
						},
						LastName: {
							required: true,
							maxlength:60,
							remote: {
								url: base_url+"admission/namevalidation",
								data: {
										termid: function() {
	            				return $("#TermID").val();
										},
										LastName: function() {
	            				return $("#LastName").val();
										},
										FirstName: function() {
	            				return $("#FirstName").val();
										},
										DateOfBirth: function() {
	            				return $("#DateOfBirth").val();
										},

								},
							 type: "post"}

						},
						Gender: {
							required: true
						},
						DateOfBirth: {
							required: true,
							date: true,
						},
						civilstatus: {
							required: true
						},
						ReligionID: { required: true },
						NationalityID: { required: true },
						MobileNo: { required: true, maxlength:14, number: true, remote: {url: base_url+"admission/mobilevalidation", data: {termid: function() { return $("#TermID").val();}}, type: "post"}},
						Res_Address: {required : true,maxlength:250},
						Res_Street: {required: false,maxlength:250 },
						Perm_Street: {required: false,maxlength:250 },
						Res_TownCity : {required: true,maxlength:250},
						Res_Province : {required: true,maxlength:250},
						Res_ZipCode : {required: true, number:true,maxlength:10},
						Perm_ZipCode :{number:true, maxlength:10},
						Perm_Address: {maxlength:250 },
						Perm_TownCity: {maxlength:250 },
						Perm_Province: {maxlength:250 },
						TelNo :{number:true, maxlength:20 },
						PlaceOfBirth : {required: true},
						Father_Nationality: {maxlength: 60},
						Father: {required: true, maxlength: 60},
						Father_Occupation: {maxlength: 60},
						Father_TelNo: {number:true,maxlength: 60},
						Mother_Nationality: {maxlength: 60},
						Father_Address: {maxlength: 2500},
					      //Father_Email: {email:true,maxlength: 60},
						Mother: {required:true, maxlength: 60},
						Mother_Occupation: {maxlength: 60},
						Mother_TelNo: {number:true,maxlength: 60},
						Mother_Address: {maxlength: 2500},
					      //Mother_Email: {email:true,maxlength: 60},
						Guardian: {maxlength: 250},
						Guardian_Address: {maxlength: 2500},
						Guardian_TelNo: {number:true,maxlength: 60},
						Guardian_Relationship: {maxlength: 60},
						Spouse: {maxlength: 60},
						Spouse_Address: {maxlength: 2500},
						Spouse_Email: {email:true,maxlength: 60},
						Spouse_TelNo: {number:true, maxlength: 60},
						JHS_School: {maxlength: 60},
						JHS_Address: {maxlength: 2500},
						JHS_Graduated: {maxlength: 60},
						JHS_Award: {maxlength: 60},
						SHS_School: {maxlength: 60},
						JHS_Degree: {maxlength: 60},
						SHS_Degree: {maxlength: 60},
						Disability: {maxlength: 90},
						SHS_Address: {maxlength: 2500},
						SHS_Graduated: {maxlength: 60},
						SHS_Award: {maxlength: 60},
						College_School: {maxlength: 60},
						College_Address: {maxlength: 2500},
						College_InclDates: {maxlength: 60},
						College_Award: {maxlength: 60},
						LastSchool_Name: {maxlength: 60},
						LastSchool_Address: {maxlength: 2500},
						LastSchool_Degree: {maxlength: 60},
						LastSchool_SY: {maxlength: 60},
						wnamea: {maxlength: 60},
						waddra: {maxlength: 2500},
						wpositiona: {maxlength: 60},
						wcontacta: {maxlength: 60},
						lexam_passed: {maxlength: 40},
						Other_School: {maxlength: 60},
						know_others: {maxlength: 60},
						Mother_Tongue: {maxlength: 40},
						Other_Language: {maxlength: 40},
						LRN: {maxlength: 90, number:true,},
						Indigenous_Group : {maxlength: 40},
						TermID:{ required: true  },
						Choice1_campusID:{ required: true  },
						ApplyTypeID:{ required: true },
						Choice1_Course:{ required: true },
						Choice2_Course:{ required: true },
						Email: {email:true,required :true, maxlength: 60, remote: {url: base_url+"admission/emailvalidation", data: {termid: function() {
                return $("#TermID").val();}}, type: "post"}},
			    },

			    messages: {
			      FirstName:{
							required: "Please specify your LastName, FirstName and Birthdate",
							remote: "LastName, FirstName and Birth Date already exists",
						} ,
						LastName:{
								required: "Please specify your LastName, FirstName and Birthdate",
							remote: "LastName, FirstName and Birth Date already exists",
						} ,
						DateOfBirth:{
									required: "Please specify your LastName, FirstName and Birthdate",
								remote: "LastName, FirstName and Birth Date already exists",
							} ,
						MobileNo:{
							remote: "This Mobile Number is already taken! Try another.",
						},
			      Email: {
			        required: "We need your email address to contact you",
			        Email: "Your email address must be in the format of name@domain.com",
							remote: "This username is already taken! Try another.",

			      }
			    },
					groups: {
						nameGroup:  "LastName FirstName  DateOfBirth ",
					},

				// Do not change code below
				errorPlacement : function(error, element) {
					if (element.attr("name") == "LastName" || element.attr("name") == "FirstName" ||  element.attr("name") == "DateOfBirth" )
						error.insertAfter(element.parent());
						 //error.insertAfter("#LastName");
					else
					// error.insertAfter(element);
					error.insertAfter(element.parent());
				}

			  });

			// fuelux wizard
			  var wizard = $('.wizard').wizard();

				$('#admissionWizard').on('actionclicked.fu.wizard', function (evt, data) {
		// do somethingco
	//	console.log('next');
	});


			  wizard.on('finished', function (e, data) {


					//	 console.log("submitted!");
									 //$("#fuelux-wizard").submit();

								//	 e.preventDefault();
									 var tdata= $("#fuelux-wizard").serializeArray();
							//		 console.log(tdata);
									 $.ajax({
													 type: "POST",
													 url: $("#fuelux-wizard").attr('action'),
													 data: tdata,
													   dataType: "json",
													 success:function(tdatas)
													 {
												//		console.log(tdatas);

														$.ajax({
																	 type: "POST",
																	 url: base_url+'admission/submitted',
																		dataType: "json",
																	 data: {Name: tdatas.Name , AppNo: tdatas.AppNo },
																	 success:function(tdata)
																	 {
															//			 console.log(tdata);
																	 	 $('#content').html(tdata)
																		 localStorage.removeItem("campus");
																	 }

																 });
															 // $.bigBox({
																// 			 title : "Congratulations!",
																// 			 content : tdatas.Name + ",  Your application was successfully submitted.. Kindly proceed to Admission Office for your verification of application. Thank you!",
																// color : "#739E73",
																// 			 //timeout: 8000,
																// 			 icon : "fa fa-check",
																// 			 number : ""
																// 			 }, function() {
																// 								 closedthis();
																// 			 });




													 },
													 error: function (XHR, status, response) {
														 // alert('fail');

																			 $.smallBox({
																			 title: "Oops! Sorry Your application have failed to submitted! Please try again later",
																			 content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
																			 color: "#C46A69",
																			 iconSmall: "fa fa-check bounce animated",
																			 timeout: 4000
																		 });

													 }

													 });





			  });

				function init(){
					var campus_s = 	localStorage.getItem("campus");
					var $radios = $('input:radio[name=department]');
					$('#DateOfBirth').combodate({maxYear: 2018,});
					$('.month').attr('placeholder','Month');
					$('.day').attr('placeholder','Day');
					$('.year').attr('placeholder','Year');
					if(campus_s =='taytay'){
						$radios.filter('[value=shs]').prop('checked', true);
						$('.for_shs').removeClass('hidden');
						$('.for_college').addClass('hidden');
						$('#Choice1_campusID').val(1002);
						campuslist(campus_s);
						var apptype = getData(base_url+'admission/getapptype','POST',{isshs:1, campus:campus});
					//	console.log('app'+apptype.data);
						apptypes(apptype.data);
					}else{
							$('#Choice1_campusID').val(1);

						$radios.filter('[value=college]').prop('checked', true);
						$('.for_shs').addClass('hidden');
							$('#radiocollege').removeClass('hidden');
							campuslist(campus_s);
					}
					var shs = $('#department').val();
					var c = (shs == 'shs' ? 1 : 0)
					var campus = $('#Choice1_campusID').val();
					var result = getData(base_url+'admission/getcourse','POST',{isshs:c, campus:campus});
					var apptype = getData(base_url+'admission/getapptype','POST',{isshs:c, campus:campus});
			//		console.log('app'+apptype.data);
					apptypes(apptype.data);
					var courselists = result.data;
					courselist(courselists);
				}

            function closedthis()
			{
				/*$.smallBox({
					title : "Great! You just closed that last alert!",
					content : "This message will be gone in 5 seconds!",
					color : "#739E73",
					iconSmall : "fa fa-cloud",
					timeout : 5000
				}); */
				//$("#btncancel").attr('href');
				var xredirect = $("#mnuhome").attr('href');
				    xredirect = xredirect+'/logout';
                                    window.location = xredirect;
			}

			function campuslist(campus){

				var a ='';
				a += '<option value="0" selected="" disabled=""> - Select Campus - </option> ';
				if(campus == 'taytay'){
					a += '<option value="gs">Grade School</option>';
					a += '<option value="jhs">Junior High School</option>';
				    a += '<option value="shs" '+(campus== 'taytay'? 'selected': '')+' >Senior High School</option>';
				}else{
				    a += '<option value="shs" '+(campus== 'taytay'? 'selected': '')+' >Senior High School</option>'; 
					a += '<option value="college" selected>Undergraduate/College/Professional</option>';
					a += '<option value="graduate">Graduate</option>';
				}

				$('#department').html(a);
                                $('#department').trigger('change');
			}
			function apptypes(type){

				var a= '';
				a = '	<option value="0" selected="" disabled=""> - Select Application Type - </option>'
				$.each(type, function(key, val){
					a += '<option '+(val.ApplicationType == 'Freshman' ? 'selected' : '')+' value='+val.TypeID+'  >'+ val.ApplicationType + ' </option>';
				});

				$('#ApplyTypeID').html(a);

			}

			function courselist(course){
				var ismobile  = (!$('#fullscreen').is(':visible'));
				var department = $('#department').val();
                                var xlabel         = 'First Choice';
                                var ylabel         = 'Application Type';
				console.log('mobile:'+ismobile);
				var a  = '<option value="0" selected="" disabled=""> - Select Course - </option>';
                                var c = 0;
				$.each(course, function(key, val){
					if(department=='graduate' || department=='college'){
					  if((department=='graduate' && (val.CollegeID==5 || val.CollegeID==6 || val.CollegeID==7)) || (department=='college' && (val.CollegeID!=5 && val.CollegeID!=6 && val.CollegeID!=7))){
					    if(c!=val.CollegeID){
						  a += '<option value="-1" data-major="-1" data-college='+val.CollegeID+' disabled><strong></strong></option>';	
						  a += '<option value="-1" data-major="-1" data-college='+val.CollegeID+' disabled><strong>'+ val.CollegeName +'</strong></option>';	
						  c = val.CollegeID;
						}
						a += '<option value='+val.ProgID+' data-major="'+val.MajorID+'" data-college='+val.CollegeID+'>'+ val.ProgCode +' : ' + val.ProgName +''+ (val.Major ==''? '': ' <i>Major in </i> '+val.Major) +'</option>';
					  }
					}else{
                                            xlabel = "Academic Track Strand";
                                            ylabel = "Grade Level";
					    a += '<option value='+val.ProgID+' data-major="'+val.MajorID+'" data-college='+val.CollegeID+'>'+ val.ProgCode +' : ' + ((ismobile==false)?val.ProgName:'') +''+ (val.Major ==''? '': ' <i>Major in </i> '+val.Major) +'</option>';
					}
				});
				
				if(department=='gs' || department=='jhs'){
				 a = '<option value="16" data-major="0" data-college="8" selected disabled></option>';
				}
				$('#AppType_Label').html(ylabel);
				$('#Choice1_Label').html(xlabel);
				$('#Choice1_Course').html(a);
				$('#Choice3_Course').html(a);
				$('#Choice2_Course').html(a);
				
				rem_antidepr();
			}
			  //to follow
			   wizard.on('change', function (e, data) {
				// console.log('change');
			//	 console.log(data.step);



                                 $('.btn-prev').removeClass('disabled');
				 if (data.step === 6 && data.direction==='next'){
					 var tdatas= $("#fuelux-wizard").serializeArray();
					  console.log(tdatas);


						 if ($('#policy1').prop('checked') && $('#policy2').prop('checked') && $('#policy3').prop('checked')    ) {

							 $.ajax({
							 					type: "POST",
							 					url: base_url+'admission/overview',
							 					 dataType: "json",
							 					data: tdatas,
							 					success:function(tdata)
							 					{
							 						console.log(tdata);

							 						$('#overview_all').html(tdata)
							 					}

							 				});
						 }else {
							 e.preventDefault();
							 $.smallBox({
	 						title: "Oops! Please read and agree to the policy",
	 						content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
	 						color: "#C46A69",
	 						iconSmall: "fa fa-check bounce animated",
	 						timeout: 4000
	 					});
						 }



				 }

				 var $valid = $("#fuelux-wizard").valid();
				 //  $valid = true;

				 //if(data.step===1 && data.direction==='next') {
				 if(data.direction==='next') {        
					if(data.step===2){
					   $('.combodate').removeClass('state-error');
					   $('.combodate').removeClass('state-success');
					   $('.combodate').find('select').each(function(){
						  var tmpval = $(this).val();
						  if(tmpval==''){
							  $(this).closest('.combodate').addClass('state-error');
							  $valid = false;
						  } 
					   });			   
					}      
					
					if(data.step===3){
					   var fstats = $('[name="Father_CivilStatusID"]').val();			   
					   var mstats = $('[name="Mother_CivilStatusID"]').val();		 
					   $('#Father_TelNo').closest('label').removeClass('state-success');	   
					   $('#Mother_TelNo').closest('label').removeClass('state-success');
                                           $('#Father_TelNo').closest('label').removeClass('state-error');	   
                                           $('#Mother_TelNo').closest('label').removeClass('state-error');	   
					   if(fstats!=5 && $('#Father_TelNo').val()==''){
						  $('#Father_TelNo').closest('label').addClass('state-error');
						  $valid = false;
					   }
					   if(mstats!=5 && $('#Mother_TelNo').val()==''){
						  $('#Mother_TelNo').closest('label').addClass('state-error');
						  $valid = false;
					   }
					}   
					
					if(!$valid){
						// cancel change
						e.preventDefault();
						$validator.focusInvalid();
					}else{
						//////////////////
						// if (data.step === 2 && data.direction==='next'){
						// 	var FirstName = $('#FirstName').val();
						// 	var LastName = $('#LastName').val();
						// 	var Bday = $('#DateOfBirth').val();
						// 	var termid = $('#TermID').val();
						// 		// e.preventDefault();
						// 	$.ajax({
						// 					type: "POST",
						// 					url: base_url+'admission/namevalidation',
						// 					 dataType: "json",
						// 					data: {FirstName: FirstName ,LastName: LastName, DateOfBirth:Bday, termid : termid },
						// 					success:function(tdata)
						// 					{
						// 						console.log(tdata);
						// 						if (tdata === true){
						//
						//
						// 					 }else{
						//
						// 							 $.smallBox({
						// 							title: "Oops! You have already submitted your Application",
						// 							content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
						// 							color: "#C46A69",
						// 							iconSmall: "fa fa-check bounce animated",
						// 							timeout: 4000
						//
						// 						});
						//
						// 					 }
						// 					//	$('#content').html(tdata)
						// 					}
						//
						// 				});
						//
						// }
						/////////////////////////////////////
						// allow change
					}
				}

			  });
				//POLICY 1 start
				$('#policy1').on('click', function() {
					$('#policy1').prop('checked', false);
					$('#policymodal1').modal('show');
                                        if($("#scrollme").prop('scrollHeight') > $("#scrollme").prop('offsetHeight')){
				           $('#policymodal1').find('#agree_1').attr('disabled',true);
					   $('#scrollme').scrollTop = 0;
                                        }else{
				           $('#policymodal1').find('#agree_1').removeAttr('disabled');
					   $('#scrollme').scrollTop = 0;
                                        }
				});

				$('.modal-body').scroll(function() {
					var a = $('#scrollme').height();
					var b = $('.modal-body').prop('scrollHeight');
					var c = $('.modal-body').scrollTop();
					if ( c >= ((b-a -40 )* 0.8))  {
						$('#agree_1').removeAttr('disabled');
					}else {

					}
				});
				$('#agree_1').on('click', function(){
					$('#policy1').prop('checked', true);
					$('#policymodal1').modal('hide');

				});
				//POLICY 2
				//$('#policy2').on('click', function() {
				//	$('#policy2').prop('checked', false);
				//	$('#policymodal2').modal('show');
			    //});

				$('#scrollme2').scroll(function() {
					var a = $('#scrollme2').height();
					var b = $('#scrollme2').prop('scrollHeight');
					var c = $('#scrollme2').scrollTop();
					if ( c >= ((b-a -40 )* 0.8))  {
						$('#agree_2').removeAttr('disabled');
					}else {
					}
				});
				$('#agree_2').on('click', function(){
					$('#policy2').prop('checked', true);
					$('#policymodal2').modal('hide');

				});

				//POLICY 3
				$('#policy3').on('click', function() {
					$('#policy3').prop('checked', false);
					$('#policymodal3').modal('show');
				      //$('#policymodal3').find('#agree_3').attr('disabled',true);
					$('#scrollme3').scrollTop = 0;
				});

				$('#scrollme3').scroll(function() {
					var a = $('#scrollme3').height();
					var b = $('#scrollme3').prop('scrollHeight');
					var c = $('#scrollme3').scrollTop();
					if ( c >= ((b-a -40 )* 0.8))  {
						$('#agree_3').removeAttr('disabled');
					}else {
						$('#agree_3').attr('disabled');
					}
				});
				$('#agree_3').on('click', function(){
					$('#policy3').prop('checked', true);
					$('#policymodal3').modal('hide');

				});

				$('#knowbyh').on('click', function(){
				//	console.log($(this).val());

					if ($('#knowbyh').prop('checked')) {
						$('input[name="know_others"]').attr('type', 'text');
						//blah blah
					}else{
						$('input[name="know_others"]').attr('type', 'hidden');
					}
				});

				$('#same_as_res').on('click', function(){
					var res_a = $('#Res_Address').val();
					var res_s = $('#Res_Street').val();
					var res_t = $('#Res_TownCity').val();
					var res_p = $('#Res_Province').val();
					var res_z = $('#Res_Zipcode').val();
					if ($('#same_as_res').prop('checked')) {
					  	$('#Perm_Address').val(res_a);
							$('#Perm_Street').val(res_s);
							$('#Perm_TownCity').val(res_t);
							$('#Perm_Province').val(res_p);
							$('#Perm_ZipCode').val(res_z);
					}else{
						$('#Perm_Address').val('');
						$('#Perm_Street').val('');
						$('#Perm_TownCity').val('');
						$('#Perm_Province').val('');
						$('#Perm_ZipCode').val('');
					}
				});
				
				$('.addr_opt').on('click', function(){
				    var target = $(this).attr('data-target');
					var src    = $(this).attr('data-src');
					var xid    = ((src=='permanent')?'Perm':'Res');
					var a      = $('#'+xid+'_Address').val();
					    a      = ((a=='null' || a==undefined)?'':a);
					var s      = $('#'+xid+'_Street').val();
					    s      = ((s=='null' || s==undefined)?'':s);
					var t      = $('#'+xid+'_TownCity').val();
					    t      = ((t=='null' || t==undefined)?'':(t+','));
					var p      = $('#'+xid+'_Province').val();
					    p      = ((p=='null' || p==undefined)?'':p);
					var z      = $('#'+xid+'_Zipcode').val();
					$('.addr_opt[data-target="'+target+'"][data-src!="'+src+'"]').prop('checked',false);
					console.log(target);
					if ($(this).prop('checked')) {
						$('#'+target).val(a+' '+s+' '+t+' '+p);
					}else{
						$('#'+target).val('');
					}
				});

				$('#Choice1_campusID').on('change', function() {
				var campus = $('#Choice1_campusID').val();
				var res = getData(base_url+'admission/getcourse','POST',{isshs:0, campus:campus});
				var course = res.data;
				courselist(course);
				rem_antidepr();
				})


$('input[type=radio][name=isIndigenous]').change(function() {
if (this.value == '1') {
	$('input[name="Indigenous_Group"]').attr('type', 'text');
}
else if (this.value == '0') {
	$('input[name="Indigenous_Group"]').attr('type', 'hidden');
}
});

$('#department').change(function() {
        $('.for_shs').removeClass('hidden');
		$('.for_college').removeClass('hidden');
		$('.for_graduate').removeClass('hidden');
        $('.college-only').removeClass('hidden');		
        if (this.value == 'college' || this.value == 'graduate') {
				$('.for_shs').addClass('hidden');
				$('.for_graduate').addClass('hidden');
				$('.for_college').removeClass('hidden');
                if (this.value == 'graduate'){ 
                  $('#Choice2_Course').closest('section').addClass('hidden');
                  $('#Choice3_Course').closest('section').addClass('hidden');
				  $('#SHS_School').closest('.for_college').addClass('hidden');
				  $('.for_graduate').removeClass('hidden');
                }
				var campus = $('#Choice1_campusID').val();
				var res = getData(base_url+'admission/getcourse','POST',{isshs:0, campus:campus});
				var apptype = getData(base_url+'admission/getapptype','POST',{isshs:0, campus:campus, progclass:0});
			//	console.log('app'+apptype.data);
				apptypes(apptype.data);
				var course = res.data;
			//	console.log(res.data);
				courselist(course);
        }else if (this.value == 'shs') {
		          $('.college-only').addClass('hidden');
				  $('#Choice1_Course').closest('section').removeClass('hidden');

				  var campus = $('#Choice1_campusID').val();
				  $('.for_shs').removeClass('hidden');
				  $('.for_college').addClass('hidden');
				  $('.for_graduate').addClass('hidden');

				  var res = getData(base_url+'admission/getcourse','POST',{isshs:1, campus:campus});
					var apptype = getData(base_url+'admission/getapptype','POST',{isshs:1, campus:campus});
				//	console.log('app'+apptype.data);
					apptypes(apptype.data);
					var course = res.data;
			       //	console.log(res.data);
					courselist(course);
        }else if (this.value == 'gs'){
		            $('.college-only').addClass('hidden');
			        $('#Choice1_Course').closest('section').addClass('hidden');
			        $('#Choice2_Course').closest('section').addClass('hidden');

					var apptype = getData(base_url+'admission/getapptype','POST',{isshs:1, campus:campus, progclass:11});
				//	console.log('app'+apptype.data);
					apptypes(apptype.data);

				} else if (this.value == 'jhs'){
		            $('.college-only').addClass('hidden');
					$('#Choice1_Course').parent().parent().addClass('hidden');
			                $('#Choice2_Course').closest('section').addClass('hidden');


					var apptype = getData(base_url+'admission/getapptype','POST',{isshs:1, campus:campus, progclass:20});
			//		console.log('app'+apptype.data);
					apptypes(apptype.data);

				}
    });

		    $('body').on('change','[name="nationality"]',function(){
				var xval = $(this).val();
                if(xval<=1){
					$('.foreigndata').addClass('hidden');

				}else{
					$('.foreigndata').removeClass('hidden');
				}
			});

		    $('body').on('change','[name="choice1"],[name="Choice1_Course"]',function(){
				var xval = $(this).val();
				var xcol = $(this).find('option[value="'+xval+'"]').attr('data-college');
				var xmjr = $(this).find('option:selected').attr('data-major');
				var xarr = {'2':{'exam':'NMAT','school':'medical','class':'.for_COM'},'3':{'exam':'PHILSCAT','school':'law','class':'for_SOL'}};

				if(xmjr!==undefined){
						$('#Choice1_CourseMajor').val(xmjr);
				}

				if(xarr[xcol]!= undefined){
					$('.examdata').removeClass('hidden');
					$('.examname').html(xarr[xcol]['exam']);
					$('#otherapply').attr('placeholder',"What other "+xarr[xcol]['school']+" schools have you applied into?");
					$('.examschool').html(xarr[xcol]['school']);
					$(xarr[xcol]['class']).removeClass('hidden');
				}else{
					$('.examdata').addClass('hidden');
					$('.examdata').find('input[type="text"]').val('');
					$('.examdata').find('input[type="checkbox"]').prop('checked',false);
					$('.for_CAS').removeClass('hidden');
				}
			});
})
