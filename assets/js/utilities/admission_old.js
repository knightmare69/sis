$(document).ready(function() {
			pageSetUp();					

			  var $validator = $("#fuelux-wizard").validate({
			    
			    rules: {
			      fname: {
			        required: true
			      },
			      lname: {
			        required: true
			      },			      
			      gender: {
			        required: true
			      },
                  birthdate: {
			        required: true
			      },
                  civilstatus: {
			        required: true
			      },
                  religion: { required: true },
                  nationality: { required: true },
                  
                  elem: { required: true },
                  elemaddress: { required: true },
                  elemgraduate: { required: true },
                  
                  hs: { required: true },
                  hsaddress: { required: true },
                  hsgraduate: { required: true },
                  
                  ayterm:{ required: true  },
                  campus:{ required: true  },
                  apptype:{ required: true },
                  choice1:{ required: true },
                  choice2:{ required: true }
                  
                  
			    },
			    
			    messages: {
			      fname: "Please specify your First name",
			      lname: "Please specify your Last name",
			      email: {
			        required: "We need your email address to contact you",
			        email: "Your email address must be in the format of name@domain.com"
			      }
			    },			   
					
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
				
			  });
			  	  
			// fuelux wizard
			  var wizard = $('.wizard').wizard();
			  
			  wizard.on('finished', function (e, data) {
			    
			    console.log("submitted!");
                //$("#fuelux-wizard").submit();            
			      
                e.preventDefault();
                var tdata= $("#fuelux-wizard").serializeArray();                
                $.ajax({
                        type: "POST",
                        url: $("#fuelux-wizard").attr('action'),
                        data: tdata, 
                        success:function(tdata)
                        {                                                   
                            $.bigBox({
                                    title : "Congratulations!",
                                    content : tdata + "Your application was successfully submitted.. Kindly proceed to Admission Office for your verification of application. Thank you!",
                        			color : "#739E73",
                                    //timeout: 8000,
                                    icon : "fa fa-check",
                                    number : ""
                                    }, function() {
                                              closedthis();
                                    });
                            
                            
                        },
                        error: function (XHR, status, response) {
                          // alert('fail');
                          
                                    $.smallBox({
                                    title: "Oops! Sorry Your application have failed to submitted! Please try again later",
                                    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                    color: "#C46A69",
                                    iconSmall: "fa fa-check bounce animated",
                                    timeout: 4000                  
                                  });
                                                       
                        }

                        });
                                          
                
			  });
              
            function closedthis() 
			{
				/*$.smallBox({
					title : "Great! You just closed that last alert!",
					content : "This message will be gone in 5 seconds!",
					color : "#739E73",
					iconSmall : "fa fa-cloud",
					timeout : 5000
				}); */
				//$("#btncancel").attr('href');
				var xredirect = $("#mnuhome").attr('href');
				    xredirect = xredirect+'/logout';
                window.location = xredirect;
			}
            
		      
			  //to follow 
			   wizard.on('change', function (e, data) {
				 console.log('change');
				 
				 var $valid = $("#fuelux-wizard").valid();
				     $valid = true;
				  
				 //if(data.step===1 && data.direction==='next') {
                 if(data.direction==='next') {
					if(!$valid){
						// cancel change
						e.preventDefault();
						$validator.focusInvalid();										
					}else{
						// allow change						
					}
				}	
								
			  });
			  
		    $('body').on('change','[name="nationality"]',function(){
				var xval = $(this).val();
                if(xval<=1){
					$('.foreigndata').addClass('hidden');
				}else{
					$('.foreigndata').removeClass('hidden');
				}				
			});
			
		    $('body').on('change','[name="choice1"]',function(){
				var xval = $(this).val();
				var xcol = $(this).find('option[value="'+xval+'"]').attr('data-college');
				var xarr = {'3':{'exam':'NMAT','school':'medical'},'4':{'exam':'PHILSCAT','school':'law'}};
                if(xarr[xcol]!= undefined){
					$('.examdata').removeClass('hidden');
					$('.examname').html(xarr[xcol]['exam']);
					$('#otherapply').attr('placeholder',"What other "+xarr[xcol]['school']+" schools have you applied into?");
					$('.examschool').html(xarr[xcol]['school']);
				}else{
					$('.examdata').addClass('hidden');
					$('.examdata').find('input[type="text"]').val('');
					$('.examdata').find('input[type="checkbox"]').prop('checked',false);
				}				
			});
		})
