var selcouseid = 0;
var selsched = 0;
var selschedid = 0;
var selsubj = 0;
var selsubjid = 0;
var selsubjcode = '';
var selsubjtitle = '';
var selsubjlec = 0;
var selsubjlab = 0;
var selsubjunit = 0;
var selsubjallow = '';
var rowsel = 0;
var mousePos;
var waiting = '<span class="fa fa-spinner fa-spin fa-1x" style="color:Blue;"></span>';
var allow = '<i class="fa-fw fa fa-check-circle" style="color:Green;"></i>';
var isadvised = 0;
var alertid = 0;
var totalrequest = 0;
var prev_elem_style="";

function loadinfo(opt){   
 if(opt==undefined){opt='';}
 var divida = '#xstdeval';
 var dividb = '#xstdsum';
 var xlink = base_url+((opt=='')?'advising/txn/get/evaluation/':'advising/txn/get/re-evaluation/');
 $('#tblloader1').html('<span class="fa fa-refresh fa-spin fa-2x"></span>  Loading.. Please Wait..'); 
 try
 {
  $.ajax({
	  type: "GET",
	  url: xlink,
	  dataType: "JSON",
	  success: function (data) 
			   {
				 if(data.result)
				 {
				  if(data.content['evaluation']!=''){$(divida).html(data.content['evaluation']); }
				  if(data.content['summary']!=''){$(dividb).html(data.content['summary']); }
				  if(opt!=''){opt();}
				 }
				 else
				 {
				  $('#tblloader2').html('<i class="fa fa-warning"></i> Loading Failed!'); 
				 }					 
			  },
	  error: function (XHR, status, response) { $('#tblloader2').html('<i class="fa fa-warning"></i> Loading Failed!'); }
	 });
 }
 catch(e)
 {
  $('#tblloader2').html('<i class="fa fa-warning"></i> Loading Failed!');	 
 } 
}

function loadadvised(){
 var divid = '#xtbladv';
 $('#tblloader1').html('<span class="fa fa-refresh fa-spin fa-2x"></span>  Loading.. Please Wait..'); 
 //loadinfo();
 $.ajax({
		  type: "GET",
		  url: base_url+'advising/txn/get/advisedsubj/',
		  dataType: "JSON",
		  success: function (data) 
				   {
				     var xlock = $('#xlock').html();
				     var xerror = 0;     
					 if(data.result)
					 {
					  $(divid).html(data.content.xtable);
	                  /*generate the details of advised subject*/
					  $("#min").val(data.content.xdetail['min']);
					  $("#max").val(data.content.xdetail['max']);
					  $("#totalsubj").val(data.content.xdetail['count']);
					  $("#totallec").val(formatgrade(data.content.xdetail['totallec']));
					  $("#totallab").val(formatgrade(data.content.xdetail['totallab']));					  
                      $("#totalcunit").val(formatgrade(data.content.xdetail['totalcredit']));
					  
					  rem_antidepr();
					  if(data.content.xdetail['max']==0 || data.content.xdetail['max']=='' || data.content.xdetail['max']==undefined)
					  {
					   alarmarcadia(0.3);
					   return 0;
					  }
					  
					  if(xlock==0 || xlock==''){return 0;}
					  if(data.content.xdetail['noadvised']==0)
					  {
					   $('#xsave').removeClass('hidden');
					   $('#xsave').removeAttr('disabled');
					   $('#xsave').attr('type','submit');
					   $('#xaddwarn').removeClass('hidden');
					  }
					  else if(data.content.xdetail['noadvised']>0)
					  {
					   $('#xproceedreg').removeClass('hidden');
					   $('#xproceedreg').removeAttr('disabled');
					   $('#xproceedreg').attr('type','submit');
					   $('#xaddwarn').removeClass('hidden');
					  }					  
					 }
                     else
                     { 
				      $('#tblloader1').html('<p><i class="fa fa-warning"></i> Nothing To Advise<br/></p>');
					  rem_antidepr();
				     }
				   },
			error: function (XHR, status, response) {$('#tblloader1').html('<p>Loading Failed. Server Error<br/></p>');}
		 });

}

function proceedto(xlink)
{
 if(xlink!='' && xlink!=' ')
 {
  antidepressant();
  window.location = xlink;         
 }
}

function formatgrade(x){return !Number(x) ? x : Number(x)%1 === 0 ? Number(x).toFixed(2) : x; }

function trselected(e) {
   var mytbl = document.getElementById("advisedsubj");
   var rw = mytbl.getElementsByTagName("tr");
   $(rw).removeClass('bg-color-blueLight');   
   $(e).addClass('bg-color-blueLight');
   $(e).addClass('strong');
   rowsel = e.rowIndex;
   selcouseid = e.id.replace('s','');
   if (rw.item(rowsel).cells[5].innerHTML != ""){
      $("#btnremove").removeClass('disabled');   
   } else {
      $("#btnremove").addClass('disabled');   
   }
   
   $("#btnchange").removeClass('disabled'); 
   $("#btnchange").removeAttr('disabled'); 
   document.getElementById("btndelete").disabled=false;  
}

function schedselected(e) {
   var mytbl = document.getElementById("oschedule");
   var rw = mytbl.getElementsByTagName("tr");
   
   $(rw).removeClass('info');   
   $(e).addClass('info');
   selsched = e.rowIndex;
   selschedid = e.id;
   var isallow = rw.item(selsched).cells[0].id
   var xstr = isallow.split('_');
   var xreq = rw.item(selsched).cells[2].id
   //alert(xreq);
   if(xstr[1] == '1'){
      $("#btnsubmit").removeClass('disabled');
      $("#btnrequest").addClass('disabled');
   } else {
      $("#btnsubmit").addClass('disabled');
      $("#btnrequest").removeClass('disabled');   
   }
   
   if (xstr[2] == 1) {
         $("#btnprepared").removeClass('disabled');
   } else {
          $("#btnprepared").addClass('disabled');
   }
   
   if ( xreq == 3 ) {
         $("#btnrequest").text('Cancel Request');         
   } else {
         if (totalrequest ==  2) {
                  $("#btnrequest").addClass('disabled');
         } 
   
         $("#btnrequest").text('Send Request');
   }
   $("#btnprepared").removeClass("btn-success");          
}

function subjICselected(e) {
   var mytbl = document.getElementById("osubjIC");
   var rw = mytbl.getElementsByTagName("tr");
   var totalunit = 0;
   
   $(rw).removeClass('info');   
   $(e).addClass('info');
   selsubj = e.rowIndex-1;
   selsubjid = e.id;
   var subjunit = rw.item(selsubj).cells[5].innerHTML;
   
   selsubjcode = rw.item(selsubj).cells[1].innerHTML;
   selsubjtitle = rw.item(selsubj).cells[2].innerHTML;
   selsubjlec = rw.item(selsubj).cells[3].innerHTML;
   selsubjlab = rw.item(selsubj).cells[4].innerHTML;
   selsubjallow = rw.item(selsubj).cells[6].innerHTML;
   selsubjunit = parseFloat(subjunit);
   
   $("#btnsubjadd").removeAttr("disabled");
   $('#btnsubjadd').removeClass("disabled");
}

function subjNICselected(e) {
   var mytbl = document.getElementById("osubjNIC");
   var rw = mytbl.getElementsByTagName("tr");
   
   $(rw).removeClass('info');   
   $(e).addClass('info');
   selsubj = e.rowIndex-1;
   selsubjid = e.id;
   var subjunit = rw.item(selsubj).cells[5].innerHTML;
   
   selsubjcode = rw.item(selsubj).cells[1].innerHTML;
   selsubjtitle = rw.item(selsubj).cells[2].innerHTML;
   selsubjlec = rw.item(selsubj).cells[3].innerHTML;
   selsubjlab = rw.item(selsubj).cells[4].innerHTML;
   selsubjallow = rw.item(selsubj).cells[6].innerHTML;
   selsubjunit = parseFloat(subjunit);
   selsubjunit = parseFloat(subjunit);
   
   $('#btnsubjadd').removeAttr("disabled");
   $('#btnsubjadd').removeClass("disabled");
}

function checknaddsubj()
{
 var unit = 0;
 var subjid = selsubjid.replace('s','');
 var subjunit = selsubjunit;
 unit = document.getElementById('totallec').innerHTML;
 var lec = parseFloat(unit);
 unit = document.getElementById('totallab').innerHTML;
 var lab = parseFloat(unit);
 unit = document.getElementById('maxload').innerHTML;
 var max = parseFloat(unit);
 var temptotal = subjunit+lec+lab;
 var sbjallow = selsubjallow;
 var tbl = document.getElementById('advisedsubj'),
     rows = tbl.getElementsByTagName('tr');
        
 var subjectload = '';
 for (var i = 1, len = rows.length; i < len; i++) 
 {
  var subjloadid = rows.item(i).id;
  if(subjloadid!='')
  {subjloadid=subjloadid.replace('s','');
   subjectload = subjectload  + subjloadid + '|';}         
 }
 
 //"("+selsubjcode+")"+selsubjtitle
 if(subjectload.indexOf(subjid)>0)
 {
   $.SmartMessageBox(
   {
	 title : "<i class='fa fa-warning fa-2x' style='color:yellow;'></i> Subject is already in the list.",
	 content : "You can't enroll the same subject.",
	 buttons : "[Ok]"                 
   }, 
	 function(ButtonPress, Value) 
	 {
	   if (ButtonPress == "Ok") {return false;}
	 });
  
 }
 else if(sbjallow!='1' && sbjallow!=1)
 {
   var xreason = "You are not allow to add ("+selsubjcode+")"+selsubjtitle+".";
   if(sbjallow=='p')
   {xreason = "You are not allow to add ("+selsubjcode+")"+selsubjtitle+", because the prerequisite of this subject is not completed.";}
   if(sbjallow=='pass')
   {xreason = "You are not allow to add ("+selsubjcode+")"+selsubjtitle+", because you already passed this subject.";}
   if(sbjallow=='cre')
   {xreason = "You are not allow to add ("+selsubjcode+")"+selsubjtitle+", because this subject is already credited.";}
   if(sbjallow=='unp')
   {xreason = "You are not allow to add ("+selsubjcode+")"+selsubjtitle+", because this subject has grades and its not posted yet.";}
   
   $.SmartMessageBox(
         {
		 title : "<i class='fa fa-warning fa-2x' style='color:yellow;'></i> ("+selsubjcode+")"+selsubjtitle+" is not allowed to be add.",
		 content : xreason,
		 buttons : "[Ok]"                 
		 }, 
		 function(ButtonPress, Value) 
		 {
		   if (ButtonPress == "Ok") {return false;}
		 });
 }
 else if(max< temptotal)
 {
  $.SmartMessageBox(
         {
		 title : "<i class='fa fa-warning fa-2x' style='color:yellow;'></i> Sum of the units is greater than allowed maximum load units.",
		 content : "You must delete or remove some subjects to reduce units.",
		 buttons : "[Ok]"                 
		 }, 
		 function(ButtonPress, Value) 
		 {
		   if (ButtonPress == "Ok") {return false;}
		 });
 }
 else
 {
   var advid = document.getElementById("hfAdviseID").value;
   //var selcouseid
   $.ajax({
		type: "POST",
		url: $("#advurl").attr('href') + '/addsubject' ,
		data: {adviseid: advid,  subjectid : subjid},
		success: function (result) {               
           var strout = result;
		   //alert(strout);
		   if(result==1 || result=='1')
           {		   
			$.smallBox({
				 	    title: "Success! "+selsubjcode+" have been added.",
				 	    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
				    	color: "#739e73",
				    	iconSmall: "fa fa-check bounce animated",
					    timeout: 5000                  
				      });
				  
            $('#addsubjmodal').modal('hide');
			var row = "<tr id='s"+subjid+"' onclick='trselected(this)' ondblclick='popschedule()' style='font-size: x-small;'>"+
                       "<td></td>"+
					   "<td>"+selsubjcode+"</td>"+
				       "<td>"+selsubjtitle+"</td>"+
				       "<td style='font-weight: bold;'>"+selsubjlec+"</td>"+
				       "<td style='font-weight: bold;'>"+selsubjlab+"</td>"+
				       "<td></td>"+
				       "<td></td>"+				       
					  "</tr>";
             $('#advisedsubj tbody').append(row);
			 summarizetable();
			}
       		else if(result==0 || result=='0')
            {
			$.smallBox({
				 	    title: "Failed to add ("+selsubjcode+")"+selsubjtitle+"!",
				 	    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
				    	color: "#C46A69",
				    	iconSmall: "fa fa-times shake animated",
					    timeout: 5000                  
				      });
			}			
			
			},
		error: function (result) {
					alert(result.status + ': ' + result.statusText);
				}
	});
 } 
 
 summarizetable();
 selsubj = 0;
 selsubjid = 0;
 selsubjunit = 0;
}


function closemousemenu()
{       
  $("#schedoption").hide();
}
    
function handleMouseMove(event) {
   event = event || window.event; // IE-ism
   mousePos = {
       x: event.clientX,
       y: event.clientY
   };
}

function getMousePosition() {
    var pos = mousePos;
    if (!pos) {
        // We haven't seen any movement yet
    }
    else {
        // Use pos.x and pox.y
        alert(pos.x + ' x ' + pox.y); 
    }
}

function WhichButton (e,event) {
            // all browsers except IE before version 9
        if ('which' in event) {
            switch (event.which) {
            case 1:
                //alert ("Left button is pressed");
                closemousemenu();
                trselected(e);
                break;
            case 2:
                alert ("Middle button is pressed");
                break;
            case 3:
                alert ("Right button is pressed" + mousePos.x );
                //var offset = $( "#schedoption" ).offset();
                    
                //$( "#schedoption" ).style.top = mousePos.x + 'px';
	            //$( "#schedoption" ).style.left = mousePos.y + 'px';
                //$( "#schedoption" ).css({top: mousePos.y, left: mousePos.x});
                //$( "#schedoption" ).show();

                break;
            }
        }
        else {
                // Internet Explorer before version 9
            if ('button' in event) {
                var buttons = "";
                if (event.button & 1) {
                    buttons += "left";
                }
                if (event.button & 2) {
                    if (buttons == "") {
                        buttons += "right";
                    }
                    else {
                        buttons += " + right";
                    }
                }
                if (event.button & 4) {
                    if (buttons == "") {
                        buttons += "middle";
                    }
                    else {
                        buttons += " + middle";
                    }
                }
                alert ("The following buttons are pressed: " + buttons);
            }
        }
    }
    
function popschedule()
{
  if($('#btnchange').length > 0)
  {
   antidepressant();
   initoschedules();
  }
}

function remschedule(){
    
    var mytbl = document.getElementById("advisedsubj");
    var rw = mytbl.getElementsByTagName("tr");
    var cell = mytbl.getElementsByTagName('td');
    var advid = document.getElementById("hfAdviseID").value;
   
    selcouseid = rw.item(rowsel).id;
    removeschedule(advid ,selcouseid);
    rw.item(rowsel).cells[5].innerHTML="";
    rw.item(rowsel).cells[6].innerHTML="";
    //rw.item(rowsel).cells[9].innerHTML="";
    //rw.item(rowsel).cells[10].innerHTML="";
    //$( "#mouse_menu_right" ).hide();
	summarizetable();
}

function initoschedules(e){
    
    var tid = $("#hfTermID").val();
    var cid = $("#hfcampusid").val();
    var progid = $("#hfProgID").val();
    var currid = $("#hfCurrID").val();
    
    var mytbl = document.getElementById("advisedsubj");
    var rw = mytbl.getElementsByTagName("tr");
    var cell = mytbl.getElementsByTagName('td');

    selcouseid = rw.item(rowsel).id ;
    var subjid = selcouseid.replace('s','');
    
    if (subjid == '') 
	{
      alert("Please select your subject.");
      exit;
    }
    //alert('progid:'+ progid+', termid:'+ tid +', campusid:'+ cid+', curriculum:'+ currid+', subject:'+ subjid);
    $.ajax({
            type: "POST",            
            url: $("#advurl").attr('href') + '/offeredschedule' ,
            data: { progid: progid, termid: tid , campusid : cid, curriculum: currid, subject: subjid },
            success:function(tdata) {  loadofferedcourses(tdata) },
            error: function (XHR, status, response) {
			                                            rem_antidepr();
			                                            alert("No Offered schedule from your college/program");
														
			                                        }
            });
}

function loadofferedcourses(result) {

    $("#btnprepared").removeClass("btn-success");

	if  (result.trim() == ""){
		alert("No found schedule offered at this time.");
		exit;
	}
    $("#btnsubmit").addClass('disabled');
    totalrequest = 0
    $('#oschedule tr').slice(1).remove();
    var rwindex =0;
    var strings = result.split(';');
    for (var i = 0; i < strings.length; i++) {
      
        var col = strings[i].split('|');
        var schedid = col[0].replace('"','');
        var program = col[1];        
        var section = (col[2] == "" ? "&nbsp;" : col[2]);
        var subject = (col[3] == "" ? "&nbsp;" : col[3]);
        var limit = (col[4] == "" ? "&nbsp;" : col[4]);
        var schedule = (col[5] == "" ? "&nbsp;" : col[5]);
        var lec = (col[6] == "" ? "&nbsp;" : col[6]);
        var lab = (col[7] == "" ? "&nbsp;" : col[7]);
        var xcolor = (col[8] == "1" ? 'style="color:Blue;"' : '');
        var xallow = col[8] + "_" +col[9];
        var secid = col[10];
        var x = i+1;
        var row = '<tr id="'+ schedid.trim() + '" onclick="schedselected(this)" ondblclick="' + (col[8]=="1"? 'setschedule();':'') + '" ' +  xcolor + ' ><td id="allow'+x+'_'+xallow+'" align="center"><small>' + x + '.</small></td>' +
                    '<td><small>' + schedid + '</small></td>' +
                    '<td id="'+ col[8] +'"><small>' + program + (col[8]=="1"? allow:(col[8]=="3"? waiting:'')) + '</small></td>' +
                    '<td id="'+ secid +'"><small>' + section  +'</small></td>' +
                    '<td align="center"><small>' + subject + '</small></td>' +
                    '<td align="center"><small>' + lec + '</small></td>' +
                    '<td align="center"><small>' + lab + '</small></td>' +                    
                    '<td align="center"><small><b>' + limit.replace('\\','') + '</b></small></td>' +                                        
                    '<td style="white-space:pre"><small>' + schedule + '</small></td>' +                    
                  '</tr>'
        $('#oschedule tbody').append(row);
        rwindex=rwindex+1;
        if (col[8]==3) {
         totalrequest = totalrequest + 1
        }
        
    }

	rem_antidepr();
    $("#btnrequest").attr("data-original-title","<i class='fa fa-check text-success'></i> You have "+ totalrequest  +" request schedule(s)");
    $('#mwindow').modal('show')
    $("#oschedule").find("tr:first")
}

function setschedule(){        
    
    var advid = document.getElementById("hfAdviseID").value;
    
    var tbl = document.getElementById("oschedule");
    var rw = tbl.getElementsByTagName("tr");
    var cell = tbl.getElementsByTagName('td');
        
    var schedid = rw.item(selsched).id;    
    var section = rw.item(selsched).cells[3].innerHTML;
    var scode = rw.item(selsched).cells[4].innerHTML;
    var lec = rw.item(selsched).cells[5].innerHTML;
    var lab = rw.item(selsched).cells[6].innerHTML;
    var schedule = rw.item(selsched).cells[8].innerHTML;
    var my_sched = getmySchedules();
    
    if (my_sched != "") {

        $.ajax({
            type: "POST",            
            url: $("#advurl").attr('href') + '/checkconflict' ,
            data: { selcsid : schedid , myscids: my_sched },
            success: function (result) {
                     var conflict = result;
					 //alert(conflict);
                       if (conflict > 0 ){
                        //alert("Sorry, Unable to load selected schedule.. Conflict Found in your schedule(s)");
                        $.smallBox({
                           title: "Ooops! Conflict schedule was found. Selection schedule was cancelled!",
                           content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                           color: "#C46A69",
                           iconSmall: "fa fa-check bounce animated",
                           timeout: 5000                  
                        });
                         
                       } else{                        
                        var tblreg = document.getElementById("advisedsubj");
                        var reg_row = tblreg.getElementsByTagName("tr");

                        reg_row.item(rowsel).cells[5].innerHTML = section;
                        reg_row.item(rowsel).cells[6].innerHTML = schedule;
                        reg_row.item(rowsel).cells[1].id = schedid;
                        saveschedule( advid ,reg_row.item(rowsel).id,schedid,true);
						
                        $('#mwindow').modal('hide');
                        $("#btnremove").removeClass('disabled'); 
                        $("#btnregister").removeClass('disabled');  
                       }

                    },
            error: function (result) {
                        alert(result.status + ': ' + result.statusText);
                    }
        });    
    } 
 else{
        var tblreg = document.getElementById("advisedsubj");
        var reg_row = tblreg.getElementsByTagName("tr");

		reg_row.item(rowsel).cells[5].innerHTML = section;
		reg_row.item(rowsel).cells[6].innerHTML = schedule;
		reg_row.item(rowsel).cells[1].id = schedid;
		saveschedule( advid ,reg_row.item(rowsel).id,schedid,true);
		
		$('#mwindow').modal('hide');
		$("#btnremove").removeClass('disabled'); 
		$("#btnregister").removeClass('disabled');  
    }
}

function preparedschedule() {
    var pkid = document.getElementById("hfAdviseID").value;    
    var tbl = document.getElementById("advisedsubj");         
    var rw = tbl.getElementsByTagName("tr");         
    var rows = $('#advisedsubj tr').length;
    var scid = document.getElementById("oschedule").getElementsByTagName("tr").item(selsched).cells[3].id;

    $.ajax({
        type: "POST",
        url: base_url+'advising/preferredsection',
        data: { advid: pkid, idx: scid },
        success: function (result) {

            var tsr = result.split(';');
            for (var i = 0; i < tsr.length; i++) {
                var col = tsr[i].split('|');
                for (var j = 1; j <= rows; j++) {
                    //alert(rw.item(j).id + "|" + col[3].trim());
                    if (rw.item(j).id == 's' + col[3].trim()) 
					{
                        rw.item(j).cells[5].innerHTML = col[1];
                        rw.item(j).cells[6].innerHTML = col[2];
                        rw.item(j).cells[1].id = col[0].trim();
                        saveschedule( pkid ,rw.item(j).id,col[0].trim());                       
                        break;
                    }
                }
            }
            $.smallBox({
                    title: "Success! Prepared selected section completed.",
                    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                    color: "#739E73",
                    iconSmall: "fa fa-check bounce animated",
                    timeout: 5000
                });

            $('#mwindow').modal('hide');
            $("#btnremove").removeClass('disabled'); 
            $("#btnregister").removeClass('disabled');


        },
        error: function (result) {
            alert(result.status + ': ' + result.statusText);
        }
    });
        
         //alert( scid + " You prepared this section!"); 
}


function optsectionselect() {
    var pkid = document.getElementById("hfAdviseID").value;
    var scid = document.getElementById("optsection").value;    
    var tbl = document.getElementById("advisedsubj");         
    var rw = tbl.getElementsByTagName("tr");         
    var rows = $('#advisedsubj tr').length;
    
	if(scid==''){return false;}
	antidepressant();
	
	$.ajax({
        type: "POST",
        url: base_url+'advising/preferredsection',
        data: { advid: pkid, idx: scid },
        success: function (result){
            result = result.trim();
			rem_antidepr();
			if(result=='')
			{
			 alertmsg('danger',"Sorry! No schedule is available in this section.");
			 $("#optsection").val('');
             return false;			 
			}
            
			var tsr = result.split(';');
            for (var i = 0; i < tsr.length; i++){
                var col = tsr[i].split('|');
                for (var j = 1; j <= rows; j++) {
                    //alert(rw.item(j).id + "|" + col[3].trim());
                    if ($('#s' + col[3].trim()).length>0) 
					{
					 $('#s' + col[3].trim()).find('td:nth-child(6)').html(col[1].trim());
					 $('#s' + col[3].trim()).find('td:nth-child(7)').html(col[2].trim());
					 $('#s' + col[3].trim()).find('td:nth-child(2)').attr('id',col[0].trim());
						
					 saveschedule( pkid ,'s'+col[3].trim(),col[0].trim());                       
                     break;
                    }
                }
            }
            
			registersubjects();
			alertmsg('success',"Success! Preffered Selected Section Completed.");
			$('#mwindow').modal('hide');
            $("#btnremove").removeClass('disabled'); 
            $("#btnregister").removeClass('disabled');  
        },
        error: function(result){
		    rem_antidepr();
			alertmsg('danger',"Failed To Retrieve Needed Data!");
        }
    });
}

function registersubjects()
{
 antidepressant();
 $.ajax({
        type: "POST",
        url: base_url+'advising/txn/set/registersubj',
		async:false,
        dataType: "JSON",
		success: function (data){
		 if(data.result)
		  alertmsg('save',"Success! Saved Selected Section(s).");		
	     else
		  alertmsg('danger',"Failed To Save Selected Section(s).");		 
		
		 setTimeout(function(){rem_antidepr(); },2000);
		},
		error: function(result){
		 rem_antidepr();
		 alertmsg('danger',"Failed To Retrieve Needed Data!");
        }
 });
}

function getmySchedules(){
    var tbl = "advisedsubj";
    var table = document.getElementById(tbl),
        rows = table.getElementsByTagName('tr'),
        cells = table.getElementsByTagName('td');
        
    var subjectload = '';
    var cols = 7;
    var icompute = 0;
   
    for (var i = 1, len = rows.length; i < len; i++) {
        if (i!=rowsel){         
            //var schedid = rows.item(i).cells[cols].innerHTML.trim();
            var schedid = rows.item(i).cells[1].id;
             if (schedid!='' && schedid!='totalsbj'){ subjectload = subjectload  + schedid + '|'  }                     
         }                                                      
    }
    subjectload = subjectload.substring(0, subjectload.length-1);
	if (subjectload=='')
	{subjectload=='0|0';}
	
    return subjectload
}

function saveschedule(advid, subid, csid, notify ){
    $.ajax({
        type: "POST",
        url: $("#advurl").attr('href') + '/enrollment',
        data: { adviseid: advid, subjectid: subid, scheduleid: csid },
		async: false,
        success: function (result) {
            if (notify) { 
                $.smallBox({
                    title: "Success! Section selected completed.",
                    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                    color: "#739E73",
                    iconSmall: "fa fa-check bounce animated",
                    timeout: 5000
                });
            }
			summarizetable();
        },
        error: function (result) {
            alert(result.status + ': ' + result.statusText);
        }
    });

}

function removeschedule(advid, subid) {
   
   $.ajax({
            type: "POST",
            url: $("#advurl").attr('href') + '/removeschedule' ,
            data: { adviseid: advid,  subjectid : subid  },
            success: function (result) {               
                       $.smallBox({
                        title: "Success! Section have been removed.",
                        content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                        color: "#C46A69",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 1000                  
                      });
					  var tbl = "advisedsubj";
                      var table = document.getElementById(tbl),
                          rows = table.getElementsByTagName('tr');
					  rows.item(rowsel).cells[1].id=0;					  
					  summarizetable();
                    },
            error: function (result) {
                        alert(result.status + ': ' + result.statusText);
                    }
        });

}

function cancelrequest(csid){
         
         var mytbl = document.getElementById("oschedule");
         var rw = mytbl.getElementsByTagName("tr");
         
         $.ajax({
            type: "POST",
            url: $("#advurl").attr('href') + '/cancelrequest' ,
            data: { idx: csid  },
            success: function (result) {               
                       $.smallBox({
                        title: "Success! Request was successfully cancelled.",
                        content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                        color: "#C46A69",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 1000                  
                      });
                           rw.item(selsched).cells[2].id ='0';                                       
                           rw.item(selsched).cells[2].innerHTML = rw.item(selsched).cells[2].innerHTML.replace(waiting,"");                                       
                           $("#btnrequest").text('Send Request');
                           totalrequest = totalrequest - 1
                           $("#btnrequest").attr("data-original-title","<i class='fa fa-check text-success'></i> You have "+ totalrequest  +" request schedule(s)");
                           

                    },
            error: function (result) {
                        //alert(result.status + ': ' + result.statusText);
                        return false;
                    }
        });
         
}

function summarizetable(x)
{
 if (x===undefined){x=0;}
 
 var tbl = document.getElementById("advisedsubj");         
 var rw = tbl.getElementsByTagName("tr");         
 var rows = $('#advisedsubj tr').length;
 var totalsbj = 0;
 var totallec = 0;
 var totallab = 0;
 var totalesbj = 0;
 var totaleunit = 0;
 
 if($('#advisedsubj .danger').length > 0)
 {
  var tsubjects	= '';
  var csv_subjs = '';
  $('#advisedsubj .danger').each(function(){
	var subjid = $(this).attr('id');
	var tcode = $(this).find('td:nth-child(2)').html();
	var tdesc = $(this).find('td:nth-child(3)').html();  
	
	if((tcode!=='' && tcode!==undefined) && (tdesc!=='' && tdesc!==undefined))
	{
     tsubjects = tsubjects+' -> '+tcode+' - '+tdesc+'<br/>';
	 csv_subjs = csv_subjs+((csv_subjs=='')?'':',')+subjid.replace('s','');
    }
  });
  
  var ptitle   = '<i class="fa fa-warning txt-color-yellow"></i>WARNING! Advised Subject(s) will be <strong>Remove!</strong>';
  var pcontent = 'We re-evaluate your academic details and the following subject(s) will be remove because the prerequisite(s) is/are not attained.'+
                 '<br/>'+tsubjects+
				 '<br/><br/>Possible Reason(s):'+
				 '<br/>-> No Grade(s) or Unposted Grade(s) of the Pre-Requisite Subjects'+
				 '<br/>-> Failure of the Pre-Requisite Subjects';
  var pcallback = function(ButtonPress,Value){if(ButtonPress=='OK'){ console.log('remove subjects'); $('#advisedsubj .danger').remove(); summarizetable(1);} else{return false;}};
  
  $('#hfToDelete').val(csv_subjs);  
  promptmsg(pcontent,ptitle,'','','','[OK]',pcallback);
  return false;  
 }	 
  
 var xcount=1; 
 $("#totalsbj").html(0);
 $("#totalesbj").html(0);
 $("#totallec").html('0.0');
 $("#totallab").html('0.0');
 $("#totaleunit").html('0.0');   
 $('#advisedsubj tbody tr:not(.hidden)').each(function(){
   var maxload = $('#maxload').html();
   var section = $(this).find('td:nth-child(6)').html();
   var schedule = $(this).find('td:nth-child(7)').html();
   var lec = $(this).find('td:nth-child(4)').html();
   var lab = $(this).find('td:nth-child(5)').html();
   totalsbj = totalsbj + 1;
   totallec = totallec + parseFloat(lec);
   totallab = totallab + parseFloat(lab);
   
   $(this).find('td:nth-child(1)').html(xcount+'.');
   $("#totalsbj").html(totalsbj);
   $("#totallec").html(totallec.toFixed(1));
   $("#totallab").html(totallab.toFixed(1));
   if(section!=''||schedule!='')
   {
    totalesbj = totalesbj + 1;
    totaleunit = totaleunit +(parseFloat(lec)+ parseFloat(lab));
   }
   
   if(totaleunit <= parseFloat(maxload) && $('#advisedsubj').attr('opt-retention')=='1')
    $('.btnsubjdelete').addClass('hidden');
   else if(totaleunit > parseFloat(maxload) && $('#advisedsubj').attr('opt-retention')=='1')
	$('.btnsubjdelete').removeClass('hidden');
  
   $("#totalesbj").html(totalesbj);
   $("#totaleunit").html(Number(totaleunit).toFixed(1));    
   xcount++;
  });
  
  if(totalsbj==0 && x==1)
  {
   var ptitle   = '<i class="fa fa-warning txt-color-yellow"></i>WARNING! No Subject found in the list.';
   var pcontent = 'Kindly approach the registrar office. It seems all the subject advised to you doesnt attained its prerequisites.';
   			  
   promptmsg(pcontent,ptitle);
   $('#btnregister').attr('disabled','true').addClass('hidden');
  }
}

function deladvsbj()
{
 var advid = document.getElementById("hfAdviseID").value;
 //var selcouseid
 $.ajax({
		type: "POST",
		url: $("#advurl").attr('href') + '/removesubject' ,
		data: { adviseid: advid,  subjectid : selcouseid  },
		success: function (result) {               
           var strout = result;
		   //alert(strout);
		   if(result==1 || result=='1')
           {		   
			$.smallBox({
				 	    title: "Success! Subject have been removed.",
				 	    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
				    	color: "#C46A69",
				    	iconSmall: "fa fa-check bounce animated",
					    timeout: 5000                  
				      });
				  
                  document.getElementById("advisedsubj").deleteRow(rowsel);
                  document.getElementById("btndelete").disabled=true;
                  summarizetable();
			}
       		else if(result==0 || result=='0')
            {
			 $.smallBox({
				 	    title: "Failed to remove subject advised!",
				 	    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
				    	color: "#C46A69",
				    	iconSmall: "fa fa-times shake animated",
					    timeout: 5000                  
				      });
			 summarizetable();			
			}
		 },
		error: function (result) {alert(result.status + ': ' + result.statusText);}
	});
summarizetable();				
}

function xfullscreen(widgetid,xbtn)
{
 $(xbtn).attr("data-original-title","Restore");
 $(xbtn).attr("onclick","xrestore("+"'"+widgetid+"'"+",this);");
 $(xbtn).html('<i class="glyphicon glyphicon-resize-small"></i>');
 $('#'+widgetid).attr("id","jarviswidget-fullscreen-mode");
 $('#jarviswidget-fullscreen-mode').attr('style','display:block;height:100%;background-color:white;');
 prev_elem_style = $('#jarviswidget-fullscreen-mode .jarviswidget-body').attr('style');
 $('#jarviswidget-fullscreen-mode .jarviswidget-body').attr('style','height:90%;overflow:auto;');
}

function xrestore(widgetid,xbtn)
{
 $(xbtn).attr("data-original-title","Fullscreen");
 $(xbtn).attr("onclick","xfullscreen("+"'"+widgetid+"'"+",this);");
 $(xbtn).html('<i class="glyphicon glyphicon-resize-full"></i>');
 $('#jarviswidget-fullscreen-mode .jarviswidget-body').attr('style',prev_elem_style);
 prev_elem_style="";
 $('#jarviswidget-fullscreen-mode').attr("id",widgetid);
}


function initiatedarkmatter()
{
 var xid = $('#xpage').html();
 //alert(xid);
 if(xid==1)
 {
	 
 }
 else if(xid==2)
 {
  antidepressant();
  loadinfo();
  setTimeout(function(){loadadvised();},1500);	 
 }	 
  
 else if(xid=3)
  summarizetable();
 
 if($('#studentfilter').length<=0) alarmarcadia();
 
}

function alarmarcadia(xid)
{
 if(xid===undefined){xid='';}
 var xtitle="";
 var xcontent="";
 var xicon="";
 var xcolor="";
 var xtimeout="50000";
 var enablealarm = 1;

 var inactive = $('#xwarningid').attr('data-inactive');
 var account = $('#xwarningid').attr('data-account');
 var balance = $('#xwarningid').attr('data-balance');
 var regonly = $('#xwarningid').attr('data-regonly');
 var isregular = $('#xstats').attr('data-reg');  
 var schedadv = $('#xwarningid').attr('data-schedadv');
 var schedenr = $('#xwarningid').attr('data-schedenr');
 var current_balance = $('#xbalance').html();
 var retention = (($('#xretention').length>0)?($('#xretention').attr('data-retention')):1); 
 
 if(xid=='')
 {  
  if(inactive=="1"){bigalertmsg('danger',"Your student profile is <strong>Inactive</strong>. Please go to registrar's office to activate.",'<i class="fa fa-warning"></i> Alert!');}
  if(schedadv=="1" && schedenr=="0")
  {bigalertmsg('warning',"Advising Period Is <strong>Closed</strong> at this moment.",'<i class="fa fa-warning"></i> Warning!');}	  
  else if(schedadv=="1" && schedenr=="1")
  {bigalertmsg('warning',"Enrollment Period Is <strong>Closed</strong> at this moment.",'<i class="fa fa-warning"></i> Warning!');}	  
  
  if(account=="1"){bigalertmsg('danger',"Please report to the Dean's Office and settle your accountabilities in order to proceed enrollment",'<i class="fa fa-warning"></i> Alert!');}	  	  
  if(balance=="0" && parseFloat(current_balance)>0 && parseFloat(current_balance)!==undefined && parseFloat(current_balance)!==NaN)
	bigalertmsg('warning','Please settle your balance as soon as possible','<i class="fa fa-warning"></i> Alert!');
  else if(balance=="1")
	bigalertmsg('danger',"Please settle your balance as soon as possible",'<i class="fa fa-warning"></i> Alert!'); 
  
  if(regonly=="1" && isregular=="0")
  {
   rem_antidepr();
   promptmsg("Your current academic status doesn't fit to the criteria given by the registrar to proceed online enrollment."+
             "<br>Possible Reasons:<small>"+
             "<br>- Advised Subject(s) doesn't fit a block section."+
             "<br>- Unposted Grades from Previous Semester."+
             "<br>- Not Updated Evaluation(Re-Evaluate your account)."+
             "<br>- No Available or Offered Subject(s).</small>",
			 "<i class='fa fa-warning fa-2x' style='color:red;'></i> Warning");
  }
  else if($('#xretention').length>0)
  {
   if($('#xretention').attr('data-retention') == 0 || $('#xretention').attr('data-retention') == '')
   {
	promptmsg("No Summary of Grades Yet!"+
             "<br>Possible Reasons:<small>"+
             "<br>- No Grade Or Unposted(Coordinate with your instructors/professors).</b>"+
             "<br>- No Grade Summary(Ask the Registrar Office to Re-Calculate your grade summary).</b>"+
             "<br>- Not Updated Evaluation(Ask the Registrar Office to Re-Evaluate your academic details).</b>",
			 "</small><i class='fa fa-warning fa-2x' style='color:yellow;'></i> Warning");
   }
   else if($('#xretention').attr('data-retention') > 2)
   {
	var retent_stats = $('#xretention').html();
	promptmsg("Your retention status is <strong>"+((retent_stats!='')?retent_stats:'not fit to proceed online enrollment')+"</strong>."+
              "<br>"+((xalert_retention!=undefined)?xalert_retention:("<small>Kindly approach the Registrar Office about this matter.</small>")),
			  "<i class='fa fa-warning fa-2x' style='color:yellow;'></i> Warning");
   }	  
  }	  
 }
 else
 {  
  if(xid==0.3){bigalertmsg('danger',"No max load unit. Please go to the registrar to fix this.",'<i class="fa fa-warning"></i> Warning!');}	  	  
  if(xid==0.4){bigalertmsg('warning',"No Subjects is Offered.",'<i class="fa fa-warning"></i> Warning!');}
 }   
}

function xdisplayalert(xtitle,xcontent,xicon,xcolor,xtimeout,xenable)
{
 if(xenable==1)
 {
 $.bigBox({
            title : xtitle,
            content : xcontent,
            icon : xicon,
			color: xcolor,
			colors: xcolor,
			sound: true
		  }); 
 } 
}

function addsbj()
{
 var curri = document.getElementById('hfCurrID').value;
 var term = document.getElementById('hfTermID').value;
 //alert('initialize');
 $.ajax({
		 type: "POST",         
		 url: $("#advurl").attr('href') + '/SIC_SNIC' ,
		 data: { termid : term , curriculum:curri },
		 dataType: "text",
		 success: function (result) 
		        {
				    var sic = document.getElementById('osubjIC');
					var snic = document.getElementById('osubjNIC');
                    var strdata = result;
					//alert('result:'+strdata);
					if (strdata!= 'nodata-[o.o]-nodata' && strdata!='nodata' && strdata!='') 
					{
					var rs = strdata.split("-[o.o]-");
					if(rs[0]!='nodata')
					{
					 sic.innerHTML=rs[0];
					 $("#subjtab a:first").tab("show");
					}
					else
					{
					 sic.innerHTML="<tr><td colspan=5><center>No Subject available.</center></td></tr>";
					 if(rs[1]!='nodata')
					 {
					  $("#subjtab a:last").tab("show");
					 }
					}
					
					if(rs[1]!='nodata')
					{snic.innerHTML=rs[1];}
					else
					{snic.innerHTML="<tr><td colspan=5><center>No Subject available.</center></td></tr>";}
					
					
					$('#addsubjmodal').modal('show');
					}
					else
					{
					 $.SmartMessageBox({
		                 title : "<i class='fa fa-warning fa-2x' style='color:red;'></i> No Other Offered Subjects",
		                 content : "All offered subjects are already in the list.",
		                 buttons : "[Ok]"
						 },function(ButtonPress, Value){
						   if (ButtonPress == "Ok") {return false;}
					    });
					}
				},
		   error: function (err) 
		   {alert(err.status + ': ' + err.statusText);}
	   });
}

function minloadwarning()
{
 antidepressant();
 var minload = document.getElementById("minload").innerHTML;   
 var maxload = document.getElementById("maxload").innerHTML;   
 var totalload =document.getElementById("totaleunit").innerHTML;
 minload = ((parseFloat(minload)!==undefined && parseFloat(minload)!=='NaN')?parseFloat(minload):(-1));
 maxload = ((parseFloat(maxload)!==undefined && parseFloat(maxload)!=='NaN')?parseFloat(maxload):(-1));
 totalload = ((parseFloat(totalload)!==undefined && parseFloat(totalload)!=='NaN')?parseFloat(totalload):0);
 
 if(minload=='-1')
 {
  rem_antidepr();
  promptmsg(((xalert_nominload!=undefined)?xalert_nominload:("If you want to proceed this transaction, you have to coordinate with the registrar office in order to adjust your minimum load or proceed to manual enrollment.")),
             "<i class='fa fa-warning fa-2x' style='color:red;'></i> Undefined Minimum Load!");
  return false;
 }
 
 if(maxload=='-1')
 {
   rem_antidepr();
   promptmsg(((xalert_nomaxload!=undefined)?xalert_nomaxload:("If you want to proceed this transaction, you have to coordinate with the registrar office in order to adjust your maximum load or proceed to manual enrollment.")),
             "<i class='fa fa-warning fa-2x' style='color:red;'></i> Undefined Maximum Load!");
  return false;
 } 
 
 if (totalload == 0)
 {
   rem_antidepr();
   promptmsg("You cant register if all subjects have no schedule selected.",
             "<i class='fa fa-exclamation-circle fa-2x' style='color:red;'></i> No Schedule Selected");
  return false;
 }
 if (totalload < minload && $('#hfToDelete').val()=='')
 {
  rem_antidepr();
  promptmsg(((xalert_highminload!=undefined)?xalert_highminload:("If you want to proceed this transaction, you have to coordinate with the registrar office in order to adjust your minimum load or proceed to manual enrollment.")),
             "<i class='fa fa-warning fa-2x' style='color:yellow;'></i> Your Total selected units is lower than Minimum Load Units");
  return false;
 }
 if (totalload > maxload)
 {
   rem_antidepr();
   promptmsg("Due to your retention status, You need to remove subject(s) to fit your maximum load units.",
             "<i class='fa fa-warning fa-2x' style='color:yellow;'></i> Your Total selected units is greater than Maximum Load Units");
  
  $('.btnsubjdelete').removeClass('hidden');
  $('.instruc_reten').removeClass('hidden');
  $('.instruc_overall').addClass('hidden');
  $('#advisedsubj').attr('opt-retention',1);
  return false;
 }	 
}

function checkadvisedsubj()
{
 var xlength=$('#advisedform input[type="hidden"][id*="sbjid"]').length;
 var xongroup=0;
 var xgroupno=0;
 
 $('#xonesubjtbl tbody').html('');
 $('#xonesubjprocess').attr('onclick','');
 
 $('#advisedform input[type="hidden"][id*="sbjid"]').each(
  function()
  {
   var inp = $(this);
   var xid=inp.attr('id');
   var xgrp=inp.attr('data-group');
   var xcode=inp.attr('data-code');
   var xtitle=inp.attr('data-title');
   var xval=inp.val();
   
   if(xgrp!=0 && xgroupno==0)
   {
    xmember = $('#advisedform input[type="hidden"][id*="sbjid"][data-group="'+xgrp+'"]').length;
    if(xmember>1)
	{
	 xgroupno=xgrp;
	 xongroup=xmember;
	}
   }
   
   if(xgrp!=0 && xgroupno==xgrp)
     $('#xonesubjtbl tbody').append('<tr><td><input type="radio" name="onesubj" value="'+xid+'"></td><td>'+xcode+'</td><td>'+xtitle+'</td></tr>');
   
  
   xlength--;
   if(xlength==0 && xongroup>0)
   {
    rem_antidepr();
    $('#xonesubjprocess').attr('onclick','rem_nonselected()');
	$('#OneSubjOnly').modal({
                             backdrop: 'static',
                             keyboard: false  // to prevent closing with Esc button (if you want this too)
                            });
    //$('#OneSubjOnly').modal('toggle');
    return false;
   }
   else if(xlength==0 && xongroup==0)
   {
    $('#advisedform').attr('onsubmit','');
	$('#advisedform').submit();
   }	
  }
 );
 
 return false;
}

function rem_nonselected()
{
 var xlength=$('#xonesubjtbl input[type="radio"]').length;
 var xtoberem=$('#xonesubjtbl input[type="radio"]:not(:checked)').length;
 
 if(xlength > xtoberem)
 {
  $('#xonesubjtbl input[type="radio"]:not(:checked)').each(
   function()
   {
    var xopt = $(this);
    var xval = xopt.val();	
    
    $('#'+xval).attr('data-group','0');
    $('#'+xval).removeAttr('name');
    $('#'+xval).removeAttr('value');
	
	xtoberem--;
	
	if(xtoberem==0)
	 {
	  $('#OneSubjOnly').modal('hide');
	  antidepressant();
	  checkadvisedsubj();
	 }  
   }
  );
 }
 else
 {
  alert('Please select one of the subjects');
 } 

}

//$(function () {
$('document').ready(function() {
   
   pageSetUp();
   
   //window.onload = loadadvised;
   window.onload = initiatedarkmatter;
   
   //minloadwarning();
   
   //chat
   /*
   $("body").append("<div>"+
                    "<div style='background-color:#004D60;"+
                    "border-left-color:rgba(0, 0, 0, 0.14902);"+
			        "border-left-style:solid;"+
			        "border-left-width:5px;"+
			        "bottom:10px;"+
			        "box-sizing:content-box;"+
			        "color:#FFFFFF;"+
			        "height:250px;"+
			        "overflow:hidden;"+
			        "padding:5px 5px 5px;"+
			        "position:fixed;"+
			        "right:10px;"+
			        "width:320px;"+
			        "z-index:99999;'>"+
                    "<p>Administrator</p><div style='background-color:white;height:200px;color:blue;'><p><b>Administrator:</b> Hi!</p></div><input type='text'/><input type='button'/>"+
                    "</div></div>");   
	*/
	
   $("#advisedsubj").mousedown(function(ev){
      if(ev.which == 3) {
         
      }      
   });
   
   $("#btnrequest").click(function(k){
         var mytbl = document.getElementById("oschedule");
         var rw = mytbl.getElementsByTagName("tr");
         
      if (selschedid != 0){
      
         var cmmd  = $("#btnrequest").text() ; 
         
         if (cmmd == "Cancel Request") {
                  cancelrequest(selschedid)                   
                  exit;         
         }
         
      
         $.SmartMessageBox({
                 title : "Request ",
                 content : "Please enter your message",
                 buttons : "[Cancel][Send]",                 
                 input : "text",
                 placeholder : "Enter your message (Optional)"
                 }, function(ButtonPress, Value) {
                     //alert("btn : " + ButtonPress + " Msg: " + Value);
                     if (ButtonPress == "Cancel") {
                        return 0;
                     }
                     
                       $.ajax({
                           type: "POST",         
                           url: base_url + 'advising/request' ,
                           data: { schedid : selschedid , message:Value },
                           success: function (e) {
                                                          
                                    if (e != 0 ) {
                                       $.smallBox({
                                       title: "Success! Your request has been sent.",
                                       content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                                       color: "#739E73",
                                       iconSmall: "fa fa-check bounce animated",
                                       timeout: 2000                  
                                     });
                                       rw.item(selsched).cells[2].id ='3';                                       
                                       rw.item(selsched).cells[2].innerHTML += waiting;
                                       $("#btnrequest").text('Cancel Request');                                       
                                       totalrequest=totalrequest+1
                                       $("#btnrequest").attr("data-original-title","<i class='fa fa-check text-success'></i> You have "+ totalrequest  +" request schedule(s)");
                                    }
                                   },
                           error: function (result) {
                                       alert(result.status + ': ' + result.statusText);
                                   }
                       });
                                    
             });
   
      }
      k.preventDefault();
   });
   
   $("#btnpreg").click(function(){              
       var isexist = $('#feesinfo').length;
       var count = $('#feesinfo tbody').find('tr').length;
       var url = base_url+'advising/printout';
       if(isexist>0 && count<=1){
        alert("No assessment to print!");
        return false;
       }else 
	window.open(url, "Pre-Registration", "location=0,scrollbars=yes,status=no,toolbar=no,menubar=no,resizable=no,width=850,height=525");        
   });
  
   $("#prntcor").click(function(){         
         window.open(base_url+'advising/printcor', "Certification of Registration", "location=0,scrollbars=yes,status=no,toolbar=no,menubar=no,resizable=no,width=850,height=525");         
   });  
	
   $("#xadvise").click(function(){         
     antidepressant();
     window.location = base_url+'advising/adviser';   
   });

   $("#xreg").click(function(){         
     antidepressant();
     window.location = base_url+'advising/schedules';         
   });	  
   
   $("#btnprepared").click(function (){                  
      $("#btnprepared").toggleClass("btn-success");         
   });
   
   $("#xproceedreg").click(function(){         
	 antidepressant();
   });
	
   $("#xvwassessment").click(function(){antidepressant();});	
   
   $("#btnsubmit").click(function (){
	  var tdata   = $('#advisedsubj').find('tbody');
	  var trrow   = $(tdata).find('.bg-color-blueLight');
	  var schedid = $(trrow).attr('id');
	  var xunits  = $(trrow).data('units');
	  var selunit = $('#totaleunit').html();
	  var maxunit = $('#maxload').html();
	  var tmpunit = parseFloat(xunits) + parseFloat(selunit);
	  if(parseFloat(maxunit)<tmpunit){
		  $('#mwindow').find('[data-dismiss]').trigger('click');
		  alertmsg('danger','Selected Unit(s) will exceed the allowed maximum units!');
		  return false;
	  }
	  
      if ($("#btnprepared").hasClass("btn-success")) {
        preparedschedule();
      }else{
        setschedule();
      }  
   });
   
   if (document.getElementById("optsection")) 
   {
	var tbl = document.getElementById("advisedsubj");         
    var rw = tbl.getElementsByTagName("tr");         
    var rows = $('#advisedsubj tr').length;
	var xsection='';
	var xsectionid='';
	
	for (var j = 1; j < rows-1; j++) 
	{
	  var tmpsec = rw.item(j).cells[5].innerHTML.trim();
	  var tmpsecid = rw.item(j).cells[5].id;
	  //alert(tmpsecid);
	  if((xsection=='' && tmpsec!='') || (xsection==tmpsec && tmpsec!=''))
      {xsection = tmpsec;
	   xsectionid = tmpsecid;}
	  else
	  {xsection ='undefined';
	   xsectionid ='undefined';}
    }
	//alert(xsection);
	if(xsection!='' && xsection!='undefined')
	{
	 if($('#optsection option[data-value="'+xsection+'"]').length>0)
	  $('#optsection option[data-value="'+xsection+'"]').attr('selected','selected');
	 else
      $('#optsection').append('<option data-value="'+xsection+'" value="'+xsectionid+'" selected="selected">'+xsection+'</option>'); 
	}
   }
   
   $('body').on('click','.btnreadvise',function(){
	 antidepressant('Re-advising.. This may take a lot of time.. Please Wait..');
     loadinfo(function(){loadadvised();});
   });
   
   $('body').on('submit','#advisedform',function(e){
	  e.preventDefault();
	  $('#xsave').attr('disabled','true');
	  $('#xproceedreg').attr('disabled','true');
	  return false;
   });	 
   
   $('body').on('submit','#register',function(e){
	 e.preventDefault();
	 $('#btnregister').attr('disabled','true');
	 return false;
   });
   
   $('body').on('click','#xsave,#xproceedreg',function(){
	  try
	  {
	   $.ajax({
        type: "POST",         
        url: base_url+'advising/txn/set/advise/',
	    data: $('#advisedform').serialize(), 
        dataType: 'JSON',
	    success: function(data){
	     if(data.result){
		  alertmsg('save','Success!');
          window.location = base_url+data.content.trim();   
		 }
         else{
		  alertmsg('danger',data.error);	
		  rem_antidepr();		
	      $('#xsave').removeAttr('disabled');
	      $('#xproceedreg').removeAttr('disabled');
		 }
	    },
	    error: function(err){
		 alertmsg('danger','Unable to Save Advised Subjects! Please Try Again');
         rem_antidepr();   
	     $('#xsave').removeAttr('disabled');
	     $('#xproceedreg').removeAttr('disabled');
	    }
	   });
      }
	  catch(e)
	  {
	   alertmsg('danger','Unable to Save Advised Subjects! Please Try Again');
       rem_antidepr();    
	   $('#xsave').removeAttr('disabled');
	   $('#xproceedreg').removeAttr('disabled');
	  }
	  
	  return false;	  
   });
   
   $('body').on('click','.btnsubjdelete',function(){
	  var trow = $(this).closest('tr');
	  var tid = $(trow).attr('id');
	  var todelete = $('#hfToDelete').val();
	      todelete = todelete+((todelete=='')?'':',')+tid.replace('s','');
		  
      $(trow).addClass('hidden');
	  $('#hfToDelete').val(todelete);
	  $('#acad_stats').html('Irregular');
	  summarizetable();	  
   });
   
   $('body').on('click','#btnregister',function(){
	  if(minloadwarning()==false){$('#btnregister').removeAttr('disabled'); return false;}
	  try
	  {
	   $.ajax({
        type: "POST",         
        url: base_url+'advising/txn/set/register/',
	    data: $('#register').serialize(), 
        dataType: 'JSON',
	    success: function(data){
	     if(data.result){
		  alertmsg('save','Success!');
		  if(data.error!==undefined)
		  {
		   rem_antidepr();	  
		   promptmsg(data.error,'<i class="fa fa-warning fa-2x" style="color:yellow;"></i> Warning!','','','','[Ok]',function(ButtonPress,Value){
			 if(ButtonPress=='Ok')
			 {
			  antidepressant();
			  window.location=base_url+data.content.trim();
			 }
             return false;			 
		   });
		  }
		  else
		   window.location=base_url+data.content.trim();
		 }
         else{
		  alertmsg('danger',data.error);	
		  rem_antidepr();		
	      $('#btnregister').removeAttr('disabled');
		 }
	    },
	    error: function(err){
		 alertmsg('danger','Unable to Register Advised Subjects! Please Try Again');
         rem_antidepr();   
	     $('#btnregister').removeAttr('disabled');
	    }
	   });
      }
	  catch(e)
	  {
	   alertmsg('danger','Unable to Register Advised Subjects! Please Try Again');
       rem_antidepr();    
	   $('#btnregister').removeAttr('disabled');
	  }
	  
	  return false;	  
   });
   
   //========================================================================================================
   //======================= Select Student for Administrator================================================
   //========================================================================================================
   $('body').on('click','#btnselectitem',function(){
	 var studno = $('.select-list .info').attr('id');
	 var studname = $('.select-list .info').find('.studname').html();
	 
	 $('#studentfilter').attr('data-id',studno);
	 $('#studentfilter').val(studname);
	 $('#studentno').val(studno);
	 $('#studentno').change();
	 $('#selectmodal').modal('hide');
   });
   
   $('#studentno').change(function(){
   if($(this).val()==''){return false;}  
	 var stdno = $(this).val();
     antidepressant('Loading Info.. Please Wait..');
	 try
	 {
	  $.ajax({
       type: "POST",         
       url: base_url+'advising/txn/get/studentinfo/'+stdno,
       dataType: 'JSON',
	   success: function(data){
		 if(data.result)
		 {
		  $('#xayterm').html(data.content.advising_data['activeterm']);
		  $('#xcampus').html(data.content.advising_data['campus']);	 
		  $('#xadvising').html(data.content.advising_data['advperiod']);
		  $('#xenrollment').html(data.content.advising_data['enperiod']);
		  
		  $('#xstudno').html(data.content.advising_data['studentno']);	 
		  $('#xregid').html(data.content.advising_data['regid']);
		  $('#xregdate').html(data.content.advising_data['regdate']);
		  $('#xcollege').html(data.content.advising_data['college']);	 
		  $('#advisingcontent #xcollege').html(data.content.advising_data['college']);	 
		  $('#xprogram').html(data.content.advising_data['program']);	 
		  $('#advisingcontent #xprogram').html(data.content.advising_data['program']);	 
		  $('#xcurriculum').html(data.content.advising_data['curriculum']);	 
		  $('#advisingcontent #xcurriculum').html(data.content.advising_data['curriculum']);	 	 
		  $('#xyrlvl').html(data.content.advising_data['yrlvl']);	 	 
		  $('#advisingcontent #xyrlvl').html(data.content.advising_data['yrlvl']);	 	 	 	 
		  
		  $('#xretention').closest('tr').addClass('hidden');
          $('#xretention').attr('data-retention','1');
          $('#xretention').html('');			 
		  if(data.content.advising_data['retentionid']!==undefined)
		  {
		   if(data.content.advising_data['retentionid']!='1')
           {
		    $('#xretention').closest('tr').removeClass('hidden');
            $('#xretention').attr('data-retention',data.content.advising_data['retentionid']);
            $('#xretention').html(data.content.advising_data['retention']);			 
		   }				
		  }	
		  
		  $('#xaccount').html(data.content.advising_data['accounts']);	 
		  $('#xfeestemp').html(data.content.advising_data['tempcode']);	 
		  $('#xbalance').html(data.content.advising_data['balance']);	 
		  $('#xstats').html(data.content.advising_data['status']+' '+data.content.advising_data['btnstatus']);	 
		  $('#xstats').attr('data-reg',data.content.advising_data['isreg']);
		  
		  $('#xwarningid').attr('data-schedadv',data.content.xalert['schedadv']);
		  $('#xwarningid').attr('data-schedenr',data.content.xalert['schedenr']);
		  $('#xwarningid').attr('data-regonly',data.content.xalert['regonly']);
		  $('#xwarningid').attr('data-balance',data.content.xalert['balance']);
		  $('#xwarningid').attr('data-account',data.content.xalert['account']);
		  $('#xwarningid').attr('data-inactive',data.content.xalert['inactive']);
		  
		  $('.btn-advctrl').find('button').removeAttr('class');
		  $('.btn-advctrl').find('button').addClass('btn btn-default xmargin-left-2 disabled');
		  $('.btn-advctrl').find('button').attr('disabled',true);
		  
		  if(data.content.megalock==1)
		  {
			if(data.content.advising_data['xadv']==1)
			{
		     $('#xadvise').removeClass('disabled');
		     $('#xadvise').removeAttr('disabled');
		     $('#xadvise').removeClass('btn-default');
		     $('#xadvise').addClass('btn-primary');
		    }
			
			if(data.content.advising_data['xenr']==1)	
			{
		     $('#xreg').removeClass('disabled');
		     $('#xreg').removeAttr('disabled');
		     $('#xreg').removeClass('btn-default');
		     $('#xreg').addClass('btn-success');
		    }
			if(data.content.advising_data['xprt']==1)	
			{
		     $('#btnpreg').removeClass('disabled');
		     $('#btnpreg').removeAttr('disabled');
		     $('#btnprint').removeAttr('disabled');
		    }
			if(data.content.advising_data['xcor']==1)	
			{
		     $('#prntcor').removeClass('disabled');
		     $('#prntcor').removeAttr('disabled');
		     $('#btnprint').removeAttr('disabled');
		    }
		  }
		  else
		  {
		   $('.btn-advctrl').find('button').addClass('hidden');
		   $('.alert-maintenance').removeClass('hidden');
		  }
		  console.log('Alarm');
		  alarmarcadia('');
		  //alertmsg('success','Data is Loaded');
		 }
         else
		 {
		  alertmsg('danger','Failed to load data!');
          $('#studentfilter').val('');
	      $('#studentno').val('');
	     }
	  
	    rem_antidepr(); 
       },
	   error: function(err){alertmsg('danger','Failed to load data!'); rem_antidepr(); }
	  });
     }
	 catch(e)
	 {
	  alertmsg('danger','Failed to load data!');	
	  rem_antidepr();  
	 }
   });
   
   //========================================================================================================
   //========================================================================================================
});

