var termid = -1;
var xlink  = base_url+'advschedctrl/';
$(document).ready(function(){
 termid = $('#term').val();
 get_schedlist();
 $('#timestart').timepicker({defaultTime: '12:00 AM'});
 $('#timeend').timepicker({defaultTime: '11:59 PM'});

 $('body').on('change','#term',function(){
	termid = $(this).val();
	get_schedlist();
 });

 $('body').on('click','.btnedit',function(){
	var trow = $(this).closest('tr');
	clear_form(); 
	display_data(trow);
 });

 $('body').on('click','.btnremove',function(){
	var indx = $(this).closest('tr').attr('id');
	delete_schedule(indx);
 });

 $('body').on('click','#btnsave',function(){
	save_schedule();
 });
 
 $('body').on('click','.btnrefresh.btnnew,#btncancel',function(){
   clear_form(); 
 });
 
 $('body').on('click','.btnrefresh',function(){
  get_schedlist();
 });
 
});

function clear_form()
{ 
 $('#program').val(-1);
 $('#yrlvl').val(-1);
 $('#datestart').val('');
 $('#timestart').val('12:00 AM');
 $('#dateend').val('');
 $('#timeend').val('11:59 PM');
 $('#inactve').prop('checked',false);
}

function display_data(trow)
{
 var progid   = $(trow).attr('data-prog');
 var majorid  = $(trow).attr('data-major');
 var yrlvlid  = $(trow).attr('data-yrlvl');
 var dstart   = $(trow).find('td:nth-child(4)').html();
 var tstart   = '';
 var dend     = $(trow).find('td:nth-child(5)').html();
 var tend     = '';
 var inactive = $(trow).attr('data-inactive');
 
 if(dstart=='')
  tstart = '12:00 AM';	 
 else
 {
  var tmpdatea = dstart.split(" ");
  dstart = tmpdatea[0];
  tstart = tmpdatea[1]+' '+tmpdatea[2];
 }	 
 
 if(dend=='')
  tend = '11:59 PM';
 else
 {
  var tmpdateb = dend.split(" ");
  dend = tmpdateb[0];
  tend = tmpdateb[1]+' '+tmpdateb[2];
 }	 
 
 $('#program').val(progid+':'+majorid);
 $('#yrlvl').val(yrlvlid);
 $('#datestart').val(dstart);
 $('#timestart').val(tstart);
 $('#dateend').val(dend);
 $('#timeend').val(tend);
}

function get_schedlist()
{
  if(termid > 0)
  {
	$.ajax({
	     type:"POST",
	     url:xlink+'txn/get/tblist/'+termid,
	     data:{},
	     dataType: "JSON",
	     async:true,
		 success: function (rs){
		  if(rs.success)
		   $('#tblist tbody').html(rs.content);
          else
		   alert('Failed to load data!');
	     },
		 error:function(err){
		   alert('Error on retrieving data');	 
		 }
		 
    });		 
  }	  
}

function save_schedule()
{
  if(termid>0)
  {
     var progid   = $('#program').find('option:selected').attr('data-prog');
     var majorid  = $('#program').find('option:selected').attr('data-major');
     var yrlvlid  = $('#yrlvl').val();
     var dstart   = $('#datestart').val();	 
     var tstart   = $('#timestart').val();	 
     var dend     = $('#dateend').val();	 
     var tend     = $('#timeend').val();	 
	 var inactive = (($('#inactive').prop('checked'))?1:0);
	 var indx     = $('[data-prog="'+progid+'"][data-major="'+majorid+'"][data-yrlvl="0"]').attr('id');
	 
	 if(indx == undefined || indx=='')
	  indx     = $('[data-prog="'+progid+'"][data-major="'+majorid+'"][data-yrlvl="'+yrlvlid+'"]').attr('id');
	 
	 if(indx == undefined || indx=='')
	  indx     = 'new';
	 	 
	 if(progid!=undefined && progid!='' && progid>0 && dstart!='' && dend!='')
	 {
	  var triala = new Date(dstart+' '+tstart);
	  var trialb = new Date(dend+' '+tend);
       
      if(triala>=trialb)
	  {	  
       alert('Invalid "From" and "To"! Please input a correct date');
       return false;
      }
	  
	  antidepressant();
	  $.ajax({
	     type:"POST",
	     url:xlink+'txn/set/schedule',
	     data:{indx:indx,termid:termid,progid:progid,majorid:majorid,yrlvl:yrlvlid,dstart:dstart+' '+tstart,dend:dend+' '+tend,inactive:inactive},
	     dataType: "JSON",
	     async:true,
		 success: function (rs){
		  if(rs.success)
		   get_schedlist();
          else
		   alert('Failed to process data!');
		  
		  rem_antidepr();
	     },
		 error:function(err){
		   alert('Error on processing data');
		   rem_antidepr();	 
		 }
		 
     });	
	}
  }	  
}

function delete_schedule(indx)
{
  if(termid>0)
  { 
	 if(indx!=undefined && indx!='')
	 {	 
      antidepressant();
	  $.ajax({
	     type:"POST",
	     url:xlink+'txn/delete/schedule',
	     data:{indx:indx},
	     dataType: "JSON",
	     async:true,
		 success: function (rs){
		  if(rs.success)
		   get_schedlist();
          else
		   alert('Failed to process data!');
		
		  rem_antidepr();
	     },
		 error:function(err){
		   alert('Error on processing data');	
		   rem_antidepr(); 
		 }
		 
     });	
	}
  }	  
}