var xalert_nominload = "If you want to proceed this transaction, you have to coordinate with the registrar in order to adjust your minimum load or proceed to manual enrollment.";
var xalert_nomaxload = "If you want to proceed this transaction, you have to coordinate with the registrar in order to adjust your maximum load or proceed to manual enrollment.";
var xalert_highminload = "If you want to proceed this transaction, you have to coordinate with the Vice Dean Office in order to adjust your minimum load or proceed to manual enrollment.";
var xalert_lessmaxload = "If you want to proceed this transaction, you have to coordinate with the Vice Dean Office in order to adjust your maximum load or proceed to manual enrollment.";
var xalert_retention = "<small>Kindly approach the Registrar Office about this matter.</small>";
