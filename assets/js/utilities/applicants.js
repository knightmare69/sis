var page    = 1;
var dmcm    = {
	load: function(opt,data){
		var campus  = $('#campus').val();
		var ayterm  = $('#ayterm').val();
		var apptype = $('#apptype').val();
		var url     = base_url;
	    var xdata   = {'page':page,'filter':data};
		
		if(campus>0 && ayterm>0 && apptype>0){
		  var xparam = {'opt':opt,'campus':campus,'ayterm':ayterm,'apptype':apptype,'param':data};
		  $.ajax({
			  type: "POST",
			  url: base_url+'admission/txn/get/applist/',
			  data: xparam,
			  success: function (data){
                                   $('#tblapplist').find('tbody').html(data); 
			  },
			  error:function(err){
				     alert('Error Has Occured!'); 
			  }
		  });		  
		}
	},
    mapped: function(){
		$('body').on('change','.checkbox',function(){
		   var ishead  = $(this).closest('thead').length;
		   var ischeck = $(this).find('input[type="checkbox"]').is(":checked");
		   var tbl     = $(this).closest('table');
		   if(ishead){
			   $(tbl).find('tbody').find('.checkbox').find('input[type="checkbox"]').prop('checked',ischeck);
		   }else{
			   if(ischeck==false){
			     $(tbl).find('thead').find('.checkbox').find('input[type="checkbox"]').prop('checked',ischeck);
			   }
		   }
		});
		
		$('body').on('click','.btnsearch',function(){
			var param = $('.txtparam').val();
			dmcm.load(0,param);
		});
		
		$('body').on('change','#campus,#ayterm,#apptype',function(){
			var param = $('.txtparam').val();
			dmcm.load(0,param);
		});
		
		$('body').on('keypress','.txtparam',function(e){
			if(e.keyCode==13){
				$('.btnsearch').click();
			}
		});
		
		$('body').on('keypress','.txtparam',function(e){
			if(e.keyCode==13){
				$('.btnsearch').click();
			}
		});
	},	
    init: function() {
			this.mapped();
	}
	
};

$(document).ready(function(){
   dmcm.init();
   $('body').on('click','.btn-print,.btn-permit',function(){
	  var xid = $(this).attr('data-id');
	  var url = base_url+(($(this).is('.btn-print'))?'admission/printapp/':'admission/printout/')+xid;
      window.open(url, '_blank');	  
   });
});