var sel_recipient = [];
var sel_count     = 0;
var limit         = 500;
var prev_Top	  = 0;
var xajax;

function selectall()
{
 $('input[type="checkbox"]').prop("checked",$('#xselect').prop("checked"));
}

function deselectprimo()
{
 if($('#xselect').prop("checked")==true)
 {
  $('#xselect').removeAttr("checked");
 }
}

function sendmail()
{
 var xlink  = base_url+'enotify/txn/set/send/';
 var xsubj  = $("#xsubj").val();
 var xmsg   = $("#xmsg").val();
 var xcount = $('input[type="checkbox"]').length;
 var xcheck = $(':checked').length;
 var xemail = 'jhe69samson@yahoo.com';
 
 if(xcheck>500)
 {
  alert('500 recipient are allowed per message!');
  return false;
 }	 
 
 if(xmsg.trim()=='')
 {
  alert('No Message to Send');
  return false;
 }
 
 if(xsubj.trim()=='')
 {
  alert('No Subject to use.');
  return false;
 }
 
 xmsg = xmsg.replace(/\n/g, "<br/>");
 antidepressant();
 for(i=1;i<=xcount;i++)
 {
  var xid = "#chk"+i;
  var ischecked = $(xid).prop("checked");
  if(ischecked)
  {
   if(xemail=='')
    xemail = $(xid).attr("data-pointer");
   else
    xemail = xemail + ',' + $(xid).attr("data-pointer");
    
  }
 }
 
$.ajax({
		 type: "POST",         
		 url: xlink,
		 data: { email : xemail, subj : xsubj, msg : xmsg },
		 dataType: "JSON",
		 async:true,
		 success: function (rs){
				    if (rs.success) 
					 alertmsg('success','Message has been sent.');
				    else
					  alertmsg('danger',rs.error);
				    rem_antidepr();
				},
          error: function(err){
			  alertmsg('danger','Error While Sending Email');  
		  }				
	    });
}

function putloader(xelem)
{
 if($(xelem).find('.loader').length<=0)
  $(xelem).append('<tr class="loader"><td colspan="6" class="text-align-center"><i class="fa fa-refresh fa-spin"></i> Loading..</td></tr>')	  
 else
  $('.loader').show();	
}

function get_list()
{
 var xend  = $('.last').length;	
 var xfind = $('#xfilter').val();
 var xlast = $('[data-uid]').last().attr('data-uid');
 var xlink = base_url+'enotify/txn/get/list/';
     xlast = ((xlast==undefined)?0:xlast);
	 
 if(xajax==undefined)
 {	 
  putloader($('#xmembers').find('tbody'));	 
  xajax = $.ajax({ 
		 type: "POST",         
		 url: xlink,
		 data: {uid:xlast, find:xfind},
		 dataType: "JSON",
		 async:true,
		 success: function (rs){
					console.clear();
				    $('.loader').replaceWith(rs.content);
				    $('#xfilter').keyup();
                    xajax=undefined;
				},
          error: function(err){
			  $('.loader').replaceWith('');
			  alertmsg('danger','Error While Loading List');  
			  xajax=undefined;
		  }				
	    });
 }		
}

$(document).ready(function(){
  $('body').on('click','.chkdata',function(){
	  deselectprimo(); 
  });
  
  $('body').on('keyup','#xfilter',function(){
	    var xtarget = $(this).attr('data-target');
		if(xtarget==undefined || xtarget=='') return false;
		
		console.log('keyup');
		var $rows = $(xtarget+' tbody tr');
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
     	$rows.show().filter(function(){
			var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			return !~text.indexOf(val);
		}).hide();	
		
		get_list();
	});
	
  $('#xmembers').scroll(function(){
	var curr_Top = $(this).scrollTop();
	if(curr_Top > prev_Top)
	{
     putloader($(this).find('tbody'));
     get_list();
    }
	
	prev_Top = curr_Top;
  });	
  
  $('body').on('click','#xclear',function(){
	  $('#xsubj').val('');
      $('#xmsg').val('');	
      $('#emailbody').code('');	  
  });
  
  $('body').on('click','.btnrefresh',function(){
	  $('#xfilter').val('');
	  $('#xmembers').find('tbody').html('');
	  putloader($('#xmembers').find('tbody'));
      get_list();	  
  });
  
  $('body').on('keyup','.note-editable',function(){
	 $('#xmsg').val($('#emailbody').code()); 
  });
  
  $('#emailbody').summernote({
	 height: '360px',          
     minHeight: '250px',             
     maxHeight: '360px', 
	 focus: false,
	 tabsize: 4
  });
  
  if($('.note-editable').length>0)
  {
   $('.defaulteditor').addClass('hidden');
   $('.summereditor').removeClass('hidden');	  
  } 
  else
  {
   $('.defaulteditor').removeClass('hidden');
   $('.summereditor').addClass('hidden');	  
  } 
  
  get_list();	
});