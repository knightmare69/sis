function evaluation(opt){
 if(opt===undefined){opt=1;}
 antidepressant();
 var xurl = $("#mnuhome").attr('href');
 var xstdno = $('#studentno').val();
 var divida = 'xstdeval';
 var dividb = 'xstdsum';
 var dividc = 'details';
 xurl = xurl.replace('home','evaluation/reevaluate');
 if(typeof xstdno != 'undefined' && xstdno != "undefined" && xstdno != ""){xurl = xurl+'/'+xstdno+'/'+opt;}
 
 try
 {
  $.ajax({
         type: "POST",            
         url: xurl ,
         dataType: "JSON",
         success: function (result){
				     if(result.result)
					 {
					  if(result.content!=''){$('#'+divida).html(result.content);}
					  if(result.summary!=''){$('#'+dividb).html(result.summary);}
					  if(result.xtra!=''){$('#'+dividc).html(result.xtra);}
					 }
                     else
                     {
					  $('#tblloader2').html('<i class="fa fa-warning"></i> Loading Failed');
					  alertmsg('danger','Failed to Load Data!');
				     }
					 rem_antidepr();
					 console.clear();
                 },
         error: function (result){
			         $('#tblloader2').html('<i class="fa fa-warning"></i> Loading Failed');
					 alertmsg('danger','Error While Loading Data!');
					 rem_antidepr();
		        }
       });
 }
 catch(err)
 {
  $('#tblloader2').html('<i class="fa fa-warning"></i> Loading Failed');
  alertmsg('danger','Error While Loading Data!');
  rem_antidepr();
 }
}

function remap_user()
{
 antidepressant();
 var url = $("#mnuhome").attr('href');
 url = url.replace('home','profile');
 window.location = url;
}

function printevalr()
{
 
 var xurl = $("#mnuhome").attr('href');
 var xstdno = $('#studentno').val();
 xurl = xurl.replace('home','evaluation/printdc');
 if(typeof xstdno != 'undefined' && xstdno != "undefined" && xstdno != "")
 {
  xurl = xurl+'/'+xstdno;
 }
 
 window.open(xurl, "Evaluation Result", "location=0,scrollbars=yes,status=no,toolbar=no,menubar=no,resizable=no,width=850,height=525"); 
 console.clear(); 
}

$(window).load(function(){
 var xstat = $('#xstatus').attr('data-status');
 var noeval = $('#xstdeval').find('.alert-danger').length;
 //return 0;
 if((xstat=='' || xstat=='failed') && noeval==0)
 {
  if($('#studentfilter').length > 0){$('#tblloader2').html('<span class="fa fa-warning"/> Select student to view evaluation.'); return false;}
  if($('select #studentno').length > 0){remap_user(); return false;}
	 
  $.SmartMessageBox({
	title   : "<center><i class='fa fa-warning fa-2x' style='color:yellow;'></i>  Academic Evaluation is not available yet</center>",
	content : "<center>If you want to perform evaluation, click Ok.</center>",
	buttons : "[Cancel][OK]"
	}, 
	function(ButtonPress, Value) 
	{
     if (ButtonPress == "OK") 
	  evaluation();
	 else
	  remap_user();
	});
 } 
});

$('document').ready(function(){
  var studno = $('#studentno').val();
  $("#xdefecient").click(function(){printevalr();});  
  
  $("#xreevaluate").click(function(){evaluation(1);}); 
  
  $("#studentno").change(function(){evaluation(0);}); 
  
  $('body').on('click','#btnselectitem',function(){
	var studno = $('.select-list .info').attr('id');
	var studname = $('.select-list .info').find('.studname').html();
	 
	$('#studentfilter').attr('data-id',studno);
	$('#studentfilter').val(studname);
	$('#studentno').val(studno);
	$('#studentno').change();
	$('#selectmodal').modal('hide');
   });	
  if(studno!==undefined && studno!==''){
       $('#xreevaluate').trigger('click');
  }
});
