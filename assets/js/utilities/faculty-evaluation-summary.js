$('document').ready(function(){
  $('body').on('click','#btnrefresh',function(e){
    var t = $('#term').val();
    //loadURL('faculty-evaluation-summary/txn/get/info?term='+t,$('#tblfaculty tbody'));
    $.ajax({
	     type:"POST",
	     url: 'faculty-evaluation-summary/txn/get/info',
	     data:{term:t},
    	 dataType: "JSON",
	     async:true,
		 success: function (result){
		  if(result.success)
		  {
			$('#tblfaculty tbody').html(result.content);
            console.clear();			
		  }
		  else
		  {
		   alert('Failed to load data');
		   rem_antidepr();
		  }
		 },
		 error: function(xerror)
		 {
		  alert('Failed to load data');
		  rem_antidepr();
		 }
		}); 
  });
  
  $('body').on('click','.view',function(e){
    var empid = $(this).attr('data-empn');
	var resp  = $(this).attr('data-resp');
	var term  = $('#term').val();
	
    $('.faculty-list').hide();
    $('.evaluation-form').show();
    $('html,body').scrollTop(0);
    $('#respondent').html($(this).attr('data-resp') );
	$('#selEmployee').html($(this).attr('data-name'));
	$('#btnprint').attr('data-empn',empid);
	$('#btnprint').attr('data-resp',resp);
	$('#btnprint').attr('data-term',term);
    load_evaluation( $(this).attr('data-id'), $(this).attr('data-resp') );
  });
  
  $('body').on('click','.print,#btnprint',function(){
    var empid = $(this).attr('data-empn');
	var resp  = $(this).attr('data-resp');
	var term  = $('#term').val();
	if(resp<=0){
	 alert('No Respondent!! Nothing to Print!!!');
	}else{
	 window.open(base_url+'faculty_evaluation_summary/printout/faculty/'+term+'/'+empid,'_blank');
	}
  });
  
  $('body').on('click','#btncancel',function(e){
    $('.faculty-list').show();
    $('.evaluation-form').hide();
	$('#btnprint').attr('data-empn','');
	$('#btnprint').attr('data-resp',0);
	$('#btnprint').attr('data-term',0);
  });
  
  $("#term").on('change',function () {
    var t = $('#term').val();    
     localStorage.setItem('term', t);
  });
  $(".filter-table").on('change',function () {
        var tblname = 'tblfaculty';
        var data = this.value.split(" ");
        var r = $("#"+tblname+" tbody").find("tr");
        if (this.value == "") {
            r.show();
            return;
        }
        r.hide();
        r.filter(function (i, v) {
            var $t = $(this);
            for (var d = 0; d < data.length; ++d) {
                if ($t.text().toLowerCase().indexOf(data[d].toLowerCase()) > -1) {
                    return true;
                }
            }
            return false;
        })
        .show();
    }).focus(function () {
        this.value = "";
        $(this).css({ "color": "black"});
        $(this).unbind('focus');
    }).css({"color": "#C0C0C0"});
    
    function load_evaluation(id,resp){
        var t = $('#term').val();

    //loadURL('faculty-evaluation-summary/txn/get/info?term='+t,$('#tblfaculty tbody'));
    $.ajax({
	     type:"POST",
	     url: 'faculty-evaluation-summary/txn/get/eval',
	     data:{term:t, id: id, respondent: resp },
    	 dataType: "JSON",
	     async:true,
		 success: function (result){
		  console.log('test');
		  console.log(result);
		  if(result.success)
		  {
			$('#tblquestions tbody').html(result.content);
            $('#tblcomment tbody').html(result.comment);
            //console.clear();			
		  }
		  else
		  {
		   alert('Failed to load data');
		   //rem_antidepr();
		  }
		 },
		 error: function(xerror)
		 {
		  alert('Failed to load data');
		  //rem_antidepr();
		 }
		}); 
    }
    
    var term = localStorage.getItem('term');
    
    if (term != undefined){
        $('#term').val(term);
        $('#btnrefresh').click();
    }

    $('.evaluation-form').hide();
    	
});