window.onload = xdisplayschedules;

var emptyimg="";

var xtablimit = 4;
var xactivetab = 0;
var xtabid = 0;
var xtabstatus = 1;

var xroot = '';
var xterm = 0;
var xside = 0;
var xmidposted = 0;
var xfinposted = 0;

var xglink = '';
var xglink = '';

var divschedid = 'xsched';
var divschedstatid = 'xstats';

var xpgcomp  ='';
var xptgcomp ='';
var xprecomp ='';

var xmgcomp  ='';
var xmtgcomp ='';
var xmidcomp ='';

var xpfgcomp    ='';
var xpftgcomp   ='';
var xprefincomp ='';;

var xfgcomp  ='';
var xftgcomp ='';
var xfincomp ='';

var xpcs ='';
var xpe  ='';
var xpg  ='';
var xptg ='';
var xpre ='';

var xmcs ='';
var xme  ='';
var xmg  ='';
var xmtg ='';
var xmid ='';

var xpfcs ='';
var xpfe  ='';
var xpfg  ='';
var xpftg ='';
var xprefin ='';

var xfcs ='';
var xfe  ='';
var xfg  ='';
var xftg ='';
var xfin ='';

var xrex    ='';
var xgrade  ='';
var xremark ='';

var xprestats=0;
var xmidstats=0;
var xprefinstats=0;
var xfinstats=0;
 
var FA_detected = false;


function xdisplayschedules()
{
  var xlink = $('#mnuhome').attr('href');
  var termid = $('#ayterm').val();
  var facultyid = $('#mnufac').attr('data-pointer');
  
  xlink = xlink.replace('home','faculty/classsched');
  
  if(xlink!='')
  {   
      var divid = '#'+divschedid;
	  var dividb = '#'+divschedstatid;
	  
	  $(divid).html('<center><span class="fa fa-refresh fa-4x fa-spin"></span></center>');
	  
	  ajaxschedules(xlink,divid,dividb,termid,facultyid);
  }	  
}

function ajaxschedules(xlink,divid,dividb,termid,facultyid)
{
   var mineonly= 1;
   var campid = $('#campus').val();
   
   if($('#userscheds').length > 0)
   {
    if($('#userscheds').prop("checked")==false)
	{mineonly= 0;}
   }
   
   $(divid).html('<center><span class="fa fa-refresh fa-4x fa-spin"></span></center>');
   $.ajax({
		  type: "POST",
		  url: xlink,
		  data: {termid:"'"+termid+"'",campusid:campid,facultyid:"'"+facultyid+"'",mineonly:"'"+mineonly+"'"},
		  dataType: "text",
		  success: function (result) 
		           {
				    var data = result.split('[=,=]');
			        $(divid).html(data[0]);
			        $(dividb).html(data[1]);
					$('#xreload').removeAttr('onclick');
					$('#xreload').attr('onclick','xdisplayschedules();');
		           }
	      });
}

function checkencodeperiod()
{
 xlink = xroot+'checkgradeencoding';
 $('#xnote').html('');
 $.ajax({
		  type: "POST",
		  url: xlink,
		  data: {termid:"'"+xterm+"'"},
		  dataType: "text",
		  success: function (result) 
		           {
				    var x = result;
					//alert(x);
					if(x!=="no data")
					{
					 if(x==0)
					 {$('#xnote').html('<div class="alert alert-warning alert-block"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading">Alert</h4>Grade encoding is not allowed this time.</div>');
					  enableencoding(0);}
					 else if(x==1)
					 {$('#xnote').html('<div class="alert alert-success alert-block"><a class="close" data-dismiss="alert" href="#">×</a>Grade encoding for Midterm is allowed this time.</div>');
					  enableencoding(1);}
					 else if(x==2)
					 {$('#xnote').html('<div class="alert alert-success alert-block"><a class="close" data-dismiss="alert" href="#">×</a>Grade encoding for Finals is allowed this time.</div>');
					  enableencoding(2);}
					}
					else
					{$('#xnote').html('<div class="alert alert-danger alert-block"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading">Error</h4>Unable to fetch the right data.<br>Please report this imediately.</div>');
					  enableencoding(0);}
				   }
		});
}

function dsply_gradeencode(schedid,xown)
{
 var xlink = $('#mnuhome').attr('href');
 var divid = '#xgradeclass'; 
 var tblid ='';
 var xid ='';
 var xmultitab = $('#facultyinfo').attr('data-multitab');
 
 var xlabel='';
 var xsubcode = "";
 var xsubtitle = "";
 var xsubsec="";
 
 xlink = xlink.replace('home','faculty/classgrade');
 
 $('#xnote').html('');
 $(divid+' table').hide();
  
 if ($('table[data-pointer="'+schedid+'"]').length > 0 && xtabstatus==1)
 {
  xactivetab = $('table[data-pointer="'+schedid+'"]').attr('id').replace('gradetable','');
  tblid = '#gradetable'+xactivetab;
 
  $('#'+$('table[data-pointer="'+schedid+'"]').attr('id')).show();
  $('#xgraderefresh').removeClass('disabled');
  generatestats();
  managebuttons();
  
  $('#tg'+xactivetab).click();
  
  return 0;
 }

 if(xtabstatus==1 && xmultitab==1)
 {
  xid = xtabid;

  if(xtabid > 0)
  {
    if($('#xtabsnav li[id*="tl"]').length < xtablimit)
    {
	 addtab(xid);
	}
    else
    {
	   $.smallBox({
			  title: "Tab count Limit Reach! Close other tab(s) to open new one.",
			  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
			  color: "##A90329",
			  iconSmall: "fa fa-warning shake animated",
			  timeout: 10000                  
			 });
		return false;	 
	}	
  }	
 }
 else
 {
  xid = xactivetab;
 } 
 
 if(xmultitab==0)
 {
  xid = xactivetab;
  xtabid=xactivetab;
 }
 
 tblid = '#gradetable'+xid;
 xactivetab = xid;
 
 if(xtabstatus==1)
 {
  xsubcode = $('#xschedules tr[data-pointer="'+schedid+'"] #scode').html();
  xsubtitle = $('#xschedules tr[data-pointer="'+schedid+'"] #stitle').html();
  xsubsec = $('#xschedules tr[data-pointer="'+schedid+'"] #ssec').html();
  xlabel='<b>'+xsubcode+' - '+xsubtitle+' | '+xsubsec+'</b>';
 }
 else
 {
  xlabel=$(tblid).attr('data-label');
 }
  
 if($(tblid).length > 0)
 {$(tblid).replaceWith('<div id="'+tblid.replace('#','')+'"><center><span class="fa fa-refresh fa-4x fa-spin"></span></center></div>');}
 else if($(tblid).length <= 0 && $(divid+' table').length>0)
 {$(divid).append('<div id="'+tblid.replace('#','')+'"><center><span class="fa fa-refresh fa-4x fa-spin"></span></center></div>');}
 else
 {$(divid).html('<div id="'+tblid.replace('#','')+'"><center><span class="fa fa-refresh fa-4x fa-spin"></span></center></div>');}
 
//if(xtabstatus==2){ return 0;}
 
 var xcode=$('#xschedules tr[data-pointer="'+schedid+'"]').attr('data-code');
 var xsec=$('#xschedules tr[data-pointer="'+schedid+'"]').attr('data-section');
 
 $('#tg'+xid).click();
 $('#tg'+xid).attr('onclick','activetab('+xid+');');
 //$('#tg'+xid).append('<i class="fa fa-times txt-color-red" onclick="removetab('+xid+');"></i><br><small>['+xcode+'('+xsec+')]</small>');
 if(xtabstatus==1 && xmultitab==1)
 {
  $('#tg'+xid).append('<i class="fa fa-times txt-color-red" onclick="removetab('+xid+');"></i>');
 }
 
 $.ajax({
		  type: "POST",
		  url: xlink,
		  data: {schedid:"'"+schedid+"'",xid:"'"+xid+"'"},
		  dataType: "text",
		  success: function (result) 
		           {
				    //alert(result);
				    if(result.trim()!='')
					{
				     $(tblid).replaceWith(result);
					 $(tblid).attr('data-mgmt',xown);
					 $(tblid).attr('data-label',xlabel);
					 $('#xgraderefresh').removeClass('disabled');
					 generatestats();
					 managebuttons();
					 
					 if(xtabstatus==1)
                     {xtabid++;}
					 else
					 {xtabstatus=1;}
					}
					else
					{
					 $(tblid).replaceWith('<p><center><i class="fa fa-warning"></i> No Data Found.</center></p>');
					 $('#xgraderefresh').addClass('disabled');
					}
		           },
			error: function(){ alert('Error Loading Gradesheet. Please Report to administrators.');}	   
				   
	      });
}

function refreshgrade()
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 
 if($(tblid).length > 0)
 {
  var schedid = $(tblid).attr('data-pointer');
  var xown = $(tblid).attr('data-mgmt');
   
  if(schedid!='')
  {
   xtabstatus=2;
   dsply_gradeencode(schedid,xown);
  }
   
 }
}

function generatestats()
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 var totalmale = 0;
 var totalfemale = 0;
 var totalstud = 0;
 var tmpid = '';
 
 var xpass = 0;
 var xfail = 0;
 var xINC = 0;
 var xdrop = 0;
 
 if($(tblid).length > 0)
 {
  tmpid = tblid+' tr[data-gender="M"]';
  totalmale = parseInt($(tmpid).length);
  tmpid = tblid+' tr[data-gender="F"]';
  totalfemale = parseInt($(tmpid).length);
  tmpid = tblid+' span[data-remark="Passed"]';
  xpass = parseInt($(tmpid).length);
  tmpid = tblid+' span[data-remark="Failed"]';
  xfail = parseInt($(tmpid).length);
  tmpid = tblid+' span[data-remark="Incomplete"]';
  xINC = parseInt($(tmpid).length);
  tmpid = tblid+' span[data-remark="Dropped"]';
  xdrop = parseInt($(tmpid).length);
  totalstud = totalfemale + totalmale;
 }
 
 

 $('#statsMale').html(totalmale);
 $('#statsFemale').html(totalfemale);
 $('#statsTStud').html(totalstud);
 
 $('#statsPass').html(xpass);
 $('#statsFailed').html(xfail);
 $('#statsINC').html(xINC);
 $('#statsDrop').html(xdrop);
}

function managebuttons()
{
 resetstudinfo();
 
 var xid = xactivetab;
 var ximg = $('#mnuhome').attr('href');
     ximg = ximg.replace('home','assets/img/posted.png');
 var tblid = '#gradetable'+xid;
 var xown = $(tblid).attr('data-mgmt');
 var xallow = $('#xgradeclass').attr('data-allow');
 var xpost = $('#xgradeclass').attr('data-xpost');
 
 var onperioda = -1;
 var onperiodb = -1;
 var isposteda = -1;
 var ispostedb = -1;
 
 if($("#xpostm").length > 0)
 {$("#xpostm").addClass('disabled').addClass('hidden');}
 
 if($("#xunpostm").length > 0)
 {$("#xunpostm").addClass('disabled').addClass('hidden');}
 
 if($("#xpostf").length > 0)
 {$("#xpostf").addClass('disabled').addClass('hidden');}
 
 if($("#xunpostf").length > 0)
 {$("#xunpostf").addClass('disabled').addClass('hidden');}
 
 $(tblid).css('background-image', '');
 
 
 $('#xnote').html('');
 $('#xsheetinfo').addClass('hidden');
 $('#xsheetinfo').html('');
 
 $("#xupload").addClass('disabled');
 $("#xupload").addClass('hidden');
 $("#xupload").attr('onclick','');
 $('#xsave').addClass('disabled')
 $('#xsave').removeAttr('onclick');
 $('#xprintgrade').addClass('disabled');
 
 if($(tblid).length > 0)
 {
  $('#xsheetinfo').html($(tblid).attr('data-label'));
  $('#xsheetinfo').removeClass('hidden');
  
  onperioda = $(tblid).attr('data-onperioda');
  onperiodb = $(tblid).attr('data-onperiodb');
  
  isposteda = $(tblid).attr('data-posteda');
  ispostedb = $(tblid).attr('data-postedb');
  
  
  $('#xprintgrade').removeClass('disabled');
 
  if(xown==1)
  {
   $("#xupload").removeClass('disabled').removeClass('hidden').attr('onclick','manageupload()');
  } 
  
  if(onperiodb==0)
  {$('#xnote').html('<div class="alert alert-warning"><i class="fa fa-warning"></i> Midterm Grade Encoding is Closed at the Moment.</div>');}
  else
  {
   if(ispostedb==0 && isposteda==0)
   {
    $("#xpostm").removeClass('disabled').removeClass('hidden').attr('onclick','postgradesheet(2);');
    $('#xsave').removeClass('disabled');
    $('#xsave').attr('onclick','antidepressant();savegrades();');
	
	enableencoding(2);
   }
   else
   {
    if($("#xunpostm").length > 0)
	{
	 $("#xunpostm").removeClass('disabled').removeClass('hidden').attr('onclick','unpostgradesheet(2);');
	}
   }
  }
  
  
  if(onperioda==0)
  {
   if(onperiodb==0)
    $('#xnote').html('<div class="alert alert-warning"><i class="fa fa-warning"></i> Grade Encoding is Closed at the Moment.</div>');
 //else
 // $('#xnote').html('<div class="alert alert-warning"><i class="fa fa-warning"></i> Final Grade Encoding is Closed at the Moment.</div>');
   
   if(isposteda==1)
   {
    if($("#xpostm").length > 0)
    {$("#xpostm").addClass('disabled').addClass('hidden');}
 
    if($("#xunpostm").length > 0)
    {$("#xunpostm").addClass('disabled').addClass('hidden');}
	
    $('#xnote').html('<div class="alert alert-warning"><i class="fa fa-warning"></i> Grades are already <b>posted</b>! No changes can be done.</div>');
    $(tblid).css('background-image', 'url('+ximg+')');
   }
  }
  else
  {
   if(isposteda==0)
   {
    $('#xnote').html('');
    $("#xpostf").removeClass('disabled').removeClass('hidden').attr('onclick','postgradesheet(1);');
    $('#xsave').removeClass('disabled');
    $('#xsave').attr('onclick','antidepressant();savegrades();');
	enableencoding(1);
   }
   else if(isposteda==1)
   {
    if($("#xpostm").length > 0)
    {$("#xpostm").addClass('disabled').addClass('hidden');}
 
    if($("#xunpostm").length > 0)
    {$("#xunpostm").addClass('disabled').addClass('hidden');}
	
     
    if($("#xunpostf").length > 0)
	{$("#xunpostf").removeClass('disabled').removeClass('hidden').attr('onclick','unpostgradesheet(1);');}
	
    $('#xnote').html('<div class="alert alert-warning"><i class="fa fa-warning"></i> Grades are already <b>posted</b>! No changes can be done.</div>');
    $(tblid).css('background-image', 'url('+ximg+')');
   }
  }
  
  managetabindex(); 
 }
}

function postgradesheet(xopt)
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 var xlink = $('#mnuhome').attr('href');
     xpassword = xlink.replace('home','home/passwordverify');
 
 var schedid = $(tblid).attr('data-pointer');
 var isaltered = $(tblid).attr('data-altered');
 
 var xfileb = $(tblid).attr('data-fileb');
 var xfilea = $(tblid).attr('data-filea');
 
 var xfileup = false;
 
 if(xopt==1)
 {
  xopt='2.1';
  if(xfilea.trim()!='') xfileup=true;
 }
 else if(xopt==2)
 {
  xopt='2.2';
  if(xfileb.trim()!='') xfileup=true;
 }
 
 if(isaltered == 1)
 {
  $.SmartMessageBox({
                    title : "<i class='fa fa-warning txt-color-xyellow'></i> Warning: Grade Sheet is <strong>NOT</strong> yet <strong>SAVED</strong>.",
                    content : "Please save the grade sheet before posting.",
                    buttons : "[OK]"
                   }, 
				   function(ButtonPress, Value) 
				   {
				    if (ButtonPress == "OK") 
				    {return 0;}
				   });
  return false;
 }
 
 if(xfileup == false && $('#xupload').length>0)
 {
  $.SmartMessageBox({
                    title : "<i class='fa fa-warning txt-color-xyellow'></i> Warning: Grade Sheet is <strong>Not</strong> yet uploaded.",
                    content : "Please make sure that grade sheet is uploaded before posting.",
                    buttons : "[OK]"
                   }, 
				   function(ButtonPress, Value) 
				   {
				    if (ButtonPress == "OK") 
				    {return 0;}
				   });
  return false;
 }
 
 var validatefield = checkrequiredfields(xopt); 
 
 if(validatefield == false)
 {
  $.SmartMessageBox({
                    title : "<i class='fa fa-warning txt-color-xyellow'></i> Warning: This Grade Sheet is Incomplete.",
                    content : "Please make sure that all final grade are filled."+((FA_detected==true)?"<br/> Please input all absences of student with FA(Failed Due to Absences).":""),
                    buttons : "[OK]"
                   }, 
				   function(ButtonPress, Value) 
				   {
				    if (ButtonPress == "OK") 
				    {
					  FA_detected=false;
					  return 0;
					}
				   });
  return false;
 }
 
 $.SmartMessageBox({
                    title : "<i class='fa fa-warning txt-color-xyellow'></i> Warning: You are about to POST this Grade Sheet.",
                    content : "Please make sure that you have thoroughly checked all entries, NO FURTHER MODIFICATIONS WILL BE ALLOWED, After Posting"
                               + "<br> Do you wish to continue?",
                    buttons : "[No][Yes]"
                   }, 
				   function(ButtonPress, Value) 
				   {
				   if (ButtonPress == "No") 
				   {return 0;}
 
				    $.SmartMessageBox({
								 title : "Verification Needed",
								 content : "Enter your Password For Verification",
								 buttons : "[Cancel][Ok]",                 
								 input : "password",
								 placeholder : "Enter your password here"
								 }, 
								 function(ButtonPress, Value) 
								 {
								   if (ButtonPress == "Cancel") 
								   {return 0;}
								   
									$.ajax({
											type: "POST",
											url: xpassword,
											data: {pwd:"'"+Value+"'"},
											async:false,
											dataType: "text",
											success: function (result) 
													 {
													  var data = result.trim();
													  if(data=='nodata' || data=='invalid')
													  {
													   alert('Invalid Password');
													   return;
													  }
													  else
													  {
														xlink = xlink.replace('home','faculty/manage/2');
														if(xopt!=0)
														{
														  managegradesheet(xlink,xopt,schedid);
														}
													  }
													 }
										   }); 
								 }); 
				    });
}

function unpostgradesheet(xopt)
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 var xlink = $('#mnuhome').attr('href');
     xpassword = xlink.replace('home','home/passwordverify');
 
 var schedid = $(tblid).attr('data-pointer');
 
 if(xopt==1)
 {xopt='3.1';}
 else if(xopt==2)
 {xopt='3.2';}
 
 $.SmartMessageBox({
                 title : "Verification Needed",
                 content : "Enter your Password For Verification",
                 buttons : "[Cancel][Ok]",                 
                 input : "password",
                 placeholder : "Enter your password here"
                 }, 
				 function(ButtonPress, Value) 
				 {
				   if (ButtonPress == "Cancel") 
				   {return 0;}
				   
				    $.ajax({
					        type: "POST",
					        url: xpassword,
					        data: {pwd:"'"+Value+"'"},
					        async:false,
					        dataType: "text",
					        success: function (result) 
					                 {
					                  var data = result.trim();
					                  if(data=='nodata' || data=='invalid')
					                  {
					                    alert('Invalid Password');
					                  }
									  else
									  {
									    xlink = xlink.replace('home','faculty/manage/2');
										if(xopt!=0)
										{
										  managegradesheet(xlink,xopt,schedid);
										}
									  }
					                 }
					       }); 
                 });
}

function managegradesheet(xlink,xopt,schedid)
{
  $.ajax({
	    type: "POST",
	    url: xlink,
	    data: {opt:"'"+xopt+"'",sched:"'"+schedid+"'"},
	    dataType: "text",
	    success: function(result) 
			     {
				  var data = result.trim();
				  if(data!='nodata')
				  {refreshgrade();}
			     }
	   });
}

function enableencoding(xopt)
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xactivetab;
 var encodeother = $('#xgradeclass').attr('data-allow');
 var xown = $(tblid).attr('data-mgmt');
 var xletteronly = $(tblid).attr('data-lettergrade');
 
 var xclass=((xletteronly==1)?'letteronly':'');
 
 if((xown==1) || (xown==0 && encodeother==1))
 {
  if(xopt==1)
  {
   $(tblid +' span[id*="rex"]').each(
    function()
    {
     var colid  = this.id;
     var colval = $(tblid +' #'+colid).html();
	 var x = colid.replace("rex","");
     var xcept = $(this).is('.except');
	 if(xcept==1) return true;
	 turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"',xclass);
    }
   );
   
   $(tblid +' span[id*="fin"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("fin","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"',xclass);
   }
   );
     
   $(tblid +' span[id*="ftg"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("ftg","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
     
   $(tblid +' span[id*="fg"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("fg","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
     
   $(tblid +' span[id*="fe"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("fe","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
   
   $(tblid +' span[id*="fcs"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("fcs","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
	turntoinput(tblid +' #'+colid,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
   
  }
  else if(xopt==2)
  {
   $(tblid +' span[id*="mid"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("mid","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"',xclass);
   }
   );
     
   $(tblid +' span[id*="mtg"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("mtg","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
     
   $(tblid +' span[id*="mg"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("mg","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
     
   $(tblid +' span[id*="me"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("me","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
     
   $(tblid +' span[id*="mcs"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("mcs","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
  }
  else if(xopt==3)
  {
   $(tblid +' span[id*="pre"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("pre","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"',xclass);
   }
   );
     
   $(tblid +' span[id*="ptg"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("ptg","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
     
   $(tblid +' span[id*="pg"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("pg","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
     
   $(tblid +' span[id*="pe"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("pe","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
     
   $(tblid +' span[id*="pcs"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("pcs","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   }
   );
  }
  
  if(xopt>0)
  {
   $(tblid +' span[id*="noofabs"]').each(
   function()
   {
    var colid  = this.id;
    var colval = $(tblid +' #'+colid).html();
    var x = colid.replace("noofabs","");
    var xcept = $(this).is('.except');
	if(xcept==1) return true;
    turntoinput(this,colid,colval,'onchange="computation('+x+');" onfocus="showstudinfo('+x+');"','numberonly');
   });
  }
 }
 
 $(tblid+" input").on("keydown", function (event){
      var current = $(this).attr('tabindex');
	  
	  if (event.keyCode == 13){
       event.preventDefault();
	   current++;
	   $(tblid+' input[tabindex="'+current+'"]').focus();
      }
    });
}

function turntoinput(xtarget,id,tvalue,attrib,xclass){
 attrib = ((attrib!==undefined)?attrib:'');
 xclass = ((xclass!==undefined)?xclass:'');
 tvalue = ((tvalue!==undefined)?tvalue:'');
 
 var tmpctrl = '<input id="'+id+'" type=text class="ptc_tbltextbox xcenter '+xclass+'" style="box-shadow:none;padding:0px !important;" '+attrib+' value="'+tvalue+'"/>';
 $(xtarget).replaceWith(tmpctrl);
 //alert(tmpctrl);
}


function selectrow(a)
{
 $(a).addClass("info").siblings().removeClass('info');
}


function hidecolumn(a)
{
 $('#xschedules tr').find('td:eq('+a+'),th:eq('+a+')').hide();
}

function showcolumn(a)
{
 $('#xschedules tr').find('td:eq('+a+'),th:eq('+a+')').show();
}

function addtab(x)
{
 $('#xtabsnav').append('<li id="tl'+x+'"><a data-toggle="tab" href="#g0" id="tg'+x+'" class=""><i class="fa fa-table"></i><span class="hidden-mobile hidden-tablet"><small> Grade Encoding </small></span> </a></li>');
}

function activetab(x)
{
 var divid = '#xgradeclass'; 
 var tblid ='';
 
 $(divid+' table').hide();
 
 xactivetab = x;
 tblid = '#gradetable'+x;
 
 $(tblid).show();
 $('#xgraderefresh').removeClass('disabled');
 generatestats();
 managebuttons();
}

function removetab(x)
{
 $('#tg'+x).attr('href','');
 $('#tl'+x).remove();
 $('#gradetable'+x).remove();
 $('#cs0').click();
}


function findgrade(x)
{
 var output = '';
 x = x.toString();
 
 if(x.trim()!='' && x.trim()!='@und')
 {
  
  $('#tblgradesys tbody tr').each(function()
  {
   var xmin = $(this).attr('data-min');
   var xmax = $(this).attr('data-max');
   var xgrade = $(this).attr('data-grade');
   var isnum = $.isNumeric(x);
  
   if(isnum==true)
   {
	var a = parseFloat(xmin);
	var b = parseFloat(xmax);
	//alert('('+a+' <= '+parseFloat(x)+') && ('+b+' >= '+parseFloat(x)+')');
	if((a <= parseFloat(x)) && (b >= parseFloat(x)))
	{
	 output = xgrade.toString();
	 return false;
	}
   }
   else
   {
	if(xmin == x && xmax== x)
	{
	 output = xgrade;
	 return false;
	}
   }
  
  });
  
  return output;
  
 }
 else
 {
  return '';
 }
}


function findremarks(x)
{
 var routput = '';
 
 if(x.trim()!='' && x.trim()!='@und')
 {
	
  $('#tblgradesys tbody tr').each(function()
  {
   var xmin = $(this).attr('data-min');
   var xmax = $(this).attr('data-max');
   var xgrade = $(this).attr('data-grade');
   var xremark = $(this).attr('data-remark');
   var isnum = $.isNumeric(x);
  
   if(isnum==true)
   {
	var a = parseFloat(xmin);
	var b = parseFloat(xmax);
	
	if(((a <= parseFloat(x)) && (b >= parseFloat(x))) || xgrade==x)
	{
	 routput = xremark;
	 return false;
	}
   }
   else
   {
	if((xmin == x && xmax== x) || xgrade==x)
	{
	 routput = xremark;
	 return false;
	}
   }
  
  });
  return routput;
 }
 else
 {
  return '';
 }
}


function findtransmute(x)
{
 var toutput = '';
 
 x=x.toString();
 if(x.trim()!='' && x.trim()!='@und')
 {
  $('#tblgradetrans tbody tr').each(function()
  {
   var xmin = $(this).attr('data-min');
   var xmax = $(this).attr('data-max');
   var tgrade = $(this).attr('data-tgrade');
   var isnum = $.isNumeric(x);
  
   if(isnum==true)
   {
	var a = parseFloat(xmin);
	var b = parseFloat(xmax);
	if((a <= parseFloat(x)) && (b >= parseFloat(x)))
	{
	 toutput = tgrade;
	 return false;
	}
   }
   else
   {
	if(xmin == x && xmax== x)
	{
	 toutput = tgrade;
	 return false;
	}
   }
  
  });
  return toutput;
 }
 else
 {
  return '';
 }
}

function managetabindex()
{
 var tabopt = $('#edirection').val();
 var tabindx = 1;
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 
 if(tabopt==1)
 {
  var count = $(tblid+' tr[id*="stud"]').length;
  
  for(i=1;i<=count;i++)
  {
   if($(tblid+' input[id="mcs'+i+'"]').length>0){$(tblid+' input[id="mcs'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   if($(tblid+' input[id="me'+i+'"]').length>0){$(tblid+' input[id="me'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   if($(tblid+' input[id="mg'+i+'"]').length>0){$(tblid+' input[id="mg'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   if($(tblid+' input[id="mtg'+i+'"]').length>0){$(tblid+' input[id="mtg'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   if($(tblid+' input[id="mid'+i+'"]').length>0){$(tblid+' input[id="mid'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   
   if($(tblid+' input[id="fcs'+i+'"]').length>0){$(tblid+' input[id="fcs'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   if($(tblid+' input[id="fe'+i+'"]').length>0){$(tblid+' input[id="fe'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   if($(tblid+' input[id="fg'+i+'"]').length>0){$(tblid+' input[id="fg'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   if($(tblid+' input[id="ftg'+i+'"]').length>0){$(tblid+' input[id="ftg'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   if($(tblid+' input[id="fin'+i+'"]').length>0){$(tblid+' input[id="fin'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   
   if($(tblid+' input[id="rex'+i+'"]').length>0){$(tblid+' input[id="rex'+i+'"]').attr('tabindex',tabindx); tabindx++;}
   
   if($(tblid+' input[id="noofabs'+i+'"]').length>0){$(tblid+' input[id="noofabs'+i+'"]').attr('tabindex',tabindx); tabindx++;}
  }
 }
 else if(tabopt==2)
 {
  $(tblid+' input[id*="mcs"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="me"]').each(function(){var tmpid = this.id;  $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="mg"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="mtg"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="mid"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  
  $(tblid+' input[id*="fcs"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="fe"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="fg"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="ftg"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="fin"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  
  $(tblid+' input[id*="rex"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
  $(tblid+' input[id*="noofabs"]').each(function(){var tmpid = this.id; $(tblid+' #'+tmpid).attr('tabindex',tabindx); tabindx++;});
 }

}

function clearcalculator()
{
 xpcs ='';
 xpe  ='';
 xpg  ='';
 xptg ='';
 xpre ='';

 xmcs ='';
 xme  ='';
 xmg  ='';
 xmtg ='';
 xmid ='';

 xpfcs ='';
 xpfe  ='';
 xpfg  ='';
 xpftg ='';
 xprefin ='';

 xfcs ='';
 xfe  ='';
 xfg  ='';
 xftg ='';
 xfin ='';

 xrex    ='';
 xgrade  ='';
 xremark ='';
}

function computation(x)
{
 clearcalculator();
 
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 $(tblid).attr('data-altered','1');
 
 var istransmuted =$(tblid).attr('data-transmuted');
 
 
 xpgcomp  =(($('#prelimdesc').length >0)?$('#prelimdesc').attr('data-grade'):"");
 xptgcomp =(($('#prelimdesc').length >0)?$('#prelimdesc').attr('data-tgrade'):"");
 xprecomp =(($('#prelimdesc').length >0)?$('#prelimdesc').attr('data-fgrade'):"");
 
 xmgcomp  =(($('#middesc').length >0)?$('#middesc').attr('data-grade'):"");
 xmtgcomp =(($('#middesc').length >0)?$('#middesc').attr('data-tgrade'):"");
 xmidcomp =(($('#middesc').length >0)?$('#middesc').attr('data-fgrade'):"");
 
 xpfgcomp  =(($('#prefindesc').length >0)?$('#prefindesc').attr('data-grade'):"");
 xpftgcomp =(($('#prefindesc').length >0)?$('#prefindesc').attr('data-tgrade'):"");
 xprefincomp =(($('#prefindesc').length >0)?$('#prefindesc').attr('data-fgrade'):"");
 
 xfgcomp  =(($('#findesc').length >0)?$('#findesc').attr('data-grade'):"");
 xftgcomp =(($('#findesc').length >0)?$('#findesc').attr('data-tgrade'):"");
 xfincomp =(($('#findesc').length >0)?$('#findesc').attr('data-fgrade'):"");

 xpcs =(($(tblid+' input[id="pcs'+x+'"]').length >0)?$(tblid+' input[id="pcs'+x+'"]').val():'');
  if(xpcs.trim()==''){xpcs =(($(tblid+' span[id="pcs'+x+'"]').length >0)?$(tblid+' span[id="pcs'+x+'"]').html():'');}
  if(xpcs.trim()==''){xpcs = '@und';}
 xpe =(($(tblid+' input[id="pe'+x+'"]').length >0)?$(tblid+' input[id="pe'+x+'"]').val():'');
  if(xpe.trim()==''){xpe =(($(tblid+' span[id="pe'+x+'"]').length >0)?$(tblid+' span[id="pe'+x+'"]').html():'');}
  if(xpe.trim()==''){xpe = '@und';}
 xpg =(($(tblid+' input[id="pg'+x+'"]').length >0)?$(tblid+' input[id="pg'+x+'"]').val():'');
  if(xpg.trim()==''){xpg =(($(tblid+' span[id="pg'+x+'"]').length >0)?$(tblid+' span[id="pg'+x+'"]').html():'');}
  if(xpg.trim()==''){xpg = '@und';}
 xptg =(($(tblid+' input[id="ptg'+x+'"]').length >0)?$(tblid+' input[id="ptg'+x+'"]').val():'');
  if(xmtg.trim()==''){xptg =(($(tblid+' span[id="ptg'+x+'"]').length >0)?$(tblid+' span[id="ptg'+x+'"]').html():'');}
  if(xmtg.trim()==''){xptg = '@und';}
 xpre =(($(tblid+' input[id="pre'+x+'"]').length >0)?$(tblid+' input[id="pre'+x+'"]').val():'');
  if(xpre.trim()==''){xpre =(($(tblid+' span[id="pre'+x+'"]').length >0)?$(tblid+' span[id="pre'+x+'"]').html():'');}
  if(xpre.trim()==''){xpre = '@und';}

 xmcs =(($(tblid+' input[id="mcs'+x+'"]').length >0)?$(tblid+' input[id="mcs'+x+'"]').val():'');
  if(xmcs.trim()==''){xmcs =(($(tblid+' span[id="mcs'+x+'"]').length >0)?$(tblid+' span[id="mcs'+x+'"]').html():'');}
  if(xmcs.trim()==''){xmcs = '@und';}
 xme =(($(tblid+' input[id="me'+x+'"]').length >0)?$(tblid+' input[id="me'+x+'"]').val():'');
  if(xme.trim()==''){xme =(($(tblid+' span[id="me'+x+'"]').length >0)?$(tblid+' span[id="me'+x+'"]').html():'');}
  if(xme.trim()==''){xme = '@und';}
 xmg =(($(tblid+' input[id="mg'+x+'"]').length >0)?$(tblid+' input[id="mg'+x+'"]').val():'');
  if(xmg.trim()==''){xmg =(($(tblid+' span[id="mg'+x+'"]').length >0)?$(tblid+' span[id="mg'+x+'"]').html():'');}
  if(xmg.trim()==''){xmg = '@und';}
 xmtg =(($(tblid+' input[id="mtg'+x+'"]').length >0)?$(tblid+' input[id="mtg'+x+'"]').val():'');
  if(xmtg.trim()==''){xmtg =(($(tblid+' span[id="mtg'+x+'"]').length >0)?$(tblid+' span[id="mtg'+x+'"]').html():'');}
  if(xmtg.trim()==''){xmtg = '@und';}
 xmid =(($(tblid+' input[id="mid'+x+'"]').length >0)?$(tblid+' input[id="mid'+x+'"]').val():'');
  if(xmid.trim()==''){xmid =(($(tblid+' span[id="mid'+x+'"]').length >0)?$(tblid+' span[id="mid'+x+'"]').html():'');}
  if(xmid.trim()==''){xmid = '@und';}

 xfcs =(($(tblid+' input[id="fcs'+x+'"]').length >0)?$(tblid+' input[id="fcs'+x+'"]').val():'');
  if(xfcs.trim()==''){xfcs =(($(tblid+' span[id="fcs'+x+'"]').length >0)?$(tblid+' span[id="fcs'+x+'"]').html():'');}
  if(xfcs.trim()==''){xfcs = '@und';}
 xfe =(($(tblid+' input[id="fe'+x+'"]').length >0)?$(tblid+' input[id="fe'+x+'"]').val():'');
  if(xfe.trim()==''){xfe =(($(tblid+' span[id="fe'+x+'"]').length >0)?$(tblid+' span[id="fe'+x+'"]').html():'');}
  if(xfe.trim()==''){xfe = '@und';}
 xfg =(($(tblid+' input[id="fg'+x+'"]').length >0)?$(tblid+' input[id="fg'+x+'"]').val():'');
  if(xfg.trim()==''){xfg =(($(tblid+' span[id="fg'+x+'"]').length >0)?$(tblid+' span[id="fg'+x+'"]').html():'');}
  if(xfg.trim()==''){xfg = '@und';}
 xftg =(($(tblid+' input[id="ftg'+x+'"]').length >0)?$(tblid+' input[id="ftg'+x+'"]').val():'');
  if(xftg.trim()==''){xftg =(($(tblid+' span[id="ftg'+x+'"]').length >0)?$(tblid+' span[id="ftg'+x+'"]').html():'');}
  if(xftg.trim()==''){xftg = '@und';}
 xfin =(($(tblid+' input[id="fin'+x+'"]').length >0)?$(tblid+' input[id="fin'+x+'"]').val():'');
  if(xfin.trim()==''){xfin =(($(tblid+' span[id="fin'+x+'"]').length >0)?$(tblid+' span[id="fin'+x+'"]').html():'');}
  if(xfin.trim()==''){xfin = '@und';}
 
 xrex =(($(tblid+' input[id="rex'+x+'"]').length >0)?$(tblid+' input[id="rex'+x+'"]').val():'@und');
 xgrade ='';
 xremark ='';
 
 
 if($.isNumeric(xrex)==true){xrex = parseFloat(xrex).toFixed(2);}
 
 if($.isNumeric(xfin)==true)
     xfin = parseFloat(xfin).toFixed(2);
 else if(xfin!='')
     xfin = xfin.toUpperCase();
 
 if($.isNumeric(xmid)==true)
     xmid = parseFloat(xmid).toFixed(2);
 else if(xmid!='')
     xmid = xmid.toUpperCase();
 
 if(checkinputvalidation(xrex)==false)
 {
  alert('Input is Invalid.');
  $(tblid+' #rex'+x).val('').focus();
  $(tblid+' #remark'+x).html('');
  return false;
 } 
 
 if(checkinputvalidation(xfin)==false)
 {
  alert('Input is Invalid.');
  $(tblid+' #fin'+x).val('').focus();
  $(tblid+' #remark'+x).html('');
  return false;
 }
 
 if(checkinputvalidation(xmid)==false)
 {
  alert('Input is Invalid.');
  $(tblid+' #mid'+x).val('').focus();
  $(tblid+' #remark'+x).html('');
  return false;
 }  
 
 //midterm computation
 if(xmgcomp.trim()!='')
 {
  xmg = ((processcomp(xmgcomp)!='')?processcomp(xmgcomp):xmg);
  if(xmg!='' && xmg!='@und')
  {
   if($.isNumeric(xmg)==true)
   {
    //xmg = parseFloat(xfg).toFixed(3);
	xmg = parseInt(xmg)+'.000';
   }
     
   if($(tblid+' input[id="mg'+x+'"]').length>0){$(tblid+' input[id="mg'+x+'"]').val(xmg);}
   if($(tblid+' span[id="mg'+x+'"]').length>0){$(tblid+' span[id="mg'+x+'"]').html(xmg);}
  }   
 }
 
 if(xmtgcomp.trim()!='' && istransmuted==1)
 {
   var tmpmtg = processcomp(xmtgcomp);
   xmidcomp=xmidcomp.replace('@MG','@MTG');
   xmtg = (tmpmtg.toString()!='')?tmpmtg:xmtg; 
 }
 
 if(xmidcomp.trim()!='')
 {
   var tmpmid = processcomp(xmidcomp);
   xmid = (tmpmid.toString()!='')?tmpmid:xmid;   
 }
 
 //final computation
 if(xfgcomp.trim()!='')
 {
  xfg = ((processcomp(xfgcomp)!='')?processcomp(xfgcomp):xfg);
  if(xfg!='' && xfg!='@und')
  { 
   if($.isNumeric(xfg)==true)
   {
    //xfg = parseFloat(xfg).toFixed(3);
	xfg = parseInt(xfg)+'.000';
   }
   
   if($(tblid+' input[id="fg'+x+'"]').length>0){$(tblid+' input[id="fg'+x+'"]').val(xfg);}
   if($(tblid+' span[id="fg'+x+'"]').length>0){$(tblid+' span[id="fg'+x+'"]').html(xfg);}
  }   
 }
 
 if(xftgcomp.trim()!='' && istransmuted==1)
 {
  var tmpftg = processcomp(xftgcomp);
  xfincomp=xfincomp.replace('@FG','@FTG');
  xftg = (tmpftg.toString()!='')?tmpftg:xftg;  
 }
 
 if(xfincomp.trim()!='')
 {
   var tmpfin = processcomp(xfincomp);
   xfin = (tmpfin.toString()!='')?tmpfin:xfin;  
 }
 
 if(xfin!='' && xfin!='@und')
 {xgrade=xfin;}
 if(xrex!='' && xrex!='@und')
 {xgrade=xrex;}
 
 
 if(xgrade!='' && xgrade!='@und')
 {xremark=findremarks(xgrade)}
 
 
 if(xmtg!='' && xmtg!='@und' && istransmuted==1)
 {
  if($(tblid+' input[id="mtg'+x+'"]').length>0){$(tblid+' input[id="mtg'+x+'"]').val(xmtg);}
  if($(tblid+' span[id="mtg'+x+'"]').length>0){$(tblid+' span[id="mtg'+x+'"]').html(xmtg);}
 }
 else
 {
  if($(tblid+' input[id="mtg'+x+'"]').length>0){$(tblid+' input[id="mtg'+x+'"]').val('');}
  if($(tblid+' span[id="mtg'+x+'"]').length>0){$(tblid+' span[id="mtg'+x+'"]').html('');}
 }
 
 if(xmid!='' && xmid!='@und')
 {
  xmid = ((xmid=="@UND" || xmid=="@und")?"":xmid);
  //if($.isNumeric(xmid)){xmid=xmid.toFixed(2).toString();}
  if($(tblid+' input[id="mid'+x+'"]').length>0){$(tblid+' input[id="mid'+x+'"]').val(xmid);}
  if($(tblid+' span[id="mid'+x+'"]').length>0){$(tblid+' span[id="mid'+x+'"]').html(xmid);}
 } 
 
 if(xftg!='' && xftg!='@und' && istransmuted==1)
 {
  if($(tblid+' input[id="ftg'+x+'"]').length>0){$(tblid+' input[id="ftg'+x+'"]').val(xftg);}
  if($(tblid+' span[id="ftg'+x+'"]').length>0){$(tblid+' span[id="ftg'+x+'"]').html(xftg);}
 }
 else
 {
  if($(tblid+' input[id="ftg'+x+'"]').length>0){$(tblid+' input[id="ftg'+x+'"]').val('');}
  if($(tblid+' span[id="ftg'+x+'"]').length>0){$(tblid+' span[id="ftg'+x+'"]').html('');}
 }
 
 if(xfin.toString()!='' && xfin.toString()!='@und')
 {
  xfin = ((xfin=="@UND" || xfin=="@und")?"":xfin);
  if($(tblid+' input[id="fin'+x+'"]').length>0){$(tblid+' input[id="fin'+x+'"]').val(xfin);}
  if($(tblid+' span[id="fin'+x+'"]').length>0){$(tblid+' span[id="fin'+x+'"]').html(xfin);}
 }
 
 if(xrex.toString()!='' && xrex.toString()!='@und')
 {
  xrex = ((xrex=="@UND" || xrex=="@und")?"":xrex);
  if($(tblid+' input[id="rex'+x+'"]').length>0){$(tblid+' input[id="rex'+x+'"]').val(xrex);}
  if($(tblid+' span[id="rex'+x+'"]').length>0){$(tblid+' span[id="rex'+x+'"]').html(xrex);}
 }
 
 if(xremark!='@und')
 {
  if($(tblid+' input[id="remark'+x+'"]').length>0){$(tblid+' input[id="remark'+x+'"]').val(xremark).attr('data-remark',xremark);}
  if($(tblid+' span[id="remark'+x+'"]').length>0){$(tblid+' span[id="remark'+x+'"]').html(xremark).attr('data-remark',xremark);}
 }

 generatestats();
}

function processcomp(xformula)
{
  var tmptrans="";
  var tmpfinal="";
  
  xformula = xformula.replace(/%/g,"/100");
  
  xformula = xformula.replace("@PCS",xpcs);
  xformula = xformula.replace("@PE",xpe);
  xformula = xformula.replace("@PG",xpg);
  xformula = xformula.replace("@PTG",xptg);
  xformula = xformula.replace("@PID",xpre);
  
  xformula = xformula.replace("@MCS",xmcs);
  xformula = xformula.replace("@ME",xme);
  xformula = xformula.replace("@MG",xmg);
  xformula = xformula.replace("@MTG",xmtg);
  xformula = xformula.replace("@MID",xmid);
  
  xformula = xformula.replace("@FCS",xfcs);
  xformula = xformula.replace("@FE",xfe);
  xformula = xformula.replace("@FG",xfg);
  xformula = xformula.replace("@FTG",xftg);
  xformula = xformula.replace("@FIN",xfin);
  
  if(xformula.indexOf('Transmute:')>=0 && xformula.indexOf("@") < 0)
  {
   tmptrans = xformula.replace('Transmute:','');
   tmptrans = eval(tmptrans);
   
   return findtransmute(tmptrans);
  }
  
  if(xformula.indexOf('Final:')>=0 && xformula.indexOf("@") < 0)
  {
   tmpfinal = xformula.replace('Final:','');
   tmpfinal = eval(tmpfinal);
   
   return findgrade(tmpfinal);
  }
  
  //alert(xformula);
  if(xformula.indexOf("@") < 0)
  {
   return eval(xformula);
  }
  else
  {
   return '';
  }
}

function savegrades()
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 var xlink = $('#mnuhome').attr('href');
     xlink = xlink.replace('home','faculty/manage/1');
 var xcount = $(tblid+' tr[id*="stud"]').length;
 
 var schedid = $(tblid).attr('data-pointer');
 var xonperioda = $(tblid).attr('data-onperioda');
 var xonperiodb = $(tblid).attr('data-onperiodb');
 var xposteda = $(tblid).attr('data-posteda');
 var xpostedb = $(tblid).attr('data-postedb');
 var xsinglepost = $('#xgradeclass').attr('data-singlep');
 var xopt=0; 
 
 var xerror=0;
 
 xprestats=0;
 xmidstats=0;
 xprefinstats=0;
 xfinstats=0;
 
 antidepressant();
 
 if(xpostedb==0 && xonperiodb==1)
 {
  xopt='1.2';
  for(i=1;i<=xcount;i++)
  {
   var xstdno = $(tblid+' tr[id="stud'+i+'"]').attr('data-std');
   var xcs =  (($(tblid+' input[id="mcs'+i+'"]').length >0)?$(tblid+' input[id="mcs'+i+'"]').val():0);
   var xexam = (($(tblid+' input[id="me'+i+'"]').length >0)?$(tblid+' input[id="me'+i+'"]').val():0);
   var xgrade = (($(tblid+' input[id="mg'+i+'"]').length >0)?$(tblid+' input[id="mg'+i+'"]').val():0);
   var xtgrade = (($(tblid+' input[id="mtg'+i+'"]').length >0)?$(tblid+' input[id="mtg'+i+'"]').val():0);
   var xfinal = (($(tblid+' input[id="mid'+i+'"]').length >0)?$(tblid+' input[id="mid'+i+'"]').val():'');
   var xrex = (($(tblid+' input[id="rex'+i+'"]').length >0)?$(tblid+' input[id="rex'+i+'"]').val():'');
   var xremark = (($(tblid+' span[id="remark'+i+'"]').length >0)?$(tblid+' span[id="remark'+i+'"]').html():'');
   var xnoabs = (($(tblid+' input[id="noofabs'+i+'"]').length >0)?$(tblid+' input[id="noofabs'+i+'"]').val():0);
   
   if(xfinal==''){
	xmidstats++;					
	if(xerror==0 && xmidstats==xcount)
	  donesaving(xcount);
	else if(xerror > 0 && xmidstats==xcount)
	  donesaving(-1);   
	continue;   
   }
   
   $.ajax({
		  type: "POST",
		  url: xlink,
		  data: {opt:"'"+xopt+"'",sched:"'"+schedid+"'",stdno:"'"+xstdno+"'",cs:"'"+xcs+"'",exam:"'"+xexam+"'",grade:"'"+xgrade+"'",tgrade:"'"+xtgrade+"'",final:"'"+xfinal+"'",rex:"'"+xrex+"'",remark:"'"+xremark+"'",noabs:"'"+xnoabs+"'"},
		  //async:false,
		  dataType: "text",
		  success: function (result) 
		           {
				    var data = result.trim();
					if(data=='nodata') xerror++;
					
                    xmidstats++;
					
					if(xerror==0 && xmidstats==xcount)
					  donesaving(xcount);
				    else if(xerror > 0 && xmidstats==xcount)
					  donesaving(-1);
				   },
           error:function(){xmidstats++; xerror++; if(xmidstats==xcount){donesaving(-1);}}
	      });
   
  }//for
 }
 else
 {
  xmidstats = -1;
 } 
 
 
 if(xposteda==0 && xonperioda==1)
 {
  xopt='1.1';
  for(i=1;i<=xcount;i++)
  {
   var xstdno = $(tblid+' tr[id="stud'+i+'"]').attr('data-std');
   var xcs =  (($(tblid+' input[id="fcs'+i+'"]').length >0)?$(tblid+' input[id="fcs'+i+'"]').val():0);
   var xexam = (($(tblid+' input[id="fe'+i+'"]').length >0)?$(tblid+' input[id="fe'+i+'"]').val():0);
   var xgrade = (($(tblid+' input[id="fg'+i+'"]').length >0)?$(tblid+' input[id="fg'+i+'"]').val():0);
   var xtgrade = (($(tblid+' input[id="ftg'+i+'"]').length >0)?$(tblid+' input[id="ftg'+i+'"]').val():0);
   var xfinal = (($(tblid+' input[id="fin'+i+'"]').length >0)?$(tblid+' input[id="fin'+i+'"]').val():'');
   var xrex = (($(tblid+' input[id="rex'+i+'"]').length >0)?$(tblid+' input[id="rex'+i+'"]').val():'');
   var xremark = (($(tblid+' span[id="remark'+i+'"]').length >0)?$(tblid+' span[id="remark'+i+'"]').html():'');
   var xnoabs = (($(tblid+' input[id="noofabs'+i+'"]').length >0)?$(tblid+' input[id="noofabs'+i+'"]').val():0);
   
   if(xfinal==''){
	xfinstats++;					
	if(xerror==0 && xfinstats==xcount)
	  donesaving(xcount);
	else if(xerror > 0 && xfinstats==xcount)
	  donesaving(-1);   
	continue;   
   }
   
   $.ajax({
		  type: "POST",
		  url: xlink,
		  data: {opt:"'"+xopt+"'",sched:"'"+schedid+"'",stdno:"'"+xstdno+"'",cs:"'"+xcs+"'",exam:"'"+xexam+"'",grade:"'"+xgrade+"'",tgrade:"'"+xtgrade+"'",final:"'"+xfinal+"'",rex:"'"+xrex+"'",remark:"'"+xremark+"'",noabs:"'"+xnoabs+"'"},
		  //async:false,
		  dataType: "text",
		  success: function (result) 
		           {
				    var data = result.trim();
					if(data=='nodata') xerror++;
					
                    xfinstats++;
					
					if(xerror==0 && xfinstats==xcount)
					{
					 donesaving(xcount);	
				     console.clear();
					}  
				    else if(xerror > 0 && xfinstats==xcount)
					  donesaving(-1);
				   },
           error:function(){xfinstats++; xerror++; if(xfinstats==xcount){donesaving(-1);}} 
	      });
   
  }//for
 }
 else
 {
  xfinstats = -1;
 }
 
 }
 	  
function donesaving(x)
{
 if((xmidstats==-1 || xmidstats==x) && (xfinstats==-1 || xfinstats==x) && x!=-1)
 {
  
  var tblid = '#gradetable'+xactivetab;
  $(tblid).attr('data-altered','0');
  
  rem_antidepr();
  $.smallBox({
			  title: "Successfully Save Grade Sheet.",
			  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
			  color: "#739e73",
			  iconSmall: "fa fa-check bounce animated",
			  timeout: 10000                  
			 });
 }
 else if(x==-1)
 {
  rem_antidepr();
  $.smallBox({
			  title: "Failed to Save Grade Sheet.",
			  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
			  color: 'rgb(196, 106, 105)',
			  iconSmall: "fa fa-warning bounce animated",
			  timeout: 10000                  
			 });
  
 }	 
}


function passwordverification()
{
 var xlink = $('#mnuhome').attr('href');
     xlink = xlink.replace('home','home/passwordverify');
 
 $.SmartMessageBox({
                 title : "Verification Needed",
                 content : "Enter your Password For Verification",
                 buttons : "[Cancel][Ok]",                 
                 input : "password",
                 placeholder : "Enter your password here"
                 }, 
				 function(ButtonPress, Value) 
				 {
				   if (ButtonPress == "Cancel") 
				   {return 0;}
				   
				    $.ajax({
					        type: "POST",
					        url: xlink,
					        data: {pwd:"'"+Value+"'"},
					        async:false,
					        dataType: "text",
					        success: function (result) 
					                 {
					                  var data = result.trim();
					                  if(data=='nodata' || data=='invalid')
					                  {
					                   alert('Invalid Password');
                                       return 0;
					                  }
					                 }
					       }); 
                 });
 
}

function checkrequiredfields(xopt)
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid; 
 
 var onperioda = $(tblid).attr('data-onperioda');
 var onperiodb = $(tblid).attr('data-onperiodb');
 var valid=true;
 
 if(onperiodb==1 && xopt=='2.2')
 {
  $(tblid+' input[id*="mid"]').each(function(){
	 var xval = this.value;
     var xid = this.id;
     var count = xid.replace("mid","");
	 var noabs = $('#noofabs'+count).val();
	 
	 if(xval.trim()==""){valid=false;return false;}
	 if(xval.trim()=="FA" && (noabs==0 || noabs==undefined)){FA_detected=true; valid=false;return false;}
	 console.log(noabs);
   });
 }
 
 if(onperioda==1 && xopt=='2.1')
 {
  $(tblid+' input[id*="fin"]').each(function(){
	 var xval = this.value;      var xid = this.id;
     var xid = this.id;
     var count = xid.replace("fin","");
	 var noabs = $('#noofabs'+count).val();
	 
	 if(xval.trim()==""){valid=false;return false;}
	 if(xval.trim()=="FA" && (noabs==0 || noabs==undefined)){FA_detected=true; valid=false;return false;}
   
   });
  //$(tblid+' input[id*="rex"]').each(function(){var xval = this.value; if(xval.trim()==""){valid=false;return false;}});
 }
 
 console.log(valid); 
 return valid; 
}

function checkinputvalidation(x)
{
 var valid=false;
 
 if(x.trim()!="" && x.trim()!=" " && x.trim()!="@und" && x.trim()!="@UND")
 {
  $('#tblgradesys tbody tr').each(function(){var xgrade = $(this).attr('data-grade'); if(xgrade==x){valid=true;return false;}}); 
 }else{
  valid=true;
 }
 return valid;
}

function printgradesheet(xopt)
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid; 
 var xlink = $('#mnuhome').attr('href');

 var xtermid = $('#ayterm').val();
 var xcampusid = $('#campus').val(); 
 
 var xschedid = $(tblid).attr('data-pointer');
 var xfacultyid = $(tblid).attr('data-handler');
 
 if(xopt>=0)
     xlink = xlink.replace('home','faculty/printgradesheets/'+xschedid+'/'+xfacultyid+'/'+xopt);
 else
     xlink = xlink.replace('home','faculty/printgradesheets/'+xtermid+'/'+xcampusid+'/'+xopt);
	 
	 
 window.open(xlink, "Grade Sheet", "location=0,scrollbars=yes,status=no,toolbar=no,menubar=no,resizable=no,width=850,height=525");         
}

function printsubmitmonitor()
{
 var xlink = $('#mnuhome').attr('href');

 var xtermid = $('#ayterm').val();
 var xcampusid = $('#campus').val(); 
 
 xlink = xlink.replace('home','faculty/printsubmitmonitor/'+xtermid+'/'+xcampusid);
  
 window.open(xlink, "Monitoring of Grades Submission", "location=0,scrollbars=yes,status=no,toolbar=no,menubar=no,resizable=no,width=850,height=525");         
}

function showstudinfo(x)
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 var xlink = base_url+'faculty/manage/4';

 var xstd = $(tblid+' #stud'+x).attr('data-std');
 var xname = $(tblid+' #stud'+x).attr('data-name');
 var xgen = $(tblid+' #stud'+x).attr('data-gender');
 var xprog = $(tblid+' #stud'+x).attr('data-prog');
 var xpic  = $(tblid+' #stud'+x).attr('data-pic');
 
 $(tblid+' tbody tr').removeClass('bg-color-blueLight');
 
 if(xstd.trim()=="")
 {
  $('#stdnum').html('-');
  $('#stdname').html('-');
  $('#stdsex').html('-');
  $('#stdprog').html('-');
 
  return 0;
 }

 if($('#stdnum').html() == xstd){return 0; }	 
 
 $(tblid+' #stud'+x).addClass('bg-color-blueLight');
 $('#stdnum').html(xstd);
 $('#stdname').html(xname);
 $('#stdsex').html(xgen);
 $('#stdprog').html(xprog);
 
 if(xpic!==undefined && xpic!='')
  $('#stdpic').attr('src',xpic);
 else
 {	
  $.ajax({
		type: "GET",
		url: xlink,
		data: {idno:xstd},
		dataType: "text",
		success: function (result) 
				 {
				  var data = result.trim();
				  if(data=='nodata' || data=='invalid')
				   return 0;
				  else
				  {
				   $(tblid+' #stud'+x).attr('data-pic',data)
				   $('#stdpic').attr('src',data);
				  }
				 }
	   }); 	 
 }
}

function resetstudinfo()
{
 if(emptyimg.trim()!="")
 {
  $('#stdpic').attr('src',emptyimg);
 }
 else
 {
  emptyimg=$('#stdpic').attr('src');
 }
 
 $('#stdnum').html('-');
 $('#stdname').html('-');
 $('#stdsex').html('-');
 $('#stdprog').html('-');
}

function manageupload()
{
 var xid = xactivetab;
 var tblid = '#gradetable'+xid;
 var xschedid = $(tblid).attr('data-pointer');
 var xfacid = $(tblid).attr('data-handler');
 var xsecid = $(tblid).attr('data-target');
 var xown = $(tblid).attr('data-mgmt');
 
 var xonperiodb = $(tblid).attr('data-onperiodb');
 var xpostedb = $(tblid).attr('data-postedb');
 
 var xonperioda = $(tblid).attr('data-onperioda');
 var xposteda = $(tblid).attr('data-posteda');
 
 $('#div_usheetterm').hide();
 $('#usheetterm').html(''); 
 
 $('#uschedid').val(xschedid);
 $('#ufacid').val(xfacid);
 $('#usecid').val(xsecid);
 
 if($("#xpostm").length > 0 && xonperiodb==1 && xpostedb==0) 
 { 
  $('#usheetterm').append('<option value="Midterm" selected="selected">Midterm</option>');
  $('#div_usheetterm').show();
 }
 
 if($("#xpostf").length > 0 && xonperioda==1 && xposteda==0) 
 { 
  $('#usheetterm').append('<option value="Final" selected="selected">Final</option>');
  $('#div_usheetterm').show();
 } 
 
 if($("#xpostf").length > 0 && $("#xpostm").length <= 0  && xonperioda==1 && xposteda==0) 
 {
  $('#usheetterm').html(''); 
  $('#usheetterm').append('<option value="Final" selected="selected">Final</option>');
 } 
}

function spellcaster(input)
{
     var xallowed = [".doc",".docx",".xls",".xlsx",".pdf"];
	
     var x ='';
	 var xext='';
	 var xuplimit = 1024 * 2000;
	 
	 //alert(prevprofile);
	 if (input.files && input.files[0]) 
	 {
	    x = input.id;
	    var reader = new FileReader();
		reader.onload = function(e) 
		{
		    var filetype = input.files[0].type;
			var filesize = input.files[0].size;
			var filename = input.files[0].name;
			var xfiletype = '.'+filename.split('.')[filename.split('.').length-1];
			//alert(filename);
			//alert(xfiletype);
			//return 0;
			
			if($.inArray(xfiletype,xallowed) > -1 && filetype.indexOf('image/') < 0)
			{
			 if(filesize <= xuplimit)
			 {
			  xext= '.'+xfiletype;
			  
			 }
			 else
			 {
			 clearupload();
			 fileerror(2);
			 }
			}
			else
			{
			 clearupload();
			 fileerror(1);
			}
		}
		reader.readAsDataURL(input.files[0]);
	 }
	 else
	 {
	  clearupload();
	  fileerror(0);
	 }
}

function fileerror(a)
{
var xcontent = '';
if(a==0)
{xcontent = 'No file was detected.';}
else if(a==1)
{xcontent = 'File to be upload is not a valid file. Please select other file.';}
else if(a==2)
{xcontent = 'File size is greater than 2MB. Please reduce the file size before uploading.';}

if(a!='')
 {
 $.SmartMessageBox({
	 title : "<i class='fa fa-warning fa-2x' style='color:red;'></i> Image can't be upload!",
	 content : xcontent,
	 buttons : "[Ok]"
	 },function(ButtonPress, Value){
	   if (ButtonPress == "Ok") {return false;}
	});
 }
}

function clearupload()
{
 $('#ugradesheet').replaceWith('<input type="file" class="form-control" id="ugradesheet" name="ugradesheet" accept=".doc,.docx,.xls,.xlsx,.pdf" placeholder="File(Word,Excel,PDF)" required="true"  onchange="spellcaster(this);"/>');
}

$('document').ready(function(){
  $('body').on('keyup','.MessageBoxContainer [type="password"]',function(e){
	 if(e.keyCode=='13'){
		 $('#bot2-Msg2').click();
	 } 
  });	
});

//hiding a column in javascript
//$('td:nth-child(2)').hide();