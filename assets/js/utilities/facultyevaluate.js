var xmin = 0;
var xmax = 5;

function eval_save(opt){
	switch(opt){
		case 'all':
			var eid = $('#xid').val();
			var fid = $('#faculty').val();
			var sid = $('#schedule').val();
			var com = $('#eval_comment').val();
			var ans = '';
			$('.evalans').each(function(){
			 var xid = $(this).attr('id');
			 var qid = $(this).attr('data-ai');
			 var val = $(this).val();
			     ans = ((ans!='')?(ans+'|'):'')+qid+':'+val;
		   });
 		   $.ajax({
			  type: "POST",
			  url: base_url+'faculty_evaluation/mgmt/set',
			  data: {'xid':"'"+eid+"'",'sched':"'"+sid+"'",'faculty':"'"+fid+"'",'ans':"'"+ans+"'",'comment':"'"+com+"'"},
			  async:true,
			  dataType: "JSON",
			  success: function (rs){
						if(rs.success){
						   $('#xid').val(rs.EntryID);
						}
					   },
			   error:function(){
				   alert('Error!');
			   }
		    });
		break;
		case 'sub':
			var eid = $('#xid').val();
			var fid = $('#faculty').val();
			var sid = $('#schedule').val();
			var com = $('#eval_comment').val();
			var ans = '';
			$('.evalans').each(function(){
			 var xid = $(this).attr('id');
			 var qid = $(this).attr('data-ai');
			 var val = $(this).val();
			     ans = ((ans!='')?(ans+'|'):'')+qid+':'+val;
		   });
 		   $.ajax({
			  type: "POST",
			  url: base_url+'faculty_evaluation/mgmt/set',
			  data: {'xid':"'"+eid+"'",'sched':"'"+sid+"'",'faculty':"'"+fid+"'",'ans':"'"+ans+"'",'comment':"'"+com+"'"},
			  async:false,
			  dataType: "JSON",
			  success: function (rs){
						if(rs.success){
						   $('#xid').val(rs.EntryID);
						   return rs.EntryID;
						}
					   },
			   error:function(){
				   return false;
			   }
		    });
		break;
	}
	$('.btn-refresh').click();
}

$(document).ready(function(){
   $('body').on('click','.btn-refresh',function(){
	   $.ajax({
		  type: "POST",
		  url: base_url+'faculty_evaluation/mgmt/get/list',
		  data: {},
		  async:false,
		  dataType: "JSON",
		  success: function (rs){
					if(rs.success){
					   $('.tbl-list').find('tbody').html(rs.content);
					}
				   },
		   error:function(){
			   alert('Failed To Reload!');
		   }
		});
        rem_antidepr();
   });	
   
   $('body').on('click','.btn-evaluate',function(){
	    var tmpval = $(this).attr('data-entry');
		antidepressant();
		$('#xid').val($(this).attr('data-entry'));
	    $('#faculty').val($(this).attr('data-id'));
	    $('#schedule').val($(this).attr('data-sched'));
	    $('#eval_comment').val('');		
	   
		$.ajax({
		  type: "POST",
		  url: base_url+'faculty_evaluation/mgmt/get/eval',
		  data: {xid:tmpval},
		  async:false,
		  dataType: "JSON",
		  success: function (rs){
					if(rs.success){
					   $('.frm-eval').find('tbody').html(rs.content);  
					   $('#eval_comment').val(rs.comment);  
					   $('.tbl-list').addClass('hidden');	   
					   $('.frm-eval').removeClass('hidden');	
					   rem_antidepr();
					}
				   },
		   error:function(){
			   alert('Failed To Load!');
		   }
		});
   });
   
   $('body').on('click','.btn-submit',function(){
		antidepressant();
		$.ajax({
		  type: "POST",
		  url: base_url+'faculty_evaluation/mgmt/submit',
		  data: {},
		  async:true,
		  dataType: "JSON",
		  success: function (rs){
					if(rs.success){
					  window.location.replace(base_url+'grades');	
					}else{
					  alert('Failed To Submit');	
					}
				 },
		   error:function(){
			   alert('Problem Encountered While Submitting!');
		   }
		});
		rem_antidepr();
   });
   
   function validate(){
	   var valid = 1;
	   $('.evalans').each(function(){
                 console.log(parseFloat($(this).val()));
		 if($(this).val()=='' || parseFloat($(this).val())==0 || parseFloat($(this).val())==false){
		     valid = 0;	 
                     return valid;
		 }  
	   });
           return valid
   }
   
   $('body').on('click','.btn-submitthis',function(){
	    var xid = $('#xid').val();
        var isvalid = validate();
	    if(isvalid==0){
		  alert('Complete You Evaluation Before Submitting!');
		  return false;
		}else{           
	       var cmt   = $('#eval_comment').val();
	       var conf  = false;
           if(cmt.trim()=='' || cmt==' '){
		       conf = confirm('You didn`t give your comment! Do you want to proceed?');
		       if(conf==false){
                  return false;		  
               }
	       }

			antidepressant();
			var eid = $('#xid').val();
			var fid = $('#faculty').val();
			var sid = $('#schedule').val();
			var com = $('#eval_comment').val();
			var ans = '';
			$('.evalans').each(function(){
			 var qid = $(this).attr('data-ai');
			 var val = $(this).val();
				 ans = ((ans!='')?(ans+'|'):'')+qid+':'+val;
		    });
		    $.ajax({
			  type: "POST",
			  url: base_url+'faculty_evaluation/mgmt/set',
			  data: {'xid':"'"+eid+"'",'sched':"'"+sid+"'",'faculty':"'"+fid+"'",'ans':"'"+ans+"'",'comment':"'"+com+"'"},
			  dataType: "JSON",
			  success: function (rs){
						if(rs.success){
						   $('#xid').val(rs.EntryID);
						   xid = rs.EntryID;
						   if(xid=='new' || xid==false){
							   alert('Failed To Submit!');
							   rem_antidepr();
							   return false;			   
						   }
							
							$.ajax({
							  type: "POST",
							  url: base_url+'faculty_evaluation/mgmt/submit/'+xid,
							  data: {},
							  async:true,
							  dataType: "JSON",
							  success: function (rs){
										if(rs.success){
										  $('.btn-refresh').click();
			
										  $('.tbl-list').removeClass('hidden');	   
										  $('.frm-eval').addClass('hidden');	   	  
										  $('#xid').val('new');
										  $('#faculty').val('');
										  $('#schedule').val('');
										  $('.evalans').val('');
										  $('#eval_comment').val('');	
										}else{
										  alert('Failed To Submit');	
										}
			                            rem_antidepr();
									 },
							   error:function(){
								   alert('Problem Encountered While Submitting!');
			                       rem_antidepr();
							   }
							});
						}
			   },
			   error:function(){
				   alert('Failed To Submit');	
				   rem_antidepr();
			   }
			});
		}
   });
   
   $('body').on('click','.btn-save',function(){
	    antidepressant();
	    eval_save('all');
   });
   
   $('body').on('blur','.evalans,#eval_comment',function(){
	  var tmpval = $(this).val();
      if(tmpval==''){
		return false;  
	  }else{
		if(parseFloat(tmpval)==false || parseFloat(tmpval)=="NaN"){
		  alert('Invalid Input!');		  
		  $(this).val('');
          return false;
		}else{
	      //eval_save('all');
		}  
	  }	  
   });
  
   $('body').on('click','.evalopt',function(){
	  var tmpval = $(this).val();
	  var xid = $(this).attr('name');
               xid  = xid.replace('opt','a');
          $('#'+xid).val(tmpval);
          $('#'+xid).trigger('blur');
          console.log(xid+':'+tmpval);
   });
   
}); //end of ready