var history_path = [];
var current_path = 0;
var clipboard_path = "";
var clipboard_name = "";
var clipboard_proc = "copy";
var clipboard_dir  = false;

$(document).ready(function() {
 adaptivediv();
 $('.txtfile').val('');
 $('body').on('click','.table-parent',function(){
  var trchild = $(this).next('tr');
  if($(this).is('.active'))
  {
    $(trchild).addClass('hidden');
	$(this).removeClass('active');     
    $(this).find('.fa-folder-open').addClass('fa-folder');
    $(this).find('.fa-folder-open').removeClass('fa-folder-open');   
    $(trchild).addClass('hidden');    
    $(trchild).find('.table-child').addClass('hidden');    
    $(trchild).find('.table-parent').removeClass('active');    
    $(trchild).find('.table-parent').find('.fa-folder-open').addClass('fa-folder');
    $(trchild).find('.table-parent').find('.fa-folder-open').removeClass('fa-folder-open');
  }
  else
  {
	$(this).addClass('active');   
    $(this).find('.fa-folder').addClass('fa-folder-open');   
    $(this).find('.fa-folder').removeClass('fa-plus');   
    $(trchild).removeClass('hidden');      
  }	  
 });
 
 $('body').on('click','.btnfupload',function(){
    var xpath = $('.open-files').attr('data-path');
    $('#uploadmodal').find('.open-dir').val(xpath);
	$('#uploadmodal').modal('show');
 });
 
 $("#upform").submit(function(evt){	 
      evt.preventDefault();
	  var xpath = $('.main-dir').attr('data-path');
      var formData = new FormData($(this)[0]);
	  var xlink    = base_url+'fileeditor/manage/set/uploadfile/';
      $.ajax({
          url: xlink,
          type: 'POST',
          data: formData,
          async: false,
          cache: false,
          contentType: false,
          enctype: 'multipart/form-data',
          processData: false,
          success: function (rs) {
			$('#uploadmodal').find('#upform').reset();
			$('#uploadmodal').find('#upform').clear();
		    $('#uploadmodal').modal('hide');
          }
      });
      return false;
 });
 
 $('body').on('dblclick','.table-file',function(){
   $('.txtfile').val('');
   $('.success').removeClass('success');
   $('.imgpreview').addClass('hidden');
   if($(this).is('[data-editable="true"]'))
   {	   
	$(this).addClass('success');
	$('.file-selected').html($(this).attr('data-path')); 
	$('.txtfile').removeAttr('disabled');
	$('.txtfile').removeClass('hidden');
	get_file_content($(this).attr('data-path'));
	manage_buttons(1);
   }
   else
   {
	var xfilepath=$(this).attr('data-path');
	$(this).addClass('success');
	$('.file-selected').html($(this).attr('data-path')); 
	alertmsg('danger','File Selected Is NOT Editable!');
	$('.txtfile').attr('disabled',true);
	$('.txtfile').val('');
	if(xfilepath.indexOf('.jpeg')>0 || xfilepath.indexOf('.jpg')>0 || xfilepath.indexOf('.png')>0 || xfilepath.indexOf('.bmp')>0 || xfilepath.indexOf('.gif')>0)
	{
	 $('.txtfile').addClass('hidden');
	 $('.imgpreview').find('img').attr('src',$(this).attr('link-path'));
	 $('.imgpreview').removeClass('hidden');	
	}	
	manage_buttons(2);
   }
   
   if($(this).attr('link-path')!==undefined && $(this).attr('link-path')!='')
	 $('.btndownload').attr('href',$(this).attr('link-path'));
   else
	 $('.btndownload').attr('href',$('.btndownload').attr('default-url'));  
 
   $('.btnclose').removeClass('hidden');
});
 
 $('body').on('click','.btnsave',function(){
  var xpath = $('.file-selected').html();
  var xdata = $('.txtfile').val();
  if(xpath=='New File')
  {
   $('.lblmode').html('Save');
   $('#btnselectfile').addClass('hidden'); 
   $('#btnsavefile').removeClass('hidden'); 
   $('.divfilename').removeClass('hidden'); 
   $('.txtfilename').val(''); 
   if($('.open-files').attr('data-path')!='' && $('.open-files').attr('data-path')!=undefined)
    $('.open-dir').val($('.open-files').attr('data-path'));  
   else	   
    $('.open-dir').val($('.main-dir').attr('data-path'));  
   loadlist($('.open-dir').val(),'.open-files');
   $('#filemodal').modal('show');	 
  }
  else if(xpath!='' && xdata!=''){save_file_content(xpath); }	   
 });
 
 $('body').on('click','#btnsavefile',function(){
  var xpath = $('.open-dir').val();
  var xname = $('.txtfilename').val();
  var xdata = $('.txtfile').val();
  if(xpath.substr(xpath.length-1)!='/'){xpath = xpath+'/'; }
  if(xpath!='' && xname!='' && xdata!=''){save_file_content(xpath+xname,true); }	   
  $('#filemodal').modal('hide');	  	 
 });
 
 $('body').on('click','.btnnew',function(){
  $('.tbfile a').tab('show');
  $('.file-selected').html('New File'); 
  $('.txtfile').val('');
  $('.txtfile').removeAttr('disabled');
  $('.txtfile').removeClass('hidden'); 
  $('.imgpreview').addClass('hidden'); 
  $('.btndownload').attr('href',$('.btndownload').attr('default-url')); 
  $('.btnclose').removeClass('hidden');
  manage_buttons(1);
 });
 
 $('body').on('click','.btnclose',function(e){
  $('a[href="#xplore"]').tab('show');	 
  $('.file-selected').html('');
  $('.txtfile').val('');
  $('.txtfile').attr('disabled','true');
  $('.txtfile').removeClass('hidden');
  $('.imgpreview').addClass('hidden');
  $('.btnclose').addClass('hidden');  
  $('.btndownload').attr('href',$('.btndownload').attr('default-url'));
  manage_buttons(0);
 }); 

 $('body').on('click','.btnopen',function(){
  $('.lblmode').html('Open');
  $('#btnselectfile').removeClass('hidden'); 
  $('#btnsavefile').addClass('hidden'); 
  $('.divfilename').addClass('hidden'); 
  
  loadlist($('.main-dir').attr('data-path'),'.open-files');
  $('.open-dir').val($('.main-dir').attr('data-path'));
  $('#btnselectfile').attr('disabled',true);
  $('#filemodal').modal('show');	 
 });
 
 $('body').on('click','.btndelete',function(){
  var xpath = $('.file-selected').html();
  delete_file(xpath);
  $('.btnclose').click();
 });
 
 $('body').on('click','.btnfdel',function(){
  var xdir  = $('.open-files').attr('data-path');
  var xpath = $('.open-files .active').attr('data-path');
  delete_file(xpath);
  loadlist(xdir,'.open-files');
 });
 
 
 $('body').on('click','.btnfcopy',function(){
  if($('.open-files .active').length > 0)
  {
   clipboard_path = $('.open-files .active').attr('data-path');
   clipboard_name = $('.open-files .active').attr('data-name');
   clipboard_dir  = (($('.open-files .active').attr('data-sub')=='1')?true:false);
   clipboard_proc = "copy";
  }
  $('.btnfpaste').removeClass('disabled');
 });
 
 $('body').on('click','.btnfcut',function(){
  if($('.open-files .active').length > 0)
  {	  
   clipboard_path = $('.open-files .active').attr('data-path');
   clipboard_name = $('.open-files .active').attr('data-name');
   clipboard_dir  = (($('.open-files .active').attr('data-sub')=='1')?true:false);
   clipboard_proc = "cut";
  }
  $('.btnfpaste').removeClass('disabled');
 });
 
 $('body').on('click','.btnfpaste',function(){
  if(clipboard_name!="" && clipboard_name!=undefined)
  {
   var xfolder = $('.open-files').attr('data-path');
   paste_path(clipboard_path,xfolder);
   if(clipboard_proc=="cut")
   {	   
    clipboard_path = "";
    clipboard_name = "";
    clipboard_proc = "copy";
	clipboard_dir  = false;
    $('.btnfpaste').addClass('disabled');
   }
  } 
 });
 
 
 $('body').on('click','.btnshow',function(){
  $('.article_dir').removeClass('hidden');
  $('.article_main').addClass('col-sm-9 col-md-9 col-lg-10');
  $('.article_main').removeClass('col-sm-12 col-md-12 col-lg-12');
 });
 
 $('body').on('click','.btnhide',function(){
  $('.article_dir').addClass('hidden');
  $('.article_main').addClass('col-sm-12 col-md-12 col-lg-12');
  $('.article_main').removeClass('col-sm-9 col-md-9 col-lg-10');
 });	 
 
 $('body').on('click','.btnreload',function(){
  loadlist('Base Path','.main-files');	 
 });
 loadlist($('.main-dir').attr('data-path'),'.main-files');
 loadlist($('.main-dir').attr('data-path'),'.open-files');
 
 $('body').on('click','.btnback',function(){
  var count = history_path.length;
  var xpath = '';
  if(current_path>0){current_path--; }
  if(current_path<count)
  xpath = history_path[current_path];	  
  if(xpath!=''){loadlist(xpath,'.open-files'); }
 });
 
 $('body').on('click','.open-files tr',function(){
   $('.open-files .active').removeClass('active');
   $(this).addClass('active');
   $('#btnselectfile').removeAttr('disabled');
   if($(this).attr('data-sub')=='0'){$('.txtfilename').val($(this).attr('data-name')); }	   
 });
 
 $('body').on('dblclick','.open-files tr',function(){
  $('#btnselectfile').click();	 
 });
 
 $('body').on('click','#btnselectfile',function(){
  var xpath = $('.open-files .active').attr('data-path');
  if($('.open-files .active').attr('data-sub')==1)
  {
   $('.btnback').attr('data-path',$('.open-dir').val());
   loadlist(xpath,'.open-files');
   current_path++;
   $('.open-dir').val(xpath);
   $('#btnselectfile').attr('disabled',true);
  }
  else
  {
   if($('.open-files .active').attr('data-editable')=='1')
   {	   
    $('.file-selected').html(xpath); 
    $('.txtfile').removeAttr('disabled');
    $('.txtfile').removeClass('hidden');
    get_file_content(xpath);
    $('#filemodal').modal('hide');	 
    manage_buttons(1);  
   }	
  }	  
 });
 
 $('body').on('click','.btnfadd',function(){
  var xpath = $('.open-files').attr('data-path');
  var xfolder = prompt('Enter Folder Name','New Folder'); 
  if(xpath!='' && (xfolder!='' && xfolder!=null)){create_folder(xpath,xfolder); }
 });
 
 $('body').on('keyup','.open-dir',function(e){
   //console.log(e.keyCode); 
   if(e.keyCode==13)
   {
	var xpath = $('.open-dir').val();
    if(xpath.trim()==''){return false; }	
	loadlist(xpath,'.open-files');
    $('#btnselectfile').attr('disabled',true);
   }	   
 });
});

function adaptivediv()
{
 var screenheight = $(document).height();
 var adaptiveheight = (screenheight * 0.75);
 
 $('.adaptivediv').attr('style','height:'+adaptiveheight+'px !important;');	
}

function manage_buttons(opt)
{
 if(opt=='' || opt==undefined){opt=0; }
 $('.btnsave').addClass('disabled');
 $('.btndownload').addClass('disabled');	
 $('.btnpaste').addClass('disabled');		
 $('.btncut').addClass('disabled');	
 $('.btncopy').addClass('disabled');	
 $('.btndelete').addClass('disabled');		
 if(opt==1)
 {	 
  $('.btnsave').removeClass('disabled');
  $('.btndelete').removeClass('disabled');
 }
 else if(opt==2)
 { 
  $('.btndownload').removeClass('disabled');
  $('.btndelete').removeClass('disabled');
 }	 
}

function get_file_content(xpath)
{ 
 if(xpath==undefined){return false; }	
 antidepressant();
 $.ajax({
	type:'POST',
	url:base_url+'fileeditor/manage/get/open/',
	data: {path:xpath},
	dataType:'JSON',
	success: function (output) 
	{                
	 if(output.result)
	 {
      $('.txtfile').val(output.content);		 
	  $('.btnclose').removeClass('hidden');
	  $('.tbfile a').tab('show');
	  console.clear();
	 }
	 else
	  alertmsg('danger','BLIMEY!');
	 
	 rem_antidepr();			 
	},
	error: function (result) 
	{
	 alertmsg('danger','Error While Opening File!');
	 rem_antidepr();
	}
 });
}

function delete_file(xpath)
{
 if(xpath==undefined || xpath==''){return false; }	
 antidepressant();
 $.ajax({
	type:'POST',
	url:base_url+'fileeditor/manage/set/delete/',
	data: {path:xpath},
	dataType:'JSON',
	success: function (output) 
	{                
	 if(output.result)
	 {	 
	  alertmsg('delete','Successfully Delete File!');
	  $('[data-path="'+xpath+'"]').remove();
	  manage_buttons(0);
	  console.clear();
	 }
	 else
	  alertmsg('danger','Unable To Delete File!');
	 
	 rem_antidepr();			 
	},
	error: function (result) 
	{
	 alertmsg('danger','Error While Saving File!');
	 rem_antidepr();
	}
 });	
}

function paste_path(xpath,xfolder)
{ 
 if(xpath==undefined){return false; }	
 var xlink = base_url+'fileeditor/manage/set/paste/'+clipboard_proc;
 var xdestination = ((clipboard_dir==true && clipboard_proc=='copy')?(xfolder+clipboard_name):xfolder);
 antidepressant();
 $.ajax({
	type:'POST',
	url:xlink,
	data: {path:xpath,dpath:xdestination},
	dataType:'JSON',
	success: function (output) 
	{                
	 if(output.result)
	 {	 
	  loadlist(xfolder,'.open-files');
	  console.clear();
	 }
	 else
	  alertmsg('danger','BLIMEY!');
	},
	error: function (result) 
	{
	 alertmsg('danger','Error While Creating Folder!');
	 rem_antidepr();
	}
 });
}

function create_folder(xpath,xfolder)
{ 
 if(xpath==undefined){return false; }	
 var xlink = base_url+'fileeditor/manage/set/create/';
 var xnew = xpath+xfolder;
 antidepressant();
 $.ajax({
	type:'POST',
	url:xlink,
	data: {path:xnew},
	dataType:'JSON',
	success: function (output) 
	{                
	 if(output.result)
	 { 	 
	  loadlist(xpath,'.open-files');
	  console.clear();
	 }
	 else
	  alertmsg('danger','BLIMEY!');
	},
	error: function (result) 
	{
	 alertmsg('danger','Error While Creating Folder!');
	 rem_antidepr();
	}
 });
}

function save_file_content(xpath,xnew)
{ 
 if(xnew==undefined){xnew=false; }	
 if(xpath==undefined){return false; }	
 var xdata = $('.txtfile').val();
 if(xnew==true)
  var xlink = base_url+'fileeditor/manage/set/newsave/';
 else
  var xlink = base_url+'fileeditor/manage/set/save/';
	 
 antidepressant();
 $.ajax({
	type:'POST',
	url:xlink,
	data: {path:xpath,data:xdata},
	dataType:'JSON',
	success: function (output) 
	{                
	 if(output.result)
	 {
	  if(xnew==true){$('.file-selected').html(xpath); }	  
	  alertmsg('save','YOWZAH!');
	  console.clear();
	 }
	 else
	  alertmsg('danger','BLIMEY!');
	 
	 rem_antidepr();			 
	},
	error: function (result) 
	{
	 alertmsg('danger','Error While Saving File!');
	 rem_antidepr();
	}
 });
}

function loadlist(xpath,xtarget)
{
 if(xpath==undefined || xpath==''){return false;}
 if(xtarget==undefined || xtarget==''){return false;}
 if(xtarget=='.main-files')
   var xlink=base_url+'fileeditor/manage/get/directory_list/';
 else
   var xlink=base_url+'fileeditor/manage/get/directory_list/custom';
 
 if(xpath.substr(xpath.length - 1)!='/'){xpath = xpath+'/'; }
 antidepressant();
 $.ajax({
	type:'POST',
	url:xlink,
	data: {path:xpath},
	dataType:'JSON',
	success: function (output) 
	{                
	 if(output.result)
	 {
	  $(xtarget).html(output.content);
      if(xtarget=='.open-files')
	  {
	   $(xtarget).attr('data-path',xpath);
	   history_path[current_path]=xpath; 
      }	  
	  console.clear();
	 } 
	 else
	  alertmsg('danger','BLIMEY!');
	 
	 rem_antidepr();			 
	},
	error: function (result) 
	{
	 alertmsg('danger','Error While Loading Directory!');
	 rem_antidepr();
	}
 });
}