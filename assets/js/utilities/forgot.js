  var optobject = '';
  var uid  = '';
  var type = 0;
  
  runAllForms();
  
  $('body').ready(function(){
	  $('body').on('submit','#reset-form',function(e){
		e.preventDefault();
		var xlink=base_url+'forgot/txn/set/';
		var xdata;
		
		$('.optloading').addClass('hidden'); 
		$('.opterror').addClass('hidden');   
		
		if(validate_form())
        {
		 switch(optobject)
		 {
		  case 'optstyle0':
		   var uname = $('#username').val();
		   var email = $('#email').val();
		       xdata = {uname:uname,email:email};
			   xlink = xlink+'reset';
		  break;	 
		  case 'optstyle1':
		   var uid    = $('#idno').attr('data-uid');
		   var type   = $('#idno').attr('data-type');
		   var uname  = $('#idno').val();
		   var ans    = $('#answer').val();
		   var qid    = $('.question').attr('data-pid');
		   var amail  = $('#amail').val();
		   var notify = $('#emailme').prop('checked'); 
		       xdata  = {uid:uid,type:type,uname:uname,qid:qid,ans:ans,amail:amail,notify:notify};
			   xlink = xlink+'xreset';
		  break;
		 }
		 $('.optloading').removeClass('hidden'); 
		 $('.btnsubmit').attr('disabled',true);
		 $.ajax({
		   type:"POST",
           url:xlink,
		   data:xdata,
		   dataType:"JSON",
		   async:true,
		   success:function(rs){
			 $('.optloading').addClass('hidden'); 
			 $('.btnsubmit').removeAttr('disabled');
			 if(rs.success)
			  $('#main').html(rs.content);
			 else
			 {
			  $('.opterror').removeClass('hidden');   
			  $('.alert-content').html(rs.error);    
			 }
             console.clear();			 
		   },
		   error:function(err){
			 $('.optloading').addClass('hidden'); 
			 $('.btnsubmit').removeAttr('disabled');
			 $('.opterror').removeClass('hidden');   
			 $('.alert-content').html('Error on reseting your password.');   
		   }
		 });
	    }			
	 });
	 
	 $('body').on('click','.btnpass,.btninfo,.btnback',function(){
		 var btn       = $(this);
		     optobject = $(this).attr('data-target');
		 if(optobject!=undefined && optobject!='')
		 {  
	        $('.state-success').removeClass('state-success');  
		    $('.state-error').removeClass('state-error');  
			$('.optloading').removeClass('hidden'); 
		    $('.opterror').addClass('hidden');   
			$('.optstylesel').addClass('hidden'); 
			$('.optstyle0').addClass('hidden'); 
			$('.optstyle1').addClass('hidden'); 
		    $('.optadditional').addClass('hidden');
			$('.btnback').addClass('hidden'); 
			$('.btnnext').addClass('hidden'); 
			$('.btnsubmit').addClass('hidden'); 
		    $('.note').remove();
			setTimeout(function(){
			  $('.optloading').addClass('hidden'); 
			  $('.'+optobject).removeClass('hidden'); 
			  
			  $('input[type="text"]').val('');
			  $('input[type="checkbox"]').prop('checked',false);
			  
			  if(btn.is('.btnback')==false)
			  {	  
			   $('.btnback').removeClass('hidden');
			   if(btn.is('.btninfo'))
			   {	   
			    $('#idno').removeAttr('disabled');
				$('.btnnext').removeClass('hidden'); 
			   }
			   else
				$('.btnsubmit').removeClass('hidden'); 
			  }
			 },300);
		 }	 
	 });
	
     $('body').on('click','.btnnext',function(){
		var idno  = $('#idno').val();
		var xlink = base_url+'forgot/txn/get/question';
		$('.state-success').removeClass('state-success');  
		$('.state-error').removeClass('state-error');  
		$('.opterror').addClass('hidden');   
		$('.note').remove();
		
		if(idno.trim()=='')
		{
		 $('#idno').closest('.input').addClass('state-error');	
		 $('#idno').closest('.input').append('<div class="note note-error">You must enter a valid idno.</div>')
		 return false;
		}	
		
		$('.optloading').removeClass('hidden');
		$.ajax({
		 type:"POST",
         url:xlink,
		 data:{idno:idno},
		 dataType:"JSON",
		 async:true,
		 success:function(rs){
		   if(rs.success==true)
		   {
            $('#idno').attr('disabled',true);			   
		    $('#idno').attr('data-uid',rs.uid);
		    $('#idno').attr('data-type',rs.type);
			$('.question').attr('data-pid',rs.pid);
			$('.question').html(rs.param);
		    $('#amail').val(rs.email);
			if(rs.pid=='2' || rs.pid=='0')
			 $('#answer').datepicker({dateFormat: 'dd/mm/yy'});	
			else
             $('#answer').datepicker('destroy');
		 
		    $('.btnnext').addClass('hidden'); 
		    $('.optloading').addClass('hidden');
		    $('.optadditional').removeClass('hidden');
		    $('.btnsubmit').removeClass('hidden');  
		   }
           else
           {
		    $('.optloading').addClass('hidden');
		    $('#idno').closest('.input').addClass('state-error');	
			$('#idno').closest('.input').append('<div class="note note-error">'+rs.error+'</div>')
		   }
           console.clear();		   
          },
          error:function(err){
		    $('.optloading').addClass('hidden');
		    $('#idno').closest('.input').addClass('state-error');	
			$('#idno').closest('.input').append('<div class="note note-error">No Account Is Found With That Credential</div>')
		  }		  
		});
	 });	
	 
  });
  
  function validate_form()
  {
	$('.state-success').removeClass('state-success');  
	$('.state-error').removeClass('state-error');  
	$('.note').remove();
	switch(optobject)
	{
	 case 'optstyle0':
	  var uname = $('#username').val();
	  var email = $('#email').val();
	  if(uname.trim()=='' && email.trim()=='')
	  {
		$('.optstyle0').find('.input').removeClass('state-success');  
		$('.optstyle0').find('.input').addClass('state-error');  
		alert('Please enter either your email or password');
		return false;
	  }	  
	  return true;
     break;	 
	 case 'optstyle1':
	   var uid    = $('#idno').attr('data-uid');
	   var type   = $('#idno').attr('data-type');
	   var uname  = $('#idno').val();
	   var ans    = $('#answer').val();
	   var qid    = $('.question').attr('data-pid');
	   var amail  = $('#amail').val();
	   var notify = $('#emailme').prop('checked'); 
	   if(ans.trim()=='')
	   {
		 $('#answer').closest('.input').addClass('state-error');
         $('#answer').closest('.input').append('<div class="note note-error">You must complete all the required field</div>');		 
	   }	   
	   if(amail.trim()=='')
	   {
		 $('#amail').closest('.input').addClass('state-error'); 
         $('#answer').closest('.input').append('<div class="note note-error">You must complete all the required field</div>'); 
	   }	
       
       if($('.state-error').length>0)		   
	    return false;
	   else
		return true;
	 break;
    }
  }
  
  // Validation
  $(function () {
	  // Validation
	  $("#smart-form-register").validate({

		  // Rules for form validation
		  rules: {
			  username: {
				  required: true,
				  minlength: 3,
				  remote: {
					  url: thisurl,
					  type: "post"
				  }
			  },
			  email: {
				  required: true,
				  email: true
			  }
			  
		  },

		  // Messages for form validation
		  messages: {
			  username: {
				  required: 'Please enter your login',
				  remote: 'login ALREADY in use!'

			  },
			  email: {
				  required: 'Please enter your email address',
				  email: 'Please enter a VALID email address'
			  }                  
		  },

		  // Ajax form submition
		  submitHandler: function (form) {
			  $(form).ajaxSubmit({
				  success: function () {
					  $("#smart-form-register").addClass('submited');
				  }
			  });
		  },

		  // Do not change code below
		  errorPlacement: function (error, element) {
			  error.insertAfter(element.parent());
		  }
	  });

  });
  
