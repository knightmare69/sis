$(document).ready(function(){
	var xlink = $('a[title="Sign Out"]').attr('href');
	
	$('#studentno').change(function(){
		var studno = $(this).val();
		antidepressant();
		
		$.ajax({
	     type:"POST",
	     url:xlink.replace('logout','') + 'grades/txn/get/info',
	     data:{student:studno},
	     dataType: "JSON",
	     async:true,
		 success: function (result){
		  if(result.success)
		  {
			$('#details').html(result.xtra);
			$('#xterms').html(result.term);  
			$('#xgrade').html(result.content);
			if(result.summary)
			{
			 $('.clearfix').removeClass('hidden');				
			 $('.gwa').html(result.gwa);
			 $('.enrolled').html(result.enrolled);			
			 $('.earned').html(result.earned);				
			 $('.cqpa1').html(result.cqpa1);				
			 $('.cqpa2').html(result.cqpa2);			
			}
			else
             $('.clearfix').addClass('hidden');
            
			var termid='';
			if($('#optterms').is(':visible'))
             termid=$('#optterms').val();
		    else
			 termid=$('.list-group .active').attr('id');	
		    
			if(termid!='' && termid!=' ' && termid!=undefined && studno!='' && studno!=' ' && studno!=undefined)
			  load_grades(studno,termid); 
			else
			  rem_antidepr();
            
            //console.clear();			
		  }
		  else
		  {
		   alert('Failed to load data');
		   rem_antidepr();
		  }
		 },
		 error: function(xerror)
		 {
		  alert('Failed to load data');
		  rem_antidepr();
		 }
		});
	});
	
	$('body').on('click','.list-group-item:not(.list-group-item-success), #optterms option',function(){
		var termid = $(this).attr('id');
		var studno = $('#studentno').val();
		
		$('.list-group-item:not(.list-group-item-success)').removeClass('active');
		$(this).addClass('active');
		
		load_grades(studno,termid);
	});
	
	$('#optterms').change(function(){
		var termid = $(this).val();
		var studno = $('#studentno').val();
		
		$('.list-group-item:not(.list-group-item-success)').removeClass('active');
		$('#'+termid).addClass('active');
		
		load_grades(studno,termid);
	});
	
	$('body').on('click','.btn-print',function()
	{
     var studno = $('#studentno').val();
	 var termid = $('#xterms .active').attr('id');
	 var url = base_url+'grades/printg/'+studno+'/'+termid;
     window.open(url, "Report of Grades", "location=0,scrollbars=yes,status=no,toolbar=no,menubar=no,resizable=no,width=850,height=525");         
	});
	
	$('body').on('click','#btnselectitem',function(){
	 var studno = $('.select-list .info').attr('id');
	 var studname = $('.select-list .info').find('.studname').html();
	 
	 $('#studentfilter').attr('data-id',studno);
	 $('#studentfilter').val(studname);
	 $('#studentno').val(studno);
	 $('#studentno').change();
	 $('#selectmodal').modal('hide');
	});
       
    $('#optterms').trigger('change');
});

function load_grades(studno,termid)
{
 if(studno=='' || studno==' ' || studno==undefined || termid=='' && termid==undefined){return false;}
 antidepressant();
 $.ajax({
	 type:"POST",
	 url:base_url + 'grades/txn/get/grades',
	 data:{student:studno, term:termid},
	 dataType: "JSON",
	 async:true,
	 success: function (result){
	  if(result.success)
	  {
		$('#details').html(result.xtra); 
		$('#xgrade').html(result.content);
		if(result.summary)
		{
		 $('.clearfix').removeClass('hidden');				
		 $('.gwa').html(result.gwa);
		 $('.enrolled').html(result.enrolled);			
		 $('.earned').html(result.earned);				
		 $('.cqpa1').html(result.cqpa1);				
		 $('.cqpa2').html(result.cqpa2);			
		}
		else
		 $('.clearfix').addClass('hidden');
        
		//console.clear();			
	  }
	  else
	   alert('Failed to load data');
	  rem_antidepr();
	 },
	 error: function(xerror)
	 {
	  alert('Failed to load data');
	  rem_antidepr();
	 }
 });	
}