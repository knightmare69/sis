

$(document).ready(function() {
	var isloaded = $('[data-isloaded]').attr('data-isloaded');
    //alert(location.hash);
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

	/*
	 * Fixed table height
	 */
	
	tableHeightSize();
	
	$(window).resize(function() {
	  tableHeightSize()
	})
	
	function tableHeightSize() {
		var tableHeight = $(window).height() - 212;
		$('.table-wrap').css('height', tableHeight + 'px');
	}
	
	/*
	 * LOAD INBOX MESSAGES
	if(isloaded=='false')
    { 
     loadInbox();	
	}
	*/
	
	function loadInbox() 
	{
	 //loadURL("view/email/inbox.php", $('#inbox-content > .table-wrap'))
	 var url = $(".inbox-load").attr('id');
	 loadURL(url, $('#inbox-content > .table-wrap'))
	}

	/*
	 * Buttons (compose mail and inbox load)
	 */
	$(".inbox-menu").click(function(){
	  var url  = $(this).attr('id');
	  var trgt = $(this).attr('data-target');
	  var lbl  = $(this).text();
	  var cls  = $(this).attr('class');
	  var xcls = ((cls!='' && cls!=undefined)? cls.replace(' inbox-menu','-mini'):'');
	      cls  = ((cls!='' && cls!=undefined)? cls.replace(' inbox-menu',''):'');
	  $('[data-page]').attr('data-page',0);
	  $('[data-page]').attr('data-target',trgt);
	  $('.inbox-menu-label').html(lbl);
	  $('.inbox-menu-lg').find('li').removeClass('active');
	  $('.inbox-menu-sm').find('.fa-check').remove();
	  
	  if(cls!='')
	  {	  
	   $('.'+cls).closest('li').addClass('active');
	   $('.'+xcls).prepend('<i class="fa fa-check"></i>');
	  }
	  
	  loadURL(url, $('#inbox-content > .table-wrap'));
	  if(trgt=='inbox')
	   window.history.pushState({},'',base_url+'inbox');
	  else
	   window.history.pushState({},'',base_url+'inbox?page='+trgt);
	});

	// compose email
	$("#compose-mail").click(function(){
	 var url = $("#compose-mail").attr('data-info');
	 loadURL(url, $('#inbox-content > .table-wrap'));
	 window.history.pushState({},'',base_url+'inbox');
	})
    
	
	$('body').on('click','.btn-next,.btn-prev',function(){
		var proc = $(this).attr('data-process');
		var trgt = $('[data-page]').attr('data-target');
		var url  = $('.inbox-menu-lg').find('[data-target="'+trgt+'"]').attr('id');
		var page = $('[data-page]').attr('data-page');
		    page = parseInt(page);
		
		if(proc=='-')
		  page = ((proc=='-' && page>0)? (page-1) : 0);
		else
		  page = page+1;
			
	 	
	   loadURL(url+'/'+page, $('#inbox-content > .table-wrap'));
	   $('[data-page]').attr('data-page',page);
	});	
	
	$('body').on('click','.deletebutton',function(){
	  var target = $('[data-page]').attr('data-target');
	  var chkmsg = $('.chkmsg:checked');
	  var mid    = [];
	  //alertmsg('warning','Checked:'+chkmsg.length);	
	  if(chkmsg.length>0)
	  {	  
	   for(var i=0,len=chkmsg.length;i<len;i++)
	   {
		 var xid = $(chkmsg[i]).closest('tr').attr('id');  
		 if(xid!=undefined)
		 {
		  mid[i]= xid.replace('msg','');
		 }	 
	   }
	   
	   if(mid.length>0)
	   {
		 $.ajax({
		   type: "POST",         
		   url: base_url+'inbox/txn/set/delete',
		   data: {mid:mid},
		   dataType: "JSON",
		   async:true,
		   success: function (rs){
			          if(rs.success)
					  {	 
					   alertmsg('success','Success');
					  } 
					  else
					   alertmsg('danger','Failed to delete');
				      
					  $('.inbox-menu-lg').find('[data-target="'+target+'"]').click();
					},
		   error: function(err){
				  alertmsg('danger','Error While Sending');
				 }	   
		 });
	   } 	   
      } 	   
	});
});	
	

