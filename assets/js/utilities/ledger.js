$('document').ready(function(){
  $('#studentno').change(function(){
	var studno = $(this).val();
	antidepressant();
	 
	$.ajax({
	  type:"POST",
	  url:base_url+'ledger/data/info',
	  data:{student:studno},
	  dataType: "JSON",
	  async:true,
	  success: function (result){
	   if(result.success)
	   {
	    $('#details').html(result.content);
	    load_data(studno);
	   }
	   else
	   {
		alertmsg('danger','Failed To Load Data!');   
	    rem_antidepr();
	   }   
	  },
      error:function(result){
	   alertmsg('danger','Failed To Load Data!');
	   rem_antidepr();
	  }
	});		 
  });
	
  $('body').on('click','#btnselectitem',function(){
    var studno = $('.select-list .info').attr('id');
    var studname = $('.select-list .info').find('.studname').html();
 
    $('#studentfilter').attr('data-id',studno);
    $('#studentfilter').val(studname);
    $('#studentno').val(studno);
    $('#studentno').change();
    $('#selectmodal').modal('hide');
  });
	

});

function load_data(studno)
{
 if(studno===undefined){rem_antidepr(); return false; }
 antidepressant();
 $.ajax({
	type:"POST",
	url:base_url+'ledger/data/data',
	data:{student:studno},
	dataType: "JSON",
	async:true,
	success: function (result){
	 if(result.success)
	  $('.ledger-data').html(result.content);
	 else
	  alertmsg('danger','Failed To Load Data!');   
	
	 rem_antidepr();
	},
    error:function(result){
	  alertmsg('danger','Failed To Load Data!');
	  rem_antidepr();
	}
 });		
}