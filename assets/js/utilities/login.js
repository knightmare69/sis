 var currentURL = window.location.href;
 runAllForms();

 $(function() {
	// Validation
	$("#login-form").validate({
	   // Rules for form validation
	   rules : {
		  username : {
			 required : true
		  },
		  email : {
			 required : true,
			 email : true
		  },
		  bday : {
			 required : true,
			 email : true
		  },
		  password : {
			 required : true,
			 minlength : 3,
			 maxlength : 20
		  }
	   },

	   // Messages for form validation
	   messages : {
		  username : {
			 required : 'Please enter your username or email address'
		  },
		  bday : {
			 required : 'Please enter your birthday'
		  },
		  email : {
			 required : 'Please enter your email address',
			 email : 'Please enter a VALID email address'
		  },
		  password : {
			 required : 'Please enter your password'
		  }
	   },

	   // Do not change code below
	   errorPlacement : function(error, element) {
		  error.insertAfter(element.parent());
	   }
	});
 });
 
 $('body').ready(function(){
	if(currentURL.includes(base_url)==false){
          window.location.replace(base_url);
        }
 
	$('#login-form').each(function() { this.reset() });
	$('#login-form').find('input').val('');
        $('#login-form').find('[name="username"]').focus();
	
	$('body').on('change','.input',function(){
		$('.alert').addClass('hidden');
		$('.alert').find('.alert-content').html('');
        $('#login-form').find('.input').removeClass('state-success');
        $('#login-form').find('.input').removeClass('state-error');
	});	
	
	$('body').on('blur','.input',function(){
		$('.alert').addClass('hidden');
		$('.alert').find('.alert-content').html('');
    });	
	
	$('body').on('click','.btnsend',function(e){
	  var xname  = $('#txtname').val();
	  var xstdno = $('#txtstdno').val();
	  var xmsg   = $('#txtmsg').val();
	  var xdata  = $('#frmcontact').serialize();	
	  if(xname=='' || xname==' ' || xstdno=='' || xstdno==' ' || xmsg==''){
		  alert('Please fill up the form first before sending!');
	  }else{
		//return false;
		$.ajax({
		 type:"POST",
         url:base_url+'login/contactus/',
		 data:xdata,
		 dataType:"JSON",
		 async:true,
		 success:function(rs){
			     if(rs.success){
				   $('#contactmodal').modal("hide");
				   $('#contactmodal').find('input').val('');
				   $('#contactmodal').find('textarea').val('');
                 }else{ 	 
			       alert('Failed To Sent Email');
			     }
		 },
		 error:function(e){
			 alert('Failed To Sent Email');
		 }
		});		 
	  }
	});	
	
	$('body').on('submit','#login-form',function(e){
	  e.preventDefault();
	  
	  var action = $('#login-form').attr('action');
      var xdata  = $('#login-form').serialize();
	  var btn = $('.btnsubmit');
	  btn.attr('data-loading-text','<i class="fa fa-refresh fa-spin"></i> Signing In...');
	  btn.button('loading');
		
	  $.ajax({
		 type:"POST",
         url:action,
		 data:xdata,
		 dataType:"JSON",
		 async:true,
		 success:function(rs){
			     if(rs.success)
				   window.location.href=rs.content;
                 else
				 { 	 
                   $('#login-form').find('.input').removeClass('state-success');
                   $('#login-form').find('.input').addClass('state-error');
				   $('.alert').removeClass('hidden');
				   $('.alert').find('.alert-content').html(rs.error);
				 }  
				 setTimeout(function(){btn.button('reset'); }, 1500);
		        },
		 error:function(err){
				 $('#login-form').find('.input').removeClass('state-success');
				 $('#login-form').find('.input').addClass('state-error');
				 $('.alert').removeClass('hidden');
				 $('.alert').find('.alert-content').html('Error while validating');
				 setTimeout(function(){btn.button('reset'); }, 1500);
		       }		
	  });
	  //console.clear();
	}); 
	
 });