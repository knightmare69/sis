// DO NOT REMOVE : GLOBAL FUNCTIONS!
var prevprofile='';
var xuplimit = 1024 * 2000;
var xuplimith = 500;
var xuplimitw = 500;
var xajax = 0;

function spellcaster(input,xtarget)
{
     var x ='';
	 var xext='';
	 if(prevprofile =='')
	 {prevprofile = $('#'+xtarget).attr('src');}
	 //alert(prevprofile);
	 if (input.files && input.files[0]) 
	 {
	    x = input.id;
	    var reader = new FileReader();
		reader.onload = function(e) 
		{
		    var filetype = input.files[0].type;
			var filesize = input.files[0].size;
			
			if(filetype.indexOf('image/') == 0)
			{
			 if(filesize <= xuplimit)
			 {
			  xext= '.'+filetype.replace('image/','');
			  
			  var str= e.target.result;
			  $('#temp_pic').attr('src', e.target.result);
			  var temp = document.getElementById('temp_pic');
			  var fileheight = temp.height;
			  var filewidth = temp.width;
			
    		  if(fileheight<=xuplimith && filewidth<=xuplimitw)
			  {
			   $('#'+xtarget).attr('src', e.target.result);
			   $('#xpic_upload').removeAttr('disabled');
			  }
			  else
			  {
			   clearupload(x);
			   imgerror(3);
			  }
			 }
			 else
			 {
			 clearupload(x);
			 imgerror(2);
			 }
			}
			else
			{
			 clearupload(x);
			 imgerror(1);
			}
		}
		reader.readAsDataURL(input.files[0]);
	 }
	 else
	 {
	  clearupload(x);
	  imgerror(0);
	 }
}

function imgerror(a)
{
var xcontent = '';
if(a==0)
{xcontent = 'No file was detected.';}
else if(a==1)
{xcontent = 'File to be upload is not an image file. Please select an image file.';}
else if(a==2)
{xcontent = 'Image size is greater than 2MB. Please reduce the file size before uploading.';}
else if(a==3)
{xcontent = 'Image height or width is greater than 500x500. Resizing the image to 500x500 or lower.';}

if(a!='')
 {
 $.SmartMessageBox({
	 title : "<i class='fa fa-warning fa-2x' style='color:red;'></i> Image can't be upload!",
	 content : xcontent,
	 buttons : "[Ok]"
	 },function(ButtonPress, Value){
	   if (ButtonPress == "Ok") {return false;}
	});
 }
}

function clearupload(x)
{
 $('#'+x).replaceWith('<input id="xprofile_up" name="pic_file" type="file" onchange="spellcaster(this,'+"'xprofile'"+');" accept="image/*" />');
 $('#temp_pic').replaceWith('<img id="temp_pic" class="img-responsive hidden">');
 $('#xprofile').attr('src',prevprofile);
 $('#xpic_upload').attr('disabled','disabled');
 prevprofile ='';
}

function editprofile()
{
 //alert('uy');
 $("#protab a:last").tab("show");
 $("#userSettings li:eq(1) a").tab("show");
}

function xdisarm()
{
 $('.editable').editable('toggleDisabled'); //editable('option', 'disabled', true);//
 $('.editable').addClass('editable-disabled');
 
 $('#familybackground input[type="text"]').attr('onkeypress','return false;');
 $('#familybackground input[type="text"]').attr('onkeydown','return false;');
 $('#familybackground input[type="text"]').attr('onkeyup','return false;');
 
 $('#educationalbg input[type="text"]').attr('onkeypress','return false;');
 $('#educationalbg input[type="text"]').attr('onkeydown','return false;');
 $('#educationalbg input[type="text"]').attr('onkeyup','return false;');
}

function updateusername()
{
  var thisurl = $('#thyHome').attr('href') + '/username'; 
  $.ajax({
		  url: thisurl,
		  dataType: "text",
		  success: function (result) 
				   {
				    var strresponse = result;
					if(strresponse != 'nochange')
					{$('#username').html(strresponse);}
				   }
		});
}

function saveallfamily()
{
 if($('#xform_parent').length>0)
  saveparentinfo();
  
 if($('#xform_father').length>0)
  savefatherinfo();
  
 if($('#xform_mother').length>0)
  savemotherinfo();
  
 if($('#xform_guardian').length>0)
  saveguardianinfo();
  
 if($('#xform_emergency').length>0)
  saveemergencyinfo();
  
 if($('#xform_spouse').length>0)
  savespouseinfo();

}

function savealleduc()
{
 if($('#xform_elem').length>0)
  saveeleminfo();
  
 if($('#xform_hs').length>0)
  savehsinfo();
  
 if($('#xform_college').length>0)
  savecollegeinfo();
  
 if($('#xform_vocational').length>0)
  savevocationalinfo();
}


function saveparentinfo()
{
 antidepressant();
 $('#xform_parent input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function saveeleminfo()
{
 antidepressant();
 $('#xform_elem input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function savehsinfo()
{
 antidepressant();
 $('#xform_hs input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function savecollegeinfo()
{
 antidepressant();
 $('#xform_college input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}
function savevocationalinfo()
{
 antidepressant();
 $('#xform_vocational input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function savefatherinfo()
{
 antidepressant();
 $('#xform_father input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function savemotherinfo()
{
 antidepressant();
 $('#xform_mother input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function saveguardianinfo()
{
 antidepressant();
 $('#xform_guardian input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function saveemergencyinfo()
{
 antidepressant();
 $('#xform_emergency input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function savespouseinfo()
{
 antidepressant();
 $('#xform_spouse input[type="text"]').each(
  function()
  {
   var inp = $(this);
   var xfld = inp.attr('name');
   var xval = inp.val();
   
   if(xval!='' && xfld!='')
    {
	 xajax++;
 	 savedata(xfld,xval);
	}
  }
 );
}

function savedata(xfield,xdata)
{
 var xurl = $('#thyHome').attr('href') + '/record'; 
 $.ajax({
		  type: "POST",         
          url: xurl,
		  data: {name:xfield , value:xdata},
		  dataType: "text",
		  success: function (result) 
				   {
				    if(xajax>0)
					 xajax--;
				    
					if(xajax==0)
					{
					 rem_antidepr();
					 $.smallBox({
							   title: "Success! Your data has been save.",
							   content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
							   color: "#739E73",
							   iconSmall: "fa fa-check bounce animated",
							   timeout: 2000                  
							   });
					}
				   },
		  error: function (result) 
				   {
				    alert('Something went wrong please report this to administrator.');
				    rem_antidepr();
				   }
		});
}
 



$(document).ready(function () {

    pageSetUp();
    
	var thisurl = $('#thyHome').attr('href') + '/record'; 
    var $validator = $("#palitpwd").validate({

        rules: {
            pwd: { required: true,
                minlength: 3,
                maxlength: 20
            },
            npwd: { required: true,
                minlength: 5,
                maxlength: 20
            },
            cpwd: { required: true,
                minlength: 5,
                maxlength: 20,
                equalTo: '#npwd'
            }
        },

        messages: {
            pwd: { required: "Please enter current your password",
                minlength: "Password must be greater than 5 characters but not more than 20 chars"
            },
            npwd: { required: "Please enter your new password",
                minlength: "Password must be greater than 5 characters but not more than 20 chars"
            }
                  ,
            cpwd: {
                required: 'Please enter your password one more time',
                equalTo: 'Please enter the same password as above'
            }
        },

        // Do not change code below
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }

    });
    
    /*
    * X-Editable
    */

    (function (e) {
        "use strict";
        var t     = function(e){this.init("address", e, t.defaults)};
		var fname = function(e){this.init("fname",e,fname.defaults)};
		var mname = function(e){this.init("mname",e,mname.defaults)};
		
        e.fn.editableutils.inherit(t, e.fn.editabletypes.abstractinput);
        e.fn.editableutils.inherit(fname, e.fn.editabletypes.abstractinput);
        e.fn.editableutils.inherit(mname, e.fn.editabletypes.abstractinput);

        e.extend(t.prototype, {
            render: function () {
                this.$input = this.$tpl.find("input")
            },
            value2html: function (t, n) {
                if (!t) {
                    e(n).empty();
                    return
                }
				
				var c,s,b;
				
                var r = e("<div>").text(t.address).html() + 
		         " "  + e("<div>").text(t.street).html() +
		                " "  + e("<div>").text(t.brgy).html() +
		                " "  + e("<div>").text(t.city).html() +
						", " + e("<div>").text(t.prov).html() + 
		                ", " + e("<div>").text(t.zip).html();
                e(n).html(r)
            },
            html2value: function (e) {
			    //alert('html:'+e);
                return null
            },
            value2str: function (e) {
                var t = "";
                if (e)
                    for (var n in e)
                        t = t + n + ":" + e[n] + ";";
                return t
            },
            str2value: function (e) {
			    //alert('str2value'+e);
                return e
            },
            value2input: function (e) {
                if (!e)
                    return;
                this.$input.filter('[name="address"]').val(e.address);
                this.$input.filter('[name="street"]').val(e.street);
                this.$input.filter('[name="brgy"]').val(e.brgy);
                this.$input.filter('[name="city"]').val(e.city);
                this.$input.filter('[name="prov"]').val(e.prov);
                this.$input.filter('[name="zip"]').val(e.zip);
            },
            input2value: function () {
                return {
				    address: this.$input.filter('[name="address"]').val(),
                    street: this.$input.filter('[name="street"]').val(),
                    brgy: this.$input.filter('[name="brgy"]').val(),
                    city: this.$input.filter('[name="city"]').val(),
                    prov: this.$input.filter('[name="prov"]').val(),
                    zip: this.$input.filter('[name="zip"]').val()
                }
            },
            activate: function () {
                this.$input.filter('[name="address"]').focus()
            },
            autosubmit: function () {
                this.$input.keydown(function (t) {
                    t.which === 13 && e(this).closest("form").submit()
                })
            }
        });
		
        t.defaults = e.extend({}, e.fn.editabletypes.abstractinput.defaults, {
            tpl: '<div class="editable-address"><label><span>Addr: </span><input type="text" name="address" class="form-control input-sm"></label></div>'+
			     '<div class="editable-address"><label><span>Street: </span><input type="text" name="street" class="form-control input-sm"></label></div>'+
			     '<div class="editable-address"><label><span>Brgy: </span><input type="text" name="brgy" class="form-control input-sm"></label></div>'+
			     '<div class="editable-address"><label><span>City: </span><input type="text" name="city" class="form-control input-sm"></label></div>'+
			     '<div class="editable-address"><label><span>Prov: </span><input type="text" name="prov" class="form-control input-sm"></label></div>'+
			     '<div class="editable-address"><label><span>ZipCode: </span><input type="text" name="zip" class="form-control input-sm"></label></div>',
            inputclass: ""
        });
		
        fname.defaults = e.extend({}, e.fn.editabletypes.abstractinput.defaults, {
            tpl: '<div class="editable-address"><label><span>Last: </span><input type="text" name="lname" class="form-control input-sm" placeholder="Lastname"></label></div>'+
                 '<div class="editable-address"><label><span>First: </span><input type="text" name="fname" class="form-control input-sm" placeholder="Firstname"></label></div>'+
			     '<div class="editable-address"><label><span>Middle: </span><input type="text" name="mname" class="form-control input-sm" placeholder="Middlename"></label></div>',
            inputclass: ""
        });
		
        mname.defaults = e.extend({}, e.fn.editabletypes.abstractinput.defaults, {
            tpl: '<div class="editable-address"><label><span>Last: </span><input type="text" name="lname" class="form-control input-sm" placeholder="Lastname"></label></div>'+
                 '<div class="editable-address"><label><span>First: </span><input type="text" name="fname" class="form-control input-sm" placeholder="Firstname"></label></div>'+
                 '<div class="editable-address"><label><span>Middle: </span><input type="text" name="mname" class="form-control input-sm" placeholder="Middlename"></label></div>'+
			     '<div class="editable-address"><label><span>Maiden: </span><input type="text" name="maname" class="form-control input-sm" placeholder="Maidenname"></label></div>',
            inputclass: ""
        });
		
        e.fn.editabletypes.address = t;
        e.fn.editabletypes.fname   = fname;
        e.fn.editabletypes.mname   = mname;
    })(window.jQuery);

    //ajax mocks
    $.mockjaxSettings.responseTime = 500;

    $.mockjax({
        url: '/post',
        response: function (settings) {
            log(settings, this);
        }
    });



    $.mockjax({
        url: '/error',
        status: 400,
        statusText: 'Bad Request',
        response: function (settings) {
            this.responseText = 'Please input correct value';
            log(settings, this);
        }
    });

    $.mockjax({
        url: '/csgroups',
        response: function (settings) {
            this.responseText = [{
                value: 1,
                text: 'Single'
            }, {
                value: 2,
                text: 'Married'
            }, {
                value: 3,
                text: 'Separated'
            }, {
                value: 4,
                text: 'Widow/Widower'
            }];
            log(settings, this);
        }
    });

    $.mockjax({
        url: '/relgroups',
        response: function (settings) {
            var rel = $('#relopt').html();
			var rel_arr = rel.split("@");
			var opt = new Array()
			//alert(rel);
			for(i=0; i < rel_arr.length; i++)
			{
			 if(rel_arr[i]!='')
			 {
			  var str_val = rel_arr[i].split('|');
			  opt[i] = {value:str_val[0],text:str_val[1]};
			  //alert(str_val[0]+'|'+str_val[1]);
			 }
			}
			this.responseText = opt;
            log(settings, this);
        }
    });

    $.mockjax({
        url: '/natgroups',
        response: function(settings) 
		{
            var nat = $('#natopt').html();
			var nat_arr = nat.split("@");
			var opt = new Array()
			//alert(rel);
			for(i=0; i < nat_arr.length; i++)
			{
			 if(nat_arr[i]!='')
			 {
			  var str_val = nat_arr[i].split('|');
			  opt[i] = {value:str_val[0],text:str_val[1]};
			  //alert(str_val[0]+'|'+str_val[1]);
			 }
			}
			this.responseText = opt;
            log(settings, this);
        }
    });

    //TODO: add this div to page
    function log(settings, response) {
        var s = [],str;
		
        s.push(settings.type.toUpperCase() + ' url = "' + settings.url + '"');
        for (var a in settings.data) {
            if (settings.data[a] && typeof settings.data[a] === 'object') {
                str = [];
                for (var j in settings.data[a]) {
                    str.push(j + ': "' + settings.data[a][j] + '"');
                }
                str = '{ ' + str.join(', ') + ' }';
            } else {
                str = '"' + settings.data[a] + '"';
            }
            s.push(a + ' = ' + str);
        }
        s.push('RESPONSE: status = ' + response.status);

        if (response.responseText) {
            if ($.isArray(response.responseText)) {
                s.push('[');
                $.each(response.responseText, function (i, v) {
                    s.push('{value: ' + v.value + ', text: "' + v.text + '"}');
                });
                s.push(']');
            } else {
                s.push($.trim(response.responseText));
            }
        }
        s.push('--------------------------------------\n');
        $('#console').val(s.join('\n') + $('#console').val());
    }

    /*
    * X-EDITABLES
    */

    if (window.location.href.indexOf("?mode=inline") > -1) {
        $('#inline').prop('checked', true);
        $.fn.editable.defaults.mode = 'inline';
    } else {
        $('#inline').prop('checked', false);
        $.fn.editable.defaults.mode = 'popup';
    }

    //defaults
    //$.fn.editable.defaults.url = '/post';
    $.fn.editable.defaults.mode = 'inline'; //use this to edit inline
    //$.fn.editable.defaults.ajaxOptions = {url: $('#thyHome').attr('href') + '/save', type: 'text', send: 'auto'}

    //enable / disable
	
	$('#enable').click(function () {
        $('.editable').editable('toggleDisabled');
    });

    $('#btnsave').click(function () {

    });
    
    $('#trialf').editable({
        url: thisurl
	   ,send: 'auto'
	   ,validate: function (value) {
            if ($.trim(value) == '')
                return 'This field is required';
        },
		success: function(response, newValue)
		{
		  alert('trial');
		}
    });	
	
    $('#trialm').editable({
        url: thisurl
	   ,send: 'auto'
	   ,validate: function (value) {
            if ($.trim(value) == '')
                return 'This field is required';
        },
		success: function(response, newValue)
		{
		  alert('trial');
		}
    });	
	  
    //editables
    $('#username').editable({
        url: '/post',
        type: 'text',
        pk: 1,
        name: 'username',
        title: 'Enter username'
    });

    $('#lastname').editable({
        url: thisurl, type: 'text', send: 'auto',
        validate: function (value) {
            if ($.trim(value) == '')
                return 'This field is required';
        },
		success: function(response, newValue)
		{updateusername();}
    });

    $('#firstname').editable({
        url: thisurl, type: 'text', send: 'auto',
        validate: function (value) {
            if ($.trim(value) == '')
                return 'This field is required';
        },
		success: function(response, newValue)
		{updateusername();}
    });

    $('#middlename').editable({
        url: thisurl, type: 'text', send: 'auto',
        validate: function (value) {
            if ($.trim(value) == '')
                return 'This field is required';
        }
    });

    $('#mobileno').editable({
        url: thisurl, type: 'text', send: 'auto',
        validate: function (value) {
            if ($.trim(value) == '')
                return 'This field is optional';
        }
    });

    $('#telno').editable({
        url: thisurl, type: 'text', send: 'auto',
        validate: function (value) {
            if ($.trim(value) == '')
                return 'This field is optional';
        }
    });

    $('#birthplace').editable({
        url: thisurl, type: 'text', send: 'auto'
    });


    $('#sex').editable({
        url: thisurl, type: 'text', send: 'auto',
        prepend: "not selected",
        source: [{
            value: 'M',
            text: 'Male'
        }, {
            value: 'F',
            text: 'Female'
        }],
        display: function (value, sourceData) {
            var colors = {
                "": "gray",
                1: "green",
                2: "blue"
            }, elem = $.grep(sourceData, function (o) {
                return o.value == value;
            });

            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        }
    });

    $('#status').editable({
        url: thisurl, send: 'auto', type: 'select'
    });

    $('#csgroup').editable({
        url: thisurl, send: 'auto', type: 'text',
        showbuttons: false
    });

    $('#natgroup').editable({
        url: thisurl, send: 'auto', type: 'text',
        showbuttons: false
    });

    $('#relgroup').editable({
        url: thisurl, send: 'auto', type: 'text',
        showbuttons: false
    });

    $('#dob').editable({
        url: thisurl, send: 'auto', type: 'text'
    });

    $('#state').editable({
        source: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
		            "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas",
		            "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota",
		            "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey",
		            "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon",
		            "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas",
		            "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"
		        ]
    });
    
    var countries = [];
    $.each({
        "BD": "Bangladesh",
        "BE": "Belgium",
        "BF": "Burkina Faso",
        "BG": "Bulgaria",
        "BA": "Bosnia and Herzegovina",
        "BB": "Barbados",
        "WF": "Wallis and Futuna",
        "BL": "Saint Bartelemey",
        "BM": "Bermuda",
        "BN": "Brunei Darussalam",
        "BO": "Bolivia",
        "BH": "Bahrain",
        "BI": "Burundi",
        "BJ": "Benin",
        "BT": "Bhutan",
        "JM": "Jamaica",
        "BV": "Bouvet Island",
        "BW": "Botswana",
        "WS": "Samoa",
        "BR": "Brazil",
        "BS": "Bahamas",
        "JE": "Jersey",
        "BY": "Belarus",
        "O1": "Other Country",
        "LV": "Latvia",
        "RW": "Rwanda",
        "RS": "Serbia",
        "TL": "Timor-Leste",
        "RE": "Reunion",
        "LU": "Luxembourg",
        "TJ": "Tajikistan",
        "RO": "Romania",
        "PG": "Papua New Guinea",
        "GW": "Guinea-Bissau",
        "GU": "Guam",
        "GT": "Guatemala",
        "GS": "South Georgia and the South Sandwich Islands",
        "GR": "Greece",
        "GQ": "Equatorial Guinea",
        "GP": "Guadeloupe",
        "JP": "Japan",
        "GY": "Guyana",
        "GG": "Guernsey",
        "GF": "French Guiana",
        "GE": "Georgia",
        "GD": "Grenada",
        "GB": "United Kingdom",
        "GA": "Gabon",
        "SV": "El Salvador",
        "GN": "Guinea",
        "GM": "Gambia",
        "GL": "Greenland",
        "GI": "Gibraltar",
        "GH": "Ghana",
        "OM": "Oman",
        "TN": "Tunisia",
        "JO": "Jordan",
        "HR": "Croatia",
        "HT": "Haiti",
        "HU": "Hungary",
        "HK": "Hong Kong",
        "HN": "Honduras",
        "HM": "Heard Island and McDonald Islands",
        "VE": "Venezuela",
        "PR": "Puerto Rico",
        "PS": "Palestinian Territory",
        "PW": "Palau",
        "PT": "Portugal",
        "SJ": "Svalbard and Jan Mayen",
        "PY": "Paraguay",
        "IQ": "Iraq",
        "PA": "Panama",
        "PF": "French Polynesia",
        "BZ": "Belize",
        "PE": "Peru",
        "PK": "Pakistan",
        "PH": "Philippines",
        "PN": "Pitcairn",
        "TM": "Turkmenistan",
        "PL": "Poland",
        "PM": "Saint Pierre and Miquelon",
        "ZM": "Zambia",
        "EH": "Western Sahara",
        "RU": "Russian Federation",
        "EE": "Estonia",
        "EG": "Egypt",
        "TK": "Tokelau",
        "ZA": "South Africa",
        "EC": "Ecuador",
        "IT": "Italy",
        "VN": "Vietnam",
        "SB": "Solomon Islands",
        "EU": "Europe",
        "ET": "Ethiopia",
        "SO": "Somalia",
        "ZW": "Zimbabwe",
        "SA": "Saudi Arabia",
        "ES": "Spain",
        "ER": "Eritrea",
        "ME": "Montenegro",
        "MD": "Moldova, Republic of",
        "MG": "Madagascar",
        "MF": "Saint Martin",
        "MA": "Morocco",
        "MC": "Monaco",
        "UZ": "Uzbekistan",
        "MM": "Myanmar",
        "ML": "Mali",
        "MO": "Macao",
        "MN": "Mongolia",
        "MH": "Marshall Islands",
        "MK": "Macedonia",
        "MU": "Mauritius",
        "MT": "Malta",
        "MW": "Malawi",
        "MV": "Maldives",
        "MQ": "Martinique",
        "MP": "Northern Mariana Islands",
        "MS": "Montserrat",
        "MR": "Mauritania",
        "IM": "Isle of Man",
        "UG": "Uganda",
        "TZ": "Tanzania, United Republic of",
        "MY": "Malaysia",
        "MX": "Mexico",
        "IL": "Israel",
        "FR": "France",
        "IO": "British Indian Ocean Territory",
        "FX": "France, Metropolitan",
        "SH": "Saint Helena",
        "FI": "Finland",
        "FJ": "Fiji",
        "FK": "Falkland Islands (Malvinas)",
        "FM": "Micronesia, Federated States of",
        "FO": "Faroe Islands",
        "NI": "Nicaragua",
        "NL": "Netherlands",
        "NO": "Norway",
        "NA": "Namibia",
        "VU": "Vanuatu",
        "NC": "New Caledonia",
        "NE": "Niger",
        "NF": "Norfolk Island",
        "NG": "Nigeria",
        "NZ": "New Zealand",
        "NP": "Nepal",
        "NR": "Nauru",
        "NU": "Niue",
        "CK": "Cook Islands",
        "CI": "Cote d'Ivoire",
        "CH": "Switzerland",
        "CO": "Colombia",
        "CN": "China",
        "CM": "Cameroon",
        "CL": "Chile",
        "CC": "Cocos (Keeling) Islands",
        "CA": "Canada",
        "CG": "Congo",
        "CF": "Central African Republic",
        "CD": "Congo, The Democratic Republic of the",
        "CZ": "Czech Republic",
        "CY": "Cyprus",
        "CX": "Christmas Island",
        "CR": "Costa Rica",
        "CV": "Cape Verde",
        "CU": "Cuba",
        "SZ": "Swaziland",
        "SY": "Syrian Arab Republic",
        "KG": "Kyrgyzstan",
        "KE": "Kenya",
        "SR": "Suriname",
        "KI": "Kiribati",
        "KH": "Cambodia",
        "KN": "Saint Kitts and Nevis",
        "KM": "Comoros",
        "ST": "Sao Tome and Principe",
        "SK": "Slovakia",
        "KR": "Korea, Republic of",
        "SI": "Slovenia",
        "KP": "Korea, Democratic People's Republic of",
        "KW": "Kuwait",
        "SN": "Senegal",
        "SM": "San Marino",
        "SL": "Sierra Leone",
        "SC": "Seychelles",
        "KZ": "Kazakhstan",
        "KY": "Cayman Islands",
        "SG": "Singapore",
        "SE": "Sweden",
        "SD": "Sudan",
        "DO": "Dominican Republic",
        "DM": "Dominica",
        "DJ": "Djibouti",
        "DK": "Denmark",
        "VG": "Virgin Islands, British",
        "DE": "Germany",
        "YE": "Yemen",
        "DZ": "Algeria",
        "US": "United States",
        "UY": "Uruguay",
        "YT": "Mayotte",
        "UM": "United States Minor Outlying Islands",
        "LB": "Lebanon",
        "LC": "Saint Lucia",
        "LA": "Lao People's Democratic Republic",
        "TV": "Tuvalu",
        "TW": "Taiwan",
        "TT": "Trinidad and Tobago",
        "TR": "Turkey",
        "LK": "Sri Lanka",
        "LI": "Liechtenstein",
        "A1": "Anonymous Proxy",
        "TO": "Tonga",
        "LT": "Lithuania",
        "A2": "Satellite Provider",
        "LR": "Liberia",
        "LS": "Lesotho",
        "TH": "Thailand",
        "TF": "French Southern Territories",
        "TG": "Togo",
        "TD": "Chad",
        "TC": "Turks and Caicos Islands",
        "LY": "Libyan Arab Jamahiriya",
        "VA": "Holy See (Vatican City State)",
        "VC": "Saint Vincent and the Grenadines",
        "AE": "United Arab Emirates",
        "AD": "Andorra",
        "AG": "Antigua and Barbuda",
        "AF": "Afghanistan",
        "AI": "Anguilla",
        "VI": "Virgin Islands, U.S.",
        "IS": "Iceland",
        "IR": "Iran, Islamic Republic of",
        "AM": "Armenia",
        "AL": "Albania",
        "AO": "Angola",
        "AN": "Netherlands Antilles",
        "AQ": "Antarctica",
        "AP": "Asia/Pacific Region",
        "AS": "American Samoa",
        "AR": "Argentina",
        "AU": "Australia",
        "AT": "Austria",
        "AW": "Aruba",
        "IN": "India",
        "AX": "Aland Islands",
        "AZ": "Azerbaijan",
        "IE": "Ireland",
        "ID": "Indonesia",
        "UA": "Ukraine",
        "QA": "Qatar",
        "MZ": "Mozambique"
    }, function (k, v) {
        countries.push({
            id: k,
            text: v
        });
    });

    $('#country').editable({
        source: countries,
        select2: {
            width: 200
        }
    });

    $('#resaddress').editable({
        url: thisurl, send: 'auto', type: 'text',
        value: {
            address: "",
			street: "",
            brgy: "",
            city: "",
            prov: "",
            zip: ""
        },
        validate: function (value) {
            if (value.city == '')
                return 'city is required!';
        },
        display: function (value) {
            if (!value) {
                $(this).empty();
                return;
            }
            var html = $('<div>').text(value.address).html() +' ' +
			           $('<div>').text(value.street).html() + ' '+$('<div>').text(value.brgy).html()+' ' +
			           '<b>' + $('<div>').text(value.city).html() + ', '+$('<div>').text(value.prov).html()+'</b>, ' +
			             ' ' + $('<div>').text(value.zip).html();
			if(value.address=='')
        	{html='';}		
            $(this).html(html);
        }
    });

    $('#permaddress').editable({
        url: thisurl, send: 'auto', type: 'text',
        value: {
            address: "",
			street: "",
            brgy: "",
            city: "",
            prov: "",
            zip: ""
        },
        validate: function (value) {
            if (value.city == '')
                return 'city is required!';
        },
        display: function (value) {
            if (!value) {
                $(this).empty();
                return;
            }
            var html = $('<div>').text(value.address).html() +' ' +
			           $('<div>').text(value.street).html() + ' '+$('<div>').text(value.brgy).html()+' ' +
			           '<b>' + $('<div>').text(value.city).html() + ', '+$('<div>').text(value.prov).html()+'</b>, ' +
			             ' ' + $('<div>').text(value.zip).html();
			if(value.address=='')
        	{html='';}				 
            $(this).html(html);
        }
    });

    $('.editable').on('hidden', function (e, reason){
        if (reason === 'save' || reason === 'nochange') {
            var $next = $(this).closest('tr').next().find('.editable');
            if ($('#autoopen').is(':checked')) {
                setTimeout(function () {
                    $next.editable('show');
                }, 300);
            } else {
                $next.focus();
            }
        }
    });
	
	
  var xallow = $('#xenable').html();
  if(xallow==0||xallow=='0'){xdisarm(); }

  (function () {
    var $image = $('.img-container > img');
    var $dataX = 0;
    var $dataY = 0;
    var $dataHeight = 0;
    var $dataWidth = 0;
    var $dataRotate = 0;
    var $dataScaleX = 0;
    var $dataScaleY = 0;
    var options = {
          aspectRatio: NaN,
          preview: '.img-preview',
          crop: function (e) {
            $dataX = Math.round(e.x);
            $dataY = Math.round(e.y);
            $dataHeight = Math.round(e.height);
            $dataWidth = Math.round(e.width);
            $dataRotate = e.rotate;
            $dataScaleX = e.scaleX;
            $dataScaleY = e.scaleY;
          }
        };

    $image.on({
      'build.cropper': function (e) {
        console.log(e.type);
      },
      'built.cropper': function (e) {
        console.log(e.type);
      },
      'cropstart.cropper': function (e) {
        console.log(e.type, e.action);
      },
      'cropmove.cropper': function (e) {
        console.log(e.type, e.action);
      },
      'cropend.cropper': function (e) {
        console.log(e.type, e.action);
      },
      'crop.cropper': function (e) {
        console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
      },
      'zoom.cropper': function (e) {
        console.log(e.type, e.ratio);
      }
    }).cropper(options);

  }());
  
});

	