            var xinput = {};
			runAllForms();
			
			function msgbox(content){
			   $.smallBox({
				 title:     "Warning! "+content,
				 content:   "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
				 color:     "red",
				 iconSmall: "fa fa-warning",
			   });
			}
			
			function xvalidate_form(){
				var xvalid    = 0;
				var xcomplete = 0;
				var xref      = $('#content').attr('data-reference');		
				var xcaptcha  = (($('[name="captcha"]').length>0)?$('[name="captcha"]').val():xref);		
				var tmppwd    = $('[name="password"]').val();
				
				if($('.state-error').length > 0){return 1;}
				console.log('username');
				if($('[name="username"]').val()=='' || $('[name="username"]').val()==' '){return 2;}
				console.log('email');
				if($('[name="email"]').val()=='' || $('[name="email"]').val()==' '){return 2;}
				console.log('password');
				if($('[name="password"]').val()=='' || $('[name="password"]').val()==' '){return 2;}
				console.log('confirm');
				if($('[name="passwordConfirm"]').val()=='' || $('[name="passwordConfirm"]').val()==' '){return 2;}
				console.log('captcha - '+xref+':'+xcaptcha);
				if (tmppwd.search(/[a-zA-Z0-9]/i)==-1 || tmppwd.search(/[A-Z]/i)==-1 || tmppwd.search(/[0-9]/i)==-1){
				   $('[name="password"]').addClass('invalid');
				   $('[name="password"]').closest('label').removeClass('state-success');
				   $('[name="password"]').closest('label').addClass('state-error');
				   $('[name="password"]').closest('section').append('<em class="invalid" for="password">Password contain atleast one character and one number.</em>');
				   return 2;
				}
				
				if($('[name="captcha"]').length>0 && (xcaptcha.toUpperCase()!=xref.toUpperCase() || xcaptcha=='')){
				 $('[name="captcha"]').addClass('invalid');
				 $('[name="captcha"]').closest('label').removeClass('state-success');
				 $('[name="captcha"]').closest('label').addClass('state-error');
				 $('[name="captcha"]').closest('section').append('<em class="invalid" for="captcha">Invalid Captcha.</em>');
				 return 2;
				}
				return 0;
			}
			  
			$(document).ready(function(){
			  $('body').on('change','input[name="username"]',function(){
			      var tmpA = $(this).val();
				  $('input[name="email"]').val(tmpA);
              });
			  $('body').on('change','input[name="email"]',function(){
			      var tmpA = $(this).val();
				  $('input[name="username"]').val(tmpA);
              });

			  $('body').on('blur','input[type="text"]',function(){
			      $('#btnregister').removeClass('disabled');
			      $('#btnregister').removeAttr('disabled');
			      $('#btnregister').html('Register');  
                          });

			  $('body').on('blur','input[type="password"]',function(){
			      $('#btnregister').removeClass('disabled');
			      $('#btnregister').removeAttr('disabled');
			      $('#btnregister').html('Register');  
                          });

			  $('body').on('change','input[type="text"]',function(){
			      $('#btnregister').removeClass('disabled');
			      $('#btnregister').removeAttr('disabled');
			      $('#btnregister').html('Register');  
                          });

			  $('body').on('change','input[type="password"]',function(){
			      $('#btnregister').removeClass('disabled');
			      $('#btnregister').removeAttr('disabled');
			      $('#btnregister').html('Register');  
                          });

			  $('body').on('click','.xfrm_back',function(){
				 $('.xbtn_well').removeClass('hidden');
				 $('.xfrm_well').addClass('hidden');
                          });
			  
			  $('body').on('click','.btn-type',function(){
				  var xtype   = $(this).attr('data-type');
				  var xlabel  = $(this).html();
				  var ishiddn = $(this).is('.hidden');
				  if(xtype!=undefined && ishiddn==false){
					  $('.xtype').html(xlabel);
					  $('.xbtn_well').addClass('hidden');
					  $('.xfrm_well').removeClass('hidden');
					  $('#txntype').val(xtype);
				  }
			  });	
			  
			  
			  $('body').on('click','.btn-refresh-captcha',function(){
				//alert('trial lang');
				  $.ajax({
					   type: "POST",
					   url: base_url+'register/regenerate_captcha',
					   dataType: "JSON",
					   success: function (result){
						 if(result!=='')
						 {
						   $('[name="captcha"]').val('');
						   $('#content').attr('data-reference',result['word']);
						   $('#xcaptcha').html(result['image']);
						   console.log($('#content').attr('data-reference'));
						 }
					   }
				  });
				  
				  $('#btnregister').removeClass('disabled');
			      $('#btnregister').removeAttr('disabled');
			      $('#btnregister').html('Register');  
			   });
			   
			   $('[name="captcha"]').change(function(){
				  var xinput = $(this).val();
				      xinput = xinput.trim();
				  var xref   = $('#content').attr('data-reference');
				      xref   = xref.trim();
				  
				  $(this).removeClass('valid');
				  $(this).closest('label').removeClass('state-success');
				  $(this).removeClass('invalid');
				  $(this).closest('label').removeClass('state-error');
				
				  if(xinput.trim()!="" && xinput.toUpperCase()==xref.toUpperCase())
				  {
					$(this).addClass('valid');
					$(this).closest('label').removeClass('state-error');
					$(this).closest('label').addClass('state-success');
				  }
				  else
				  { 
			        $(this).addClass('invalid');
					$(this).closest('label').removeClass('state-success');
					$(this).closest('label').addClass('state-error');
				  }	  
				 
				 $('#btnregister').removeClass('disabled');
				 $('#btnregister').removeAttr('disabled');
				 $('#btnregister').html('Register');  
			   });
			   
			   $('body').on('focusout','[name="captcha"]',function(){
				  var xinput = $(this).val();
				      xinput = xinput.trim();
				  var xref   = $('#content').attr('data-reference');
				      xref   = xref.trim();
				  
				  $(this).removeClass('valid');
				  $(this).closest('label').removeClass('state-success');
				  $(this).removeClass('invalid');
				  $(this).closest('label').removeClass('state-error');
				
				  if(xinput.trim()!="" && xinput.toUpperCase()==xref.toUpperCase())
				  {
					console.log('valid');
					$(this).addClass('valid');
					$(this).closest('label').removeClass('state-error');
					$(this).closest('label').addClass('state-success');
				  }
				  else
				  { 
			        console.log('invalid');
			        $(this).addClass('invalid');
					$(this).closest('label').removeClass('state-success');
					$(this).closest('label').addClass('state-error');
				  }	  
				 
				 $('#btnregister').removeClass('disabled');
				 $('#btnregister').removeAttr('disabled');
				 $('#btnregister').html('Register');  
			   });
			   
			   $('#smart-form-register').submit(function(f){
				var xvalid = xvalidate_form();
				console.log(xvalid);
				if(xvalid>0)
				{
                 if(xvalid==1)
                 {					 
				  $.smallBox({
				   title: "Please correct the error found in the form!",
				   content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
				   color: "#C46A69",
				   iconSmall: "fa fa-check bounce animated",
				   timeout: 2000                  
			      });
				 }
				 else if(xvalid==2)
                 {					 
				  $.smallBox({
				   title: "Please complete the form before you click register!",
				   content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
				   color: "#C46A69",
				   iconSmall: "fa fa-check bounce animated",
				   timeout: 2000                  
			      });
				 }
				 f.preventDefault();	
				}
				
				//f.preventDefault();	
			    $('#btnregister').removeClass('disabled');
			    $('#btnregister').removeAttr('disabled');
			    $('#btnregister').html('Register');  
			   });
			   
			});
			
			// Model i agree button
			$("#i-agree").click(function(){
				$this=$("#terms");
				if($this.checked) {
					$('#myModal').modal('toggle');
				} else {
					$this.prop('checked', true);
					$('#myModal').modal('toggle');
				}
			});
			
			function showurl(){
				alert(document.URL + '/checkusername');
			}
			
			var thisurl = document.URL + "/checkusername";
			var thiseurl = document.URL + "/checkemail";
			// Validation
			$(function() {
				// Validation
				$("#smart-form-register").validate({

				 // Rules for form validation
				 rules : {
						    username : {
							           required : true,
							           minlength : 5,
							           maxlength : 30,
							           remote: {url: thisurl,type: "post"}
						               },
						    email : {
							        required : true,
							        email : true,
						            remote: {url: thiseurl,type: "post"}
						            },
						    password : {
							        required : true,
							        minlength : 5,
							        maxlength : 20
						            },
						    passwordConfirm : {
							        required : true,
							        minlength : 5,
							        maxlength : 20,
							        equalTo : '#password'
						           },
						 //firstname : {required : true},
						 //lastname : {required : true},
						 //gender : {required : true},
						   terms : {required : true}
					   },

					// Messages for form validation
					messages : {
						username : {
							required  : 'Please enter your login',
							remote: 'login ALREADY in use!'
						},
						email : {
							required : 'Please enter your email address',
							email : 'Please enter a VALID email address',
							remote: 'email ALREADY in use!'
						},
						password : {
							required : 'Please enter your password'
						},
						passwordConfirm : {
							required : 'Please enter your password one more time',
							equalTo : 'Please enter the same password as above'
						},
						//firstname : {required : 'Please select your first name'},
						//lastname : {required : 'Please select your last name'},
						//gender : {required : 'Please select your gender'},
						terms : {required : 'You must agree with Terms and Conditions'}
					},
                    // Ajax form submition
					submitHandler : function(form) {
				        var xvalid = xvalidate_form();
				            form  = $('#smart-form-register');  
				        $('#btnregister').addClass('disabled');
					    $('#btnregister').attr('disabled','disabled');
						$('#btnregister').html('<i class="fa fa-spinner fa-spin"></i> Register');
						
						if(xvalid>0)
						{
						 form.preventDefault();
						 $('#btnregister').removeClass('disabled');
					     $('#btnregister').removeAttr('disabled');
						 $('#btnregister').html('Register');
						}
						$(form).ajaxSubmit({
							success : function() 
							 {$("#smart-form-register").addClass('submited');},
							error: function (XHR, status, response) 
							 { 
							   alert('Form Submition Failed');
						       $('#btnregister').removeClass('disabled');
					           $('#btnregister').removeAttr('disabled');
					           $('#btnregister').html('Register');
						     }
						    });
					},

					// Do not change code below
					errorPlacement : function(error, element) 
					{
					    $('#btnregister').html('Register');
						error.insertAfter(element.parent());
					}
				});

			});