var xrequest;
$(document).ready(function(){

  $('body').on('click','.btnexecute',function(){
	var xdata = $('#query').val();
	$('#result').html('<i class="fa fa-refresh fa-spin"></i> Loading.. Please Wait..');
	//antidepressant();
	enablebtn(2);
    if(xdata=='' || xdata==undefined)
    {
	 $('#result').html('Nothinng to execute!');
	 enablebtn(1);
	 //rem_antidepr();
     return false;	 
	}		
	 
	xrequest = $.ajax({
			type:'POST',
			url: base_url+'sqlmanager/txn',
			data: {query:xdata},
			dataType:'JSON',
			success:function(rdata)
			{
			 if(rdata.result)
			  $('#result').html(rdata.content);	 
			 else
			  $('#result').html(rdata.error);
			 
			 enablebtn(1);
			 //rem_antidepr();					 
			},
			error: function(err)
			{
			 $('#result').html('Failed to Execute Query!');	
			 enablebtn(1);
			 //rem_antidepr();
			}					
		   });	 
    });
    
	$('body').on('keypress','#query',function(){
     $('.btnrefresh').removeAttr('disabled');
	});
	
	$('body').on('click','.btnstop',function(){
	  if(xrequest!=null){xrequest.abort(); xrequest=null; }
      $('#result').html('<i class="fa fa-warning animate shake"></i> Execution Manually Stopped!');
	  enablebtn(1);	  
	});
	
	$('body').on('click','.btnrefresh',function(){
	  $('#query').html('');
	  enablebtn(1);	
	});
});

function enablebtn(opt)
{
 if(opt==undefined){opt=1; }
 $('.btnexecute').attr('disabled',true);
 $('.btnstop').attr('disabled',true);
 $('.btnrefresh').attr('disabled',true);
 $('#query').attr('disabled',true);
 if(opt==1)
 {	 
  $('.btnexecute').removeAttr('disabled');
  $('.btnrefresh').removeAttr('disabled');
  $('#query').removeAttr('disabled');
 }
 else if(opt==2)
  $('.btnstop').removeAttr('disabled');
 	 
}
