function displaystudentinfo() {
        var studentno = document.getElementById('studentno').value;
        document.getElementById('hiddenstudentno').value=studentno;
        
        document.forms['studentsearch'].submit(function(event){
        event.preventDefault();  
        });
}


function submitform() {
        
        var studentname = document.getElementById('studentname').innerText;
        var studentno = document.getElementById('hiddenstudentno').value;
        var parentid = document.getElementById('userid').value;
        var birthdate = document.getElementById('dateofbirth').value;
        var termid = document.getElementById('termid').value;
        var regid = document.getElementById('registrationid').value;
        var receipt = document.getElementById('receipt').value;
        var date = document.getElementById('date').value;
        
        if (studentno == '') {
                  $.smallBox({
										title : "You failed to submit valid informations. Student Subscription is not sucessful!",
										content : "<i class='fa fa-clock-o'></i> <i> You pressed Submit...</i>",
										color : "#C46A69",
                                                                                iconSmall : "fa fa-times fa-2x fadeInRight animated",
										timeout : 4000
									});      
        }
        else{
        

        $.SmartMessageBox({
				title : "Student Subscription!",
				content : "You are now about to subscribe  " + studentname + ".",
				buttons : '[Cancel][Activate]',
				
			}, function(ButtonPressed) {
				if (ButtonPressed === "Activate") {
                                                $.ajax({
                                                type: "POST",
                                                url: document.URL + '/validatestudentinfo',
                                                data: {studno:studentno, birth:birthdate, term:termid, registration:regid, receiptno:receipt, payment:date}, 
                                                success:function(res)
                                                {
                                                                        if (res== 1) {
                                                                                                
                                                                                                document.forms['subscribe_validationinfo'].submit(function(event){
                                                                                                event.preventDefault();  
                                                                                                });                                                    
                                                                                                
                                                                                                $.smallBox({
										title : "Student Subscription is successful!",
										content : "<i class='fa fa-clock-o'></i> <i> You pressed Submit...</i>",
										color : "#659265",
										iconSmall : "fa fa-check fa-2x fadeInRight animated",
										timeout : 6000
									});  
                                                                        }
                                                                        else{
                                                       $.smallBox({
										title : "You failed to submit valid informations. Student Subcription is not successful!",
										content : "<i class='fa fa-clock-o'></i> <i> You pressed Submit...</i>",
										color : "#C46A69",
                                                                                iconSmall : "fa fa-times fa-2x fadeInRight animated",
										timeout : 6000
									});
                                                                        }
                                                                                                
                                                },
                                                error: function (XHR, status, response) {
                                                                                                
                                                $.smallBox({
                                                        title : "Internal Error!",
                                                        content : "<i class='fa fa-clock-o'></i> <i>You pressed Submit...</i>",
                                                        color : "#C46A69",
                                                        iconSmall : "fa fa-times fa-2x fadeInRight animated",
                                                        timeout : 6000
                                                        });	  
                                                }
                                                        
                        });
                                        										
				}
				if (ButtonPressed === "Cancel") {
					$.smallBox({
						title : "Student Subcription has been cancelled",
						content : "<i class='fa fa-clock-o'></i> <i>You pressed Cancel...</i>",
						color : "#C46A69",
						iconSmall : "fa fa-times fa-2x fadeInRight animated",
						timeout : 4000
					});
				}
	
			});
			e.preventDefault();
}
        
}

