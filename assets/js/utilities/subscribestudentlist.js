function unsubscribe(x)
{
	$.ajax({
		type: "POST",
		url: document.URL + '/countstudent',
		data: {}, 
		success:function(res)
		{
			if (res == 0) {
				$.smallBox({
					title : "Unable to unsubscribe!",
					content : "<i class='fa fa-clock-o'></i> <i> You must have atleast one(1) subscribed student!</i>",
					color : "#C46A69",
					iconSmall : "fa fa-times fa-2x fadeInRight animated",
					timeout : 7000
				});
			}else{
				
				$.SmartMessageBox({
					title : "Unsubscribe!",
					content : "Please enter your password",
					buttons : '[Cancel][Submit]',
					input : "password",
					placeholder : "Enter your pssword"
					
				}, function(ButtonPressed, Value) {
					if (ButtonPressed === "Submit") {
	
						$.ajax({
								type: "POST",
								url: document.URL + '/checkpwd',
								data: {pwd:Value}, 
								success:function(res)
								{
									if (res == 1)
									{
										document.getElementById('indexid').value=x;
										document.forms['subscribestudentlist_unsubscribe'].submit(function(event){
										event.preventDefault();  
										});
										
										$.smallBox({
											title : "Unsubcribe is successful!",
											content : "<i class='fa fa-clock-o'></i> <i> You pressed Submit...</i>",
											color : "#659265",
											iconSmall : "fa fa-check fa-2x fadeInRight animated",
											timeout : 6000
										});
									}
									else
									{
										$.smallBox({
											title : "Password is not valid! Unsubscribe is not successful!",
											content : "<i class='fa fa-clock-o'></i> <i>You pressed Submit...</i>",
											color : "#C46A69",
											iconSmall : "fa fa-times fa-2x fadeInRight animated",
											timeout : 6000
										});
									}
									
							},
								error: function (XHR, status, response) {
									
									$.smallBox({
										title : "Internal Error!",
										content : "<i class='fa fa-clock-o'></i> <i>You pressed Submit...</i>",
										color : "#C46A69",
										iconSmall : "fa fa-times fa-2x fadeInRight animated",
										timeout : 6000
									});	  
								}
				
							});	
					
					}
					if (ButtonPressed === "Cancel") {
						$.smallBox({
							title : "Unsubscribe has been cancelled",
							content : "<i class='fa fa-clock-o'></i> <i>You pressed Cancel...</i>",
							color : "#C46A69",
							iconSmall : "fa fa-times fa-2x fadeInRight animated",
							timeout : 4000
						});
					}
		
				});
				e.preventDefault();
				
				
				
			}
			
		},
		error: function (XHR, status, response)
		{
			$.smallBox({
				title : "Internal Error!",
				content : "<i class='fa fa-clock-o'></i> <i>You pressed Submit...</i>",
				color : "#C46A69",
				iconSmall : "fa fa-times fa-2x fadeInRight animated",
				timeout : 6000
			});
			
		}
	});
}