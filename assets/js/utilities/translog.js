var $xtimeout;
var executed = 0;
var page=0;
var limit=20;
var onlast=false;
var xlastid = 0;
var xsearch = 0;

$('document').ready(function(){
 $('body').on('change','.datepicker',function(){
	var time = $(this).attr('data-time');
	var xval = $(this).val();
	$(this).val(((xval.indexOf(':')<0)? (xval+' '+time) : xval));
 });
 
 $('body').on('click','.btnprev',function(){
  antidepressant();
  if(page>0){page--; }
  load_list();
 });
   
 $('body').on('click','.btnnext',function(){
  antidepressant();
  page++;
  load_list();
 });
 
 $('body').on('click','.btnrefresh',function(){
  antidepressant();
  xsearch=0;
  page=0;
  $('tr[data-search]').removeAttr('data-search');
  $('#uname').val('');
  $('#ipadd').val('');
  $('#xmodule').val('');
  $('#xaction').val('');
  $('#xparam').val('');
  load_list();
 });
 
 $('body').on('click','.btnsearch',function(){
  antidepressant();
  xsearch=1;
  page=0;
  load_list();
 });
 
});

function load_list(lastid,opt,callback)
{
 console.log('loading list from:'+lastid);
 if(lastid===undefined) lastid='';
 if(opt===undefined) opt=0;
 if(callback===undefined) callback='';
 var xstart = ((xsearch==1)?$('#eventstart').val():'');
 var xend   = ((xsearch==1)?$('#eventend').val():'');
 var xname  = ((xsearch==1)?$('#uname').val():'');
 var xipadd = ((xsearch==1)?$('#ipadd').val():'');
 var xmod   = ((xsearch==1)?$('#xmodule').val():'');
 var xact   = ((xsearch==1)?$('#xaction').val():'');
 var xparam = ((xsearch==1)?$('#xparam').val():'');
 var xlink  = base_url+'translogs/getdata/'+lastid;
 var xpage  = page*limit;
 $.ajax({
		 type: "POST",
		 url: xlink,
		 data:{estart:xstart,eend:xend,uname:xname,ipadd:xipadd,module:xmod,action:xact,param:xparam,page:xpage,limit:limit},
		 dataType: "JSON",
		 success: function (result)
			  {
				if(result.result)
				{
				 var arr_count = result.content.length;
				 if(arr_count>0)
				 {	
				  onlast=false;
                  $('#transbody').html('');			 
				  for(i=0;i<arr_count;i++)
                  {
			       var tmprow = $('#dt_basic .second').clone().html('');
				   for(j=1;j<8;j++)
				   {
				    //tmprow.addClass('hidden');
				    tmprow.removeClass('second');
				    tmprow.attr('data-row',result.content[i][0]);	  
				    tmprow.append('<td>'+result.content[i][j]+'</td>');  
				   }
                   $('#dt_basic tbody').append(tmprow);
				  }
				 }
				 else
				 {
				  onlast=true;
				  page = ((page>0)?page-1:0);
				  rem_antidepr(); 
				 }	 
                 xlastid=result.last;				 
				 rem_antidepr();
				}
				else
				{
				 onlast=true;
				 page = ((page>0)?page-1:0);
				 rem_antidepr();
				}
				
				if(onlast)
				  $('.btnnext').attr('disabled',true);
				else
				  $('.btnnext').removeAttr('disabled');
			    
				if(page==0)
				  $('.btnprev').attr('disabled',true);
				else
				  $('.btnprev').removeAttr('disabled');
			  
				//console.clear();
                if(callback!=''){callback();}				
			  }
         ,error: function (XHR, status, response) 
		 {
		  alert(response);
		  rem_antidepr();
		  //console.clear();
		 }
		});
}
