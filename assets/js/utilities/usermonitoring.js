var $xtimeout;
var executed = 0;
var page=1;
var limit=10;
var onlast=false;
var xlastid =0;

function eliminateuser(btn)
{
 var xid = $(btn).attr("data-pointer");
 var trow = $(btn).closest('tr');
 var xlink = $("#mnuhome").attr('href');
 xlink = xlink.replace("home","usermonitoring/trash/"+xid);
 antidepressant();
 $.ajax({
		 type: "POST",
		 url: xlink,
		 dataType: "text",
		 success: function (result)
		          {
				    var str = result.trim();
					if(str!='' || str!='nodata')
					{
                     $(trow).remove();				 
					 rem_antidepr();
					} 
					else
                     rem_antidepr();						
				  }
         ,error: function (XHR, status, response) 
		 {
		 alert(response);
		 rem_antidepr();
		 }
		}); 
}

function activateuser(btn)
{
 var xid = btn.getAttribute("data-pointer");
 var xlink = $("#mnuhome").attr('href');
 xlink = xlink.replace("home","usermonitoring/activate/"+xid);
 antidepressant();
 $.ajax({
		 type: "POST",
		 url: xlink,
		 dataType: "text",
		 success: function (result)
		          {
				    var str = result.trim();
					if(str!='' || str!='nodata')
					{
					 $('tr[data-row="'+xid+'"]').find('.btn-primary').attr('data-active',1);
					 $('tr[data-row="'+xid+'"]').find('td:nth-child(9)').html('<i class="fa fa-check-square-o">');
					 refreshlist();
					}					
				  }
         ,error: function (XHR, status, response) 
		 {
		 alert(response);
		 rem_antidepr();
		 }
		});
}

function optAll()
{
 var xchecked = $('#xuserlist #xchkall:checked').length;
 
 if(xchecked==1)
 {
  $('#xuserlist input[id*="xchkuser"]').prop('checked',true);
 }
 else
 {
  $('#xuserlist input[id*="xchkuser"]').prop('checked',false);
 }
}

function checkopt()
{
 var xlength= $('select[name="xuserlist_length"][aria-controls="xuserlist"]').val();
 var xcount= $('#xuserlist input[id*="xchkuser"]:checked').length;
 $('#xuserlist #xchkall').prop('checked',false);
 
 if(xlength==xcount)
  $('#xuserlist #xchkall').prop('checked',true);
}

function bulkmgmt(xopt)
{
//alert($('#xuserlist input[id*="xchkuser"]:checked').length);
var xcount = $('#xuserlist input[id*="xchkuser"]:checked').length;
executed = 0;

if(xcount<=0)
 return false;

antidepressant();

$('#xuserlist input[id*="xchkuser"]:checked').each(function()
{
var xid = this.getAttribute("value");
//alert(xid);
var xlink = $("#mnuhome").attr('href');
if(xopt==1)
 xlink = xlink.replace("home","usermonitoring/trash/"+xid);
else
 xlink = xlink.replace("home","usermonitoring/activate/"+xid);


$.ajax({
	 type: "POST",
	 url: xlink,
	 dataType: "text",
	 success: function (result)
			  {
				var str = result.trim();
				if(str!='' || str!='nodata')
				{
				 if(xopt==1)
				  $('tr[data-row="'+xid+'"]').remove();	 
				 else
                 {
				  $('tr[data-row="'+xid+'"]').find('.btn-primary').attr('data-active',1);
				  $('tr[data-row="'+xid+'"]').find('td:nth-child(9)').html('<i class="fa fa-check-square-o"></i>');
				 }
		    
				 executed++; 
				 donebulkajax(executed,xcount);
				}					
			  }
	 ,error: function (XHR, status, response) 
	 {
	  alert(response);
	  rem_antidepr();
	 }
	});

/**/	
});

}

function donebulkajax(x,y)
{
 if(x==y)
  refreshlist();
}

function refreshlist()
{
 antidepressant();
 $('tr[data-row] input[type="checkbox"]').prop('checked',false);
 page=1;
 show_page(page,limit);
}

function load_list(lastid,opt,callback)
{
 console.log('loading list from:'+lastid);
 if(lastid===undefined) lastid='9999';
 if(opt===undefined) opt=0;
 if(callback===undefined) callback='';
 var xlink = base_url+"usermonitoring/xlist/"+lastid;
 $.ajax({
		 type: "GET",
		 url: xlink,
		 dataType: "JSON",
		 success: function (result)
			  {
				if(result.result)
				{
				 var arr_count = result.content.length;
				 for(i=0;i<arr_count;i++)
                 {
			      if($('tr[data-row="'+result.content[i][0]+'"]').length<=0)
				  {	  
				   var tmprow = $('#xuserlist tfoot tr').clone();
                   for(j=1;j<10;j++)
				   {
				    tmprow.addClass('hidden');
				    tmprow.attr('data-row',result.content[i][0]);	  
				    tmprow.find('td:nth-child('+j+')').html(result.content[i][j]);  
				   }
                   $('#xuserlist tbody').append(tmprow);
				  }
                  else
                  {
				   for(j=1;j<10;j++)
				   {
				    $('tr[data-row="'+result.content[i][0]+'"]').find('td:nth-child('+j+')').html(result.content[i][j]);  
				   }
                  }					  
				 }
                 xlastid=result.lastid;				 
				 rem_antidepr();
				}
				else
				{
				 onlast=true;
				 console.clear();
				 rem_antidepr();
				}
				
				console.clear();
                if(callback!=''){callback();}				
			  }
         ,error: function (XHR, status, response) 
		 {
		  alert(response);
		  rem_antidepr();
		  console.clear();
		 }
		});
}

function search_list()
{
 var xval = $('#txtsearch').val();
 var xlink = base_url+"usermonitoring/slist/";
 $.ajax({
		 type: "POST",
		 url: xlink,
		 data:{search:xval},
		 dataType: "JSON",
		 success: function (result)
			  {
				$('.alert-nodata').addClass('hidden');
				if(result.result)
				{
				 $('#xuserlist tbody tr').addClass('hidden');
				 var arr_count = result.content.length;
				 for(i=0;i<arr_count;i++)
                 {
			      if($('tr[data-row="'+result.content[i][0]+'"]').length<=0)
				  {	  
				   var tmprow = $('#xuserlist tfoot tr').clone();
                   for(j=1;j<10;j++)
				   {
				    tmprow.attr('data-row',result.content[i][0]);	  
				    tmprow.find('td:nth-child('+j+')').html(result.content[i][j]);  
				   }
                   $('#xuserlist tbody').append(tmprow);
				  }
                  else
                  {
				   $('tr[data-row="'+result.content[i][0]+'"]').removeClass('hidden');	  
				   for(j=1;j<10;j++)
				   {
				    $('tr[data-row="'+result.content[i][0]+'"]').find('td:nth-child('+j+')').html(result.content[i][j]);  
				   }
				  }
                  if($('tr[data-row]:not(.hidden)').length <=0){$('.alert-nodata').removeClass('hidden'); }				  
				 }
                 rem_antidepr();
				}
				else
				{
				 $('#xuserlist tbody tr').addClass('hidden');
				 $('.alert-nodata').removeClass('hidden');
				 rem_antidepr();
				}
			  }
         ,error: function (XHR, status, response) 
		 {
		  alert(response);
		  rem_antidepr();
		 }
		});
}

function refresh_datatables()
{
 rem_antidepr();
 show_page(page,limit);
}

function load_subscription(xuserid)
{
 var xlink = $('#mnuhome').attr('href');
     xlink = xlink.replace("home","usermonitoring/getsubscriptionlist/");
	 
 $('.sub_list').removeClass('hidden');
 $('#subscription tbody').html('');
  
  $.ajax({
	 type: "POST",
	 url: xlink,
	 data: {username:xuserid},
	 dataType: "text",
	 success: function (result)
			  {
				var str = result.trim();
				$('#subscription tbody').html(str);
				console.log('content:'+$('#subscription tbody').html());
			  },
	 error: function (XHR, status, response){alert(response);}
	});
}

function show_page(spage,slimit)
{
 $('#txtsearch').val('');
 if(xlastid==0){xlastid = $("#xuserlist [data-row]:last").attr('data-row'); }
 if(xlastid==undefined){xlastid=0;}
 console.log('lastid is:'+xlastid);
 load_list(xlastid,1,function(){
  var xcount = $('#xuserlist tbody tr').length;
  if(spage>1){spage=spage*limit;}
  $('.alert-nodata').addClass('hidden'); 
  var lastrow = spage+slimit;
  $('#xuserlist tbody tr').addClass('hidden');
  for(i=spage;i<lastrow;i++)
  {
   $('#xuserlist tbody').find('tr:nth-child('+i+')').removeClass('hidden');	 
  }	  
 });
}

$(document).ready(function(){
   antidepressant();
   console.log('done');
   setTimeout(function(){refresh_datatables(); },800);
   
   $('body').on('click','.btn-delete_sub',function(){$(this).closest('tr').remove();}); 	
   $('body').on('click','.btn-refresh_sub',function(){$('#subscription tbody').html(''); if($('#opt').val()>0 && $('#uidtype').val()==2){load_subscription($('#prevuname').val());}});
   $('body').on('click','.btn-new_sub',function(){
	   var tmp = $('#subscription tfoot').find('.sub_pattern').clone();
	   $('#subscription tbody').append(tmp);
   }); 
   
   $('body').on('click','.btnprev',function(){
	 antidepressant();
	 if(page>1) page--;
	 show_page(page,limit);
   });
   
   $('body').on('click','.btnnext',function(){
	 antidepressant();
	 page++;
	 show_page(page,limit);
   });
   
   $('body').on('click','.btnsearch',function(){
	 var search_value = $('#txtsearch').val();
     if(search_value.trim()!='')
     {		  
	  antidepressant();
	  search_list();
     }	   
   });
   
   $('body').on('keyup','#txtsearch',function(e){
	 if(e.keyCode == 13){$('.btnsearch').click(); }
	 else
	 {
	  if($('#txtsearch').val()==''){
	   antidepressant();
	   show_page(page,limit); 
	  }	 
	 }	 
   });
   
   $('#uidtype').change(function(){
	 var xvalue=$(this).val();
	 if(xvalue=='2')
	   $('.sub_list').removeClass('hidden');
	 else
	   $('.sub_list').addClass('hidden');
   });
 });
 
 
 function create_user()
 {
  clear_mgmt();
 
  $('#xusermgmt #pass').html('<div class="form-group col-sm-12"><input type="password" class="form-control" id="upass" name="upass" placeholder="Password"/>'+
                                                    '<input type="password" class="form-control" id="urpass" name="urpass" placeholder="Re-entry Password"/></div>');
  
  $('#xusermgmt #upass').attr('onblur','xvalidate_inp(this)');
  $('#xusermgmt #urpass').attr('onblur','xvalidate_inp(this)');
 }
 
 function manage_user(btn)
 {
  clear_mgmt();
  var xid = btn.getAttribute("data-pointer");
  var xusername=btn.getAttribute("data-uname");
  var xfullname=btn.getAttribute("data-xname");
  var xsplit=xfullname.split('|');
  
  var xlname="";
  var xfname="";
  var xmname="";
  var xminame="";
  
  if(xsplit.length>0)
  {
   xlname=xsplit[0];
   xfname=xsplit[1];
   xmname=xsplit[2];
   xminame=xsplit[3];
  }
  
  var xemail=btn.getAttribute("data-email");
  var xtelno=btn.getAttribute("data-telno");
  var xidno=btn.getAttribute("data-idno");
  var xidtype=btn.getAttribute("data-type");
  var xactive=btn.getAttribute("data-active");
  
  $('#xusermgmt #opt').val(1);
  $('#xusermgmt #prevuname').val(xusername);
  $('#xusermgmt #uname').val(xusername);
  $('#xusermgmt #lname').val(xlname);
  $('#xusermgmt #fname').val(xfname);
  $('#xusermgmt #mname').val(xmname);
  $('#xusermgmt #miname').val(xminame);
  $('#xusermgmt #email').val(xemail);
  $('#xusermgmt #mobile').val(xtelno);
  $('#xusermgmt #uidno').val(xidno);
  $('#xusermgmt #uidtype').val(xidtype);
  
  var xactivatebtn = '<div class="col-sm-12">'+
                     '<button class="btn btn-success" data-pointer="'+xid+'" data-dismiss="modal" aria-hidden="true" onclick="activateuser(this);"><i class="fa fa-check"></i> Validate</button>'+
                     '<button type="button" class="btn btn-danger" onclick="resetpass('+"'"+xusername+"'"+');"><i class="fa fa-gear"></i> Reset Password</button>'+
                     '<button type="button" class="btn btn-warning" onclick="regen_code('+"'"+xusername+"','"+xemail+"'"+');"><i class="fa fa-ticket"></i> Validation Code</button>'+
					 '</div>'+
                     '<div class="col-sm-12"><span id="xresult"></span></div>';
  
  if(xactive==1)
  {
   $('#xusermgmt #uactive').prop('checked',true);
   xactivatebtn = '<div class="col-sm-12">'+
                     '<button type="button" class="btn btn-warning" onclick="resetpass('+"'"+xusername+"'"+');"><i class="fa fa-gear"></i> Reset Password</button>'+
                     '<button type="button" class="btn btn-warning" onclick="regen_code('+"'"+xusername+"','"+xemail+"'"+');"><i class="fa fa-ticket"></i> Validation Code</button>'+
					 '</div>'+
                     '<div class="col-sm-12"><span id="xresult"></span></div>'; 
  }
  
  if(xidtype==2) load_subscription(xusername);
  
  $('#xusermgmt #pass').html(xactivatebtn);
 }
 
 function clear_mgmt()
 {
  $('#xusermgmt input[type="text"]').val('');
  $('#xusermgmt input[type="hidden"]').val('');
  $('#xusermgmt #opt').val(0);
  $('#xusermgmt input[type="password"]').val('');
  $('#xusermgmt input[type="checkbox"]').prop('checked',false);
  $('#xusermgmt select').val(0);
  
  $('#xusermgmt #pass').html('');
  $('#usermodification .modal-footer').show();
  $('.help-block').remove();
  $('.form-group').removeClass('has-error');
  
  $('#xusermgmt #uname').attr('onblur','xvalidate_inp(this)');
  $('#xusermgmt #email').attr('onblur','xvalidate_inp(this)');
  $('.sub_list').addClass('hidden');
  $('.sub_list tbody').html('');
 }
 
 function resetpass(x)
 {
  var xlink = $("#mnuhome").attr('href');
  xlink = xlink.replace("home","usermonitoring/reset/");
  antidepressant();
  $('#lnpass').html('');
  $.ajax({
		 type: "POST",
		 url: xlink,
		 data: {username:"'"+x+"'"},
		 dataType: "text",
		 success: function (result)
		          {
				    var str = result.trim();
					if(str!='' && str!='nodata')
					{
					 $('#lnpass').html('<b>New Pass: '+str+'</b> <button type="button" class="btn btn-success" data-dismiss="modal" aria-hidden="true">Ok</button>');
					 $('#xresult').html(str);
					 $('#usermodification .modal-footer').hide();
					}
					else
					{
					 $('#xresult').html('<b>Failed to Reset</b>');
					}
		            rem_antidepr();					
				  }
         ,error: function (XHR, status, response) 
		 {
		 alert(response);
		 rem_antidepr();
		 }
		}); 
 
 }
 
 function regen_code(x,y)
 {
  var xlink = base_url+"usermonitoring/regenerate/";
  
  antidepressant();
  $('#lncode').html('');
  $.ajax({
		 type: "POST",
		 url: xlink,
		 data: {username:"'"+x+"'",email:"'"+y+"'"},
		 dataType: "text",
		 success: function (result)
		          {
				    var str = result.trim();
					if(str!='' && str!='nodata')
					{
					 $('#xresult').html('<b>New Code: '+str+'</b> <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Ok</button>');
					 $('#usermodification .modal-footer').hide();
					}
					else
					{
					 $('#xresult').html('<b>Failed to Reset</b>');
					}
		            rem_antidepr();					
				  }
         ,error: function (XHR, status, response) 
		 {
		 alert(response);
		 rem_antidepr();
		 }
		}); 
 
 }
 
 function manage_save(x)
 {
  var xlink = $("#mnuhome").attr('href');
  xlink = xlink.replace("home","usermonitoring/management/");
  
  var xopt = $('#usermodification #opt').val();
  var xprevuname = $('#usermodification #prevuname').val();
  var xusername = $('#usermodification #uname').val();
  var xpass = '';
  
  var xvalidated = xvalidate_form();
  //alert(xvalidated);
  
  if(xvalidated==false)
  {
   rem_antidepr();	
   return false;
  }
  
  if(xopt==0)
  {
    xpass = $('#usermodification #upass').val();
  }
  var xemail = $('#usermodification #email').val();
  
  var xlname = $('#usermodification #lname').val();
  var xfname = $('#usermodification #fname').val();
  var xmname = $('#usermodification #mname').val();
  var xminame = $('#usermodification #miname').val();
  
  var xmobile = $('#usermodification #mobile').val();
  
  var xuidno = $('#usermodification #uidno').val();
  var xutype = $('#usermodification #uidtype').val();
  var xactive = $('#usermodification #uactive:checked').length;
  
  var xsubscript = '';
  //alert(xactive);
  
  if(xutype=='2')
  {
	$('#subscription tbody tr').each(function(){
	 var studno = $(this).find('[name="studno"]').val();
	 var relation = $(this).find('[name="relation"]').val();
	 xsubscript = xsubscript+((xsubscript!='')?'@':'')+studno+'|'+relation;
	});  
  }
  
  antidepressant();
  $.ajax({
		 type: "POST",
		 url: xlink,
		 data: {opt:xopt
		       ,prevuname:xprevuname
		       ,username:xusername
			   ,email:xemail
			   ,upass:xpass
			   ,lname:xlname
			   ,fname:xfname
			   ,mname:xmname
			   ,miname:xminame
			   ,mobile:xmobile
			   ,uidno:xuidno
			   ,uidtype:xutype
			   ,active:xactive
			   ,subscript:xsubscript
			   },
		 dataType: "text",
		 success: function (result)
		          {
				    rem_antidepr();
				    var str = result.trim();
					if(str!='' && str!='nodata')
					{
					 alertmsg('save',"Success Saving Data.");
					 clear_mgmt();
					 if(x==1)
					 {
					  $('#usermodification #cancel').click();
					  antidepressant();
					  refreshlist();
					 }
					}
					else
					{
					 alertmsg("danger","Failed to Save Data.");
					}			
				  }
         ,error: function (XHR, status, response) 
		 {
		  alert(response);
		  rem_antidepr();
		 }
		}); 
 
 }
 
function xvalidate_inp(xinp)
{
  var xtargetname = xinp.getAttribute("name");
  var xtargetvalue = xinp.value;
  var xwarning='';
  var xlink = $("#mnuhome").attr('href');
   
   $('.form-group:has(#'+xtargetname+') span').remove();
   $('.form-group:has(#'+xtargetname+')').removeClass('has-error');

   if(xtargetvalue=='' || xtargetvalue=== 'undefined')
     return false;
      
   if(xtargetname =='uname')
   {
    xlink = xlink.replace("home","register/checkusername/");
	
	if(xtargetvalue=='' || xtargetvalue=== 'undefined')
     return false;
	 
	if(xtargetvalue==$('#prevuname').val())
     return false; 
   
	$.ajax({
		 type: "POST",
		 url: xlink,
		 data: {username:xtargetvalue},
		 dataType: "text",
		 success: function (result)
		          {
				    var str = result.trim();
					if(str=='' || str=='false' || str=='nodata')
					{
					 $('.form-group:has(#uname)').addClass("has-error");
					 $('.form-group:has(#uname)').append('<span class="help-block"><i class="fa fa-warning"/> Username is already in use.</span>');
					}				
				  }
         ,error: function (XHR, status, response) 
		 {
		  alert(response);
		 }
		 });
    return false;		 
   }
   else if(xtargetname =='email')
   {
    xlink = xlink.replace("home","register/checkemail/");
	var xusername = $('#xusermgmt #uname').val();
	
	$.ajax({
		 type: "POST",
		 url: xlink,
		 data: {email:xtargetvalue,username:xusername},
		 dataType: "text",
		 success: function (result)
		          {
				    var str = result.trim();
					if(str=='' || str=='false' || str=='nodata')
					{
					 $('.form-group:has(#email)').addClass("has-error");
					 $('.form-group:has(#email)').append('<span class="help-block"><i class="fa fa-warning"/> Email already in use.</span>');
					}				
				  }
         ,error: function (XHR, status, response) 
		 {
		  alert(response);
		 }
		 });
    return false;		 
   }

}

function xvalidate_form()
{
  var xinputs = ['uname','upass','lname','fname','uidno','uidtype'];
  var output =true;
  
  for(i=0; i<xinputs.length;i++)
  {
	if($('#xusermgmt #'+xinputs[i]).val()=='' && (xinputs[i]!='uidno' && $('#xusermgmt #uidtype').val()!='2'))
	{
	  $('#xusermgmt #'+xinputs[i]).attr('onblur','xvalidate_inp(this);');
	  $('.form-group:has(#'+xinputs[i]+')').addClass("has-error");
      if($('.form-group:has(#'+xinputs[i]+') span').length<=0)
         $('.form-group:has(#'+xinputs[i]+')').append('<span class="help-block"><i class="fa fa-warning"/> Input is required.</span>');
      else	
         $('.form-group:has(#'+xinputs[i]+') span').html('<i class="fa fa-warning"/> Input is required.');
   
	  //$('.form-group:has(#'+xinputs[i]+')').append('<span class="help-block"><i class="fa fa-warning"/> Input is required.</span>');
  
	  rem_antidepr();
      output = false;
	}
  }
  
  if($('#upass').val()!=$('#urpass').val())
  {
   $('.form-group:has(#upass)').addClass("has-error");
   if($('.form-group:has(#upass) span').length<=0)
    $('.form-group:has(#upass)').append('<span class="help-block"><i class="fa fa-warning"/> Password must be the same.</span>');
   else	
    $('.form-group:has(#upass) span').html('<i class="fa fa-warning"/> Password must be the same.');
   
   rem_antidepr();
   output = false;
  }
  else if($('#upass').val()=='' || $('#urpass').val()=='')
  {
   $('.form-group:has(#upass)').addClass("has-error");
   if($('.form-group:has(#upass) span').length<=0)
      $('.form-group:has(#upass)').append('<span class="help-block"><i class="fa fa-warning"/> Password is a must.</span>');
   else
      $('.form-group:has(#upass) span').html('<i class="fa fa-warning"/> Password is a must.');
     
   rem_antidepr();
   output = false;
  }
  
  if($('.has-error').length>0)
  {
   rem_antidepr();
   output = false;
  }
  
  return output;
}

