var xuserid='';
var xidno  ='';
var xidtype='';

$(document).ready(function(){
 $('body').on('click','.table-parent',function(){
   var trchild = $(this).next('tr');
   if($(this).is('.active'))
   {
	$(this).removeClass('active');     
    $(this).find('i').removeClass('fa-minus');   
    $(this).find('i').addClass('fa-plus');
    $(trchild).addClass('hidden');    
    $(trchild).find('.table-child').addClass('hidden');    
    $(trchild).find('.table-parent').removeClass('active');    
    $(trchild).find('.table-parent').find('i').addClass('fa-plus');
    $(trchild).find('.table-parent').find('i').removeClass('fa-minus');    
   }
   else
   {
	$(this).addClass('active');   
    $(this).find('i').removeClass('fa-plus');   
    $(this).find('i').addClass('fa-minus');   
    $(trchild).removeClass('hidden');    
   }	   
 });
 
 $('body').on('click','.btnreset',function(){
  xuserid = '';
  xidno   = '';
  xidtype = '';
  $('.udefault').removeClass('success');
  $('.uusers').removeClass('success');  
  $('.txtuser').html('');
  $('.chkprivilege').prop('checked',false);
  
 });
 
 $('body').on('click','.udefault,.uusers',function(){
  $('.udefault').removeClass('success');
  $('.uusers').removeClass('success');  
  $(this).addClass('success');
  xuserid = (($(this).attr('data-uid')!==undefined)?($(this).attr('data-uid')):'');
  xidno   = (($(this).attr('data-idno')!==undefined)?($(this).attr('data-idno')):'');
  xidtype = (($(this).attr('data-idtype')!==undefined)?($(this).attr('data-idtype')):'');
  $('.txtuser').html($(this).html());
  get_privileges();
 });
 
 $('body').on('click','.btnsave',function(){
  var count=$('.chkprivilege').length;
  var p_id='';
  var xdata='';
  $('.chkprivilege').each(function(){
	var tmp_id=$(this).attr('page-id');
    var tmp_name=$(this).attr('page-name');
	var value=(($(this).prop('checked')==true)?'1':'0');
    if(p_id=='' || p_id!=tmp_id)
	 xdata=xdata+((xdata!='')?'|':'')+'p_id:'+tmp_id+','+$(this).attr('name')+':desc&'+$(this).attr('desc')+'#value&'+value;
    else
	 xdata=xdata+((xdata!='')?',':'')+$(this).attr('name')+':desc&'+$(this).attr('desc')+'#value&'+value;
    
	p_id=tmp_id;
  });	  
  //alert(xdata);
  if(xdata!='')
  {
   antidepressant();
   $.ajax({
            type: "POST",            
            url: base_url+'userprivileges/manage/set/save_default/',
            data: {userid:xuserid,idno:xidno,idtype:xidtype,data:xdata},
			dataType:'JSON',
            success: function (output) 
			{                
			 if(output.result)
			  alertmsg('save',output.content);
		     else
			  alertmsg('danger',output.error);
		     
             rem_antidepr();			 
            },
            error: function (result) 
			{
             alertmsg('danger','Error While Saving Data!');
			 rem_antidepr();
            }
        });	  
  }	  
 });
 
 $('body').on('click','.adduser',function(){
  //alert('lipad!');
  $('#selectmodal').find('#data').html('User');
  $('#selectmodal').find('#data-detail').html('a user from the list.');
  $('.select-list').html('<div class="alert alert-danger"><i class="fa fa-warning"></i> No Data Available.</div>');
  $('#selectmodal').modal('show');
  $('#selectfilter').attr('search-type','user');
  $('#selectfilter').val('');
  setTimeout(function(){$('#selectfilter').focus();},1000);
 });

 $('body').on('click','#btnselectitem',function(){
  //alert('yowzah!');
  antidepressant();
  if($('.select-list .info').length<=0){return false; }
  var tmp_username = $('.select-list .info').find('.username').html();
  var tmp_idno     = $('.select-list .info').find('.idno').html();
  var tmp_idtype   = $('.select-list .info').find('.idtype').attr('data');
  if(tmp_username=='' || tmp_username==undefined || tmp_idno=='' || tmp_idno==undefined || tmp_idtype=='' || tmp_idtype==undefined){return false; }
  $.ajax({
	     type:'POST',
		 url:base_url+'userprivileges/manage/set/save_custom_user/',
		 data:{userid:tmp_username,idno:tmp_idno,idtype:tmp_idtype},
		 dataType:'JSON',
		 success: function(output)
		 {
		  if(output.result)
		  {
		   $('#selectmodal').modal('hide');  
		  }	  
          else
           alert('blimey');			  
		  
		  rem_antidepr();
		 },
		 error:function(result)
		 {
		  alert('blimey'); 
		  rem_antidepr();
		 }
  });
  
 });
 
});

function get_privileges(){
 antidepressant();
 //rem_antidepr();
 $('.chkprivilege').prop('checked','false');
 $.ajax({
		type: "POST",            
		url: base_url+'userprivileges/manage/get/user_privilege/',
		data: {userid:xuserid,idno:xidno,idtype:xidtype},
		dataType:'JSON',
		success: function (output) 
		{                
		 if(output.result)
		 {
		  var xprivilege = output.content;
		  $.each(xprivilege,function(i,v){
			var xp_id=xprivilege[i]['p_id'];
            $.each(xprivilege[i],function(i2,v2){
			 if(i2!='p_id')
             {
			  var xp_name = i2;
              var xp_value=xprivilege[i][i2]['value'];	
              $('[page-id="'+xp_id+'"][name="'+xp_name+'"]').prop('checked',((xp_value==1)?true:false));
			  //console.log('[page-id="'+xp_id+'"][name="'+xp_name+'"] length:'+$('.chkprivilege [page-id="'+xp_id+'"][name="'+xp_name+'"]').length+' value:'+xp_value);
			  console.clear();
			 }				 
			});			
		  });
		 }
		 else
		  alertmsg('danger',output.error);
		 
		 rem_antidepr();			 
		},
		error: function (result) 
		{
		 alertmsg('danger','Error While Saving Data!');
		 rem_antidepr();
		}
	});	
}