'	VBS to read a BLOB (actually Image) column in SQL Server and save each row's image to a file.
'	Example here reads PDFs stored in DocImage.Image column and saves each to a PDF file.

'On Error Resume Next

Dim IDNo
Dim IDType
Dim HostName
Dim DBName
Dim Integrated
Dim UserName
Dim Password
Dim FilePath
Dim sql
Dim xcount

IDNo = Wscript.Arguments.Named("idno")
IDType = Wscript.Arguments.Named("idtype")
HostName = Wscript.Arguments.Named("host")
DBName = Wscript.Arguments.Named("db")
Integrated = Wscript.Arguments.Named("security")
UserName = Wscript.Arguments.Named("userid")
Password = Wscript.Arguments.Named("pass")
FilePath = Wscript.Arguments.Named("path")
sql = ""
xcount = 0


IF (IsNull(IDNo)= FALSE and IDNo <> "") then

Const adTypeBinary = 1
Const adSaveCreateOverWrite = 2

IF (IsNull(FilePath)= TRUE or FilePath= "") then
FilePath = "D:\dump.jpeg"
END IF

Set rs = CreateObject("ADODB.Recordset")

If (IsNull(Integrated)= True or Integrated=0) then
cn = "Provider=SQLOLEDB;Data Source=" & HostName & ";Initial Catalog=" & DBName & ";Integrated Security=SSPI;"
Else
cn = "Provider=SQLOLEDB;Data Source=" & HostName & ";Initial Catalog=" & DBName & ";User Id=" & UserName & ";Password=" & Password & ";"
End if

If IDType = 1 or IDType = "1" then
sql = "SELECT StudentPicture FROM ES_Students Where StudentNo='" & IDNo & "' and StudentPicture IS NOT NULL"
picfield = "StudentPicture"
ElseIf IDType = 3 or IDType = "3" then
sql = "SELECT Photo FROM Hr_Employees Where EmployeeID='" & IDNo & "' and Photo IS NOT NULL"
picfield = "Photo"
End if

IF sql <> "" then

rs.Open sql, cn
While Not rs.EOF
    fOut = FilePath
    Set stream = CreateObject("ADODB.Stream")
    With stream
        .Type = adTypeBinary
        .Open
        .Write rs.Fields(picfield).Value
        .SaveToFile fOut, adSaveCreateOverWrite
        .Close
    End With
	xcount = xcount + 1
    rs.MoveNext
Wend

rs.Close
End IF

END IF

Wscript.Quit