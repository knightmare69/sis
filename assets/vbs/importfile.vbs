' VBS to read a BLOB (actually Image) column in SQL Server and save each row's image to a file.
' Example here reads PDFs stored in DocImage.Image column and saves each to a PDF file.

' On Error Resume Next

Dim IDNo
Dim HostName
Dim DBName
Dim DBTable
Dim Integrated
Dim UserName
Dim Password
Dim FilePath
Dim FileName
Dim sql
Dim criteria
Dim binary
Dim xcount

IDNo = Wscript.Arguments.Named("idno")
HostName = Wscript.Arguments.Named("host")
DBName = Wscript.Arguments.Named("db")
DBTable = Wscript.Arguments.Named("tbl")
Integrated = Wscript.Arguments.Named("security")
UserName = Wscript.Arguments.Named("userid")
Password = Wscript.Arguments.Named("pass")
FilePath = Wscript.Arguments.Named("path")
FileName = Wscript.Arguments.Named("name")
sql = ""
xcount = 0


IF (IsNull(IDNo)= FALSE and IDNo <> "") then

Const adTypeBinary = 1

IF (IsNull(FilePath)= TRUE or FilePath= "") then
Wscript.Quit
END IF

IF (IsNull(DBTable)= TRUE or DBTable= "") then
Wscript.Quit
END IF

Set rs = CreateObject("ADODB.Recordset")

If (IsNull(Integrated)= True or Integrated=0) then
cn = "Provider=SQLOLEDB;Data Source=" & HostName & ";Initial Catalog=" & DBName & ";Integrated Security=SSPI;"
Else
cn = "Provider=SQLOLEDB;Data Source=" & HostName & ";Initial Catalog=" & DBName & ";User Id=" & UserName & ";Password=" & Password & ";"
End if

sql = "SELECT TOP 1 * FROM " & DBTable & " Where Attachment='" & FileName & "'"

IF sql <> "" then

rs.Open sql, cn, 2, 2

'IF rs.EOF THEN
    Set stream = CreateObject("ADODB.Stream")
    With stream
        .Type = adTypeBinary
        .Open
        .LoadFromFile FilePath
	binary = .Read
    'rs.Save	
        .Close
    End With
	
	'MsgBox binary
	
	rs.Fields("Data").Value = binary
    rs.Update	
	
'END IF

rs.Close
End IF

END IF

Wscript.Quit
'On Error Goto 0