<?php
//DO NOT ALTER ANY VALUE IN THIS FILE. IF VALUE IS ALTER AND AFFECTS THE SYSTEM PROCCESS, WE WILL NOT FIX IT.
define('HEADER_TITLE','PRISMS Online'); 
define('MAINTENANCE_MODE','0'); 
define('MAINTENANCE_START','06:00:00'); 
define('MAINTENANCE_MSG','<i class="fa fa-spinner fa-spin txt-color-blue"></i> Updating... We will resume by @MAINTENANCEMODE'); 
define('PROMPT_WARN',''); 
define('HELP_DESK','bedistaportal.help@sanbeda.edu.ph'); 
//===========================================================================
define('INSTITUTION','San Beda University'); 
define('INST_CODE','SBU'); 
define('INST_EMAIL','info@sanbeda.edu.ph'); 
define('INST_SITE','http://www.sanbeda.edu.ph'); 
define('INST_PHONE','850-8898'); 
//===========================================================================
define('xDBDRIVER','SQL Server Native Client 11.0'); 
// define('xHOST','DESKTOP-H3PCL0T'); 
// define('xDBNAME','SanBeda');
// define('xUSERNAME','sa'); 
// define('xPASSWORD','knightmare69'); 
define('xHOST','122.55.57.83'); 
define('xDBNAME','sbcm');
define('xUSERNAME','sa'); 
define('xPASSWORD','Princ3t3ch'); 
//===========================================================================
define('EMAIL_LIMIT','500'); 
define('EMAIL_TAG','PRISMS'); 
define('EMAIL_NOTIFY','1'); 
//===========================================================================
define('AUTO_VALID','1'); 
define('ENABLE_CAPTCHA','0'); 
define('CREATE_APP','0');
//===========================================================================
define('CREATE_STYLE','1'); 
define('ACTIVE_ADMISSION','1');
define('APPLICANT_REGISTER','1'); 
define('ACTIVE_STUDENT','1'); 
define('ENABLE_STUDENT','1'); 
define('ADAPT_STUDENT','1');
define('INST_STUD','Bedan'); 
define('ACTIVE_PARENT','1'); 
define('ENABLE_PARENT','1'); 
define('PARENT_ACTIVATE','0');
define('INST_PAR','Parent/Guardian of a Bedan'); 
define('ACTIVE_FACULTY','0'); 
define('ENABLE_FACULTY','1'); 
define('ADAPT_FACULTY','1'); 
define('INST_FAC','SBCA Faculty'); 
define('ACTIVE_ADMIN','0'); 
define('ENABLE_ADMIN','1'); 
define('INST_ADM','SBCA Administrator'); 
//===========================================================================
define('EMALPERPC',0);
//===========================================================================
define('PARENT_EVALUATION','0'); 
define('PARENT_LEDGER',''); 
define('PARENT_ACCOUNTABILITY',''); 
//===========================================================================
define('FACULTY_EMAIL','@bulsu.edu.ph');
define('FACULTY_EVALID',0);
//==========================================================================
define('SHOW_PROFILE',1);
define('REQS_PROFILE',1);
//==========================================================================
define('DELAY_MID',0);
define('DELAY_FIN',0);
//==========================================================================
define('ENABLE_RETENTION',0);
//==========================================================================
define('TERM_SHS',7);
define('TERM_CAS',27);
define('TERM_GRD',28);
//==========================================================================
define('WITH_CHINESE','0'); 
define('GRADES_GSHS_PERIOD_AVE','0'); 
define('GRADES_GSHS_GRADE1','1'); 
define('GRADES_GSHS_LGRADE1','0'); 
define('GRADES_GSHS_GRADE2','1'); 
define('GRADES_GSHS_LGRADE2','0'); 
define('GRADES_GSHS_GRADE3','1'); 
define('GRADES_GSHS_LGRADE3','0'); 
define('GRADES_GSHS_GRADE4','1'); 
define('GRADES_GSHS_LGRADE4','0'); 
define('GRADES_GSHS_GRADEF','1'); 
define('GRADES_GSHS_LGRADEF','0'); 
define('GRADES_GSHS_CONDUCT1','0'); 
define('GRADES_GSHS_LCONDUCT1','0'); 
define('GRADES_GSHS_CONDUCT2','0'); 
define('GRADES_GSHS_LCONDUCT2','0'); 
define('GRADES_GSHS_CONDUCT3','0'); 
define('GRADES_GSHS_LCONDUCT3','0'); 
define('GRADES_GSHS_CONDUCT4','0'); 
define('GRADES_GSHS_LCONDUCT4','0'); 
define('GRADES_GSHS_CONDUCTF','0'); 
define('GRADES_GSHS_LCONDUCTF','0'); 
//============================================================================
define('SKELETON_KEY','6d590cfaadb5ed2fa092caccab61af1543c1a29f'); 
//============================================================================
//EPayment
//============================================================================
define('Connect7',TRUE);
define('Connect7URL','http://testpay.7-eleven.com.ph:8888/');
define('Connect7ID','7-Eleven');
define('Connect7Key','628e936f45884030ac1f34bcde9c28efa6ae9c839623b45b8942bd4490e1f05d');

define('DragonPay',TRUE);


?>