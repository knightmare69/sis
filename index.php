﻿<?php
include('config.php');
//var_dump(fsockopen("smtp.gmail.com", 465, $errno, $errstr, 3.0));
//var_dump($errno);
//var_dump($errstr);
//die();


stream_context_set_default([
 'ssl' => [
        'verify_peer' => false,
        'verify_peer_name' => false,
   ]
]);
if(file_exists('executor.php')){include('executor.php');}
define('ENVIRONMENT', 'production');
if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			error_reporting(E_ALL);
		break;
	
		case 'testing':
		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}
}

	$system_path = 'system';
    $apps_path = 'application';
    
	
	
	
	
	
	
	
	
	

	if (defined('STDIN')){chdir(dirname(__FILE__));}
    if(file_exists('_') && file_exists('_/config/constants.php')){$apps_path = '_';}

	
	if (realpath($system_path)!== FALSE)
	{
		$system_path = realpath($system_path).'/';
	}

		$system_path = rtrim($system_path, '/').'/';

		if ( ! is_dir($system_path))
	{
		exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
	}

		define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
        define('EXT', '.php');
        define('BASEPATH', str_replace("\\", "/", $system_path));

		define('FCPATH', str_replace(SELF, '', __FILE__));

		define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));


	if (is_dir($apps_path))
	{
		define('APPPATH', $apps_path.'/');
	}
	else
	{
		if ( ! is_dir(BASEPATH.$apps_path.'/'))
		{
		 exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
		}

		define('APPPATH', BASEPATH.$apps_path.'/');
	}

	
require_once BASEPATH.'core/CodeIgniter.php';
?>