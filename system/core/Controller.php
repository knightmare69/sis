<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

    
	private static $instance;
    var $blacklisted = array('119.95.244.58');
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		self::$instance =& $this;
		//block listed users
		$xclient = $this->xidentifyuser();
		$this->IP_Bouncer($xclient);
		
		//Para lang may makita ang clint in time na super tagal mag load ng page
		//ob_start();
		//try
		//{echo file_get_contents(BASEPATH."core/Filler.html");}
		//catch(Exception $e)
		//{echo "Loading Page.. Please Wait...";}
		//log_message('debug', "Load Blank Page Filler");
		
		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();
		
		log_message('debug', "Controller Class Initialized");
	}

	public static function &get_instance()
	{
		return self::$instance;
	}
	
	public function StripSlashes($str)
    {
        if(get_magic_quotes_gpc())
        {
            $str = stripslashes($str);
        }
        return $str;
    }
    
    public function MessageBox($str){
        print "<script type=\"text/javascript\">"; 
        print "alert('". $this->StripSlashes($str) ."');"; 
        print "</script>";  
    }
	
    public function IP_Bouncer($ip)
	{
	  if(in_array($ip, $this->blacklisted))
      {
            //echo 'Your blocked in accessing this website.';
			include(APPPATH.'errors/error_403.php');
			log_message('error', "Restricted IP detected. IP:".$ip);
            exit();
      }
	}

    public function xidentifyuser()
    {
     $ipaddress = '';
     if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
     else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
     else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
     else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
     else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
     elseif($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
     else
        $ipaddress = 'UNKNOWN';
     return $ipaddress;
	}	
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */